<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Contactus;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Contactusfeedback;
use AdminBundle\Entity\Evaluationfeedback;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Evaluationfeedbackgallery;
use AdminBundle\Entity\Restaurantmaster;
use AdminBundle\Entity\Restaurantmenu;
use AdminBundle\Entity\Restaurantoffer;
use AdminBundle\Entity\Reviewmaster;
use AdminBundle\Entity\Usermaster;

class MyprofileController extends BaseController {
    
	public function __construct(){
		$this->checkSessionAction();
		$obj = new BaseController();
        $obj->checkUserStatus();
	}
    /**
     * @Route("/myprofile")
     * @Template()
     */
    public function indexAction() {



         $session = new session();
        $user_id = $session->get('user_id');
        $usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findOneBy(array('is_deleted' => 0, 'user_master_id' => $user_id));
        $reviewimage=null;
        if (!empty($usermaster)) {
                    $reviewimage = $this->getMediaUrl($usermaster->getUser_image());

        }
        return array('usermaster'=>$usermaster,'reviewimage'=>$reviewimage);
    }
     public function getMediaUrl($media_id) {
        $media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
        $media = $media_repository->find($media_id);

        $live_path = $this->container->getParameter('live_path');
        if (!empty($media)) {
            $media_url = $live_path . $media->getMedia_location() . '/' . $media->getMedia_name();
        } else {
            $media_url = $live_path . '/bundles/Resource/default.png';
        }

        return $media_url;
    }

    /**
     * @Route("/myprofile/validateuser")
     */
    public function validateuserAction(Request $request) {
       $data = array();

       $post_data = $request->request->all();

       if(!array_key_exists('password', $post_data)){
         $data['success'] = false;
         $data['message'] = 'Password is empty';
       }

        $session = $this->get('session');
        $email = $session->get('email');
        $password = $post_data['password'];

        if(!isset($email) or !isset($password)){
          $data['success'] = false;
          $data['message'] = 'Email or Password not found';

          echo json_encode($data);exit;
        }

        $user_check = $this->getDoctrine()->getManager()->getRepository(Usermaster :: class)->findOneBy(
            array(
                'email'=>$email,
                'password'=>md5($password),
                'user_role_id'=>3,
                'status'=>'active',
                'is_deleted'=>0,
              )
             );

        if(!empty($user_check)){
          $data['success'] = true;
          $data['message'] = 'User found';
        } else {
          $data['success'] = false;
          $data['message'] = 'User not found';
        }

        echo json_encode($data);exit;
    }

    /**
     * @Route("/myprofile/updatepassword")
     */
    public function updatepasswordAction() {
        $session = new session();
        $current_password = $_POST['password'];
        $new_password = $_POST['new_password'];
        $user_id = $session->get('user_id');
		

        if (!empty($current_password)) {
            $usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findOneBy(array('is_deleted' => 0, 'user_master_id' => $user_id, 'password' => md5($current_password)));
            if (empty($usermaster)) {
                return new Response('error');
            } else {
                $usermaster->setPassword(md5($new_password));
                $em = $this->getDoctrine()->getManager();
                $em->persist($usermaster);
                $em->flush();
                return new Response('success');
            }
        }
        return new Response($user_id);
    }

    /**
     * @Route("/myprofile/updateprofile")
     */
    public function updateprofileAction() {
        $session = new session();
		
        $live_path = $this->container->getParameter('live_path');
		
        $nickname = $_POST['nickname'];
        $fname =$_POST['fname'];
        $lname = $_POST['lname'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $user_id = $session->get('user_id');
        $filename = $_FILES['inputfile']['name'];
       $file_path = $this->container->getParameter('file_path');
                        //$path = $file_path . '/uploads/WSimages/';
                        // $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';
                        $upload_dir = $this->container->getParameter('upload_dir1');
                        $location = '/Resource/Restaurant-Evaluate';
                        $upload_dir = $upload_dir . $location;

                        if (!is_dir($upload_dir)) {
                            mkdir($upload_dir);
                        }

                        $clean_image = preg_replace('/\s+/', '', $filename);
                        $media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

                        $is_uploaded = move_uploaded_file($_FILES['inputfile']['tmp_name'], $upload_dir . '/' . $media_name);
                        $media_id12=0;
                        if ($is_uploaded) {
                            $em = $this->getDoctrine()->getManager();

                            $mediamaster = new Medialibrarymaster();
                            $mediamaster->setMedia_type_id(1);
                            $mediamaster->setMedia_title($media_name);
                            $mediamaster->setMedia_location("/bundles/{$location}");
                            $mediamaster->setMedia_name($media_name);
                            $mediamaster->setCreated_on(date('Y-m-d h:i:s'));
                            $mediamaster->setIs_deleted(0);

                            $em->persist($mediamaster);
                            $em->flush();

                            $media_id12 = $media_id = $mediamaster->getMedia_library_master_id();
                        }
        //$targ = "/Resource/Restaurant-Evaluate".$_FILES['inputfile']['name'];
       // move_uploaded_file($src, $targ);
            $usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findOneBy(array('is_deleted' => 0, 'email' => $email));
            if (!empty($usermaster)) {
                $usermaster_a = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Usermaster')
                        ->findOneBy(array('is_deleted' => 0, 'email' => $email, 'user_master_id' => $user_id));
                if (!empty($usermaster_a)) {
                    $media = 0;
                    $userbiocheck = $this->getDoctrine()
                                ->getManager()
                                ->getRepository('AdminBundle:Usermaster')
                                ->findOneBy(array('is_deleted' => 0, 'user_bio' => $nickname));
                    if(!empty($userbiocheck)){
                        $userbiocheck_a = $this->getDoctrine()
                                ->getManager()
                                ->getRepository('AdminBundle:Usermaster')
                                ->findOneBy(array('is_deleted' => 0, 'user_bio' => $nickname, 'user_master_id' => $user_id));
                        if(!empty($userbiocheck_a)){
                            $userbiocheck_a->setUser_bio($nickname);
                            $userbiocheck_a->setUser_firstname($fname);
                            $userbiocheck_a->setUser_lastname($lname);
                            $userbiocheck_a->setUser_mobile($phone);
                            $userbiocheck_a->setEmail($email);
                            if(!empty($media_id12)){
                            $userbiocheck_a->setUser_image($media_id12);
                            }
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($userbiocheck_a);
                            $em->flush();
							
//user image url
							$image_url = $live_path . '/bundles/Resource/default.png'; 
							if($media_id12 != 0 ){
								$image_url = $this->getImage($media_id12, $live_path) ; 
							}
							$this->get('session')->set('image_url',$image_url);	
                            return new Response('success');
                        }else{
                            return new Response('error2');
                        }
                    }else{
                        $usermaster2 = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Usermaster')
                            ->findOneBy(array('is_deleted' => 0, 'user_master_id' => $user_id));
                        $usermaster2->setUser_bio($nickname);
                        $usermaster2->setUser_firstname($fname);
                        $usermaster2->setUser_lastname($lname);
                        $usermaster2->setUser_mobile($phone);
                        $usermaster2->setEmail($email);
                        if(!empty($media_id12)){
                        $usermaster2->setUser_image($media_id12);
                        }
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($usermaster2);
                        $em->flush();
                        return new Response('success');
                    }

                } else {
                    return new Response('error');
                }
            } else {
                $media = 0;
                $usermaster->setUsername($nickname);
                $usermaster->setUser_firstname($fname);
                $usermaster->setUser_lastname($lname);
                $usermaster->setUser_mobile($phone);
                $usermaster->setEmail($email);
                if(!empty($media_id12)){
                $usermaster->setUser_image($media_id12);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($usermaster);
                $em->flush();
                return new Response('success');
            }
    }

}
