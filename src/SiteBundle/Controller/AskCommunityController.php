<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Communitychat;


class AskCommunityController extends BaseController {
    
	private $PERPAGE;
	
	public function __construct(){
		$obj = new BaseController();
        $obj->checkUserStatus();
		$this->PERPAGE = 5;
	}
	
	/**
     * @Route("/askCommunity",name="site_ask_community")
     * @Template()
     */
    public function indexAction()
    {	   
        if($this->get('session')->get('language_id') == null){
			$this->get('session')->set('language_id','1');
		}
		$chat_data = array();
	   
	   $start = 0;
	   $limit = $this->PERPAGE;
	   
	   $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   
	   $cnt_query = "select * from community_chat chat join user_master user on chat.user_id = user.user_master_id left join media_library_master media on media.media_library_master_id = user.user_image where parent_id= 0 and user.is_deleted=0 and chat.is_deleted=0 order by chat.created_datetime desc";
	   
	   $stmt1 = $con->prepare($cnt_query);
	   $stmt1->execute();
	   $cnt_chat = $stmt1->fetchAll();
	   
	   $count_chat = ceil(count($cnt_chat) / $this->PERPAGE);
	   
	   $get_chat_que_sql = "select chat.* ,user.username,user.user_bio,user.user_image,media.* from community_chat chat join user_master user on chat.user_id = user.user_master_id left join media_library_master media on media.media_library_master_id = user.user_image where parent_id= 0 and user.is_deleted=0 and chat.is_deleted=0 order by chat.created_datetime desc limit {$start},{$limit}";
       
	   $stmt = $con->prepare($get_chat_que_sql);
	   $stmt->execute();
	   $chat_main_que = $stmt->fetchAll();
	   if($chat_main_que){
			foreach($chat_main_que as $chat_que){
			   $get_chat_ans_sql = "select chat.* ,user.username,user.user_bio,user.user_image,media.* 
									from community_chat chat 
									join user_master user on chat.user_id = user.user_master_id 
									left join media_library_master media on media.media_library_master_id = user.user_image where parent_id='".$chat_que['community_chat_id']."' and user.is_deleted=0 and chat.is_deleted=0 order by chat.created_datetime desc";		
			   $em = $this->getDoctrine()->getManager(); 
			   $con = $em->getConnection();
			   $stmt = $con->prepare($get_chat_ans_sql);
			   $stmt->execute();
			   $chat_ans = $stmt->fetchAll();
			   $chat_details [] = array(
									'questio_id'=>$chat_que['community_chat_id'],
									'que_asked_by'=>$chat_que['user_bio'],
									'que_ask_date'=>$chat_que['created_datetime'],
									'media_title_asked_by'=>$chat_que['media_title'],
									'media_location_asked_by'=>$chat_que['media_location'],
									'main_que'=>$chat_que['community_chat'],
									'answer'=>$chat_ans,
									'parent_id'=>$chat_que['community_chat_id']
									);
			}
	   }
/*		echo "<pre>";
		print_r($chat_details);exit; */
		
		return array('count' => $count_chat, 'chat_details'=>$chat_details);	
	}
	
	/**
     * @Route("/paginateAskCommunity", name="site_askcommunity_pagination")
     */
    public function paginateAskCommunityAction()
    {
		$request = $this->getRequest();
		$session = $this->get('session');
		$page_no = $request->get('page_no');
		
		if(isset($page_no) && $page_no != ''){
			
			$start = ($page_no - 1) * $this->PERPAGE;
			$limit = $this->PERPAGE;
			
			$em = $this->getDoctrine()->getManager();
			$conn = $em->getConnection();
			
			$query = "select chat.* ,user.username,user.user_bio,user.user_image,media.* from community_chat chat join user_master user on chat.user_id = user.user_master_id left join media_library_master media on media.media_library_master_id = user.user_image where parent_id= 0 and user.is_deleted=0 and chat.is_deleted=0 order by chat.created_datetime desc limit {$start},{$limit}";
			
			$statement = $conn->prepare($query);
			$statement->execute();
			$chat_main_que = $statement->fetchAll();
			
			$html = '';
			if(!empty($chat_main_que)){
				$live_path = $this->container->getParameter('live_path');
				
				foreach($chat_main_que as $chat_que){
				   $get_chat_ans_sql = "select chat.* ,user.username,user.user_bio,user.user_image,media.* 
										from community_chat chat 
										join user_master user on chat.user_id = user.user_master_id 
										left join media_library_master media on media.media_library_master_id = user.user_image where parent_id='".$chat_que['community_chat_id']."' and user.is_deleted=0 and chat.is_deleted=0 order by community_chat_id desc";		
				   
				   $stmt = $conn->prepare($get_chat_ans_sql);
				   $stmt->execute();
				   $chat_ans = $stmt->fetchAll();
				   
					$label_id = 107;
					$language_id = $session->get('language_id');
					$word = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id'=>$label_id,'language_id'=>$language_id));
					$text = $word->getWord_name();
					
					$label_id = 107;
					$word1 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id'=>$label_id,'language_id'=>$language_id));
					$text1 = $word1->getWord_name();
				   
					$html .= '<div class="panel panel-default">
								<div class="panel-body">
									<div class="media ask-community-qa-wrap">
										<div class="media-left">';
					
										if($chat_que['media_title'] != ''){
											$html .= "<img class='media-object img-circle' src='{$live_path}/{$chat_que['media_location']}/{$chat_que['media_title']}'>";
										} else {
											$html .= "<img class='media-object img-circle' src='{$live_path}/bundles/Resource/default.png'>";
										}
										
										$html .= '</div>';
									
										$html .= '<div class="media-body">';
											
											$html .= "<h4 class='media-heading'>{$chat_que['user_bio']}</h4>";
											$created_datetime = date('d-m-Y', strtotime($chat_que['created_datetime']));
											$html .= "<span><small>{$created_datetime}</small></span>";
											$html .= "<p>{$chat_que['community_chat']}</p>";
											
											if(!empty($chat_ans)){
												if(count($chat_ans) > 3){
													$html .= "<div class='addscroll'>";
												} else {
													$html .= "<div>";
												}
												
												foreach($chat_ans as $_answer){
													$html .= "<div class='media ask-community-qa-wrap-nested'>
														<div class='media-left'>";
														
														if($_answer['media_title'] != ''){
															$html .= "<img class='media-object img-circle' src='{$live_path}/{$chat_que['media_location']}/{$chat_que['media_title']}'>";
														} else {
															if($_answer['user_id'] == 1){
																$html .= "<img class='media-object img-circle' src='{$live_path}/bundles/site/images/shrayek.png'>";
															} else {
																$html .= "<img class='media-object img-circle' src='{$live_path}/bundles/Resource/default.png'>";
															}
														}
														$html .= '</div>';
														
														$html .= '<div class="media-body">';
															$html .= "<h4 class='media-heading'>{$_answer['user_bio']}</h4>";
															$html .= "<p>{$_answer['community_chat']}</p>";
															
														$html .= '</div>';
													
													$html .= '</div>';
												}
												
												$html .= '</div>';
											}
						$html .= '</div>';
						
						$html .= '<div class="panel-footer">
									<div class="row">';
							if($this->get('session')->get('userid') != NULL){		
								$html .= "<form method='POST' action='{$live_path}saveReply'>";
							}	
								$html .= "<input type='hidden' name='parent_id' value='{$chat_que['community_chat_id']}'>";
								
								$html .= "<input type='hidden' name='question_id' value='{$chat_que['community_chat_id']}'>";
								
								$html .= "<div class='col-sm-10'>";
								
									$html .= "<input type='text' id='answer_{$chat_que['community_chat_id']}' name='answer' class='form-control' placeholder='{$text}' required>";
								
								$html .= "</div>";
								$html .= "<div class='col-sm-2'>";
								
								if($this->get('session')->get('userid') == NULL){
									$save_reply_function = "onclick = save_reply('false','{$chat_que['community_chat_id']}');";
								}else{
									$save_reply_function = '';
								}	
									$html .= "<button type='submit' id='replyBtn' class='btn btn-danger btn-block' {$save_reply_function}>{$text1}</button>";
									
								$html .= "</div>";
							if($this->get('session')->get('userid') != NULL){	
								$html .= '</form>';
							}
						$html .= "</div><div id='errorreply_{$chat_que['community_chat_id']}'></div>
								 </div>";
						
					$html .= '			</div>
									</div>
								</div>
							  </div>';
				}
			}
			
			$data = array(
				'success' => 1,
				'html' => $html
			);
		} else {
			$data = array(
				'success' => 0,
				'html' => ''
			);
		}
		
		echo json_encode($data);exit;
	}
	
	/**
     * @Route("/serchAskCommunity", name="site_askcommunity_search")
     */
    public function paginateAskCommunitySearchAction()
    {
		$request = $this->getRequest();
		$session = $this->get('session');
		$keyword = $request->get('keyword');
		
		if(isset($keyword) && $keyword != ''){
			
			$em = $this->getDoctrine()->getManager();
			$conn = $em->getConnection();
			
			$query = "select chat.* ,user.username,user.user_bio,user.user_image,media.* from community_chat chat join user_master user on chat.user_id = user.user_master_id left join media_library_master media on media.media_library_master_id = user.user_image where parent_id= 0 and user.is_deleted=0 and chat.is_deleted=0 and (user.username LIKE '%".$keyword."%' or user.user_bio LIKE '%".$keyword."%' or chat.community_chat LIKE '%".$keyword."%') order by chat.created_datetime desc";
			
			$statement = $conn->prepare($query);
			$statement->execute();
			$chat_main_que = $statement->fetchAll();
			
			$html = '';
			if(!empty($chat_main_que)){
				$live_path = $this->container->getParameter('live_path');
				
				foreach($chat_main_que as $chat_que){
				   $get_chat_ans_sql = "select chat.* ,user.username,user.user_bio,user.user_image,media.* 
										from community_chat chat 
										join user_master user on chat.user_id = user.user_master_id 
										left join media_library_master media on media.media_library_master_id = user.user_image where parent_id='".$chat_que['community_chat_id']."' and user.is_deleted=0 and chat.is_deleted=0 order by community_chat_id desc";		
				   
				   $stmt = $conn->prepare($get_chat_ans_sql);
				   $stmt->execute();
				   $chat_ans = $stmt->fetchAll();
				   
					$label_id = 107;
					$language_id = $session->get('language_id');
					$word = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id'=>$label_id,'language_id'=>$language_id));
					$text = $word->getWord_name();
					
					$label_id = 107;
					$word1 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id'=>$label_id,'language_id'=>$language_id));
					$text1 = $word1->getWord_name();
				   
					$html .= '<div class="panel panel-default">
								<div class="panel-body">
									<div class="media ask-community-qa-wrap">
										<div class="media-left">';
					
										if($chat_que['media_title'] != ''){
											$html .= "<img class='media-object img-circle' src='{$live_path}/{$chat_que['media_location']}/{$chat_que['media_title']}'>";
										} else {
											$html .= "<img class='media-object img-circle' src='{$live_path}/bundles/Resource/default.png'>";
										}
										
										$html .= '</div>';
									
										$html .= '<div class="media-body">';
											
											$html .= "<h4 class='media-heading'>{$chat_que['user_bio']}</h4>";
											$created_datetime = date('d-m-Y', strtotime($chat_que['created_datetime']));
											$html .= "<span><small>{$created_datetime}</small></span>";
											$html .= "<p>{$chat_que['community_chat']}</p>";
											
											if(!empty($chat_ans)){
												if(count($chat_ans) > 3){
													$html .= "<div class='addscroll'>";
												} else {
													$html .= "<div>";
												}
												
												foreach($chat_ans as $_answer){
													$html .= "<div class='media ask-community-qa-wrap-nested'>
														<div class='media-left'>";
														
														if($_answer['media_title'] != ''){
															$html .= "<img class='media-object img-circle' src='{$live_path}/{$chat_que['media_location']}/{$chat_que['media_title']}'>";
														} else {
															if($_answer['user_id'] == 1){
																$html .= "<img class='media-object img-circle' src='{$live_path}/bundles/site/images/shrayek.png'>";
															} else {
																$html .= "<img class='media-object img-circle' src='{$live_path}/bundles/Resource/default.png'>";
															}
														}
														$html .= '</div>';
														
														$html .= '<div class="media-body">';
															$html .= "<h4 class='media-heading'>{$_answer['user_bio']}</h4>";
															$html .= "<p>{$_answer['community_chat']}</p>";
															
														$html .= '</div>';
													
													$html .= '</div>';
												}
												
												$html .= '</div>';
											}
						$html .= '</div>';
						
						$html .= '<div class="panel-footer">
									<div class="row">';
							if($this->get('session')->get('userid') != NULL){		
								$html .= "<form method='POST' action='{$live_path}saveReply'>";
							}	
								$html .= "<input type='hidden' name='parent_id' value='{$chat_que['community_chat_id']}'>";
								
								$html .= "<input type='hidden' name='question_id' value='{$chat_que['community_chat_id']}'>";
								
								$html .= "<div class='col-sm-10'>";
								
									$html .= "<input type='text' id='answer_{$chat_que['community_chat_id']}' name='answer' class='form-control' placeholder='{$text}' required>";
								
								$html .= "</div>";
								$html .= "<div class='col-sm-2'>";
								
								if($this->get('session')->get('userid') == NULL){
									$save_reply_function = "onclick = save_reply('false','{$chat_que['community_chat_id']}');";
								}else{
									$save_reply_function = '';
								}	
									$html .= "<button type='submit' id='replyBtn' class='btn btn-danger btn-block' {$save_reply_function}>{$text1}</button>";
									
								$html .= "</div>";
							if($this->get('session')->get('userid') != NULL){	
								$html .= '</form>';
							}
						$html .= "</div><div id='errorreply_{$chat_que['community_chat_id']}'></div>
								 </div>";
						
					$html .= '			</div>
									</div>
								</div>
							  </div>';
				}
			}
			
			$data = array(
				'success' => 1,
				'html' => $html
			);
		} else {
			$data = array(
				'success' => 0,
				'html' => ''
			);
		}
		
		echo json_encode($data);exit;
	}
	
	
	/**
     * @Route("/saveReply",name="site_save_reply")
     * @Template()
     */	
	public function saveReplyAction(Request $req)
	{	
		if($this->get('session')->get('userid') != NULL){
			$check_que = "select community_chat_id from community_chat where parent_id=0 and community_chat_id='".$req->request->get('question_id')."'";
			$em = $this->getDoctrine()->getManager(); 
			$con = $em->getConnection();
			$stmt = $con->prepare($check_que);
			$stmt->execute();
			   $chat_que = $stmt->fetchAll();
			   if(!empty($chat_que))
				{
					$Communitychat_new = new Communitychat();
					$Communitychat_new->setCommunity_chat($req->request->get('answer'));  
					$Communitychat_new->setParent_id($req->request->get('parent_id'));
					$Communitychat_new->setCreated_datetime(date('Y-m-d H:i:s'));
					$Communitychat_new->setUser_id($this->get('session')->get('userid'));
					$em = $this->getDoctrine()->getManager(); 
					$em->persist($Communitychat_new);
					$em->flush();

					#loyaltyPoints changes
					if(!empty($Communitychat_new->getCommunity_chat_id())){
						
						$user_id = $this->get('session')->get('userid');	

						$activity_type = 'answer_question';
						$points_added = $this->addCompetitionPointsToUser($user_id,$activity_type,0,$Communitychat_new->getCommunity_chat_id(),'community_chat');
					}


					#loyaltyPoints changes
					$flash_message1 = "Your  Reply saved successfully";
					if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
					{
						$flash_message1 = "تم حفظ الرد بنجاح";
					}
					
					$entity = $this->getDoctrine()->getManager();
					$community_chat = $entity->getRepository('AdminBundle:Communitychat')->find($req->request->get('parent_id'));
					if(!empty($community_chat)){
						$community_chat->setCreated_datetime(date('Y-m-d H:i:s'));
						$entity->flush();
					}
					
					
					$this->get('session')->getFlashBag()->set('success_msg', $flash_message1);
					return $this->redirectToRoute('site_ask_community'); 
//					return new Response("success");
				} 
		}else{
			$flash_message = "Please Login first to Reply , Thank You";
			if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
			{
				$flash_message = "يرجى تسجيل الدخول أولا إلى الرد، شكرا لك";
			}
			$this->get('session')->getFlashBag()->set('success_msg',$flash_message);
			return $this->redirectToRoute('site_ask_community'); 					
		}	
	} 
	
	/**
     * @Route("/saveQuestion",name="site_save_question")
     * @Template()
     */	
	public function saveQuestionAction(Request $req)
	{
		if($this->get('session')->get('userid') != NULL){
			$Communitychat_new = new Communitychat();
			$Communitychat_new->setCommunity_chat($_POST['question']);  
			$Communitychat_new->setParent_id(0);
			$Communitychat_new->setCreated_datetime(date('Y-m-d H:i:s'));
			$Communitychat_new->setUser_id($this->get('session')->get('userid'));
			$em = $this->getDoctrine()->getManager(); 
			$em->persist($Communitychat_new);
			$em->flush();
			
			$flash_message1 = "Your Question Saved";
			if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
			{
				$flash_message1 = "تم حفظ سؤالك بنجاح";
			}
			
            $this->get('session')->getFlashBag()->set('success_msg',$flash_message1);
			//return $this->redirectToRoute('site_ask_community'); 
			return new Response("success");
		}else{
			//$flash_message = "Please Login first to ask Question , Thank You";
			//if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
			//{
			//	$flash_message = "يرجى تسجيل الدخول أولا لطرح سؤال، شكرا لك";
			//}
			
			//$this->get('session')->getFlashBag()->set('success_msg',$flash_message);
			//return $this->redirectToRoute('site_ask_community'); 	
			return new Response("error");		
		}		
	} 	
}
