<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Visitorcountermaster;
use AdminBundle\Entity\Competitionuserrelation;

use \DateTime;

class BaseController extends Controller {

    public function __construct() {
        date_default_timezone_set('Asia/Kuwait');
        //date_default_timezone_set('Asia/Kolkata');
    }

    
    
    function checkUserStatus() {
        $session = new Session();
        $is_active = true;

        if (array_key_exists('userid', $session->all())) {
            if ($session->has('userid')) {
                $servername = 'localhost';
                $username = 'shrayek_shrayek';
                $password = 'r6KnZEQrWA';
                $dbname = 'shrayek_testV1';

                $query = "select * from user_master where user_master_id = {$session->get('userid')}";
                $conn = mysqli_connect($servername, $username, $password, $dbname);

                if ($conn->connect_error) {
                    
                } else {
                    $sql = "select * from user_master where user_master_id = {$session->get('userid')}";
                    $result = $conn->query($sql);

                    if (!empty($result)) {
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                if ($row['status'] == 'inactive') {
                                    $is_active = false;
                                    $session->set('userid', '');
                                    if ($session->get('language_id') == 1) {
//                                        $session->getFlashBag()->set('error_msg1', 'Your account has been deactivated. Please contact Administrator for more detail.');
                                        $session->getFlashBag()->set('error_msg1', 'Your Membership Has Been Suspended Please contact site Admin');
                                    } else {
//                                        $session->getFlashBag()->set('error_msg1', 'حسابك تم تعطيله. يرجى الاتصال مسؤول لمزيد من التفاصيل.');
                                        $session->getFlashBag()->set('error_msg1', 'تم إيقاف عضويتك , يرجى مراجعة مسؤولي الموقع للإستفسار ');
                                    }
                                    $hostname = $_SERVER["SERVER_NAME"];
                                    header("location:http://" . $hostname . "/shreyak_live");
                                    exit;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function checkSessionAction() {
        $session = new Session();

        if (( $session->get('user_id') == '' && $session->get('username') == '')) {

            $hostname = $_SERVER["SERVER_NAME"];
            header("location:http://" . $hostname . "/shreyak_new");
            exit;
        }
    }

    public function getImage($media_library_master_id, $live_path) {

        $media_library = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $media_library_master_id, 'is_deleted' => 0));
        $r = '';
        if (!empty($media_library)) {

            $r = $live_path . $media_library->getMedia_location() . "/" . $media_library->getMedia_name();

            return $r;
        }
        return $r;
    }

    function mediauploadAction($file, $tmpname, $path, $upload_dir, $mediatype_id) {

        $clean_image = preg_replace('/\s+/', '', $file);
        $logo_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777);
        }
        //logo upload check
        if (move_uploaded_file($tmpname, $upload_dir . $logo_name)) {
            $medialibrarymaster = new Medialibrarymaster();

            $medialibrarymaster->setMedia_type_id($mediatype_id);
            $medialibrarymaster->setMedia_title($logo_name);
            $medialibrarymaster->setMedia_location($path);
            $medialibrarymaster->setMedia_name($logo_name);
            $medialibrarymaster->setCreated_on(date('Y-m-d H:i:s'));
            $medialibrarymaster->setIs_deleted(0);

            $em = $this->getDoctrine()->getManager();
            $em->persist($medialibrarymaster);
            $em->flush();
            $media_library_master_id = $medialibrarymaster->getMedia_library_master_id();
            return $media_library_master_id;
        } else {
            return FALSE;
        }
    }

    public function firequery($query) {
        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $data = $em->fetchAll();
        return $data;
    }

    public function addCompetitionPointsToUser($user_id,$activity_type,$restaurant_id,$related_id,$related_table,$category_id = 0){

        $evaluation_type_activities = array(
            'evaluation_with_image',
            'evaluation_with_comments',
            'evaluation_with_image_comment',
            'evaluation_without_image_comment',
            'evaluation_new_restaurant'
        );

        #check any competition exist
        $today = date('Y-m-d H:i:s');
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();

        $sql_comp_check = "select main_competition_id from competition_master
                            where start_date <= '$today' AND end_date >= '$today'
                            and is_deleted = '0' and status = 'active'";

        $comp_check_stmt = $con->prepare($sql_comp_check);
        $comp_check_stmt->execute();
        $comp_exist = $comp_check_stmt->fetchAll();
        
        if(!empty($comp_exist)){
            foreach($comp_exist as $_comp_exist){

                $comp_id = $_comp_exist['main_competition_id'];
                
                // check if category excluded
                $_resSql = "SELECT rel.* from restaurant_category_relation rel, category_master c, restaurant_master r where rel.main_category_id = c.main_category_id and rel.restaurant_id = r.main_restaurant_id and r.main_restaurant_id = {$restaurant_id} and rel.is_deleted = 0 and c.is_deleted = 0 and r.is_deleted = 0";
                $_resSqlRecord = $this->firequery($_resSql);

                $catList = null;
                if(!empty($_resSqlRecord)){
                    foreach($_resSqlRecord as $_rel){
                        $catList[] = $_rel['main_category_id'];
                    }

                    if(!empty($catList)){
                        $catList = implode(',', $catList);

                        $_sql = "SELECT * from competition_category_relation where competition_id = {$comp_id} and category_id in ($catList) and is_deleted = 0 and type = 'exclude' ";
                        $_sqlRec = $this->firequery($_sql);

                        if(!empty($_sqlRec)){
                            continue;
                        }
                    }
                }

/*
                $_sql = "SELECT * from competition_category_relation where competition_id = {$comp_id} and category_id = '$category_id' and is_deleted = 0 and type = 'exclude' ";
                $_sqlRec = $this->firequery($_sql);

//                echo"<pre>";print_r($_sqlRec);echo"</br>";
                if(!empty($_sqlRec)){
                    continue;
                }
*/

                #checkExclutionMaster 
                $exclude_rest = null;
                $extra_points = 0 ; 
                if(!empty($restaurant_id)){
                    $exclude_rest = $em->getRepository("AdminBundle:Competitionexclusionmaster")->findOneBy(["restaurant_id"=>$restaurant_id,"competition_id"=>$comp_id,"is_deleted"=>0]);
                    $extra_points_rest = $em->getRepository("AdminBundle:Competitionextrapoints")->findOneBy(["rest_id"=>$restaurant_id,"comp_id"=>$comp_id,"is_deleted"=>0]);
                    
                    if($extra_points_rest){
                        $extra_points += $extra_points_rest->getExtra_points();
                    }
                }

                if($exclude_rest){
                    continue;
                }

                if(!empty($category_id)){

                    $extra_points_category = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findOneBy(["category_id"=>$category_id,"competition_id"=>$comp_id,"is_deleted"=>0,'type'=>'include']);

                    if($extra_points_category){
                        $extra_points += $extra_points_category->getExtra_points();
                    }
                }                

                #get Activity Details and Points
                $sql_activity_details = "select * from activity_master 
                                         JOIN competition_activity_relation rel ON rel.activity_id = activity_master.main_activity_id  
                                         where activity_name = '$activity_type' and activity_master.is_deleted = '0'and  
                                        rel.is_deleted = '0' and rel.competition_id = '$comp_id'";
                $act_check_stmt = $con->prepare($sql_activity_details);
                $act_check_stmt->execute();
                $act_exist = $act_check_stmt->fetchAll();
                
                if($act_exist){

                    $act_exist = $act_exist[0];

                    $comp_points = $act_exist['points'];
                    $activity_id = $act_exist['main_activity_id'];

                    $new_points = new Competitionuserrelation();
                    $new_points->setCompetition_id($comp_id);
                    $new_points->setActivity_id($activity_id);
                    $new_points->setRestaurant_id($restaurant_id);
                    $new_points->setRelated_id($related_id);
                    $new_points->setRelated_table_name($related_table);
                    $new_points->setUser_id($user_id);
                    $new_points->setPoints($comp_points + $extra_points );
                    $new_points->setCreated_datetime(date('Y-m-d H:i:s'));
                    
                    if(in_array($activity_type,$evaluation_type_activities)){

                        #need to count after approved evaluation from admin
                        $new_points->setIs_deleted(1);

                    }else{
                        $new_points->setIs_deleted(0);

                    }

                    $em->persist($new_points);
                    $em->flush();

//                    echo "saved_points : ".$new_points->getCompetition_user_relation_id();
                }
            }
            return true;
        }
        
        return false;
    }

}
