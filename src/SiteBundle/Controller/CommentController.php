<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Siteprivacy;

class CommentController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/featured-comment/{comment_id}", defaults={"comment_id":""})
     * @Template()
     */
    public function indexAction($comment_id) {
        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();

        $review = "SELECT ef.*,um.user_firstname,um.user_lastname,um.user_bio,um.username,media_library_master.media_location , media_library_master.media_name from evaluation_feedback as ef 
		left join user_master as um on um.user_master_id=ef.user_id 
		left join media_library_master ON um.user_image = media_library_master.media_library_master_id 
		where ef.evaluation_feedback_id = {$comment_id} and ef.is_deleted=0 and ef.is_featured='yes' group by ef.user_id order by ef.evaluation_feedback_id";

        $stmt = $con->prepare($review);
        $stmt->execute();
        $feature_comment = $stmt->fetchAll();
        $feature_comments = [];

        if (!empty($feature_comment)) {
            $live_path = $this->container->getParameter('live_path');
            foreach ($feature_comment as $key => $val) {
                if ($val['media_location'] !== '' && $val['media_name'] != '')
                    $user_image = $live_path . $val['media_location'] . "/" . $val['media_name'];
                else
                    $user_image = $live_path . '/bundles/Resource/default.png';

                $feature_comments[] = array(
                    "evaluation_feedback_id" => $val['evaluation_feedback_id'],
                    "comments" => urldecode($val['comments']),
                    "userimage" => $user_image,
//                    "username" => $val['user_firstname'] . ' ' . $val['user_lastname'],
                    "username" => $val['user_bio'],
                    "created_datetime" => $val['created_datetime'],
                );
            }

            if (!empty($feature_comments)) {
                $feature_comments = $feature_comments[0];
            }
        }

        return array(
            'feature_comments' => $feature_comments
        );
    }

}
