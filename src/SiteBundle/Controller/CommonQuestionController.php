<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Commonquestion;



class CommonQuestionController extends BaseController {
	
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
    /**
     * @Route("/commonQuestion",name="site_common_question")
     * @Template()
     */
    public function indexAction()
    {
		if($this->get('session')->get('language_id') != ''){
            $language_id=$this->get('session')->get('language_id');
        }else{
            $language_id=1;
        }
		
		$common_que = array();
		$get_que_sql = "select * from common_question where is_deleted=0 and language_id='".$language_id."' and status='active' order by sort_order";		
		$em = $this->getDoctrine()->getManager(); 
		$con = $em->getConnection();
		$stmt = $con->prepare($get_que_sql);
		$stmt->execute();
		$common_que = $stmt->fetchAll();
				
		return array('common_que'=>$common_que);	
	}



}
