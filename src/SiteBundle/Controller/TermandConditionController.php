<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Termsconditionsmaster;



class TermandConditionController extends BaseController {
    
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
	/**
     * @Route("/termsAndCondition",name="site_terms_and_condition")
     * @Template()
     */
    public function indexAction()
    {	   
		if($this->get('session')->get('language_id') != ''){
            $language_id=$this->get('session')->get('language_id');
        }else{
            $language_id=1;
        }
		
		$terms_condition = array();	
		$terms_condition = $this->getDoctrine()->getManager()->getRepository(Termsconditionsmaster :: class)->findBy(array('is_deleted'=>0,'language_id'=>$language_id));
		
		return array('terms_condition'=>$terms_condition);	
	}



}
