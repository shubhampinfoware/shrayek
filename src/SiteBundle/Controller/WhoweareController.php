<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Aboutus;



class WhoweareController extends BaseController {
    
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
	/**
     * @Route("/whoWeAre",name="site_who_we_are")
     * @Template()
     */
    public function indexAction()
    {
		if(!empty($this->get('session')->get('language_id'))){
                    $language_id=$this->get('session')->get('language_id');
                }else{
                    $language_id=1;
                }
		
		$about_us = array();	
		$about_us = $this->getDoctrine()->getManager()->getRepository(Aboutus :: class)->findBy(array('is_deleted'=>0,'language_id'=>$language_id));
		return array('about_us'=>$about_us,'language_id'=>$language_id);	
	}



}
