<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Suggestioncomplaints;



class SuggestionandComplainController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/suggestionAndComplain",name="site_suggestion_complain")
     * @Template()
     */
    public function indexAction()
    {	
		return array();       
	}

	/**
     * @Route("/save_suggestion",name="site_save_suggestion")
     * @Template()
     */
    public function saveSuggestionAction(Request $req)
    {
	//	var_dump($req->request);exit;
		if($this->get('session')->get('userid') != NULL){	
			if($req->request->all()){
				$suggestion = new Suggestioncomplaints();
				$suggestion->setNick_name($req->request->get('nick_name'));
				$suggestion->setEmail_address($req->request->get('email'));
				$suggestion->setSubject($req->request->get('suggestion_type'));
				$suggestion->setComments($req->request->get('comments'));
				$suggestion->setCreated_date(date('Y-m-d H:i:s'));
				$em = $this->getDoctrine()->getManager();
				$em->persist($suggestion);
				$em->flush();
				$flash_message = "Thank you for sharing your Suggestions/Complaints with us.";	
				if($this->get('session')->get('language_id') == 2){
					$flash_message = "نشكرك على المشاركة بإقتراحاتك و إبلاغنا بالمشكله";
				}
				//$this->get('session')->getFlashBag()->set('success_msg',$flash_message);
				$this->get('session')->getFlashBag()->set('success_msg1',$flash_message);
				return $this->redirectToRoute('site_suggestion_complain');
			}
		}else{
			$flash_message = "Please Login first to Reply , Thank You";
			if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
			{
				$flash_message = "يرجى تسجيل الدخول أولا إلى الرد، شكرا لك";
			}
			//$this->get('session')->getFlashBag()->set('success_msg',$flash_message);
			$this->get('session')->getFlashBag()->set('success_msg1',$flash_message);
			return $this->redirectToRoute('site_default_index'); 							
		}	
	}	
}
