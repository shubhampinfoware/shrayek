<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Productmaster;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Votemaster;
use AdminBundle\Entity\Votecomments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/vote")
     * @Template()
     */
    public function indexAction() {
        // category list for slider
//        $category = $this->getCategoryList();
        // product list
        ;

        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        $commentslist = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Votecomments")->findBy(array("is_deleted" => 0));

        foreach ($commentslist as $commentslist) {
            $user_id = $commentslist->getUser_id();
            $User = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(array("is_deleted" => 0, "user_master_id" => $user_id));
        //    $username = $User->getUsername();
		
            if(!empty($User)){
				$username = $User->getUser_bio();
				$usermediaid = $User->getUser_image();
				$media = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Medialibrarymaster")->findOneBy(array("is_deleted" => 0, "media_library_master_id" => $usermediaid));
				$mediapath = '';
				if (!empty($media)) {
					$imagelocation = $media->getMedia_location();
					$medianame = $media->getMedia_name();
					$mediapath = $imagelocation . "/" . $medianame;
				}
				
				$total[] = array(
					"commentslist" => $commentslist,
					"username" => $username,
					"mediapath" => $mediapath,
				);
			}
        }


        if ($this->get('session')->get('language_id') == null) {
            $language_id = '1';
        } else {
            $language_id = $this->get('session')->get('language_id');
        }

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Productmaster');
        $products = $repository->findBy(
                array(
                    'product_status' => 'active',
                    'is_deleted' => 0,
                    'language_id' => $language_id
                ),array('product_master_id'=>'DESC')
        );
        $product_list = array();
        foreach ($products as $_product) {
            // get media url
            $_product->media_url = $this->getMediaUrl($_product->getProduct_image_id());
            // get Vote of Product
            $excellent = $bad = $medium = 0;
            $vote_excellent = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Votemaster')->findBy(array("is_deleted" => 0, "product_id" => $_product->getMain_product_id(), "vote_type" => 'excellent'));
            $vote_medium = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Votemaster')->findBy(array("is_deleted" => 0, "product_id" => $_product->getMain_product_id(), "vote_type" => 'medium'));
            $vote_bad = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Votemaster')->findBy(array("is_deleted" => 0, "product_id" => $_product->getMain_product_id(), "vote_type" => 'bad'));

            $tot_vote_count = count($vote_excellent) + count($vote_medium) + count($vote_bad);

            if (count($vote_excellent) > 0) {
               // $excellent = round((count($vote_excellent) / $tot_vote_count ) * 100);
                $excellent = round(( 100 /$tot_vote_count)) * count($vote_excellent);
            }
            if (count($vote_medium) > 0) {
               // $medium = round((count($vote_medium) / $tot_vote_count ) * 100);
                $medium = round(( 100 /$tot_vote_count)) * count($vote_medium);
            }
            if (count($vote_bad) > 0) {
                //$bad = round((count($vote_bad) / $tot_vote_count ) * 100);
                $bad = round(( 100 /$tot_vote_count)) * count($vote_bad);
            }
            if (!(( $bad + $medium + $excellent) == 100 )) {
                $rem = 100 - ($bad + $medium + $excellent);
                if ($excellent != 0)
                    $excellent = $excellent + $rem;
            }
           
            $product_list[] = array(
                "details" => $_product,
                "excellent" => $excellent,
                "medium" => $medium,
                "bad" => $bad,
                'total'=>$tot_vote_count,
                'count_excellent'=>count($vote_excellent),
                'count_medium'=>count($vote_medium),
                'count_bad'=>count($vote_bad),
            );
        }
       
        return array(
            'product_list' => $product_list, 'totallist' => $total
        );
    }

    /**
     * @Route("/voteproduct")
     * @Template()
     */
    public function voteproductAction() {
        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        if (isset($_REQUEST['product_id']) && !empty($_REQUEST['product_id'])) {
            $product_id = $_REQUEST['product_id'];
            $session = new Session();
            $user_id = $_REQUEST['user_id'];
            
            // check id insetred or not 
            $check_vote = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Votemaster")->findOneBy(array("is_deleted" => 0, "product_id" => $product_id, "user_id" => $user_id));
            $em = $this->getDoctrine()->getManager();
            if (empty($check_vote)) {
                $vote = new Votemaster();
                $vote->setProduct_id($product_id);
                $vote->setUser_id($user_id);
                $vote->setVote_type($_REQUEST['voteType']);
                $vote->setCreated_datetime(date("Y-m-d H:i:s"));
                $vote->setIs_deleted(0);
                $em->persist($vote);
                $em->flush();

                #loyaltyPoints changes
                if(!empty($vote->getVote_master_id())){
                                        
                    $activity_type = 'voting_product';
                    $points_added = $this->addCompetitionPointsToUser($user_id,$activity_type,0,$vote->getVote_master_id(),'vote_master');
                }
                #loyaltyPoints changes

                //$this->get('session')->getFlashBag()->set('success_msg', 'Your Vote saved successfully');
				if($this->get('session')->get('language_id') == 1){
					$this->get('session')->getFlashBag()->set('success_msg1', 'Your Vote saved successfully');
				}else if($this->get('session')->get('language_id') == 2){
					$this->get('session')->getFlashBag()->set('success_msg1', 'تم إرسال تصويتتك بنجاح');
				}
                //return $this->redirectToRoute('site_product_index');

            } else {
                //$this->get('session')->getFlashBag()->set('error_msg', 'You Vote this Product earlier');
				if($this->get('session')->get('language_id') == 1){
					$this->get('session')->getFlashBag()->set('error_msg1', 'You Vote this Product earlier');
				} else if($this->get('session')->get('language_id') == 2){
					$this->get('session')->getFlashBag()->set('error_msg1', 'لقد صوّت هذا المنتج في وقت سابق');
				}
                //return $this->redirectToRoute('site_product_index');
            }
            
             return new Response("success");
        }
    }

    public function getCategoryList() {
        $get_cat_sql = "select cat.category_name,cat.main_category_id,cat.language_id,media.media_location,media.media_name from category_master cat left join media_library_master media on media.media_library_master_id=cat.category_image_id where cat.category_status ='active' and cat.is_deleted=0 order by sort_order";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($get_cat_sql);
        $stmt->execute();
        $category = $stmt->fetchAll();

        return $category;
    }

    public function getMediaUrl($media_id) {
        $media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
        $media = $media_repository->find($media_id);

        $live_path = $this->container->getParameter('live_path');
        if (!empty($media)) {
            $media_url = $live_path . $media->getMedia_location() . '/' . $media->getMedia_name();
        } else {
            $media_url = $live_path . '/bundles/Resource/default.png';
        }

        return $media_url;
    }

    /**
     * @Route("/votesubmit")
     */
    public function votesubmitAction() {

        $user_id = $_POST['user_id'];
        $product_id = $_POST['product_id'];
        $comment = $_POST['comment'];

        if ($comment == "") {
            return new Response("enter your comment");
        }

        $check_vote = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Votecomments")->findOneBy(array("is_deleted" => 0, "product_id" => $product_id, "user_id" => $user_id));
        $em = $this->getDoctrine()->getManager();
        if (empty($check_vote)) {
            $vote = new Votecomments();
            $vote->setProduct_id($product_id);
            $vote->setUser_id($user_id);
            $vote->setComments($comment);
            $vote->setCreated_datetime(date("Y-m-d H:i:s"));
            $vote->setIs_deleted(0);
            $em->persist($vote);
            $em->flush();

            #loyaltyPoints changes
            if(!empty($vote->getVote_comment_id())){
                        
                $activity_type = 'voting_with_comment';
                $points_added = $this->addCompetitionPointsToUser($user_id,$activity_type,0,$vote->getVote_comment_id(),'vote_comments');
            }
            #loyaltyPoints changes

            //$this->get('session')->getFlashBag()->set('success_msg', 'Your comment saved successfully');
			if($this->get('session')->get('language_id') == 1) {
				$this->get('session')->getFlashBag()->set('success_msg1', 'Your comment saved successfully');
			} else if($this->get('session')->get('language_id') == 2) {
				$this->get('session')->getFlashBag()->set('success_msg1', 'تم إرسال رسالتك بنجاح');
			}
            
            return new Response("success");
        } else {
            //$this->get('session')->getFlashBag()->set('error_msg', 'You comment this Product earlier');
			if($this->get('session')->get('language_id') == 1) {
				$this->get('session')->getFlashBag()->set('error_msg1', 'You comment this Product earlier');
			} else if($this->get('session')->get('language_id') == 2) {
				$this->get('session')->getFlashBag()->set('error_msg1', 'قمت بتعليق هذا المنتج في وقت سابق');
			}

            return new Response("error");
        }
    }

    /**
     * @Route("/readcomment")
     */
    public function readcommentAction() {

        $product_id = $_POST['product_id'];
        $user_id = $_POST['user_id'];
        $check_vote = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Votecomments")->findOneBy(array("is_deleted" => 0, "product_id" => $product_id, "user_id" => $user_id));
        $comments = $check_vote->getComments();
        $User = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(array("is_deleted" => 0, "user_master_id" => $user_id));
    //   $username = $User->getUsername();
        $username = $User->getUser_bio();

        $usercomments = array(
            'comments' => $comments, 'username' => $username
        );
        return new JsonResponse($usercomments);
    }

}
