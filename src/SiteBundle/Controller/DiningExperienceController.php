<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Diningexperience;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Diningexperiencegallery;

class DiningExperienceController extends BaseController {

    private $PERPAGE;

    public function __construct() {
        $obj = new BaseController();
        $obj->checkUserStatus();
		$this->PERPAGE = 5;
    }

    /**
     * @Route("/DiningExperience",name="site_dinning_expr")
     * @Template()
     */
    public function indexAction() {
		
		if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        $restaraunts = array();
        $get_restaraunts_sql = "select 	restaurant_name,language_id,main_restaurant_id from restaurant_master where is_deleted=0 and language_id='" . $this->get('session')->get('language_id') . "'";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($get_restaraunts_sql);
        $stmt->execute();
        $restaraunts = $stmt->fetchAll();

        return array('restaraunts' => $restaraunts);
    }

    /**
     * @Route("/ViewDiningExperience",name="site_dinning_expr_view")
     * @Template()
     */
    public function viewDiningExprAction() {
        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        $language_id = $this->get('session')->get('language_id');
        $din_exr_gallery = array();
        $dinnig_expr = array();
        $dinning_view_data = array();

        $start = 0;
        $POSTS_PER_PAGE = 5;
        if (!isset($page)) {
            $page = 1;
        }
        //Figure out the first and last posts on this page
        //$first = ($page - 1) * $POSTS_PER_PAGE;
        //$last = $page * $POSTS_PER_PAGE - 1;

        $first = 0;
        $last = $this->PERPAGE;

        $din_exr_sql1 = "select din_expr.* from dining_experience din_expr where din_expr.is_deleted=0 and din_expr.status = 'active' order by create_date desc";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt1 = $con->prepare($din_exr_sql1);
        $stmt1->execute();
        $dinnig_expr1 = $stmt1->fetchAll();
       // $din_exr_sql = "select din_expr.* from dining_experience din_expr where din_expr.is_deleted=0 order by create_date desc limit $first,$last";
        //$din_exr_sql = "select dining_experience.* ,restaurant_master.restaurant_name from dining_experience JOIN restaurant_master ON dining_experience.restaurant_id = restaurant_master.main_restaurant_id where dining_experience.is_deleted=0  and restaurant_master.is_deleted=0 and restaurant_master.language_id = ".$language_id ." order by create_date desc limit ".$first.",".$last;
        $din_exr_sql = "select dining_experience.* from dining_experience where dining_experience.is_deleted=0 and dining_experience.status = 'active' order by create_date desc limit ".$first.",".$last;
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($din_exr_sql);
        $stmt->execute();
        $dinnig_expr = $stmt->fetchAll();

        if ($dinnig_expr) {
            $din_exr_gallery_sql = "select media.* ,d_e_g.dining_experience_id from media_library_master media join dining_experience_gallery d_e_g on d_e_g.image_id = media.media_library_master_id where d_e_g.is_deleted=0";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($din_exr_gallery_sql);
            $stmt->execute();
            $din_exr_gallery = $stmt->fetchAll();
        }
		
        return array('count' => count($dinnig_expr1), 'dinnig_expr' => $dinnig_expr, 'din_exr_gallery' => $din_exr_gallery);
    }

    /**
     * @Route("/paginateDiningExperience", name="site_dining_pagination")
     */
    public function paginateDiningExperienceAction() {
        $request = $this->getRequest();
        $page_no = $request->get('page_no');

        if (isset($page_no) && $page_no != '') {

            $start = ($page_no - 1) * $this->PERPAGE;
            $limit = $this->PERPAGE;

            $query = "select din_expr.* from dining_experience din_expr where din_expr.status = 'active' and din_expr.is_deleted=0 order by create_date desc limit {$start},{$limit}";

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $statement = $conn->prepare($query);
            $statement->execute();
            $dinnig_experience = $statement->fetchAll();

            $html = '';
            if (!empty($dinnig_experience)) {
                $live_path = $this->container->getParameter('live_path');
                foreach ($dinnig_experience as $_experience) {

                    $din_exr_gallery_sql = "select media.* ,d_e_g.dining_experience_id from media_library_master media join dining_experience_gallery d_e_g on d_e_g.image_id = media.media_library_master_id where d_e_g.dining_experience_id = {$_experience['dining_experience_id']} and d_e_g.is_deleted = 0";

                    $stmt = $conn->prepare($din_exr_gallery_sql);
                    $stmt->execute();
                    $din_exr_gallery = $stmt->fetchAll();

                    $html .= '<div class="panel panel-default">
								<div class="panel-body">
									<div class="media ask-community-qa-wrap">
										<div class="media-body">';

                    $html .= "<h4 class='media-heading'>{$_experience['nick_name']}</h4>";
                    $html .= "<h5>{$_experience['restaurant_id']}</h5>";
                    $html .= "<hr class='hr'>";
                    $html .= "<p>{$_experience['comments']}</p>";
                    $html .= "<hr class='hr'>";

                    if (!empty($din_exr_gallery)) {
                        $html .= "<ul class='list-inline-comment'>";
                        foreach ($din_exr_gallery as $_gallery) {
                            $html .= "<li>";
                            $html .= "<a href='{$live_path}{$_gallery['media_location']}/{$_gallery['media_name']}' data-fancybox='comment-images'>";
                            $html .= "<img src='{$live_path}{$_gallery['media_location']}/{$_gallery['media_name']}' class='img-thumbnail'>";
                            $html .= "</li>";
                        }
                        $html .= "</ul>";
                    }

                    $html .= '			</div>
									</div>		
								</div>
							  </div>';
                }
            }

            $data = array(
                'success' => 1,
                'html' => $html
            );
        } else {
            $data = array(
                'success' => 0,
                'html' => ''
            );
        }

        echo json_encode($data);
        exit;
    }

    /**
     * @Route("/save_din_expr",name="site_save_din_expr")
     * @Template()
     */
    public function saveDiningkAction(Request $req) {
        if ($this->get('session')->get('userid') != NULL) {
            if ($req->request->all()) {

                $Diningexperience_new = new Diningexperience();
                $Diningexperience_new->setNick_name($req->request->get('nick_name'));
                $Diningexperience_new->setEmail_address($req->request->get('email'));
                $Diningexperience_new->setRestaurant_id($req->request->get('restaraunt'));
                $Diningexperience_new->setBranch_id($req->request->get('branch'));
                $Diningexperience_new->setStatus('active');
                $Diningexperience_new->setComments($req->request->get('opinion'));
                $Diningexperience_new->setCreate_date(date('Y-m-d H:i:s'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($Diningexperience_new);
                $em->flush();

                $dinnig_expr_id = $Diningexperience_new->getDining_experience_id();

                #loyaltyPointsChanges
                $user_master = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(['email'=>$req->request->get('email'),'is_deleted'=>0]);
                if($user_master && !empty($dinnig_expr_id)){
                    $user_id = $user_master->getUser_master_id();
                
                    $activity_type = 'dining_new_experience';
                    $points_added = $this->addCompetitionPointsToUser($user_id,$activity_type,0,$dinnig_expr_id,'dining_experience');

                }
                #loyaltyPointsChanges

                $logo_id = 0;
                //	var_dump($_FILES);exit;
                if (!empty($_FILES['images'])) {
                    for ($i = 0; $i < count($_FILES['images']['name']); $i++) {
                        $media = $_FILES['images']['name'][$i];
                        $tmp_path = $_FILES['images']['tmp_name'][$i];
                        if (isset($media) && $media != '') {

                            $upload_dir = $this->container->getParameter('upload_dir1') . '/Resource/DiningExpr/';
                            $path = "/bundles/Resource/DiningExpr";
                            $mediatype = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository(Mediatype::class)
                                    ->findOneBy(array(
                                'media_type_name' => 'Image',
                                'is_deleted' => 0)
                            );
                            $allowedExts = explode(',', $mediatype->getMedia_type_allowed());
                            $temp = explode('.', $media);
                            $extension = end($temp);
                            if (in_array($extension, $allowedExts)) {
                                $media_id = $this->mediauploadAction($media, $tmp_path, $path, $upload_dir, $mediatype->getMedia_type_id());
                            }
                            if ($media_id) {
                                $logo_id = $media_id;
                                $Diningexperiencegallery_new = new Diningexperiencegallery();
                                $Diningexperiencegallery_new->setDining_experience_id($dinnig_expr_id);
                                $Diningexperiencegallery_new->setImage_id($media_id);
                                $em = $this->getDoctrine()->getManager();
                                $em->persist($Diningexperiencegallery_new);
                                $em->flush();

                                $flash_message1 = "Thank you for sharing your dining experience with us.";
                                if ($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2') {
                                    $flash_message1 = "شكرا لتقاسم تجربة الطعام معنا";
                                }
                                //$this->get('session')->getFlashBag()->set('success_msg', $flash_message1);
                                $this->get('session')->getFlashBag()->set('success_msg1', $flash_message1);
                            } else {
                                $logo_id = 0;
                                $flash_message1 = "Image not uploaded properly";
                                if ($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2') {
                                    $flash_message1 = "لم يتم تحميل الصورة بشكل صحيح";
                                }
                                //$this->get('session')->getFlashBag()->set('error_msg', $flash_message1);
                                $this->get('session')->getFlashBag()->set('error_msg1', $flash_message1);
                            }
                        }
                    }
                } else {
                    $flash_message1 = "Thank you for sharing your dining experience with us.";
                    if ($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2') {
                        $flash_message1 = "شكرا لتقاسم تجربة الطعام معنا";
                    }
                    //$this->get('session')->getFlashBag()->set('success_msg', $flash_message1);
                    $this->get('session')->getFlashBag()->set('success_msg1', $flash_message1);
                }
                return $this->redirectToRoute('site_dinning_expr');
            }
        } else {
            $flash_message = "Please Login first to Reply , Thank You";
            if ($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2') {
                $flash_message = "يرجى تسجيل الدخول أولا إلى الرد، شكرا لك";
            }
            //$this->get('session')->getFlashBag()->set('success_msg', $flash_message);
            $this->get('session')->getFlashBag()->set('success_msg1', $flash_message);
            return $this->redirectToRoute('site_default_index');
        }
    }

}
