<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Contactmaster;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Visitorcountermaster;
use AdminBundle\Entity\Worddictionary;
use AdminBundle\Entity\Savedtop10restaurant;
use AdminBundle\Entity\Savedcategorywiserestaurant;

class DefaultController extends BaseController {

    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction() {

        /* echo date('Y-m-d H:i:s a');
          exit; */

        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        $lang_id = $this->get('session')->get('language_id');

        //$query_advertise = "select adv.* from advertise_master adv where status = 'active' and is_deleted = 0  and website_mobile_type='website' and start_date < now() and end_date > now()";

        $now = date('Y-m-d H:i:s');

        /* $query_advertise = "select adv.* from advertise_master adv where status = 'active' and is_deleted = 0  and website_mobile_type='website' and show_in_website = '' and start_date < '{$now}' and end_date > '{$now}' and language_id=$lang_id"; */
        $query_advertise = "select adv.* from advertise_master adv where status = 'active' and is_deleted = 0  and website_mobile_type='website' and start_date < '{$now}' and end_date > '{$now}' and language_id=$lang_id";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $advstmt = $con->prepare($query_advertise);
        $advstmt->execute();
        $advertise1 = $advstmt->fetchAll();

        // also show banners added for mobile and show in website is true
        $query_advertise1 = "select adv.* from advertise_master adv where status = 'active' and is_deleted = 0  and website_mobile_type='mobile' and show_in_website != '' and start_date < '{$now}' and end_date > '{$now}' and language_id=$lang_id";

        $advstmt1 = $con->prepare($query_advertise1);
        $advstmt1->execute();
        $advertise2 = $advstmt1->fetchAll();

        $advertise = array_merge($advertise1, $advertise2);

        $mainheader = array();
        $rightcolumn = array();
        $thirdcolumn = array();
        if (!empty($advertise)) {
            foreach ($advertise as $_advertise) {

                $media = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster')->find($_advertise['advertise_image_id']);

                $media_url = '';
                if (!empty($media)) {
                    $live_path = $this->container->getParameter('live_path');
                    $media_url = $live_path . $media->getMedia_location() . '/' . $media->getMedia_name();
                }

                $_advertise['media_url'] = $media_url;
                if ($_advertise['advertise_type'] == 'mainheader' or $_advertise['show_in_website'] == 'mainheader') {
                    $mainheader[] = $_advertise;
                } else if ($_advertise['advertise_type'] == 'rightcolumn' or $_advertise['show_in_website'] == 'rightcolumn') {
                    $rightcolumn[] = $_advertise;
                } else if ($_advertise['advertise_type'] == 'thirdcolumn' or $_advertise['show_in_website'] == 'thirdcolumn') {
                    $thirdcolumn[] = $_advertise;
                }
            }
        }

        $all_banner = array(
            'mainheader' => $mainheader,
            'rightcolumn' => $rightcolumn,
            'thirdcolumn' => $thirdcolumn
        );

       
        $live_path = $this->container->getParameter('live_path');
        //----------------category wise Restaurant Evaluantion Lisitng ( top 5 )---------------------------
        if ($this->get('session')->get('language_id') != '') {
            $language_id = $this->get('session')->get('language_id');
        } else {
            $language_id = 1;
        }
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $live_path = $this->container->getParameter('live_path');
        $display_eve_category_array = NULL;
        //$category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 group by main_category_id";;
        $category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 And language_id = {$language_id} group by main_category_id";
        ;
        $category_listing = $this->firequery($category_listing_query);
        if (!empty($category_listing)) {

            foreach ($category_listing as $catekey => $catval) {
                $img_url = '';
                $evaluation_res_array = [];
                if ($catval['category_image_id'] != 0) {
                    $img_url = $this->getImage($catval['category_image_id'], $live_path);
                }
                // if there is Value in Table then Show From Table 
                $saved_category_restaurant_query  = "SELECT *  FROM `saved_categorywise_restaurant` WHERE is_deleted = 0 and `category_id` = ".$catval['main_category_id']." AND `update_datetime` between '".date("Y-m-d 00:00:00")."' and '".date("Y-m-d 23:59:59")."' order by update_datetime desc limit 0,5";
              //  echo $saved_category_restaurant_query ; exit;
                
                $saved_category_restaurant_list = $this->firequery($saved_category_restaurant_query);
                
                if(!empty($saved_category_restaurant_list) && count($saved_category_restaurant_list) == 5 ){
                    foreach($saved_category_restaurant_list as $savekey=>$saveval){
                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $saveval['restaurant_id'], "language_id" => $language_id));
                        $rest_name = '';
                        if (!empty($restaurant_info)) {
                            $rest_name = $restaurant_info->getRestaurant_name();
                        } else {
                            $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                    ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" =>$saveval['restaurant_id']));
                            ///  var_dump($restaurant_info);
                            //  var_dump(array("is_deleted"=>0,"main_restaurant_id"=>$resval['restaurant_id']));
                            $rest_name = $restaurant_info->getRestaurant_name();
                        }
                        $evaluation_res_array[] = array(
                                    "restaurant_id" => $saveval['restaurant_id'],
                                    "restaurant_name" => stripslashes($rest_name),
                                    "evaluation_cnt" => $saveval['evaluation_cnt'],
                                    "total_evaluation" => $saveval['total_evaluation'],
                                    "rating_point" => $saveval['rating_point'],
                                    "rating_percentage" => $saveval['rating_percentage']
                                );
                    }
                    $price = array();
                    if(!empty($evaluation_res_array)){
                        foreach ($evaluation_res_array as $key => $row) {
                            $price['rating_percentage'][$key] = $row['rating_percentage'];
                            $price['rating_point'][$key] = $row['rating_point'];
                        }

                        if (isset($price['rating_percentage']) && is_array($price['rating_percentage']) && isset($price['rating_point']) && is_array($price['rating_point'])) {
                            array_multisort($price['rating_percentage'], SORT_DESC, $price['rating_point'], SORT_DESC, $evaluation_res_array);
                        }
                    }
                     $display_eve_category_array[] = array(
                        "category_id" => $catval['main_category_id'],
                        "category_name" => $catval['category_name'],
                        "image" => $img_url,
                        "restaurant_list" => $evaluation_res_array
                    );
                }
                else{    
                    $evaluation_res_array = [] ;
                    
                    /*  $restaurant_detail_query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt ,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level)) as total_evaluation ,  evaluation_feedback.restaurant_id  FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_category_relation` WHERE `main_category_id` = ".$catval['main_category_id']." AND `restaurant_id` IN (SELECT restaurant_id FROM `evaluation_feedback` WHERE `status` = 'approved' AND `is_deleted` = 0) AND `is_deleted` = 0  
                      )  AND restaurant_master.is_deleted = 0 group by restaurant_id order by total_evaluation DESC limit 0,5"; */

                    $restaurant_detail_query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt ,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation ,  evaluation_feedback.restaurant_id  FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_foodtype_relation` WHERE `main_caetgory_id` = " . $catval['main_category_id'] . " AND `restaurant_id` IN (SELECT restaurant_id FROM `evaluation_feedback` WHERE `status` = 'approved' AND `is_deleted` = 0) AND `is_deleted` = 0 
    )  AND restaurant_master.is_deleted = 0  group by restaurant_id order by total_evaluation DESC limit 0,5";
                    $restaurant_detail_list = $this->firequery($restaurant_detail_query);
                    if (!empty($restaurant_detail_list)) {
                        foreach ($restaurant_detail_list as $reskey => $resval) {

                            // check res food_type relation
                            $check_res_foodtype_rel = $this->getDoctrine()->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy([
                                'restaurant_id' => $resval['restaurant_id'],
                                'main_caetgory_id' => $catval['main_category_id'],
                                'is_deleted' => 0
                            ]);

                            if (!empty($check_res_foodtype_rel)) {
                                $isRelated = false;
                                // check cat food_type relation
                                foreach ($check_res_foodtype_rel as $_catFoodtypeRel) {
                                    $check_cat_foodtype_rel = $this->getDoctrine()->getRepository('AdminBundle:Foodtypecategoryrelation')->findOneBy([
                                        'main_category_id' => $_catFoodtypeRel->getMain_caetgory_id(),
                                        'main_food_type_id' => $_catFoodtypeRel->getMain_foodtype_id(),
                                        'is_deleted' => 0
                                    ]);
                                    if (!empty($check_cat_foodtype_rel)) {
                                        $isRelated = true;
                                    }
                                }

                                if ($isRelated) {
                                    // if foodtype and categoty relation is active
                                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id'], "language_id" => $language_id));
                                    $rest_name = '';
                                    if (!empty($restaurant_info)) {
                                        $rest_name = $restaurant_info->getRestaurant_name();
                                    } else {
                                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                                ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id']));
                                        ///  var_dump($restaurant_info);
                                        //  var_dump(array("is_deleted"=>0,"main_restaurant_id"=>$resval['restaurant_id']));
                                        $rest_name = $restaurant_info->getRestaurant_name();
                                    }
                                    //----------------- get Points and rates-------------------
                                    $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $resval['restaurant_id'] . "' and status='approved' and is_deleted = 0) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $resval['restaurant_id'];
                                    $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                                    $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                                    $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                                    //$tot_ev_points = $tot_ev_points + $evpoints;                        
                                    $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
                                    $rating_point_percentage = 0;
                                    if ($rating_point > 0) {
                                        $rating_point_percentage = $rating_point * 20;
                                    }
                                    $evaluation_res_array[] = array(
                                        "restaurant_id" => $resval['restaurant_id'],
                                        "restaurant_name" => stripslashes($rest_name),
                                        "evaluation_cnt" => $resval['evaluation_cnt'],
                                        "total_evaluation" => $resval['total_evaluation'],
                                        "rating_point" => $rating_point,
                                        "rating_percentage" => $rating_point_percentage
                                    );
                                    // check in saved_categorywise_restaurant exist or not
                                   // $check_query = "SELECT *  FROM `saved_categorywise_restaurant` WHERE `category_id` = ".$catval['main_category_id']." AND `restaurant_id` = ". $resval['restaurant_id']." AND `update_datetime` = '".date("Y-m-d H:i:s")."' AND `is_deleted` = 0";
                                   // $check_list =$this->firequery($check_query);
                                   // if(empty($check_query)){
//                                        $saved_categorywise_restaurant = new Savedcategorywiserestaurant();
//                                        $saved_categorywise_restaurant->setCategory_id($catval['main_category_id']);
//                                        $saved_categorywise_restaurant->setRestaurant_id($resval['restaurant_id']);
//                                        $saved_categorywise_restaurant->setBranch_id(0);
//                                        $saved_categorywise_restaurant->setEvaluation_cnt($resval['evaluation_cnt']);
//                                        $saved_categorywise_restaurant->setTotal_evaluation($resval['total_evaluation']);
//                                        $saved_categorywise_restaurant->setRating_point($rating_point);
//                                        $saved_categorywise_restaurant->setRating_percentage($rating_point_percentage);
//                                        $saved_categorywise_restaurant->setUpdate_datetime(date("Y-m-d H:i:s"));
//                                        $saved_categorywise_restaurant->setIs_deleted(0);
//                                        $em->persist($saved_categorywise_restaurant);
//                                        $em->flush();
                                    //}
                                }
                            }
                        }
                        $price = array();
                        foreach ($evaluation_res_array as $key => $row) {
                            $price['rating_percentage'][$key] = $row['rating_percentage'];
                            $price['rating_point'][$key] = $row['rating_point'];
                        }

                        if (isset($price['rating_percentage']) && is_array($price['rating_percentage']) && isset($price['rating_point']) && is_array($price['rating_point'])) {
                            array_multisort($price['rating_percentage'], SORT_DESC, $price['rating_point'], SORT_DESC, $evaluation_res_array);
                        }
                    }
                    $display_eve_category_array[] = array(
                        "category_id" => $catval['main_category_id'],
                        "category_name" => $catval['category_name'],
                        "image" => $img_url,
                        "restaurant_list" => $evaluation_res_array
                    );
                
                }
            }
        }
//        echo "<pre>";
//        print_r($display_eve_category_array);
//        exit;
        
        // ---------Top 10 Restaurants Evaluation -----------------------------------------------------
        $restaurant_perc = array();
        $top_10_restaurant_array = array();
        $fetch_saved_top10_resturantQuery = " SELECT * FROM `saved_top10_restaurant` where is_deleted= 0 and `updated_datetime` between '".date("Y-m-d 00:00:00")."' and '".date("Y-m-d 23:59:59")."' order by updated_datetime desc limit 0,10";
        $fetch_saved_top10_resturantList = $this->firequery($fetch_saved_top10_resturantQuery);
        if(!empty($fetch_saved_top10_resturantList) && count($fetch_saved_top10_resturantList) == 10 ){
            foreach($fetch_saved_top10_resturantList as $topkey=>$topval){
                
                $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $topval['restaurant_id'], "language_id" => $language_id));
                $rest_name = '';
                if (!empty($restaurant_info)) {
                    $rest_name = $restaurant_info->getRestaurant_name();
                    $image_url = $this->getImage($restaurant_info->getLogo_id(), $live_path);
                } else {
                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $topval['restaurant_id']));
                    $rest_name = $restaurant_info->getRestaurant_name();
                    $image_url = $this->getImage($restaurant_info->getLogo_id(), $live_path);
                }
                $top_10_restaurant_array[] = array(
                        "restaurant_id" => $topval['restaurant_id'],
                        "restaurant_name" => stripslashes($rest_name),
                        "logo" => $image_url,
                        // "evaluation_points"=>$val['total_evaluation'],
                        "points" =>$topval['points'],
                        "rating" => $topval['rating'],
                        "rating_point_percentage" =>$topval['rating_point_percentage'],
    //                          
                    );
            }
            $price = array();
            if(!empty($top_10_restaurant_array)){
                foreach ($top_10_restaurant_array as $key => $row) {
                    $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                    $price['points'][$key] = $row['points'];
                }
                if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                    array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $top_10_restaurant_array);
                }
            }
        }
        else{
            //---Delete all top 10 
         
            $top_rest_sql = " SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation, evaluation_feedback.restaurant_id, restaurant_master.logo_id FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id =evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and restaurant_master.is_deleted = 0 group by restaurant_id order by total_evaluation DESC limit 0,10";

            $stmt = $con->prepare($top_rest_sql);
            $stmt->execute();
            $top_10_restaurant_list = $stmt->fetchAll();
            $stmt->closeCursor();

           
            if (!empty($top_10_restaurant_list)) {
                foreach ($top_10_restaurant_list as $key => $val) {
                    $evaluation_feedback = null;
                    if (!empty($user_id)) {
                        $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                ->findOneBy(array("restaurant_id" => $val['restaurant_id'], "user_id" => $user_id, "is_deleted" => 0));
                        $faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
                                ->findOneBy(array(
                            'shop_id' => $val['restaurant_id'],
                            'user_id' => $user_id,
                            'type' => 'add',
                            'is_deleted' => 0
                                )
                        );
                    }
    //                      
                    $total_rate = $val['evaluation_cnt'];
                    // $tot_ev_points = $tot_ev_points + $val['total_evaluation'] ;
                    $image_url = $this->getImage($val['logo_id'], $live_path);
                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $val['restaurant_id'], "language_id" => $language_id));
                    $rest_name = '';
                    if (!empty($restaurant_info)) {
                        $rest_name = $restaurant_info->getRestaurant_name();
                    } else {
                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $val['restaurant_id']));
                        $rest_name = $restaurant_info->getRestaurant_name();
                    }
                    //----------------- get Points and rates-------------------
                    $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $val['restaurant_id'] . "' and status='approved'  and is_deleted = 0) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level+ evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $val['restaurant_id'];
                    $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                    $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                    $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                    //  $tot_ev_points = $tot_ev_points + $evpoints;                        
                    $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
                    $rating_point_percentage = 0;
                    if ($rating_point > 0) {
                        $rating_point_percentage = $rating_point * 20;
                    }
                    //-----------------------------------------------------------
                    $rrating = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0 ;
                    $top_10_restaurant_array[] = array(
                        "restaurant_id" => $val['restaurant_id'],
                        "restaurant_name" => stripslashes($rest_name),
                        "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                        "is_bookmark" => !empty($faq_info) ? true : false,
                        "logo" => $image_url,
                        // "evaluation_points"=>$val['total_evaluation'],
                        "points" => $total_rate,
                        "rating" => $rrating,
                        "rating_point_percentage" => $rating_point_percentage,
    //                          
                    );
//                    $Savedtop10restaurant = new Savedtop10restaurant();
//                    $Savedtop10restaurant->setRestaurant_id($val['restaurant_id']);
//                    $Savedtop10restaurant->setPoints($total_rate);
//                    $Savedtop10restaurant->setRating($rrating);
//                    $Savedtop10restaurant->setRating_point_percentage($rating_point_percentage);
//                    $Savedtop10restaurant->setUpdated_datetime(date("Y-m-d H:i:s"));
//                    $em->persist($Savedtop10restaurant);
//                    $em->flush();
                }

                $price = array();
                foreach ($top_10_restaurant_array as $key => $row) {
                    $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                    $price['points'][$key] = $row['points'];
                }
                if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                    array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $top_10_restaurant_array);
                }

            }
        }
        /* $top_10_restaurant_array = [] ;
          if(!empty($restaurant_perc)){
          foreach($restaurant_perc  as $key=>$val){
          $image_url = $this->getImage($val['logo_id'], $live_path);
          $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
          ->findOneBy(array("is_deleted"=>0,"main_restaurant_id"=>$val['restaurant_id'],"language_id"=>$language_id));
          $rest_name = '';
          if(!empty($restaurant_info))
          {    $rest_name = $restaurant_info->getRestaurant_name(); }
          else{
          $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
          ->findOneBy(array("is_deleted"=>0,"main_restaurant_id"=>$val['restaurant_id']));
          ///  var_dump($restaurant_info);
          //  var_dump(array("is_deleted"=>0,"main_restaurant_id"=>$resval['restaurant_id']));
          $rest_name = $restaurant_info->getRestaurant_name();
          }
          $top_10_restaurant_array[] = array(
          "restaurant_id"=>$val['restaurant_id'],
          "restaurant_name"=>$rest_name,
          "logo"=>$image_url ,
          "points"=>$val['evaluation_cnt']
          );
          }
          } */

        // feature comments
        /* $review = "SELECT ef.*,um.user_firstname,um.user_lastname,um.username,um.user_bio,media_library_master.media_location , media_library_master.media_name from evaluation_feedback as ef 
          left join user_master as um on um.user_master_id=ef.user_id
          left join media_library_master ON um.user_image = media_library_master.media_library_master_id
          where ef.is_deleted=0 and ef.is_featured='yes' and ef.show_featured = 'active' group by ef.user_id order by ef.evaluation_feedback_id"; */

        $review = "SELECT ef.*,um.user_firstname,um.user_lastname,um.username,um.user_bio,um.user_image FROM evaluation_feedback ef, user_master um WHERE ef.user_id = um.user_master_id and `is_featured` = 'yes' AND `show_featured` = 'active' AND ef.is_deleted = 0 order by created_datetime desc";

        $stmt = $con->prepare($review);
        $stmt->execute();
        $feature_comment = $stmt->fetchAll();

        $feature_comments = [];

        if (!empty($feature_comment)) {
            foreach ($feature_comment as $key => $val) {
                /* if($val['media_location']!=='' && $val['media_name']!='')
                  $user_image=$live_path . $val['media_location'] . "/".$val['media_name'];
                  else
                  $user_image=$live_path.'/bundles/Resource/default.png'; */

                $feature_comments[] = array(
                    "evaluation_feedback_id" => $val['evaluation_feedback_id'],
                    "comments" => urldecode($val['comments']),
                    "userimage" => $this->getImage($val['user_image'], $live_path),
                    "username" => $val['user_bio']
                );
            }
        }

        /* echo '<pre>';
          print_r($feature_comments);
          exit; */

        //SITE MEMBERS HIGHLIGTED NUMBER OF EVALUATION
        $hilighted = "SELECT count(ef.user_id) as ecount,ef.*,um.user_firstname,um.username,um.user_lastname,um.user_bio,media_library_master.media_location , media_library_master.media_name from evaluation_feedback as ef 
		left join user_master as um on um.user_master_id=ef.user_id 
		left join media_library_master ON um.user_image = media_library_master.media_library_master_id 
		where ef.is_deleted=0 and ef.status = 'approved' group by ef.user_id order by count(ef.user_id) desc limit 0,10 ";

        $stmt = $con->prepare($hilighted);
        $stmt->execute();
        $hilighted_evalution = $stmt->fetchAll();
        $hilighted_evalutions = [];

        if (!empty($hilighted_evalution)) {
            foreach ($hilighted_evalution as $key => $val) {
                if ($val['media_location'] !== '' && $val['media_name'] != '')
                    $user_image = $live_path . $val['media_location'] . "/" . $val['media_name'];
                else
                    $user_image = $live_path . '/bundles/Resource/default.png';
                $hilighted_evalutions[] = array(
                    "evaluation_feedback_id" => $val['evaluation_feedback_id'],
                    "comments" => urldecode($val['comments']),
                    "userimage" => $user_image,
                    "username" => $val['user_bio'],
                    "max_counter" => $val['ecount']
                );
            }
        }

        //var_dump($display_eve_category_array);exit;
        return array(
            'all_banner' => $all_banner,
            'evaluation_category' => $display_eve_category_array,
            'top_10_restaurant' => $top_10_restaurant_array,
            'feature_comments' => $feature_comments,
            'hilighted_evalutions' => $hilighted_evalutions
        );
    }

     /**
     * @Route("/indexforcron")
     */
    public function indexforcronAction() {

        /* echo date('Y-m-d H:i:s a');
          exit; */

        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        $lang_id = $this->get('session')->get('language_id');

     

        /* echo"<pre>";
          print_r($all_banner);
          exit; */

        $live_path = $this->container->getParameter('live_path');
        ;
        //----------------category wise Restaurant Evaluantion Lisitng ( top 5 )---------------------------
        if ($this->get('session')->get('language_id') != '') {
            $language_id = $this->get('session')->get('language_id');
        } else {
            $language_id = 1;
        }
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $live_path = $this->container->getParameter('live_path');
        $display_eve_category_array = NULL;
        //$category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 group by main_category_id";;
        $category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 And language_id = {$language_id} group by main_category_id";
        ;
        $category_listing = $this->firequery($category_listing_query);
        if (!empty($category_listing)) {

            foreach ($category_listing as $catekey => $catval) {
                $img_url = '';
                $evaluation_res_array = [];
                if ($catval['category_image_id'] != 0) {
                    $img_url = $this->getImage($catval['category_image_id'], $live_path);
                }
                //Delete Savedcategorywiserestaurant all 
                $delete_saved_categorywiserest = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Savedcategorywiserestaurant")->findBy(array("category_id"=>$catval['main_category_id']));
                if($delete_saved_categorywiserest){
                    foreach($delete_saved_categorywiserest as $delkey=>$delval){
                        $delval->setIs_deleted(1);
                        $em->persist($delval);
                        $em->flush();
                    }
                }
                // if there is Value in Table then Show From Table 
                $saved_category_restaurant_query  = "SELECT *  FROM `saved_categorywise_restaurant` WHERE is_deleted = 0 and `category_id` = ".$catval['main_category_id']." AND `update_datetime` between '".date("Y-m-d 00:00:00")."' and '".date("Y-m-d 23:59:59")."' order by update_datetime desc limit 0,5";
                //echo $saved_category_restaurant_query ; exit;
                
                $saved_category_restaurant_list = $this->firequery($saved_category_restaurant_query);
                
                if(!empty($saved_category_restaurant_list) && count($saved_category_restaurant_list) == 5 && false ){
                    foreach($saved_category_restaurant_list as $savekey=>$saveval){
                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $saveval['restaurant_id'], "language_id" => $language_id));
                        $rest_name = '';
                        if (!empty($restaurant_info)) {
                            $rest_name = $restaurant_info->getRestaurant_name();
                        } else {
                            $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                    ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" =>$saveval['restaurant_id']));
                            ///  var_dump($restaurant_info);
                            //  var_dump(array("is_deleted"=>0,"main_restaurant_id"=>$resval['restaurant_id']));
                            $rest_name = $restaurant_info->getRestaurant_name();
                        }
                        $evaluation_res_array[] = array(
                                    "restaurant_id" => $saveval['restaurant_id'],
                                    "restaurant_name" => stripslashes($rest_name),
                                    "evaluation_cnt" => $saveval['evaluation_cnt'],
                                    "total_evaluation" => $saveval['total_evaluation'],
                                    "rating_point" => $saveval['rating_point'],
                                    "rating_percentage" => $saveval['rating_percentage']
                                );
                    }
                    $price = array();
                    if(!empty($evaluation_res_array)){
                        foreach ($evaluation_res_array as $key => $row) {
                            $price['rating_percentage'][$key] = $row['rating_percentage'];
                            $price['rating_point'][$key] = $row['rating_point'];
                        }

                        if (isset($price['rating_percentage']) && is_array($price['rating_percentage']) && isset($price['rating_point']) && is_array($price['rating_point'])) {
                            array_multisort($price['rating_percentage'], SORT_DESC, $price['rating_point'], SORT_DESC, $evaluation_res_array);
                        }
                    }
                     $display_eve_category_array[] = array(
                        "category_id" => $catval['main_category_id'],
                        "category_name" => $catval['category_name'],
                        "image" => $img_url,
                        "restaurant_list" => $evaluation_res_array
                    );
                }
                else{    
                    $evaluation_res_array = [] ;
                    //Delete Savedcategorywiserestaurant all 
                    $delete_saved_categorywiserest = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Savedcategorywiserestaurant")->findBy(array("category_id"=>$catval['main_category_id']));
                    if($delete_saved_categorywiserest){
                        foreach($delete_saved_categorywiserest as $delkey=>$delval){
                            $delval->setIs_deleted(1);
                            $em->persist($delval);
                            $em->flush();
                        }
                    }
               
                    /*  $restaurant_detail_query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt ,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level)) as total_evaluation ,  evaluation_feedback.restaurant_id  FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_category_relation` WHERE `main_category_id` = ".$catval['main_category_id']." AND `restaurant_id` IN (SELECT restaurant_id FROM `evaluation_feedback` WHERE `status` = 'approved' AND `is_deleted` = 0) AND `is_deleted` = 0  
                      )  AND restaurant_master.is_deleted = 0 group by restaurant_id order by total_evaluation DESC limit 0,5"; */

                    $restaurant_detail_query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt ,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation ,  evaluation_feedback.restaurant_id  FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_foodtype_relation` WHERE `main_caetgory_id` = " . $catval['main_category_id'] . " AND `restaurant_id` IN (SELECT restaurant_id FROM `evaluation_feedback` WHERE `status` = 'approved' AND `is_deleted` = 0) AND `is_deleted` = 0 
    )  AND restaurant_master.is_deleted = 0  group by restaurant_id order by total_evaluation DESC limit 0,5";
                    $restaurant_detail_list = $this->firequery($restaurant_detail_query);
                    if (!empty($restaurant_detail_list)) {
                        foreach ($restaurant_detail_list as $reskey => $resval) {

                            // check res food_type relation
                            $check_res_foodtype_rel = $this->getDoctrine()->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy([
                                'restaurant_id' => $resval['restaurant_id'],
                                'main_caetgory_id' => $catval['main_category_id'],
                                'is_deleted' => 0
                            ]);

                            if (!empty($check_res_foodtype_rel)) {
                                $isRelated = false;
                                // check cat food_type relation
                                foreach ($check_res_foodtype_rel as $_catFoodtypeRel) {
                                    $check_cat_foodtype_rel = $this->getDoctrine()->getRepository('AdminBundle:Foodtypecategoryrelation')->findOneBy([
                                        'main_category_id' => $_catFoodtypeRel->getMain_caetgory_id(),
                                        'main_food_type_id' => $_catFoodtypeRel->getMain_foodtype_id(),
                                        'is_deleted' => 0
                                    ]);
                                    if (!empty($check_cat_foodtype_rel)) {
                                        $isRelated = true;
                                    }
                                }

                                if ($isRelated) {
                                    // if foodtype and categoty relation is active
                                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id'], "language_id" => $language_id));
                                    $rest_name = '';
                                    if (!empty($restaurant_info)) {
                                        $rest_name = $restaurant_info->getRestaurant_name();
                                    } else {
                                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                                ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id']));
                                        ///  var_dump($restaurant_info);
                                        //  var_dump(array("is_deleted"=>0,"main_restaurant_id"=>$resval['restaurant_id']));
                                        $rest_name = $restaurant_info->getRestaurant_name();
                                    }
                                    //----------------- get Points and rates-------------------
                                    $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $resval['restaurant_id'] . "' and status='approved') as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $resval['restaurant_id'];
                                    $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                                    $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                                    $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                                    //$tot_ev_points = $tot_ev_points + $evpoints;                        
                                    $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
                                    $rating_point_percentage = 0;
                                    if ($rating_point > 0) {
                                        $rating_point_percentage = $rating_point * 20;
                                    }
                                    $evaluation_res_array[] = array(
                                        "restaurant_id" => $resval['restaurant_id'],
                                        "restaurant_name" => stripslashes($rest_name),
                                        "evaluation_cnt" => $resval['evaluation_cnt'],
                                        "total_evaluation" => $resval['total_evaluation'],
                                        // "rating_point" => $rating_point,
                                        "rating_point" => $total_rate,
                                        "rating_percentage" => $rating_point_percentage
                                    );
                                    // check in saved_categorywise_restaurant exist or not
                                   // $check_query = "SELECT *  FROM `saved_categorywise_restaurant` WHERE `category_id` = ".$catval['main_category_id']." AND `restaurant_id` = ". $resval['restaurant_id']." AND `update_datetime` = '".date("Y-m-d H:i:s")."' AND `is_deleted` = 0";
                                   // $check_list =$this->firequery($check_query);
                                   // if(empty($check_query)){
                                        $saved_categorywise_restaurant = new Savedcategorywiserestaurant();
                                        $saved_categorywise_restaurant->setCategory_id($catval['main_category_id']);
                                        $saved_categorywise_restaurant->setRestaurant_id($resval['restaurant_id']);
                                        $saved_categorywise_restaurant->setBranch_id(0);
                                        $saved_categorywise_restaurant->setEvaluation_cnt($resval['evaluation_cnt']);
                                        $saved_categorywise_restaurant->setTotal_evaluation($resval['total_evaluation']);
                                        // $saved_categorywise_restaurant->setRating_point($rating_point);
                                        $saved_categorywise_restaurant->setRating_point($total_rate);
                                        $saved_categorywise_restaurant->setRating_percentage($rating_point_percentage);
                                        $saved_categorywise_restaurant->setUpdate_datetime(date("Y-m-d H:i:s"));
                                        $saved_categorywise_restaurant->setIs_deleted(0);
                                        $em->persist($saved_categorywise_restaurant);
                                        $em->flush();
                                    //}
                                }
                            }
                        }
                        $price = array();
                        foreach ($evaluation_res_array as $key => $row) {
                            $price['rating_percentage'][$key] = $row['rating_percentage'];
                            $price['rating_point'][$key] = $row['rating_point'];
                        }

                        if (isset($price['rating_percentage']) && is_array($price['rating_percentage']) && isset($price['rating_point']) && is_array($price['rating_point'])) {
                            array_multisort($price['rating_percentage'], SORT_DESC, $price['rating_point'], SORT_DESC, $evaluation_res_array);
                        }
                    }
                    $display_eve_category_array[] = array(
                        "category_id" => $catval['main_category_id'],
                        "category_name" => $catval['category_name'],
                        "image" => $img_url,
                        "restaurant_list" => $evaluation_res_array
                    );
                
                }
            }
        }
//        echo "<pre>";
//        print_r($display_eve_category_array);
//        exit;
        
        // ---------Top 10 Restaurants Evaluation -----------------------------------------------------
        $restaurant_perc = array();
        $top_10_restaurant_array = array();
        $fetch_saved_top10_resturantQuery = " SELECT * FROM `saved_top10_restaurant` where is_deleted= 0 and `updated_datetime` between '".date("Y-m-d 00:00:00")."' and '".date("Y-m-d 23:59:59")."' order by updated_datetime desc limit 0,10";
        $fetch_saved_top10_resturantList = $this->firequery($fetch_saved_top10_resturantQuery);
        if(!empty($fetch_saved_top10_resturantList) && count($fetch_saved_top10_resturantList) == 10 && false ){
            foreach($fetch_saved_top10_resturantList as $topkey=>$topval){
                
                $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $topval['restaurant_id'], "language_id" => $language_id));
                $rest_name = '';
                if (!empty($restaurant_info)) {
                    $rest_name = $restaurant_info->getRestaurant_name();
                    $image_url = $this->getImage($restaurant_info->getLogo_id(), $live_path);
                } else {
                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $topval['restaurant_id']));
                    $rest_name = $restaurant_info->getRestaurant_name();
                    $image_url = $this->getImage($restaurant_info->getLogo_id(), $live_path);
                }
                $top_10_restaurant_array[] = array(
                        "restaurant_id" => $topval['restaurant_id'],
                        "restaurant_name" => stripslashes($rest_name),
                        "logo" => $image_url,
                        // "evaluation_points"=>$val['total_evaluation'],
                        "points" =>$topval['points'],
                        "rating" => $topval['rating'],
                        "rating_point_percentage" =>$topval['rating_point_percentage'],
    //                          
                    );
            }
            $price = array();
            if(!empty($top_10_restaurant_array)){
                foreach ($top_10_restaurant_array as $key => $row) {
                    $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                    $price['points'][$key] = $row['points'];
                }
                if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                    array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $top_10_restaurant_array);
                }
            }
        }
        else{
            //---Delete all top 10 
            $delete_top10rest = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Savedtop10restaurant")->findAll();
                    if($delete_top10rest){
                        foreach($delete_top10rest as $delkey=>$delval){
                            $delval->setIs_deleted(1);
                            $em->persist($delval);
                            $em->flush();
                        }
                    }
            $top_rest_sql = " SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation, evaluation_feedback.restaurant_id, restaurant_master.logo_id FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id =evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and restaurant_master.is_deleted = 0 group by restaurant_id order by total_evaluation DESC limit 0,10";

            $stmt = $con->prepare($top_rest_sql);
            $stmt->execute();
            $top_10_restaurant_list = $stmt->fetchAll();
            $stmt->closeCursor();

           
            if (!empty($top_10_restaurant_list)) {
                foreach ($top_10_restaurant_list as $key => $val) {
                    $evaluation_feedback = null;
                    if (!empty($user_id)) {
                        $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                ->findOneBy(array("restaurant_id" => $val['restaurant_id'], "user_id" => $user_id, "is_deleted" => 0));
                        $faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
                                ->findOneBy(array(
                            'shop_id' => $val['restaurant_id'],
                            'user_id' => $user_id,
                            'type' => 'add',
                            'is_deleted' => 0
                                )
                        );
                    }
    //                      
                    $total_rate = $val['evaluation_cnt'];
                    // $tot_ev_points = $tot_ev_points + $val['total_evaluation'] ;
                    $image_url = $this->getImage($val['logo_id'], $live_path);
                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $val['restaurant_id'], "language_id" => $language_id));
                    $rest_name = '';
                    if (!empty($restaurant_info)) {
                        $rest_name = $restaurant_info->getRestaurant_name();
                    } else {
                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $val['restaurant_id']));
                        $rest_name = $restaurant_info->getRestaurant_name();
                    }
                    //----------------- get Points and rates-------------------
                    $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $val['restaurant_id'] . "' and status='approved') as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level+ evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $val['restaurant_id'];
                    $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                    $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                    $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                    //  $tot_ev_points = $tot_ev_points + $evpoints;                        
                    $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
                    $rating_point_percentage = 0;
                    if ($rating_point > 0) {
                        $rating_point_percentage = $rating_point * 20;
                    }
                    //-----------------------------------------------------------
                    $rrating = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0 ;
                    $top_10_restaurant_array[] = array(
                        "restaurant_id" => $val['restaurant_id'],
                        "restaurant_name" => stripslashes($rest_name),
                        "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                        "is_bookmark" => !empty($faq_info) ? true : false,
                        "logo" => $image_url,
                        // "evaluation_points"=>$val['total_evaluation'],
                        "points" => $total_rate,
                        "rating" => $rrating,
                        "rating_point_percentage" => $rating_point_percentage,
    //                          
                    );
                    $Savedtop10restaurant = new Savedtop10restaurant();
                    $Savedtop10restaurant->setRestaurant_id($val['restaurant_id']);
                    $Savedtop10restaurant->setPoints($total_rate);
                    $Savedtop10restaurant->setRating($rrating);
                    $Savedtop10restaurant->setRating_point_percentage($rating_point_percentage);
                    $Savedtop10restaurant->setUpdated_datetime(date("Y-m-d H:i:s"));
                    $em->persist($Savedtop10restaurant);
                    $em->flush();
                }

                $price = array();
                foreach ($top_10_restaurant_array as $key => $row) {
                    $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                    $price['points'][$key] = $row['points'];
                }
                if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                    array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $top_10_restaurant_array);
                }

            }
        }
      

      
        /* echo '<pre>';
          print_r($feature_comments);
          exit; */

        //var_dump($display_eve_category_array);exit;
        return new Response(json_encode(array(
           'evaluation_category' => $display_eve_category_array,
            'top_10_restaurant' => $top_10_restaurant_array
            
        )));
    }

    /**
     * @Route("/getAppLink")
     */
    public function getAppLinkAction() {

        $general_setting = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Generalsetting')->findOneBy(array("is_deleted" => 0, "general_setting_key" => 'app_link'));

        $app_link = array();
        if (!empty($general_setting)) {
            $json_data = json_decode($general_setting->getGeneral_setting_value());
            if (!empty($json_data)) {
                $app_link['ios_link'] = $json_data->ios_link;
                $app_link['android_link'] = $json_data->android_link;
            }
        }
        return new JsonResponse($app_link);
    }

    /**
     * @Route("/getSocialLinks")
     */
    public function getSocialLinksAction() {

        $contact_us = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Contactus')->findAll();

        $social_links = array();
        if (!empty($contact_us)) {
            foreach ($contact_us as $_contact) {
                $social_links['fb_link'] = $_contact->getFb_link();
                $social_links['insta_link'] = $_contact->getInsta_link();
                $social_links['twitter_link'] = $_contact->getTwitter_link();
            }
        }
        return new JsonResponse($social_links);
    }

    public function getMemberSettingsAction() {
        $em = $this->getDoctrine()->getManager();
        $general_setting = $em->getRepository('AdminBundle:Generalsetting')->findOneBy([
            'general_setting_key' => 'visitor_member_setting',
            'is_deleted' => 0
        ]);

        $memeber_visitor_setting = array();
        if (!empty($general_setting)) {
            $value = $general_setting->getGeneral_setting_value();
            if ($value != '') {
                $web_setting = json_decode($value);
                if (!empty($web_setting)) {
                    $memeber_visitor_setting['show_visitor'] = $web_setting->show_visitor;
                    $memeber_visitor_setting['show_member'] = $web_setting->show_member;
                }
            }
        }

        return new JsonResponse($memeber_visitor_setting);
    }

    function countersAction() {

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        // get members counter
        $member_counter = "SELECT * from user_master where is_deleted=0 and user_role_id=3";

        $stmt = $con->prepare($member_counter);
        $stmt->execute();
        $member_counters = $stmt->fetchAll();

        // get visitors counter
        $ip = $_SERVER['REMOTE_ADDR'];
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $counter = $connection->prepare("SELECT * from visitor_counter_master where is_deleted=0 and ip_address='" . $ip . "'");
        $counter->execute();
        $ip_exist_check = $counter->fetchAll();
        $view_count = 0;
        if (empty($ip_exist_check)) {
            $em = $this->getDoctrine()->getManager();
            $Visitorcountermaster = new Visitorcountermaster();
            $Visitorcountermaster->setIp_address($ip);
            $Visitorcountermaster->setIs_deleted(0);

            $em->persist($Visitorcountermaster);
            $em->flush();
        }
        $visitor_counters = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Visitorcountermaster')
                ->findBy(array('is_deleted' => 0));
        $counters = array();
        $counters['member_counters'] = count($member_counters);
        $counters['visitor_counters'] = count($visitor_counters);

        return new JsonResponse($counters);
    }

    /**
     * @Route("/logincheck")
     */
    public function logincheckAction(Request $req) {
        if ($req->request->all()) {
            $email = $req->request->get('email');
            $password = md5($req->request->get('password'));

            $user_check = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(array('email' => $email, 'password' => $password, 'is_deleted' => 0, 'user_role_id' => 3));
            $live_path = $this->container->getParameter('live_path');

            if (empty($user_check)) {
                return new Response('error');
            } else {

                if ($user_check->getStatus() == 'inactive') {
                    return new Response('inactive');
                } else {
                    $userid = $user_check->getUser_master_id();
                    $username = $user_check->getUsername();
                    $nickname = $user_check->getUser_bio();
                    $email = $user_check->getEmail();
                    $contact_no = $user_check->getUser_mobile();
                    $current_lang_id = $user_check->getCurrent_lang_id();
                    $profile_image_id = $user_check->getUser_image();
                    $image_url = $live_path . '/bundles/Resource/default.png';
                    if ($profile_image_id != 0) {
                        $image_url = $this->getImage($profile_image_id, $live_path);
                    }
                    $this->get('session')->set('userid', $userid);
                    $this->get('session')->set('user_id', $userid);
                    $this->get('session')->set('username', $username);
                    $this->get('session')->set('nick_name', $nickname);
                    $this->get('session')->set('email', $email);
                    $this->get('session')->set('contact_no', $contact_no);
                    $this->get('session')->set('role_id', $user_check->getUser_role_id());
                    $this->get('session')->set('current_lang_id', $current_lang_id);
                    $this->get('session')->set('image_url', $image_url);
                    //$this->get('session')->getFlashBag()->set('success_msg', 'Login Successfully,Thank You.');
                    $label_id = 275;
                    $language_id = $this->get('session')->get('language_id');
                    $word = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id' => $label_id, 'language_id' => $language_id));
                    $text = $word->getWord_name();

                    $this->get('session')->getFlashBag()->set('success_msg1', $text);
                    return new Response('success');
                }
            }
        }
    }

    /**
     * @Route("/logoutcheck")
     */
    public function logoutcheckAction(Request $req) {
        $session = new Session;
        if (!empty($session->get('email'))) {
            $this->get('session')->remove('email');
            $this->get('session')->remove('userid');
            $this->get('session')->remove('username');
            $this->get('session')->remove('nick_name');
            $this->get('session')->remove('current_lang_id');
            $this->get('session')->remove('role_id');
            //$this->get('session')->getFlashBag()->set('success_msg', 'Logout Successfully, Thank You.');
            $label_id = 273;
            $language_id = $session->get('language_id');
            $word = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id' => $label_id, 'language_id' => $language_id));
            $text = $word->getWord_name();
            $this->get('session')->getFlashBag()->set('success_msg1', $text);
            return new Response('success');
        }
    }

    /**
     * @Route("/getCategories")
     * @Template()
     */
    public function getCategoryAction() {

        if ($this->get('session')->get('language_id') != '') {
            $language_id = $this->get('session')->get('language_id');
        } else {
            $language_id = 1;
        }
        $category = array();
        $get_cat_sql = "select cat.category_name,cat.main_category_id,cat.language_id,media.media_location,media.media_name from category_master cat left join media_library_master media on media.media_library_master_id=cat.category_image_id where cat.category_status ='active' and cat.is_deleted=0 and cat.language_id ='" . $language_id . "'order by sort_order";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($get_cat_sql);
        $stmt->execute();
        $category = $stmt->fetchAll();
        //	var_dump($category);exit;	
        return array('categories' => $category);
    }

    /**
     * @Route("/forgot-password")
     * @Template()
     */
    public function forgotpasswordAction(Request $request) {

        if ($request->request->all()) {
            $email = $_POST["forgetemail"];

            $chkuser = $this->getDoctrine()
                    ->getRepository("AdminBundle:Usermaster")
                    ->findOneBy(
                    array(
                        'is_deleted' => '0',
                        'email' => $email,
                        'status' => 'Active',
                    )
            );

            if (empty($chkuser)) {
                return new Response('error');
            }
            if (!empty($chkuser)) {
                $live_path = $this->container->getParameter('live_path');
                $to = $email;
                $link = md5($email) . time();
                $to = $email;
                $full_link = $live_path . 'password-Reset/' . $link . '/' . $email;

                $system_email = "help@shrayek.com";

                $subject = "Shreyak - Reset Password";
                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'From: ' . $system_email . "\r\n" .
                        'Reply-To: ' . $to . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();

                $file = '<!doctype html>
					<html>
					<head>
						<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
						<title>Shreyak</title>
					</head>
					<body bgcolor="#d4eef9" style="margin:0; padding:0;">
					<div style="background-color: #d4eef9;">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td>
								<table align="center" border="0" cellpadding="20" cellspacing="0" width="650">
									<tbody>
										<tr>
											<td bgcolor="#FFFFFF">
											<table border="0" cellpadding="0" cellspacing="0" width="100%">
												<tbody>
													<tr>
														<td></td>
													</tr>
													<tr>
														<td><h3>Shreyak</h3></td>
													</tr>
													<tr>
														<td style="border-bottom:1px solid #f2f2f2;"></td>
													</tr>
													<tr>
														<td></td>
													</tr>
													<tr>
														<td></td>
													</tr>
													<tr>
														<td>
														<p style="color:#1B75BB; padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:18px; line-height:normal;">Dear ' . $chkuser->getUser_firstname() . ' ' . $chkuser->getUser_lastname() . ',</p>
									</td>
								</tr>
								<tr>
									<td><br> We have received your reset password link</td>
								</tr>
								<tr>
									<td>
									<br><p style="color:#666; padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:normal;">Your reset password link is : <a href="' . $full_link . '" class="btn btn-success">Reset Password</a></p>
									</td>
								</tr>
								<tr>
									<td><br>Please click on reset link and reset it. Thanks</td>
								</tr>
								<tr>
									<td></td>
								</tr>
								<tr>
									<td align="center" style="border-top:1px solid #f2f2f2;"></td>
								</tr>
								<tr>
									<td>
									<br><p style="color:#666; padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:normal;"><strong>Thanking you,</strong><br />
									Shreyak Team</p>
									</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			<br />
			<br />
			&nbsp;</td>
		</tr>
	</tbody>
</table>
<br />
<br />
<br />
<br />
<br />
&nbsp;</div>
</body>
</html>
';
                $message = $file;

                if (@mail($to, $subject, $message, $headers)) {
                    $response = "Password sent to your registered Email ID. Please check spam folder if you did not find mail.";
                    //$this->get('session')->getFlashBag()->set('success_msg', 'Reset link sent to '.$to);
                    $this->get('session')->getFlashBag()->set('success_msg1', 'Reset link sent to ' . $to);
                    return new Response('success');
                } else {
                    $response = "Please enter valid Email ID.";
                    //$this->get('session')->getFlashBag()->set('error_msg', 'Please enter valid Email ID');
                    $this->get('session')->getFlashBag()->set('error_msg1', 'Please enter valid Email ID');
                    return new Response('error');
                }
            }
        }
        //return $this->redirect($this->generateUrl("site_default_index"));
    }

    /**
     * @Route("/password-Reset/{hash}/{email}",defaults={"hash"="","email"=""})
     * @Template()
     */
    public function passwordresetviewAction($hash, $email, Request $request) {
        if (isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {
            return array("done" => $_REQUEST['status']);
        }
        if (isset($hash) && !empty($hash) && isset($email) && !empty($email)) {
            $user_id = '';
            $hash = str_split($hash, 32);
            $time = $hash[1];
            $hashemail = $hash[0];
            if ((md5($email) == $hashemail) && (intval($time + (30 * 60)) > time())) {
                $user = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("AdminBundle:Usermaster")
                        ->findOneBy(array("email" => $email, "status" => "active", "is_deleted" => '0'));
                if (!empty($user)) {
                    $user_id = $user->getUser_master_id();
                    $msg = "1"; // user found
                } else {
                    $msg = "2"; // user not found
                }
            } else {
                $msg = "3"; // link expired
            }
            return array(
                'user_id' => $user_id,
                'email' => $email,
                'msg' => $msg
            );
        }
        $msg = 2;
        return array('msg' => $msg);
    }

    /**
     * @Route("/updatepassword/")
     * @Template()
     */
    public function updatepasswordAction(Request $request) {
        if ($request->request->all()) {

            $msg = "2";
            $user = $this->getDoctrine()
                    ->getManager()
                    ->getRepository("AdminBundle:Usermaster")
                    ->findOneBy(array("email" => $request->get('email'), 'status' => 'active', 'is_deleted' => 0));

            if (!empty($user)) {
                $em = $this->getDoctrine()->getManager();
                $update = $em->getRepository("AdminBundle:Usermaster")->find($user->getUser_master_id());
                $update->setPassword(md5($request->get('password')));
                $update->setShow_password($request->get('password'));
                $em->flush();
                $msg = "1";
            }

            return $this->redirect($this
                                    ->generateUrl("site_default_passwordresetview") . "?status=" . $msg);
        }
    }

    // change_language site 
    /**
     * @Route("/changelanguage/")
     * @Template()
     */
    public function changelanguage(Request $request) {
        if ($request->request->get('language_id')) {
            $this->get('session')->set('language_id', $request->request->get('language_id'));
            $lang_id = $this->get('session')->get('language_id');
            return new Response($lang_id);
        }
    }

    // langugae wise static data 
    /**
     * @Route("/static_data/{language_id}/{label_id}",defaults={"label_id"="0","label_id"="0"})
     * @Template()
     */
    public function staticdataAction($language_id, $label_id) {
        $word = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Worddictionary")->findOneBy(array('main_word_dictionary_id' => $label_id, 'language_id' => $language_id));
        if ($word) {
            return new Response($word->getWord_name());
        } else {
            return new Response('');
        }
    }

    function msort($array, $key, $sort_flags = SORT_REGULAR) {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                asort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }

}
