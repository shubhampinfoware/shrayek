<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Siteprivacy;

class LoyaltyPointsController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/userLoyaltyPoints/{login_user_id}", defaults={"login_user_id":""})
     * @Template()
     */
    public function indexAction($login_user_id) {
		
        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }

		$language_id = $this->get('session')->get('language_id');
		
		$con = $this->getDoctrine()->getManager()->getConnection();
	
		$live_path = $this->container->getParameter('live_path');
		
		$sql_activitys_users = "select *,competition_master.name as competition_name,competition_user_relation.created_datetime as created_on from competition_user_relation 
							JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
							JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
							LEFT JOIN restaurant_master ON restaurant_master.main_restaurant_id = competition_user_relation.restaurant_id 
							where competition_user_relation.user_id = '$login_user_id' 
							and competition_user_relation.is_deleted= 0 and restaurant_master.language_id = '$language_id' group by competition_user_relation_id order by competition_user_relation.created_datetime DESC";
							
		$stmt = $con->prepare($sql_activitys_users);
		$stmt->execute();	
		$pointsDetails = $stmt->fetchAll();

		$response = null;
		
		if(!empty($pointsDetails)){
			
			foreach($pointsDetails as $_pointsDetails){
					
				$restaurant_details = null ;
				
				if(!empty($_pointsDetails['restaurant_name'])){
					$restaurant_details = array(
						"restaurant_name" => $_pointsDetails['restaurant_name'],
						"restaurant_logo" => $this->getimage($_pointsDetails['logo_id'],$live_path)
					);					
				}	

				
				$response [] = array(
					'points' => $_pointsDetails['points'],
					'competition_name' => $_pointsDetails['competition_name'],
					'activity_title' => $_pointsDetails['activity_title'],
					'created_on' => strtotime($_pointsDetails['created_on']),
					'start_date' => strtotime($_pointsDetails['start_date']),
					'end_date' => strtotime($_pointsDetails['end_date']),
					'rule_book' => $this->getimage($_pointsDetails['rule_book'],$live_path),
					'restaurant_details' => $restaurant_details
				);
			}
			$this->error = "SFD";
		}
				
		echo"<pre>";print_r($response);exit;
        return array(
            'response' => $response
        );
    }

}
