<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Commonquestion;



class CompetitionAndAwardController extends BaseController {
	
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
    /**
     * @Route("/CompetitionAndAward",name="site_competetion_award")
     * @Template()
     */
    public function indexAction()
    {
		if($this->get('session')->get('language_id') != ''){
            $language_id=$this->get('session')->get('language_id');
        }else{
            $language_id=1;
        }
		
	   $competition_award = array();	
	   $get_competition_award_sql = "select comp_award.*,media.* from competition_award comp_award 
	   								left join media_library_master media on comp_award.competition_award_cover_image_id = media.media_library_master_id
	   								where comp_award.status='active' and comp_award.is_deleted=0 and comp_award.language_id='".$language_id."'";		
       $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_competition_award_sql);
	   $stmt->execute();
	   $competition_award = $stmt->fetchAll();
	 //  var_dump($competition_award);exit;
	 	
		return array('competition_award'=>$competition_award);	
	}

    /**
     * @Route("/ViewCompetition/{comp_main_id}",name="site_competetion_award_view",defaults={"comp_main_id"="0"})
     * @Template()
     */
    public function showCompetitionAction($comp_main_id)
    {
		if($this->get('session')->get('language_id') != ''){
            $language_id=$this->get('session')->get('language_id');
        }else{
            $language_id=1;
        }
		
/*	   $category = array();	
	   $get_cat_sql = "select cat.category_name,cat.main_category_id,cat.language_id,media.media_location,media.media_name from category_master cat left join media_library_master media on media.media_library_master_id=cat.category_image_id where cat.category_status ='active' and cat.is_deleted=0 order by sort_order";		
       $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_cat_sql);
	   $stmt->execute();
	   $category = $stmt->fetchAll();
*/
	   $competition_award_details = array();	
	   $get_competition_award_details_sql = "select comp_award.competition_award_title, comp_award.description,comp_award.main_competition_award_id, comp_award.competition_award_cover_image_id,comp_award.language_id,comp_award.start_date,comp_award.end_date,comp_award.competition_award_date as created_date from competition_award comp_award where comp_award.status='active' and comp_award.language_id='".$language_id."' and comp_award.is_deleted=0 and comp_award.main_competition_award_id='".$comp_main_id."'";		
       $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_competition_award_details_sql);
	   $stmt->execute();
	   $competition_award_details = $stmt->fetchAll();

		$competition_award_list = array();
	   if(!empty($competition_award_details)){
		   $live_path = $this->container->getParameter('live_path');
		   foreach($competition_award_details as $_competition){
			   $_competition['media_url'] = $this->getImage($_competition['competition_award_cover_image_id'], $live_path);
			   $competition_award_list[] = $_competition;
		   }
	   }
	   
	   $competition_award_gallery = array();	
	   $get_competition_award_gallery_sql = "select media.*,comp_gallery.main_competition_award_id 
	   										 from competition_award_gallery comp_gallery 
	   										 left join media_library_master media on comp_gallery.image_id=media.media_library_master_id
	   										 where comp_gallery.main_competition_award_id='".$comp_main_id."' and comp_gallery.is_deleted=0";		
       $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_competition_award_gallery_sql);
	   $stmt->execute();
	   $competition_award_gallery = $stmt->fetchAll();	   
		
		
		/* echo '<pre>';
		print_r($competition_award_list);
		exit; */
		
	 //  var_dump($competition_award_gallery);exit;
		return array('competition_award'=>$competition_award_list,'competition_award_gallery'=>$competition_award_gallery);	
	}

}
