<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Siteprivacy;



class SearchController extends BaseController {
	
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
    /**
     * @Route("/search")
     * @Template()
     */
    public function indexAction()
    {

		if($this->get('session')->get('language_id') == null){
			$language_id = '1';
		}else{
			$language_id = $this->get('session')->get('language_id');
		}
		$restaurant_list = array();
		$search_category = array();
		$keyword='';
		if(isset($_REQUEST['keyword']) && !empty($_REQUEST['keyword']))
		{
			$keyword= str_replace("'","\'",$_REQUEST['keyword']);
			$keyword_equals = str_replace("'","\\\\\'",$_REQUEST['keyword']);
		
	   
	 //search restaurant
	   //$get_rest_sql = "select rest.* from  restaurant_master as rest where (rest.status='active' or rest.status='open_soon') and rest.is_deleted=0 and rest.language_id='".$language_id."' and  rest.restaurant_name LIKE '%$keyword%' group by rest.main_restaurant_id order by rest.restaurant_name ";
		$get_rest_sql = "select rest.* from  restaurant_master as rest where (rest.status='active') and rest.is_deleted=0 and rest.language_id='".$language_id."' 
						 and  (rest.restaurant_name LIKE '%$keyword%' OR rest.restaurant_name = '$keyword_equals' ) group by rest.main_restaurant_id order by rest.restaurant_name ";
		$em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_rest_sql);
	   $stmt->execute();
	   $restaraunts = $stmt->fetchAll();
	  

	   $firstLetter = '-1';
	   $letter_wise_rest = array();
	   foreach($restaraunts as $rest_data){
	  
			$restaurant_name = $rest_data['restaurant_name'];
			$pos = strpos($restaurant_name, "'");
			if($pos != ''){
				$restaurant_name = stripslashes($restaurant_name);
				$rest_data['restaurant_name'] = $restaurant_name;
			}

	   		$rest_data['media_url'] = $this->getMediaUrl($rest_data['logo_id']);
			
			$gallery_repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantgallery');
			$restaurant_gallery = $gallery_repository->findBy(
				array(
					'restaurant_id' => $rest_data['restaurant_master_id'],
					'is_deleted' => 0
				)
			);
			
			$gallery = array();
			if(!empty($restaurant_gallery)){
				foreach($restaurant_gallery as $_gallery){
					$gallery[] = $this->getMediaUrl($_gallery->getImage_id());
				}
			}
			
// evaluation gallery............

            $queryeval = "SELECT * FROM evaluation_feedback_gallery INNER JOIN evaluation_feedback ON evaluation_feedback.evaluation_feedback_id=evaluation_feedback_gallery.evaluation_feedback_id INNER JOIN restaurant_master ON restaurant_master.restaurant_master_id=evaluation_feedback.restaurant_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.restaurant_id='".$rest_data['restaurant_master_id']."'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($queryeval);
            $stmt->execute();
            $evaluation_gallery = $stmt->fetchAll();
		
			if(!empty($evaluation_gallery)){
				foreach($evaluation_gallery as $_gallery){
					$gallery[] = $this->getMediaUrl($_gallery['media_id']);
				}
			}
// evaluation gallery ends.............	
			
			$rest_data['gallery'] = $gallery;
			$restaurant_list[] = $rest_data;
	   }
	   
	   //search category
	   $get_cat_sql = "select cat.category_name,cat.main_category_id,cat.language_id,media.media_location,media.media_name from category_master cat left join media_library_master media on media.media_library_master_id=cat.category_image_id where cat.category_status ='active' and cat.category_name LIKE '%$keyword%' and cat.is_deleted=0 and cat.language_id='".$language_id."' order by sort_order";		
       $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_cat_sql);
	   $stmt->execute();
	   $search_category = $stmt->fetchAll();

		}
		
		$live_path = $this->container->getParameter('live_path');

/*		echo "<pre>";
		print_r($restaurant_list);exit;
*/		
		return array("restaurant_list"=>$restaurant_list,"search_category"=>$search_category,"live_path"=>$live_path,'keyword'=>$keyword); 
		
    }
	
	
	
	public function getMediaUrl($media_id){
		$media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
		$media = $media_repository->find($media_id);
		
		$live_path = $this->container->getParameter('live_path');
		if(!empty($media)){
			$media_url = $live_path.$media->getMedia_location().'/'.$media->getMedia_name();
		} else {
			$media_url = $live_path.'/bundles/Resource/default.png';
		}
		
		return $media_url;
	}

	/**
    * @Route("/ajax-res-list")
    * @Template()
    */
    public function ajaxResListAction(Request $request)
    {
		$DEFAULT_LIMIT = 100;
		$language_id = $this->get('session')->get('language_id');
		$keyword = $request->get('keyword');
		
		$html = '';
		$success = false;
		if(isset($keyword)){
			$get_rest_sql = "select rest.* from  restaurant_master as rest where (rest.status='active' or rest.status='open_soon') and rest.is_deleted=0 and rest.language_id= {$language_id} and rest.status = 'active' and rest.restaurant_name LIKE '{$keyword}%' group by rest.main_restaurant_id order by rest.restaurant_name ";
			
			$em = $this->getDoctrine()->getManager(); 
			$con = $em->getConnection();
			$stmt = $con->prepare($get_rest_sql);
			$stmt->execute();
			$restaraunts = $stmt->fetchAll();
			
			$html = '';
			$starter = 1;
			if(!empty($restaraunts)){
				foreach($restaraunts as $_res){
					//if($starter < $DEFAULT_LIMIT){
						$restaurant_name = $_res['restaurant_name'];
						$restaurant_name = stripcslashes($restaurant_name);
						$html .= "<option data-value='{$_res['main_restaurant_id']}'>{$restaurant_name}</option>";
					//}
					$starter++;
				}
			}
			$success = true;
		}
		
		if($html == ''){
			if($language_id == 2){
				$html = '<option>لم يتم العثور على مطعم</option>';
			} else {
				$html = '<option>No Restaurant Found</option>';
			} 
		}
		
		$data = array(
			'success' => $success,
			'html' => $html
		);
		
		return new Response(json_encode($data));
	}
}
