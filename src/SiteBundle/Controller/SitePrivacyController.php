<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Siteprivacy;
use AdminBundle\Entity\Evaluationfeedback;
use AdminBundle\Entity\Evaluationfeedbackgallery;
use AdminBundle\Entity\Medialibrarymaster;



class SitePrivacyController extends BaseController {
	
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
    /**
     * @Route("/sitePrivacy",name="web_site_privacy")
     * @Template()
     */
    public function indexAction()
    {
		if($this->get('session')->get('language_id') != ''){
            $language_id=$this->get('session')->get('language_id');
        }else{
            $language_id=1;
        }
		
		$site_privacy = array();	
		$site_privacy = $this->getDoctrine()->getManager()->getRepository(Siteprivacy :: class)->findBy(array('is_deleted'=>0,'language_id'=>$language_id));
		
		return array('site_privacy'=>$site_privacy);	
	}

    /**
     * @Route("/editEvalComment",name="site_edit_comment")
     * @Template()
     */
    public function editCommentAction(Request $req)
    {
		$eval_id = $req->request->get('eval_id');
		$comment_new = $req->request->get('comment_new');
		
		$evaluation = array();
		$em = $this->getDoctrine()->getManager();	
		$evaluation = $em->getRepository(Evaluationfeedback :: class)->findOneBy(array('is_deleted'=>0,'evaluation_feedback_id'=>$eval_id));
		if($evaluation){
			$evaluation->setComments($comment_new);
			$em->flush();
			$this->get('session')->getFlashBag()->set('success_msg1', 'Comment Edited');			
			return new Response('edited');
		}else{
			return new Response('error');		
		}
	}

    /**
     * @Route("/deleteEvalImg",name="site_delete_eval_img")
     * @Template()
     */
    public function deleteEvalImageAction(Request $req)
    {
		$img_id = $req->request->get('img_id');
		
		$evaluation = array();
		$em = $this->getDoctrine()->getManager();	
		$evaluation_img = $em->getRepository(Evaluationfeedbackgallery :: class)->findOneBy(array('is_deleted'=>0,'evaluation_feedback_gallery_id'=>$img_id));
		if($evaluation_img){
			$evaluation_img->setIs_deleted(1);
			$em->flush();
			$this->get('session')->getFlashBag()->set('success_msg1', 'Image Deleted');			
			return new Response('deleted');
		}else{
			return new Response('error');		
		}
	}

    /**
     * @Route("/AddEvalImg",name="site_add_eval_img")
     * @Template()
     */
    public function addEvalImageAction(Request $req)
    {
		$eval_id = $req->request->get('eval_id');

		$Config_live_site = $this->container->getParameter('live_path');
		$file_path = $this->container->getParameter('file_path');
		if (!empty($_FILES['image'])) {
			foreach ($_FILES['image']['name'] as $key => $val) {
				$filename = $_FILES['image']['name'][$key];
				$tmpname = $_FILES['image']['tmp_name'][$key];
				//file path to uploads folder (/bundles/design/uploads)
				$file_path = $this->container->getParameter('file_path');
				//$path = $file_path . '/uploads/WSimages/';
				// $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';
				$upload_dir = $this->container->getParameter('upload_dir1');
				$location = '/Resource/Restaurant-Evaluate';
				$upload_dir = $upload_dir . $location;

				if (!is_dir($upload_dir)) {
					mkdir($upload_dir);
				}

				$clean_image = preg_replace('/\s+/', '', $filename);
				$media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

				$is_uploaded = move_uploaded_file($_FILES['image']['tmp_name'][$key], $upload_dir . '/' . $media_name);

				if ($is_uploaded) {
					$em = $this->getDoctrine()->getManager();

					$mediamaster = new Medialibrarymaster();
					$mediamaster->setMedia_type_id(1);
					$mediamaster->setMedia_title($media_name);
					$mediamaster->setMedia_location("/bundles/{$location}");
					$mediamaster->setMedia_name($media_name);
					$mediamaster->setCreated_on(date('Y-m-d h:i:s'));
					$mediamaster->setIs_deleted(0);

					$em->persist($mediamaster);
					$em->flush();

					$media_id12 = $media_id = $mediamaster->getMedia_library_master_id();
				}

				// $media_id12 = $media_id = $this->mediaupload($_FILES['image']['tmp_name'][$key], $upload_dir, $location, 1);
				if (!empty($media_id12)) {

					$evaluation_feedback_gallery = new Evaluationfeedbackgallery();
					$evaluation_feedback_gallery->setEvaluation_feedback_id($eval_id);
					$evaluation_feedback_gallery->setMedia_id($media_id12);
					$evaluation_feedback_gallery->setMedia_type_id(1);
					$evaluation_feedback_gallery->setIs_deleted(0);
					$em->persist($evaluation_feedback_gallery);
					$em->flush();
				}
			}
		}
		$this->get('session')->getFlashBag()->set('success_msg1', 'Evaluated Image Added successfully');	
		return $this->redirectToRoute('site_restaurant_viewmyresevaluation');	
	}	
}
