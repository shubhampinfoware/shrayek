<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Contactus;
use AdminBundle\Entity\Contactusfeedback;

class ContactusController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/contactus")
     * @Template()
     */
    public function indexAction() {
	
		if($this->get('session')->get('language_id') != ''){
            $language_id=$this->get('session')->get('language_id');
        }else{
            $language_id=1;
        }	

        $contact_us = array();
        $contact_us = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Contactus')->findBy(array('language_id'=>$language_id));

        return array('contact_us' => $contact_us);
    }

    /**
     * @Route("/save_feedback",name="site_save_feedback")
     * @Template()
     */
    public function saveFeedbackAction(Request $req) {
		$flash_message = '';
		if($this->get('session')->get('userid') != NULL || 1==1){
			if(array_key_exists('g-recaptcha-response', $_REQUEST)){
				if ($req->request->all()) {
					$feedback = new Contactusfeedback();
					$feedback->setName($req->request->get('name'));
					$feedback->setEmail_address($req->request->get('email'));
					$feedback->setPhone_number($req->request->get('phone_number'));
					$feedback->setMessage($req->request->get('message'));
					$feedback->setCreated_date(date('Y-m-d H:i:s'));
					$em = $this->getDoctrine()->getManager();
					$em->persist($feedback);
					$em->flush();

					$flash_message1 = "Your Feedback saved successfully";
					if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2'){
						$flash_message1 = "تم حفظ تعليقاتك بنجاح";
					}		     						
					//$this->get('session')->getFlashBag()->set('success_msg',$flash_message1);
					$this->get('session')->getFlashBag()->set('success_msg1',$flash_message1);
					return $this->redirectToRoute('site_contactus_index');
				}
			} else {
				$flash_message = "Invalid Captcha";
				if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
				{
					$flash_message = "كلمة التحقق غير صالحة";
				}
			}
		}else{
			$flash_message = "Please Login first to Reply , Thank You";
			if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
			{
				$flash_message = "يرجى تسجيل الدخول أولا إلى الرد، شكرا لك";
			}
			//$this->get('session')->getFlashBag()->set('success_msg',$flash_message);
			$this->get('session')->getFlashBag()->set('success_msg1',$flash_message);
			return $this->redirectToRoute('site_default_index');
		}
		
		$this->get('session')->getFlashBag()->set('success_msg1',$flash_message);
		return $this->redirectToRoute('site_default_index');
    }
}