<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Mediatype;



class UserLoginController extends BaseController {
    /**
     * @Route("/userRegister",name="site_user_register")
     * @Template()
     */
    public function indexAction(Request $req)
    {
		if(!empty($_POST)){
			$nick_name = $req->request->get('name');
			$first_name = $req->request->get('full_name');
			$last_name = $req->request->get('last_name');
			$birthday_date = $req->request->get('birthday_day');
			$birthday_month = $req->request->get('birthday_month');
			$birthday_year = $req->request->get('birthday_year');
			$email = $req->request->get('email');
			$phone = $req->request->get('phone_number');
			$password = $req->request->get('password');
			$gender = $req->request->get('gender');

			$date_of_birth = date('Y-m-d',strtotime($birthday_date.'-'.$birthday_month.'-'.$birthday_year)); 	
			if(strtotime($date_of_birth) > strtotime('now')){
				return new Response('invalidDate');
			}else{
                $user_check_name = $this->getDoctrine()->getManager()->getRepository(Usermaster :: class)->findOneBy(array('user_bio'=>$nick_name,'is_deleted'=>0,'user_role_id'=>3));
				$user_check = $this->getDoctrine()->getManager()->getRepository(Usermaster :: class)->findOneBy(array('email'=>$email,'is_deleted'=>0,'user_role_id'=>3));
				if(!empty($user_check)){
					return new Response('emailRegisterd');
				}elseif(!empty($user_check_name)){
					return new Response('usernameRegistered');				
				}
				else{
					$user_image = 0;
					if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic'])){
						$media = $_FILES['profile_pic']['name'];
                        $tmp_path = $_FILES['profile_pic']['tmp_name'];
                        if (isset($media) && $media != '') {

                            $upload_dir = $this->container->getParameter('upload_dir1') . '/Resource/profile_pic/';
                            $path = "/bundles/Resource/profile_pic";
                            $mediatype = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository(Mediatype::class)
                                    ->findOneBy(array(
                                'media_type_name' => 'Image',
                                'is_deleted' => 0)
                            );
                            $allowedExts = explode(',', $mediatype->getMedia_type_allowed());
                            $temp = explode('.', $media);
                            $extension = end($temp);
                            if (in_array($extension, $allowedExts)) {
                                $media_id = $this->mediauploadAction($media, $tmp_path, $path, $upload_dir, $mediatype->getMedia_type_id());
                            }
                            if ($media_id) {
								$user_image = $media_id;
                            } else {
                                $user_image = 0;
                                $flash_message1 = "Image not uploaded properly";
                                if ($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2') {
                                    $flash_message1 = "لم يتم تحميل الصورة بشكل صحيح";
                                }
								return new Response($flash_message1);				
                            }
                        }						
					}
					$new_user = new Usermaster();
					$new_user->setUser_role_id(3);
					$new_user->setUser_bio($nick_name);
					$new_user->setUsername($email);
					$new_user->setPassword(md5($password));
					$new_user->setShow_password($password);
					$new_user->setUser_firstname($first_name);
					$new_user->setUser_lastname($last_name);
					$new_user->setUser_mobile($phone);
					$new_user->setEmail($email);
					$new_user->setUser_image($user_image);
					$new_user->setAddress_master_id(0);
					$new_user->setParent_user_id(0);
					$new_user->setUser_gender($gender);
					$new_user->setDate_of_birth($date_of_birth);
					$new_user->setCreated_by(3);
					$new_user->setStatus('active');
					$new_user->setUser_type('user');
					$new_user->setCurrent_lang_id(1);
					$new_user->setDomain_id(1);
					$new_user->setCreated_datetime(date('Y-m-d')); 
					$new_user->setLast_modified(date('Y-m-d'));
					$new_user->setLast_login(date('Y-m-d'));
					$em = $this->getDoctrine()->getManager();
					$em->persist($new_user);
					$em->flush();
					//$this->get('session')->getFlashBag()->set('success_msg', 'You registered Successfully,Thank You.');
					$this->get('session')->getFlashBag()->set('success_msg1', 'You Registered Successfully, Thank You.');
					
					#addtion Loyalty Points
					$points_added = $this->addCompetitionPointsToUser($new_user->getUser_master_id(),'registration',0,$new_user->getUser_master_id(),'user_master');
					#addtion Loyalty Points done

					$live_path = $this->container->getParameter('live_path');
					
					$userid = $new_user->getUser_master_id();
					$username = $new_user->getUsername();
					$nickname = $new_user->getUser_bio();
					$email = $new_user->getEmail();
					$contact_no = $new_user->getUser_mobile();
					$current_lang_id = $new_user->getCurrent_lang_id();
					$profile_image_id = $new_user->getUser_image();
					$image_url = $live_path . '/bundles/Resource/default.png'; 
					if($profile_image_id != 0 ){
						$image_url = $this->getImage($profile_image_id, $live_path) ; 
					}
					$this->get('session')->set('userid',$userid);
					$this->get('session')->set('user_id',$userid);
					$this->get('session')->set('username',$username);
					$this->get('session')->set('nick_name',$nickname);
					$this->get('session')->set('email',$email);
					$this->get('session')->set('contact_no',$contact_no);
					$this->get('session')->set('role_id',$new_user->getUser_role_id());
					$this->get('session')->set('current_lang_id',$current_lang_id);
					$this->get('session')->set('image_url',$image_url);
							
					
					return new Response('success');					
				}
			}
		}
		return new Response('error');
	}
     

}
