<?php
namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;

use AdminBundle\Entity\Commonquestion;
use AdminBundle\Entity\Restaurantmaster;
use AdminBundle\Entity\Categorymaster;



class CategoryFoodController extends BaseController {
	
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }
	
    /**
     * @Route("/category_wise_restaraunts/{main_category_id}",name="site_cat_wise_rest",defaults={"main_category_id"="0"})
     * @Template()
     */
    public function indexAction($main_category_id)
    {  
		if($this->get('session')->get('language_id') == null){
			$language_id = 1;
		}else{
			$language_id = $this->get('session')->get('language_id');
		}
		// category name
		$category_name = '';
		$cat = $this->getDoctrine()->getManager()->getRepository(Categorymaster :: class)->findOneBy(array('language_id' =>$language_id,'main_category_id'=>$main_category_id));
		if($cat){
			$category_name = $cat->getCategory_name();
		}	
		
	   $restaraunts = array();
	   $get_rest_sql = "select rest.restaurant_master_id as restaurent_id,rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_category_relation cat join restaurant_master rest on rest.main_restaurant_id=cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and cat.is_deleted=0 and rest.language_id='".$language_id."' and cat.main_category_id='".$main_category_id."' group by main_restaurant_id order by rest.restaurant_name";
	   $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_rest_sql);
	   $stmt->execute();
	   $restaraunts = $stmt->fetchAll();

	   $firstLetter = '-1';
	   $letter_wise_rest = array();
	   foreach($restaraunts as $rest_data){

			$restaurant_name = $rest_data['restaurant_name'];
			$pos = strpos($restaurant_name, "'");
			if($pos != ''){
				$restaurant_name = stripslashes($restaurant_name);
				$rest_data['restaurant_name'] = $restaurant_name;
			}

	   		if($firstLetter != mb_substr($rest_data['restaurant_name'], 0, 1, 'utf8')){
	   			$firstLetter = 	mb_substr($rest_data['restaurant_name'], 0, 1, 'utf8');
				
	   			$letter_wise_rest[$firstLetter][0]  = $rest_data;
	   		}
	   		else{
	   			array_push($letter_wise_rest[$firstLetter],$rest_data);
	   		}
	   }
	   $food_types = array();
	   $get_food_type_sql = "select food_type.food_type_name,food_type.language_id,food_type.main_food_type_id from food_type_category_relation food_cat join food_type_master food_type on food_type.main_food_type_id=food_cat.main_food_type_id where food_type.is_deleted=0 and food_type.language_id='".$language_id."' and food_cat.is_deleted=0 and food_cat.main_category_id='".$main_category_id."' and food_cat.is_deleted = 0 group by food_type.main_food_type_id ORDER BY food_type.food_type_name";
	   $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_food_type_sql);
	   $stmt->execute();
	   $food_types = $stmt->fetchAll();
	   	
	   return array('food_types'=>$food_types,'category_name'=>$category_name,'restaraunts'=>$letter_wise_rest,'main_category_id'=>$main_category_id); 
	}

	/**
     * @Route("/filter-restaurants")
     * @Template()
     */
    public function filterRestaurantsAction(Request $req)
    {
		$html = '';
		$success = false;
		
		$category_id = $req->get('category_id');
		$food_type_id = $req->get('food_type_id');
		$res_name = $req->get('res_name');
		
		if(isset($category_id)){
			
			$session = $this->get('session');
			$language_id = $session->get('language_id');
			
			if(isset($food_type_id) && $food_type_id != '' && $food_type_id != 0){
				$get_rest_sql = "select rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_foodtype_relation food_cat join restaurant_master rest on rest.main_restaurant_id=food_cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and food_cat.is_deleted=0 and  food_cat.main_caetgory_id='".$category_id."' and rest.language_id='".$language_id."' and food_cat.main_foodtype_id='".$food_type_id."' order by rest.restaurant_name";
			} elseif(isset($category_id) && $category_id != '') {
				 $get_rest_sql = "select rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_category_relation cat join restaurant_master rest on rest.main_restaurant_id=cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and rest.language_id='".$language_id."' and cat.is_deleted=0 and  cat.main_category_id='".$category_id."' and rest.restaurant_name like '%".$res_name."%' order by rest.restaurant_name";
			}
			
			$em = $this->getDoctrine()->getManager(); 
			$con = $em->getConnection();
			$stmt = $con->prepare($get_rest_sql);
			$stmt->execute();
			$restaraunts = $stmt->fetchAll();					
			$firstLetter = '-1';
			$letter_wise_rest = array();
			foreach($restaraunts as $rest_data){
				$restaurant_name = $rest_data['restaurant_name'];
				$pos = strpos($restaurant_name, "'");
				if($pos != ''){
					$restaurant_name = stripslashes($restaurant_name);
					$rest_data['restaurant_name'] = $restaurant_name;
				}

				if($firstLetter != mb_substr($rest_data['restaurant_name'], 0, 1, 'utf8')){
					$firstLetter = 	mb_substr($rest_data['restaurant_name'], 0, 1, 'utf8');
			
					$letter_wise_rest[$firstLetter][0]  = $rest_data;
				}
				else{
					array_push($letter_wise_rest[$firstLetter],$rest_data);
				}
			}
			
			if(!empty($letter_wise_rest)){
				$live_path = $this->container->getParameter('live_path');
				foreach($letter_wise_rest as $key => $value){
					$html .= "<div class='wall-column card'>
								<div class='wall-item type-block card-body'>
									<h4>".strtoupper($key)."</h4>";
					
									$_url = $this->generateUrl('site_restaurant_view', array('restaurent_id' => $rest_data['main_restaurant_id']));
					
									$html .= '<ul class="list-unstyled">';
									foreach($value as $rest_data){
										$html .= "<li>";
											$html .= "<a href='{$_url}'>{$rest_data['restaurant_name']}</a>";
										$html .= "</li>";
									}
									$html .= '</ul>';
					$html .= '</div>';
				$html .= '</div>';
				}
			}
			$success = true;
		}
		
		if($html == ''){
			if($language_id == 1){
				$html = '<p>No Restaraunt found for this category</p>';
			} elseif($language_id == 2) {
				$html = '<p>لم يتم العثورعلى المطعم لهذ الفئة</p>';
			}
		}
		
		$data = array(
			'success' => $success,
			'html' => $html
		);
		
		return new Response(json_encode($data));
	}
	
    /**
     * @Route("/category_food_wise_restaraunts",name="site_cat_food_wise_rest")
     * @Template()
     */
    public function foodCatRestAction(Request $req)
    {
		if($this->get('session')->get('language_id') == null){
			$language_id = 1;
		}else{
			$language_id = $this->get('session')->get('language_id');
		}
		
	   $restaraunts = array();
       if($req->request->get('food_type_id') == '0'){
			if($req->request->get('keyword') != NULL && $req->request->get('keyword') != '' ){
			   $get_rest_sql = "select rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_category_relation cat join restaurant_master rest on rest.main_restaurant_id=cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and rest.language_id='".$language_id."' and cat.is_deleted=0 and  cat.main_category_id='".$req->request->get('category_id')."' and rest.restaurant_name like '%".$req->request->get('keyword')."%' order by rest.restaurant_name";
			}else{
			   $get_rest_sql = "select rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_category_relation cat join restaurant_master rest on rest.main_restaurant_id=cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and rest.language_id='".$language_id."' and cat.is_deleted=0 and  cat.main_category_id='".$req->request->get('category_id')."' order by rest.restaurant_name";
		   }
       }else{	
			if($req->request->get('keyword') != NULL && $req->request->get('keyword') != '' ){	   
			   $get_rest_sql = "select rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_foodtype_relation food_cat join restaurant_master rest on rest.main_restaurant_id=food_cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and food_cat.is_deleted=0 and  food_cat.main_caetgory_id='".$req->request->get('category_id')."' and food_cat.main_foodtype_id='".$req->request->get('food_type_id')."' and rest.language_id='".$language_id."' and rest.restaurant_name like '%".$req->request->get('keyword')."%' order by rest.restaurant_name";
			}else{
			   $get_rest_sql = "select rest.restaurant_name,rest.language_id,rest.main_restaurant_id from restaurant_foodtype_relation food_cat join restaurant_master rest on rest.main_restaurant_id=food_cat.restaurant_id where rest.status='active' and rest.is_deleted=0 and food_cat.is_deleted=0 and  food_cat.main_caetgory_id='".$req->request->get('category_id')."' and rest.language_id='".$language_id."' and food_cat.main_foodtype_id='".$req->request->get('food_type_id')."' order by rest.restaurant_name";
			}   
		}

		$em = $this->getDoctrine()->getManager(); 
		$con = $em->getConnection();
		$stmt = $con->prepare($get_rest_sql);
		$stmt->execute();
		$restaraunts = $stmt->fetchAll();					
		$firstLetter = '-1';
		$letter_wise_rest = array();
	   foreach($restaraunts as $rest_data){
			
	   		if($firstLetter != mb_substr($rest_data['restaurant_name'], 0, 1, 'utf8')){
	   			$firstLetter = 	mb_substr($rest_data['restaurant_name'], 0, 1, 'utf8');
	   	
	   			$letter_wise_rest[$firstLetter][0]  = $rest_data;
	   		}
	   		else{
	   			array_push($letter_wise_rest[$firstLetter],$rest_data);
	   		}
	   }

	   return new Response(json_encode($letter_wise_rest));

	}

    /**
     * @Route("/add_new_restaraunt",name="site_add_new_rest")
     * @Template()
     */
    public function addNewRestarauntAction(Request $req)
    {  
		$Restaurant_data = new Restaurantmaster();
		$Restaurant_data->setRestaurant_name($req->request->get('rest_name'));
		$Restaurant_data->setRestaraunt_branch($req->request->get('rest_branch'));
		$Restaurant_data->setRestaurant_menu(0);
		$Restaurant_data->setPhone_number('');
		$Restaurant_data->setDescription('');
		$Restaurant_data->setLogo_id(0);
		$Restaurant_data->setTimings('');
		$Restaurant_data->setAddress_id(0);
		$Restaurant_data->setStatus('pending');
		$Restaurant_data->setLanguage_id(1);
		$Restaurant_data->setMain_restaurant_id(0);
		$em = $this->getDoctrine()->getManager();
		$em->persist($Restaurant_data);
		$em->flush();

		$rest_main_id = $Restaurant_data->getRestaurant_master_id();	
		$Restaurant_data->setMain_restaurant_id($rest_main_id);
		$em->flush();

		$flash_message1 = "Restaurant Added successfully";
		if($this->get('session')->get('language_id') != null && $this->get('session')->get('language_id') == '2')
		{
			$flash_message1 = "تمت إضافة المطعم بنجاح";
		}		     			

		$this->get('session')->getFlashBag()->set('success_msg',$flash_message1);
		return $this->redirectToRoute('site_default_index');
	}
	
}
