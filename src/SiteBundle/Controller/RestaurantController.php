<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Cookie;
use AdminBundle\Entity\Contactus;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Contactusfeedback;
use AdminBundle\Entity\Evaluationfeedback;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Evaluationfeedbackgallery;
use AdminBundle\Entity\Restaurantmaster;
use AdminBundle\Entity\Restaurantmenu;
use AdminBundle\Entity\Foodtypemaster;
use AdminBundle\Entity\Restaurantoffer;
use AdminBundle\Entity\Addressmaster;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Foodtypecategoryrelation;
use AdminBundle\Entity\Restaurantfoodtyperelation;
use AdminBundle\Entity\Bookmarkmaster;
use AdminBundle\Entity\Branchmaster;
use AdminBundle\Entity\Evaluationlikerelation;

class RestaurantController extends BaseController {

    public $REVIEW_PER_PAGE = 5;    
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkUserStatus();
    }

    /**
     * @Route("/restaurant/view/{restaurent_id}", defaults={"restaurent_id" : ""})
     * @Template()
     */
    public function viewAction($restaurent_id) {

        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
        // open soon restaurant list
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster');
        $restaurant = $repository->findOneBy([
            'main_restaurant_id' => $restaurent_id,
			'status' => 'active',
            "language_id" => $this->get('session')->get('language_id'),
            'is_deleted' => 0
        ]);
        //menu
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmenu');
        $restaurantmenu = $repository->findBy([
            'restaurant_id' => $restaurent_id,
            'language_id' => $this->get('session')->get('language_id'),
            'is_deleted' => 0
                ], array('sort_order' => 'ASC'));
        //offer
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantoffer');
        $restaurantoffer = $repository->findBy([
            'restaurant_id' => $restaurent_id,
            'is_deleted' => 0
        ]);
        //foodtype

        /* $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantfoodtyperelation');
        $restaurantfoodtype = $repository->findBy([
            'restaurant_id' => $restaurent_id,
            'is_deleted' => 0
        ]); */
		$sql = "select * from restaurant_foodtype_relation where restaurant_id = {$restaurent_id} and is_deleted = 0 group by main_foodtype_id";
		$restaurantfoodtype = $this->firequery($sql);
		
        $food_types = array();
        $foodtypelist[] = array();
        if (!empty($restaurantfoodtype)) {
            foreach ($restaurantfoodtype as $restaurantfoodtype) {
				$check_foodtype_cat_rel = $this->getDoctrine()->getRepository('AdminBundle:Foodtypecategoryrelation')->findOneBy([
					'main_food_type_id' => $restaurantfoodtype['main_foodtype_id'],
					'main_category_id' => $restaurantfoodtype['main_caetgory_id'],
					'is_deleted' => 0
				]);
				
				if(!empty($check_foodtype_cat_rel)){
					$repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster');
					$foodtype = $repository->findOneBy([
						'main_food_type_id' => $restaurantfoodtype['main_foodtype_id'],
						'is_deleted' => 0,
						"language_id" => $this->get('session')->get('language_id')
					]);
					if ($foodtype) {
						$foodtypelist[] = array(
							"foodtypename" => $foodtype->getFood_type_name()
						);
						$food_types[] = $foodtype->getFood_type_name();
					}
				}
            }
        }
		
		$food_type_str = implode(', ', $food_types);
		
		//bookmark
        $Companyfavouritemaster = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Bookmarkmaster')
                ->findOneBy(array('is_deleted' => 0, 'user_id' => $this->get('session')->get('userid'), 'shop_id' => $restaurent_id));
        //var_dump($Companyfavouritemaster);exit;
        //location
        $address_master = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Addressmaster')
                ->findBy(array('is_deleted' => 0, 'language_id' => $this->get('session')->get('language_id'), 'owner_id' => $restaurent_id));
		
		$Addressmaster = array();
		if(!empty($address_master)){
			foreach($address_master as $_address){
				if($_address->getAddress_name() != ''){
					$Addressmaster = $_address;
				}
			}
		}
		
        //review
        
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedback');
        $reviewmaster = $repository->findBy([
            'restaurant_id' => $restaurent_id,
            'status' => 'approved',
            'is_deleted' => 0
                ], array('created_datetime' => 'desc'),$this->REVIEW_PER_PAGE,0);
        $reviewlist[] = array();
        $reviewImages[] = array();
       
        $total_review = 0 ;

        if (!empty($reviewmaster)) {

            $review_count = "select evaluation_feedback_id from evaluation_feedback where restaurant_id = '$restaurent_id' and status = 'approved' and is_deleted = 0 ";

            $stmt_review_count = $this->getDoctrine()->getManager()->getConnection()->prepare($review_count);                 
            $stmt_review_count->execute();
            $review_exist_ = $stmt_review_count->fetchAll();

            

            if(!empty($stmt_review_count)){
                $total_review = count($review_exist_);
            }

            $temp_count = 0;
            foreach ($reviewmaster as $reviewmaster) {

				if($reviewmaster->getComments()){
					
					$repository = $this->getDoctrine()->getRepository('AdminBundle:Usermaster');
					$usermaster = $repository->findOneBy([
						'user_master_id' => $reviewmaster->getUser_id(),
						'is_deleted' => 0
					]);
					if ($usermaster) {
						//$username = $usermaster->getUsername();
						$username = $usermaster->getUser_firstname().' '.$usermaster->getUser_lastname();
						if($username == '' || $username == ' '){
							$option_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array("language_id" => $this->get('session')->get('language_id'), "is_deleted" => 0, "main_word_dictionary_id" => 301));
							if(!empty($option_name)){
								$username = $option_name->getWord_name();
							}
						}
						
						$userimage = $this->getMediaUrl($usermaster->getUser_image());

						$review_id = $reviewmaster->getEvaluation_feedback_id();
						$userreview = $reviewmaster->getComments();
						$service_level = $reviewmaster->getService_level();
						$dining_level = $reviewmaster->getDining_level();
						$atmosphere_level = $reviewmaster->getAtmosphere_level();
						$price_level = $reviewmaster->getPrice_level();
						$clean_level = $reviewmaster->getClean_level();
						$speed_level = $reviewmaster->getDelivery_speed_level();
						$packaging_level = $reviewmaster->getPackaging_level();
						
						$main_branch_id = $reviewmaster->getMain_branch_id();
						
						$totalpoint = ($service_level + $dining_level + $atmosphere_level + $price_level + $clean_level + $packaging_level + $speed_level) / 5;
						$reviewimage = array();
						$repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedbackgallery');
						$Reviewgallery = $repository->findBy([
							'evaluation_feedback_id' => $review_id,
							'is_deleted' => 0
						]);
						if (!empty($Reviewgallery)) {
							foreach ($Reviewgallery as $_gallery) {
								$reviewimage[] = $this->getMediaUrl($_gallery->getMedia_id());
								$reviewImages[] = $this->getMediaUrl($_gallery->getMedia_id());
							}
						}

						## get admin comment
						$admin_comment_list = $this->getDoctrine()->getRepository('AdminBundle:Evaluationadmincomment')->findBy(
								array(
									'evaluation_feedback_id' => $reviewmaster->getEvaluation_feedback_id(),
									'is_deleted' => 0
								)
						);

						//array_shift($reviewImages);

						$admin_comment = array();
						if (!empty($admin_comment_list)) {
							foreach ($admin_comment_list as $_comment) {
								$admin_comment[] = $_comment->getAdmin_comment();
							}
						}

						#branch
						$rest_branch = $this->getDoctrine()->getRepository('AdminBundle:Branchmaster')->findOneBy(
								array(
									'main_branch_master_id' => $main_branch_id,
									'is_deleted' => 0
								)
						);
						$branch_name = '';
						if($rest_branch){
							$branch_name = $rest_branch->getBranch_name();
						}
						
#get Like Count from Evaluationlikerelation
									$likes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'like'));
									$likes = 0;
									if($likes_data){
										$likes = count($likes_data);
									}
									$dislikes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'dislike'));
									$dislikes = 0;
									if($dislikes_data){
										$dislikes = count($dislikes_data);
									}
#get Like Count from Evaluationlikerelation ends
							$like_by_user = false;
							$dislike_by_user = false;
							if($this->get('session')->get('user_id') != NULL){

								$likes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'user_id'=>$this->get('session')->get('user_id'),'operation'=>'like'));
								
								
								if($likes_data_user){
									$like_by_user = true;
								}
								
								$dislikes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'user_id'=>$this->get('session')->get('user_id'),'operation'=>'dislike'));
								
								
								if($dislikes_data_user){
									$dislike_by_user = true;
								}
							
							}

							

									
						
						$reviewlist[] = array(
							'username' => $username,
							'review_id' => $review_id,
							'userreview' => urldecode(preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $userreview)),
							'userimage' => $userimage,
						//	'userreview' => $this->decodeEmoticons($userreview),
							'reviewimage' => $reviewimage,
							'admin_comment' => $admin_comment,
							'branch_name' => $branch_name,
							'rate' => $totalpoint,
							'likes'=>$likes,
							'dislikes'=>$dislikes,
							'like_by_user'=>$like_by_user,
							'dislike_by_user'=>$dislike_by_user,
							'created_date' =>$reviewmaster->getCreated_datetime()
						);
					}
				}
            }
        }
        //var_dump($reviewlist);exit;
        if (empty($restaurant)) {
            $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster');
            $restaurant = $repository->findOneBy([
                'main_restaurant_id' => $restaurent_id,
				'status' => 'active',
                'is_deleted' => 0
            ]);
        }

#Branches		

        $repository1= $this->getDoctrine()->getRepository('AdminBundle:Branchmaster');
        $branch_list = $repository1->findBy([
            'main_restaurant_id' => $restaurent_id,
            'language_id' => $this->get('session')->get('language_id'),
            'is_deleted' => 0
                ], array('main_branch_flag' => 'DESC'));
				
				
#Branches ends		
		
        $photo_gallery = array();
        if (!empty($restaurant)) {
			
			// remove \'s from res name
			$restaurant_name = $restaurant->getRestaurant_name();
			$pos = strpos($restaurant_name, "'");
			if($pos != ''){
				$restaurant_name = stripslashes($restaurant_name);
			}

            $gallery_repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantgallery');
            $restaurant_gallery = $gallery_repository->findBy(
                    array(
                        'restaurant_id' => $restaurent_id,
                        'is_deleted' => 0
                    )
            );

            $gallery = array();
            if (!empty($restaurant_gallery)) {

                foreach ($restaurant_gallery as $_gallery) {
                    $gallery[] = $this->getMediaUrl($_gallery->getImage_id());
                }
            }

            $photo_gallery1 = array_merge($gallery, $reviewImages);

            if (!empty($photo_gallery1)) {
                foreach ($photo_gallery1 as $key => $value) {
                    if (!is_array($value)) {
                        $photo_gallery[] = $value;
                    }
                }
            }

            $menuimage = array();
            if (!empty($restaurantmenu)) {
                foreach ($restaurantmenu as $_gallery) {
                    if ($this->getMediaMenuUrl($_gallery->getMedia_id()) != '') {
                        $menuimage[] = $this->getMediaMenuUrl($_gallery->getMedia_id());
                    }
                }
            }

            $offerimage = array();
            if (!empty($restaurantoffer)) {
                foreach ($restaurantoffer as $_gallery) {
                    $offerimage[] = $this->getMediaUrl($_gallery->getMedia_id());
                }
            }
            /*
              $queryeval = "SELECT * FROM evaluation_feedback_gallery INNER JOIN evaluation_feedback ON evaluation_feedback.evaluation_feedback_id=evaluation_feedback_gallery.evaluation_feedback_id INNER JOIN restaurant_master ON restaurant_master.restaurant_master_id=evaluation_feedback.restaurant_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.restaurant_id='".$restaurent_id."'";
              $em = $this->getDoctrine()->getManager();
              $con = $em->getConnection();
              $stmt = $con->prepare($queryeval);
              $stmt->execute();
              $evaluation_gallery = $stmt->fetchAll();
              $evalgallery = array();
              if (!empty($evaluation_gallery)) {
              foreach ($evaluation_gallery as $_egallery) {
              $evalgallery[] = $this->getMediaUrl($_egallery['media_id']);
              }
              }
             */
            $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $restaurent_id . "' and status='approved' and is_deleted = 0) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level +evaluation_feedback.delivery_speed_level + evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $restaurent_id;


            $evpoints = $this->firequery($restaurant_total_evpointsQuery);

            $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
            $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;

            $rate_per = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
            $rate = number_format(($rate_per * 20), 1);

            $restaurant->restaurant_name1 = $restaurant_name;
            $restaurant->timing = $restaurant->getTimings();
			$restaurant->media_url = $this->getMediaUrl($restaurant->getLogo_id());
            $restaurant->gallery = $gallery;
            $restaurant->menuimage = $menuimage;
            $restaurant->offerimage = $offerimage;
            $restaurant->rate = $rate;
            $restaurant->points = $total_rate;
            $restaurant->reviewlist = $reviewlist;
            $restaurant->food_type_str = $food_type_str;
            $restaurant->foodtypelist = $foodtypelist;
            $restaurant->bookmark = $Companyfavouritemaster;
            $restaurant->addressmaster = $Addressmaster;
            $restaurant->photo_gallery = $photo_gallery;
            $restaurant->branch_list = $branch_list;
            $restaurant->total_review = $total_review;
            $restaurant->page = !empty($total_review) ? ceil($total_review / $this->REVIEW_PER_PAGE) : 0;

//            $restaurant->eval_gallery = $evalgallery;
        } else {
            $restaurant = array();
        }
        /*
           echo '<pre>';
          print_r($restaurant);
          exit; 
        */
        return array(
            'restaurant' => $restaurant,
			'restaurent_id'=>$restaurent_id
        );
    }
    
    public function decodeEmoticons($src) {
	    $replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $src);
	    $result = mb_convert_encoding($replaced, "UTF-16", "HTML-ENTITIES");
	    $result = mb_convert_encoding($result, 'utf-8', 'utf-16');
	    return $result;
	}
    /**
     * @Route("/getPaginationReview")
     */
    public function getPaginationReviewAction(Request $req) {
        
        $restaurent_id = $req->request->get('rest_id');
        $page_no = $req->request->get('page_no');

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster');
        $restaurant = $repository->findOneBy([
            'main_restaurant_id' => $restaurent_id,
			'status' => 'active',
            "language_id" => $this->get('session')->get('language_id'),
            'is_deleted' => 0
        ]);
        $rest_media = '';    
        if($restaurant){
            $rest_media = $this->getMediaUrl($restaurant->getLogo_id());;
        }

        $offset = ( $page_no - 1 ) * $this->REVIEW_PER_PAGE;

        $repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedback');
        $reviewmaster = $repository->findBy([
            'restaurant_id' => $req->request->get('rest_id'),
            'status' => 'approved',
            'is_deleted' => 0
                ], array('created_datetime' => 'desc'),$this->REVIEW_PER_PAGE,$offset);

//        print_r($reviewmaster);exit;
        $reviewlist[] = array();
        $reviewImages[] = array();
       
        $total_review = 0 ;

        if (!empty($reviewmaster)) {
          
            $temp_count = 0;
            foreach ($reviewmaster as $reviewmaster) {

				if($reviewmaster->getComments()){
					
					$repository = $this->getDoctrine()->getRepository('AdminBundle:Usermaster');
					$usermaster = $repository->findOneBy([
						'user_master_id' => $reviewmaster->getUser_id(),
						'is_deleted' => 0
					]);
					if ($usermaster) {
						//$username = $usermaster->getUsername();
						$username = $usermaster->getUser_firstname().' '.$usermaster->getUser_lastname();
						if($username == '' || $username == ' '){
							$option_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array("language_id" => $this->get('session')->get('language_id'), "is_deleted" => 0, "main_word_dictionary_id" => 301));
							if(!empty($option_name)){
								$username = $option_name->getWord_name();
							}
						}
						
						$userimage = $this->getMediaUrl($usermaster->getUser_image());

						$review_id = $reviewmaster->getEvaluation_feedback_id();
						$userreview = $reviewmaster->getComments();
						$service_level = $reviewmaster->getService_level();
						$dining_level = $reviewmaster->getDining_level();
						$atmosphere_level = $reviewmaster->getAtmosphere_level();
						$price_level = $reviewmaster->getPrice_level();
						$clean_level = $reviewmaster->getClean_level();
						$speed_level = $reviewmaster->getDelivery_speed_level();
						$packaging_level = $reviewmaster->getPackaging_level();
						
						$main_branch_id = $reviewmaster->getMain_branch_id();
						
						$totalpoint = ($service_level + $dining_level + $atmosphere_level + $price_level + $clean_level + $packaging_level + $speed_level) / 5;
						$reviewimage = array();
						$repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedbackgallery');
						$Reviewgallery = $repository->findBy([
							'evaluation_feedback_id' => $review_id,
							'is_deleted' => 0
						]);
						if (!empty($Reviewgallery)) {
							foreach ($Reviewgallery as $_gallery) {
								$reviewimage[] = $this->getMediaUrl($_gallery->getMedia_id());
								$reviewImages[] = $this->getMediaUrl($_gallery->getMedia_id());
							}
						}

						## get admin comment
						$admin_comment_list = $this->getDoctrine()->getRepository('AdminBundle:Evaluationadmincomment')->findBy(
								array(
									'evaluation_feedback_id' => $reviewmaster->getEvaluation_feedback_id(),
									'is_deleted' => 0
								)
						);

						//array_shift($reviewImages);

						$admin_comment = array();
						if (!empty($admin_comment_list)) {
							foreach ($admin_comment_list as $_comment) {
								$admin_comment[] = $_comment->getAdmin_comment();
							}
						}

						#branch
						$rest_branch = $this->getDoctrine()->getRepository('AdminBundle:Branchmaster')->findOneBy(
								array(
									'main_branch_master_id' => $main_branch_id,
									'is_deleted' => 0
								)
						);
						$branch_name = '';
						if($rest_branch){
							$branch_name = $rest_branch->getBranch_name();
						}
						
#get Like Count from Evaluationlikerelation
									$likes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'like'));
									$likes = 0;
									if($likes_data){
										$likes = count($likes_data);
									}
									$dislikes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'dislike'));
									$dislikes = 0;
									if($dislikes_data){
										$dislikes = count($dislikes_data);
									}
#get Like Count from Evaluationlikerelation ends
							$like_by_user = false;
							$dislike_by_user = false;
							if($this->get('session')->get('user_id') != NULL){

								$likes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'user_id'=>$this->get('session')->get('user_id'),'operation'=>'like'));
								
								
								if($likes_data_user){
									$like_by_user = true;
								}
								
								$dislikes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'user_id'=>$this->get('session')->get('user_id'),'operation'=>'dislike'));
								
								
								if($dislikes_data_user){
									$dislike_by_user = true;
								}
							
							}

							

									
						
						$reviewlist[] = array(
							'username' => $username,
							'review_id' => $review_id,
							'userreview' => urldecode(preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $userreview)),
							'userimage' => $userimage,
							'reviewimage' => $reviewimage,
							'admin_comment' => $admin_comment,
							'branch_name' => $branch_name,
							'rate' => $totalpoint,
							'likes'=>$likes,
							'dislikes'=>$dislikes,
							'like_by_user'=>$like_by_user,
							'dislike_by_user'=>$dislike_by_user,
							'created_date' =>$reviewmaster->getCreated_datetime()
						);
					}
				}
            }

            $label_id = 309;
            $language_id = $this->get('session')->get('language_id');
            $word = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id'=>$label_id,'language_id'=>$language_id));
            $branch_text = $word->getWord_name();
            
            $label_id = 305;
            $word1 = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array('main_word_dictionary_id'=>$label_id,'language_id'=>$language_id));
            $rate_text = $word1->getWord_name();

            $review_html = '';
            if(!empty($reviewlist)){

                foreach($reviewlist as $_reviewlist){
                    if(empty($_reviewlist)){
                        continue;
                    }
                    $review_html .= '<div class="media" style="overflow:hidden;">';
                    $left_style = '';
                    if($this->get('session')->get('language_id') == 2){
                        $left_style = 'fillpadd-left-ar';
                    }
                    $review_html .= '<div class="media-left '.$left_style.'"><a href="#"><img class="media-object img-circle" src="'.$_reviewlist['userimage'].'"></a></div>';
                    $review_html .= '<div class="media-body">';
                    $review_html .= '<h4 class="media-heading">'.$_reviewlist['username'].'</h4>';
                    $review_html .= '<p><strong>'.$branch_text.' :';
                    $review_html .= $_reviewlist['branch_name'].'</strong></p>';
                    $review_html .= '<p><strong>'.$rate_text.' <span class="badge">'.$_reviewlist['rate'].'</span></strong></p>';
                    $review_html .= '<p>'.$_reviewlist['userreview'].'</p>';
                    if(!empty($_reviewlist['reviewimage'])){

                        $review_html .= '<ul class="list-inline-comment">';
                        foreach($_reviewlist['reviewimage'] as $review_image_){
                            $review_html .= '<li><a href="'.$review_image_.'" data-fancybox="comment-imagesom yousef"><img src="'.$review_image_.'" class="img-thumbnail" style="height:auto;max-height:150px"></a></li>';
                        }
                        $review_html .= '</ul>';

                    }

                    $review_html .= '<p class="text-right">'.date('d-m-Y',strtotime($_reviewlist['created_date'])).'</p>';
                    
                    if(!empty($_reviewlist['admin_comment']) && !empty($restaurant) ){
                        $review_html .= '<div class="admin-comments">';
                        $review_html .= '<div class="col-md-12">';
                        $right_style1 = '';
                        $style_atr = '';
                        $left_style1 = '';
                        if($this->get('session')->get('language_id') == 1){
                            $right_style1 = ' col-md-offset-1 ';
                            $style_atr = 'style="text-align: end;"';
                        }else{
                            $left_style1 = ' admin-cmt-ar ';
                        }

                        $review_html .= '<div class="'.$right_style1.'col-md-3 fillpadd-ar '.$left_style1.'" '.$style_atr.'>';
                        $review_html .= '<img src="'.$rest_media.'" class="user-default img-circle">';
                        $review_html .= '</div>';

                        $right_style2 = '';
                        $style_atr1 = '';
                        $left_style2 = '';
                        if($this->get('session')->get('language_id') == 1){
                            $right_style2 = ' col-md-offset-1 ';
                        }else{
                            $left_style2 = ' text-left ';
                            $style_atr1 = 'style="float:right;text-align: start;"';

                        }

                        $review_html .= '<div class="col-md-8 admin-cmt fillpadd-right '.$left_style2.'" '.$style_atr1.'>';
                        $review_html .= '<p class="admin-txt" style="margin-bottom: 2px;"><b>'.$restaurant->getRestaurant_name().'</b></p>';

                        foreach($_reviewlist['admin_comment'] as $_admin_comment){

                            $review_html .= '<p>';
                            $review_html .= '<span style="/*white-space:pre;*/">'.$_admin_comment.'</span>';
                            $review_html .= '</p>';
                        
                            $review_html .= '</div>';
                            $review_html .= '</div>';

                            
                        }
                                                                                       
                         $review_html .= '</div>';
                    }
                    
                    $review_html .= '</div>';
                    $review_html .= '<div class="media-left">';
                    $review_html .= '<b id="liked_count_'.$_reviewlist['review_id'].'">'.$_reviewlist['likes'].'</b>';
                    
                    if($this->get('session')->get('user_id') != NULL ){
                        $btn_style = '';
                        if($_reviewlist['like_by_user'] == true){
                           $btn_style = 'liked_btn'; 
                        }
                        $review_html .= '<i class="fa fa-thumbs-up '.$btn_style.'" style="cursor:pointer" onclick="toggleLike(\''.$_reviewlist['review_id'].'\',\'like\',\'\')" id="liked_btn_'.$_reviewlist['review_id'].'"></i>';
                    }else{														
                        $review_html .= '<i class="fa fa-thumbs-up" data-toggle="modal" data-target="#loginModal"  style="cursor:pointer"></i>';
                    }

                    $review_html .= '<b id="disliked_count_'.$_reviewlist['review_id'].'">'.$_reviewlist['dislikes'].'</b>';

                    
                    if($this->get('session')->get('user_id') != NULL ){
                        $btn_style = '';
                        if($_reviewlist['dislike_by_user'] == true){
                           $btn_style = 'liked_btn'; 
                        }
                        $review_html .= '<i class="fa fa-thumbs-down '.$btn_style.'" style="cursor:pointer" onclick="toggleLike(\''.$_reviewlist['review_id'].'\',\'dislike\',\''.$_reviewlist['dislike_by_user'].'\')" id="disliked_btn_'.$_reviewlist['review_id'].'"></i>';
                    }else{														
                        $review_html .= '<i class="fa fa-thumbs-down" data-toggle="modal" data-target="#loginModal"  style="cursor:pointer"></i>';
                    }

                    $review_html .= '</div>';
                    $review_html .= '</div>';
                }
            }
            return new Response($review_html);

        }else{
            return new Response('');
        }

    }

    /**
     * @Route("/setbookmark")
     */
    public function setbookmarkAction() {

        $shop_id = $_POST['restaurant_id'];
        $user_id = $_POST['userid'];
        $em = $this->getDoctrine()->getManager();
        if ($_POST['status'] == 'add') {
            $Companyfavouritemaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Bookmarkmaster')
                    ->findOneBy(array('is_deleted' => 0, 'user_id' => $user_id, 'shop_id' => $shop_id));
            if (!empty($Companyfavouritemaster)) {
                $Companyfavouritemaster->setType("add");
                $em->persist($Companyfavouritemaster);
                $em->flush();
            } else {
                $Companyfavouritemaster = new Bookmarkmaster();
                $Companyfavouritemaster->setUser_id($user_id);
                $Companyfavouritemaster->setShop_id($shop_id);

                $Companyfavouritemaster->setCreate_date("2018-02-13 06:26:43");
                $Companyfavouritemaster->setType("add");
                $Companyfavouritemaster->setIs_deleted(0);
                $em->persist($Companyfavouritemaster);
                $em->flush();
            }
        }
        if ($_POST['status'] == 'remove') {
            $Companyfavouritemaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Bookmarkmaster')
                    ->findOneBy(array('is_deleted' => 0, 'user_id' => $user_id, 'shop_id' => $shop_id));
            if (!empty($Companyfavouritemaster)) {
                $Companyfavouritemaster->setType("remove");
                $em->persist($Companyfavouritemaster);
                $em->flush();
            } else {
                $Companyfavouritemaster = new Bookmarkmaster();
                $Companyfavouritemaster->setUser_id($user_id);
                $Companyfavouritemaster->setShop_id($shop_id);

                $Companyfavouritemaster->setCreate_date("2018-02-13 06:26:43");
                $Companyfavouritemaster->setType("remove");
                $Companyfavouritemaster->setIs_deleted(0);
                $em->persist($Companyfavouritemaster);
                $em->flush();
            }
        }
        return new Response('1');
    }

    /**
     * @Route("/my-bookmark")
     * @Template()
     */
    public function mybookmarkAction() {
        $session = $this->get('session');

        $user_id = $session->get('user_id');
        if (isset($user_id) && $user_id != '') {
            $bookmark = $this->getDoctrine()->getRepository('AdminBundle:Bookmarkmaster')->findBy(
                    array(
                        'user_id' => $user_id,
                        'type' => 'add',
                        'is_deleted' => 0
                    )
            );

//			echo"<pre>";print($user_id);exit;
            $restaurant_list = array();
            if (!empty($bookmark)) {
                foreach ($bookmark as $_bookmark) {
                    $_restaurant = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findOneBy(array('main_restaurant_id' => $_bookmark->getshop_id(), 'language_id' => $this->get('session')->get('language_id')));
                    if ($_restaurant) {
                        $_restaurant->media_url = $this->getMediaUrl($_restaurant->getLogo_id());

                        $_restaurant->res_description = substr($_restaurant->getDescription(), 0, 100) . ((strlen($_restaurant->getDescription()) > 100) ? '...' : '');

                        $gallery_repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantgallery');
                        $restaurant_gallery = $gallery_repository->findBy(
                                array(
                                    'restaurant_id' => $_bookmark->getshop_id(),
                                    'is_deleted' => 0
                                )
                        );

                        $gallery = array();
                        if (!empty($restaurant_gallery)) {
                            foreach ($restaurant_gallery as $_gallery) {
                                $gallery[] = $this->getMediaUrl($_gallery->getImage_id());
                            }
                        }

                        $queryeval = "SELECT * FROM evaluation_feedback_gallery INNER JOIN evaluation_feedback ON evaluation_feedback.evaluation_feedback_id=evaluation_feedback_gallery.evaluation_feedback_id INNER JOIN restaurant_master ON restaurant_master.main_restaurant_id=evaluation_feedback.restaurant_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.restaurant_id='" . $_bookmark->getshop_id() . "'";
                        $em = $this->getDoctrine()->getManager();
                        $con = $em->getConnection();
                        $stmt = $con->prepare($queryeval);
                        $stmt->execute();
                        $evaluation_gallery = $stmt->fetchAll();

                        $evalgallery = array();
                        if (!empty($evaluation_gallery)) {
                            foreach ($evaluation_gallery as $_egallery) {
                                $evalgallery[] = $this->getMediaUrl($_egallery['media_id']);
                            }
                        }

                        $address_details = '';
                        $address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("owner_id" => $_restaurant->getMain_restaurant_id()));
                        if (!empty($address_info)) {
                            $address_details = array(
                                "address_name" => $address_info->getAddress_name(),
                                "lat" => $address_info->getLat(),
                                "lng" => $address_info->getLng()
                            );
                        }
                        $_restaurant->restaurant_id = $_bookmark->getshop_id();
                        $_restaurant->gallery = array_merge($gallery, $evalgallery);
                        $_restaurant->address = $address_details;
                        $_restaurant->bookmark_master_id = $_bookmark->getBookmark_master_id();

                        // get catgory
                        $get_cat_sql = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and rfr.restaurant_id= " . $_bookmark->getshop_id() . " group by cm.main_category_id";
                        $em = $this->getDoctrine()->getManager();
                        $con = $em->getConnection();
                        $stmt = $con->prepare($get_cat_sql);
                        $stmt->execute();
                        $categorys = $stmt->fetchAll();
                        if (!empty($categorys)) {
                            $restaurant_list[] = $_restaurant;
                        }
                    }
                }
            }

            return array(
                'restaurant_list' => $restaurant_list
            );
        }

        return;
    }

    /**
     * @Route("/evaluate-restaurant/{res_id}", defaults={"res_id" : ""})
     * @Template()
     */
    public function evaluateRestaurantAction($res_id) {

        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
		
		$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and main_restaurant_id = '".$res_id."' and language_id = '".$this->get('session')->get('language_id')."' group by main_branch_master_id";

		$em = $this->getDoctrine()->getManager();
		$conn = $em->getConnection();
		$stmt = $conn->prepare($branch_query);
		$stmt->execute();
		$branch_list = $stmt->fetchAll();
			
        // category list for slider
        // $category = $this->getCategoryList();

        $session = $this->get('session');
        $user_id = $user_role_id = '';
        if ($session->has('user_id')) {
            $user_id = $this->get('session')->get('user_id');
            $user_role_id = $this->get('session')->get('role_id');
        }

        // category list
        // $category_repository = $this->getDoctrine()->getRepository('AdminBundle:Categorymaster');
        // $category_list = $category_repository->findBy(array('is_deleted' => 0));
        $category_query = " SELECT * FROM category_master where is_deleted = 0  and language_id = " . $this->get('session')->get('language_id') . " group by main_category_id";
        $category_list_arr = $this->firequery($category_query);
        if (!empty($category_list_arr)) {
            foreach ($category_list_arr as $cakey => $cval) {
                $category_list[] = array(
                    "category_master_id" => $cval['category_master_id'],
                    "category_name" => $cval['category_name'],
                    "main_category_id" => $cval['main_category_id']
                );
            }
        }

        // restaurant list
        $restaurent_repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster');
        /*
          if (isset($user_id) && $user_id != '' && $user_role_id != 1) {
          // selected restaurants
          $evaluation_collection = $this->getDoctrine()
          ->getRepository('AdminBundle:Evaluationfeedback')
          ->findBy(
          array(
          'user_id' => $user_id,
          'is_deleted' => 0
          )
          );

          if (!empty($evaluation_collection)) {

          $restaurant_list = array();
          foreach ($evaluation_collection as $_evaluation) {
          if ($_evaluation->getIs_deleted() == 0) {
          $restaurant = $restaurent_repository->findOneBy([
          'restaurant_master_id' => $_evaluation->getRestaurant_id(),
          'is_deleted' => 0,
          'status' => 'active'
          ]);
          if (!empty($restaurant)) {
          $restaurant_list[] = $restaurant;
          }
          }
          }
          } else {
          $restaurant_list = $restaurent_repository->findBy(array('is_deleted' => 0, 'status' => 'active'), array('restaurant_name' => 'ASC'));
          }
          } else {
          $restaurant_list = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findBy(
          array('is_deleted' => 0, 'status' => 'active', "language_id" => $this->get('session')->get('language_id')), array('restaurant_name' => 'ASC')
          );
          }
         */
        $restaurant_list = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findBy(
                array('is_deleted' => 0, 'status' => 'active', "language_id" => $this->get('session')->get('language_id')), array('restaurant_name' => 'ASC')
        );

        if(!empty($restaurant_list)){
            foreach($restaurant_list as $_restaurant_list){
                $restaurant_list_new[] = array('restaurant_name' => stripslashes($_restaurant_list->getRestaurant_name()) ,
                                                'main_restaurant_id' => $_restaurant_list->getMain_restaurant_id() ) ;

            }
        }
        // food type list
        $foodtype_repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster');
        $foodtype_list = $foodtype_repository->findBy(array('is_deleted' => 0, 'language_id' => $this->get('session')->get('language_id')));

        // level list
        $level_repository = $this->getDoctrine()->getRepository('AdminBundle:Levelmaster');
        $level_list = $level_repository->findBy(array('is_deleted' => 0, "language_id" => $this->get('session')->get('language_id')));

        if (empty($category_list)) {
            $category_list = array();
        }

        if (empty($restaurant_list_new)) {
            $restaurant_list_new = array();
        }

        if (empty($foodtype_list)) {
            $foodtype_list = array();
        }

        if (empty($level_list)) {
            $level_list = array();
        }

        if (isset($res_id) && $res_id != '') {

            $restaurant = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->find($res_id);

            return array(
                'restaurant_id' => $res_id,
                'category_list' => $category_list,
                'restaurant_list' => $restaurant_list_new,
                'foodtype_list' => $foodtype_list,
                'level_list' => $level_list,
				'branch_list'=>$branch_list
            );
        } else {
            return array(
                'category_list' => $category_list,
                'restaurant_list' => $restaurant_list_new,
                'foodtype_list' => $foodtype_list,
                'level_list' => $level_list
            );
        }
    }

    /**
     * @Route("/save-evaluate-restaurant")
     * @Template()
     */
    public function saveEvaluateRestaurantAction(Request $request) {
//		echo"<pre>";print_r($_REQUEST);exit;	
        $session = new Session();
        $user_id = $session->get('userid');
        $referer = $request->headers->get('referer');


		if ($request->request->all()) {

			#eval_type changes
			$evaluation_type = $eval_type = $_REQUEST['evaluation_type'];
            
            $dining_level = $_REQUEST['dining-level'];
            $price_level = $request->request->get('price-level');
            $clean_level = $request->request->get('clean-level');

            $speed_level = 0;
            $packaging_level = 0;
            $service_level = 0;
            $atmosphere_level = 0;

			if($request->request->get('evaluation_type') == 'dining'){
				$speed_level = 0;
                $packaging_level = 0;
                $service_level = $request->request->get('service-level');
                $atmosphere_level = $_REQUEST['atmosphere-level'];
			}
			
			if($request->request->get('evaluation_type') == 'delivery'){
				$service_level = 0;
                $atmosphere_level = 0;
                $speed_level = $_REQUEST['speed-level'];
			    $packaging_level = $_REQUEST['packaging-level'];
			}			
			
			#eval_type changes ends
            
            $new_restaurant_flag = false;
            
			$search_restaurant = $request->request->get('search_restaurant');
			if (isset($search_restaurant) && $search_restaurant != '') {
				// get category_id

				$category_id = 0;
				$foodtype_id = 0;
				$restaurant_id = $search_restaurant;
				$branch_id = $request->request->get('branch');
				/* $query = "select rel.* from restaurant_master res, restaurant_foodtype_relation rel where res.main_restaurant_id = rel.restaurant_id and res.main_restaurant_id = {$search_restaurant} and res.language_id = {$this->get('session')->get('language_id')} and res.is_deleted = 0 group by main_restaurant_id";
				$rel_data = $this->firequery($query);

				if (!empty($rel_data)) {
					$rel_data = $rel_data[0];
					if (!empty($rel_data)) {
						$category_id = $rel_data['main_caetgory_id'];
						$foodtype_id = $rel_data['main_foodtype_id'];
						$restaurant_id = $search_restaurant;
					}
				} */
			} else {
				$category_id = $request->request->get('category');
				$foodtype_id = $request->request->get('food-type');
				$restaurant_id = $request->request->get('restaurant');
				$branch_id = $request->request->get('branch');
				
				// check for new restaurant and branch
				$em = $this->getDoctrine()->getManager();
				$new_restaurant = $em->getRepository('AdminBundle:Restaurantmaster')->findBy([
					'main_restaurant_id' => $restaurant_id,
					'is_deleted'=>1
				]);
				
				if(!empty($new_restaurant)){

                    $new_restaurant_flag = true;

					foreach($new_restaurant as $new_restaurant_){
					
						$new_restaurant_->setIs_deleted(0);
						$em->flush();
					
					}
				}
				
				$new_branch = $em->getRepository('AdminBundle:Branchmaster')->findBy([
					'main_branch_master_id' => $branch_id
				]);
				
				if(!empty($new_branch)){
					foreach($new_branch as $new_branch_){
					
						$new_branch_->setIs_deleted(0);
						$em->flush();
					
					}
				}
				
			}

			// validate existing evaluation
			$existing_evaluation = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedback')->findBy(array('is_deleted' => 0));

			foreach ($existing_evaluation as $_eval) {
				if ( $_eval->getMain_branch_id() == $branch_id && $_eval->getEval_type() == $eval_type && $_eval->getUser_id() == $user_id && $_eval->getCategory_id() == $category_id && $_eval->getFood_type_id() == $foodtype_id && $_eval->getRestaurant_id() == $restaurant_id) {
					//$this->get('session')->getFlashBag()->set('error_msg', 'Restaurant already evaluated');
					if ($this->get('session')->get('language_id') == 1) {
						//$this->get('session')->getFlashBag()->set('error_msg', 'You have already evaluated the restaurant, kindly go to \'My Evaluation\' page if you wish to edit.');
						$this->get('session')->getFlashBag()->set('already_evaluated', 'You have already evaluated the restaurant, kindly go to \'My Evaluation\' page if you wish to edit.');
					} else {
						$this->get('session')->getFlashBag()->set('already_evaluated', 'يرجى الذهاب لأيقونة تقاييمي إذا أردت التعديل ');
					}
					return $this->redirect($referer);
				}
			}

			$media_id1 = 0;
			$media1 = $_FILES['invoice']['name'];
			if (isset($media1) && $media1 != '') {

				$upload_dir = $this->container->getParameter('upload_dir1');
				$location = '/Resource/Restaurant-Evaluate';

				//$allowedExts = explode(',', $mediatype->getMedia_type_allowed());
				$temp = explode('.', $_FILES['invoice']['name']);
				$extension = end($temp);

				$media_id1 = $this->mediaupload($_FILES['invoice'], $upload_dir, $location, 4);
			}

			if (!isset($category_id) or $category_id == '') {
				$category_id = 0;
			}

			if (!isset($foodtype_id) or $foodtype_id == '') {
				$foodtype_id = 0;
			}
			
			if($media_id1 == 0){
				if ($this->get('session')->get('language_id') == 1) {
					$this->get('session')->getFlashBag()->set('error_msg1', 'Something went wrong');
				} else {
					$this->get('session')->getFlashBag()->set('error_msg1', 'فشل الإتصال الرجاء المحاوله بعد فتره وجيزه');
				}
				return $this->redirect($this->generateUrl("site_restaurant_evaluaterestaurant"));
			}

			$em = $this->getDoctrine()->getManager();
			$evaluate = new Evaluationfeedback();
			$evaluate->setUser_id($user_id);
			$evaluate->setCategory_id($category_id);
			$evaluate->setFood_type_id($foodtype_id);
			$evaluate->setRestaurant_id($restaurant_id);
			$evaluate->setMain_branch_id($branch_id);
			$evaluate->setService_level($service_level);
			$evaluate->setDining_level($dining_level);
			$evaluate->setAtmosphere_level($atmosphere_level);
			$evaluate->setDelivery_speed_level($speed_level);
            $evaluate->setPackaging_level($packaging_level);
            $evaluate->setPrice_level($price_level);
			$evaluate->setClean_level($clean_level);
			$evaluate->setEval_type($request->request->get('evaluation_type'));
			$evaluate->setStatus('under_evaluation');
			$evaluate->setIs_featured('no');
			$evaluate->setShow_featured('active');
			$evaluate->setComments(urlencode($request->request->get('comment')));
			$evaluate->setInvoice_image_id($media_id1);
			$evaluate->setCreated_datetime(date('Y-m-d h:i:s'));
			$evaluate->setIs_deleted(0);

			$em->persist($evaluate);
			$em->flush();
			$evaluation_feedback_id = $evaluate->getEvaluation_feedback_id();
			$Config_live_site = $this->container->getParameter('live_path');
			$file_path = $this->container->getParameter('file_path');
			if (!empty($_FILES['image'])) {
				$image_array = array();
				if (array_key_exists('images_hidden', $request->request->all())) {
					$image_str = $request->request->get('images_hidden');
					$image_array = explode(',', $image_str);
				}

				foreach ($_FILES['image']['name'] as $key => $val) {

					$filename = $_FILES['image']['name'][$key];
					if (in_array($filename, $image_array)) {
						$tmpname = $_FILES['image']['tmp_name'][$key];
						//file path to uploads folder (/bundles/design/uploads)
						$file_path = $this->container->getParameter('file_path');
						//$path = $file_path . '/uploads/WSimages/';
						// $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';
						$upload_dir = $this->container->getParameter('upload_dir1');
						$location = '/Resource/Restaurant-Evaluate';
						$upload_dir = $upload_dir . $location;

						if (!is_dir($upload_dir)) {
							mkdir($upload_dir);
						}

						$clean_image = preg_replace('/\s+/', '', $filename);
						$media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

						$is_uploaded = move_uploaded_file($_FILES['image']['tmp_name'][$key], $upload_dir . '/' . $media_name);

						if ($is_uploaded) {
							$em = $this->getDoctrine()->getManager();

							$mediamaster = new Medialibrarymaster();
							$mediamaster->setMedia_type_id(1);
							$mediamaster->setMedia_title($media_name);
							$mediamaster->setMedia_location("/bundles/{$location}");
							$mediamaster->setMedia_name($media_name);
							$mediamaster->setCreated_on(date('Y-m-d h:i:s'));
							$mediamaster->setIs_deleted(0);

							$em->persist($mediamaster);
							$em->flush();

							$media_id12 = $media_id = $mediamaster->getMedia_library_master_id();
						}

						// $media_id12 = $media_id = $this->mediaupload($_FILES['image']['tmp_name'][$key], $upload_dir, $location, 1);
						if (!empty($media_id12)) {

							$evaluation_feedback_gallery = new Evaluationfeedbackgallery();
							$evaluation_feedback_gallery->setEvaluation_feedback_id($evaluation_feedback_id);
							$evaluation_feedback_gallery->setMedia_id($media_id12);
							$evaluation_feedback_gallery->setMedia_type_id(1);
							$evaluation_feedback_gallery->setIs_deleted(0);
							$em->persist($evaluation_feedback_gallery);
							$em->flush();
						}
					}
				}
            }
            
            #LoyaltyPoints changes

            $activity_type = "evaluation_without_image_comment";

            if (!empty($_FILES['image']) && !empty($_FILES['image']['size'][0])) {
                $activity_type = "evaluation_with_image";
            }
    
            if($request->request->get('comment') != ''){
                $activity_type = "evaluation_with_comments";
                
                if (!empty($_FILES['image']) && !empty($_FILES['image']['size'][0])) {
                    $activity_type = "evaluation_with_image_comment";
                }
            }

            if($new_restaurant_flag){
                $activity_type = "evaluation_new_restaurant";
            }

            if(!empty($evaluation_feedback_id) && !empty($restaurant_id) && !empty($user_id)){
                $points_added = $this->addCompetitionPointsToUser($user_id,$activity_type,$restaurant_id,$evaluation_feedback_id,'evaluation_feedback',$category_id);
            }

            #LoyaltyPoints changes done.


			if ($this->get('session')->get('language_id') == 1) {
				/* $this->get('session')->getFlashBag()->set('success_msg', 'Thank you for your valuable vote and comment'); */
				$this->get('session')->getFlashBag()->set('success_msg1', 'Thank you for Your Evaluation, We hope to see you soon');
			} else {
				$this->get('session')->getFlashBag()->set('success_msg1', 'شكراً لتقييمك ، نأمل أن نراك قريبًا');
			}
		} else {
			/* $this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong'); */
			if ($this->get('session')->get('language_id') == 1) {
				/* $this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong'); */
				$this->get('session')->getFlashBag()->set('error_msg1', 'Something went wrong');
			} else {
				$this->get('session')->getFlashBag()->set('error_msg1', 'فشل الإتصال الرجاء المحاوله بعد فتره وجيزه');
				//$this->get('session')->getFlashBag()->set('error_msg', 'فشل الإتصال الرجاء المحاوله بعد فتره وجيزه');
			}
		}
		
		return $this->redirect($this->generateUrl("site_restaurant_evaluaterestaurant"));
	}

        public function mediaupload($file, $upload_dir, $location, $media_type_id) {

            $upload_dir = $upload_dir . $location;

            if (!is_dir($upload_dir)) {
                mkdir($upload_dir);
            }

            $clean_image = preg_replace('/\s+/', '', $file['name']);
            $media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

            $is_uploaded = move_uploaded_file($file['tmp_name'], $upload_dir . '/' . $media_name);

            if ($is_uploaded) {
                $em = $this->getDoctrine()->getManager();

                $mediamaster = new Medialibrarymaster();
                $mediamaster->setMedia_type_id($media_type_id);
                $mediamaster->setMedia_title($media_name);
                $mediamaster->setMedia_location("/bundles/{$location}");
                $mediamaster->setMedia_name($media_name);
                $mediamaster->setCreated_on(date('Y-m-d h:i:s'));
                $mediamaster->setIs_deleted(0);

                $em->persist($mediamaster);
                $em->flush();

                return $mediamaster->getMedia_library_master_id();
            }
            return false;
        }

        /**
         * @Route("/getFilteredRestaurants/{category_id}/{foodtype_id}", defaults={"category_id":"","foodtype_id":""})
         * @Template()
         */
        public function getFilteredRestaurantsAction($category_id, $foodtype_id) {

            $cat_type = '';
            $food_type = '';

            if (isset($category_id) && $category_id != 0) {
                $cat_type = "and cm.main_category_id = {$category_id}";
            }

            if (isset($foodtype_id) && $foodtype_id != 0) {
                $food_type = "and ftm.main_food_type_id = {$foodtype_id}";
            }

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $st = $conn->prepare("SELECT rm.*
						from restaurant_master as rm
						left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
						left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
						left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
						where rm.is_deleted=0 {$cat_type} {$food_type} and rm.language_id={$this->get('session')->get('language_id')} and rm.is_deleted=0 and rm.status != 'inactive' and rfr.is_deleted = 0 group by rm.main_restaurant_id ORDER BY rm.restaurant_name ASC,cm.sort_order DESC");

            $st->execute();
            $restaurant_list = $st->fetchAll();


            $html = '';
            if (!empty($restaurant_list)) {

                /* echo '<pre>';
                  print_r($restaurant_list);
                  exit; */

                //$html = "<select id='first-disabled' class='selectpicker form-control' data-hide-disabled='true' data-live-search='true'>";

                $option_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array("language_id" => $this->get('session')->get('language_id'), "is_deleted" => 0, "main_word_dictionary_id" => 95));
                $html .= "<option value=''>" . $option_name->getWord_name() . "</option>";
                if (!empty($restaurant_list)) {
                    foreach ($restaurant_list as $_restaurant) {
                        $html .= "<option value='" . $_restaurant['main_restaurant_id'] . "'>".stripcslashes($_restaurant['restaurant_name'])."</option>";
                    }
                }
                //$html .= "</select>";
                echo $html;
                exit;
            }

            echo 'false';
            exit;
        }
		
        /**
         * @Route("/getFilteredRestaurantsBranches/{main_restaurant_id}", defaults={"main_restaurant_id":"","foodtype_id":""})
         * @Template()
         */
        public function getBranchsAction($main_restaurant_id) {

			$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and status = 'active' and  main_restaurant_id = '".$main_restaurant_id."' and language_id = '".$this->get('session')->get('language_id')."' group by main_branch_master_id";

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
			$stmt = $conn->prepare($branch_query);
			$stmt->execute();
			$branch_list = $stmt->fetchAll();

            $html = '';
            if (!empty($branch_list)) {

                /* echo '<pre>';
                  print_r($restaurant_list);
                  exit; */

                //$html = "<select id='first-disabled' class='selectpicker form-control' data-hide-disabled='true' data-live-search='true'>";

                $option_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array("language_id" => $this->get('session')->get('language_id'), "is_deleted" => 0, "main_word_dictionary_id" => 307));
                $html .= "<option value=''>" . $option_name->getWord_name() . "</option>";
                if (!empty($branch_list)) {
                    foreach ($branch_list as $_branch) {
                        $html .= "<option value='" . $_branch['main_branch_master_id'] . "'>{$_branch['branch_name']}</option>";
                    }
                }
                //$html .= "</select>";
                echo $html;
                exit;
            }

            echo 'false';
            exit;
        }
		

        /**
         * @Route("/restaurant/addnewrestaurantsite")     *
         */
        public function addnewrestaurantsiteAction() {
            $restaurant_name = $_REQUEST['new_restaurant_name'];
            $restaurant_branchname = $_REQUEST['branch_name'];
            // var_dump($restaurant_name);
            $em = $this->getDoctrine()->getManager();
            if ($restaurant_name != '') {
				$new_restaurant_id = 0 ;
				$main_branch_id = 0 ;
				
				$languages = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Languagemaster")->findBy(array('is_deleted'=>0));
				if(!empty($languages)){
					foreach($languages as $lang){
						
						$restaurant = new Restaurantmaster();
						$restaurant->setRestaurant_name($restaurant_name);
						$restaurant->setRestaurant_menu(0);
						$restaurant->setrestaraunt_branch($restaurant_branchname);
						$restaurant->setPhone_number('');
						$restaurant->setDescription('');
						$restaurant->setOpening_date(NULL);
						$restaurant->setLogo_id(0);
						$restaurant->setTimings('');
						$restaurant->setAddress_id(0);
						$restaurant->setStatus('pending');
						$restaurant->setLanguage_id($lang->getLanguage_master_id());
						$restaurant->setDomain_id(0);
						$restaurant->setMain_restaurant_id($new_restaurant_id);
						$restaurant->setIs_deleted(1);
						$em->persist($restaurant);
						$em->flush();

						if($new_restaurant_id == 0){
							$new_restaurant_id = $restaurant->getRestaurant_master_id();							
							$restaurant->setMain_restaurant_id($new_restaurant_id);
							$em->flush();													
						}
						
						$new_branch_master = new Branchmaster();
						$new_branch_master->setMain_restaurant_id($new_restaurant_id);
						$new_branch_master->setBranch_name($restaurant_branchname);
						$new_branch_master->setBranch_address_id(0);
						$new_branch_master->setMain_branch_flag(1);
						$new_branch_master->setStatus('pending');
						$new_branch_master->setOpening_date(NULL);
						$new_branch_master->setTimings('');
						$new_branch_master->setMobile_no(0);
						$new_branch_master->setDescription('');						
						$new_branch_master->setLanguage_id($lang->getLanguage_master_id());
						$new_branch_master->setMain_branch_master_id($main_branch_id);
						$new_branch_master->setIs_deleted(1);
						$em->persist($new_branch_master);
						$em->flush();

						if($main_branch_id == 0){
						
							$main_branch_id = $new_branch_master->getBranch_master_id();
							
							$new_branch_master->setMain_branch_master_id($main_branch_id);
							$em->flush();			
						
						}
					}
				}		
                // var_dump($restaurant);exit;
				$data = array('new_restaurant_id'=>$new_restaurant_id,'new_branch_id'=>$main_branch_id);
                return new Response(json_encode($data));
            }
            return new Response("enter your comment");

            //    var_dump($restaurant_name);exit;
        }

        /**
         * @Route("/restaurant/open-soon")
         * @Template()
         */
        public function restaurantOpenSoonAction() {
            if ($this->get('session')->get('language_id') != '') {
                $language_id = $this->get('session')->get('language_id');
            } else {
                $language_id = 1;
            }

            // category list for slider
            $category = $this->getCategoryList();

            // open soon restaurant list
            $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster');
            $restaurants = $repository->findBy(
                    array(
                        'status' => 'open_soon',
                        'is_deleted' => 0,
                        'language_id' => $language_id
                    )
            );

            $restaurant_list = array();

            foreach ($restaurants as $_restaurant) {
                // get media url
                $_restaurant->restaurant_name1 = stripslashes($_restaurant->getRestaurant_name());
                $_restaurant->media_url = $this->getMediaUrl($_restaurant->getLogo_id());

                //FOOD Type
                $food_type_query = "select foodtype.main_food_type_id, foodtype.language_id, foodtype.food_type_name 
								from restaurant_foodtype_relation rel join food_type_master foodtype 
								ON rel.main_foodtype_id = foodtype.main_food_type_id
								where rel.is_deleted = 0 and foodtype.is_deleted = 0 
								and rel.restaurant_id = {$_restaurant->getMain_restaurant_id()} 
								and foodtype.language_id='" . $language_id . "'";
                $con = $this->getDoctrine()->getManager()->getConnection();
                $stmt = $con->prepare($food_type_query);
                $stmt->execute();
                $food_type = $stmt->fetchAll();
//			print($food_type_query);exit;

                $gallery_repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantgallery');
                $restaurant_gallery = $gallery_repository->findBy(
                        array(
                            'restaurant_id' => $_restaurant->getMain_restaurant_id(),
                            'is_deleted' => 0
                        )
                );

                $gallery = array();
                if (!empty($restaurant_gallery)) {
                    foreach ($restaurant_gallery as $_gallery) {
                        $gallery[] = $this->getMediaUrl($_gallery->getImage_id());
                    }
                }

                $queryeval = "SELECT * FROM evaluation_feedback_gallery INNER JOIN evaluation_feedback ON evaluation_feedback.evaluation_feedback_id=evaluation_feedback_gallery.evaluation_feedback_id INNER JOIN restaurant_master ON restaurant_master.restaurant_master_id=evaluation_feedback.restaurant_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.restaurant_id='" . $_restaurant->getRestaurant_master_id() . "'";
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($queryeval);
                $stmt->execute();
                $evaluation_gallery = $stmt->fetchAll();

                $evalgallery = array();
                if (!empty($evaluation_gallery)) {
                    foreach ($evaluation_gallery as $_egallery) {
                        $evalgallery[] = $this->getMediaUrl($_egallery['media_id']);
                    }
                }

                $address_details = '';
                $address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("owner_id" => $_restaurant->getMain_restaurant_id()));
                if (!empty($address_info)) {
                    $address_details = array(
                        "address_name" => $address_info->getAddress_name(),
                        "lat" => $address_info->getLat(),
                        "lng" => $address_info->getLng()
                    );
                }
				
				$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and main_restaurant_id = '".$_restaurant->getMain_restaurant_id()."' and language_id = '".$this->get('session')->get('language_id')."' group by main_branch_master_id";

				$em = $this->getDoctrine()->getManager();
				$conn = $em->getConnection();
				$stmt = $conn->prepare($branch_query);
				$stmt->execute();
				$branch_list = $stmt->fetchAll();
			
                $_restaurant->restaurant_id = $_restaurant->getMain_restaurant_id();
                $_restaurant->gallery = array_merge($gallery, $evalgallery);
                $_restaurant->gallery = $gallery;
                $_restaurant->address = $address_details;
                $_restaurant->phone_no = $_restaurant->getPhone_number();
                $_restaurant->food_type = $food_type;
                $_restaurant->branch_list = $branch_list;

                // get catgory
                $get_cat_sql = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and rfr.restaurant_id= " . $_restaurant->getMain_restaurant_id() . " group by cm.main_category_id";
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($get_cat_sql);
                $stmt->execute();
                $categorys = $stmt->fetchAll();
                if (!empty($categorys)) {
                    $restaurant_list[] = $_restaurant;
                }
            }
//		var_dump($restaurant_list);exit;
            return array(
                'categories' => $category,
                'restaurant_list' => $restaurant_list
            );
        }

        /**
         * @Route("/view_evaluation/{res_id}", defaults={"res_id":""})
         * @Template()
         */
        public function viewresevaluationAction($res_id) {
            if ($this->get('session')->get('language_id') == null) {
                $this->get('session')->set('language_id', '1');
            }
            if (isset($res_id) && $res_id != '') {
                $query = "SELECT evaluation_feedback.*,restaurant_master.restaurant_name,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_bio,user_master.user_lastname,user_master.username,user_master.user_image FROM evaluation_feedback LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.restaurant_master_id LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.food_type_master_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.category_master_id=evaluation_feedback.category_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.status = 'approved' AND evaluation_feedback.restaurant_id='" . $res_id . "'";
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($query);
                $stmt->execute();
                $evaluation = $stmt->fetchAll();

                $restaurant = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->find($res_id);
                $restaurant_name = '';
                if (!empty($restaurant)) {
                    $restaurant_name = $restaurant->getRestaurant_name();
                }

                $evalresult = array();
                if (!empty($evaluation)) {
                    foreach ($evaluation as $key => $value) {
                        $chk_user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->find($value['user_id']);
                        if (!empty($chk_user)) {
                            $media_id = $value['user_image'];
                            if (!isset($media_id) or $media_id == '') {
                                $media_id = 0;
                            }
                            $user_image = $this->getMediaUrl($media_id);

                            $evalresult[] = array(
//							'user_name' => $value['user_firstname'] . ' ' . $value['user_lastname'],
                                'user_name' => $value['user_bio'],
                                'user_image' => $user_image,
                                'category_name' => $value['category_name'],
                                'food_type_name' => $value['food_type_name'],
                                'restaurant' => $value['restaurant_name'],
                                'comments' => $this->changeToEmoji($value['comments']),
                                'status' => $value['status'],
                                'evaluation_feedback_id' => $value['evaluation_feedback_id'],
                                'service_level' => $value['service_level'],
                                'dining_level' => $value['dining_level'],
                                'atmosphere_level' => $value['atmosphere_level'],
                                'price_level' => $value['price_level'],
                                'clean_level' => $value['clean_level'],
                                'invoice_image_id' => $value['invoice_image_id']
                            );
                        }
                    }
                }

                return array(
                    'restaurant_name' => $restaurant_name,
                    'evalresult' => $evalresult
                );
            }
            return array();
        }

        public function getCategoryList() {
            $get_cat_sql = "select cat.category_name,cat.main_category_id,cat.language_id,media.media_location,media.media_name from category_master cat left join media_library_master media on media.media_library_master_id=cat.category_image_id where cat.category_status ='active' and cat.is_deleted=0 order by sort_order";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($get_cat_sql);
            $stmt->execute();
            $category = $stmt->fetchAll();

            return $category;
        }

        public function getMediaUrl($media_id) {
            $media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
            $media = $media_repository->find($media_id);

            $live_path = $this->container->getParameter('live_path');
            if (!empty($media)) {
                $media_url = $live_path . $media->getMedia_location() . '/' . $media->getMedia_name();
            } else {
                $media_url = $live_path . '/bundles/Resource/default.png';
            }

            return $media_url;
        }

        public function getMediaMenuUrl($media_id) {
            $media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
            $media = $media_repository->find($media_id);

            $live_path = $this->container->getParameter('live_path');
            if (!empty($media)) {
                $media_url = $live_path . $media->getMedia_location() . '/' . $media->getMedia_name();
            } else {
                $media_url = '';
            }

            return $media_url;
        }

        public function changeToEmoji($src){
            $replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $src);
    
            $result = mb_convert_encoding($replaced, "UTF-16", "HTML-ENTITIES");
            
            $result = mb_convert_encoding($result, 'utf-8', 'utf-16');
    
            return urldecode($src);
        }

        /**
         * @Route("/view_my_evaluation")
         * @Template()
         */
        public function viewmyresevaluationAction() {
            if ($this->get('session')->get('language_id') == null) {
                $this->get('session')->set('language_id', '1');
            }
            $user_id = $this->get('session')->get('userid');

            if ($user_id != NULL) {
                $query = "SELECT evaluation_feedback.*,branch_master.branch_name,restaurant_master.restaurant_name,restaurant_master.logo_id,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_bio,user_master.user_lastname,user_master.username,user_master.user_image FROM evaluation_feedback 
				LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.main_restaurant_id 
				LEFT JOIN branch_master ON evaluation_feedback.main_branch_id=branch_master.main_branch_master_id 
				LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.food_type_master_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.category_master_id=evaluation_feedback.category_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.user_id='" . $user_id . "' and restaurant_master.language_id='" . $this->get('session')->get('language_id') . "' and branch_master.is_deleted = 0 group by evaluation_feedback.evaluation_feedback_id";
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($query);
                $stmt->execute();
                $evaluation = $stmt->fetchAll();

                $evalresult = array();
                if (!empty($evaluation)) {
                    foreach ($evaluation as $key => $value) {
                        $queryeval = "SELECT * FROM evaluation_feedback_gallery INNER JOIN evaluation_feedback ON evaluation_feedback.evaluation_feedback_id=evaluation_feedback_gallery.evaluation_feedback_id INNER JOIN restaurant_master ON restaurant_master.restaurant_master_id=evaluation_feedback.restaurant_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback_gallery.is_deleted = 0 AND evaluation_feedback.evaluation_feedback_id='" . $value['evaluation_feedback_id'] . "'";
                        $em = $this->getDoctrine()->getManager();
                        $con = $em->getConnection();
                        $stmt = $con->prepare($queryeval);
                        $stmt->execute();
                        $evaluation_gallery = $stmt->fetchAll();

                        $evalgallery = array();
                        if (!empty($evaluation_gallery)) {
                            foreach ($evaluation_gallery as $_egallery) {
                                $evalgallery[] = array('image' => $this->getMediaUrl($_egallery['media_id']), 'eval_gallery_id' => $_egallery['evaluation_feedback_gallery_id']);
                            }
                        }
                        $chk_user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->find($user_id);
                        if (!empty($chk_user)) {
                            $media_id = $value['logo_id'];
                            if (!isset($media_id) or $media_id == '') {
                                $media_id = 0;
                            }
                            $user_image = $this->getMediaUrl($media_id);

                            $evalresult[] = array(
//							'user_name' => $value['user_firstname'] . ' ' . $value['user_lastname'],
                                'user_name' => $value['user_bio'],
                                'user_image' => $user_image,
                                'category_name' => $value['category_name'],
                                'food_type_name' => $value['food_type_name'],
                                'restaurant' => $value['restaurant_name'],
                                'branch_name' => $value['branch_name'],
                                'comments' => $this->changeToEmoji($value['comments']),
                                'status' => $value['status'],
                                'reject_reason' => $value['rejected_reason'],
                                'evaluation_feedback_id' => $value['evaluation_feedback_id'],
                                'service_level' => $value['service_level'],
                                'dining_level' => $value['dining_level'],
                                'atmosphere_level' => $value['atmosphere_level'],
                                'price_level' => $value['price_level'],
                                'clean_level' => $value['clean_level'],
                                'invoice_image_id' => $value['invoice_image_id'],
                                'eval_gallery' => $evalgallery
                            );
                        }
                    }
                }
                // 			echo $user_id;
                //             echo "<pre>";print_r($evalresult);exit;
                return array(
                    'evalresult' => $evalresult
                );
            } else {
                return $this->redirectToRoute('site_default_index');
            }
            return array();
        }

        /**
         * @Route("/getFilteredFoodtype/{category_id}", defaults={"category_id":""})
         * @Template()
         */
        public function getFilteredFoodtypeAction($category_id) {
            if (isset($category_id) && $category_id != 0) {
                $rel_repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypecategoryrelation');
                $cat_foodtype_relation = $rel_repository->findBy(
                        array(
                            'main_category_id' => $category_id,
                            'is_deleted' => 0
                        )
                );
//			print_r($cat_foodtype_relation);exit;
                $html = '';
                $food_type_list = array();
                if (!empty($cat_foodtype_relation)) {
                    $repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster');

                    $unique_foodtype_rel = array();
                    if (!empty($cat_foodtype_relation)) {
                        foreach ($cat_foodtype_relation as $_relation) {
                            $unique_foodtype_rel[] = $_relation->getMain_food_type_id();
                        }
                        $unique_foodtype_rel = array_unique($unique_foodtype_rel);
                    }

                    if (!empty($unique_foodtype_rel)) {
                        foreach ($unique_foodtype_rel as $foodtype_id) {
                            $food_type = $repository->findOneBy(array('is_deleted' => 0, 'main_food_type_id' => $foodtype_id, 'language_id' => $this->get('session')->get('language_id')));
                            if (!empty($food_type)) {
                                $food_type_list[] = $food_type;
                            }
                        }
                    }

                    //$html = "<select id='first-disabled' class='selectpicker form-control' data-hide-disabled='true' data-live-search='true'>";
                    $option_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array("language_id" => $this->get('session')->get('language_id'), "is_deleted" => 0, "main_word_dictionary_id" => 149));
                    $html .= "<option value=''>" . $option_name->getWord_name() . "</option>";
                    if (!empty($food_type_list)) {
                        foreach ($food_type_list as $_food_type) {
                            $html .= "<option value='" . $_food_type->getMain_food_type_id() . "'>{$_food_type->getFood_type_name()}</option>";
                        }
                    }
                    //$html .= "</select>";
                    echo $html;
                    exit;
                }
            } else {
                $html = '';
                $food_type_list = array();
                $food_type_list_all = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster')->findBy(array('is_deleted' => 0, 'language_id' => $this->get('session')->get('language_id')));
                foreach ($food_type_list_all as $food_ty) {
                    $food_type_list[] = $food_ty;
                }
                $html .= "<option value=''>Select Food Type</option>";
                if (!empty($food_type_list)) {
                    foreach ($food_type_list as $_food_t) {
                        $html .= "<option value='" . $_food_t->getMain_food_type_id() . "'>{$_food_t->getFood_type_name()}</option>";
                    }
                }
                //$html .= "</select>";
                echo $html;
                exit;
            }
            echo 'false';
            exit;
        }

        /**
         * @Route("/searchEvaluation", name="site_restaurant_search_eval")
         */
        public function searchEvaluationAction() {
            if ($this->get('session')->get('language_id') == null) {
                $this->get('session')->set('language_id', '1');
            }
            $user_id = $this->get('session')->get('userid');

            $request = $this->getRequest();
            $year = $request->get('year');
            $keyword = $request->get('keyword');

            if ($user_id != NULL) {
                if (isset($keyword) && $keyword != '') {
                    $query = "SELECT evaluation_feedback.*,branch_master.branch_name,restaurant_master.restaurant_name,restaurant_master.logo_id,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_bio,user_master.user_lastname,user_master.username,user_master.user_image FROM evaluation_feedback 
					LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.main_restaurant_id 
					LEFT JOIN branch_master ON evaluation_feedback.main_branch_id=branch_master.main_branch_master_id  
					LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.food_type_master_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.category_master_id=evaluation_feedback.category_id
						  WHERE (restaurant_master.restaurant_name LIKE '%" . $keyword . "%' OR evaluation_feedback.comments LIKE '%" . $keyword . "%') and evaluation_feedback.is_deleted=0 AND evaluation_feedback.user_id='" . $user_id . "' and restaurant_master.language_id='" . $this->get('session')->get('language_id') . "' and branch_master.is_deleted = 0 group by evaluation_feedback.evaluation_feedback_id";
                } else {
                    $query = "SELECT evaluation_feedback.*,branch_master.branch_name,restaurant_master.restaurant_name,restaurant_master.logo_id,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_bio,user_master.user_lastname,user_master.username,user_master.user_image FROM evaluation_feedback 
					LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.main_restaurant_id 
					LEFT JOIN branch_master ON evaluation_feedback.main_branch_id=branch_master.main_branch_master_id 
					LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.food_type_master_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.category_master_id=evaluation_feedback.category_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.user_id='" . $user_id . "' and restaurant_master.language_id='" . $this->get('session')->get('language_id') . "' and branch_master.is_deleted = 0  group by evaluation_feedback.evaluation_feedback_id";
                }

                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($query);
                $stmt->execute();
                $evaluation = $stmt->fetchAll();

                $evaluation_new = array();
                if (isset($year) && $year != '') {
                    foreach ($evaluation as $key => $value) {
                        if (date('Y', strtotime($value['created_datetime'])) == $year) {
                            $evaluation_new[] = $value;
                        }
                    }
                    unset($evaluation);
                    $evaluation = $evaluation_new;
                }
//			print_r($evaluation);
                $evalresult = array();
                if (!empty($evaluation)) {
                    foreach ($evaluation as $key => $value) {
                        $queryeval = "SELECT * FROM evaluation_feedback_gallery INNER JOIN evaluation_feedback ON evaluation_feedback.evaluation_feedback_id=evaluation_feedback_gallery.evaluation_feedback_id INNER JOIN restaurant_master ON restaurant_master.restaurant_master_id=evaluation_feedback.restaurant_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback_gallery.is_deleted = 0 AND evaluation_feedback.evaluation_feedback_id='" . $value['evaluation_feedback_id'] . "'";
                        $em = $this->getDoctrine()->getManager();
                        $con = $em->getConnection();
                        $stmt = $con->prepare($queryeval);
                        $stmt->execute();
                        $evaluation_gallery = $stmt->fetchAll();

                        $evalgallery = array();
                        if (!empty($evaluation_gallery)) {
                            foreach ($evaluation_gallery as $_egallery) {
                                $evalgallery[] = $this->getMediaUrl($_egallery['media_id']);
                            }
                        }
                        $chk_user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->find($user_id);
                        if (!empty($chk_user)) {
                            $media_id = $value['logo_id'];
                            if (!isset($media_id) or $media_id == '') {
                                $media_id = 0;
                            }
                            $user_image = $this->getMediaUrl($media_id);

                            $evalresult[] = array(
//							'user_name' => $value['user_firstname'] . ' ' . $value['user_lastname'],
                                'user_name' => $value['user_bio'],
                                'user_image' => $user_image,
                                'category_name' => $value['category_name'],
                                'food_type_name' => $value['food_type_name'],
                                'restaurant' => $value['restaurant_name'],
                                'branch_name' => $value['branch_name'],
                                'comments' => $this->changeToEmoji($value['comments']),
                                'status' => $value['status'],
                                'reject_reason' => $value['rejected_reason'],
                                'evaluation_feedback_id' => $value['evaluation_feedback_id'],
                                'service_level' => $value['service_level'],
                                'dining_level' => $value['dining_level'],
                                'atmosphere_level' => $value['atmosphere_level'],
                                'price_level' => $value['price_level'],
                                'clean_level' => $value['clean_level'],
                                'invoice_image_id' => $value['invoice_image_id'],
                                'eval_gallery' => $evalgallery
                            );
                        }
                    }
                }
            }
            $html = '';
            if (!empty($evalresult)) {
                foreach ($evalresult as $eval) {
                    $html .= '<div class="panel panel-default">';
                    $html .= '<div class="panel-heading">';
                    $html .= '<div class="row">';
                    $class = 'text-warning';
                    if ($eval['status'] == 'rejected') {
                        $class = 'text-danger';
                    }
                    if ($eval['status'] == 'approved') {
                        $class = 'text-success';
                    }
                    $html .= '<div class="col-sm-8"><h4 class="' . $class . '">';
                    if ($eval['status'] == 'rejected') {
                        $html .= '<a data-toggle="collapse" href="#collapseExamplee' . $eval['evaluation_feedback_id'] . '" aria-expanded="false" aria-controls="collapseExample">' . $eval['status'] . '<span class="badge">?</span></a>';
                    } else {
                        $html .= $eval['status'];
                    }
                    $html .= '</h4></div>';

                    $edit_txt = 'Edit';
                    if ($eval['status'] == 'approved') {
                        $edit_txt = '';
                    }

                    $html .= '<div class="col-sm-4 text-right"><a href="javascript:void(0)" onclick="show_edit(' . $eval['evaluation_feedback_id'] . ')" >' . $edit_txt . '</a></div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="panel-body">';

                    if ($eval['status'] == 'rejected') {
                        $html .= '<div class="collapse" id="collapseExamplee' . $eval['evaluation_feedback_id'] . '">';
                        $html .= '<div class="well">' . $eval['reject_reason'] . '</div>';
                        $html .= '</div>';
                    }

                    $rate = ($eval['service_level'] + $eval['dining_level'] + $eval['price_level'] + $eval['clean_level'] + $eval['atmosphere_level']) / 5;
                    $html .= '<div class="row">';
                    $html .= '<div class="col-sm-2"><img src="' . $eval['user_image'] . '" class="img-responsive img-thumbnail"></div>';
                    $html .= '<div class="col-sm-10">';
                    $html .= '<h3>' . $eval['restaurant'] . '</h3>';
                    $html .= '<h5>' . $eval['branch_name'] . '</h5>';
                    $html .= '<p><strong>Rated <span class="badge">' . $rate . '</span></strong></p>';
                    $html .= '<p id="comment_' . $eval['evaluation_feedback_id'] . '">' . urldecode(preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $this->changeToEmoji($eval['comments']))) . '</p>';
                    $html .= '<div id="edit_area_' . $eval['evaluation_feedback_id'] . '"></div>';
                    $html .= '<ul class="list-inline-comment">';

                    if ($eval['eval_gallery'] != null) {
                        foreach ($eval['eval_gallery'] as $gallery) {
                            $html .= '<li><a href="' . $gallery . '" data-fancybox="comment-images"><img src="' . $gallery . '" class="img-thumbnail"></a></li>';
                        }
                    }

                    $html .= '</ul>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
            $data = array(
                'success' => 1,
                'html' => $html
            );
            echo json_encode($data);
            exit;
        }
		
        /**
         * @Route("/veiwBranch/{main_branch_id}",defaults={"main_branch_id"="0"})
		 * @Template()
         */
        public function veiwBranchAction($main_branch_id) {

			$em = $this->getDoctrine()->getManager();
			
			$sql_brach_exist = "SELECT branch.*,address_master.address_name, address_master.owner_id ,address_master.lat ,address_master.lng FROM branch_master branch LEFT JOIN address_master ON branch.branch_address_id = address_master.address_master_id WHERE branch.is_deleted = 0 AND branch.main_branch_master_id='" . $main_branch_id . "' and branch.language_id = '".$this->get('session')->get('language_id')."' ";
						
			$con = $em->getConnection();
			$stmt = $con->prepare($sql_brach_exist);
			$stmt->execute();
			$branch_data = $stmt->fetchAll();

			if (!empty($branch_data)) {
				foreach($branch_data as $key=>$value){
					
					$address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findBy(
						array(
							"main_address_id" => $value['branch_address_id'],
							'language_id' => $value['language_id']
						)
					);
					
					if(!empty($address_info)){
						foreach($address_info as $_addr){
							$branch_data[$key]['address_name'] = $_addr->getAddress_name();
						}
					}
					
					$branch_data[$key]['branch_name'] = stripslashes($value['branch_name']);		
				}
			}
//			echo"<pre>";print_r($branch_data);exit;	
			return array('branch_data'=>$branch_data);			
		}		

    /**
     * @Route("/restaurant/toggleLike")
     * @Template()
     */
    public function tooglelikeAction(Request $req) {
		$evaluation_id = $req->request->get('evaluation_id');
		$user_id = $req->request->get('user_id');
		$operation = !empty($req->request->get('operation')) ? $req->request->get('operation') : 'like';
		
			$em = $this->getDoctrine()->getManager();
			
            $response = array();
			
            $evaluation = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Evaluationfeedback')
                    ->findOneBy(array("evaluation_feedback_id"=>$evaluation_id,'is_deleted'=>0));
		
            if ($evaluation) {

			$user = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findOneBy(array("user_master_id"=>$user_id,'is_deleted'=>0));
				
				if($user){
					#check liked or not Evaluationlikerelation
					$entry_exist = $this->getDoctrine()
								->getManager()
								->getRepository('AdminBundle:Evaluationlikerelation')
								->findOneBy(array("evaluation_id"=>$evaluation_id,'user_id'=>$user_id,'operation'=>$operation));
					if($entry_exist){
						if($entry_exist->getIs_deleted() == 0){
							$entry_exist->setIs_deleted(1);
						}else{
							$entry_exist->setIs_deleted(0);
						}

						$entry_exist->setCreated_datetime(date('Y-m-d H:i:s'));
						$em->flush();
						
						if($entry_exist->getIs_deleted() == 0){
							$liked = true;							
						}else{
							$liked = false;
						}

						$evaluation_id = $entry_exist->getEvaluation_id();
						
					}else{
						#newLike
						$new_like = new Evaluationlikerelation();
						$new_like->setUser_id($user_id);
						$new_like->setEvaluation_id($evaluation_id);
						$new_like->setCreated_datetime(date('Y-m-d H:i:s'));
						$new_like->setIs_deleted(0);
						$new_like->setOperation($operation);
						
						$em->persist($new_like);
						$em->flush();
						
						$liked = true;
						$evaluation_id = $new_like->getEvaluation_id();
					}
					
					if(strtolower($operation) == 'dislike'){
						#remove like if user liked it
						$entry_exist_like = $this->getDoctrine()
							->getManager()
							->getRepository('AdminBundle:Evaluationlikerelation')
							->findOneBy(array("evaluation_id"=>$evaluation_id,'user_id'=>$user_id,"operation"=>'like'));
							
						if($entry_exist_like){
							$entry_exist_like->setIs_deleted(1);
							$em->flush();
						}	
						
					}
					
					if(strtolower($operation) == 'like'){
						#remove dislike if user disliked it
						$entry_exist_dislike = $this->getDoctrine()
							->getManager()
							->getRepository('AdminBundle:Evaluationlikerelation')
							->findOneBy(array("evaluation_id"=>$evaluation_id,'user_id'=>$user_id,"operation"=>'dislike'));
							
						if($entry_exist_dislike){
							$entry_exist_dislike->setIs_deleted(1);
							$em->flush();
						}	
						
					}
					
					#give updated count in response
					$entry_exist_like = $this->getDoctrine()
							->getManager()
							->getRepository('AdminBundle:Evaluationlikerelation')
							->findBy(array("evaluation_id"=>$evaluation_id,"operation"=>'like','is_deleted'=>0));
					$total_like = 0;			
					if($entry_exist_like){
						$total_like = count($entry_exist_like);
					}
					
					$entry_exist_dislike = $this->getDoctrine()
							->getManager()
							->getRepository('AdminBundle:Evaluationlikerelation')
							->findBy(array("evaluation_id"=>$evaluation_id,"operation"=>'dislike','is_deleted'=>0));

					$total_dislike = 0;
					
					if($entry_exist_dislike){
						$total_dislike = count($entry_exist_dislike);
					}
					#give updated count in response done
					
					$response = array('liked_flag'=>$liked,'evaluation_id'=>$evaluation_id,'total_dislike'=>$total_dislike,'total_like'=>$total_like);
					
					$this->error = "SFD";
					
				}else{
					$response = false;
				}
            }else{
				$response = false;
			}
//			exit($liked);
			if($response){
				return new Response(json_encode($response));
			}else{
				echo "false";exit;				
			}

	}

	/**
     * @Route("/restaurant/getBranchDetails/{restaurent_id}/{main_branch_id}", defaults={"restaurent_id" : "","main_branch_id"="0"})
     * @Template()
     */
    public function viewBranchAction($restaurent_id,$main_branch_id) {

        if ($this->get('session')->get('language_id') == null) {
            $this->get('session')->set('language_id', '1');
        }
		$img_html = '';
        // open soon restaurant list
        $repository = $this->getDoctrine()->getRepository('AdminBundle:Branchmaster');
        
		$branch = $repository->findOneBy([
            'main_restaurant_id' => $restaurent_id,
            'main_branch_master_id' => $main_branch_id,
			'status' => 'active',
            "language_id" => $this->get('session')->get('language_id'),
            'is_deleted' => 0
        ]);
		$adress_id = 0;
		
		if($branch){
			$adress_id = $branch->getBranch_address_id();
		}
		
        $address_master = $this->getDoctrine()
                ->getManager()
                ->getRepository("AdminBundle:Addressmaster")
                ->findOneBy(array('is_deleted' => 0, 'address_master_id' => $adress_id));
		$Addressmaster = array();
		$adress_name = '--';
		$lat = '0';
		$lng = '0';
		if(!empty($address_master)){
			$adress_name = $address_master->getAddress_name();
			$lat = $address_master->getLat();
			$lng = $address_master->getLng();
		}
		
        //review
        $main_branch_flag = false;
        if($branch){

            if($branch->getMain_branch_flag() == 1){
                $main_branch_flag = true;
                $repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedback');
                $reviewmaster = $repository->findBy([
                    'restaurant_id' => $restaurent_id,
                    'status' => 'approved',
                    'is_deleted' => 0
                        ], array('created_datetime' => 'desc'));			
            }else{
                $repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedback');
                $reviewmaster = $repository->findBy([
                    'restaurant_id' => $restaurent_id,
                    'main_branch_id' => $main_branch_id,
                    'status' => 'approved',
                    'is_deleted' => 0
                        ], array('created_datetime' => 'desc'));			
            }
    

        }

        $reviewlist[] = array();
        $reviewImages[] = array();

        if (!empty($reviewmaster)) {
            $temp_count = 0;
            foreach ($reviewmaster as $reviewmaster) {

				if($reviewmaster->getComments()){
					
					$repository = $this->getDoctrine()->getRepository('AdminBundle:Usermaster');
					$usermaster = $repository->findOneBy([
						'user_master_id' => $reviewmaster->getUser_id(),
						'is_deleted' => 0
					]);
					if ($usermaster) {
						//$username = $usermaster->getUsername();
						$username = $usermaster->getUser_firstname().' '.$usermaster->getUser_lastname();
						if($username == '' || $username == ' '){
							$option_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Worddictionary')->findOneBy(array("language_id" => $this->get('session')->get('language_id'), "is_deleted" => 0, "main_word_dictionary_id" => 301));
							if(!empty($option_name)){
								$username = $option_name->getWord_name();
							}
						}
						
						$userimage = $this->getMediaUrl($usermaster->getUser_image());

						$review_id = $reviewmaster->getEvaluation_feedback_id();
						$userreview = $reviewmaster->getComments();
						$service_level = $reviewmaster->getService_level();
						$dining_level = $reviewmaster->getDining_level();
						$atmosphere_level = $reviewmaster->getAtmosphere_level();
						$price_level = $reviewmaster->getPrice_level();
						$clean_level = $reviewmaster->getClean_level();
						$speed_level = $reviewmaster->getDelivery_speed_level();
						$packaging_level = $reviewmaster->getPackaging_level();
						
						$main_branch_id = $reviewmaster->getMain_branch_id();
						
						$totalpoint = ($service_level + $dining_level + $atmosphere_level + $price_level + $clean_level + $packaging_level + $speed_level) / 5;
						$reviewimage = array();
						$repository = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedbackgallery');
						$Reviewgallery = $repository->findBy([
							'evaluation_feedback_id' => $review_id,
							'is_deleted' => 0
						]);
						if (!empty($Reviewgallery)) {
							foreach ($Reviewgallery as $_gallery) {
								$reviewimage[] = $this->getMediaUrl($_gallery->getMedia_id());
								$reviewImages[] = $this->getMediaUrl($_gallery->getMedia_id());
							}
						}

						## get admin comment
						$admin_comment_list = $this->getDoctrine()->getRepository('AdminBundle:Evaluationadmincomment')->findBy(
								array(
									'evaluation_feedback_id' => $reviewmaster->getEvaluation_feedback_id(),
									'is_deleted' => 0
								)
						);

						//array_shift($reviewImages);

						$admin_comment = array();
						if (!empty($admin_comment_list)) {
							foreach ($admin_comment_list as $_comment) {
								$admin_comment[] = $_comment->getAdmin_comment();
							}
						}

						#branch
						$rest_branch = $this->getDoctrine()->getRepository('AdminBundle:Branchmaster')->findOneBy(
								array(
									'main_branch_master_id' => $main_branch_id,
									'is_deleted' => 0
								)
						);
						$branch_name = '';
						if($rest_branch){
							$branch_name = $rest_branch->getBranch_name();
						}
						
#get Like Count from Evaluationlikerelation
                            $likes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'like'));
                            $likes = 0;
                            if($likes_data){
                                $likes = count($likes_data);
                            }
                            $dislikes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'dislike'));
                            $dislikes = 0;
                            if($dislikes_data){
                                $dislikes = count($dislikes_data);
                            }
#get Like Count from Evaluationlikerelation ends
							$like_by_user = false;
							$dislike_by_user = false;
							if($this->get('session')->get('user_id') != NULL){

								$likes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'operation'=>'like','is_deleted'=>0,'user_id'=>$this->get('session')->get('user_id')));
								
								
								if($likes_data_user){
									$like_by_user = true;
                                }
                                
                                $dislikes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$review_id,'is_deleted'=>0,'operation'=>'dislike','user_id'=>$this->get('session')->get('user_id')));
								
								
								if($dislikes_data_user){
									$dislike_by_user = true;
								}
							
							}
							
						$reviewlist[] = array(
							'username' => $username,
							'review_id' => $review_id,
							'userreview' => urldecode($userreview),
							'userimage' => $userimage,
							'reviewimage' => $reviewimage,
							'admin_comment' => $admin_comment,
							'branch_name' => $branch_name,
							'rate' => $totalpoint,
                            'like_by_user'=>$like_by_user,
                            'dislike_by_user'=>$dislike_by_user,
                            'likes'=>$likes,
                            'dislikes'=>$dislikes,
							'created_datetime'=>$reviewmaster->getCreated_datetime()
						);
					}
				}
            }
        }
//        var_dump($reviewlist);exit;
        if (empty($branch)) {
            $repository = $this->getDoctrine()->getRepository('AdminBundle:Branchmaster');
            $branch = $repository->findOneBy([
                'main_branch_id' => $main_branch_id,
				'status' => 'active',
                'is_deleted' => 0
            ]);
        }
			$branch_eval_where = '';
			
			if(!$main_branch_flag){
				$branch_eval_where = " and evaluation_feedback.main_branch_id = '$main_branch_id' ";
			}
		
            $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $restaurent_id . "' and status='approved' and is_deleted = 0 $branch_eval_where ) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level +evaluation_feedback.delivery_speed_level + evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and evaluation_feedback.restaurant_id = " . $restaurent_id." and restaurant_master.language_id = ".$this->get('session')->get('language_id')." $branch_eval_where "; 

	
//			print($restaurant_total_evpointsQuery);exit;
            $evpoints = $this->firequery($restaurant_total_evpointsQuery);
			
			$evpoints1 = $evpoints;
            $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
            $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;

            $rate_per = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
            $rate = number_format(($rate_per * 20), 1);

		$contact_no = '--';
		$timings = '--';
		
		if(!empty($branch)){
		    if($branch->getMobile_no() == 0 || $branch->getMobile_no() == NULL || empty($branch->getMobile_no()) ){
		        $rest_info = $this->firequery("SELECT * FROM `restaurant_master` WHERE `main_restaurant_id` = ".$restaurent_id." AND `is_deleted` = 0 order by phone_number desc limit 0 ,1");
		        if(!empty($rest_info)){
		            $contact_no = $rest_info[0]['phone_number'];
		        }
		    }else{
			    $contact_no = $branch->getMobile_no();
		    }
			$timings = $branch->getTimings();
			$branch_name = $branch->getBranch_name();
        }	
        
            $repository = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Branchgallery");
            $gallery_images = $repository->findBy(
                    array(
                        'branch_id' => $main_branch_id,
                        'is_deleted' => 0
                    )
            );
    

        $live_path = $this->container->getParameter('live_path');    
        
		$gallery_files = null;
		
        if (!empty($gallery_images)) {
            foreach ($gallery_images as $_gallery_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $gallery_img = $repository->find($_gallery_image->getImage_id());

                if (!empty($gallery_img)) {

                    $img_url = $live_path."/". $gallery_img->getMedia_location()."/".$gallery_img->getMedia_name();
                    
                    $gallery_files[] = array(
                        'main_media_id' => $gallery_img->getMedia_library_master_id(),
                        'main_gallery_id' => $_gallery_image->getBranch_gallery_id(),
                        'media_name' => $gallery_img->getMedia_name(),
                        'media_location' => $gallery_img->getMedia_location(),
                        "img_url" => $img_url
                    );
                }
            }
        }


		#html making start review
		$img_html = '';
		
		if($gallery_files){
			foreach($gallery_files as $gall){
				$img_html .= '<a href="'.$gall['img_url'].'" class="photo-galleries hide" data-fancybox="photo_gallery"><img src="'.$gall['img_url'].'"></a>';
			}
		}
		$review_html = '';
		if(!empty($reviewlist)){
			foreach($reviewlist as $review){
				if(!empty($review)){

                    if($review['like_by_user']){
						$liked_btn = "liked_btn";
					}else{
						$liked_btn = "";						
                    }
                    
                    if($review['dislike_by_user']){
						$disliked_btn = "liked_btn";
					}else{
						$disliked_btn = "";						
                    }
                    
					$review_html .= '<div class="media" style="overflow:visible;">';
					$review_html .= '<div class="media-left "><a href="#"><img class="media-object img-circle" src="'.$review['userimage'].'"></a></div>';
					$review_html .= '<div class="media-body"><h4 class="media-heading">'.$review['username'].'</h4><span style="float:right"><b id="liked_count_'.$review['review_id'].'">'.$review['likes'].'</b>&nbsp;&nbsp;<i class="fa fa-thumbs-up '.$liked_btn.'" style="cursor:pointer" onclick="toggleLike('.$review['review_id'].',\'like\')" id="liked_btn_'.$review['review_id'].'"></i>&nbsp;&nbsp;<b id="disliked_count_'.$review['review_id'].'">'.$review['dislikes'].'</b>&nbsp;&nbsp;<i class="fa fa-thumbs-down '.$disliked_btn.'" style="cursor:pointer" onclick="toggleLike('.$review['review_id'].',\'dislike\')" id="disliked_btn_'.$review['review_id'].'"></i></span>';
												
					$review_html .= '<p><strong>Rated <span class="badge">'.$review['rate'].'</span></strong></p>';
					$review_html .= '<p style="white-space:pre;">'.$review['userreview'].'</p>';
					$review_html .='<ul class="list-inline-comment">';
					if(!empty($review['reviewimage'])){
						foreach($review['reviewimage'] as $img_review_1){
							
							$review_html .= ' <li><a href="'.$img_review_1.'" data-fancybox="comment-imagesUser Tester"><img src="'.$img_review_1.'" class="img-thumbnail" style="height:65px;"></a></li>';
						}
					}
					$review_html .= '</ul>';
					$review_html .= '<p class="text-right">'.date('d-m-Y',strtotime($review['created_datetime'])).'</p>';
					$review_html .= '</div>';
					
					
                    
//					$review_html .= '<div class="media-left">';
                    
//                    $review_html .= '';
					$review_html .='</div>';
				}
			}
		}

		$response = array('branch'=>$branch,'gallery_files'=>$gallery_files,'img_html'=>$img_html,'reviewlist'=>$reviewlist,'adress'=>$Addressmaster,'rate'=>$rate,'total_rate'=>$total_rate,'evpoints'=>$evpoints,'rate_per'=>$rate_per,'review_html'=>$review_html,'contact_no'=>$contact_no,'timings'=>$timings,'branch_name'=>$branch_name,'evpoints1'=>$evpoints1,'adress_name'=>$adress_name,'lat'=>$lat,'lng'=>$lng);	
		
		#html making ends
//		echo"<pre>";print_r($response);exit;
		
		return new Response(json_encode($response));
    }
	
		
    }
    