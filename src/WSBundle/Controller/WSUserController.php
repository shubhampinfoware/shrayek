<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Usermaster;

class WSUserController extends WSBaseController {
	
	/**
	* @Route("/ws/update_profile/{param}",defaults = {"param"=""},requirements={"param"=".+"})
	*
	*/
	public function update_profileAction($param){
//		try{
			$this->title = "Update Profile";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(),
				),
			);
			if($this->validateData($param)){
				$response = array();
				$user_id = $param->user_id;
				
				if(isset($user_id) && $user_id != ''){
                                    $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
					$em = $this->getDoctrine()->getManager();
					$user = $em->getRepository('AdminBundle:Usermaster')->find($user_id);
                                        if(!empty($param->nick_name)){
                                            $nikname = $param->nick_name;
                                            $st = $conn->prepare("SELECT * FROM user_master WHERE user_bio != '$nikname' and user_master_id=$user_id");
                                            $st->execute();
                                            $shopList = $st->fetchAll();
                                            if(!empty($shopList)){
                                                $st1 = $conn->prepare("SELECT * FROM user_master WHERE user_bio = '$nikname'");
                                                $st1->execute();
                                                $shopList1 = $st1->fetchAll();
                                                 if(!empty($shopList1)){
                                                    $this->error = "UNAE";   
                                                    $this->data = false;
                                                    return $this->responseAction();
                                                }
                                            }
                                        }
					
					if(!empty($user)){
						
						if(isset($_FILES['image']) && !empty($_FILES['image'])){
							$media_id = '';
							
							$file = $_FILES['image']['name'];
							$extension = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
							
							if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
								$tmpname = $_FILES['image']['tmp_name'];
								$file_path = $this->container->getParameter('file_path');
								$path = $file_path . '/uploads/WSimages';
								$upload_dir = $this->container->getParameter('upload_dir1') . '/design/uploads/WSimages/';
								
								$media_id = $this->mediauploadAction($file, $tmpname, $path, $upload_dir, 1);
								$media_url = $this->getimage($media_id);
							}
						}
						
						if(isset($param->firstname)) {
							$user->setUser_firstname($param->firstname);
						}
						if(isset($param->lastname)) {
							$user->setUser_lastname($param->lastname);
						}
						if(isset($param->nick_name)) {
							$user->setUser_bio($param->nick_name);
						}
						if(isset($param->email)) {
							
							$userIsExist = $this->getDoctrine()
								->getManager()
								->getRepository("AdminBundle:Usermaster")
								->findOneBy(
									array(
										"email"=>$param->email,
										"status"=>"active",
										"is_deleted"=>0,
									)
								);
							if(empty($userIsExist) || $userIsExist->getUser_master_id()==$user_id){
								$user->setEmail($param->email);
								$user->setUsername($param->email);
							}else{
								$this->error = "EAE" ;
							}
						}
						if(isset($param->password)) {
							$user->setPassword(md5($param->password));
							$user->setShow_password($param->password);
						}
						if(isset($param->user_mobile)) {
							$user->setUser_mobile($param->user_mobile);
						}
						if(isset($media_id) && $media_id != ''){
							$user->setUser_image($media_id);
						}
						if(isset($param->user_gender)) {
							$user->setUser_gender($param->user_gender);
						}
						if(isset($param->date_of_birth)) {
							$user->setDate_of_birth(date('Y-m-d', $param->date_of_birth/1000));
						}
						//echo date('Y-m-d', $param->date_of_birth/1000);
						$em->flush();
						$latest_address_info= $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted"=>0,"owner_id"=>$user->getUser_master_id(),"base_address_type"=>'primary')); 
						$address_list = NULL ;        
						if(!empty($latest_address_info)  && $latest_address_info != NULL){
							$address_list = $this->getaddressAction($latest_address_info->getAddress_master_id(),$language_id);
						}
						if (!empty($address_list)) {
							$userAddress = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy( array("address_master_id" => $user->getAddress_master_id(),"is_deleted" => 0) );
						}  

						$response = array(
							"usermaster_id" => $user->getUser_master_id(),
							"nick_name" => $user->getUser_bio(),
							"user_firstname" => $user->getUser_firstname(),
							"user_lastname" => $user->getUser_lastname(),
							"user_mobile" => $user->getUser_mobile(),
							"user_image" => $this->getimage($user->getUser_image()),
							"role_id" => $user->getUser_role_id(),
							"user_emailid" => $user->getEmail(),
							"date_of_birth" => strtotime($user->getDate_of_birth())*1000,
							// "user_phoneno" => $this->keyDecryptionAction($userMaster->getUser_mobile()),
							"username" => $user->getUsername(),
							"language" => $user->getCurrent_lang_id(),
							"user_gender" => $user->getUser_gender(),
							"domain_id" => $user->getDomain_id(),
							"address" => $address_list
						);

						//$response = $user_id;
						$this->error = "SFD";
					} else {
						$this->error = "UNE";
						$response = false;
					}
				}
			}
			
			$this->data = $response;
			return $this->responseAction();
//		} catch (\Exception $e) {
//			$this->error = "SFND";
//			$this->data = false;
//			return $this->responseAction();
//		}
	}
	
	/**
	* @Route("/ws/site_member_count/{param}",defaults = {"param"=""},requirements={"param"=".+"})
	*
	*/
	public function site_member_countAction($param)
	{	
	   try{
			$this->title = "Site Members";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(),
				),
			);
			if($this->validateData($param)){
				$response = array();
				$data = 0;
				$site_members = '';
				
				$site_members = $this->getDoctrine()
						->getManager()
						->getRepository('AdminBundle:Usermaster')
						->findBy(array('user_role_id' => 3, 'is_deleted' => 0));
				
				if(!empty($site_members)){
					$response = $data = count($site_members);
				}
				
				if (!empty($data)) {
					$response = $data;
					$this->error = "SFD";
				}
				if (empty($response)) {
					$response = false;
					$this->error = "NRF";
				}
				
				$this->data = $response;
				return $this->responseAction();
			}
		} catch (\Exception $e) {
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}
	}
}
