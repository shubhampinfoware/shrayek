<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Membershipdetails;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\Usersetting;
use AdminBundle\Entity\Apppushnotificationmaster;
use AdminBundle\Entity\Addressmaster;

class WSSiteMemberHighlightedController extends WSBaseController {

    /**
     * @Route("/ws/sitememberhighlighted/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function sitememberhighlightedAction($param) {
        /* try
          { */
        $this->title = "Site Members";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );
       
     
      $hilighted = "SELECT count(ef.user_id) as ecount,ef.*,um.user_bio,um.user_firstname,um.user_lastname,media_library_master.media_location , media_library_master.media_name from evaluation_feedback as ef 
		left join user_master as um on um.user_master_id=ef.user_id 
		left join media_library_master ON um.user_image = media_library_master.media_library_master_id 
		where ef.is_deleted=0 and ef.status = 'approved' group by ef.user_id order by count(ef.user_id) desc limit 0,10"; 
       $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($hilighted);
        $stmt->execute();
        $hilighted_evalution = $stmt->fetchAll();
        $live_path = $this->container->getParameter('live_path'); ;
	$response=array();	
        if(!empty($hilighted_evalution)){
            foreach($hilighted_evalution  as $key=>$val){
				if($val['media_location']!=='' && $val['media_name']!='')
					$user_image=$live_path . $val['media_location'] . "/".$val['media_name'];
				else
					$user_image=$live_path.'/bundles/Resource/default.png';
                $response[] = array(
                    "user_id"=>$val['user_id'],
                    "userimage"=>$user_image,
                    "username"=>$val['user_bio'],
		    "max_counter"=>$val['ecount']
                );
            }
             $this->error = "SFD";
        }else{
             $this->error = "NRF";
        }
               
                   
        
        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
        /* }
          catch(\Exception $e)
          {
          $this->error = "SFND" ;
          $this->data = false ;
          return $this->responseAction() ;
          } */
    }

}

?>
