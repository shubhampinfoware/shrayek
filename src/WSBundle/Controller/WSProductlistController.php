<?php



namespace WSBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\DependencyInjection\ContainerAware;

use Symfony\Component\PropertyAccess\PropertyAccess;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Routing\RouterInterface;

use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use AdminBundle\Entity\Productmaster;

use AdminBundle\Entity\Votemaster;

use AdminBundle\Entity\Attributecategoryrelation;



class WSProductlistController extends WSBaseController {



    /**

     * @Route("/ws/product_list/{param}",defaults =

      {"param"=""},requirements={"param"=".+"})

     *

     */

    public function product_listAction($param) {

        /* try{ */

        $this->title = "Product List";

        $param = $this->requestAction($this->getRequest(), 0);

        $this->validateRule = array(

            array(

                'rule' => 'NOTNULL',

                'field' => array(),

            ),

        );

        if ($this->validateData($param)) {

            $language_id = 1;

            if (!empty($param->language_id))

                $language_id = $param->language_id;

            if (!empty($param->user_id))

                $user_id = $param->user_id;

            $response = array();

            $data = $all_category = '';



            $all_category = $this->getDoctrine()

                    ->getManager()

                    ->getRepository('AdminBundle:Productmaster')

                    ->findBy(array('is_deleted' => 0, 'language_id' => $language_id, 'product_status' => 'active'),array("product_master_id"=>"DESC"));

		

            if (count($all_category) > 0) {



                foreach (array_slice($all_category, 0) as $lkey => $lval) {

                    $productvote = null;

                    $productvotecount = $this->getDoctrine()

                            ->getManager()

                            ->getRepository('AdminBundle:Votemaster')

                            ->findBy(array('is_deleted' => 0, 'product_id' => $lval->getMain_product_id()));

                    

                    $excellent = $bad = $medium = 0;

                    $vote_excellent = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Votemaster')->findBy(array("is_deleted" => 0, "product_id" => $lval->getMain_product_id(), "vote_type" => 'excellent'));

                    $vote_medium = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Votemaster')->findBy(array("is_deleted" => 0, "product_id" =>$lval->getMain_product_id(), "vote_type" => 'medium'));

                    $vote_bad = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Votemaster')->findBy(array("is_deleted" => 0, "product_id" =>$lval->getMain_product_id(), "vote_type" => 'bad'));



                    $tot_vote_count = count($vote_excellent) + count($vote_medium) + count($vote_bad);



                    if (count($vote_excellent) > 0) {

                       // $excellent = round((count($vote_excellent) / $tot_vote_count ) * 100);

                        $excellent = round(( 100 /$tot_vote_count)) * count($vote_excellent);

                    }

                    if (count($vote_medium) > 0) {

                       // $medium = round((count($vote_medium) / $tot_vote_count ) * 100);

                        $medium = round(( 100 /$tot_vote_count)) * count($vote_medium);

                    }

                    if (count($vote_bad) > 0) {

                        //$bad = round((count($vote_bad) / $tot_vote_count ) * 100);

                        $bad = round(( 100 /$tot_vote_count)) * count($vote_bad);

                    }

                    if (!(( $bad + $medium + $excellent) == 100 )) {

                        $rem = 100 - ($bad + $medium + $excellent);

                        if ($excellent != 0)

                            $excellent = $excellent + $rem;

                    }

                    if(!empty($user_id)){

                        $productvot = $this->getDoctrine()

                            ->getManager()

                            ->getRepository('AdminBundle:Votemaster')

                            ->findBy(array('is_deleted' => 0, 'product_id' => $lval->getMain_product_id(),'user_id'=>$user_id));

                    }

                    $data[] = array("product_id" => $lval->getMain_product_id(),

                        "product_name" => $lval->getProduct_name(),

                        "product_image" => $this->getimage($lval->getProduct_image_id()),

                        "product_description" => trim($lval->getProduct_description()),

                        "status" => array(array('name' => 'excellent', 'percentage' => $excellent . '%'), array('name' => 'medium', 'percentage' => $medium . '%'), array('name' => 'bad', 'percentage' => $bad . '%')),

                        "is_voted" => !empty($productvot) ? true : false,

                    );

                }

            }

            if (!empty($data)) {

                $response = $data;

                $this->error = "SFD";

            }

            if (empty($response)) {

                $response = false;

                $this->error = "NRF";

            }

            $this->data = $response;

        } else {

            $this->error = "PIM";

        }

        if (empty($response)) {

            $response = False;

        }

        return $this->responseAction();

        /* }

          catch (\Exception $e)

          {

          $this->error = "SFND";

          $this->data = false;

          return $this->responseAction();

          } */

    }



}



?>