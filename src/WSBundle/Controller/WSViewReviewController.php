<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Usermaster;

class WSViewReviewController extends WSBaseController {

	/**
	 * @Route("/ws/view_reviews/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function view_reviewAction($param)
	{
	   /*try{*/
			$this->title = "View Reviews";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('user_id'),
				),
			);
			if($this->validateData($param)){
				if(isset($param->shop_id) && !empty($param->shop_id)){
					$user_id = $param->user_id;
					$rest_id = $param->shop_id;

					$get_review_sql = "select review.*,rest.restaurant_name,rest.main_restaurant_id,user.username,user.user_master_id 
									   from review_master review join restaurant_master rest on review.restaurant_id=rest.main_restaurant_id
									   join user_master user on review.created_by=user.user_master_id 
									   where user.is_deleted=0 and review.is_deleted =0 and rest.is_deleted=0 and review.created_by='".$user_id."' and restaurant_id='".$rest_id."'";
					$em = $this->getDoctrine()->getManager()->getConnection()->prepare($get_review_sql);
					$em->execute();
					$review = $em->fetchAll();
				}else{
					
					$user_id = $param->user_id;
					$get_review_sql = "select review.*,rest.restaurant_name,rest.main_restaurant_id,user.username,user.user_master_id 
									   from review_master review join restaurant_master rest on review.restaurant_id=rest.main_restaurant_id
									   join user_master user on review.created_by=user.user_master_id 
									   where user.is_deleted=0 and review.is_deleted =0 and rest.is_deleted=0 and review.created_by='".$user_id."'";
									   
					$em = $this->getDoctrine()->getManager()->getConnection()->prepare($get_review_sql);
					$em->execute();
					$review = $em->fetchAll();
				}
				if(count($get_review_sql) > 0)
				{
					foreach($review as $review_data)
					{
						$get_review_sql = "select media_id from review_gallery where review_id='".$review_data['review_master_id']."'";
						$em = $this->getDoctrine()->getManager()->getConnection()->prepare($get_review_sql);
						$em->execute();
						$review_gallary = $em->fetchAll();
						$photos = array();
						if($review_gallary){	
							foreach($review_gallary as $review_gallary_img)
								{
									$photos [] = $this->getimage($review_gallary_img['media_id']);
								}
						}

						$data[]= array("review_id"=> $review_data['review_master_id'],
						"user_name"=>$review_data['user_bio'],
						"created_date"=>$review_data['created_datetime'],
						"photos"=>$photos
						);
						unset($photos);
					}

				}
				if(!empty($data))
				{
					$response = $data;
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}
	/**
	 * @Route("/ws/shopofferlist/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function shopofferlistAction($param)
	{
	   /*try{*/
			$this->title = "Shop Offer List";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('language_id','shop_id'),
				),
			);
			if($this->validateData($param)){
				$language_id = $param->language_id;
				$userid = isset($param->user_id)?$param->user_id:'';
				$shop_id = $param->shop_id;
				$response = array();
				$data = $all_category = '';
				$all_category = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Shopoffermaster')
							   ->findBy(array('language_id'=>$language_id,'is_deleted'=>0,'shop_id'=>$shop_id,'domain_id'=>'1'));
				if(count($all_category) > 0 )
				{
					foreach(array_slice($all_category,0) as $lkey=>$lval)
					{
						$data[]= array("offer_id"=> $lval->getOffer_master_id(),
						"offer_desc"=>$lval->getDescription(),
						"expire_date"=>$lval->getExpire_date(),
						"offer_image"=>$this->getimage($lval->getImage_id()),
						"language_id"=>$language_id
						);

					}

				}
				if(!empty($data))
				{
					$response = $data;
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}
	/**
	 * @Route("/ws/deleteoffer1/{param}",defaults ={"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function deleteoffer1Action($param)
	{
	   /*try{*/
			$this->title = "My Offer List";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('language_id','shop_id','offer_id','offer_type'),
				),
			);
			if($this->validateData($param)){
				$language_id = $param->language_id;
				$offer_id = isset($param->offer_id)?$param->offer_id:'';
				$shop_id = $param->shop_id;
				$response = array();
				$data = $all_category = '';
				if($offer_type == 'shop'){

					$all_category = $this->getDoctrine()
								   ->getManager()
								   ->getRepository('AdminBundle:Shopoffermaster')
								   ->findOneBy(array('language_id'=>$language_id,'is_deleted'=>0,'shop_id'=>$shop_id,'domain_id'=>'1','offer_master_id'=>$offer_id));
					if(count($all_category) > 0 )
					{
						$all_category->setIs_deleted(1);
						$em = $this->getDoctrine()->getManager();
						$em->persist($all_category);
						$em->flush();
						$data=array('shop_id'=>$shop_id);
						$response = $data;
						$this->error = "SFD" ;
					}

				}
				if($offer_type == 'category'){

					$all_category = $this->getDoctrine()
								   ->getManager()
								   ->getRepository('AdminBundle:Categoryoffermaster')
								   ->findOneBy(array('language_id'=>$language_id,'is_deleted'=>0,'shop_id'=>$shop_id,'domain_id'=>'1','offer_master_id'=>$offer_id));
					if(count($all_category) > 0 )
					{
						$all_category->setIs_deleted(1);
						$em = $this->getDoctrine()->getManager();
						$em->persist($all_category);
						$em->flush();
						$data=array('shop_id'=>$shop_id);
						$response = $data;
						$this->error = "SFD" ;
					}

				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}
	/**
	 * @Route("/ws/myofferlist/{param}",defaults ={"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function myofferlistAction($param)
	{
	   /*try{*/
			$this->title = "My Offer List";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('language_id'),
				),
			);
			if($this->validateData($param)){
				$language_id = $param->language_id;
				$category_id = isset($param->category_id)?$param->category_id:'';
				$shop_id = isset($param->shop_id)?$param->shop_id:'';
				$response = array();
				$data = $all_category = '';

				$shop_master = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Usermaster')
							   ->findOneBy(array('user_type'=>'shop','is_deleted'=>0,'user_master_id'=>$shop_id,'domain_id'=>'1'));

				$all_category = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Shopoffermaster')
							   ->findBy(array('language_id'=>$language_id,'is_deleted'=>0,'shop_id'=>$shop_id,'domain_id'=>'1'));
				if(count($all_category) > 0 )
				{
					$offers=null;
					foreach(array_slice($all_category,0) as $lkey=>$lval)
					{
						$favourite = false;
									if(!empty($userid)){
										$shop_master = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Userfavouriteoffer')
												->findOneBy(array('offer_type'=>'shop','is_deleted'=>0,'offer_id'=>$lval->getOffer_master_id(),'user_id'=>$userid));
										if(!empty($shop_master)){
											$favourite = true;
										}else{
											$favourite = false;
										}
									}
						$offers[]= array("offer_id"=> $lval->getOffer_master_id(),
						"offer_desc"=>$lval->getDescription(),
						"expire_date"=>$lval->getExpire_date(),
						"offer_image"=>$this->getimage($lval->getImage_id()),
						"favourite"=>$favourite,
						"language_id"=>$language_id
						);

					}

				}

				    if($language_id == 1){
						$shop_name = $shop_master->getName_of_shop();
					}
					if($language_id == 2){
						$shop_name = $shop_master->getName_of_shop_ar();
					}
					$data=array('shop_id'=>$shop_master->getUser_master_id(), "allow_user_to_message" => $shop_master->getAllow_user_to_message(),
                                                                        "allow_user_to_call" => $shop_master->getAllow_user_to_call(),'shop_image'=>$this->getimage($shop_master->getUser_image()),'shop_name'=>$shop_name,'shop_contact_number'=>$shop_master->getMobile_number(),'shop_offers'=>$offers);
					$response = $data;
					$this->error = "SFD" ;

				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

	/**
	 * @Route("/ws/categoryofferlist/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function categoryofferlistAction($param)
	{
	   /*try{*/
			$this->title = "Category Offer List";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('language_id'),
				),
			);
			if($this->validateData($param)){
				$language_id = $param->language_id;
				$category_id = !empty($param->category_id)?$param->category_id:'';
				$userid = isset($param->user_id)?$param->user_id:'';
				$response = array();
				$data = $all_category = $data1='';
				$shop_master_arr=null;
				if(!empty($userid)){
					$all_category1 = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Shopoffermaster')
							   ->findBy(array('language_id'=>$language_id,'is_deleted'=>0,'shop_id'=>$userid,'domain_id'=>'1'));
				}else{
					$all_category1 = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Shopoffermaster')
							   ->findBy(array('language_id'=>$language_id,'is_deleted'=>0,'domain_id'=>'1'));
				}
				$data1=array();
				/*if(count($all_category1) > 0 && empty($category_id))
				{
					foreach(array_slice($all_category1,0) as $lkey=>$lval)
					{
						$shop_id=$lval->getShop_id();
						$shop_master_data = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Usermaster')
							   ->findOneBy(array('user_type'=>'shop','is_deleted'=>0,'user_master_id'=>$shop_id,'domain_id'=>'1'));
						$favourite = false;
									if(!empty($userid)){
										$shop_master = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Userfavouriteoffer')
												->findOneBy(array('offer_type'=>'shop','is_deleted'=>0,'offer_id'=>$lval->getOffer_master_id(),'user_id'=>$userid));
										if(!empty($shop_master)){
											$favourite = true;
										}else{
											$favourite = false;
										}
									}
									 if($language_id == 1){
										$shop_name = !empty($shop_master_data)?$shop_master_data->getName_of_shop():'';
									}
									if($language_id == 2){
										$shop_name = !empty($shop_master_data)?$shop_master_data->getName_of_shop_ar():'';
									}
									$shop_master_arr=null;
									if(!empty($shop_master_data)){
										$shop_master_arr = array("shop_id"=>$shop_master_data->getUser_master_id(),'shop_image'=>$this->getimage($shop_master_data->getUser_image()),'shop_name'=>$shop_name,'shop_contact_number'=>$shop_master_data->getMobile_number());
									}
								$data1[]= array("offer_id"=> $lval->getOffer_master_id(),
								"offer_desc"=>$lval->getDescription(),
								'offer_type'=>'shop',
								"expire_date"=>$lval->getExpire_date(),
								"offer_image"=>$this->getimage($lval->getImage_id()),
								"language_id"=>$language_id,
								"favourite"=>$favourite,
								"shop"=>$shop_master_arr,
								'allow_user_to_message'=>$lval->getAllow_user_to_message(),
								'allow_user_to_call'=>$lval->getAllow_user_to_call(),
								);

					}

				}*/
				if(!empty($category_id)){
					$sql2= "SELECT * FROM category_offer_master WHERE language_id='$language_id' AND is_deleted='0' AND domain_id='1' AND category_id='$category_id' AND expire_date >= '".date('Y-m-d')."'  order by offer_master_id DESc";
/*
					$all_category = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Categoryoffermaster')
							   ->findBy(array('language_id'=>$language_id,'is_deleted'=>0,'category_id'=>$category_id,'domain_id'=>'1'),array(),array('offer_master_id','desc'));
*/
				}else{
					$sql2= "SELECT * FROM category_offer_master WHERE language_id='$language_id' AND is_deleted='0' AND domain_id='1' AND expire_date >= '".date('Y-m-d')."' order by offer_master_id DESc";
					/*
					$all_category = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Categoryoffermaster')
							   ->findBy(array('language_id'=>$language_id,'is_deleted'=>0,'domain_id'=>'1'),null,array('offer_master_id','desc'));
*/
				}
				$emp = $this->getDoctrine()->getManager()->getConnection()->prepare($sql2);
				$emp->execute();
				$all_category = $emp->fetchAll();

				if(!empty($all_category))
				{
					foreach($all_category as $lval)
					{
						if(!empty($lval['shop_id']))
						{
							$shop_master_data = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Usermaster')
							   ->findOneBy(array('user_type'=>'shop','is_deleted'=>0,'user_master_id'=>$lval['shop_id'],'domain_id'=>'1'));

									$shop_name = !empty($shop_master_data)?$shop_master_data->getName_of_shop():'';
									$shop_description =  !empty($shop_master_data)?$shop_master_data->getShop_description_en():'';
									$shop_master_arr=null;
									if(!empty($shop_master_data)){
										$shop_master_arr = array("shop_id"=>$shop_master_data->getUser_master_id(),"allow_user_to_message" => $shop_master_data->getAllow_user_to_message(),
                                                                        "allow_user_to_call" => $shop_master_data->getAllow_user_to_call(),'shop_image'=>$this->getimage($shop_master_data->getUser_image()),'shop_name'=>$shop_name,'description'=>$shop_description,'shop_contact_number'=>$shop_master_data->getMobile_number());
									}
						}

						$favourite = false;
									if(!empty($userid)){
										$shop_master = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Userfavouriteoffer')
												->findOneBy(array('offer_type'=>'category','is_deleted'=>0,'offer_id'=>$lval['offer_master_id'],'user_id'=>$userid));
										if(!empty($shop_master)){
											$favourite = true;
										}else{
											$favourite = false;
										}
									}
						$getofferdesc = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Categoryoffermaster')
												->findOneBy(array('is_deleted'=>0,'offer_master_id'=>$lval['offer_master_id'],'language_id'=>2));

						$data[]= array("offer_id"=> $lval['offer_master_id'],
						"offer_desc"=>$lval['description'],
						"offer_desc_ar"=>!empty($getofferdesc)?$getofferdesc->getDescription():'',
						"offer_type"=>"category",
						"expire_date"=>$lval['expire_date'],
						"offer_image"=>$this->getimage($lval['image_id']),
						"favourite"=>$favourite,
						"language_id"=>$language_id,
						"shop"=>$shop_master_arr,
						'allow_user_to_message'=>$lval['allow_user_to_message'],
						'allow_user_to_call'=>$lval['allow_user_to_call'],
                                                'from_admin'=>true
						);

					}

				}
				if(!empty($category_id)){
					$all_category3 = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Postadsmaster')
							   ->findBy(array('lang_id'=>$language_id,'is_deleted'=>0,'category_id'=>$category_id,'domain_id'=>'1'));
				}else{
					$all_category3 = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Postadsmaster')
							   ->findBy(array('lang_id'=>$language_id,'is_deleted'=>0,'domain_id'=>'1'));
				}
				$data3=array();
				/*if(count($all_category3) > 0 )
				{
					foreach(array_slice($all_category3,0) as $lkey3=>$lval3)
					{
						if(!empty($lval3->getUser_id())){
							$shop_master_data = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Usermaster')
							   ->findOneBy(array('user_type'=>'shop','is_deleted'=>0,'user_master_id'=>$lval3->getUser_id(),'domain_id'=>'1'));
							    if($language_id == 1){
										$shop_name = !empty($shop_master_data)?$shop_master_data->getName_of_shop():'';
									}
									if($language_id == 2){
										$shop_name = !empty($shop_master_data)?$shop_master_data->getName_of_shop_ar():'';
									}
									$shop_master_arr=null;
									if(!empty($shop_master_data)){
										$shop_master_arr = array("shop_id"=>$shop_master_data->getUser_master_id(),'shop_image'=>$this->getimage($shop_master_data->getUser_image()),'shop_name'=>$shop_name,'shop_contact_number'=>$shop_master_data->getMobile_number());
									}
						}

						$favourite = false;
									if(!empty($userid)){
										$shop_master = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Userfavouriteoffer')
												->findOneBy(array('offer_type'=>'post','is_deleted'=>0,'offer_id'=>$lval3->getMain_postads_master_id(),'user_id'=>$userid));
										if(!empty($shop_master)){
											$favourite = true;
										}else{
											$favourite = false;
										}
									}
						$data3[]= array("offer_id"=> $lval3->getMain_postads_master_id(),
						"offer_desc"=>$lval3->getPost_description(),
						"offer_type"=>"post",
						"expire_date"=>'',
						"offer_image"=>$this->getimage($lval3->getPost_image_id()),
						"favourite"=>$favourite,
						"language_id"=>$language_id,
						"shop"=>$shop_master_arr,
						'allow_user_to_message'=>$lval3->getAllow_user_to_message(),
						'allow_user_to_call'=>$lval3->getAllow_user_to_call(),
						);

					}

				}*/
				if(!empty($data))
				{
					$response = array_merge($data,$data1,$data3);
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}
}
?>
