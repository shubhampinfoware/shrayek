<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Aboutus;

class WSEvaluationGalleryListingController extends WSBaseController {

    /**
     * @Route("/ws/evaluationgallery/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function evaluationGalleryAction($param) {
        try {
            $this->title = "Evaluation Gallery Listing";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {

                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();

                $evaluation_id_array = array();
                if (1) {
                    if (array_key_exists('restaurant_id', $param)) {
                        $restaurant_id = $param->restaurant_id;
                    }
                    if (array_key_exists('evaluation_feedback_id', $param)) {
                        $evaluation_feedback_id = $param->evaluation_feedback_id;
                    }

                    $filter = array();
                    $filter['is_deleted'] = 0;
                    $where = '';
                    if (isset($restaurant_id)) {
                        $where .= " restaurant_id='" . $restaurant_id . "' and ";
                    }
                    if (isset($evaluation_feedback_id)) {
                        $where .= " evaluation_feedback_id='" . $evaluation_feedback_id . "' and ";
                    }

                    $sql_evaluation_feedback = "select evaluation_feedback_id,restaurant_id from evaluation_feedback where $where is_deleted=0 and status != 'rejected' and status != 'pending' and status != 'under_evaluation' group by restaurant_id";

                    $con = $this->getDoctrine()->getManager()->getConnection();
                    $stmt = $con->prepare($sql_evaluation_feedback);
                    $stmt->execute();
                    $evaluations = $stmt->fetchAll();

//					print_r($sql_evaluation_feedback);exit;					

                    if (!empty($evaluations)) {
                        foreach ($evaluations as $evaluations_data) {
                            $media_gallery = array();
                            if (isset($evaluation_feedback_id)) {
                                $sql_evaluation_feedback_rest_wise = "select eval_gallery.media_id,eval.evaluation_feedback_id,eval.restaurant_id from evaluation_feedback eval JOIN evaluation_feedback_gallery eval_gallery ON eval.evaluation_feedback_id=eval_gallery.evaluation_feedback_id where eval_gallery.evaluation_feedback_id='" . $evaluation_feedback_id . "' and eval.restaurant_id='" . $evaluations_data['restaurant_id'] . "' and eval.is_deleted=0 and eval_gallery.is_deleted=0 and  eval.status = 'approved'";
                            } else {
                                $sql_evaluation_feedback_rest_wise = "select eval_gallery.media_id,eval.evaluation_feedback_id,eval.restaurant_id from evaluation_feedback eval JOIN evaluation_feedback_gallery eval_gallery ON eval.evaluation_feedback_id=eval_gallery.evaluation_feedback_id where eval.restaurant_id='" . $evaluations_data['restaurant_id'] . "' and eval.is_deleted=0 and eval_gallery.is_deleted=0 and  eval.status = 'approved'";
                            }
                           // print_r($sql_evaluation_feedback_rest_wise);exit;
                            $con = $this->getDoctrine()->getManager()->getConnection();
                            $stmt = $con->prepare($sql_evaluation_feedback_rest_wise);
                            $stmt->execute();
                            $evaluations_restaraunt_wise = $stmt->fetchAll();

                            if ($evaluations_restaraunt_wise) {
								
                                foreach ($evaluations_restaraunt_wise as $eval_gallery_table) {
                                    $media_url = $this->getMediaUrl($eval_gallery_table['media_id']);
                                    if (!empty($media_url)) {
                                        $media_gallery [] = $media_url;
                                    }
                                }
                            }
                            if (!empty($media_gallery)) {
                                $response = $media_gallery;
                            }
                            unset($media_gallery);
                            //	$this->error = "SFD";					 
                        }
                    }
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }

            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            if (!empty($response)) {
                $this->error = "SFD";
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND" . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }

    public function getMediaUrl($media_id) {
        $data = array();
        if (isset($media_id) && $media_id != '') {
            $media = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster')->find($media_id);

            if (!empty($media)) {
                $live_path = $this->container->getParameter('live_path');

                $media_url = $live_path . '/' . $media->getMedia_location() . '/' . $media->getMedia_title();

                $data = array(
                    'media_id' => $media->getMedia_library_master_id(),
                    'media_url' => $media_url
                );
            }
        }

        return $data;
    }

}

?>