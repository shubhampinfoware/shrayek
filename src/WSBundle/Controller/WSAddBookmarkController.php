<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Bookmarkmaster;
use AdminBundle\Entity\Diningexperiencegallery;
use AdminBundle\Entity\Attributecategoryrelation;
class WSAddBookmarkController extends WSBaseController {
	
	/**
	 * @Route("/ws/addbookmark/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	   public function addbookmarkAction($param)
	{
	   /*try{*/
			$this->title = "Add Book Mark";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('shop_id','user_id','type'),
				),
			);
			if($this->validateData($param)){
				$shop_id = $param->shop_id;
                                $user_id = $param->user_id;
                                $type = $param->type;
				$em = $this->getDoctrine()->getManager();
				$conn = $em->getConnection();
				$faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
					->findOneBy(array(
						'shop_id'=>$shop_id,
                                                'user_id'=>$user_id,
            
						'is_deleted'=>0
					)
				);
                                if(empty($faq_info)){
                                $dining_experience = new Bookmarkmaster();
				$dining_experience->setShop_id($shop_id);
                                $dining_experience->setUser_id($user_id);
                                $dining_experience->setType('add');                               
                                $dining_experience->setCreate_date(date('Y-m-d h:i:s'));
                                $dining_experience->setDomain_id(0);
                                $dining_experience->setIs_deleted(0);
                                $em = $this->getDoctrine()->getManager();
                                $em->persist($dining_experience);
                                $em->flush(); 
                               
                                $this->error = "SFD" ;
				$this->data = array('bookmark_id'=>$dining_experience->getBookmark_master_id(),'type'=>$type,'user_id'=>$user_id);
                                }else{
									if($faq_info->getType() == 'add'){
										$faq_info->setType('remove');
									}else{
										$faq_info->setType('add');
									}

									$faq_info->setCreate_date(date('Y-m-d h:i:s'));
									$em->flush(); 
									$this->error = "SFD" ;
									
									$this->data = array('bookmark_id'=>$faq_info->getBookmark_master_id(),'type'=>$type,'user_id'=>$user_id);
								}
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

}
?>