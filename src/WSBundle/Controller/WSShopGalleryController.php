<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Membershipdetails;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\Usersetting;
use AdminBundle\Entity\Apppushnotificationmaster;
use AdminBundle\Entity\Addressmaster;

class WSShopGalleryController extends WSBaseController {

    /**
     * @Route("/ws/shop_gallery/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function shop_galleryAction($param) {
        /* try
          { */
        $this->title = "Shop Gallery";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('shop_id'),
            ),
        );

        
        if ($this->validateData($param)) {
            $shop_id = $param->shop_id;
            $hilighted = "SELECT * FROM restaurant_gallery as ef join restaurant_master as um on um.restaurant_master_id=ef.restaurant_id 
		left join media_library_master ON ef.image_id = media_library_master.media_library_master_id 
		where ef.is_deleted=0 and ef.restaurant_id='$shop_id' and um.is_deleted='0'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($hilighted);
            $stmt->execute();
            $hilighted_evalution = $stmt->fetchAll();
            $live_path = $this->container->getParameter('live_path');
            ;
            $response = array();
            if (!empty($hilighted_evalution)) {
                foreach ($hilighted_evalution as $key => $val) {
                    if ($val['media_location'] !== '' && $val['media_name'] != '')
                        $user_image = $live_path . $val['media_location'] . "/" . $val['media_name'];
                    else
                        $user_image = $live_path . '/bundles/Resource/default.png';
                    $response[] = array(
                        "media_id" => $val['image_id'],
                        "shopimage" => $user_image
                    );
                }
                $this->error = "SFD";
            }else {
                $this->error = "NRF";
            }
        } else {
            $this->error = "PIM";
        }

        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
        /* }
          catch(\Exception $e)
          {
          $this->error = "SFND" ;
          $this->data = false ;
          return $this->responseAction() ;
          } */
    }

}

?>
