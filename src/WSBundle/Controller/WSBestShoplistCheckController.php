<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Evaluationfeedback;
use AdminBundle\Entity\Evaluationfeedbackgallery;
use AdminBundle\Entity\Bookmarkmaster;
use AdminBundle\Entity\Savedtop10restaurant;
use AdminBundle\Entity\Savedcategorywiserestaurant;

class WSBestShoplistCheckController extends WSBaseController {

    /**
     * @Route("/ws/bestShopListAsPerCategoryCheck/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function bestShopListAsPerCategoryAction($param) {
        try {
            $this->title = "Evaluated Best  Restaurant";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('language_id'),
                ),
            );
            $response = false;

            if ($this->validateData($param)) {

                $language_id = $param->language_id;
                $user_id = '';
                if (isset($param->user_id)) {
                    $user_id = $param->user_id;
                }
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $live_path = $this->container->getParameter('live_path');
                $display_eve_category_array = NULL;
                $category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 and language_id = $language_id group by main_category_id";
                ;
                $category_listing = $this->firequery($category_listing_query);
                if (!empty($category_listing)) {
                    foreach ($category_listing as $catekey => $catval) {
                        $img_url = '';
                        if ($catval['category_image_id'] != 0) {
                            $img_url = $this->getImage($catval['category_image_id'], $live_path);
                        }
                        $evaluation_res_array = [];
                        // if there is Value in Table then Show From Table 
                        $saved_category_restaurant_query = "SELECT *  FROM `saved_categorywise_restaurant` WHERE is_deleted = 0 and `category_id` = " . $catval['main_category_id'] . " AND `update_datetime` between '" . date("Y-m-d 00:00:00") . "' and '" . date("Y-m-d 23:59:59") . "' order by update_datetime desc limit 0,5";
                        //  echo $saved_category_restaurant_query ; exit;

                        $saved_category_restaurant_list = $this->firequery($saved_category_restaurant_query);
                        if (!empty($saved_category_restaurant_list) && count($saved_category_restaurant_list) == 5) {
                            foreach ($saved_category_restaurant_list as $savekey => $saveval) {
                                $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $saveval['restaurant_id'], "language_id" => $language_id));
                                $rest_name = '';
                                if (!empty($restaurant_info)) {
                                    $rest_name = $restaurant_info->getRestaurant_name();
                                } else {
                                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $saveval['restaurant_id']));
                                    ///  var_dump($restaurant_info);
                                    //  var_dump(array("is_deleted"=>0,"main_restaurant_id"=>$resval['restaurant_id']));
                                    $rest_name = $restaurant_info->getRestaurant_name();
                                }
                                $evaluation_res_array[] = array(
                                    "restaurant_id" => $saveval['restaurant_id'],
                                    "restaurant_name" => stripslashes($rest_name),
                                    "evaluation_cnt" => $saveval['evaluation_cnt'],
                                    "total_evaluation" => $saveval['total_evaluation'],
                                    "points" => $saveval['rating_point'],
                                    "rating_point_percentage" => $saveval['rating_percentage']
                                );
                            }
                            $price = array();
                            if (!empty($evaluation_res_array)) {
                                foreach ($evaluation_res_array as $key => $row) {
                                    $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                                    $price['points'][$key] = $row['points'];
                                }

                                if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                                    array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $evaluation_res_array);
                                }
                            }
                            $display_eve_category_array[] = array(
                                "category_id" => $catval['main_category_id'],
                                "category_name" => $catval['category_name'],
                                "image" => $img_url,
                                "restaurant_list" => $evaluation_res_array
                            );
                        } else {
                            $restaurant_detail_query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt ,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation ,  evaluation_feedback.restaurant_id  FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_foodtype_relation` WHERE `main_caetgory_id` = " . $catval['main_category_id'] . " AND `restaurant_id` IN (SELECT restaurant_id FROM `evaluation_feedback` WHERE `status` = 'approved' AND `is_deleted` = 0) AND `is_deleted` = 0 
)  AND restaurant_master.is_deleted = 0  group by restaurant_id order by total_evaluation DESC limit 0,5";
                            $restaurant_detail_list = $this->firequery($restaurant_detail_query);

                            if (!empty($restaurant_detail_list)) {
                                foreach ($restaurant_detail_list as $reskey => $resval) {
                                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id'], "language_id" => $language_id));
                                    $rest_name = '';
                                    if (!empty($restaurant_info)) {
                                        $rest_name = $restaurant_info->getRestaurant_name();
                                    } else {
                                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                                ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id']));
                                        $rest_name = $restaurant_info->getRestaurant_name();
                                    }
                                    //----------------- get Points and rates-------------------
                                    $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $resval['restaurant_id'] . "' and status='approved' and is_deleted = 0) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $resval['restaurant_id'];
                                    $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                                    $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                                    $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                                    //$tot_ev_points = $tot_ev_points + $evpoints;                        
                                    $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
                                    $rating_point_percentage = 0;
                                    if ($rating_point > 0) {
                                        $rating_point_percentage = $rating_point * 20;
                                    }
                                    //-----------------------------------------------------------
                                    $evaluation_res_array[] = array(
                                        "restaurant_id" => $resval['restaurant_id'],
                                        "restaurant_name" => $rest_name,
                                        "evaluation_cnt" => $resval['evaluation_cnt'],
                                        "total_evaluation" => $resval['total_evaluation'],
                                        "points" => $total_rate,
                                        "rating" => !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0,
                                        "rating_point_percentage" => $rating_point_percentage,
                                    );
                                }


                                $price = array();
                                foreach ($evaluation_res_array as $key => $row) {
                                    $price[$key] = $row['rating_point_percentage'];
                                }
                                array_multisort($price, SORT_DESC, $evaluation_res_array);


                                $display_eve_category_array[] = array(
                                    "category_id" => $catval['main_category_id'],
                                    "category_name" => $catval['category_name'],
                                    "image" => $img_url,
                                    "restaurant_list" => $evaluation_res_array
                                );
                            }
                        }
                    }
                }
                $response = $display_eve_category_array;
                $this->data = $response;

                $this->error = "SFD";
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = False;
            }
            return $this->responseAction();
        } catch (Exception $ex) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

    /**
     * @Route("/ws/topRestaurantList/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function topRestaurantListAction($param) {
        try {
            $this->title = "Top Restaurant List";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('language_id'),
                ),
            );
            $response = false;

            if ($this->validateData($param)) {

                $language_id = $param->language_id;
                $user_id = !empty($param->user_id) ? $param->user_id : '';
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $live_path = $this->container->getParameter('live_path');
                $top_10_restaurant_array = NULL;
                $fetch_saved_top10_resturantQuery = " SELECT * FROM `saved_top10_restaurant` where is_deleted= 0 and `updated_datetime` between '" . date("Y-m-d 00:00:00") . "' and '" . date("Y-m-d 23:59:59") . "' order by updated_datetime desc limit 0,10";
               
                $fetch_saved_top10_resturantList = $this->firequery($fetch_saved_top10_resturantQuery);
                
                if (!empty($fetch_saved_top10_resturantList) && count($fetch_saved_top10_resturantList) == 10 ) {
                    foreach ($fetch_saved_top10_resturantList as $topkey => $topval) {

                        $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $topval['restaurant_id'], "language_id" => $language_id));
                        $rest_name = '';
                        if (!empty($restaurant_info)) {
                            $rest_name = $restaurant_info->getRestaurant_name();
                            $image_url = $this->getImage($restaurant_info->getLogo_id(), $live_path);
                        } else {
                            $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                    ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $topval['restaurant_id']));
                            $rest_name = $restaurant_info->getRestaurant_name();
                            $image_url = $this->getImage($restaurant_info->getLogo_id(), $live_path);
                        }
                        $top_10_restaurant_array[] = array(
                            "restaurant_id" => $topval['restaurant_id'],
                            "restaurant_name" => stripslashes($rest_name),
                            "logo" => $image_url,
                            // "evaluation_points"=>$val['total_evaluation'],
                            "points" => $topval['points'],
                            "rating" => $topval['rating'],
                            "rating_point_percentage" => $topval['rating_point_percentage'],
                                //                          
                        );
                    }
                    $price = array();
                    if (!empty($top_10_restaurant_array)) {
                        foreach ($top_10_restaurant_array as $key => $row) {
                            $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                            $price['points'][$key] = $row['points'];
                        }
                        if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                            array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $top_10_restaurant_array);
                        }
                    }
                    $response = $top_10_restaurant_array;
                    $this->error = "SFD";
                } 
                else {
                    $top_10_restaurant_Query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation, evaluation_feedback.restaurant_id, restaurant_master.logo_id FROM `evaluation_feedback` JOIN restaurant_master ON restaurant_master.main_restaurant_id =evaluation_feedback.restaurant_id WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and restaurant_master.is_deleted = 0 group by restaurant_id order by total_evaluation DESC limit 0,10";

                    $stmt = $con->prepare($top_10_restaurant_Query);
                    $stmt->execute();
                    $top_10_restaurant_list = $stmt->fetchAll();
                    $top_10_restaurant_array = [];
                    $tot_ev_points = 0;
                    if (!empty($top_10_restaurant_list)) {
                        foreach ($top_10_restaurant_list as $key => $val) {
                            $evaluation_feedback = null;
                            if (!empty($user_id)) {
                                $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                        ->findOneBy(array("restaurant_id" => $val['restaurant_id'], "user_id" => $user_id, "is_deleted" => 0));
                                $faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
                                        ->findOneBy(array(
                                    'shop_id' => $val['restaurant_id'],
                                    'user_id' => $user_id,
                                    'type' => 'add',
                                    'is_deleted' => 0
                                        )
                                );
                            }
//                      
                            $total_rate = $val['evaluation_cnt'];
                            // $tot_ev_points = $tot_ev_points + $val['total_evaluation'] ;
                            $image_url = $this->getImage($val['logo_id'], $live_path);
                            $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                    ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $val['restaurant_id'], "language_id" => $language_id));
                            $rest_name = '';
                            if (!empty($restaurant_info)) {
                                $rest_name = $restaurant_info->getRestaurant_name();
                            } else {
                                $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $val['restaurant_id']));
                                $rest_name = $restaurant_info->getRestaurant_name();
                            }
                            //----------------- get Points and rates-------------------
                            $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $val['restaurant_id'] . "' and status='approved' and is_deleted = 0) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $val['restaurant_id'];
                            $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                            $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                            $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                            //  $tot_ev_points = $tot_ev_points + $evpoints;                        
                            $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0;
                            $rating_point_percentage = 0;
                            if ($rating_point > 0) {
                                $rating_point_percentage = $rating_point * 20;
                            }
                            //-----------------------------------------------------------
                            $top_10_restaurant_array[] = array(
                                "restaurant_id" => $val['restaurant_id'],
                                "restaurant_name" => stripslashes($rest_name),
                                "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                "is_bookmark" => !empty($faq_info) ? true : false,
                                "logo" => $image_url,
                                // "evaluation_points"=>$val['total_evaluation'],
                                "points" => $total_rate,
                                "rating" => !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0,
                                "rating_point_percentage" => $rating_point_percentage,
//                          
                            );
                        }

                        $price = array();
                        foreach ($top_10_restaurant_array as $key => $row) {
                            $price['rating_point_percentage'][$key] = $row['rating_point_percentage'];
                            $price['points'][$key] = $row['points'];
                        }
                        if (isset($price['rating_point_percentage']) && is_array($price['rating_point_percentage']) && isset($price['points']) && is_array($price['points'])) {
                            array_multisort($price['rating_point_percentage'], SORT_DESC, $price['points'], SORT_DESC, $top_10_restaurant_array);
                        }
                        $response = $top_10_restaurant_array;
                        $this->error = "SFD";
                    } else {
                        $this->error = "NRF";
                    }
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = False;
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (Exception $ex) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>