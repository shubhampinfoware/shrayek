<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Aboutus;

class WSSubjectListController extends WSBaseController {

    /**
     * @Route("/ws/subjectlist/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function subjectListAction($param) {
        try {
            $this->title = "Subject List";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {
                //$faq_type_id = $param->faq_type_id ;
                $language_id = 1 ;
                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
                $level_info = $em->getRepository('AdminBundle:Levelmaster')
                        ->findBy(array(
                    'language_id' => $language_id,
                    'is_deleted' => 0
                        )
                );
                $level_array = NULL ;
                if (!empty($level_info)) {
                    foreach($level_info as $lkey=>$lval){
                        $level_array[] = array(
                            "level_id"=>$lval->getMain_level_id(),
                            "level_name"=>$lval->getLevel_name(),
                            "level_number"=>$lval->getLevel_number()
                        );
                    }    
                    $response = $level_array ;
                    $this->error = "SFD";
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND" . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }

 
}

?>