<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Organizationmaster;
use AdminBundle\Entity\Shopservicescategoryrelation;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Organizationservicearea;
use AdminBundle\Entity\Areamaster;
use AdminBundle\Entity\Shoptransactionhistory;
use AdminBundle\Entity\Bookmarkmaster;

class WSRestaurantdetailController extends WSBaseController {

    /**
     * @Route("/ws/restaurant_detail/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function restaurant_detailAction($param) {

        try {
            $this->title = "Restaurant Detail";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('restaurant_id'),
                ),
            );

            if ($this->validateData($param)) {



                $shop_id = !empty($param->restaurant_id) ? $param->restaurant_id : 0;


                $con = '';
                $lang_id = 1;
                $user_id = '';
                if (isset($param->language_id)) {
                    $lang_id = $param->language_id;
                }
                if (isset($param->user_id)) {
                    $user_id = $param->user_id;
                }
                $con .= ' and rm.language_id =' . $lang_id;


                if (!empty($shop_id)) {
                    $con .= ' and rm.main_restaurant_id =' . $shop_id;
                }




                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();


                $st = $conn->prepare("SELECT rm.*,ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id,
							cm.main_category_id,cm.category_name,cm.category_image_id
							from restaurant_master as rm
							left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
							left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
							left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
							where rm.is_deleted=0 $con and rm.status != 'inactive' group by rm.main_restaurant_id ");




                $st->execute();
                $shopList = $st->fetchAll();
                //print_r($shopList);exit;

                if (!empty($shopList)) {
                    $tot_ev_points = 0;
                    foreach ($shopList as $shopMaster) {

                        // get catgory
                        $query = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $lang_id . "'  group by cm.main_category_id";
                        $cat = $this->firequery($query);
                        $category = null;
                        if (!empty($cat)) {
                            foreach ($cat as $ca) {


                                // get cuisines
                                $query = "select * from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $shopMaster['main_restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$lang_id and rfr.main_caetgory_id=" . $ca['main_category_id'];

                                $cuisines = null;


                                $cuisine = $this->firequery($query);


                                if (!empty($cuisine)) {

                                    foreach ($cuisine as $val) {


                                        $cuisines[] = array(
                                            "food_type_id" => $val['main_food_type_id'],
                                            "food_type_name" => $val['food_type_name'],
                                            "food_type_image" => $this->getimage($val['food_type_image_id'])
                                        );
                                    }
                                    $category[] = array("category_id" => $ca['main_category_id'],
                                        "category_name" => $ca['category_name'],
                                        "category_logo" => $this->getimage($ca['category_image_id']), "food_type" => $cuisines);
                                }
                            }
                        }
						
                        //$query1 = "SELECT * from address_master where main_address_id='" . $shopMaster['address_id'] . "' and language_id=" . $lang_id . " and is_deleted=0";
						$query1 = "SELECT * from address_master where owner_id='" . $shopMaster['main_restaurant_id'] . "' and language_id=" . $lang_id . " and is_deleted=0";
                        $addresss = $this->firequery($query1);
						
                        $gallery = $this->getshopcoverimage($shopMaster['main_restaurant_id']);
                        $faq_info = null;
                        if (!empty($user_id)) {
                            $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                    ->findOneBy(array("restaurant_id" => $shopMaster['main_restaurant_id'], "user_id" => $user_id, "is_deleted" => 0));
                            $faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
                                    ->findOneBy(array(
                                'shop_id' => $shopMaster['main_restaurant_id'],
                                'user_id' => $user_id,
                                'type' => 'add',
                                'is_deleted' => 0
                                    )
                            );
                        }
                        $where = '';
                        if (!empty($shopMaster['main_restaurant_id'])) {
                            $where .= ' and restaurant_id=' . $shopMaster['main_restaurant_id'];
                        }
                        $query = "select * from restaurant_menu where language_id = {$lang_id} and is_deleted = 0 $where order by sort_order ASC";
                        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
                        $em->execute();
                        $all_category = $em->fetchAll();
                        $menu_image = null;
                        if (count($all_category) > 0) {
                            foreach ($all_category as $lval) {
								if($lval['media_id'] != 0){
									$menu_image[] = array("media_id" => $lval['media_id'],
										"menu_image" => $this->getimage($lval['media_id']),
									);
								}
                            }
                        }
                        //$restaurant_total_evpointsQuery = "SELECT evaluation_feedback.service_level as sl, evaluation_feedback.dining_level as dl, evaluation_feedback.atmosphere_level as al, evaluation_feedback.price_level as pl, evaluation_feedback.clean_level as cl, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $shopMaster['main_restaurant_id'];
                        //$restaurant_total_evpointsQuery = "SELECT sum(count(evaluation_feedback.service_level) as sl+count(evaluation_feedback.dining_level) as dl+count(evaluation_feedback.atmosphere_level) as al+count(evaluation_feedback.price_level) as pl+count(evaluation_feedback.clean_level) as cl) as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $shopMaster['main_restaurant_id'];
                        $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $shopMaster['main_restaurant_id'] . "' and status='approved') as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level +evaluation_feedback.delivery_speed_level 
						+ evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $shopMaster['main_restaurant_id'];
					//	echo $restaurant_total_evpointsQuery;exit;
                        $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                        $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                        $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                        $tot_ev_points = $tot_ev_points + $evpoints;                        
                        $rating_point = !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0 ;
							
						$rating_point_average = !empty($total_rate) ? $evpoints / ($total_rate) : 0 ;
						
                        $rating_point_percentage = 0 ;
                        if($rating_point > 0 ){
                            $rating_point_percentage = $rating_point * 20 ;
                        }            
					//	exit($rating_point_percentage);

						## check for evaluation gallery
						$has_evaluation_gallery = false;
						
						$where = " restaurant_id='".$shopMaster['main_restaurant_id']."' and ";
						$check_evaluation_gallery = "select evaluation_feedback_id,restaurant_id from evaluation_feedback where {$where} is_deleted=0 and status != 'rejected' and status != 'pending' and status != 'under_evaluation' group by restaurant_id";
						
						$con = $this->getDoctrine()->getManager()->getConnection();
						$stmt = $con->prepare($check_evaluation_gallery);
						$stmt->execute();
						$evaluations = $stmt->fetchAll();
						
						if(!empty($evaluations)){
							foreach($evaluations as $evaluations_data){
								$sql_evaluation_feedback_rest_wise = "select eval_gallery.media_id,eval.evaluation_feedback_id,eval.restaurant_id from evaluation_feedback eval JOIN evaluation_feedback_gallery eval_gallery ON eval.evaluation_feedback_id=eval_gallery.evaluation_feedback_id where eval.restaurant_id='".$evaluations_data['restaurant_id']."' and eval.is_deleted=0 and eval_gallery.is_deleted=0";
								
								$con = $this->getDoctrine()->getManager()->getConnection();
								$stmt = $con->prepare($sql_evaluation_feedback_rest_wise);
								$stmt->execute();
								$evaluations_restaraunt_wise = $stmt->fetchAll();
								if($evaluations_restaraunt_wise){
									$has_evaluation_gallery = true;
								}
							}
						}
						
						if($menu_image == null){
							$has_shop_menu = false;
						} else {
							$has_shop_menu = true;
						}
						
#getRestaurant branches

					$sql_brach_exist = "SELECT branch.*,address_master.address_name, address_master.owner_id ,address_master.lat ,address_master.lng FROM branch_master branch 
					LEFT JOIN address_master ON branch.branch_address_id = address_master.address_master_id and address_master.language_id = '$lang_id'  
					WHERE branch.is_deleted = 0 AND branch.main_restaurant_id='" . $shopMaster['main_restaurant_id'] . "' and branch.language_id = '$lang_id' order by main_branch_flag DESC";
					
					
					$con = $this->getDoctrine()->getManager()->getConnection();
					$stmt = $con->prepare($sql_brach_exist);
					$stmt->execute();
					$branch_data = $stmt->fetchAll();
					
					$branch_count = 0;
					$branch_data_new = null;
					if(empty($branch_data)){
						$branch_data = null;
					}else{
						$branch_count = count($branch_data);
						
						foreach($branch_data as $key=>$value){

							if($value['main_branch_flag'] == 1){
							
							   $restaurant_total_evpointsQuery_branch = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $shopMaster['main_restaurant_id'] . "' and  main_branch_id = '".$value['main_branch_master_id']."' and status='approved') as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level +evaluation_feedback.delivery_speed_level + evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $shopMaster['main_restaurant_id']." and evaluation_feedback.main_branch_id = '".$value['main_branch_master_id']."'";
								$evpoints_branch = $this->firequery($restaurant_total_evpointsQuery_branch);
								
								$total_rate_branch = !empty($evpoints_branch) ? $evpoints_branch[0]['total_rate'] : 0;
								$evpoints_branch = !empty($evpoints_branch) ? $evpoints_branch[0]['Ev_points'] : 0;
								$tot_ev_points = $tot_ev_points + $evpoints_branch;                        
								$rating_point_branch = !empty($total_rate_branch) ? $evpoints_branch / ($total_rate_branch * 5) : 0 ;
								
								$rating_point_average_branch = !empty($total_rate_branch) ? $evpoints_branch / ($total_rate_branch) : 0 ;
								
								$rating_point_percentage_branch = 0 ;
								
								if($rating_point_branch > 0 ){
									$rating_point_percentage_branch = $rating_point_branch * 20 ;
								}
								
								$branch_data[$key]["address"] = !empty($value['address_name']) ? $value['address_name'] : '--' ;
								$branch_data[$key]["lat"] = !empty($value['lat']) ? $value['lat'] : 0 ;
								$branch_data[$key]["lng"] = !empty($value['lng']) ? $value['lng'] : 0 ;
								
								$branch_data[$key]['points'] = $total_rate_branch;
								$branch_data[$key]['mobile_no'] = !empty($value['mobile_no']) ? $value['mobile_no'] : '--';;
								
								$branch_data[$key]['rating'] = !empty($total_rate_branch) ? $evpoints_branch / ($total_rate_branch * 5) : 0;
								
								$branch_data[$key]['rating_point_percentage'] = $rating_point_percentage_branch;
								$branch_data[$key]['rating_point_average'] = $rating_point_average_branch;
								
								#get Gallery of branch
								$gallery_branch = $this->getbranchcoverimage($value['main_branch_master_id']);
								$branch_data[$key]['shop_gallery'] = !empty($gallery_branch) ? $gallery_branch : null;
								#get Gallery of branch done
								
								$branch_data_new [] = $branch_data[$key];
								
								
							}
						}
					}
						
#getRestaurant branches....						
						
                        $response[] = array(
                            "shop_id" => $shopMaster['main_restaurant_id'],
                            "shop_name" => $shopMaster['restaurant_name'],
                            "instagram_link" => !empty($shopMaster['instagram_link']) ? $shopMaster['instagram_link'] : null,
                            "shop_short_description" => $shopMaster['description'],
                            "shop_long_description" => $shopMaster['description'],
                            "shop_logo" => $this->getimage($shopMaster['logo_id']),
                            "shop_menu" => $menu_image,
                            "shop_gallery" => !empty($gallery) ? $gallery : null,
                            "opening_date" => strtotime($shopMaster['opening_date']) * 1000,
                            "points" => $total_rate,
                            "rating" => !empty($total_rate) ? $evpoints / ($total_rate * 5) : 0,
                            "rating_point_percentage" => $rating_point_percentage,
                            "rating_point_average" => $rating_point_average,
                            "cuisines" => $category,
                            "phone_number" => $shopMaster['phone_number'],
                            "additional_info" => $shopMaster['additional_info'],
                            "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                            "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                            "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                            "timing" => $shopMaster['timings'],
                            "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                            "is_bookmark" => !empty($faq_info) ? true : false,
							"has_evaluation_gallery" => $has_evaluation_gallery,
							"has_shop_menu" => $has_shop_menu,
							"branch_data"=>$branch_data_new,
							'branch_count'=>$branch_count
                        );
                    }
                    if ($tot_ev_points != 0) {

                        if (!empty($response)) {
                            foreach ($response as $akey => $aval) {
                                //var_dump($aval['shop']['total_points_of_shop']); exit;
                                $res_points = $tot_ev_points;
                                $points_percentage = ( $res_points * 25 ) / 100;
                                $rate = ( $points_percentage * 5 ) / 100;
                               // $response[$akey]['points_percentage'] = $points_percentage;
                              //  $response[$akey]['rate_value_with_decimal'] = $rate;
                              //  $response[$akey]['rate'] = number_format((float) $rate, 1, '.', '');
                            }
                        }
                    }
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            } else {
                $this->error = "SFD";
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>
