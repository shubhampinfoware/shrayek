<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Gcmuser;

class WSUpdatetokenController extends WSBaseController {

    /**
     * @Route("/ws/updatetoken/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function updatetokenAction($param) {
        try {
            $this->title = "Update Token";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('user_id', 'device_id', 'device_token', 'device_name'),
                ),
            );

            if ($this->validateData($param)) {

                $user_id = $param->user_id;
                $device_id = $param->device_id;
                $device_token = $param->device_token;
                $device_name = strtoupper($param->device_name);
                $em = $this->getDoctrine()->getManager();

                $user_name = '';
                $user_type = '';

                if ($user_id != 0) {
                    $user_type = 'user';
                    $user = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Usermaster')
                            ->findOneBy(array('user_master_id' => $user_id, 'is_deleted' => 0));
                    if ($user) {
                        $user_name = $user->getUsername();
                    }
                } else {
                    $user_type = 'guest';
                    $user_name = 'Guest user';
                }
                if ($device_name == 'ANDROID') {

                    $device_old = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Gcmuser')
                            ->findBy(array('device_id' => $device_id, 'is_deleted' => 0));

                    if ($device_old) {
                        foreach ($device_old as $device) {
                            $device->setIs_deleted(1);
                            $em->flush();
                        }
                    }

                    $new_user = new Gcmuser();
                    $new_user->setGcm_regid($device_token);
                    $new_user->setUser_id($user_id);
                    $new_user->setUser_type($user_type);
                    $new_user->setDevice_id($device_id);
                    $new_user->setApp_id('CUST');
                    $new_user->setDomain_id(0);
                    $new_user->setName($user_name);
                    $new_user->setCreated_date(date('y-m-d H:i:s'));
                    $new_user->setIs_deleted(0);

                    $em->persist($new_user);
                    $em->flush();

                  
                   
					$response = true;
					$this->error = "SFD ";
                   
                }
                // if ($device_name == 'IOS' or $device_name == 'IPHONE' && strlen($device_token) == 64) {
                if ($device_name == 'IOS' or $device_name == 'IPHONE') {

                    $device_old = $membership = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Apnsuser')
                            ->findBy(array('device_id' => $device_id, 'is_deleted' => 0));

                    if ($device_old) {
                        foreach ($device_old as $device) {
                            $device->setIs_deleted(1);
                            $em->flush();
                        }
                    }

                    $new_user = new Apnsuser();
                    $new_user->setApns_regid($device_token);
                    $new_user->setUser_id($user_id);
                    $new_user->setUser_type($user_type);
                    $new_user->setDevice_id($device_id);
                    $new_user->setApp_id('CUST');
                    $new_user->setDomain_id(0);
                    $new_user->setName($user_name);
                    $new_user->setBadge(0);
                    $new_user->setCreated_date(date('y-m-d H:i:s'));
                    $new_user->setIs_deleted(0);

                    $em->persist($new_user);
                    $em->flush();
					/*
                    $membership = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Apnsuser')
                            ->findOneBy(array('user_id' => $user_id, 'is_deleted' => 0));
                    if (!empty($membership)) {
                        $membership->setApns_regid($device_token);
                        $membership->setDevice_id($device_id);
                        $em->flush();
                    }*/
                    $response = true;
                    $this->error = "SFD";
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
            }

            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND" . $e->getMessage();
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>
