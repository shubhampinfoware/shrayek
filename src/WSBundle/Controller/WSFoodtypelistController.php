<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Medicalreport;
use AdminBundle\Entity\Attributecategoryrelation;
class WSFoodtypelistController extends WSBaseController {
	
	/**
	 * @Route("/ws/foodtypelist/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	   public function foodtypelistAction($param)
	{
	   /*try{*/
			$this->title = "Food Type List";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('language_id'),
				),
			);
			if($this->validateData($param)){
				$language_id = $param->language_id;
                                $category_id = !empty($param->category_id)?$param->category_id:0;
				$response = array();
                                $where='';
                                if(!empty($category_id)){
                                    $where.=" and acr.main_category_id=". $category_id;
                                }
				$em = $this->getDoctrine()->getManager();
		                $connection = $em->getConnection();
		                $query = "SELECT * from food_type_master left join food_type_category_relation as acr on acr.main_food_type_id=food_type_master.main_food_type_id left join category_master on category_master.main_category_id=acr.main_category_id where food_type_master.is_deleted=0 $where and food_type_master.language_id=$language_id group by food_type_master.main_food_type_id";
		                $get_post_list = $connection->prepare($query);
		                $get_post_list->execute();
		                $attributes = $get_post_list->fetchAll();
				if(count($attributes) > 0 )
				{
                                    
					foreach(array_slice($attributes,0) as $lkey=>$lval)
					{
						
                                            $data[]= array("main_food_type_id"=> $lval['main_food_type_id'],
                                                           "food_type_name"=>$lval['food_type_name'],
                                                           "main_category_id"=>$lval['main_category_id'],
                                                           "category_name"=>$lval['category_name']
                                                     );
						
					}
					
				}
				if(!empty($data))
				{
					$response = $data;
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

}
?>