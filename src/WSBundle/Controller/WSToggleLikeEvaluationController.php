<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Productmaster;
use AdminBundle\Entity\Votemaster;
use AdminBundle\Entity\Evaluationlikerelation;

class WSToggleLikeEvaluationController extends WSBaseController {

    /**
     * @Route("/ws/toggleLikeEvaluation/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function toggleLikeEvaluationAction($param) {
        /* try{ */
        $this->title = "Toggle Like Evaluation";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('evaluation_id','user_id'),
            ),
        );
        if ($this->validateData($param)) {
			
            $evaluation_id = $param->evaluation_id;
            
			$operation = !empty($param->operation) ? $param->operation : 'like';
			
            $user_id = $param->user_id;
			$liked = false;
			
			$em = $this->getDoctrine()->getManager();
			
            $response = array();
			
            $evaluation = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Evaluationfeedback')
                    ->findOneBy(array("evaluation_feedback_id"=>$evaluation_id,'is_deleted'=>0));
		
            if ($evaluation) {

			$user = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findOneBy(array("user_master_id"=>$user_id,'is_deleted'=>0));
				
				if($user){
					#check liked or not Evaluationlikerelation
					$entry_exist = $this->getDoctrine()
								->getManager()
								->getRepository('AdminBundle:Evaluationlikerelation')
								->findOneBy(array("evaluation_id"=>$evaluation_id,'user_id'=>$user_id,"operation"=>$operation));
					if($entry_exist){
						if($entry_exist->getIs_deleted() == 0){
							$entry_exist->setIs_deleted(1);
						}else{
							$entry_exist->setIs_deleted(0);
						}

						$entry_exist->setCreated_datetime(date('Y-m-d H:i:s'));
						$em->flush();
						
						if($entry_exist->getIs_deleted() == 0){
							$liked = true;							
						}else{
							$liked = false;
						}

						$evaluation_id = $entry_exist->getEvaluation_id();
						
					}else{
						#newLike
						$new_like = new Evaluationlikerelation();
						$new_like->setUser_id($user_id);
						$new_like->setOperation($operation);
						$new_like->setEvaluation_id($evaluation_id);
						$new_like->setCreated_datetime(date('Y-m-d H:i:s'));
						$new_like->setIs_deleted(0);
						
						$em->persist($new_like);
						$em->flush();
						
						$liked = true;
						$evaluation_id = $new_like->getEvaluation_id();
						
					}
					
					if(strtolower($operation) == 'dislike'){
						#remove like if user liked it
						$entry_exist_like = $this->getDoctrine()
							->getManager()
							->getRepository('AdminBundle:Evaluationlikerelation')
							->findOneBy(array("evaluation_id"=>$evaluation_id,'user_id'=>$user_id,"operation"=>'like'));
							
						if($entry_exist_like){
							$entry_exist_like->setIs_deleted(1);
							$em->flush();
						}	
						
					}
					
					if(strtolower($operation) == 'like'){
						#remove dislike if user disliked it
						$entry_exist_dislike = $this->getDoctrine()
							->getManager()
							->getRepository('AdminBundle:Evaluationlikerelation')
							->findOneBy(array("evaluation_id"=>$evaluation_id,'user_id'=>$user_id,"operation"=>'dislike'));
							
						if($entry_exist_dislike){
							$entry_exist_dislike->setIs_deleted(1);
							$em->flush();
						}	
						
					}
						
					
					$response = array('liked_flag'=>$liked,'evaluation_id'=>$evaluation_id,'operation'=>$operation);
					
					$this->error = "SFD";
					
				}else{
					$response = false;
					$this->error = "UNE";					
				}
            }else{
				$response = false;
                $this->error = "ENE";
			}

            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        return $this->responseAction();
        /* }
          catch (\Exception $e)
          {
          $this->error = "SFND";
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>