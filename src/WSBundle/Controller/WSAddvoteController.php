<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Votemaster;
use AdminBundle\Entity\Votecomments;

class WSAddvoteController extends WSBaseController {

    /**
     * @Route("/ws/add_vote/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     *
     */
    public function add_voteAction($param) {
        try {
            $this->title = "Add vote";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('product_id', 'user_id', 'vote_type'),
                ),
            );
            if ($this->validateData($param)) {

                $response = true;
                $check_vote_status = $this->getDoctrine()->getManager()->getRepository(Votemaster :: class)->findOneBy(array('product_id' => $param->product_id,
                    'user_id' => $param->user_id));

                if (empty($check_vote_status)) {
                    $vote = new Votemaster();
                    $vote->setProduct_id($param->product_id);
                    $vote->setUser_id($param->user_id);
                    $vote->setVote_type($param->vote_type);
                    $vote->setCreated_datetime(date('Y-m-d H:i:s'));
                    $vote->setIs_deleted(0);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($vote);
                    $em->flush();

                    $vote_id = $vote->getVote_master_id();
                    if (isset($vote_id) && $vote_id != '') {

                        #loyaltyPoints changes
                        if(!empty($vote_id)){
                                                
                            $activity_type = 'voting_product';
                            $points_added = $this->addCompetitionPointsToUser($param->user_id,$activity_type,0,$vote_id,'vote_master');
                        }
                        #loyaltyPoints changes

                        $response = $vote_id;
                        $this->error = "SFD";
                    }
                    if (empty($response)) {
                        $response = false;
                        $this->error = "NRF";
                    }
                    $this->data = $response;
                } else {
                    $this->error = "ARV"; //AllReady Voted				
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = False;
            }
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

    /**
     * @Route("/ws/add_votecomment/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     *
     */
    public function add_votecommentAction($param) {
        try {
            $this->title = "Add vote-comment";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {

                $response = true;

                $vote_comment = new Votecomments();
                $vote_comment->setProduct_id($param->product_id);
                $vote_comment->setUser_id($param->user_id);
                $vote_comment->setComments($param->comment);
                $vote_comment->setCreated_datetime(date('Y-m-d H:i:s'));
                $vote_comment->setIs_deleted(0);

                $em = $this->getDoctrine()->getManager();
                $em->persist($vote_comment);
                $em->flush();

                $vote_comment_id = $vote_comment->getVote_comment_id();
                if (isset($vote_comment_id) && $vote_comment_id != '') {

                    #loyaltyPoints changes
                    if(!empty($vote_comment_id)){
                                            
                        $activity_type = 'voting_with_comment';
                        $points_added = $this->addCompetitionPointsToUser($param->user_id,$activity_type,0,$vote_comment_id,'vote_comments');
                    }
                    #loyaltyPoints changes

                    $response = $vote_comment_id;
                    $this->error = "SFD";
                }
                if (empty($response)) {
                    $response = false;
                    $this->error = "NRF";
                }
                $this->data = $response;
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = False;
            }
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

    /**
     * @Route("/ws/read_comments/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     *
     */
    public function read_commentsAction($param) {
        try {
            $this->title = "Read Comments";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {
                $response = array();
                $data = $all_comments = '';

                $product_id = $param->product_id;

                $all_comments = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Votecomments')
                        ->findBy(array('is_deleted' => 0, 'product_id' => $product_id));

                if (!empty($all_comments)) {

                    foreach (array_slice($all_comments, 0) as $lkey => $lval) {

                        $user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->find($lval->getUser_id());

                        $user_image_id = $username = $user_image_url= '';
                            if (!empty($user)) {
                                $user_image_id = $user->getUser_image();
                                $username = $user->getUser_bio();
                                $user_image_url = $user->getImage_url();
                            }

                        $data[] = array(
                            "vote_comment_id" => $lval->getVote_comment_id(),
                            "comment" => $lval->getComments(),
                            "user_name" => $username,
                            "user_img" => !empty($user_image_id)?$this->getimage($user_image_id):$user_image_url,
                        );
                    }
                }

                if (!empty($data)) {
                    $response = $data;
                    $this->error = "SFD";
                }
                if (empty($response)) {
                    $response = false;
                    $this->error = "NRF";
                }

                $this->data = $response;
                return $this->responseAction();
            }
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>