<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Addressmaster;
use Symfony\Component\HttpFoundation\JsonResponse;

class WSCheckuserstatus extends WSBaseController {

    /**
     * @Route("/ws/checkuserstatus/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     */
    public function checkuserstatusAction($param) {
        try {
            $response = false;
            $faq_arr = [];
            $this->title = "Check User Status";
            //$param = $this->requestAction($this->getRequest()) ;
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('user_id'),
                ),
                    );

            if ($this->validateData($param)) {
                //$faq_type_id = $param->faq_type_id ;
                $user_id = $param->user_id;
                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
                $userMaster = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(
                        array(
                            "user_master_id" => $user_id, "status" => 'active', "is_deleted" => 0,
                ));

                if (empty($userMaster)) {
                    $response=null;
                    $this->error = "UII";
                    $this->error_msg = "User Is Inactive";
                } else {
                    $latest_address_info= $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted"=>0,"owner_id"=>$userMaster->getUser_master_id(),"base_address_type"=>'primary')); 
                $address_list = NULL ;        
                if(!empty($latest_address_info)  && $latest_address_info != NULL){
                    $address_list = $this->getaddressAction($latest_address_info->getAddress_master_id(),$language_id);
                }
                if (!empty($address_list)) {
                    $userAddress = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy( array("address_master_id" => $userMaster->getAddress_master_id(),"is_deleted" => 0) );
                } 
                    $response = array(
                    "usermaster_id" => $userMaster->getUser_master_id(),
                    "nick_name" => $userMaster->getUser_bio(),
                    "user_firstname" => $userMaster->getUser_firstname(),
                    "user_lastname" => $userMaster->getUser_lastname(),
                    "user_mobile" => $userMaster->getUser_mobile(),
                    "user_image" => !empty($userMaster->getUser_image())?$this->getimage($userMaster->getUser_image()):$userMaster->getImage_url(),
                    "role_id" => $userMaster->getUser_role_id(),
                    "user_emailid" => $userMaster->getEmail(),
                    "date_of_birth" => strtotime($userMaster->getDate_of_birth())*1000,
                   // "user_phoneno" => $this->keyDecryptionAction($userMaster->getUser_mobile()),
                    "username" => $userMaster->getUsername(),
                    "language" => $userMaster->getCurrent_lang_id(),
                    "domain_id" => $userMaster->getDomain_id(),
                    "address" => $address_list
                        );
                    $this->error = "SFD";
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                //$this->error = "NRF";
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND" . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>