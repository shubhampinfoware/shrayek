<?php
namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AdminBundle\Entity\Aboutus;

class WSUserLogoutController extends WSBaseController{
    /**
     * @Route("/ws/user-logout/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userlogoutAction($param, Request $request){
		try{
			$this->title = "App user logout";
			$param = $this->requestAction($request, 0);
			$this->validateRule = array(
				array(
					'rule' => 'NOTNULL',
					'field' => array('user_id', 'device_id', 'device_name'),
				),
					);

			if ($this->validateData($param)) {
				$user_id = $param->user_id;
				$device_name = strtoupper($param->device_name);
				$device_id = $param->device_id;

				$em = $this->getDoctrine()->getManager();
				if ($device_name == 'IPHONE' || $device_name == 'iphone' || $device_name == 'ios' || $device_name == 'IOS') {
					if (isset($device_id)) {
						
						$entity = $this->getDoctrine()->getManager();
						$update_user = $entity->getRepository('AdminBundle:Apnsuser')->findBy(
							array(
								'user_id' => $user_id,
								'device_id' => $device_id,
								'is_deleted' => 0
							)
						);
						
						if(!empty($update_user)){
							foreach($update_user as $_user){
								$_user->setIs_deleted(1);
								$entity->flush();
							}
							$this->error = "SFD";
						} else {
							$this->error = "NRF";
						}
						
						$response = true;
					}
				}
				if ($device_name == 'ANDROID' || $device_name == 'android') {
					if (isset($device_id)) {
						
						$entity = $this->getDoctrine()->getManager();
						$update_user = $entity->getRepository('AdminBundle:Gcmuser')->findBy(
							array(
								'user_id' => $user_id,
								'device_id' => $device_id,
								'is_deleted' => 0
							)
						);
						
						if(!empty($update_user)){
							foreach($update_user as $_user){
								$_user->setIs_deleted(1);
								$entity->flush();
							}
							$this->error = "SFD";
						} else {
							$this->error = "NRF";
						}
						$response = true;
					}
				}
			} else {
				$this->error = "PIM";
			}
			if (empty($response)) {
				$response = false;
				$this->error = "SFND";
			}
			$this->data = $response;
			return $this->responseAction();
		}
		catch(\Exception $e){
			$this->error = "SFND".$e ;
			$this->data = false ;
			return $this->responseAction() ;
		}
	}
}
?>