<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Suggestioncomplaints;
class WSAddsuggestionController extends WSBaseController {
	/**
	 * @Route("/ws/add_suggestion/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	
	   public function add_suggestionAction($param)
	{
	   /*try{*/
			$this->title = "Add Suggestion";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(),
				),
			);
			if($this->validateData($param)){
					$language_id=1;
				if(!empty($param->language_id))
					$language_id = $param->language_id;
				$response = array();
				
				$Suggestioncomplaints = new Suggestioncomplaints();
				$Suggestioncomplaints->setNick_name($name);
				$Suggestioncomplaints->setEmail_address($email);
				$Suggestioncomplaints->setSubject($subject);
				$Suggestioncomplaints->setComment($comment);
				$Suggestioncomplaints->setCreated_date(date('Y-m-d H:i:s'));
				$Suggestioncomplaints->setDomain_id(0);
				$Suggestioncomplaints->setIs_deleted(0);
				$em = $this->getDoctrine()->getManager();
				$em->persist($Suggestioncomplaints);
				$em->flush();
				
				$Suggestioncomplaints_id=$Suggestioncomplaints->getSuggestion_complaints_id();
				$data = $all_category = '';
				
					$all_category = $this->getDoctrine()
							   ->getManager()
							   ->getRepository('AdminBundle:Competitionaward')
							   ->findBy(array('is_deleted'=>0,'language_id'=>$language_id,'suggestion_complaints_id'=>$Suggestioncomplaints_id));
				
				if(count($all_category) > 0 )
				{
                                    
					foreach(array_slice($all_category,0) as $lkey=>$lval)
					{
						
                        $data[]= array("suggestion_id"=> $Suggestioncomplaints_id,
						"name"=>$lval->getNick_name(),
						"email"=>$lval->getEmail_address(),
						"subject"=>$lval->getSubject(),
						"comment"=>$lval->getComment(),
						"created_date"=>date('Y-m-d',strtotime($lval->getCreated_date())),

						);
						
					}
					
				}
				if(!empty($data))
				{
					$response = $data;
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}
	
}
?>