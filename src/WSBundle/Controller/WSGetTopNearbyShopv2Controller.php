<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Contactmaster;
use AdminBundle\Entity\Medicalreportimage;
use AdminBundle\Entity\Contactusfeedback;
class WSGetTopNearbyShopv2Controller extends WSBaseController {
    /**
     * @Route("/ws/getTopNearbyShopv2/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getTopNearbyShopv2Action($param) {
       // try {
            $this->title = "Top 5 Nearby Restaurant v2";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array()
            ));
            if ($this->validateData($param)) {
                $in_top_five = false ;
                $login_user_id = !empty($param->login_user_id) ? $param->login_user_id : 0;
                $language_id = !empty($param->language_id) ? $param->language_id : 1;
                $lat = !empty($param->lat) ? $param->lat : 0;
                $lng = !empty($param->lng) ? $param->lng : 0;
                $map_flag = !empty($param->map_flag) ? $param->map_flag : false;
                $live_path = $this->container->getParameter('live_path');
                $display_eve_category_array = null;
                #flag for top 5 or not..
                $is_top_five = !empty($param->is_top_five) ? strtolower($param->is_top_five) : 'no';
                $main_category_id = !empty($param->main_category_id) ? strtolower($param->main_category_id) : 0;
                if ($is_top_five == 'yes') {
                    $orderByClause = ' order by total_evaluation DESC,DISTANCE ASC';
                } else {
                    $orderByClause = ' order by DISTANCE ASC';
                }
                 $limit_rangeKms = 2;
                $con = $this->getDoctrine()->getManager()->getConnection();
                $evaluation_res_array = [] ;
//                $restaurantList = $this->firequery("SELECT * FROM `restaurant_master` where is_deleted = 0 and language_id = " . $language_id ) ;
//                if(!empty($restaurantList)){
//                    foreach($restaurantList as $rsskey=>$rssval){
//                        $query = "select cm.main_category_id,cm.category_name,cm.category_image_id from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $language_id . "' group by cm.main_category_id order by sort_order DESC";
//                        $cat = $this->firequery($query);
//
//                        $category = null;
//                        if (!empty($cat)) {
//                            foreach ($cat as $ca) {
//                                // get cuisines
//                                $query = "select ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $rssval['main_restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$language_id and rfr.main_caetgory_id=" . $ca['main_category_id'];
//                                $cuisines = null;
//                                $cuisine = $this->firequery($query);
//                               
//                                if (!empty($cuisine)) {
//                                    // echo $query ;
//                               // var_dump($cuisine);exit;
//                                    $food_typeStr = [] ; 
//                                    foreach ($cuisine as $val) {
//                                        $cuisines[] = array(
//                                            "food_type_id" => $val['main_food_type_id'],
//                                            "food_type_name" => $val['food_type_name'],
//                                            "food_type_image" => $this->getimage($val['food_type_image_id'])
//                                        );
//                                        $food_typeStr[] =  $val['food_type_name'] ;
//                                    }
//                                    $food_typeStr = implode(",",$food_typeStr);
//                                    echo $food_typeStr ; exit;
//                                    $category[] = array("category_id" => $ca['main_category_id'],
//                                        "category_name" => $ca['category_name'],
//                                        // "category_logo" => $this->getimage($ca['category_image_id']), 
//                                        "food_type" => $cuisines);
//                                }
//                            }
//                        } 
//                    }
//                }
                //-------------------------------------------------------------------
                
                //-------------------------start -------------------------------------
                $saved_category_restaurant_query = 
                        "SELECT DISTINCT
                            branch_master.main_branch_master_id,
                            branch_master.language_id,
                            branch_master.branch_address_id,
                             branch_master.main_restaurant_id ,
                             restaurant_master.logo_id,
                             restaurant_master.restaurant_name,
                             branch_master.mobile_no,
                             saved_categorywise_restaurant.evaluation_cnt,
                             saved_categorywise_restaurant.total_evaluation,
                             saved_categorywise_restaurant.rating_point,
                             saved_categorywise_restaurant.rating_percentage,

                            address_master.lat,
                            address_master.lng,
                            (
                                3959 * ACOS(
                                    COS(RADIANS($lat)) * COS(RADIANS(address_master.lat)) * COS(
                                        RADIANS(address_master.lng) - RADIANS($lng)
                                    ) + SIN(RADIANS($lat)) * SIN(RADIANS(address_master.lat))
                                )
                            ) AS DISTANCE
                        FROM
                            `branch_master`
                        JOIN address_master ON branch_master.branch_address_id = address_master.address_master_id
                        JOIN restaurant_master ON branch_master.main_restaurant_id =  restaurant_master.main_restaurant_id 
                        LEFT JOIN saved_categorywise_restaurant on restaurant_master.main_restaurant_id = saved_categorywise_restaurant.restaurant_id
                        WHERE
                            branch_master.is_deleted = 0 AND address_master.lat != '' AND address_master.lng != ''
                            AND restaurant_master.language_id = $language_id 
                            AND (saved_categorywise_restaurant.is_deleted = 0 OR saved_categorywise_restaurant.is_deleted is null )
                            AND branch_master.main_restaurant_id IN(
                            SELECT
                                restaurant_id
                            FROM
                                `saved_categorywise_restaurant`
                            WHERE
                                `category_id` = ".$main_category_id." AND `is_deleted` = 0
                            ORDER BY
                                `saved_categorywise_restaurant`.`evaluation_cnt`
                            DESC
                        )   group by branch_master.main_branch_master_id having DISTANCE <= $limit_rangeKms
                        ORDER BY DISTANCE ASC ";
                $restaurant_detail_list = $this->firequery($saved_category_restaurant_query);
               $topFiveRestaurantIds = array();
               $sorting_array = [];
                if (!empty($restaurant_detail_list)) {
                    foreach ($restaurant_detail_list as $reskey => $resval) {
                        $query = "select cm.main_category_id,cm.category_name,cm.category_image_id from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $language_id . "' group by cm.main_category_id order by sort_order DESC";
                        $cat = null;//$this->firequery($query);
                        $category = null;
                          $sorting_array[] = array(
                              "restaurant_id" => $resval['main_restaurant_id'],
                              "evaluation_cnt" => $resval['evaluation_cnt'],
                              "main_branch_master_id" => $resval['main_branch_master_id']
                          );
                        $evaluation_res_array[] = array(
                          //  "category_id" => $catval['main_category_id'],
                          //  "category_name" => $catval['category_name'],
                            "restaurant_id" => $resval['main_restaurant_id'],
                            "topFiveRestaurantIds" => $topFiveRestaurantIds,
                            "in_top_five" => $in_top_five,
                            "main_branch_master_id" => $resval['main_branch_master_id'],
                            "branch_address_id" => $resval['branch_address_id'],
                            //	"branch_data" => $branch_data,
                            "shop_logo" => $this->getimage($resval['logo_id']),
                            "phone_number" => $resval['mobile_no'],
                            //"category" => $category,
                            "cuisines" => null,
                            "cuisines_str" => 'fast food,other,Arabic',
                            "distance" => $resval['DISTANCE'],
                            "lng" => $resval['lng'],
                            "lat" => $resval['lat'],
                            "restaurant_name" => $resval['restaurant_name'],
                            "evaluation_cnt" => $resval['evaluation_cnt'],
                            "total_evaluation" => $resval['total_evaluation'],
                            "points" => $resval['rating_point'],
                            "percentage" => $resval['rating_percentage'],
                        );
                    }
                }
                // var_dump($sorting_array);
                if (!empty($sorting_array)) {
                    foreach ($sorting_array as $key => $row) {
                        $price[$key] = $row['evaluation_cnt'];
                    }
                    array_multisort($price, SORT_DESC, $sorting_array);
                }
                
                $rest_id_array = $branch_id_array =  [] ;
                $copy_sorting_array = [] ;
                if (!empty($sorting_array)) {
                    foreach ($sorting_array as $key => $row) {
                        if(!in_array($row['restaurant_id'],$rest_id_array)){
                             $rest_id_array[] = $row['restaurant_id'];
                             $branch_id_array[] = $row['main_branch_master_id'];
                             $copy_sorting_array[$row['restaurant_id']] = $row ;
                        }                       
                       if(count($rest_id_array) == 5){
                           break;
                       }
                    }                   
                }
                  
              //  var_dump($map_flag );
                 if($map_flag == "false"){
                     if (!empty($evaluation_res_array)) {
                        foreach ($evaluation_res_array as $key => $row) {
                            $price[$key] = $row['in_top_five'];
                        }
                        array_multisort($price, SORT_DESC, $evaluation_res_array);
                    }
                }
                 if (!empty($evaluation_res_array)) {
                    foreach ($evaluation_res_array as $key => $row) {
                        
                        if($map_flag == 'true'){
                            if(in_array($row['restaurant_id'],$rest_id_array)  ){
                                //  $row['in_top_five'] = true;
                                 if(in_array($row['restaurant_id'],$rest_id_array) && ($copy_sorting_array[$row['restaurant_id']]['main_branch_master_id'] == $row['main_branch_master_id'] )){
                                  $evaluation_res_array[$key]['in_top_five'] = true;
                                  $evaluation_res_array[$key]['in_top_five_flag'] = 0;
                                 }
                                  $evaluation_res_array[$key]['topFiveRestaurantIds'] = $rest_id_array;
                                  

                                  // get cuisines
                                  $query = "select ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $row['restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$language_id and rfr.main_caetgory_id=" . $main_category_id;
                                  $cuisines = null;
                                  $cuisine = $this->firequery($query);

                                  if (!empty($cuisine)) {
                                              // echo $query ;
                                         // var_dump($cuisine);exit;
                                              $food_typeStr = [] ; 
                                              foreach ($cuisine as $val) {
                                                  $cuisines[] = array(
                                                      "food_type_id" => $val['main_food_type_id'],
                                                      "food_type_name" => $val['food_type_name'],
                                                    //  "food_type_image" => $this->getimage($val['food_type_image_id'])
                                                  );
                                                  $food_typeStr[] =  $val['food_type_name'] ;
                                              }
                                              $food_typeStr = implode(",",$food_typeStr);

                                              $evaluation_res_array[$key]['cuisines'] = $cuisines;
                                              $evaluation_res_array[$key]['cuisines_str'] = $food_typeStr;
                                          }
                            }
                            else{
                                 $evaluation_res_array[$key]['in_top_five_flag'] = 1 ;
                            }
                        }else{
                           
                            if(in_array($row['restaurant_id'],$rest_id_array) && ($copy_sorting_array[$row['restaurant_id']]['main_branch_master_id'] == $row['main_branch_master_id'] )){
                                //  $row['in_top_five'] = true;
                                  $evaluation_res_array[$key]['in_top_five'] = true;
                                  $evaluation_res_array[$key]['in_top_five_flag'] = 0;
                                  $evaluation_res_array[$key]['topFiveRestaurantIds'] = $rest_id_array;


                                  // get cuisines
                                  $query = "select ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $row['restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$language_id and rfr.main_caetgory_id=" . $main_category_id;
                                  $cuisines = null;
                                  $cuisine = $this->firequery($query);
                                 
                                  $food_typeStr = [] ; 
                                  if (!empty($cuisine)) {
                                              // echo $query ;
                                         // var_dump($cuisine);exit;
                                              $food_typeStr = [] ; 
                                              foreach ($cuisine as $val) {
                                                  $cuisines[] = array(
                                                      "food_type_id" => $val['main_food_type_id'],
                                                      "food_type_name" => $val['food_type_name'],
                                                    //  "food_type_image" => $this->getimage($val['food_type_image_id'])
                                                  );
                                                  $food_typeStr[] =  $val['food_type_name'] ;
                                              }
                                              $food_typeStr = implode(",",$food_typeStr);

                                              $evaluation_res_array[$key]['cuisines'] = $cuisines;
                                              $evaluation_res_array[$key]['cuisines_str'] = $food_typeStr;
                                          }
                            }
                            else{
                               // var_dump($evaluation_res_array);
                                 unset($evaluation_res_array[$key]);
                                 
                                // $evaluation_res_array = array_values($evaluation_res_array);
                            }
                        }
                       
                       
                    }                   
                }
              $evaluation_res_array = array_values($evaluation_res_array);
              $response = $evaluation_res_array ;
                    $this->error = "SFD";
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $this->error = "NRF";
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
//        } catch (\Exception $e) {
//            $this->error = "SFND " . $e->getMessage();
//            $this->data = false;
//            return $this->responseAction();
//        }
    }
}
?>