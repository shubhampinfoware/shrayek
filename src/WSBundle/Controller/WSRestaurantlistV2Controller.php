<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Organizationmaster;
use AdminBundle\Entity\Shopservicescategoryrelation;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Organizationservicearea;
use AdminBundle\Entity\Areamaster;
use AdminBundle\Entity\Shoptransactionhistory;

class WSRestaurantlistV2Controller extends WSBaseController {

    function uniord($u) {
        // i just copied this function fron the php.net comments, but it should work fine!
        $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
        $k1 = ord(substr($k, 0, 1));
        $k2 = ord(substr($k, 1, 1));
        return $k2 * 256 + $k1;
    }

    function is_arabic($str) {
        if(mb_detect_encoding($str) !== 'UTF-8') {
            $str = mb_convert_encoding($str,mb_detect_encoding($str),'UTF-8');
        }
    
        /*
        $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
        $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
        */
        preg_match_all('/.|\n/u', $str, $matches);
        $chars = $matches[0];
        $arabic_count = 0;
        $latin_count = 0;
        $total_count = 0;
        foreach($chars as $char) {
            //$pos = ord($char); we cant use that, its not binary safe 
            $pos = $this->uniord($char);
            //echo $char ." --> ".$pos.PHP_EOL;
    
            if($pos >= 1536 && $pos <= 1791) {
                $arabic_count++;
            } else if($pos > 123 && $pos < 123) {
                $latin_count++;
            }
            $total_count++;
        }
        if($arabic_count > 0) {
            // 60% arabic chars, its probably arabic
            return true;
        }
        return false;
    }
    
    /**
     * @Route("/ws/restaurant_list_v2/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function restaurant_list_v2Action($param) {

        //  try {
        $this->title = "Restaurant List V2";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );

        if ($this->validateData($param)) {

            $ENG_LANG_ID = 1;
            $ARABIC_LANG_ID = 2;

            $category_ids = !empty($param->category_ids) ? $param->category_ids : 0;
            $food_type_ids = !empty($param->food_type_ids) ? $param->food_type_ids : 0;

            $shop_id = !empty($param->shop_id) ? $param->shop_id : 0;
            $only_opening_soon = !empty($param->only_opening_soon) ? $param->only_opening_soon : '';
            $keyword = !empty($param->keyword) ? $param->keyword : '';
            $user_id = !empty($param->user_id) ? $param->user_id : '';
            $con = '';
            $con1 = '';

            $con2 = '';

            $lang_id = 1;
            if (isset($param->language_id)) {
                $lang_id = $param->language_id;
            }

            //$con .= ' and rm.language_id =' . $lang_id;
            $only_opening_soon = "";
			$res_status = " and rm.status='active'";
            if (isset($param->only_opening_soon)) {

                if ($param->only_opening_soon == 'true')
                    $status = 'open_soon';
                else
                    $status = 'active';
                $res_status = " and rm.status='" . $status . "'";
            }
			$con .= $res_status;
            $food_type_ids = json_decode($food_type_ids);
            if (!empty($food_type_ids) && is_array($food_type_ids)) {
                $food_type_ids = implode(',', $food_type_ids);
                if (!empty($food_type_ids)) {

                    $con1 .= ' and rfr.main_foodtype_id in (' . $food_type_ids . ')';
                }
            }

            if (!empty($shop_id)) {
                $con .= ' and rm.main_restaurant_id =' . $shop_id;
            }

            $category_ids = json_decode($category_ids);
            if (!empty($category_ids) && is_array($category_ids)) {
                $category_ids = implode(',', $category_ids);
                if (!empty($category_ids)) {

                    $con2 .= ' and cm.main_category_id in (' . $category_ids . ')';
                }
            }

            if (!empty($keyword)) {
                // $con .= " and (rm.restaurant_name LIKE '%$keyword%' or rm.description LIKE '%$keyword%' or cm.category_name LIKE '%$keyword%' or ftm.food_type_name LIKE '%$keyword%' )";
                // $con .= " and (rm.restaurant_name LIKE '%$keyword%' )";

                $name_filter = " and (rm.restaurant_name LIKE '%$keyword%' )";

                $is_arabic = $this->is_arabic($keyword);
                $isSearchById = false;
                if(($is_arabic) && ($lang_id != $ARABIC_LANG_ID)){
                    $isSearchById = true;
                } else if((!$is_arabic) && ($lang_id != $ENG_LANG_ID)){
                    $isSearchById = true;
                }

                if($isSearchById){
                    $_query = "SELECT main_restaurant_id FROM restaurant_master where restaurant_name LIKE '%$keyword%' and is_deleted = 0 and status = 'active' GROUP BY main_restaurant_id";
                    $main_id_res_list = $this->firequery($_query);

                    $main_id_res_arr = [];
                    if(!empty($main_id_res_list)){
                        foreach($main_id_res_list as $_mainId){
                            $main_id_res_arr[] = $_mainId['main_restaurant_id'];
                        }

                        $main_id_str = implode(',', $main_id_res_arr);
                        $name_filter = " and (rm.main_restaurant_id in ({$main_id_str}))";
                    }
                }

                $con .= $name_filter;
            }

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            // IN USED TILL 03/10/2019
			/* $st = $conn->prepare("SELECT rm.main_restaurant_id,rm.logo_id, rm.restaurant_name, rfr.main_caetgory_id
							from restaurant_master as rm
							left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
							left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
							left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
                            where rm.is_deleted=0 $con $con1 $con2 and rm.language_id=$lang_id and rm.is_deleted=0 and rm.status != 'inactive' and rfr.is_deleted = 0 group by rm.main_restaurant_id ORDER BY rm.restaurant_name ASC,cm.sort_order DESC");*/
            $st = $conn->prepare("SELECT rm.main_restaurant_id,rm.logo_id, rm.restaurant_name, rm.language_id, rfr.main_caetgory_id
							from restaurant_master as rm
							left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
							left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
							left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
							where rm.is_deleted=0 $con $con1 $con2 and rm.is_deleted=0 and rm.status != 'inactive' and rfr.is_deleted = 0 group by rm.restaurant_master_id ORDER BY rm.restaurant_name ASC,cm.sort_order DESC");
//            $st = $conn->prepare("SELECT rm.*,ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id,
//							cm.main_category_id,cm.category_name,cm.category_image_id
//							from restaurant_master as rm
//							left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
//							left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
//							left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
//							where rm.is_deleted=0 $con $con1 $con2 and rm.language_id=$lang_id and rm.is_deleted=0 and rm.status != 'inactive' group by rm.main_restaurant_id ORDER BY rm.restaurant_name ASC,cm.sort_order DESC");




            $st->execute();
            $shopList = $st->fetchAll();
			
            $tot_ev_points = 0;
            if (!empty($shopList)) {
                foreach ($shopList as $shopMaster) {
                
                    if($shopMaster['language_id'] != $lang_id){
                        continue;
                    }

					// check res food_type relation
					$check_res_foodtype_rel = $this->getDoctrine()->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy([
						'restaurant_id' => $shopMaster['main_restaurant_id'],
						'main_caetgory_id' => $shopMaster['main_caetgory_id'],
						'is_deleted' => 0
					]);

					if(!empty($check_res_foodtype_rel)){							
						// check cat food_type relation
						$is_related = false;
						foreach($check_res_foodtype_rel as $_catFoodtypeRel){
							$check_cat_foodtype_rel = $this->getDoctrine()->getRepository('AdminBundle:Foodtypecategoryrelation')->findOneBy([
								'main_category_id' => $_catFoodtypeRel->getMain_caetgory_id(),
								'main_food_type_id' => $_catFoodtypeRel->getMain_foodtype_id(),
								'is_deleted' => 0
							]);
							if(!empty($check_cat_foodtype_rel)){
								$is_related = true;
							}
						}
						
						if($is_related){
							$response[] = array(
								"shop_id" => $shopMaster['main_restaurant_id'],
								"shop_logo" => $this->getimage($shopMaster['logo_id']),
								"shop_name" => stripslashes($shopMaster['restaurant_name']),
							);
						}
					}
                }
            }
            if ($tot_ev_points != 0) {

                if (!empty($response)) {
                    foreach ($response as $akey => $aval) {
                        //var_dump($aval['shop']['total_points_of_shop']); exit;
                        $res_points = $aval['total_rating_count'];
                        $points_percentage = ( $res_points * 25 ) / 100;
                        $rate = ( $points_percentage * 5 ) / 100;
                        $response[$akey]['points_percentage'] = $points_percentage;
                        $response[$akey]['rate_value_with_decimal'] = $rate;
                        $response[$akey]['rate'] = number_format((float) $rate, 1, '.', '');
                    }
                }
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
            $this->error = "NRF";
        } else {
            $this->error = "SFD";
        }

        $this->data = $response;
        return $this->responseAction();
        /*   } catch (\Exception $e) {
          $this->error = "SFND " . $e;
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>
