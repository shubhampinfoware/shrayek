<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Offermaster;
use AdminBundle\Entity\Usermaster;
class WSOfferlistController extends WSBaseController {

	/**
	 * @Route("/ws/get_offers/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function get_offersAction($param)
	{
	   try{
			$this->title = "Get Offers";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(),
				),
			);
			if($this->validateData($param)){

				$language_id = !empty($param->language_id)?$param->language_id:'';
                                $restaurant_id = !empty($param->restaurant_id)?$param->restaurant_id:'';
				//$type = $param->type;
                                $where='';
                                if(!empty($restaurant_id)){
                                    $where .= ' and restaurant_id='.$restaurant_id;
                                }
                                    $query= "select * from restaurant_offer where is_deleted = 0 $where";
                                    $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
                                    $em->execute();
                                    $all_category = $em->fetchAll();
                                    if(count($all_category) > 0 )
                                    {
                                            foreach($all_category as $lval)
                                            {
                                                    $data[]= array("restaurant_id"=> $restaurant_id,
                                                    "offer_image"=>$this->getimage($lval['media_id']),

                                                    );

                                            }

                                    }
                               
				if(!empty($data))
				{
					$response = $data;
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   }
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
                        return $this->responseAction();
		}
	}
	
}
?>
