<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Competitionaward;

class WSCompetitionlistController extends WSBaseController {

    /**
     * @Route("/ws/competition_list/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function competition_listAction($param) {
        /* try{ */
        $this->title = "Competition List";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(),
            ),
        );
        if ($this->validateData($param)) {
            $language_id = 1;
            if (!empty($param->language_id))
                $language_id = $param->language_id;
            $response = array();
            $data = $all_category = '';

            $all_category = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Competitionaward')
                    ->findBy(array('is_deleted' => 0, 'language_id' => $language_id, 'status' => 'active'), array('created_date' => 'DESC'));

            if (count($all_category) > 0) {

                foreach (array_slice($all_category, 0) as $lkey => $lval) {

                    $gallery = $this->getcompetition_award_photo($lval->getMain_competition_award_id());
                    $data[] = array(
                        "competition_id" => $lval->getMain_competition_award_id(),
                        "competition_name" => $lval->getCompetition_award_title(),
                        "competition_description" => $lval->getDescription(),
                        "cover_image" => $this->getimage($lval->getCompetition_award_cover_image_id()),
                        "award_date" => (strtotime($lval->getCompetition_award_date()) != NULL )? strtotime($lval->getCompetition_award_date())*1000 : NULL,
                        "created_date" => (strtotime($lval->getCreated_date()) != NULL )? strtotime($lval->getCreated_date())*1000 : NULL,
                        "start_date" => (strtotime($lval->getStart_date()) != NULL )? strtotime($lval->getStart_date())*1000 : NULL,
                        "end_date" => (strtotime($lval->getEnd_date()) != NULL )? strtotime($lval->getEnd_date())*1000 : NULL,
                        "draw_date" => (strtotime($lval->getCompetition_award_date()) != NULL )? strtotime($lval->getCompetition_award_date())*1000 : NULL,
                        "competition_gallery" => !empty($gallery) ? $gallery : null
                    );
                }
            }
            if (!empty($data)) {
                $response = $data;
                $this->error = "SFD";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        return $this->responseAction();
        /* }
          catch (\Exception $e)
          {
          $this->error = "SFND";
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>