<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use AdminBundle\Entity\Suggestioncomplaints;

class WSAddSuggestionComplaintsController extends WSBaseController {

    /**
     * @Route("/ws/add_suggestion_complaints/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function addSuggestionComplaintsAction($param) {
        try {
            $this->title = "Add Suggestion and Complains";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
                        'name',
                        'subject',
						'email',
						'comments'
                    //'user_type'
                    )
            ));
			
			$subject_array = array('suggestion','complaints','others');
            if ($this->validateData($param)) {			
				if(in_array($param->subject,$subject_array)){

					$Suggestioncomplaints_new = new Suggestioncomplaints();
					$Suggestioncomplaints_new->setNick_name($param->name);
					$Suggestioncomplaints_new->setEmail_address($param->email);
					$Suggestioncomplaints_new->setSubject($param->subject);
					$Suggestioncomplaints_new->setComments($param->comments);
					$Suggestioncomplaints_new->setCreated_date(date('Y-m-d'));
					$Suggestioncomplaints_new->setDomain_id(1);
					$em = $this->getDoctrine()->getManager();
					$em->persist($Suggestioncomplaints_new);
					$em->flush();
					
					$suggestion_id = $Suggestioncomplaints_new->getSuggestion_complaints_id();
					$response = array('suggestion_id'=>$suggestion_id);
					$this->error = "SFD";
				}else{
					$this->error = "PIW";
				}
            } else {
                $this->error = "PIM";
            }

            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>
