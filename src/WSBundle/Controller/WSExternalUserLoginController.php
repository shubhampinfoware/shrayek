<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Usermaster;
use Symfony\Component\HttpFoundation\JsonResponse;

class WSExternalUserLoginController extends WSBaseController {

    /**
     * @Route("/ws/externaluserlogin/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function externaluserloginAction($param) {
        //try{			
        $this->title = "External User Login Post";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('email',  'device_name', 'device_token', 'device_id' ,'image_url','name'),
            ),
        );
        $em = $this->getDoctrine()->getManager();
        $response = array();
        $address_arr = [];
        $area = [];
        $img = $userAddress = "";
        /* if(!empty($username) && !empty($password)){ */
        //if(!empty($email) && !empty($password) && !empty($devicename) && !empty($regid) && !empty($domain_id))
        if ($this->validateData($param)) {
            $email = $param->email;
            
            $name = $param->name;
            $role_id = 3 ;
           $image_url = str_replace('\/','/',$param->image_url);
            $regid = $device_token = $param->device_token;
            $device_id = $param->device_id;
            $domain_id = 1 ;
            
            $devicename = strtolower($param->device_name);
            $em = $this->getDoctrine()->getManager();
            
            $userMaster = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(
                    array(
                        "email" => $email,"is_deleted" => 0,
                    ));
            if (!empty($userMaster)) {

                $userMaster2 = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(
                    array(
                        "email" => $email,"status" => 'active',"is_deleted" => 0,
                    ));
                    if(empty($userMaster2)){
                        $this->error = "UII";
                        $this->data = "User is inactive";
                        return $this->responseAction();
                    }

                $latest_address_info= $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted"=>0,"owner_id"=>$userMaster->getUser_master_id(),"base_address_type"=>'primary')); 
                $address_list = NULL ;        
                if(!empty($latest_address_info)  && $latest_address_info != NULL){
                    $address_list = $this->getaddressAction($latest_address_info->getAddress_master_id(),$language_id);
                }
                if (!empty($address_list)) {
                    $userAddress = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy( array("address_master_id" => $userMaster->getAddress_master_id(),"is_deleted" => 0) );
                }               
                //------------------------------------------------------------------------------                    
                $Usermaster = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(array("user_master_id" => $userMaster->getUser_master_id(), "is_deleted" => '0'));
                if (!empty($Usermaster) && !empty($devicename) && ($devicename == 'ios' || $devicename == 'iphone')) {
                    $Apnsuser = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Apnsuser")->findOneBy(
                            array(
                                "apns_regid" => $regid,
                                "device_id" => $device_id,
                                "user_id" => $userMaster->getUser_master_id(),
                                "is_deleted" => '0'
                            )  );


                    if (!empty($Apnsuser)) {
                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE device_id = '" . $device_id . "' and is_deleted=0");
                        $apns_user_delete->execute();


                        // delete all other with this device id	=0 				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE ( device_id = '0' OR device_id = '')");
                        $apns_user_delete->execute();

                        

                        $apns_user_info = new Apnsuser();
                        $apns_user_info->setApns_regid($regid);
                        $apns_user_info->setUser_id($userMaster->getUser_master_id());
                        $apns_user_info->setUser_type($userMaster->getUser_type());
                        $apns_user_info->setDevice_id($device_id);
                        $apns_user_info->setApp_id('CUST');
                        $apns_user_info->setDomain_id($domain_id);
                        $apns_user_info->setName($Usermaster->getUser_firstname());
                        $apns_user_info->setBadge("0");
                        $apns_user_info->setCreated_date(date('Y-m-d H:i:s'));
                        $apns_user_info->setIs_deleted(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($apns_user_info);
                        $em->flush();



                        // delete all other with this device id						
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE device_id = '" . $device_id . "' and is_deleted=0 and user_id != '" . $userMaster->getUser_master_id() . "'");
                        $apns_user_delete->execute();
                        // delete all other with this device id	=0 				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE ( device_id = '0' OR device_id = '')");
                        $apns_user_delete->execute();
                        $data = true;
                    } 
                    else {
                        // delete all other with this device id
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE device_id = '" . $device_id . "' and is_deleted=0");
                        $apns_user_delete->execute();
                        // delete all other with this device id	=0 				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE ( device_id = '0' OR device_id = '')");
                        $apns_user_delete->execute();

                        $apns_user_info = new Apnsuser();
                        $apns_user_info->setApns_regid($regid);
                        $apns_user_info->setUser_id($userMaster->getUser_master_id());
                        $apns_user_info->setUser_type($userMaster->getUser_type());
                        $apns_user_info->setDevice_id($device_id);
                        $apns_user_info->setApp_id('CUST');
                        $apns_user_info->setDomain_id($domain_id);
                        $apns_user_info->setName($Usermaster->getUser_firstname());
                        //$apns_user_info->setEmail($Usermaster->getUser_emailid());
                        $apns_user_info->setBadge("0");
                        $apns_user_info->setCreated_date(date('Y-m-d H:i:s'));
                        $apns_user_info->setIs_deleted(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($apns_user_info);
                        $em->flush();
                        $data = true;
                    }
                } 
                elseif (!empty($Usermaster) && !empty($devicename) && $devicename == 'android') {
                    $Gcmuser = $this->getDoctrine() ->getManager() ->getRepository("AdminBundle:Gcmuser")->findOneBy(array("gcm_regid" => $regid,"device_id" => $device_id, "user_id" => $userMaster->getUser_master_id(), "is_deleted" => '0'));
                    if (!empty($Gcmuser)) {                        
                        $Gcmuser->setGcm_regid($regid);
                        $Gcmuser->setUser_id($userMaster->getUser_master_id());
                        $Gcmuser->setUser_type($userMaster->getUser_type());
                        $Gcmuser->setDevice_id($device_id);
                        $Gcmuser->setApp_id('CUST');
                        $Gcmuser->setDomain_id($domain_id);
                        $Gcmuser->setName($Usermaster->getUser_firstname());
                        $Gcmuser->setCreated_date(date('Y-m-d H:i:s'));
                        $Gcmuser->setIs_deleted(0);
                        $Gcmuser->setUser_type('user');                        
                        $em->flush();
                        $data = true;

                        // ---------------delete all other with this device id	---------------					
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE device_id = '" . $device_id . " 'and is_deleted=0 and user_id != '" . $userMaster->getUser_master_id() . "'");
                        $gcm_user_delete->execute();
                        //----------------- delete all other with this device id = 0 ------------------				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE (device_id = '' OR device_id ='(null)' OR device_id = '0')");
                        $gcm_user_delete->execute();
                    }
                    else {
                        $gcm_user_info = new Gcmuser();
                        $gcm_user_info->setGcm_regid($regid);
                        $gcm_user_info->setUser_id($userMaster->getUser_master_id());
                        $gcm_user_info->setUser_type($userMaster->getUser_type());
                        $gcm_user_info->setDevice_id($device_id);
                        $gcm_user_info->setApp_id('CUST');
                        $gcm_user_info->setDomain_id($domain_id);
                        $gcm_user_info->setName($Usermaster->getUser_firstname());
                        //$gcm_user_info->setEmail($Usermaster->getUser_emailid());
                        $gcm_user_info->setCreated_date(date('Y-m-d H:i:s'));
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($gcm_user_info);
                        $em->flush();
                        $data = true;

                        // delete all other with this device id						
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE device_id = '" . $device_id . " 'and is_deleted=0 and user_id != '" . $userMaster->getUser_master_id() . "'");
                        $gcm_user_delete->execute();
                        // delete all other with this device id	=0 						
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE (device_id = '' OR device_id ='(null)' OR device_id = '0')");
                        $gcm_user_delete->execute();
                      
                    }
                }

                
                $response = array(
                    "usermaster_id" => $userMaster->getUser_master_id(),
                    "nick_name" => $userMaster->getUser_bio(),
                    "user_firstname" => $userMaster->getUser_firstname(),
                    "user_lastname" => $userMaster->getUser_lastname(),
                    "user_mobile" => $userMaster->getUser_mobile(),
                    "user_image" => $userMaster->getImage_url(),
                    "role_id" => $userMaster->getUser_role_id(),
                    "user_emailid" => $userMaster->getEmail(),
                    "date_of_birth" => strtotime($userMaster->getDate_of_birth())*1000,
                   // "user_phoneno" => $this->keyDecryptionAction($userMaster->getUser_mobile()),
                    "username" => $userMaster->getUsername(),
                    "language" => $userMaster->getCurrent_lang_id(),
                    "domain_id" => $userMaster->getDomain_id(),
                    "address" => $address_list
                        );
                
              
                $this->error = "SFD";
            }
            else {
               $date = date("Y-m-d H:i:s");
                    $timestamp = date('Y-m-d H:i:s', strtotime($date));
                    $address_list = array();
                    //if customer not found
                    $user_master_info_id = new Usermaster();
                    $user_master_info_id->setUser_role_id(3);
                    $user_master_info_id->setUser_bio($name);
                    $user_master_info_id->setUsername($email);
                    $user_master_info_id->setEmail($email);
              
                    if (isset($param->address_master_id)) {
                        $user_master_info_id->setAddress_master_id($param->address_master_id);
                    }
                    else{
                         $user_master_info_id->setAddress_master_id(0);
                    }
                    $user_master_info_id->setDate_of_birth(date('Y-m-d H:i:s'));
                    $user_master_info_id->setUser_gender('');
                    $user_master_info_id->setImage_url($image_url);
                    $user_master_info_id->setParent_user_id(0);
                    $user_master_info_id->setUser_mobile('');
                    $user_master_info_id->setUser_firstname('');
                     $user_master_info_id->setUser_image(0);
                    $user_master_info_id->setUser_lastname('');
                    $user_master_info_id->setLast_modified(date('Y-m-d H:i:s'));
                    $user_master_info_id->setLast_login(date('Y-m-d H:i:s'));
                    $user_master_info_id->setStatus('active');
                    $user_master_info_id->setUser_type('user');
                    $user_master_info_id->setDomain_id(1);
                    $user_master_info_id->setCreated_by(0);
                    $user_master_info_id->setCreated_datetime(date("Y-m-d H:i:s"));
                    $user_master_info_id->setLast_modified(date("Y-m-d H:i:s"));
                    $user_master_info_id->setLast_login(NULL);
                    $user_master_info_id->setPassword('');
                    $user_master_info_id->setShow_password('');
                    $em->persist($user_master_info_id);
                    $em->flush();
                    $user_id = $user_master_info_id->getUser_master_id();
                    /*                                 * ***** for IOS apns_reg_id issue solution ****** */
                    if ($devicename == 'IPHONE') {
                        if (isset($device_id) && isset($device_token)) {
                            $apns_user_del = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository("AdminBundle:Apnsuser")
                                    ->findOneBy(array("user_id" => $user_id, "domain_id" => 1, "app_id" => $app_id, "device_id" => $device_id));
                            if (!empty($apns_user_del)) {
                                $apns_user_del->setApns_regid($device_token);
                                $apns_user_del->setCreated_date(date('Y-m-d H:i:s'));
                                $apns_user_del->setUser_type('user');
                                $apns_user_del->setIs_deleted(0);
                                $em->flush();
                            } 
                            else {
                                
                                // remove user from this device ==================/
                                $apns_guest_user_del = $this->getDoctrine()
                                        ->getManager()
                                        ->getRepository("AdminBundle:Apnsuser")
                                        ->findBy(array("domain_id" => 1, "app_id" => $app_id, "device_id" => $device_id));
                                if (!empty($apns_guest_user_del)) {
                                    foreach ($apns_guest_user_del as $ggkey => $ggval) {
                                        $ggval->setIs_deleted(1);
                                        $em->flush();
                                    }
                                }
                                //==new gcm entry -----
                                $apns_user = new Apnsuser();
                                $apns_user->setApns_regid($device_token);
                                $apns_user->setUser_id($user_id);
                                $apns_user->setUser_type('user');
                                $apns_user->setDevice_id($device_id);
                                $apns_user->setApp_id($app_id);
                                $apns_user->setDomain_id(1);
                                $apns_user->setName($name . " / " . $contact_no);
                                $apns_user->setBadge(0);
                                $apns_user->setCreated_date(date("Y-m-d H:i:s"));
                                $apns_user->setIs_deleted(0);
                                $em->persist($apns_user);
                                $em->flush();
                            }
                        }
                    }
                    if ($devicename == 'ANDROID') {
                        $gcm_user_del = $this->getDoctrine()
                                ->getManager()
                                ->getRepository("AdminBundle:Gcmuser")
                                ->findOneBy(array("user_id" => $user_id, "domain_id" => 1, "app_id" => $app_id, "device_id" => $device_id));

                        if (!empty($gcm_user_del)) {
                            if ($device_token != '' && $device_token != NULL && !empty($device_token)) {
                                $gcm_user_del->setGcm_regid($device_token);
                            }
                            $gcm_user_del->setCreated_date(date('Y-m-d H:i:s'));
                            $gcm_user_del->setUser_type('user');
                            $gcm_user_del->setIs_deleted(0);
                            $em->persist($gcm_user_del);
                            $em->flush();
                        }
                        else {

                            // remove user from this device ==================/
                            $gcm_guest_user_del = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository("AdminBundle:Gcmuser")
                                    ->findBy(array("domain_id" => 1, "app_id" => $app_id, "device_id" => $device_id));
                            if (!empty($gcm_guest_user_del)) {
                                foreach ($gcm_guest_user_del as $ggkey => $ggval) {
                                    $ggval->setIs_deleted(1);
                                    $em->flush();
                                }
                            }
                            //==new gcm entry -----
                            $gcm_user = new Gcmuser();
                            $gcm_user->setGcm_regid($device_token);
                            $gcm_user->setUser_id($user_id);
                            $gcm_user->setUser_type('user');
                            $gcm_user->setDevice_id($device_id);
                            $gcm_user->setApp_id($app_id);
                            $gcm_user->setDomain_id($domain_id);
                            $gcm_user->setName($firstname . "  " . $lastname);
                            $gcm_user->setCreated_date(date("Y-m-d H:i:s"));
                            $gcm_user->setIs_deleted(0);
                            $em->persist($gcm_user);
                            $em->flush();
                        }
                    }
                    
                   $latest_address_info= $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted"=>0,"owner_id"=>$user_master_info_id->getUser_master_id(),"base_address_type"=>'primary')); 
                $address_list = NULL ;        
                if(!empty($latest_address_info)  && $latest_address_info != NULL){
                    $address_list = $this->getaddressAction($latest_address_info->getAddress_master_id(),$language_id);
                }
                if (!empty($address_list)) {
                    $userAddress = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy( array("address_master_id" => $user_master_info_id->getAddress_master_id(),"is_deleted" => 0) );
                } 
                     $response = array(
                         "usermaster_id" => $user_master_info_id->getUser_master_id(),
                    "nick_name" => $user_master_info_id->getUser_bio(),
                    "user_firstname" => $user_master_info_id->getUser_firstname(),
                    "user_lastname" => $user_master_info_id->getUser_lastname(),
                    "user_mobile" => $user_master_info_id->getUser_mobile(),
                    "user_image" => $image_url,
                    "role_id" => $user_master_info_id->getUser_role_id(),
                    "user_emailid" => $user_master_info_id->getEmail(),
                    "date_of_birth" => strtotime($user_master_info_id->getDate_of_birth())*1000,
                   // "user_phoneno" => $this->keyDecryptionAction($userMaster->getUser_mobile()),
                    "username" => $user_master_info_id->getUsername(),
                    "language" => $user_master_info_id->getCurrent_lang_id(),
                    "domain_id" => $user_master_info_id->getDomain_id(),
                    "address" => $address_list
                       
                    );
                      $this->error = "SFD";
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
        /* 	}catch(\Exception $e){
          $this->error = "SFND" ;
          $this->data = false ;
          return $this->responseAction() ;
          } */
    }

  
}

?>