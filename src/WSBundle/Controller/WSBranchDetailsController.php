<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Productmaster;
use AdminBundle\Entity\Votemaster;
use AdminBundle\Entity\Attributecategoryrelation;

class WSBranchDetailsController extends WSBaseController {

    /**
     * @Route("/ws/branch_details/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function branch_detailsAction($param) {
        /* try{ */
        $this->title = "Branch Details";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('branch_id'),
            ),
        );
        if ($this->validateData($param)) {
            $language_id = 1;
            if (!empty($param->language_id))
                $language_id = $param->language_id;
			
            $main_branch_id = $param->branch_id;
            $response = array();
			

			$filter = array('is_deleted' => 0,'main_branch_master_id'=>$main_branch_id,'status'=>'active','language_id' => $language_id, 'main_branch_master_id' => $main_branch_id);				
            
			$branch_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Branchmaster')
                    ->findOneBy($filter);
		
            if ($branch_list) {

				$address_details = null;
				
				$adress_master = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy(array('main_address_id'=>$branch_list->getBranch_address_id(),'language_id'=>$language_id));
				
				if($adress_master){
					$address_details = array(
										'address'=>$adress_master->getAddress_name(),
										'lat'=>$adress_master->getLat(),
										'lng'=>$adress_master->getLng(),
									);
				}
				
				$response = array(
								'main_branch_id'=>$branch_list->getMain_branch_master_id(),
								'branch_name'=>$branch_list->getBranch_name(),
								'main_restaurant_id'=>$branch_list->getMain_restaurant_id(),
								'status'=>$branch_list->getStatus(),
								'opening_date'=>$branch_list->getOpening_date(),
								'timing'=>$branch_list->getTimings(),
								'main_branch_flag'=>$branch_list->getMain_branch_flag(),
								'language_id'=>$branch_list->getLanguage_id(),
								'description'=>$branch_list->getDescription(),
								'address_details'=>$address_details
							);	
                $this->error = "SFD";
            }else{
				$response = false;
                $this->error = "NRF";
			}

            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        return $this->responseAction();
        /* }
          catch (\Exception $e)
          {
          $this->error = "SFND";
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>