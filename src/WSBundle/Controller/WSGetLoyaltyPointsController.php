<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Contactmaster;
use AdminBundle\Entity\Medicalreportimage;
use AdminBundle\Entity\Contactusfeedback;

class WSGetLoyaltyPointsController extends WSBaseController
{

    /**
     * @Route("/ws/getLoyaltyPoints/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getLoyaltyPointsAction($param)
    {
        try {
            $this->title = "Loyalty Points";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
                        'login_user_id'
                    )
                )
            );

            if ($this->validateData($param)) {
                $login_user_id = $param->login_user_id;
                $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                $last_day_this_month  = date('Y-m-t');
                //                        var_dump($first_day_this_month);
                //                        var_dump($last_day_this_month); 
                //                        exit;          
                $con = $this->getDoctrine()->getManager()->getConnection();

                $OverAllsql_activitys_users = "select *,competition_master.name as competition_name,competition_master.name_ar as competition_name_ar,competition_master.description,competition_user_relation.created_datetime as created_on,
				SUM(competition_user_relation.points) as totalPoints 	
				from competition_user_relation 
				JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
				JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
				where competition_user_relation.user_id = '$login_user_id' 
				and competition_user_relation.is_deleted= 0 
				and competition_master.is_deleted= 0 
				and competition_master.status= 'active' 
				GROUP BY competition_user_relation.competition_id 
				ORDER BY competition_user_relation.created_datetime DESC";
                //echo $sql_activitys_users;exit;
                $stmt = $con->prepare($OverAllsql_activitys_users);
                $stmt->execute();
                $OveraAllPointsDetails = $stmt->fetchAll();
                $overAllPoints = 0;
                if (!empty($OveraAllPointsDetails)) {
                    foreach ($OveraAllPointsDetails as $_OveraAllPointsDetails) {
                        $overAllPoints = $overAllPoints + $_OveraAllPointsDetails['totalPoints'];
                    }
                }

                $sql_activitys_users = "select *,competition_master.name as competition_name,competition_master.name_ar as competition_name_ar,competition_master.description,competition_user_relation.created_datetime as created_on,
				SUM(competition_user_relation.points) as totalPoints 	
				from competition_user_relation 
				JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
				JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
				where competition_user_relation.user_id = '$login_user_id' 
				and competition_user_relation.is_deleted= 0 
				and competition_master.is_deleted= 0 
				and competition_master.status= 'active' 
				GROUP BY competition_user_relation.competition_id 
				ORDER BY competition_user_relation.created_datetime DESC";
                //and ( competition_user_relation.created_datetime >= '$first_day_this_month'  and competition_user_relation.created_datetime <= '$last_day_this_month' )
                //echo $sql_activitys_users;exit;
                $stmt = $con->prepare($sql_activitys_users);
                $stmt->execute();
                $pointsDetails = $stmt->fetchAll();

                $userTotalPoints = 0;

                if (!empty($pointsDetails)) {

                    foreach ($pointsDetails as $_pointsDetails) {

                        if (1) { //$_pointsDetails['is_special'] == 'yes'){
                            #getCompTotalPoints
                            $sql = "select SUM(points) as totalCompPoints from competition_activity_relation WHERE competition_id = " . $_pointsDetails['competition_id'] . " AND is_deleted = 0";
                            $stmt1 = $con->prepare($sql);
                            $stmt1->execute();
                            $totalPointsOfCompData = $stmt1->fetchAll();

                            $totalPointsOfComp = !empty($totalPointsOfCompData) ? $totalPointsOfCompData[0]['totalCompPoints'] : 0;
                            #getCompTotalPoints done

                            $userTotalPoints = $userTotalPoints + $_pointsDetails['totalPoints'];
                            if (strtotime(date("Y-m-d H:i:s")) < strtotime($_pointsDetails['end_date'])) {
                                $response['ongoing_details'][] = array(
                                    'points' => $_pointsDetails['totalPoints'],
                                    'total_points_of_comp' => $totalPointsOfComp,
                                    //	'sql' => $sql,
                                    'competition_name' => $_pointsDetails['competition_name'],
                                    'competition_name_ar' => $_pointsDetails['competition_name_ar'],
                                    'competition_description' => $_pointsDetails['description'],
                                    'competition_id' => $_pointsDetails['competition_id'],
                                    //	'activity_title' => $_pointsDetails['activity_title'],
                                    'points_get_on' => strtotime($_pointsDetails['created_on']),
                                    'start_date' => strtotime($_pointsDetails['start_date']),
                                    'end_date' => strtotime($_pointsDetails['end_date']),
                                    'start_date1' => $_pointsDetails['start_date'],
                                    'end_date1' => $_pointsDetails['end_date'],
                                    'is_special' => ($_pointsDetails['is_special'] == "yes") ? true : false,
                                    'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                                );
                            } else {
                                $response['completed'][] = array(
                                    'points' => $_pointsDetails['totalPoints'],
                                    'total_points_of_comp' => $totalPointsOfComp,
                                    //	'sql' => $sql,
                                    'competition_name' => $_pointsDetails['competition_name'],
                                    'competition_name_ar' => $_pointsDetails['competition_name_ar'],
                                    'competition_description' => $_pointsDetails['description'],
                                    'competition_id' => $_pointsDetails['competition_id'],
                                    //	'activity_title' => $_pointsDetails['activity_title'],
                                    'points_get_on' => strtotime($_pointsDetails['created_on']),
                                    'start_date' => strtotime($_pointsDetails['start_date']),
                                    'end_date' => strtotime($_pointsDetails['end_date']),
                                    'start_date1' => $_pointsDetails['start_date'],
                                    'end_date1' => $_pointsDetails['end_date'],
                                    'is_special' => ($_pointsDetails['is_special'] == "yes") ? true : false,
                                    'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                                );
                            }
                        } else {
                            #getCompTotalPoints
                            $sql = "select SUM(points) as totalCompPoints from competition_activity_relation WHERE competition_id = " . $_pointsDetails['competition_id'] . " AND is_deleted = 0";
                            $stmt1 = $con->prepare($sql);
                            $stmt1->execute();
                            $totalPointsOfCompData = $stmt1->fetchAll();

                            $totalPointsOfComp = !empty($totalPointsOfCompData) ? $totalPointsOfCompData[0]['totalCompPoints'] : 0;
                            #getCompTotalPoints done

                            $userTotalPoints = $userTotalPoints + $_pointsDetails['totalPoints'];
                            if (strtotime(date("Y-m-d H:i:s")) < strtotime($_pointsDetails['end_date'])) {
                                $response['ongoing_competion_details'][] = array(
                                    'points' => $_pointsDetails['totalPoints'],
                                    'total_points_of_comp' => $totalPointsOfComp,
                                    //	'sql' => $sql,
                                    'competition_name' => $_pointsDetails['competition_name'],
                                    'competition_name_ar' => $_pointsDetails['competition_name_ar'],
                                    'competition_description' => $_pointsDetails['description'],
                                    'competition_id' => $_pointsDetails['competition_id'],
                                    //	'activity_title' => $_pointsDetails['activity_title'],
                                    'points_get_on' => strtotime($_pointsDetails['created_on']),
                                    'start_date' => strtotime($_pointsDetails['start_date']),
                                    'end_date' => strtotime($_pointsDetails['end_date']),
                                    'is_special' => ($_pointsDetails['is_special'] == "yes") ? true : false,
                                    'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                                );
                            } else {
                                $response['completed'][] = array(
                                    'points' => $_pointsDetails['totalPoints'],
                                    'total_points_of_comp' => $totalPointsOfComp,
                                    //	'sql' => $sql,
                                    'competition_name' => $_pointsDetails['competition_name'],
                                    'competition_name_ar' => $_pointsDetails['competition_name_ar'],
                                    'competition_description' => $_pointsDetails['description'],
                                    'competition_id' => $_pointsDetails['competition_id'],
                                    //	'activity_title' => $_pointsDetails['activity_title'],
                                    'points_get_on' => strtotime($_pointsDetails['created_on']),
                                    'start_date' => strtotime($_pointsDetails['start_date']),
                                    'end_date' => strtotime($_pointsDetails['end_date']),
                                    'is_special' => ($_pointsDetails['is_special'] == "yes") ? true : false,
                                    'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                                );
                            }
                        }
                    }
                    $response['users_total_points'] = $overAllPoints; //$userTotalPoints;

                    $this->error = "SFD";
                } else {
                    $this->error = "NRF";
                }
                if (!isset($response['ongoing_competion_details'])) {
                    // $response ['ongoing_competion_details'] = NULL ;
                }
                if (!isset($response['ongoing_special_competion_details'])) {
                    //   $response ['ongoing_special_competion_details'] = NULL ;
                }
                if (!isset($response['users_total_points'])) {
                    $response['users_total_points'] = NULL;
                }
                //  $response['users_Overall_points'] = $overAllPoints ; //$userTotalPoints;
            } else {
                $this->error = "PIM";
            }

            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }
    /**
     * @Route("/ws/getLoyaltyPointsHistory/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getLoyaltyPointsHistoryAction($param)
    {
        //  try {
        $this->title = "Loyalty Points History";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array(
                    'login_user_id'
                )
            )
        );
        $con = $this->getDoctrine()->getManager()->getConnection();
        if ($this->validateData($param)) {
            $login_user_id = $param->login_user_id;
            $start_loop = 1;
            $upto_loop = (int) date('m');
            for ($start_loop = 1; $start_loop <= $upto_loop; $start_loop++) {
                $first_day_this_month = date('Y-' . $start_loop . '-01'); // hard-coded '01' for first day
                $last_day_this_month  = date('Y-' . $start_loop . '-t');

                $response = NULL;
                $sql_activitys_users = "select *,competition_master.name as competition_name,competition_master.name_ar as competition_name_ar,competition_master.description,competition_user_relation.created_datetime as created_on,
				SUM(competition_user_relation.points) as totalPoints 	
				from competition_user_relation 
				JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
				JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
				where competition_user_relation.user_id = '$login_user_id' 
				and competition_user_relation.is_deleted= 0 
				and ( competition_user_relation.created_datetime >= '$first_day_this_month'  and competition_user_relation.created_datetime <= '$last_day_this_month' )
				GROUP BY competition_user_relation.competition_id 
				ORDER BY competition_user_relation.created_datetime DESC";
                //echo $sql_activitys_users;exit;
                $stmt = $con->prepare($sql_activitys_users);
                $stmt->execute();
                $pointsDetails = $stmt->fetchAll();

                $userTotalPoints = 0;
                $response1 = $response = NULL;
                if (!empty($pointsDetails)) {

                    foreach ($pointsDetails as $_pointsDetails) {

                        if ($_pointsDetails['is_special'] == 'yes') {
                            #getCompTotalPoints
                            $sql = "select SUM(points) as totalCompPoints from competition_activity_relation WHERE competition_id = " . $_pointsDetails['competition_id'] . " AND is_deleted = 0";
                            $stmt1 = $con->prepare($sql);
                            $stmt1->execute();
                            $totalPointsOfCompData = $stmt1->fetchAll();

                            $totalPointsOfComp = !empty($totalPointsOfCompData) ? $totalPointsOfCompData[0]['totalCompPoints'] : 0;
                            #getCompTotalPoints done

                            $userTotalPoints = $userTotalPoints + $_pointsDetails['totalPoints'];

                            $response['special_competion_details'][] = array(
                                'points' => $_pointsDetails['totalPoints'],
                                'total_points_of_comp' => $totalPointsOfComp,
                                //	'sql' => $sql,
                                'competition_name' => $_pointsDetails['competition_name'],
                                'competition_name_ar' => $_pointsDetails['competition_name_ar'],
                                'competition_description' => $_pointsDetails['description'],
                                'competition_id' => $_pointsDetails['competition_id'],
                                //	'activity_title' => $_pointsDetails['activity_title'],
                                'points_get_on' => strtotime($_pointsDetails['created_on']),
                                'start_date' => strtotime($_pointsDetails['start_date']),
                                'end_date' => strtotime($_pointsDetails['end_date']),
                                'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                            );
                        } else {
                            #getCompTotalPoints
                            $sql = "select SUM(points) as totalCompPoints from competition_activity_relation WHERE competition_id = " . $_pointsDetails['competition_id'] . " AND is_deleted = 0";
                            $stmt1 = $con->prepare($sql);
                            $stmt1->execute();
                            $totalPointsOfCompData = $stmt1->fetchAll();

                            $totalPointsOfComp = !empty($totalPointsOfCompData) ? $totalPointsOfCompData[0]['totalCompPoints'] : 0;
                            #getCompTotalPoints done

                            $userTotalPoints = $userTotalPoints + $_pointsDetails['totalPoints'];

                            $response['competion_details'][] = array(
                                'points' => $_pointsDetails['totalPoints'],
                                'total_points_of_comp' => $totalPointsOfComp,
                                //	'sql' => $sql,
                                'competition_name' => $_pointsDetails['competition_name'],
                                'competition_name_ar' => $_pointsDetails['competition_name_ar'],
                                'competition_description' => $_pointsDetails['description'],
                                'competition_id' => $_pointsDetails['competition_id'],
                                //	'activity_title' => $_pointsDetails['activity_title'],
                                'points_get_on' => strtotime($_pointsDetails['created_on']),
                                'start_date' => strtotime($_pointsDetails['start_date']),
                                'end_date' => strtotime($_pointsDetails['end_date']),
                                'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                            );
                        }
                    }
                    $response1['users_total_points'] =  $userTotalPoints; //$userTotalPoints;

                    $this->error = "SFD";
                }
                if (!isset($response['competion_details'])) {
                    $response['competion_details'] = NULL;
                }
                if (!isset($response['special_competion_details'])) {
                    $response['special_competion_details'] = NULL;
                }
                if (!isset($response['users_total_points'])) {
                    $response['users_total_points'] = NULL;
                }
                $year = date("Y");
                $datepass = date("M", strtotime($year . "-" . $start_loop . "-01"));
                $response_month[$datepass] = $userTotalPoints;
            }


            //                              

        } else {
            $this->error = "PIM";
        }

        if (empty($response_month)) {
            $response_month = false;
        }
        $this->data = $response_month;
        return $this->responseAction();
        //        } catch (\Exception $e) {
        //            $this->error = "SFND";
        //            $this->data = false;
        //            return $this->responseAction();
        //        }
    }

    /**
     * @Route("/ws/getLoyaltyPointsDetails/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getLoyaltyPointsDetailsAction($param)
    {
        try {
            $this->title = "Loyalty Points Details";
            $param = $this->requestAction($this->getRequest(), 0);

            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
                        'login_user_id'
                    )
                )
            );

            if ($this->validateData($param)) {
                $login_user_id = $param->login_user_id;

                $con = $this->getDoctrine()->getManager()->getConnection();

                $sql_activitys_users = "select *,competition_master.name as competition_name,competition_master.description,
				competition_user_relation.created_datetime as created_on,				
				SUM(competition_user_relation.points) as totalPoints 	
				from competition_user_relation 
				JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
				JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
				where competition_user_relation.user_id = '$login_user_id' 
				and competition_user_relation.is_deleted= 0 
				ORDER BY competition_user_relation.created_datetime DESC";

                $stmt = $con->prepare($sql_activitys_users);
                $stmt->execute();
                $pointsDetails = $stmt->fetchAll();

                $userTotalPoints = 0;

                if (!empty($pointsDetails)) {

                    foreach ($pointsDetails as $_pointsDetails) {

                        #getCompTotalPoints
                        $sql = "select SUM(points) as totalCompPoints from competition_activity_relation WHERE competition_id = " . $_pointsDetails['competition_id'] . " AND is_deleted = 0";
                        $stmt1 = $con->prepare($sql);
                        $stmt1->execute();
                        $totalPointsOfCompData = $stmt1->fetchAll();

                        $totalPointsOfComp = !empty($totalPointsOfCompData) ? $totalPointsOfCompData[0]['totalCompPoints'] : 0;
                        #getCompTotalPoints done

                        $userTotalPoints = $userTotalPoints + $_pointsDetails['totalPoints'];

                        $response['competion_details'][] = array(
                            'points' => $_pointsDetails['totalPoints'],
                            'total_points_of_comp' => $totalPointsOfComp,
                            //	'sql' => $sql,
                            'competition_name' => $_pointsDetails['competition_name'],
                            'competition_description' => $_pointsDetails['description'],
                            'competition_id' => $_pointsDetails['competition_id'],
                            'activity_title' => $_pointsDetails['activity_title'],
                            'points_get_on' => strtotime($_pointsDetails['created_on']),
                            'start_date' => strtotime($_pointsDetails['start_date']),
                            'end_date' => strtotime($_pointsDetails['end_date']),
                            'rule_book' => $this->getimage($_pointsDetails['rule_book'])
                        );
                    }
                    $response['users_total_points'] = $userTotalPoints;
                    $this->error = "SFD";
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }

            if (empty($response)) {
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }
}
