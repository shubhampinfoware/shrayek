<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Medicalreport;
use AdminBundle\Entity\Attributecategoryrelation;

class WSCategorylistController extends WSBaseController {

    /**
     * @Route("/ws/categories/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function categoriesAction($param) {
        /* try{ */
        $this->title = "Category List";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('language_id'),
            ),
        );
        if ($this->validateData($param)) {
            $language_id = $param->language_id;
            $response = array();
            $data = $all_category = '';
            $query1 = "select * from category_master where is_deleted=0 and language_id=$language_id and category_status='active' order by sort_order desc";

            $all_category = $this->firequery($query1);
//            $all_category = $this->getDoctrine()
//                    ->getManager()
//                    ->getRepository('AdminBundle:Categorymaster')
//                    ->findBy(array('is_deleted' => 0, 'language_id' => $language_id, 'category_status' => 'active'),array(),array('sort_order','desc'));

            if (count($all_category) > 0) {

                foreach (array_slice($all_category, 0) as $lkey => $lval) {

                    $query = "select * from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id={$language_id} and rfr.main_caetgory_id={$lval['category_master_id']} group by ftm.food_type_master_id";

                    $foodtype = $this->firequery($query);

                    $food_type = array();
                    foreach ($foodtype as $_foodtype) {
                        $food_type[] = array(
                            'food_type_id' => $_foodtype['food_type_master_id'],
                            'food_type_name' => $_foodtype['food_type_name'],
                            'food_type_image' => $this->getimage($_foodtype['food_type_image_id']),
                        );
                    }

                 
                    $data[] = array(
                        "category_id" => $lval['main_category_id'],
                        "category_name" => $lval['category_name'],
                        "category_logo" => $this->getimage($lval['category_image_id']),
                        "category_logo_id" => $lval['category_image_id'],
                        "food_type" => $food_type
                    );
                }
            }
            if (!empty($data)) {
                $response = $data;
                $this->error = "SFD";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        return $this->responseAction();
        /* }
          catch (\Exception $e)
          {
          $this->error = "SFND";
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>