<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use AdminBundle\Entity\Restaurantmaster;
use AdminBundle\Entity\Branchmaster;
use AdminBundle\Entity\Languagemaster;

class WSAddRestarauntController extends WSBaseController {
	
	/**
	 * @Route("/ws/add_restaraunt/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function add_restarauntAction($param)
	{
	   /*try{*/
			$this->title = "Add Restaraunt";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('restaraunt_name'),
				),
			);
			if($this->validateData($param)){
				$rest_name = $param->restaraunt_name;
				
				$branch_name_can = $rest_name."_Delivery";
				
				$branch_name = !empty($param->branch_name) ? $param->branch_name : $branch_name_can;
				
				$flag = 0;
				$flag1 = 0;
				$main_rest_id = 0;
				$languages = $this->getDoctrine()->getManager()->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
				if(!empty($languages)){
					foreach($languages as $lang){																						
						$Restaurantmaster_new = new Restaurantmaster();
						$Restaurantmaster_new->setRestaurant_name($rest_name);
						$Restaurantmaster_new->setDescription('');
						$Restaurantmaster_new->setRestaraunt_branch($branch_name);
						$Restaurantmaster_new->setLogo_id(0);
						$Restaurantmaster_new->setAddress_id(0);
						$Restaurantmaster_new->setStatus('pending');
						$Restaurantmaster_new->setLanguage_id($lang->getLanguage_master_id());
						$Restaurantmaster_new->setMain_restaurant_id(0);
						$Restaurantmaster_new->setDomain_id(1);
						$Restaurantmaster_new->setTimings('');      
						$Restaurantmaster_new->setOpening_date(NULL);      
						$Restaurantmaster_new->setIs_deleted(1);      
						$em = $this->getDoctrine()->getManager();
						$em->persist($Restaurantmaster_new);
						$em->flush();
						if($flag == 0){
							$main_rest_id = $Restaurantmaster_new->getRestaurant_master_id();
							$flag = 1;
						}
						$Restaurantmaster_new->setMain_restaurant_id($main_rest_id);
						$em->flush();	

#make entry for branch as well, as IsDeleted = 1	

						$new_branch_master = new Branchmaster();
						$new_branch_master->setMain_restaurant_id($main_rest_id);
						$new_branch_master->setBranch_name($branch_name);
						$new_branch_master->setBranch_address_id(0);
						$new_branch_master->setMain_branch_flag(1);
						$new_branch_master->setStatus('pending');
						$new_branch_master->setOpening_date(NULL);
						$new_branch_master->setTimings('');
						$new_branch_master->setDescription('');						
						$new_branch_master->setLanguage_id($lang->getLanguage_master_id());
						$new_branch_master->setMain_branch_master_id(0);
						$new_branch_master->setIs_deleted(1);
						$new_branch_master->setMobile_no(0);
						$em->persist($new_branch_master);
						$em->flush();
						if($flag1 == 0){
							$main_branch_id = $new_branch_master->getBranch_master_id();
							$flag1 = 1;
						}
						$new_branch_master->setMain_branch_master_id($main_branch_id);
						$em->flush();			

#make entry for branch as well, as IsDeleted = 1 ends	
			
					}
					$this->error = "SFD" ;
					$this->data = array('restaraunt_main_id'=>$Restaurantmaster_new->getMain_restaurant_id(),'main_branch_id'=>$new_branch_master->getMain_branch_master_id());
				}
			}
			else{
				$this->error = "PIM" ;
			}

			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

}
?>