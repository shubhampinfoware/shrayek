<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Medicalreport;
use AdminBundle\Entity\Attributecategoryrelation;
class WSSearchfoodtypeController extends WSBaseController {
	
	/**
	 * @Route("/ws/searchfoodtype/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	   public function searchfoodtypeAction($param)
	{
	   /*try{*/
			$this->title = "Search Food Type";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(),
				),
			);
			if($this->validateData($param)){
				$language_id = !empty($param->language_id)?$param->language_id:'1';
                                $category_id = !empty($param->category_id)?$param->category_id:'';
                                $keyword = !empty($param->keyword)?$param->keyword:'';
                                $food_type_id = !empty($param->food_type_id)?$param->food_type_id:'';
				$response = array();
                                $where='';
                                if(!empty($keyword)){
                                    $where .= ' AND (food_type_master.food_type_name LIKE "%'.$keyword.'%" OR category_master.category_name LIKE "%'.$keyword.'%" OR restaurant_master.restaurant_name LIKE "%'.$keyword.'%" OR restaurant_master.description LIKE "%'.$keyword.'%")';
                                }
                                if(!empty($food_type_id)){
                                   $where .= ' AND food_type_master.main_food_type_id='.$food_type_id; 
                                }
                                if(!empty($food_type_id)){
                                   $where .= ' AND category_master.main_category_id='.$category_id; 
                                }
				$em = $this->getDoctrine()->getManager();
		                $connection = $em->getConnection();
		                $query = "SELECT * from food_type_master left join restaurant_foodtype_relation as acr on acr.main_foodtype_id=food_type_master.main_food_type_id left join restaurant_master on restaurant_master.main_restaurant_id=acr.restaurant_id left join category_master on category_master.main_category_id=acr.main_caetgory_id where food_type_master.is_deleted=0 and acr.main_caetgory_id=". $category_id." and  acr.is_deleted=0 and restaurant_master.is_deleted=0 and restaurant_master.language_id=$language_id and food_type_master.language_id=$language_id $where group by acr.restaurant_id";
		                $get_post_list = $connection->prepare($query);
		                $get_post_list->execute();
		                $attributes = $get_post_list->fetchAll();
				if(count($attributes) > 0 )
				{
                                    
					foreach(array_slice($attributes,0) as $lkey=>$lval)
					{
                                             $query1 = "SELECT * from address_master where main_address_id='".$lval['address_id']."' and language_id=". $language_id." and is_deleted=0";
                                                $addressda = $connection->prepare($query1);
                                                $addressda->execute();
                                                $addresss = $addressda->fetchAll();
                                            $gallery = $this->getshopcoverimage($lval['main_restaurant_id']);
                                            $data[]= array("shop_id"=> $lval['main_restaurant_id'],
                                                           "shop_name"=>$lval['restaurant_name'],
                                                           "short_description"=>$lval['description'],
                                                           "long_description"=>$lval['description'],
                                                           "shop_logo"=>$this->getimage($lval['logo_id']),
                                                           "shop_cover_images"=>!empty($gallery)?$gallery:null,
                                                           "total_rating_count"=>0,
                                                           "rating"=>0,
                                                           "cuisines"=>0,
                                                           "phone_number"=>0,
                                                           "address"=>!empty($addresss)?$addresss[0]['main_address_id']:0,
                                                           "lat"=>!empty($addresss)?$addresss[0]['lat']:0,
                                                           "lng"=>!empty($addresss)?$addresss[0]['lng']:0,
                                                     );
						
					}
					
				}
				if(!empty($data))
				{
					$response = $data;
					$this->error = "SFD" ;
				}
				if(empty($response)){
					$response = false ;
					$this->error = "NRF" ;
				}
				$this->data = $response ;
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

}
?>