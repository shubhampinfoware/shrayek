<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Medicalreport;
use AdminBundle\Entity\Attributecategoryrelation;

class WSGetDiningExperienceController extends WSBaseController {

    /**
     * @Route("/ws/get_dining_experience/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function get_dining_experienceAction($param) {
        /* try{ */
        $this->title = "Get Dining Experience";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('langauge_id'),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = !empty($param->user_id) ? $param->user_id : '';
            $langauge_id = $param->langauge_id;
            $response = array();
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
			
/*            $query = "select dining_experience.* , restaurant_master.restaurant_name from dining_experience JOIN restaurant_master ON dining_experience.restaurant_id = restaurant_master.main_restaurant_id where dining_experience.is_deleted = 0 and restaurant_master.is_deleted = 0";
*/			
            $query = "select dining_experience.* from dining_experience where dining_experience.is_deleted = 0 and dining_experience.status='active'
			           order by create_date DESC";
			
            $get_post_list = $connection->prepare($query);
            $get_post_list->execute();
            $attributes = $get_post_list->fetchAll();
            if (count($attributes) > 0) {

                foreach (array_slice($attributes, 0) as $lkey => $lval) {
//                                            $query1 = "SELECT * from restaurant_master where restaurant_master.is_deleted=0 and restaurant_master.restaurant_master_id='".$lval['restaurant_id']."' and restaurant_master.language_id=$langauge_id";
//                                            $get_post_list1 = $connection->prepare($query1);
//                                            $get_post_list1->execute();
//                                            $attributes1 = $get_post_list1->fetch();
                    if (!empty($lval['restaurant_id'])) {
                        $data[] = array("dining_experience_id" => $lval['dining_experience_id'],
                            "nick_name" => $lval['nick_name'],
                            "comments" => $lval['comments'],
                            "restaurant_name" => $lval['restaurant_id'],
                            "branch_name" => $lval['branch_id'],
                            "photo" => $this->getdiningexperiencephoto($lval['dining_experience_id']),
                            "create_date" => strtotime($lval['create_date'])
                        );
                    }
                }
            }
            if (!empty($data)) {
                $response = $data;
                $this->error = "SFD";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        return $this->responseAction();
        /* }
          catch (\Exception $e)
          {
          $this->error = "SFND";
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>
