<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Aboutus;

class WSReferfriendController extends WSBaseController {

    /**
     * @Route("/ws/referfriend/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function subjectListAction($param) {
        try {
            $this->title = "Refer friend";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
						'user_id',
						'domain_id'
					),
                ),
            );
            if ($this->validateData($param)) {
				
				$user_id = $param->user_id;
				$domain_id = $param->domain_id;
				
				$user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findOneBy(
					array(
						'domain_id' => $domain_id,
						'user_master_id' => $user_id,
						'is_deleted' => 0
					)
                );
                
                if (!empty($user)) {
					$response = array(
						'user_code' => $this->keyEncryptionAction($user->getUser_master_id())
					);
                    $this->error = "SFD";
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
			echo $e->getMessage();exit;
            $this->error = "SFND" . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }
}

?>