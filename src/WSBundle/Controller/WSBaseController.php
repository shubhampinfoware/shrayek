<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Usersetting;
use AdminBundle\Entity\Apppushnotificationmaster;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Apptypemaster;
use AdminBundle\Entity\Appdetails;
use AdminBundle\Entity\Competitionuserrelation;
use AdminBundle\Entity\Bwidwssecurity;

class WSBaseController extends Controller {

    public $title = "";
    public $error = "SFND";
    public $error_msg = "";
    public $data = false;
    public $validateRule = array();

    public function __construct() {
//        date_default_timezone_set("Asia/Calcutta");
		date_default_timezone_set('Asia/Kuwait');		
    }

    /**
     * @Route("/ws/")
     * @Template()
     */
    public function indexAction() {
        return array();
    }

    public function requestAction($req, $allowed_method) {
        // allowed type 0 = allowed Both request
        // allowed type 1 = allowed only GET  request
        // allowed type 2 = allowed only POST request
        //var_dump($req->getContent());
        //var_dump($req->get('param'));
        //var_dump(json_encode($req->request->all()));
        //$allowed_method =1 ;
        $request_param = '';
        switch ($req->getMethod()) {
            case 'GET':
                if ($allowed_method != 2) {
                    $request_param = $req->get('param');
                }
                break;
            case 'POST':
                if ($allowed_method != 1) {
                    $request_param = json_encode($req->request->all());
                    //$request_param = $req->getContent() ;
                }
                break;
            default :
                $request_param = array();
                break;
        }
        //var_dump(json_decode($request_param));
        //exit;
        if (!empty($request_param)) {
            //$param= json_decode(stripcslashes($request_param));
            $param = json_decode($request_param);

            if (isset($param->token) && !empty($param->token)) {
                $bwid_token = $param->token;
                $obj_security = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Bwidwssecurity')
                        ->findBy(array('token' => $bwid_token));

                if (empty($obj_security)) {

                    $this->token = 'token';
                    return $this->responseAction();
                } else {
                    $token_expire_time = '10 ';
                    $token_created = $obj_security[0]->getToken_created();
                    $expire_token_date = strtotime(date("Y-m-d h:i:s", strtotime($token_expire_time . "	minutes", $token_created)));
                    $current_time = strtotime(date("Y-m-d h:i:s"));

                    if ($current_time >= $expire_token_date) {
                        $this->token = 'token';
                        return $this->responseAction();
                    }
                }
            }

            if (JSON_ERROR_NONE == json_last_error_msg()) {
                // valid JSON
                return $param;
            }
        }
        return array();
    }

    public function responseAction() {

        if (isset($this->token) && !empty($this->token)) {
            $this->error = $this->token;
            $this->data = "";
        }

        $response = array(
            "title" => $this->title,
            "error" => $this->error,
            "error_msg" => $this->error_msg,
            "data" => $this->data,
        );


        $jsonResponse = new JsonResponse();
        $jsonResponse->setData($response);

        return $jsonResponse;
    }

    public function clean_data($data, $type) {
        if (!empty($data)) {
            switch ($type) {
                case 1:
                    // array
                    if (is_array($data)) {
                        return $data;
                    }
                    break;
                case 2:
                    // array list
                    if (is_array($data) && !empty($data)) {
                        return $data;
                    }
                    break;
            }
        }
        return null;
    }

    public function rand_string($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";


        $size = strlen($chars);
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }


        return $str;
    }

    /**
     * use to validate request param
     * @param array $param
     *
     * @return boolean
     */
    public function validateData($param) {

        if (count($this->validateRule)) {

            foreach ($this->validateRule as $value) {
                switch ($value['rule']) {
                    case 'NOTNULL' :
                        foreach ($value['field'] as $field) {

                            if (!isset($param->$field) && empty($param->$field)) {
                                // For error tracking
                                $this->error = false;
                                $this->error_msg = "Missing field : " . $field;
                              
                                return $this->error;
                            }
                        }

                        break;
                }
            }
        } else {
            $this->error = "Not Initialize";
            return false;
        }
        return true;
    }

    public function getdata($table, $condition) {
        $doc = $this->getDoctrine()->getManager()
                ->getRepository("MainAdminBundle:" . $table)
                ->findBy($condition);
        return $doc;
    }

    public function getonedata($table, $condition) {
        $doc = $this->getDoctrine()->getManager()
                ->getRepository("MainAdminBundle:" . $table)
                ->findOneBy($condition);
        return $doc;
    }

    public function firequery($query) {
        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $data = $em->fetchAll();
        return $data;
    }

    public function send_notification($registration_ids, $title, $message, $provider, $app_id, $domain_id, $tablename, $tabledataid) {

        /*
          $app_id= CUST // Customer App
          $app_id = DEL // Delivery App
          $domain_id = domain_code
         */

        switch ($provider) {
            case 1:
                $result = "FALSE";
                $development = false;
                $apns_url = NULL; // Set Later
                $pathCk = NULL; // Set Later
                $apns_port = 2195;



                $app_type_master = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Apptypemaster')
                        ->findOneBy(array('is_deleted' => 0, 'app_type_code' => $app_id));

                if (!empty($app_type_master)) {
                    $app_details = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Appdetails')
                            ->findOneBy(array('is_deleted' => 0, 'app_type_id' => $app_type_master->getApp_type_id(), "domain_id" => $domain_id, "status" => 'active'));

                    if (!empty($app_details)) {
                        if ($development) {

                            if (!empty($app_details->getApp_apns_certificate_development()) && $app_details->getApp_apns_certificate_development() != "" && !empty($app_details->getApp_apns_certificate_development_password()) && $app_details->getApp_apns_certificate_development_password() != "") {

                                $apns_url = 'gateway.sandbox.push.apple.com';

                                $pathCk = $this->container->get('kernel')->locateResource('@WSBundle/Controller/' . $app_details->getApp_apns_certificate_development());
                                $passphrase = $app_details->getApp_apns_certificate_development_password();
                            }
                        } else {
                            if (!empty($app_details->getApp_apns_certificate_production()) && $app_details->getApp_apns_certificate_production() != "" && !empty($app_details->getApp_apns_certificate_production_password()) && $app_details->getApp_apns_certificate_production_password() != "") {
                                $apns_url = 'gateway.push.apple.com';

                                $pathCk = $this->container->get('kernel')->locateResource('@WSBundle/Controller/' . $app_details->getApp_apns_certificate_production());
                                $passphrase = $app_details->getApp_apns_certificate_production_password();
                            }
                        }
                    }
                }

                if (!empty($apns_url) && $apns_url != "") {
                    $ctx = stream_context_create();
                    stream_context_set_option($ctx, 'ssl', 'local_cert', $pathCk);
                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);


                    $apns = stream_socket_client(
                            'ssl://' . $apns_url . ':' . $apns_port, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                    $data = json_decode($message);

                    $payload['aps'] = array(
                        'alert' => $data->detail,
                        'badge' => 1,
                        'sound' => 'default',
                        'response' => $data->response
                    );

                    $payload['job_id'] = $data->code;

                    // START LOOP
                    if ($registration_ids != "ALL") {
                        unset($where);
                        if (is_array($registration_ids)) {
                            $registration_ids = implode("','", $registration_ids);
                        }

                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();
                        $apns_user = $connection->prepare("SELECT * FROM apns_user WHERE apns_regid in ('" . $registration_ids . "') and is_deleted=0");

                        $apns_user->execute();
                        $apns_user_list = $apns_user->fetchAll();

                        // specific user

                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE apns_user SET badge=(badge+1) WHERE apns_regid in ('" . $registration_ids . "') and is_deleted=0");
                        $statement->execute();
                    } else {
                        // All user
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $statement = $connection->prepare("UPDATE apns_user SET badge = (badge+1) WHERE  is_deleted=0");
                        $statement->execute();
                    }

                    foreach (array_slice($apns_user_list, 0) as $key => $val) {
                        $device = $val['apns_regid'];

                        $final_payload = json_encode($payload);

                        $apnsMessage = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', $device)) . pack('n', strlen($final_payload)) . $final_payload;

                        $result = fwrite($apns, $apnsMessage);
                        //var_dump($result);exit;
                        unset($apnsMessage);
                    }

                    // END LOOP

                    if (!$apns) {
                        $result = "Failed to connect : $error $errorString " . PHP_EOL;
                    }

                    fclose($apns);
                }

                break;

            case 2:
                $result = FALSE;
                $title_name = $title;
                $data = array("title" => $title_name, "message" => $message);
                $URL = 'https://android.googleapis.com/gcm/send';

                $fields = array(
                    'registration_ids' => $registration_ids,
                    'data' => $data,
                );

                $app_type_master = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Apptypemaster')
                        ->findOneBy(array('is_deleted' => 0, 'app_type_code' => $app_id));
                if (!empty($app_type_master)) {
                    $app_details = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Appdetails')
                            ->findOneBy(array('is_deleted' => 0, 'app_type_id' => $app_type_master->getApp_type_id(), "domain_id" => $domain_id, "status" => 'active'));
                    if (!empty($app_details)) {

                        if (!empty($app_details->getApp_gcm_key()) && $app_details->getApp_gcm_key() != "") {
                            $headers = array(
                                'Authorization: key=' . $app_details->getApp_gcm_key(),
                                'Content-Type: application/json'
                            );

                            $data1 = json_decode($message);
                            $response = "";
                            if (isset($data1->response) && !empty($data1->response)) {
                                $response = print_r($data1->response, 1);
                            } else {
                                $response = "";
                            }

                            $code = "";
                            if (isset($data1->code) && !empty($data1->code)) {
                                $code = $data1->code;
                            } else {
                                $code = "";
                            }

                            $registration_ids_array = $registration_ids;
                            if (is_array($registration_ids)) {
                                $registration_ids = implode("','", $registration_ids);
                            } else {
                                $registration_ids = "";
                            }

                            $detail = "";
                            if (isset($data1->detail) && !empty($data1->detail)) {
                                $detail = $data1->detail;
                            } else {
                                $detail = "";
                            }
                            $em = $this->getDoctrine()->getManager();
                            if (!empty($registration_ids_array)) {
                                foreach ($registration_ids_array as $val) {

                                    $apppushnotificationmaster = new Apppushnotificationmaster();
                                    $apppushnotificationmaster->setDevice_name('android');
                                    $apppushnotificationmaster->setApp_id($app_id);
                                    $apppushnotificationmaster->setDomain_id($domain_id);
                                    $apppushnotificationmaster->setDevice_token($val);

                                    $connection = $em->getConnection();
                                    $gcm = $connection->prepare("SELECT * FROM gcm_user WHERE gcm_regid = '" . $val . "' and is_deleted=0 ORDER BY gcm_user_id DESC");
                                    $gcm->execute();
                                    $gcm_user = $gcm->fetchAll();
                                    if (!empty($gcm_user)) {
                                        $user_id = $gcm_user[0]['user_id'];
                                        $device_id = $gcm_user[0]['device_id'];

                                        $em = $this->getDoctrine()->getManager();
                                        $user_setting = $em->getRepository('AdminBundle:Usersetting')->findOneBy(array("user_id" => $user_id, "is_deleted" => 0));

                                        $value = json_decode($user_setting->getSetting_value(), true);

                                        $lang_id = $value['language'];

                                        $apppushnotificationmaster->setUser_id($user_id);
                                        $apppushnotificationmaster->setLanguage_id($lang_id);
                                        $apppushnotificationmaster->setDevice_id($device_id);
                                    }

                                    $apppushnotificationmaster->setData($detail);
                                    $apppushnotificationmaster->setCode($code);
                                    $apppushnotificationmaster->setTable_name($tablename);
                                    $apppushnotificationmaster->setTable_id($tabledataid);
                                    $apppushnotificationmaster->setResponse($response);
                                    $apppushnotificationmaster->setDatetime(date("Y-m-d H:i:s"));
                                    $apppushnotificationmaster->setIs_deleted(0);
                                    $em = $this->getDoctrine()->getManager();
                                    $em->persist($apppushnotificationmaster);
                                    $em->flush();
                                }
                            }
                            // Open connection
                            $ch = curl_init();


                            // Set the url, number of POST vars, POST data
                            curl_setopt($ch, CURLOPT_URL, $URL);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


                            // Disabling SSL Certificate support temporarly
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));


                            // Execute post
                            $result = curl_exec($ch);

                            if ($result === FALSE) {
                                die('Curl failed: ' . curl_error($ch));
                            }
                            // Close connection
                            curl_close($ch);
                        }
                    }
                }

                break;
        }

        return $result;
    }

    /*
      get GCM user device / @param int $user_id / @return mixed
     */

    public function find_gcm_regid($user_id) {

        if (is_array($user_id)) {
            $user_id = implode("','", $user_id);
        }

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $gcm_user = $connection->prepare("SELECT * FROM gcm_user WHERE user_id in ('" . $user_id . "') and gcm_regid NOT LIKE '' and user_type = 'user' and is_deleted = 0");
        $gcm_user->execute();
        $gcm_user_list = $gcm_user->fetchAll();

        if (count($gcm_user_list) > 0) {
            $reg_ids = array_map(function($sub) {
                return $sub['gcm_regid'];
            }, $gcm_user_list);
            return $reg_ids;
        }
        return false;
    }

    /*
      get APNS user device / @param int $user_id / @return mixed
     */

    public function find_apns_regid($user_id) {

        if (is_array($user_id)) {
            $user_id = implode("','", $user_id);
        }

        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $apns_user = $connection->prepare("SELECT * FROM apns_user WHERE user_id in ('" . $user_id . "') and apns_regid NOT LIKE '(null)' and apns_regid NOT LIKE '' and user_type = 'user' and is_deleted=0");
        $apns_user->execute();
        $apns_user_list = $apns_user->fetchAll();


        if (count($apns_user_list) > 0) {
            $reg_ids = array_map(function($sub) {
                return $sub['apns_regid'];
            }, $apns_user_list);
            return $reg_ids;
        }
        return false;
    }

    // Category List

    function get_hirerachy($lang_id, $parent_hieraerchy_id, $current_category_id, $domain_id) {
        $child_data = "";
        /* $domain_id = $this->get('session')->get('domain_id'); */

        $single_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findOneBy(array('is_deleted' => 0, 'main_category_id' => $current_category_id, 'domain_id' => $domain_id, "language_id" => $lang_id));

        /* 		var_dump($single_category);	   */
        $all_sub_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => 0, 'parent_category_id' => $single_category->getMain_category_id(), 'domain_id' => $domain_id, "language_id" => $lang_id));

        if (count($all_sub_category) == 0) {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
                $image = "";
                $category_image_id = 0;
                if (!empty($single_category->getCategory_image_id()) && $single_category->getCategory_image_id() != 0) {

                    $media_library_master = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Medialibrarymaster')
                            ->findOneBy(array('is_deleted' => 0, 'media_library_master_id' => $single_category->getCategory_image_id()));


                    if (!empty($media_library_master)) {
                        $category_image_id = $single_category->getCategory_image_id();
                        $image = $this->container->getParameter('live_path') . $media_library_master->getMedia_location() . '/' . $media_library_master->getMedia_name();
                    }
                }
            }

            $data = array(
                "category_master_id" => $single_category->getCategory_master_id(),
                "category_name" => $single_category->getCategory_name(),
                "parent_category_id" => $single_category->getParent_category_id(),
                "parent_category_name" => $parent_category_name,
                "category_description" => strip_tags($single_category->getCategory_description()),
                "main_category_id" => $single_category->getMain_category_id(),
                "category_image_id" => $category_image_id,
                "category_image" => $image,
                "language_id" => $single_category->getLanguage_id(),
                "child_data" => null,
            );
        } else {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
                $image = "";
                $category_image_id = 0;
                if (!empty($single_category->getCategory_image_id()) && $single_category->getCategory_image_id() != 0) {

                    $media_library_master = $this->getDoctrine()
                            ->getManager()
                            ->getRepository('AdminBundle:Medialibrarymaster')
                            ->findOneBy(array('is_deleted' => 0, 'media_library_master_id' => $single_category->getCategory_image_id()));


                    if (!empty($media_library_master)) {
                        $category_image_id = $single_category->getCategory_image_id();
                        $image = $this->container->getParameter('live_path') . $media_library_master->getMedia_location() . '/' . $media_library_master->getMedia_name();
                    }
                }
            }
            $data_temp[] = array(
                "category_master_id" => $single_category->getCategory_master_id(),
                "category_name" => $single_category->getCategory_name(),
                "parent_category_id" => $single_category->getParent_category_id(),
                "parent_category_name" => $parent_category_name,
                "category_description" => strip_tags($single_category->getCategory_description()),
                "main_category_id" => $single_category->getMain_category_id(),
                "category_image_id" => $category_image_id,
                "category_image" => $image,
                "language_id" => $single_category->getLanguage_id(),
                "classname" => "cat " . $single_category->getMain_category_id()
            );

            $data_child = '';
            if (count($all_sub_category) > 0) {
                foreach (array_slice($all_sub_category, 0) as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    if ($lval->getParent_category_id() == 0) {

                    } else {

                        $data_child[] = $this->get_hirerachy($lang_id, $lval->getParent_category_id(), $lval->getMain_category_id(), $domain_id);
                    }
                }
            }

            $data = array(
                "category_master_id" => $data_temp[0]['category_master_id'],
                "category_name" => $data_temp[0]['category_name'],
                "parent_category_id" => $data_temp[0]['parent_category_id'],
                "parent_category_name" => $data_temp[0]['parent_category_name'],
                "category_description" => $data_temp[0]['category_description'],
                "main_category_id" => $data_temp[0]['main_category_id'],
                "category_image_id" => $data_temp[0]['category_image_id'],
                "category_image" => $data_temp[0]['category_image'],
                "language_id" => $data_temp[0]['language_id'],
                "child_data" => $data_child,
                "classname" => $data_temp[0]['classname']
            );
        }
        return $data;
    }

    // Category List

    function get_hirerachy_id($lang_id, $parent_hieraerchy_id, $current_category_id, $domain_id) {
        $child_data = "";
        static $cate_arr_sub = '';
        /* $domain_id = $this->get('session')->get('domain_id'); */

        $single_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findOneBy(array('is_deleted' => 0, 'main_category_id' => $current_category_id, 'domain_id' => $domain_id, "language_id" => $lang_id));

        /* 		var_dump($single_category);	   */
        $all_sub_category = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Categorymaster')
                ->findBy(array('is_deleted' => 0, 'parent_category_id' => $single_category->getMain_category_id(), 'domain_id' => $domain_id, "language_id" => $lang_id));

        if (count($all_sub_category) == 0) {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
                $cate_arr_sub .= "," . $single_category->getMain_category_id();
            }

            $data = array(
                "child_data" => null,
                "cate_arr_sub" => $cate_arr_sub
            );
        } else {
            if (!empty($single_category)) {
                $parent_category_name = $single_category->getCategory_name();
            }
            $cate_arr_sub .= "," . $single_category->getMain_category_id();
            $data_temp[] = array(
                "cate_arr_sub" => $cate_arr_sub
            );

            $data_child = '';
            if (count($all_sub_category) > 0) {
                foreach (array_slice($all_sub_category, 0) as $lkey => $lval) {
                    $parent_category_name = 'No Parent ';

                    if ($lval->getParent_category_id() == 0) {

                    } else {

                        $data_child[] = $this->get_hirerachy_id($lang_id, $lval->getParent_category_id(), $lval->getMain_category_id(), $domain_id);
                    }
                }
            }

            $data = array(
                "child_data" => $data_child,
                "cate_arr_sub" => $data_temp[0]['cate_arr_sub']
            );
        }
        return $cate_arr_sub;
    }

    public function getaddressAction($address_id, $lang_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT address_master.*,city_master.city_name,area_master.area_name FROM address_master JOIN city_master ON address_master.city_id = city_master.main_city_id JOIN area_master ON address_master.area_id = area_master.main_area_id WHERE address_master.main_address_id = " . $address_id . " AND address_master.language_id = " . $lang_id . " AND city_master.language_id = " . $lang_id . " AND area_master.language_id = " . $lang_id . " AND address_master.is_deleted = 0");
        $statement->execute();
        $address_info = $statement->fetchAll();

        if (!empty($address_info) && $address_id != 0) {
            $lat = $lng = NULL;

            if (!empty($address_info[0]['lat'])) {
                $lat = $address_info[0]['lat'];
            }

            if (!empty($address_info[0]['lng'])) {
                $lng = $address_info[0]['lng'];
            }

            $address = array(
                "address" => $address_info[0]['address_name'],
                "base_address_type" => $address_info[0]['base_address_type'],
                "address_type" => $address_info[0]['address_type'],
                "city_name" => $address_info[0]['city_name'],
                "city_id" => $address_info[0]['city_id'],
                "area_name" => $address_info[0]['area_name'],
                "street" => $address_info[0]['street'],
                "flate_house_number" => $address_info[0]['flate_house_number'],
                "society_building_name" => $address_info[0]['society_building_name'],
                "landmark" => $address_info[0]['landmark'],
                "pincode" => $address_info[0]['pincode'],
                "gmap_link" => $address_info[0]['gmap_link'],
                "lat" => $lat,
                "lng" => $lng,
            );
        } else {
            /* $address = array(
              "address"=>NULL,
              "base_address_type"=>NULL,
              "address_type"=>NULL,
              "city_name"=>NULL,
              "area_name"=>NULL,
              "street"=>NULL,
              "flate_house_number"=>NULL,
              "society_building_name"=>NULL,
              "landmark"=>NULL,
              "pincode"=>NULL,
              "gmap_link"=>NULL,
              "lat"=>NULL,
              "lng"=>NULL,
              ); */
            $address = array();
        }
        return $address;
    }

    function array_push_assoc($array, $key, $value) {
        $array[$key] = $value;
        return $array;
    }

    public function getimage($media_library_master_id) {
        $live_path = $this->container->getParameter('live_path');

        $media_info = $this->getDoctrine()
                ->getManager()
                ->getRepository("AdminBundle:Medialibrarymaster")
                ->findOneBy(array("media_library_master_id" => $media_library_master_id, "is_deleted" => 0));

        if (!empty($media_info)) {
            $image_url = $live_path . $media_info->getMedia_location() . "/" . $media_info->getMedia_name();
        } else {
            $image_url = "";
        }
        return $image_url;
    }

    public function getimageData($media_library_master_id) {
        $live_path = $this->container->getParameter('live_path');
        $media_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Medialibrarymaster")->findOneBy(array("media_library_master_id" => $media_library_master_id, "is_deleted" => 0));
        if (!empty($media_info)) {
            $image_url = $live_path . $media_info->getMedia_location() . "/" . $media_info->getMedia_name();
        } else {
            $image_url = "";
        } return array($image_url, $media_info->getMedia_type_id());
    }

    function mediauploadAction($file, $tmpname, $path, $upload_dir, $mediatype_id) {


        $clean_image = preg_replace('/\s+/', '', $file);
        $logo_name = date('Y_m_d_H_i_s') . '_' . $clean_image;
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777);
        }
        //logo upload check
        if (move_uploaded_file($tmpname, $upload_dir . $logo_name)) {
            $medialibrarymaster = new Medialibrarymaster();
            $medialibrarymaster->setMedia_type_id($mediatype_id);
            $medialibrarymaster->setMedia_title($logo_name);
            $medialibrarymaster->setMedia_location($path);
            $medialibrarymaster->setMedia_name($logo_name);
            $medialibrarymaster->setCreated_on(date('Y-m-d H:i:s'));
            $medialibrarymaster->setIs_deleted(0);


            $em = $this->getDoctrine()->getManager();
            $em->persist($medialibrarymaster);
            $em->flush();
            $media_library_master_id = $medialibrarymaster->getMedia_library_master_id();
            return $media_library_master_id;
        } else {
            return FALSE;
        }
    }

    public function keyEncryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }
            return base64_encode($res);
        }
        return "";
    }

    public function keyDecryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');

            $res = '';
            $string = base64_decode($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
            return $res;
        }
        return "";
    }

    public function generate_token($device_id) {
        $token = $device_id . 'bwizard' . time();
        $new_token = md5($token);
        return $new_token;
    }

    public function getgovernoratename($governorate_id, $language_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM governorate_master WHERE main_governorate_id=$governorate_id AND is_deleted=0 AND status='active' AND language_id='$language_id'");

        $statement->execute();
        $country_master_info = $statement->fetchAll();
        if (!empty($country_master_info)) {
            return $country_master_info[0]['governorate_name'];
        }
        return false;
    }

    public function getshopcoverimage($shop_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM restaurant_gallery WHERE restaurant_id=$shop_id AND is_deleted=0");

        $statement->execute();
        $country_master_info = $statement->fetchAll();
        $gallery_arr = array();
        if (!empty($country_master_info)) {
            foreach ($country_master_info as $key => $val) {
                $gallery_arr[] = $this->getimage($val['image_id']);
            }
            return $gallery_arr;
        }
        return false;
    }
	
    public function getbranchcoverimage($shop_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM branch_gallery WHERE branch_id=$shop_id AND is_deleted=0");

        $statement->execute();
        $country_master_info = $statement->fetchAll();
        $gallery_arr = array();
        if (!empty($country_master_info)) {
            foreach ($country_master_info as $key => $val) {
                $gallery_arr[] = $this->getimage($val['image_id']);
            }
            return $gallery_arr;
        }
        return false;
    }	

    public function getdiningexperiencephoto($shop_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM dining_experience_gallery WHERE dining_experience_id=$shop_id AND is_deleted=0");

        $statement->execute();
        $country_master_info = $statement->fetchAll();
        $gallery_arr = array();
        if (!empty($country_master_info)) {
            foreach ($country_master_info as $key => $val) {
                $gallery_arr[] = $this->getimage($val['image_id']);
            }
            return $gallery_arr;
        }
        return null;
    }

    public function getcompetition_award_photo($main_competition_award_id) {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM competition_award_gallery WHERE main_competition_award_id=$main_competition_award_id AND is_deleted=0");

        $statement->execute();
        $country_master_info = $statement->fetchAll();
        $gallery_arr = array();
        if (!empty($country_master_info)) {
            foreach ($country_master_info as $key => $val) {
				if($this->getimage($val['image_id']) != ''){
					$gallery_arr[] = $this->getimage($val['image_id']);
				}
            }
            return $gallery_arr;
        }
        return false;
    }
    
     
    public function userdetails($user_id){
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM user_master WHERE user_master_id=$user_id AND is_deleted=0");

        $statement->execute();
        $country_master_info = $statement->fetchAll();
        $response = null;
        if (!empty($country_master_info)) {
            foreach ($country_master_info as $key => $val) {
                
                $response = array('user_master_id'=>$val['user_master_id'],"user_image" => !empty($val['user_image'])?$this->getimage($val['user_image']):$val['image_url'],
                        "nick_name" => $val['user_bio'],
                    "user_firstname" => $val['user_firstname'],
                    "user_lastname" => $val['user_lastname']);
				
            }
           
        }
         return $response;
    }

    public function addCompetitionPointsToUser($user_id,$activity_type,$restaurant_id,$related_id,$related_table,$category_id = 0){

        $evaluation_type_activities = array(
            'evaluation_with_image',
            'evaluation_with_comments',
            'evaluation_with_image_comment',
            'evaluation_without_image_comment',
            'evaluation_new_restaurant'
        );

        #check any competition exist
        $today = date('Y-m-d H:i:s');
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();

        $sql_comp_check = "select main_competition_id from competition_master
                            where start_date <= '$today' AND end_date >= '$today'
                            and is_deleted = '0' and status = 'active'";
      
        $comp_check_stmt = $con->prepare($sql_comp_check);
        $comp_check_stmt->execute();
        $comp_exist = $comp_check_stmt->fetchAll();
       
        if(!empty($comp_exist)){
            foreach($comp_exist as $_comp_exist){

                $comp_id = $_comp_exist['main_competition_id'];

                // check if exclude or include
                $chkCompetition = $em->getRepository("AdminBundle:Competitionmaster")->findOneBy([
                    'main_competition_id' => $comp_id,
                    'language_id' => 1
                ]);

                #checkExclutionMaster 
                $exclude_rest = null;
                $extra_points = 0 ;

                if(!empty($chkCompetition)){

                    // check if category excluded
                    $_resSql = "SELECT rel.* from restaurant_category_relation rel, category_master c, restaurant_master r where rel.main_category_id = c.main_category_id and rel.restaurant_id = r.main_restaurant_id and r.main_restaurant_id = {$restaurant_id} and rel.is_deleted = 0 and c.is_deleted = 0 and r.is_deleted = 0 GROUP by restaurant_category_relation_id";
                    $_resSqlRecord = $this->firequery($_resSql);

                    $catList = null;
                   
                    if(!empty($_resSqlRecord)){
                        foreach($_resSqlRecord as $_rel){
                            $catList[] = $_rel['main_category_id'];
                        }

                        if(!empty($catList)){
                            $catList = implode(',', $catList);

                            $_sql = "SELECT * from competition_category_relation where competition_id = {$comp_id} and category_id in ($catList) and is_deleted = 0 and type = 'exclude' ";
                         //   echo $_sql ;
                            
                            $_sqlRec = $this->firequery($_sql);
                         
                            if(empty($_sqlRec)){
                                //continue;
                            }
                        }
                    }


/*
                    $_sql = "SELECT * from competition_category_relation where competition_id = {$comp_id} and category_id = '$category_id' and is_deleted = 0 and type = 'exclude' ";
                    $_sqlRec = $this->firequery($_sql);
*/
                                 
                    if(!empty($_sqlRec)){
                       // continue;
                    }

                    $exclude_rest = $em->getRepository("AdminBundle:Competitionexclusionmaster")->findOneBy(["restaurant_id"=>$restaurant_id,"competition_id"=>$comp_id,"is_deleted"=>0]);
            
                    if($exclude_rest){
                        return false;
                    }

                    if(!empty($category_id)){

                        $extra_points_category = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findOneBy(["category_id"=>$category_id,"competition_id"=>$comp_id,"is_deleted"=>0,'type'=>'include']);

                        if($extra_points_category){
                            $extra_points += $extra_points_category->getExtra_points();
                        }
                    }

                    if(!empty($restaurant_id)){
                        $extra_points_rest = $em->getRepository("AdminBundle:Competitionextrapoints")->findOneBy(["rest_id"=>$restaurant_id,"comp_id"=>$comp_id,"is_deleted"=>0]);

                        if($chkCompetition->getExtra_point_type() == 'include'){
                            $extra_points += $chkCompetition->getExtra_point_value();
                            if($extra_points_rest){
                                $extra_points += $extra_points_rest->getExtra_points();
                            }
                        } else if($chkCompetition->getExtra_point_type() == 'exclude'){
                            /* check if restaurant not in exclude list
                            * if extra_points_rest empty -> restaurant not excluded
                            */
                            if(empty($extra_points_rest)){
                                $extra_points += $chkCompetition->getExtra_point_value();
                            }
                        }
                    }
                }

                #get Activity Details and Points
                $sql_activity_details = "select * from activity_master 
                                         JOIN competition_activity_relation rel ON rel.activity_id = activity_master.main_activity_id  
                                         where activity_name = '$activity_type' and activity_master.is_deleted = '0'and  
                                        rel.is_deleted = '0' and rel.competition_id = '$comp_id'";
               // echo $sql_activity_details;exit;
                $act_check_stmt = $con->prepare($sql_activity_details);
                $act_check_stmt->execute();
                $act_exist = $act_check_stmt->fetchAll();
                
                if($act_exist){
                    $act_exist = $act_exist[0];

                    $comp_points = $act_exist['points'];
                    $activity_id = $act_exist['main_activity_id'];

                    $new_points = new Competitionuserrelation();
                    $new_points->setCompetition_id($comp_id);
                    $new_points->setActivity_id($activity_id);
                    $new_points->setRestaurant_id($restaurant_id);
                    $new_points->setRelated_id($related_id);
                    $new_points->setRelated_table_name($related_table);
                    $new_points->setUser_id($user_id);
                    $new_points->setPoints($comp_points + $extra_points );
                    $new_points->setCreated_datetime(date('Y-m-d H:i:s'));

                    if(in_array($activity_type,$evaluation_type_activities)){

                        #need to count after approved evaluation from admin
                        $new_points->setIs_deleted(1);

                    }else{
                        $new_points->setIs_deleted(0);

                    }

//                    $new_points->setIs_deleted(0);

                    $em->persist($new_points);
                    $em->flush();
                    
                }
            }

            return true;
        }
        
        return false;
    }


}
