<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Usermaster;

class WSSuggestionTypeController extends WSBaseController {

    /**
     * @Route("/ws/suggestion_type/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     *
     */
    public function suggestion_typeAction($param) {
        try {
            $this->title = "Suggestion TYpe";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {
                $data[] = array(
                    "type_id"=>"1",
                    "type_name"=>"suggestion",
                    "display_name"=>"Suggestion"
                );
                $data[] = array(
                    "type_id"=>"2",
                    "type_name"=>"complaints",
                    "display_name"=>"Complaints"
                );
                $data[] = array(
                    "type_id"=>"3",
                    "type_name"=>"others",
                    "display_name"=>"Others"
                );
                if (!empty($data)) {
                    $response = $data;
                    $this->error = "SFD";
                }
                if (empty($response)) {
                    $response = false;
                    $this->error = "NRF";
                }

                $this->data = $response;
                return $this->responseAction();
            }
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>