<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\Gcmuser;
use Symfony\Component\HttpFoundation\JsonResponse;

class WSUserLoginController extends WSBaseController {

    /**
     * @Route("/ws/userlogin/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userLoginAction($param) {
        //try{			
        $this->title = "User Login Post";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('email', 'password',  'device_name', 'device_token', 'device_id' ,'language_id'),
            ),
        );
        $em = $this->getDoctrine()->getManager();
        $response = array();
        $address_arr = [];
        $area = [];
        $img = $userAddress = "";
        /* if(!empty($username) && !empty($password)){ */
        //if(!empty($email) && !empty($password) && !empty($devicename) && !empty($regid) && !empty($domain_id))
        if ($this->validateData($param)) {
            $language_id = $param->language_id ;
            $email = $param->email;
            $role_id = 3 ;
            $regid = $device_token = $param->device_token;
            $device_id = $param->device_id;
            $domain_id = 1 ;
            $password = md5($param->password);
            $devicename = strtolower($param->device_name);
            $em = $this->getDoctrine()->getManager();
            
			## check email
			$check_email = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(
				array(
					"email" => $email, "is_deleted" => 0,
				)
			);
			
			if(empty($check_email)){
				
				$this->error = "NRF";
				$this->data = "Email address not exists";
				return $this->responseAction();
			} else {
				## check password if email exists
				$check_email = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(
					array(
						"email" => $email, "password" => $password, "is_deleted" => 0,
					)
				);
				
				if(empty($check_email)){
					$this->error = "PIW";
					$this->data = "Password is wrong";
					return $this->responseAction();
				}
			}
			
			$userMaster = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(
                    array(
                        "email" => $email,"password" => $password,"user_role_id" => $role_id, "status" => 'active', "is_deleted" => 0,
                    ));
            if (!empty($userMaster)) {
                $latest_address_info= $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted"=>0,"owner_id"=>$userMaster->getUser_master_id(),"base_address_type"=>'primary')); 
                $address_list = NULL ;        
                if(!empty($latest_address_info)  && $latest_address_info != NULL){
                    $address_list = $this->getaddressAction($latest_address_info->getAddress_master_id(),$language_id);
                }
                if (!empty($address_list)) {
                    $userAddress = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy( array("address_master_id" => $userMaster->getAddress_master_id(),"is_deleted" => 0) );
                }               
                //------------------------------------------------------------------------------                    
                $Usermaster = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(array("user_master_id" => $userMaster->getUser_master_id(), "is_deleted" => '0'));
                // if (!empty($Usermaster) && !empty($devicename) && ($devicename == 'ios' || $devicename == 'iphone')  && (strlen($device_token) == 64)) {
                if (!empty($Usermaster) && !empty($devicename) && ($devicename == 'ios' || $devicename == 'iphone')) {
                    $Apnsuser = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Apnsuser")->findOneBy(
                            array(
                                "apns_regid" => $regid,
                                "device_id" => $device_id,
                                "user_id" => $userMaster->getUser_master_id(),
                                "is_deleted" => '0'
                            )  );


                    if (!empty($Apnsuser)) {
                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE device_id = '" . $device_id . "' and is_deleted=0");
                        $apns_user_delete->execute();


                        // delete all other with this device id	=0 				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE ( device_id = '0' OR device_id = '')");
                        $apns_user_delete->execute();

                       

                        $apns_user_info = new Apnsuser();
                        $apns_user_info->setApns_regid($regid);
                        $apns_user_info->setUser_id($userMaster->getUser_master_id());
                        $apns_user_info->setUser_type($userMaster->getUser_type());
                        $apns_user_info->setDevice_id($device_id);
                        $apns_user_info->setApp_id('CUST');
                        $apns_user_info->setDomain_id($domain_id);
                        $apns_user_info->setName($Usermaster->getUser_firstname());
                        $apns_user_info->setBadge("0");
                        $apns_user_info->setCreated_date(date('Y-m-d H:i:s'));
                        $apns_user_info->setIs_deleted(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($apns_user_info);
                        $em->flush();



                        // delete all other with this device id						
                      /*  $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE device_id = '" . $device_id . "' and is_deleted=0 and user_id != '" . $userMaster->getUser_master_id() . "'");
                        $apns_user_delete->execute();*/
                        // delete all other with this device id	=0 				
                      
                        $data = true;
                    } 
                    else {
                        // delete all other with this device id
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE device_id = '" . $device_id . "' and is_deleted=0");
                        $apns_user_delete->execute();
                        // delete all other with this device id	=0 				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $apns_user_delete = $connection->prepare("DELETE FROM apns_user WHERE ( device_id = '0' OR device_id = '')");
                        $apns_user_delete->execute();

                        $apns_user_info = new Apnsuser();
                        $apns_user_info->setApns_regid($regid);
                        $apns_user_info->setUser_id($userMaster->getUser_master_id());
                        $apns_user_info->setUser_type($userMaster->getUser_type());
                        $apns_user_info->setDevice_id($device_id);
                        $apns_user_info->setApp_id('CUST');
                        $apns_user_info->setDomain_id($domain_id);
                        $apns_user_info->setName($Usermaster->getUser_firstname());
                        //$apns_user_info->setEmail($Usermaster->getUser_emailid());
                        $apns_user_info->setBadge("0");
                        $apns_user_info->setCreated_date(date('Y-m-d H:i:s'));
                        $apns_user_info->setIs_deleted(0);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($apns_user_info);
                        $em->flush();
                        $data = true;
                    }
                } 
                elseif (!empty($Usermaster) && !empty($devicename) && $devicename == 'android') {
                    $Gcmuser = $this->getDoctrine() ->getManager() ->getRepository("AdminBundle:Gcmuser")->findOneBy(array("gcm_regid" => $regid,"device_id" => $device_id, "user_id" => $userMaster->getUser_master_id(), "is_deleted" => '0'));
                    if (!empty($Gcmuser)) {                        
                        $Gcmuser->setGcm_regid($regid);
                        $Gcmuser->setUser_id($userMaster->getUser_master_id());
                        $Gcmuser->setUser_type($userMaster->getUser_type());
                        $Gcmuser->setDevice_id($device_id);
                        $Gcmuser->setApp_id('CUST');
                        $Gcmuser->setDomain_id($domain_id);
                        $Gcmuser->setName($Usermaster->getUser_firstname());
                        $Gcmuser->setCreated_date(date('Y-m-d H:i:s'));
                        $Gcmuser->setIs_deleted(0);
                        $Gcmuser->setUser_type('user');                        
                        $em->flush();
                        $data = true;

                        // ---------------delete all other with this device id	---------------					
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE device_id = '" . $device_id . " 'and is_deleted=0 and user_id != '" . $userMaster->getUser_master_id() . "'");
                        $gcm_user_delete->execute();
                        //----------------- delete all other with this device id = 0 ------------------				
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE (device_id = '' OR device_id ='(null)' OR device_id = '0')");
                        $gcm_user_delete->execute();
                    }
                    else {
                        $gcm_user_info = new Gcmuser();
                        $gcm_user_info->setGcm_regid($regid);
                        $gcm_user_info->setUser_id($userMaster->getUser_master_id());
                        $gcm_user_info->setUser_type($userMaster->getUser_type());
                        $gcm_user_info->setDevice_id($device_id);
                        $gcm_user_info->setApp_id('CUST');
                        $gcm_user_info->setDomain_id($domain_id);
                        $gcm_user_info->setName($Usermaster->getUser_firstname());
                        //$gcm_user_info->setEmail($Usermaster->getUser_emailid());
                        $gcm_user_info->setCreated_date(date('Y-m-d H:i:s'));
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($gcm_user_info);
                        $em->flush();
                        $data = true;

                        // delete all other with this device id						
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE device_id = '" . $device_id . " 'and is_deleted=0 and user_id != '" . $userMaster->getUser_master_id() . "'");
                        $gcm_user_delete->execute();
                        // delete all other with this device id	=0 						
                        $em = $this->getDoctrine()->getManager();
                        $connection = $em->getConnection();
                        $gcm_user_delete = $connection->prepare("DELETE FROM gcm_user WHERE (device_id = '' OR device_id ='(null)' OR device_id = '0')");
                        $gcm_user_delete->execute();
                      
                    }
                }

                
                $response = array(
                    "usermaster_id" => $userMaster->getUser_master_id(),
                    "nick_name" => $userMaster->getUser_bio(),
                    "user_firstname" => $userMaster->getUser_firstname(),
                    "user_lastname" => $userMaster->getUser_lastname(),
                    "user_mobile" => $userMaster->getUser_mobile(),
                    "user_image" => !empty($userMaster->getUser_image())?$this->getimage($userMaster->getUser_image()):$userMaster->getImage_url(),
                    "role_id" => $userMaster->getUser_role_id(),
                    "user_emailid" => $userMaster->getEmail(),
                    "date_of_birth" => strtotime($userMaster->getDate_of_birth())*1000,
                   // "user_phoneno" => $this->keyDecryptionAction($userMaster->getUser_mobile()),
                    "username" => $userMaster->getUsername(),
                    "language" => $userMaster->getCurrent_lang_id(),
                    "domain_id" => $userMaster->getDomain_id(),
                    "address" => $address_list
                        );
                
              
                $this->error = "SFD";
            }
            else {
                $this->error = "UII";
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = false;
        }
        $this->data = $response;
        return $this->responseAction();
        /* 	}catch(\Exception $e){
          $this->error = "SFND" ;
          $this->data = false ;
          return $this->responseAction() ;
          } */
    }

  
}

?>