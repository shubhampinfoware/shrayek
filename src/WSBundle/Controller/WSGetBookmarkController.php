<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Bookmarkmaster;
use AdminBundle\Entity\Diningexperiencegallery;
use AdminBundle\Entity\Attributecategoryrelation;

class WSGetBookmarkController extends WSBaseController {

    /**
     * @Route("/ws/getbookmark/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function getbookmarkAction($param) {
        /* try{ */
        $this->title = "Get Book Mark";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('user_id', 'language_id'),
            ),
        );
        if ($this->validateData($param)) {
            $user_id = $param->user_id;
            $lang_id = $param->language_id;

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

              $st = $conn->prepare("SELECT rm.*,ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id,
							cm.main_category_id,cm.category_name,cm.category_image_id
							from restaurant_master as rm
							left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
							left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
							left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
                                                        join bookmark_master on bookmark_master.shop_id=rm.main_restaurant_id
							where rm.is_deleted=0 and rm.language_id=$lang_id and bookmark_master.user_id=$user_id and bookmark_master.type ='add' group by rm.main_restaurant_id ");




            $st->execute();
            $shopList = $st->fetchAll();
           

            if (!empty($shopList)) {
                foreach ($shopList as $shopMaster) {
                    // get catgory
                    $query = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $lang_id . "' group by cm.main_category_id";
                    $cat = $this->firequery($query);
                    $category = null;
                    if (!empty($cat)) {
                        foreach ($cat as $ca) {


                            // get cuisines
                            $query = "select * from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $shopMaster['main_restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$lang_id and rfr.main_caetgory_id=" . $ca['main_category_id'];

                            $cuisines = null;


                            $cuisine = $this->firequery($query);


                            if (!empty($cuisine)) {

                                foreach ($cuisine as $val) {


                                    $cuisines[] = array(
                                        "food_type_id" => $val['main_food_type_id'],
                                        "food_type_name" => $val['food_type_name'],
                                        "food_type_image" => $this->getimage($val['food_type_image_id'])
                                    );
                                }
                                $category[] = array("category_id" => $ca['main_category_id'],
                                    "category_name" => $ca['category_name'],
                                    "category_logo" => $this->getimage($ca['category_image_id']), "food_type" => $cuisines);
                            }
                        }
                    }

                    // print_r($category);exit;
                    //if (!empty($category)) {
                        $query1 = "SELECT * from address_master where main_address_id='" . $shopMaster['address_id'] . "' and language_id=" . $lang_id . " and is_deleted=0";
                        $addresss = $this->firequery($query1);

                        $gallery = $this->getshopcoverimage($shopMaster['main_restaurant_id']);

                        $response[] = array(
                            "shop_id" => $shopMaster['main_restaurant_id'],
                            "shop_name" => $shopMaster['restaurant_name'],
                            "shop_short_description" => $shopMaster['description'],
                            "shop_long_description" => $shopMaster['description'],
                            "shop_logo" => $this->getimage($shopMaster['logo_id']),
                            "shop_gallery" => !empty($gallery)?$gallery:null,
                            "total_rating_count" => 0,
                            "rating" => 0,
                            "cuisines" => $category,
                            "phone_number" => 0,
                            "address" => !empty($addresss) ? $addresss[0]['main_address_id'] : 0,
                            "lat" => !empty($addresss) ? $addresss[0]['lat'] : 0,
                            "lng" => !empty($addresss) ? $addresss[0]['lng'] : 0,
                            "timing" => 0
                        );
                        $this->error = "SFD";
//                    } else {
//                        $this->error = "NRF";
//                    }
                }
                    }else {
                        $this->error = "NRF";
                    }
            } else {
                $this->error = "PIM";
            }
           
            if (empty($response)) {
                $response = False;
            }
            $this->data = $response ;
            return $this->responseAction();
            /* }
              catch (\Exception $e)
              {
              $this->error = "SFND";
              $this->data = false;
              return $this->responseAction();
              } */
        }
    }

?>