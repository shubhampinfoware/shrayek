<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Contactmaster;
use AdminBundle\Entity\Medicalreportimage;
use AdminBundle\Entity\Contactusfeedback;
class WSGetTopNearbyShop_bkup_16aug2019Controller extends WSBaseController {
    /**
     * @Route("/ws/getTopNearbyShopbkup_16aug2019/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function getTopNearbyShopbkup_16aug2019Action($param) {
        try {
            $this->title = "Top 5 Nearby Restaurant";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array()
            ));
            if ($this->validateData($param)) {
                $login_user_id = !empty($param->login_user_id) ? $param->login_user_id : 0;
                $language_id = !empty($param->language_id) ? $param->language_id : 1;
                $lat = !empty($param->lat) ? $param->lat : 0;
                $lng = !empty($param->lng) ? $param->lng : 0;
                $live_path = $this->container->getParameter('live_path');
                $display_eve_category_array = null;
                #flag for top 5 or not..
                $is_top_five = !empty($param->is_top_five) ? strtolower($param->is_top_five) : 'no';
                $main_category_id = !empty($param->main_category_id) ? strtolower($param->main_category_id) : 0;
                if ($is_top_five == 'yes') {
                    $orderByClause = 'order by total_evaluation DESC,distance ASC';
                } else {
                    $orderByClause = 'order by distance ASC';
                }
                $con = $this->getDoctrine()->getManager()->getConnection();
                $category_listing_query = '';
                if ($main_category_id != 0) {
                    $category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 and language_id = $language_id  and main_category_id =  $main_category_id group by main_category_id";
                } else {
                    $category_listing_query = "SELECT * FROM `category_master` WHERE `category_status` = 'active' AND `is_deleted` = 0 and language_id = $language_id group by main_category_id";
                }
                $limit_rangeKms = 2;
                $evaluation_res_array = null;
                $category_listing = $this->firequery($category_listing_query);
                if (!empty($category_listing)) {
                    foreach ($category_listing as $catekey => $catval) {
                        $saved_category_restaurant_query = "SELECT restaurant_id FROM `saved_categorywise_restaurant` WHERE is_deleted = 0 and `category_id` = " . $catval['main_category_id'] . " AND `update_datetime` between '" . date("Y-m-d 00:00:00") . "' and '" . date("Y-m-d 23:59:59") . "' order by update_datetime desc limit 0,5";
                        $saved_category_restaurant_list = $this->firequery($saved_category_restaurant_query);
                        $topFiveRestaurantIds = array();
                        if (!empty($saved_category_restaurant_list)) {
                            foreach ($saved_category_restaurant_list as $_saved_category_restaurant_list) {
                                $topFiveRestaurantIds [] = $_saved_category_restaurant_list['restaurant_id'];
                            }
                        } else {
                            ###old Wway to Get Top 5
                            $restaurant_detail_query_top_5 = "SELECT 
							count(evaluation_feedback.evaluation_feedback_id) as evaluation_cnt ,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation ,  evaluation_feedback.restaurant_id  FROM `evaluation_feedback`
							JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id 
							WHERE evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_foodtype_relation` WHERE `main_caetgory_id` = " . $catval['main_category_id'] . " AND `is_deleted` = 0 
							)  and restaurant_master.is_deleted = 0 group by restaurant_id order by total_evaluation DESC limit 0,5";
                            $restaurant_detail_list_top_five = $this->firequery($restaurant_detail_query_top_5);
                            if (!empty($restaurant_detail_list_top_five)) {
                                foreach ($restaurant_detail_list_top_five as $reskey1 => $resval1) {
                                    $topFiveRestaurantIds [] = $resval1['restaurant_id'];
                                }
                            }
                            ###old Wway to Get Top 5 done
                        }
                        $restaurant_detail_query = "SELECT count(evaluation_feedback.evaluation_feedback_id) as 
				evaluation_cnt								,sum((evaluation_feedback.service_level+evaluation_feedback.dining_level+evaluation_feedback.atmosphere_level+evaluation_feedback.clean_level+evaluation_feedback.price_level + 
				evaluation_feedback.delivery_speed_level +evaluation_feedback.packaging_level )) as total_evaluation ,  
				
				( 3959 * acos( cos( radians(" . $lat . ") ) * cos( radians( address_master.lat ) ) * cos( radians( address_master.lng ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians( address_master.lat ) ) ) ) AS distance,address_master.lng,address_master.lat,
				
				evaluation_feedback.restaurant_id,restaurant_master.logo_id, restaurant_master.phone_number, branch_master.mobile_no,
				branch_master.main_branch_master_id,branch_master.branch_address_id ,restaurant_master.address_id 
				
				FROM `evaluation_feedback` 
				JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id 
				
				JOIN branch_master ON branch_master.main_branch_master_id = evaluation_feedback.main_branch_id 
				
				JOIN address_master ON address_master.address_master_id = branch_master.branch_address_id  
				WHERE 
				evaluation_feedback.`status` = 'approved' AND evaluation_feedback.`is_deleted` = 0 and 
				evaluation_feedback.restaurant_id IN (SELECT restaurant_id FROM `restaurant_foodtype_relation` WHERE 
				`main_caetgory_id` = " . $catval['main_category_id'] . " AND `restaurant_id` IN (SELECT restaurant_id 
				FROM `evaluation_feedback` WHERE `status` = 'approved' AND `is_deleted` = 0) AND `is_deleted` = 0 
				)  AND restaurant_master.is_deleted = 0  
				AND address_master.lng != 0 and address_master.lat != 0 
				group by restaurant_id having distance <= $limit_rangeKms   $orderByClause limit 
				0,5";
                        $restaurant_detail_list = $this->firequery($restaurant_detail_query);
                        if (!empty($restaurant_detail_list)) {
                            foreach ($restaurant_detail_list as $reskey => $resval) {
                                $in_top_five = false;
                                if (in_array($resval['restaurant_id'], $topFiveRestaurantIds)) {
                                    $in_top_five = true;
                                }
                                $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                        ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id'], "language_id" => $language_id));
                                $rest_name = '';
                                if (!empty($restaurant_info)) {
                                    $rest_name = $restaurant_info->getRestaurant_name();
                                } else {
                                    $restaurant_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Restaurantmaster")
                                            ->findOneBy(array("is_deleted" => 0, "main_restaurant_id" => $resval['restaurant_id']));
                                    $rest_name = $restaurant_info->getRestaurant_name();
                                }
                                //----------------- get Points and rates-------------------
                                $total_rate_sql = "SELECT count(evaluation_feedback_id) as total_rate  FROM evaluation_feedback WHERE restaurant_id = '" . $resval['restaurant_id'] . "' and status='approved' and is_deleted = 0 ";
                                $total_rate_data = $this->firequery($total_rate_sql);
                                $total_rate = 0;
                                if ($total_rate_data) {
                                    $total_rate = $total_rate_data[0]['total_rate'];
                                }
                                //-------------------Category--------------------------
                                $query = "select cm.main_category_id,cm.category_name,cm.category_image_id from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $language_id . "' group by cm.main_category_id order by sort_order DESC";
                                $cat = $this->firequery($query);
                                $category = null;
                                if (!empty($cat)) {
                                    foreach ($cat as $ca) {
                                        // get cuisines
                                        $query = "select ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $resval['restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$language_id and rfr.main_caetgory_id=" . $ca['main_category_id'];
                                        $cuisines = null;
                                        $cuisine = $this->firequery($query);
                                        if (!empty($cuisine)) {
                                            foreach ($cuisine as $val) {
                                                $cuisines[] = array(
                                                    "food_type_id" => $val['main_food_type_id'],
                                                    "food_type_name" => $val['food_type_name'],
                                                    "food_type_image" => $this->getimage($val['food_type_image_id'])
                                                );
                                            }
                                            $category[] = array("category_id" => $ca['main_category_id'],
                                                "category_name" => $ca['category_name'],
                                                // "category_logo" => $this->getimage($ca['category_image_id']), 
                                                "food_type" => $cuisines);
                                        }
                                    }
                                }
                                $evaluation_res_array[] = array(
                                    "category_id" => $catval['main_category_id'],
                                    "category_name" => $catval['category_name'],
                                    "restaurant_id" => $resval['restaurant_id'],
                                    "topFiveRestaurantIds" => $topFiveRestaurantIds,
                                    "in_top_five" => $in_top_five,
                                    "main_branch_master_id" => $resval['main_branch_master_id'],
                                    "branch_address_id" => $resval['branch_address_id'],
                                    //	"branch_data" => $branch_data,
                                    "shop_logo" => $this->getimage($resval['logo_id']),
                                    "phone_number" => $resval['mobile_no'],
                                    "category" => $category,
                                    "cuisines" => $category,
                                    "distance" => $resval['distance'],
                                    "lng" => $resval['lng'],
                                    "lat" => $resval['lat'],
                                    "restaurant_name" => $rest_name,
                                    "evaluation_cnt" => $resval['evaluation_cnt'],
                                    "total_evaluation" => $resval['total_evaluation'],
                                    "points" => $total_rate,
                                );
                            }
                        }
                    }
                    //    $response = $display_eve_category_array;
                    $price = array();
                    if (!empty($evaluation_res_array)) {
                        foreach ($evaluation_res_array as $key => $row) {
                            $price[$key] = $row['distance'];
                        }
                        array_multisort($price, SORT_ASC, $evaluation_res_array);
                    }
                    $response = $evaluation_res_array;
                    $this->error = "SFD";
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $this->error = "NRF";
                $response = false;
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND " . $e->getMessage();
            $this->data = false;
            return $this->responseAction();
        }
    }
}
?>