<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Reviewmaster;
use AdminBundle\Entity\Reviewgallery;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Attributecategoryrelation;

class WSAddReviewController extends WSBaseController {
	
	/**
	 * @Route("/ws/add_review/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	public function add_reviewAction($param)
	{
	   /*try{*/
			$this->title = "Add Review";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('shop_id','user_id','review'),
				),
			);
			if($this->validateData($param)){
				$review = $param->review;
				$shop_id = $param->shop_id;
				$user_id = $param->user_id;
                    					                                
				if (!empty($_FILES['photo'])) {
				
					$Reviewmaster_new = new Reviewmaster();
					$Reviewmaster_new->setRestaurant_id($shop_id);
					$Reviewmaster_new->setReview_text($review);
					$Reviewmaster_new->setCreated_by($user_id);
					$Reviewmaster_new->setIs_deleted(0);
					$Reviewmaster_new->setCreated_datetime(date('Y-m-d h:i:s'));
					$em = $this->getDoctrine()->getManager();
					$em->persist($Reviewmaster_new);
					$em->flush();
					for($i=0;$i<count($_FILES['photo']['name']);$i++){						
						
						if(count($_FILES['photo']['name'] == 1)){
							$filename = $_FILES['photo']['name'][$i];
							$tmpname = $_FILES['photo']['tmp_name'][$i];
						}else{
							$filename = $_FILES['photo']['name'][$i];
							$tmpname = $_FILES['photo']['tmp_name'][$i];
						}
						$upload_dir = $this->container->getParameter('upload_dir1').'/Resource/Review/';
						$path = "/bundles/Resource/Review";
						
						$mediatype = $this->getDoctrine()
								->getManager()
								->getRepository(Mediatype::class)
								->findOneBy(array(
									'media_type_name' => 'Image',
									'is_deleted'=>0)
								);
						$allowedExts = explode(',',$mediatype->getMedia_type_allowed());
						$temp = explode('.',$filename);
						$extension = end($temp);
						
						if(in_array($extension, $allowedExts)){
							$media_id = $this->mediauploadAction($filename,$tmpname,$path,$upload_dir, $mediatype->getMedia_type_id());
						}
						
						if(!empty($media_id)){
							$Reviewgallery_new = new Reviewgallery();
							$Reviewgallery_new->setReview_id($Reviewmaster_new->getReview_master_id());
							$Reviewgallery_new->setMedia_id($media_id);
							$Reviewgallery_new->setIs_deleted(0);
							$em = $this->getDoctrine()->getManager();
							$em->persist($Reviewgallery_new);
							$em->flush();
							$this->error = "SFD" ;
							$this->data = array('review_id'=>$Reviewmaster_new->getReview_master_id());
						}else{
							$this->error = "SFND" ;
						}						
					}					
				}else{
					$Reviewmaster_new = new Reviewmaster();
					$Reviewmaster_new->setRestaurant_id($shop_id);
					$Reviewmaster_new->setReview_text($review);
					$Reviewmaster_new->setCreated_by($user_id);
					$Reviewmaster_new->setIs_deleted(0);
					$Reviewmaster_new->setCreated_datetime(date('Y-m-d h:i:s'));
					$em = $this->getDoctrine()->getManager();
					$em->persist($Reviewmaster_new);
					$em->flush();				
		            $this->error = "SFD" ;
					$this->data = array('review_id'=>$Reviewmaster_new->getReview_master_id());
				}
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

}
?>