<?php
namespace WSBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Diningexperience;
use AdminBundle\Entity\Diningexperiencegallery;
use AdminBundle\Entity\Attributecategoryrelation;
class WSAddDiningExperienceController extends WSBaseController {
	
	/**
	 * @Route("/ws/add_dining_experience/{param}",defaults =
	   {"param"=""},requirements={"param"=".+"})
	 *
	 */
	   public function add_dining_experienceAction($param)
	{
	   /*try{*/
			$this->title = "Add Dining Experience";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('nick_name','email','type_restaurant_name','type_brand_name','comment'),
				),
			);
			if($this->validateData($param)){
				$nick_name = $param->nick_name;
                                $email = $param->email;
                                $type_restaurant_name = $param->type_restaurant_name;
                                $restaurant_id = !empty($param->restaurant_id) ? $param->restaurant_id : 0;
                                $branch_id = !empty($param->branch_id) ? $param->branch_id : 0;
                                $type_brand_name = $param->type_brand_name;
                                $comment = $param->comment;
				
                                $dining_experience = new Diningexperience();
				$dining_experience->setNick_name($nick_name);
                                $dining_experience->setEmail_address($email);
                                $dining_experience->setRestaurant_id($type_restaurant_name);
                                $dining_experience->setBranch_id($type_brand_name);
                                $dining_experience->setComments($comment);
                                $dining_experience->setCreate_date(date('Y-m-d h:i:s'));
                                $dining_experience->setDomain_id(0);
                                $dining_experience->setIs_deleted(0);
                                $em = $this->getDoctrine()->getManager();
                                $em->persist($dining_experience);
                                $em->flush(); 

                                $dinnig_expr_id = $dining_experience->getDining_experience_id();
                                #loyaltyPointsChanges
                                $user_master = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findOneBy(['email'=>$email,"is_deleted" => 0]);
                                if($user_master && !empty($dinnig_expr_id)){
                                    $user_id = $user_master->getUser_master_id();

                                    $activity_type = 'dining_new_experience';
                                    $points_added = $this->addCompetitionPointsToUser($user_id,$activity_type,0,$dinnig_expr_id,'dining_experience');

                                }
                                #loyaltyPointsChanges

                               
                                    if (!empty($_FILES['photo'])) {
                                        foreach($_FILES['photo']['name'] as $key=>$val){
                                            $filename = $_FILES['photo']['name'][$key];
                                            $tmpname = $_FILES['photo']['tmp_name'][$key];
                                            //file path to uploads folder (/bundles/design/uploads)
                                            $file_path = $this->container->getParameter('file_path');
                                            $path = $file_path . '/Restoraunt/';
                                            $upload_dir = $this->container->getParameter('upload_dir') . '/Restoraunt/';
                                            $media_id1 = $this->mediauploadAction($filename, $tmpname, $path, $upload_dir, 1);
                                            if(!empty($media_id1)){
                                                $dining_experienceq = new Diningexperiencegallery();
                                                $dining_experienceq->setDining_experience_id($dining_experience->getDining_experience_id());
                                                $dining_experienceq->setImage_id($media_id1);
                                                $dining_experienceq->setIs_deleted(0);
                                                $em = $this->getDoctrine()->getManager();
                                                $em->persist($dining_experienceq);
                                                $em->flush(); 
                                            }   
                                        }              
                                    }
                                
                                $this->error = "SFD" ;
				$this->data = array('dining_id'=>$dining_experience->getDining_experience_id());
			}
			else{
				$this->error = "PIM" ;
			}
			if(empty($response))
			{
				$response=False;
			}
			return $this->responseAction() ;
	   /*}
	   catch (\Exception $e)
		{
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}*/
	}

}
?>