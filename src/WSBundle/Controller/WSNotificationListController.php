<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Organizationmaster;
use AdminBundle\Entity\Shopservicescategoryrelation;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Organizationservicearea;
use AdminBundle\Entity\Areamaster;
use AdminBundle\Entity\Shoptransactionhistory;

class WSNotificationListController extends WSBaseController {

    /**
     * @Route("/ws/notificationList/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function notificationListsAction($param) {

      //  try {
            $this->title = "Notification List";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('user_id'),
                ),
            );

            if ($this->validateData($param)) {

				$em = $this->getDoctrine()->getManager();
				
				$con = $em->getConnection();
				
                $user_id = !empty($param->user_id) ? $param->user_id : 0;
                $device_id = !empty($param->device_id) ? $param->device_id : '';
                $getOnlyCount = !empty($param->getOnlyCount) ? $param->getOnlyCount : null;

                $start = !empty($param->start) ? $param->start : 0;
                $record = !empty($param->record) ? $param->record : 0;
                
                $limit_query = '';
                
                if(!empty($record)){
                    $limit_query = " LIMIT $start,$record ";
                }

                if($user_id == 0){
                    // guest users
                    $sql_unread_count = "SELECT general_notification.title,general_notification.message,general_notification.image_id ,general_notification.general_notification_id,app_push_notification_master.datetime,app_push_notification_master.table_id, app_push_notification_master.status  
                    from app_push_notification_master 						
                    JOIN general_notification ON general_notification.general_notification_id = app_push_notification_master.table_id  
                    where app_push_notification_master.user_id = '$user_id' and device_id = '{$device_id}' and status='unread' and 
                    app_push_notification_master.is_deleted = 0 and general_notification.is_deleted = 0 group by app_push_notification_master.user_id,app_push_notification_master.table_id order by app_push_notification_master.datetime";
                } else {
                    $sql_unread_count = "SELECT general_notification.title,general_notification.message,general_notification.image_id ,general_notification.general_notification_id,app_push_notification_master.datetime,app_push_notification_master.table_id, app_push_notification_master.status  
                    from app_push_notification_master 						
                    JOIN general_notification ON general_notification.general_notification_id = app_push_notification_master.table_id  
                    where app_push_notification_master.user_id = '$user_id' and status='unread' and 
                    app_push_notification_master.is_deleted = 0 and general_notification.is_deleted = 0 group by app_push_notification_master.user_id,app_push_notification_master.table_id order by app_push_notification_master.datetime";
                    //$sql_unread_count = "SELECT notification_id from app_push_notification_master where status = 'unread' and user_id ='$user_id' and is_deleted= 0 group by app_push_notification_master.table_id ";
                }
                $stmt2 = $con->prepare($sql_unread_count);     
                $stmt2->execute();
                $unreadCount = $stmt2->fetchAll();

                $total_unread_notification = 0 ;
                if($unreadCount){
                    $total_unread_notification = count($unreadCount);
                }

                if(!empty($getOnlyCount)){
					
					#getLastUnreadNotification Here
                    
                    if($user_id == 0){
                        $sqlLastNotification = "SELECT general_notification.title,general_notification.message,general_notification.image_id ,general_notification.general_notification_id,app_push_notification_master.datetime,app_push_notification_master.table_id, app_push_notification_master.status  
                        from app_push_notification_master 						
                        JOIN general_notification ON general_notification.general_notification_id = app_push_notification_master.table_id  
                        where app_push_notification_master.user_id = '$user_id' and device_id = '{$device_id}' and 
                        app_push_notification_master.is_deleted = 0 and general_notification.is_deleted = 0 group by app_push_notification_master.user_id,app_push_notification_master.table_id order by app_push_notification_master.datetime DESC LIMIT 0,1";
                    } else {
                        $sqlLastNotification = "SELECT general_notification.title,general_notification.message,general_notification.image_id ,general_notification.general_notification_id,app_push_notification_master.datetime,app_push_notification_master.table_id, app_push_notification_master.status  
                        from app_push_notification_master 						
                        JOIN general_notification ON general_notification.general_notification_id = app_push_notification_master.table_id  
                        where app_push_notification_master.user_id = '$user_id' and 
                        app_push_notification_master.is_deleted = 0 and general_notification.is_deleted = 0 group by app_push_notification_master.user_id,app_push_notification_master.table_id order by app_push_notification_master.datetime DESC LIMIT 0,1";
                    }
                    $stmt = $con->prepare($sqlLastNotification);
                    $stmt->execute();
                    $notifications = $stmt->fetchAll();
                    
                    $notification = '';
                    
                    if(!empty($notifications)){
                        $notifications = $notifications[0];
                        $notification = $notifications['title'] ." - ".$notifications['message'];
                    }
					
					#getLastUnreadNotification Here done
					
                    $this->error = "SFD";
                    $this->data = array(
                        'total_unread_notification' => $total_unread_notification,
						'notification' => $notification
                    );

                    return $this->responseAction();
                }

                if($user_id == 0){
                    $sql = "SELECT general_notification.title,general_notification.message,general_notification.image_id ,general_notification.general_notification_id,app_push_notification_master.datetime,app_push_notification_master.table_id, app_push_notification_master.status  
                    from app_push_notification_master 						
                    JOIN general_notification ON general_notification.general_notification_id = app_push_notification_master.table_id  
                    where app_push_notification_master.user_id = '$user_id' and device_id = '{$device_id}' and 
                    app_push_notification_master.is_deleted = 0 and general_notification.is_deleted = 0 group by app_push_notification_master.user_id,app_push_notification_master.table_id order by app_push_notification_master.datetime DESC $limit_query";
                } else {
                    $sql = "SELECT general_notification.title,general_notification.message,general_notification.image_id ,general_notification.general_notification_id,app_push_notification_master.datetime,app_push_notification_master.table_id, app_push_notification_master.status  
                    from app_push_notification_master 						
                    JOIN general_notification ON general_notification.general_notification_id = app_push_notification_master.table_id  
                    where app_push_notification_master.user_id = '$user_id' and 
                    app_push_notification_master.is_deleted = 0 and general_notification.is_deleted = 0 group by app_push_notification_master.user_id,app_push_notification_master.table_id order by app_push_notification_master.datetime DESC $limit_query";
                }
				
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$notifications = $stmt->fetchAll();
				
				if(!empty($notifications)){
                    $notification_table_ids = '';
                    $Config_live_site = $this->container->getParameter('live_path');
                    
					foreach($notifications as $key=>$value){

                        $media_library = $this->getDoctrine()
                                    ->getRepository('AdminBundle:Medialibrarymaster')
                                    ->findOneBy(array('media_library_master_id' => $value['image_id'], 'is_deleted' => 0));

                        $image_url = null;
                        if(!empty($media_library)){
                            $image_url = $Config_live_site . $media_library->getMedia_location() . "/" . $media_library->getMedia_name();
                        }

                        $notification_table_ids .= $value['table_id'].",";
						$notifications[$key]['image_url'] = $image_url;
						$notifications[$key]['time_stamp'] = strtotime($value['datetime']);
						$notifications[$key]['datetime'] = date('d-m-Y',strtotime($value['datetime']));
						$notifications[$key]['status'] = $value['status'];
                    }
                    
                    if($notification_table_ids != ''){
                        
                        $notification_table_ids = trim($notification_table_ids,',');

                        $sql_update = "UPDATE app_push_notification_master SET status = 'read' where status = 'unread' and user_id ='$user_id' and app_push_notification_master.table_id IN ($notification_table_ids) ";

                        $stmt1 = $con->prepare($sql_update);     
                        $stmt1->execute();
                    }
					
				/*	$response = array(
                        'total_unread_notification' => $total_unread_notification,
                        'notifications' => $notifications,
                    );
                */
                	$response = $notifications;
					$this->error = "SFD";					
				}else{
					$this->error = "NRF";
				}
			} else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            } else {
                $this->error = "SFD";
            }

            $this->data = $response;
            return $this->responseAction();
     /*   } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }*/
    }

}

?>
