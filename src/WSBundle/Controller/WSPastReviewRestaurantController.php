<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Organizationmaster;
use AdminBundle\Entity\Shopservicescategoryrelation;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Organizationservicearea;
use AdminBundle\Entity\Areamaster;
use AdminBundle\Entity\Shoptransactionhistory;

class WSPastReviewRestaurantController extends WSBaseController {

    /**
     * @Route("/ws/pastReviewedRestaurants/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function pastReviewedRestaurantsAction($param) {

      //  try {
            $this->title = "Reviewed Restaurant List";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array('user_id'),
                ),
            );

            if ($this->validateData($param)) {

				$em = $this->getDoctrine()->getManager();
				
				$con = $em->getConnection();
				
                $user_id = !empty($param->user_id) ? $param->user_id : 0;
                $language_id = !empty($param->language_id) ? $param->language_id : 1;
				
				/* $sql = "select 
				restaurant_master.main_restaurant_id,restaurant_master.restaurant_name,restaurant_master.logo_id,evaluation_feedback.evaluation_feedback_id  
						from evaluation_feedback 
						JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id 
                        where evaluation_feedback.user_id = '$user_id' and evaluation_feedback.is_deleted = 0 and restaurant_master.language_id = '$language_id' and evaluation_feedback.status = 'approved' group by evaluation_feedback_id order by evaluation_feedback_id DESC LIMIT 0,5 ";*/
                        
                $sql = "SELECT 
				restaurant_master.main_restaurant_id,restaurant_master.restaurant_name,restaurant_master.logo_id,evaluation_feedback.evaluation_feedback_id  
						from evaluation_feedback 
						JOIN restaurant_master ON restaurant_master.main_restaurant_id = evaluation_feedback.restaurant_id 
                        where evaluation_feedback.is_deleted = 0 and restaurant_master.language_id = '$language_id' and evaluation_feedback.status = 'approved' group by evaluation_feedback_id order by evaluation_feedback_id DESC LIMIT 0,5";
            
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$restaurants = $stmt->fetchAll();
				
				if(!empty($restaurants)){
					
					foreach($restaurants as $key=>$value ){
						$restaurants[$key]['image_url'] = $this->getimage($value['logo_id']);
					}
					
					$response = $restaurants;
					
					$this->error = "SFD";					
				}else{
					$this->error = "NRF";
				}
			} else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            } else {
                $this->error = "SFD";
            }

            $this->data = $response;
            return $this->responseAction();
     /*   } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }*/
    }

}

?>
