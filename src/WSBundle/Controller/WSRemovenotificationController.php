<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class WSRemovenotificationController extends WSBaseController {

    /**
     * @Route("/ws/removeNotification/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function removeNotificationAction($param) {

      //  try {
            $this->title = "Remove Notification";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
                        'notification_id'
                    ),
                ),
            );

            if ($this->validateData($param)) {

                $user_id = isset($param->user_id) ? $param->user_id : 0;
                $device_id = isset($param->device_id) ? $param->device_id : '';
                $notification_id = $param->notification_id;

				$em = $this->getDoctrine()->getManager();
                $notification = $em->getRepository("AdminBundle:Generalnotification")->findBy([
                    'general_notification_id' => $notification_id,
                    'is_deleted' => 0
                ]);

                if(!empty($notification)){
                    // foreach($notification as $_notification){
                    //     $_notification->setIs_deleted(1);
                    //     $em->flush();
                    // }

                    if($user_id == 0){
                        $app_notification = $em->getRepository("AdminBundle:Apppushnotificationmaster")->findBy([
                            'table_id' => $notification_id,
                            'user_id' => $user_id,
                            'device_id' => $device_id,
                            'is_deleted' => 0
                        ]);
                    } else {
                        $app_notification = $em->getRepository("AdminBundle:Apppushnotificationmaster")->findBy([
                            'table_id' => $notification_id,
                            'user_id' => $user_id,
                            'is_deleted' => 0
                        ]);
                    }

                    if(!empty($app_notification)){
                        foreach($app_notification as $_app){
                            $_app->setIs_deleted(1);
                            $em->flush();
                        }
                    }

                	$response = true;
					$this->error = "SFD";					
				}else{
					$this->error = "NRF";
				}
			} else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            } else {
                $this->error = "SFD";
            }

            $this->data = $response;
            return $this->responseAction();
     /*   } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }*/
    }

}

?>
