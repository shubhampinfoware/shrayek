<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Evaluationfeedback;
use AdminBundle\Entity\Evaluationfeedbackgallery;

class WSEvaluateRestaurantController extends WSBaseController {

    /**
     * @Route("/ws/evaluateRestaurant/{param}",defaults = {"param"=""},requirements={"param"=".+"})

     */
    public function evaluateRestaurantAction($param) {
//        try {
        $this->title = "Evaluate Restaurant";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('main_restaurant_id', 'user_id', 'service_level', 'dining_level', 'atmosphere_level', 'clean_level', 'comments'),
            ),
        );
        $response = false;

        if ($this->validateData($param)) {
            $level_array = array(1, 2, 3, 4, 5);
            $evaluation_id = !empty($param->evaluation_id) ? $param->evaluation_id : '';
            $restaurant_id = $param->main_restaurant_id;
            $category_id = !empty($param->main_category_id) ? $param->main_category_id : 0;
            $food_type_id = !empty($param->main_food_type_id) ? $param->main_food_type_id : 0;
            $user_id = $param->user_id;
            $invoice_image_id = !empty($param->invoice_image_id) ? $param->invoice_image_id : 0;
            $gallery_arr_ids = !empty($param->gallery_arr_ids) ? $param->gallery_arr_ids : 0;
            $service_level = $param->service_level;
            $dining_level = $param->dining_level;
            $atmosphere_level = $param->atmosphere_level;
            $price_level = !empty($param->price_level) ? $param->price_level : 1;
            $clean_level = $param->clean_level;
            if (in_array($service_level, $level_array) && in_array($clean_level, $level_array) && in_array($price_level, $level_array) && in_array($atmosphere_level, $level_array) && in_array($dining_level, $level_array)) {
                $comments = urldecode ($param->comments);

                $em = $this->getDoctrine()->getManager();

                //--------------fetch data ----
                if (!empty($category_id) && !empty($food_type_id)) {
                    $restaurant_foodtype_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Restaurantfoodtyperelation')
                            ->findOneBy(array("restaurant_id" => $restaurant_id, "main_foodtype_id" => $food_type_id, "main_caetgory_id" => $category_id, "is_deleted" => 0));
                }
                if (empty($category_id) && !empty($food_type_id)) {
                    $restaurant_foodtype_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Restaurantfoodtyperelation')
                            ->findOneBy(array("restaurant_id" => $restaurant_id, "main_foodtype_id" => $food_type_id, "is_deleted" => 0));
                }
                if (!empty($category_id) && empty($food_type_id)) {
                    $restaurant_foodtype_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Restaurantfoodtyperelation')
                            ->findOneBy(array("restaurant_id" => $restaurant_id, "main_caetgory_id" => $category_id, "is_deleted" => 0));
                }

//                if(empty($category_id) && empty($food_type_id)){
//					$restaurant_foodtype_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Restaurantfoodtyperelation')
//                            ->findOneBy(array("restaurant_id"=>$restaurant_id,"is_deleted"=>0));
//                
//                }
                //  if(!empty($restaurant_foodtype_info)){
//                    if(!empty($category_id) && !empty($food_type_id)){
//                        $entry_exist_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
//                                ->findOneBy(array("user_id"=>$user_id,"category_id"=>$category_id,"food_type_id"=>$food_type_id,"restaurant_id"=>$restaurant_id));
//
//                    }
//                    if(empty($category_id) && !empty($food_type_id)){
//                        $entry_exist_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
//                                ->findOneBy(array("user_id"=>$user_id,"food_type_id"=>$food_type_id,"restaurant_id"=>$restaurant_id));
//
//                    }
//                    if(!empty($category_id) && empty($food_type_id)){
//                        $entry_exist_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
//                                ->findOneBy(array("user_id"=>$user_id,"category_id"=>$category_id,"restaurant_id"=>$restaurant_id));
//
//                    }
//                    if(empty($category_id) && empty($food_type_id)){
//                        $entry_exist_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
//                                ->findOneBy(array("user_id"=>$user_id,"restaurant_id"=>$restaurant_id));
//
//                    }
//                  
                // if(empty($entry_exist_info)){
                $Config_live_site = $this->container->getParameter('live_path');
                $file_path = $this->container->getParameter('file_path');
                $media_id12 = 0;
                if (!empty($_FILES['invoice_image'])) {

                    $filename = $_FILES['invoice_image']['name'];
                    $tmpname = $_FILES['invoice_image']['tmp_name'];
                    //file path to uploads folder (/bundles/design/uploads)
                    $file_path = $this->container->getParameter('file_path');
                    $path = $file_path . '/uploads/WSimages/';
                    $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';
                    $media_id12 = $this->mediauploadAction($filename, $tmpname, $path, $upload_dir, 1);
                } else {
                    $media_id12 = $invoice_image_id;
                }

                //01-03-2018
                // change status approved to unser_evaluation
                // add validation for same restaurant not evalaute 


                if (!empty($evaluation_id)) {
                    $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                            ->findOneBy(array("evaluation_feedback_id" => $evaluation_id, "user_id" => $user_id, "is_deleted" => 0));
                } else {
                    /*                            $restaurant_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                      ->findOneBy(array("restaurant_id"=>$restaurant_id,"user_id"=>$user_id,"is_deleted"=>0));
                      if(empty($restaurant_info)){ */
                    $evaluation_feedback = new Evaluationfeedback();
                    /*                            }else{
                      $response=false;
                      $this->error = "AIR" ;
                      $this->data = $response;
                      return $this->responseAction();
                      } */
                }

                $evaluation_feedback->setUser_id($user_id);
                $evaluation_feedback->setCategory_id($category_id);
                $evaluation_feedback->setFood_type_id($food_type_id);
                $evaluation_feedback->setRestaurant_id($restaurant_id);
                $evaluation_feedback->setService_level($service_level);
                $evaluation_feedback->setDining_level($dining_level);
                $evaluation_feedback->setAtmosphere_level($atmosphere_level);
                $evaluation_feedback->setPrice_level($price_level);
                $evaluation_feedback->setClean_level($clean_level);
                $evaluation_feedback->setComments($comments);
                $evaluation_feedback->setInvoice_image_id($media_id12);
                $evaluation_feedback->setCreated_datetime(date("Y-m-d H:i:s"));
                $evaluation_feedback->setStatus('under_evaluation');
                $evaluation_feedback->setIs_featured('no');
                $evaluation_feedback->setIs_deleted(0);

                $em->persist($evaluation_feedback);
                $em->flush();
                $evaluation_feedback_id = $evaluation_feedback->getEvaluation_feedback_id();

                if (!empty($gallery_arr_ids)) {
                    $evaluation_feedback_gallery = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedbackgallery')
                            ->findBy(array("evaluation_feedback_id" => $evaluation_feedback_id, "is_deleted" => 0));
                    if (!empty($evaluation_feedback_gallery)) {
                        foreach ($evaluation_feedback_gallery as $keyw => $valw) {
                            $valw->setIs_deleted(1);
                            $em->flush();
                        }
                    }
                    $gallery_arr_ids1 = json_decode($gallery_arr_ids);
                    for ($i = 0; $i < count($gallery_arr_ids1); $i++) {
                        $evaluation_feedback_gallery = new Evaluationfeedbackgallery();
                        $evaluation_feedback_gallery->setEvaluation_feedback_id($evaluation_feedback_id);
                        $evaluation_feedback_gallery->setMedia_id($gallery_arr_ids1[$i]);
                        $evaluation_feedback_gallery->setMedia_type_id(1);
                        $evaluation_feedback_gallery->setIs_deleted(0);
                        $em->persist($evaluation_feedback_gallery);
                        $em->flush();
                    }
                }


                $Config_live_site = $this->container->getParameter('live_path');
                $file_path = $this->container->getParameter('file_path');
                if (!empty($_FILES['gallery_arr'])) {
                    foreach ($_FILES['gallery_arr']['name'] as $key => $val) {
                        $filename = $_FILES['gallery_arr']['name'][$key];
                        $tmpname = $_FILES['gallery_arr']['tmp_name'][$key];
                        //file path to uploads folder (/bundles/design/uploads)
                        $file_path = $this->container->getParameter('file_path');
                        $path = $file_path . '/uploads/WSimages/';
                        $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';
                        $media_id1 = $this->mediauploadAction($filename, $tmpname, $path, $upload_dir, 1);
                        if (!empty($media_id1)) {
                            $evaluation_feedback_gallery = new Evaluationfeedbackgallery();
                            $evaluation_feedback_gallery->setEvaluation_feedback_id($evaluation_feedback_id);
                            $evaluation_feedback_gallery->setMedia_id($media_id1);
                            $evaluation_feedback_gallery->setMedia_type_id(1);
                            $evaluation_feedback_gallery->setIs_deleted(0);
                            $em->persist($evaluation_feedback_gallery);
                            $em->flush();
                        }
                    }
                }
                $tot_ev_points = 0;
                $ratings = array(
                    'service_level' => $service_level,
                    'dining_level' => $dining_level,
                    'atmoshphere_level' => $atmosphere_level,
                    'price_level' => $price_level,
                    'clean_level' => $clean_level
                );
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $restaurant_total_evpointsQuery = "SELECT sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $restaurant_id;
                $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                $tot_ev_points = $tot_ev_points + $evpoints;
                $userdetails = $this->userdetails($user_id);
                $evaluation_invoice_url = null;
                if ($media_id12 != 0) {
                    $evaluation_invoice_url = array('media_id' => $media_id12, 'url' => $this->getimage($media_id12));
                }
                $get_gallery_sql = "SELECT media_id FROM `evaluation_feedback_gallery` where evaluation_feedback_id='" . $evaluation_feedback_id . "' and is_deleted=0";
                $stmt = $con->prepare($get_gallery_sql);
                $stmt->execute();
                $evaluation_gallery = $stmt->fetchAll();
                $photos = null;
                if (!empty($evaluation_gallery)) {
                    foreach ($evaluation_gallery as $img) {
                        if ($this->getimage($img['media_id']) != '') {
                            $photos [] = array('media_id' => $img['media_id'], 'url' => $this->getimage($img['media_id']));
                        }
                    }
                    $invoice = $photos;
                } else {
                    $invoice = null;
                }
                if (!empty($restaurant_id)) {
                    $reslist = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Restaurantmaster')
                            ->findOneBy(array("restaurant_master_id" => $restaurant_id));
                    if (!empty($reslist)) {
                        $shop_name = $reslist->getRestaurant_name();
                        $shop_branch = $reslist->getRestaraunt_branch();

                        if ($reslist->getLogo_id() != 0) {
                            $shop_logo = $this->getimage($reslist->getLogo_id());
                        } else {
                            $shop_logo = null;
                        }
                    }
                }
                $points_percentage = ( $evpoints * 25 ) / 100;
                $rate = ( $points_percentage * 5 ) / 100;
                //$response[$akey]['points_percentage'] = $points_percentage ;
                //$evaluation_details[$akey]['rate_value_with_decimal'] = $rate ;
                //$evaluation_details[$akey]['rate'] = number_format((float)$rate, 1, '.', '') ;
                $evaluation_feedback = null;
                if (!empty($user_id)) {
                    $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                            ->findOneBy(array("restaurant_id" => $restaurant_id, "user_id" => $user_id, "is_deleted" => 0));
                }
                $response[] = array(
                    'shop_id' => $restaurant_id,
                    'shop_name' => $shop_name,
                    'shop_logo' => $shop_logo,
                    'shop_branch' => $shop_branch,
                    'images' => $invoice,
                    'invoice' => $evaluation_invoice_url,
                    'comments' => $comments,
                    'rating' => $ratings,
                    'status' => 'under_evaluation',
                    'user_details' => $userdetails,
                    'reason_rejection' => '',
                    'evaluation_date' => strtotime(date("Y-m-d H:i:s")) * 1000,
                    "total_points_of_shop" => $evpoints,
                    'points_percentage' => $points_percentage,
                    'rate_value_with_decimal' => $rate,
                    'rate' => number_format((float) $rate, 1, '.', ''),
                    "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                );

                $this->error = "SFD";
//                    }
//                    else{
//                        
//                        $this->error = "AIR";
//                    }
//                }
//                else{
//                     $response = false;
//                    $this->error = "PIW";
//                }
            } else {
                $response = false;
                $this->error = "PIW";
            }
        } else {
            $this->error = "PIM";
        }
        if (empty($response) && $this->error != "PIM" && $this->error != "PIW") {
            $response = false;
            $this->error = "NRF1";
        }
        $this->data = $response;
        return $this->responseAction();
//        } catch (\Exception $e) {
//            $this->error = "SFND" . $e;
//            $this->data = false;
//            return $this->responseAction();
//        }
    }

}

?>