<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Contactmaster;
use AdminBundle\Entity\Medicalreportimage;
use AdminBundle\Entity\Contactusmaster;
use AdminBundle\Entity\Evaluationadmincomment;

class WSMyEvaluatedShopController extends WSBaseController {

    /**
     * @Route("/ws/my_evaluted_shop/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function myRestaurantEvalatedAction($param) {
        try {
            $this->title = "My Shop Evaluatation";
            $param = $this->requestAction($this->getRequest(), 0);
            // use to validate required param validation
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(
                    //'user_type'
                    )
            ));
//			var_dump($param);exit;
            if ($this->validateData($param)) {
                $year = isset($param->year) ? $param->year : '';
                $shop_id = isset($param->shop_id) ? $param->shop_id : '';
                
				$get_all_shop_eval = !empty($param->get_all_shop_eval) ? $param->get_all_shop_eval : 'no';
				
                $branch_id = isset($param->branch_id) ? $param->branch_id : 0;
				
                $user_id = isset($param->user_id) ? $param->user_id : '';
                $is_rejected = isset($param->is_rejected) ? $param->is_rejected : '';
                $language_id = isset($param->language_id) ? $param->language_id : '1';

				$limit_condition = '';
				if(isset($param->start_range) && isset($param->limit)){
					$start_range = $param->start_range;
					$limit = $param->limit;
					$limit_condition = " Limit {$start_range}, {$limit}";
				}

                $get_level_sql = "select * from level_master where status='active' and is_deleted=0";
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($get_level_sql);
                $stmt->execute();
                $level_data = $stmt->fetchAll();
                $keyword = NULL;
                if (isset($param->keyword) && !empty($param->keyword) && !empty($user_id)) {
                    /* $get_evaluation_sql = "select rest.restaurant_name,branch.branch_name,rest.restaurant_master_id,rest.logo_id,rest.restaraunt_branch,eval.evaluation_feedback_id,eval.status,eval.comments,eval.service_level,eval.dining_level,eval.atmosphere_level,eval.eval_type,eval.packaging_level,eval.delivery_speed_level,
									eval.price_level,eval.clean_level,eval.invoice_image_id,eval.created_datetime,eval.main_branch_id 
									from evaluation_feedback eval 
									left join restaurant_master rest on eval.restaurant_id=rest.main_restaurant_id 
									left join branch_master branch on eval.main_branch_id=branch.main_branch_master_id and branch.language_id=$language_id 									
									where rest.is_deleted = 0 and branch.is_deleted = 0 and eval.is_deleted=0 and eval.user_id='" . $param->user_id . "' 
                                    and (rest.restaurant_name like '%" . $param->keyword . "%' OR eval.comments like '%" . $param->keyword . "%' ) group by eval.evaluation_feedback_id order by eval.created_datetime desc {$limit_condition}"; */

                        $get_evaluation_sql = "select rest.restaurant_name,branch.branch_name,rest.restaurant_master_id,rest.logo_id,rest.restaraunt_branch,eval.evaluation_feedback_id,eval.status,eval.comments,eval.service_level,eval.dining_level,eval.atmosphere_level,eval.eval_type,eval.packaging_level,eval.delivery_speed_level,
									eval.price_level,eval.clean_level,eval.invoice_image_id,eval.created_datetime,eval.main_branch_id 
									from evaluation_feedback eval 
									left join restaurant_master rest on eval.restaurant_id=rest.main_restaurant_id 
									left join branch_master branch on eval.main_branch_id=branch.main_branch_master_id and branch.language_id=$language_id 									
									where rest.is_deleted = 0 and branch.is_deleted = 0 and eval.is_deleted=0 and eval.user_id='" . $param->user_id . "' 
                                    and (rest.restaurant_name like '%" . $param->keyword . "%' OR eval.comments like '%" . $param->keyword . "%' ) and eval.comments != '' group by eval.evaluation_feedback_id order by eval.created_datetime desc {$limit_condition}";
                                    

                }
                $where = $status_clause = '';
                if (!empty($user_id) && empty($shop_id)) {
                    $where .= " and eval.user_id='" . $param->user_id . "'";
                }
                if (!empty($shop_id)) {
                    $where .= " and eval.restaurant_id='" . $param->shop_id . "'";
                }
				
#branchRelated Changes		
                if (!empty($branch_id)) {
                    $where .= " and eval.main_branch_id='" . $param->branch_id . "'";
                }		
#branchRelated Changes done				
                if (!empty($is_rejected) && $is_rejected == 'no') {
//					print($is_rejected);exit;
                    $where .= " and eval.status !='rejected'";
                }

                /* $get_evaluation_sql = "select rest.restaurant_name,branch.branch_name,rest.restaurant_master_id,rest.main_restaurant_id,rest.logo_id,rest.description,rest.address_id,rest.phone_number,rest.timings,rest.restaraunt_branch,eval.evaluation_feedback_id,eval.eval_type,eval.packaging_level,eval.delivery_speed_level,eval.status,eval.comments,eval.service_level,eval.dining_level,eval.atmosphere_level, eval.user_id,eval.price_level,eval.clean_level,eval.invoice_image_id,eval.created_datetime, eval.rejected_reason,eval.main_branch_id from evaluation_feedback eval 
				left join restaurant_master rest on eval.restaurant_id=rest.main_restaurant_id 
				left join branch_master branch on eval.main_branch_id=branch.main_branch_master_id and branch.language_id=$language_id 
				where rest.is_deleted = 0 and branch.is_deleted = 0 $where and rest.language_id=$language_id and eval.is_deleted='0' group by eval.evaluation_feedback_id order by eval.created_datetime desc"; */
				
				if(!empty($param->get_all_shop_eval) && $param->get_all_shop_eval == 'yes'){
                    $status_clause = " AND eval.status != 'under_evaluation' and eval.status != 'rejected' AND eval.status != 'pending' ";
                }

                if (!empty($param->shop_id)){

                    $status_clause = " AND eval.status != 'under_evaluation' and eval.status != 'rejected' AND eval.status != 'pending' ";

                }
				
				$get_evaluation_sql = "select rest.restaurant_name,branch.branch_name,rest.restaurant_master_id,rest.main_restaurant_id,rest.logo_id,rest.description,rest.address_id,rest.phone_number,rest.timings,rest.restaraunt_branch,eval.evaluation_feedback_id,eval.eval_type,eval.packaging_level,eval.delivery_speed_level,eval.status,eval.comments,eval.service_level,eval.dining_level,eval.atmosphere_level, eval.user_id,eval.price_level,eval.clean_level,eval.invoice_image_id,eval.created_datetime, eval.rejected_reason,eval.main_branch_id from evaluation_feedback eval 
				left join restaurant_master rest on eval.restaurant_id=rest.main_restaurant_id 
				left join branch_master branch on eval.main_branch_id=branch.main_branch_master_id and branch.language_id=$language_id 
                where rest.is_deleted = 0 and branch.is_deleted = 0 $where and rest.language_id=$language_id and eval.is_deleted='0' and eval.comments != '' {$status_clause} group by eval.evaluation_feedback_id  order by eval.created_datetime desc {$limit_condition}";

                /* $get_evaluation_sql = "select rest.restaurant_name,branch.branch_name,rest.restaurant_master_id,rest.main_restaurant_id,rest.logo_id,rest.description,rest.address_id,rest.phone_number,rest.timings,rest.restaraunt_branch,eval.evaluation_feedback_id,eval.eval_type,eval.packaging_level,eval.delivery_speed_level,eval.status,eval.comments,eval.service_level,eval.dining_level,eval.atmosphere_level, eval.user_id,eval.price_level,eval.clean_level,eval.invoice_image_id,eval.created_datetime, eval.rejected_reason,eval.main_branch_id from evaluation_feedback eval 
				left join restaurant_master rest on eval.restaurant_id=rest.main_restaurant_id 
				left join branch_master branch on eval.main_branch_id=branch.main_branch_master_id and branch.language_id=$language_id 
				where rest.is_deleted = 0 and branch.is_deleted = 0 $where and rest.language_id=$language_id and eval.is_deleted='0' {$status_clause} group by eval.evaluation_feedback_id  order by eval.created_datetime desc {$limit_condition}"; */

//                echo $get_evaluation_sql;exit;
                $em = $this->getDoctrine()->getManager();
                $con = $em->getConnection();
                $stmt = $con->prepare($get_evaluation_sql);
                $stmt->execute();
                $evaluation_data = $stmt->fetchAll();
				
                $evaluation_details = null;
                $tot_ev_points = 0;

                if (!empty($evaluation_data)) {

                    $service_level = $dining_level = $atm_level = $price_level = $clean_level = 0.0;

                    foreach ($evaluation_data as $evaluation_data_details) {

                         $service_level = $dining_level = $atm_level = $price_level = $clean_level = 0.0;

                        if (!empty($year)) {
                            if (date('Y', strtotime($evaluation_data_details['created_datetime'])) == $year) {
                                $shop_name = $evaluation_data_details['restaurant_name'];
                                $shop_branch = $evaluation_data_details['branch_name'];
                                $phone_number = $evaluation_data_details['phone_number'];
                                $timings = $evaluation_data_details['timings'];
                                $shop_desc = $evaluation_data_details['description'];
                                if ($evaluation_data_details['logo_id'] != 0) {
                                    $shop_logo = $this->getimage($evaluation_data_details['logo_id']);
                                } else {
                                    $shop_logo = null;
                                }
                                //admin_comment.............
                                $admin_comment = '';
                                $admin_comment_details = $this->getDoctrine()->getManager()->getRepository(Evaluationadmincomment :: class)->findOneBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $evaluation_data_details['evaluation_feedback_id']));
                                if ($admin_comment_details) {
                                    $admin_comment = $admin_comment_details->getAdmin_comment();
                                }
                                //admin comment.............
                                $get_gallery_sql = "SELECT media_id FROM `evaluation_feedback_gallery` where evaluation_feedback_id='" . $evaluation_data_details['evaluation_feedback_id'] . "' and is_deleted=0";
                                $stmt = $con->prepare($get_gallery_sql);
                                $stmt->execute();
                                $evaluation_gallery = $stmt->fetchAll();
                                $photos = null;
                                if (!empty($evaluation_gallery)) {
                                    foreach ($evaluation_gallery as $img) {
                                        if ($this->getimage($img['media_id']) != '') {
                                            $photos [] = array('media_id' => $img['media_id'], 'url' => $this->getimage($img['media_id']));
                                        }
                                    }
                                    $invoice = $photos;
                                } else {
                                    $invoice = null;
                                }
                                $evaluation_invoice_url = null;
                                if ($evaluation_data_details['invoice_image_id'] != 0)
                                    $evaluation_invoice_url = array('media_id' => $evaluation_data_details['invoice_image_id'], 'url' => $this->getimage($evaluation_data_details['invoice_image_id']));


                                $comments = $evaluation_data_details['comments'];
                                foreach ($level_data as $level_data_details) {
                                    if ($evaluation_data_details['service_level'] == $level_data_details['level_number']) {
                                        $service_level = $level_data_details['level_number'];
                                    }
                                    if ($evaluation_data_details['dining_level'] == $level_data_details['level_number']) {
                                        $dining_level = $level_data_details['level_number'];
                                    }
                                    if ($evaluation_data_details['atmosphere_level'] == $level_data_details['level_number']) {
                                        $atm_level = $level_data_details['level_number'];
                                    }
                                    if ($evaluation_data_details['price_level'] == $level_data_details['level_number']) {
                                        $price_level = $level_data_details['level_number'];
                                    }
                                    if ($evaluation_data_details['clean_level'] == $level_data_details['level_number']) {
                                        $clean_level = $level_data_details['level_number'];
                                    }
									if ($evaluation_data_details['delivery_speed_level'] == $level_data_details['level_number']) {
                                        $speed_level = $level_data_details['level_number'];
                                    }
									if ($evaluation_data_details['packaging_level'] == $level_data_details['level_number']) {
                                        $packaging_level = $level_data_details['level_number'];
                                    }									
                                }
                                $status = $evaluation_data_details['status'];
                                $reason_rejection = $evaluation_data_details['rejected_reason'];
                                $evaluation_date = $evaluation_data_details['created_datetime'];

                                $ratings = array(
                                    'service_level' => $service_level,
                                    'dining_level' => $dining_level,
                                    'atmoshphere_level' => $atm_level,
                                    'price_level' => $price_level,
                                    'clean_level' => $clean_level,
									'speed_level' => $evaluation_data_details['delivery_speed_level'],
									'packaging_level' => $evaluation_data_details['packaging_level'],
                                );
								
								$branch_where_eval = '';
								
								if (!empty($branch_id)) {
									$branch_where_eval = " AND main_branch_id = '".$evaluation_data_details['main_branch_id']."' ";	
								}
								
                                $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $evaluation_data_details['main_restaurant_id'] . "' and is_deleted = 0 $branch_where_eval) as total_rate,sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level + evaluation_feedback.delivery_speed_level + evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $evaluation_data_details['main_restaurant_id'];
                                $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                                $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                                $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                                $tot_ev_points = $tot_ev_points + $evpoints;

                                // 03-03-2018 

                                $userdetails = $this->userdetails($evaluation_data_details['user_id']);
                                $query = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $language_id . "' group by cm.main_category_id";
                                $cat = $this->firequery($query);
                                $category = null;
                                if (!empty($cat)) {
                                    foreach ($cat as $ca) {


                                        // get cuisines
                                        $query = "select * from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $evaluation_data_details['main_restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$language_id and rfr.main_caetgory_id=" . $ca['main_category_id'];

                                        $cuisines = null;


                                        $cuisine = $this->firequery($query);


                                        if (!empty($cuisine)) {

                                            foreach ($cuisine as $val) {


                                                $cuisines[] = array(
                                                    "food_type_id" => $val['main_food_type_id'],
                                                    "food_type_name" => $val['food_type_name'],
                                                    "food_type_image" => $this->getimage($val['food_type_image_id'])
                                                );
                                            }
                                            $category[] = array("category_id" => $ca['main_category_id'],
                                                "category_name" => $ca['category_name'],
                                                "category_logo" => $this->getimage($ca['category_image_id']), "food_type" => $cuisines);
                                        }
                                    }
                                }
                                if (!empty($userdetails)) {
                                    $evaluation_feedback = null;
                                    if (!empty($user_id)) {
                                        $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                                ->findOneBy(array("restaurant_id" => $evaluation_data_details['main_restaurant_id'], "user_id" => $user_id, "is_deleted" => 0));
                                        $faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
                                                ->findOneBy(array(
                                            'shop_id' => $evaluation_data_details['main_restaurant_id'],
                                            'user_id' => $user_id,
                                            'type' => 'add',
                                            'is_deleted' => 0
                                                )
                                        );
                                    }
									
#get Like Count from Evaluationlikerelation
									$likes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'operation'=>'like'));
									$likes = 0;
									if($likes_data){
										$likes = count($likes_data);
									}
									
									$dislikes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'operation'=>'dislike'));
									$dislikes = 0;
									if($dislikes_data){
										$dislikes = count($dislikes_data);
									}
									
#get Like Count from Evaluationlikerelation ends
									
                                    $query1 = "SELECT * from address_master where main_address_id='" . $evaluation_data_details['address_id'] . "' and language_id=" . $language_id . " and is_deleted=0";
                                    $addresss = $this->firequery($query1);
                                    $flag = 'no';
                                    if (!empty($user_id)) {
										
									#check liked by user or not flag
									
									$likes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findOneBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'user_id'=>$user_id,'operation'=>'like'));
									
									$like_by_user = false;
									if($likes_data_user){
										$like_by_user = true;
									}

									$dislikes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findOneBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'user_id'=>$user_id,'operation'=>'dislike'));
									
									$dislike_by_user = false;
									if($dislikes_data_user){
										$dislike_by_user = true;
									}									
									
									#check liked by user or not flag ends
									if (!empty($param->shop_id)){
										if($status != 'under_evaluation' && $status != 'rejected'){
											
										$evaluation_details[] = array(
                                            'evaluation_id' => $evaluation_data_details['evaluation_feedback_id'],
                                            'main_branch_id' => $evaluation_data_details['main_branch_id'],
                                            'rating_type' => $evaluation_data_details['eval_type'],
											'rating_type' =>$evaluation_data_details['eval_type'],
                                            'shop' => array('shop_id' => $evaluation_data_details['main_restaurant_id'],
                                                'shop_name' => $shop_name,
                                                'shop_desc' => $shop_desc,
                                                'shop_logo' => $shop_logo,
                                                'shop_branch' => $shop_branch,
                                                "cuisines" => $category,
                                                "phone_number" => $phone_number,
                                                "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                                "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                                                "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                                                "timing" => $timings,
                                                "total_points_of_shop" => $total_rate,
                                                "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                                "is_bookmark" => !empty($faq_info) ? true : false),
                                            'images' => $invoice,
                                            'invoice' => $evaluation_invoice_url,
                                            'comments' => $comments,
                                            'rating' => $ratings,
                                            'status' => $status,
                                            'user_details' => $userdetails,
                                            'reason_rejection' => $reason_rejection,
                                            'admin_comment' => $admin_comment,
                                            'likes' => $likes,
                                            'dislikes' => $dislikes,
											'like_by_user'=>$like_by_user,
											'dislike_by_user'=>$dislike_by_user,
                                            'evaluation_date' => strtotime($evaluation_date) * 1000);											
											
										}
									}else{
											
										$evaluation_details[] = array(
                                            'evaluation_id' => $evaluation_data_details['evaluation_feedback_id'],
											'main_branch_id' => $evaluation_data_details['main_branch_id'],
                                            'rating_type' => $evaluation_data_details['eval_type'],
											'rating_type' =>$evaluation_data_details['eval_type'],
                                            'shop' => array('shop_id' => $evaluation_data_details['main_restaurant_id'],
                                                'shop_name' => $shop_name,
                                                'shop_desc' => $shop_desc,
                                                'shop_logo' => $shop_logo,
                                                'shop_branch' => $shop_branch,
                                                "cuisines" => $category,
                                                "phone_number" => $phone_number,
                                                "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                                "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                                                "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                                                "timing" => $timings,
                                                "total_points_of_shop" => $total_rate,
                                                "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                                "is_bookmark" => !empty($faq_info) ? true : false),
                                            'images' => $invoice,
                                            'invoice' => $evaluation_invoice_url,
                                            'comments' => $comments,
                                            'rating' => $ratings,
                                            'status' => $status,
                                            'user_details' => $userdetails,
                                            'reason_rejection' => $reason_rejection,
                                            'admin_comment' => $admin_comment,
                                            'likes' => $likes,
                                            'dislikes' => $dislikes,
											'like_by_user'=>$like_by_user,
											'dislike_by_user'=>$dislike_by_user,
                                            'evaluation_date' => strtotime($evaluation_date) * 1000);
										
									}
									
                                    }
									
                                    if (empty($user_id) && $status != 'under_evaluation' && $status != 'rejected') {
                                        $evaluation_details[] = array(
                                            'evaluation_id' => $evaluation_data_details['evaluation_feedback_id'],
											'main_branch_id' => $evaluation_data_details['main_branch_id'],
											'rating_type' => $evaluation_data_details['eval_type'],
                                            'shop' => array('shop_id' => $evaluation_data_details['main_restaurant_id'],
                                                'shop_name' => $shop_name,
                                                'shop_desc' => $shop_desc,
                                                'shop_logo' => $shop_logo,
                                                'shop_branch' => $shop_branch,
                                                "cuisines" => $category,
                                                "phone_number" => $phone_number,
                                                "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                                "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                                                "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                                                "timing" => $timings,
                                                "total_points_of_shop" => $total_rate,
                                                "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                                "is_bookmark" => !empty($faq_info) ? true : false),
                                            'images' => $invoice,
                                            'invoice' => $evaluation_invoice_url,
                                            'comments' => $comments,
                                            'rating' => $ratings,
                                            'status' => $status,
                                            'user_details' => $userdetails,
                                            'reason_rejection' => $reason_rejection,
                                            'admin_comment' => $admin_comment,
											'likes'=>$likes,
											'dislikes'=>$dislikes,
											'like_by_user'=>false,
											'dislike_by_user'=>false,
                                            'evaluation_date' => strtotime($evaluation_date) * 1000);
											
                                    }
                                }
                            }
                        } else {

                            $shop_name = $evaluation_data_details['restaurant_name'];
                            $shop_branch = $evaluation_data_details['branch_name'];
                            $phone_number = $evaluation_data_details['phone_number'];
                            $timings = $evaluation_data_details['timings'];
                            $shop_desc = $evaluation_data_details['description'];
                            if ($evaluation_data_details['logo_id'] != 0) {
                                $shop_logo = $this->getimage($evaluation_data_details['logo_id']);
                            } else {
                                $shop_logo = null;
                            }

                            //admin_comment.............
                            $admin_comment = '';
                            $admin_comment_details = $this->getDoctrine()->getManager()->getRepository(Evaluationadmincomment :: class)->findOneBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $evaluation_data_details['evaluation_feedback_id']));
                            if ($admin_comment_details) {
                                $admin_comment = $admin_comment_details->getAdmin_comment();
                            }
                            //admin comment.............

                            $get_gallery_sql = "SELECT media_id FROM `evaluation_feedback_gallery` where evaluation_feedback_id='" . $evaluation_data_details['evaluation_feedback_id'] . "' and is_deleted=0";
                            $stmt = $con->prepare($get_gallery_sql);
                            $stmt->execute();
                            $evaluation_gallery = $stmt->fetchAll();
                            $photos = null;
                            if (!empty($evaluation_gallery)) {
                                foreach ($evaluation_gallery as $img) {
                                    if ($this->getimage($img['media_id']) != '') {
                                        $photos [] = array('media_id' => $img['media_id'], 'url' => $this->getimage($img['media_id']));
                                    }
                                }
                                $invoice = $photos;
                            } else {
                                $invoice = null;
                            }
                            $evaluation_invoice_url = null;
                            if ($evaluation_data_details['invoice_image_id'] != 0)
                                $evaluation_invoice_url = array('media_id' => $evaluation_data_details['invoice_image_id'], 'url' => $this->getimage($evaluation_data_details['invoice_image_id']));

                            $comments = $evaluation_data_details['comments'];
                            foreach ($level_data as $level_data_details) {
                                if ($evaluation_data_details['service_level'] == $level_data_details['level_number']) {
                                    $service_level = $level_data_details['level_number'];
                                }
                                if ($evaluation_data_details['dining_level'] == $level_data_details['level_number']) {
                                    $dining_level = $level_data_details['level_number'];
                                }
                                if ($evaluation_data_details['atmosphere_level'] == $level_data_details['level_number']) {
                                    $atm_level = $level_data_details['level_number'];
                                }
                                if ($evaluation_data_details['price_level'] == $level_data_details['level_number']) {
                                    $price_level = $level_data_details['level_number'];
                                }
                                if ($evaluation_data_details['clean_level'] == $level_data_details['level_number']) {
                                    $clean_level = $level_data_details['level_number'];
                                }
								
								if ($evaluation_data_details['delivery_speed_level'] == $level_data_details['level_number']) {
                                        $speed_level = $level_data_details['level_number'];
                                    }
								if ($evaluation_data_details['packaging_level'] == $level_data_details['level_number']) {
									$packaging_level = $level_data_details['level_number'];
								}
                            }
                            $status = $evaluation_data_details['status'];
                            ;
                            $reason_rejection = $evaluation_data_details['rejected_reason'];
                            $evaluation_date = $evaluation_data_details['created_datetime'];

                            $ratings = array(
                                'service_level' => $service_level,
                                'dining_level' => $dining_level,
                                'atmoshphere_level' => $atm_level,
                                'price_level' => $price_level,
                                'clean_level' => $clean_level,
                                'speed_level' => $evaluation_data_details['delivery_speed_level'],
                                'packaging_level' => $evaluation_data_details['packaging_level'],
                            );
							
							$branch_where_eval = '';
							
							if (!empty($branch_id)) {
								$branch_where_eval = " AND main_branch_id = '".$evaluation_data_details['main_branch_id']."' ";	
							}
								
                            $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $evaluation_data_details['main_restaurant_id'] . "' and is_deleted = 0 $branch_where_eval) as total_rate,sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level + evaluation_feedback.delivery_speed_level + evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $evaluation_data_details['main_restaurant_id'];
                            $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                            $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                            $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                            $tot_ev_points = $tot_ev_points + $evpoints;
                            $evaluation_feedback = null;
                            if (!empty($user_id)) {
                                $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                        ->findOneBy(array("restaurant_id" => $evaluation_data_details['main_restaurant_id'], "user_id" => $user_id, "is_deleted" => 0));
                                $faq_info = $em->getRepository('AdminBundle:Bookmarkmaster')
                                        ->findOneBy(array(
                                    'shop_id' => $evaluation_data_details['main_restaurant_id'],
                                    'user_id' => $user_id,
                                    'type' => 'add',
                                    'is_deleted' => 0
                                        )
                                );
                            }
                            $userdetails = $this->userdetails($evaluation_data_details['user_id']);
                            $query = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $language_id . "' group by cm.main_category_id";
							
							$cat = $this->firequery($query);
                            $category = null;
                            if (!empty($cat)) {
                                foreach ($cat as $ca) {


                                    // get cuisines
                                    $query = "select * from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $evaluation_data_details['main_restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$language_id and rfr.main_caetgory_id=" . $ca['main_category_id'];

                                    $cuisines = null;
                                    $cuisine = $this->firequery($query);
									
									if (!empty($cuisine)) {

                                        foreach ($cuisine as $val) {


                                            $cuisines[] = array(
                                                "food_type_id" => $val['main_food_type_id'],
                                                "food_type_name" => $val['food_type_name'],
                                                "food_type_image" => $this->getimage($val['food_type_image_id'])
                                            );
                                        }
                                        $category[] = array("category_id" => $ca['main_category_id'],
                                            "category_name" => $ca['category_name'],
                                            "category_logo" => $this->getimage($ca['category_image_id']), "food_type" => $cuisines);
                                    }
                                }
                            }
							
#get Like Count from Evaluationlikerelation
									$likes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'operation'=>'like'));
									$likes = 0;
									if($likes_data){
										$likes = count($likes_data);
									}
									
									$dislikes_data = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'operation'=>'dislike'));
									$dislikes = 0;
									if($dislikes_data){
										$dislikes = count($dislikes_data);
									}
#get Like Count from Evaluationlikerelation ends
							
                            if (!empty($userdetails)) {
                                $query1 = "SELECT * from address_master where main_address_id='" . $evaluation_data_details['address_id'] . "' and language_id=" . $language_id . " and is_deleted=0";
                                $addresss = $this->firequery($query1);
                                if (!empty($user_id)) {
									
									#check liked by user or not flag
									
									$likes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findOneBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'user_id'=>$user_id,'operation'=>'like'));
									
									$like_by_user = false;
									if($likes_data_user){
										$like_by_user = true;
									}									
									$dislikes_data_user = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Evaluationlikerelation")->findOneBy(array('evaluation_id'=>$evaluation_data_details['evaluation_feedback_id'],'is_deleted'=>0,'user_id'=>$user_id,'operation'=>'dislike'));
									
									$dislike_by_user = false;
									if($dislikes_data_user){
										$dislike_by_user = true;
									}									
									
									#check liked by user or not flag ends
									
									if (!empty($param->shop_id)){
										if($status != 'under_evaluation' && $status != 'rejected'){
											
										$evaluation_details[] = array(
                                            'evaluation_id' => $evaluation_data_details['evaluation_feedback_id'],
											'main_branch_id' => $evaluation_data_details['main_branch_id'],
                                            'rating_type' => $evaluation_data_details['eval_type'],
											'rating_type' =>$evaluation_data_details['eval_type'],
                                            'shop' => array('shop_id' => $evaluation_data_details['main_restaurant_id'],
                                                'shop_name' => $shop_name,
                                                'shop_desc' => $shop_desc,
                                                'shop_logo' => $shop_logo,
                                                'shop_branch' => $shop_branch,
                                                "cuisines" => $category,
                                                "phone_number" => $phone_number,
                                                "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                                "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                                                "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                                                "timing" => $timings,
                                                "total_points_of_shop" => $total_rate,
                                                "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                                "is_bookmark" => !empty($faq_info) ? true : false),
                                            'images' => $invoice,
                                            'invoice' => $evaluation_invoice_url,
                                            'comments' => $comments,
                                            'rating' => $ratings,
                                            'status' => $status,
                                            'user_details' => $userdetails,
                                            'reason_rejection' => $reason_rejection,
                                            'admin_comment' => $admin_comment,
                                            'likes' => $likes,
                                            'dislikes' => $dislikes,
											'like_by_user'=>$like_by_user,
											'dislike_by_user'=>$dislike_by_user,
                                            'evaluation_date' => strtotime($evaluation_date) * 1000);		
											
										}
									}else{
											
										$evaluation_details[] = array(
                                            'evaluation_id' => $evaluation_data_details['evaluation_feedback_id'],
											'main_branch_id' => $evaluation_data_details['main_branch_id'],
                                            'rating_type' => $evaluation_data_details['eval_type'],
											'rating_type' =>$evaluation_data_details['eval_type'],
                                            'shop' => array('shop_id' => $evaluation_data_details['main_restaurant_id'],
                                                'shop_name' => $shop_name,
                                                'shop_desc' => $shop_desc,
                                                'shop_logo' => $shop_logo,
                                                'shop_branch' => $shop_branch,
                                                "cuisines" => $category,
                                                "phone_number" => $phone_number,
                                                "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                                "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                                                "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                                                "timing" => $timings,
                                                "total_points_of_shop" => $total_rate,
                                                "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                                "is_bookmark" => !empty($faq_info) ? true : false),
                                            'images' => $invoice,
                                            'invoice' => $evaluation_invoice_url,
                                            'comments' => $comments,
                                            'rating' => $ratings,
                                            'status' => $status,
                                            'user_details' => $userdetails,
                                            'reason_rejection' => $reason_rejection,
                                            'admin_comment' => $admin_comment,
                                            'likes' => $likes,
											'like_by_user'=>$like_by_user,
											'dislikes' => $dislikes,
											'dislike_by_user'=>$dislike_by_user,
                                            'evaluation_date' => strtotime($evaluation_date) * 1000);
										
									}
										
                                }
								
                                if (empty($user_id) && $status != 'under_evaluation' && $status != 'rejected') {
                                    $evaluation_details[] = array(
                                        'evaluation_id' => $evaluation_data_details['evaluation_feedback_id'],
										'main_branch_id' => $evaluation_data_details['main_branch_id'],
										'rating_type' => $evaluation_data_details['eval_type'],
                                        'shop' => array('shop_id' => $evaluation_data_details['main_restaurant_id'],
                                            'shop_name' => $shop_name,
                                            'shop_desc' => $shop_desc,
                                            'shop_logo' => $shop_logo,
                                            'shop_branch' => $shop_branch,
                                            "cuisines" => $category,
                                            "phone_number" => $phone_number,
                                            "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                            "lat" => !empty($addresss) ? !empty($addresss[0]['lat']) ? $addresss[0]['lat'] : 0 : 0,
                                            "lng" => !empty($addresss) ? !empty($addresss[0]['lng']) ? $addresss[0]['lng'] : 0 : 0,
                                            "timing" => $timings,
                                            "total_points_of_shop" => $total_rate,
                                            "is_evaluated" => !empty($evaluation_feedback) ? true : false,
                                            "is_bookmark" => !empty($faq_info) ? true : false),
                                        'images' => $invoice,
                                        'invoice' => $evaluation_invoice_url,
                                        'comments' => $comments,
                                        'rating' => $ratings,
                                        'status' => $status,
                                        'user_details' => $userdetails,
                                        'reason_rejection' => $reason_rejection,
                                        'admin_comment' => $admin_comment,
										'likes'=>$likes,
										'dislikes' => $dislikes,
										'like_by_user'=>false,
										'dislike_by_user'=>false,
                                        'evaluation_date' => strtotime($evaluation_date) * 1000);
										
                                }
                            }
                        }
                    }

                    if ($tot_ev_points != 0) {

                        if (!empty($evaluation_details)) {
                            foreach ($evaluation_details as $akey => $aval) {
                                //var_dump($aval['shop']['total_points_of_shop']); exit;
                                $res_points = $aval['shop']['total_points_of_shop'];
                                $points_percentage = ( $res_points * 25 ) / 100;
                                $rate = ( $points_percentage * 5 ) / 100;
                                $evaluation_details[$akey]['shop']['points_percentage'] = $points_percentage;
                                $evaluation_details[$akey]['shop']['rate_value_with_decimal'] = $rate;
                                $evaluation_details[$akey]['shop']['rate'] = number_format((float) $rate, 1, '.', '');
                            }
                        }
                    }

                    $this->error = "SFD";
                    $response = $evaluation_details;
                } else {
                    $this->error = "NRF";
                }
            } else {
                $this->error = "PIM";
            }

            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            } else {
                
            }
            $this->data = $response;
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }
    }

}

?>
