<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Evaluationfeedback;

class WSFeaturedCommentController extends WSBaseController {
	
	/**
	* @Route("/ws/featured_comment_list/{param}",defaults = {"param"=""},requirements={"param"=".+"})
	*
	*/
	public function featured_comment_listAction($param)
	{
		try{
			$this->title = "Featured Comment";
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array(),
				),
			);
			if($this->validateData($param)){
				$response = array();
				$data = $all_featured_comments = '';
				
				$all_featured_comments = $this->getDoctrine()
						->getManager()
						->getRepository('AdminBundle:Evaluationfeedback')
						->findBy(array('is_featured' => 'yes', 'is_deleted' => 0 ,'show_featured' => 'active'), array('created_datetime' => 'desc'));
				
				if(!empty($all_featured_comments)){

					foreach(array_slice($all_featured_comments, 0) as $lkey => $lval){
						
						$user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->find($lval->getUser_id());
						
						$user_image_id =  $username = '';
						if(!empty($user)){
							$user_image_id = $user->getUser_image();
							$username = $user->getUser_bio() ;
							$data[] = array(
									"evaluation_feedback_id" => $lval->getEvaluation_feedback_id(),
									"comment" => $lval->getComments(),
									"user_name" => $username,
									"user_img" => $this->getimage($user_image_id),
							);  
						}
						
						
					}
				}
				
				if (!empty($data)) {
					$response = $data;
					$this->error = "SFD";
				}
				if (empty($response)) {
					$response = false;
					$this->error = "NRF";
				}
				
				$this->data = $response;
				return $this->responseAction();
			}
		} catch (\Exception $e) {
			$this->error = "SFND";
			$this->data = false;
			return $this->responseAction();
		}
	}
}
?>