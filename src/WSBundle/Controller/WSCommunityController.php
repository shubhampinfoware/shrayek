<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Communitychat;

class WSCommunityController extends WSBaseController {

    /**
     * @Route("/ws/add_community_question/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     *
     */
    public function add_community_questionAction($param) {
        try {
            $this->title = "Ask Question";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {

                $response = true;

                $community_chat = new Communitychat();
                $community_chat->setCommunity_chat($param->question);
                $community_chat->setParent_id($param->parent_id);
                $community_chat->setCreated_datetime(date('Y-m-d H:i:s'));
                $community_chat->setUser_id($param->user_id);
                $community_chat->setIs_deleted(0);

                $em = $this->getDoctrine()->getManager();
                $em->persist($community_chat);
                $em->flush();

                $community_chat_id = $community_chat->getCommunity_chat_id();
                if (isset($community_chat_id) && $community_chat_id != '') {
                    $response = $community_chat_id;

                    #loyaltyPoints changes
                    if(!empty($community_chat->getCommunity_chat_id())){
                        if(!empty($param->parent_id)){

                            $activity_type = 'answer_question';
                            $points_added = $this->addCompetitionPointsToUser($param->user_id,$activity_type,0,$community_chat->getCommunity_chat_id(),'community_chat');                            

                        }                
                    }
                    #loyaltyPoints changes

                    $this->error = "SFD";
                }
                if (empty($response)) {
                    $response = false;
                    $this->error = "NRF";
                }
                $this->data = $response;
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = False;
            }
            return $this->responseAction();
        } catch (\Exception $e) {
            $this->error = "SFND";
            $this->data = false;
            return $this->responseAction();
        }
    }

    /**
     * @Route("/ws/community_question_list/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     *
     */
    public function community_question_listAction($param) {
        // pass parent_id = 0 to list all questions
       // try {
            $this->title = "Community Question List";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );
            if ($this->validateData($param)) {
                $response = array();
                $data = $all_questions = '';

                $parent_id = $param->parent_id;
                if (!isset($parent_id) or $parent_id == '') {
                    $parent_id = 0;
                }

                $all_questions = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Communitychat')
                        ->findBy(array('is_deleted' => 0, 'parent_id' => $parent_id) , array("created_datetime"=>'DESC'));

                if (!empty($all_questions)) {

                    foreach (array_slice($all_questions, 0) as $lkey => $lval) {
                        if ($lval->getUser_id() != 0) {
                            $user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->find($lval->getUser_id());

                            $user_image_id = $username = $user_image_url= '';
                            if (!empty($user)) {
                                $user_image_id = $user->getUser_image();
                                $username = $user->getUser_bio();
                                $user_image_url = $user->getImage_url();
                            }

                            $data[] = array(
                                "community_chat_id" => $lval->getCommunity_chat_id(),
                                "question" => $lval->getCommunity_chat(),
                                "parent_id" => $lval->getParent_id(),
                                "user_id" => $lval->getUser_id(),
                                "user_name" => $username,
                                "user_img" => !empty($user_image_id)?$this->getimage($user_image_id):$user_image_url,
                                "created_datetime" => (strtotime($lval->getCreated_datetime()) * 1000),
                            );
                        }
                    }
                }

                if (!empty($data)) {
                    $response = $data;
                    $this->error = "SFD";
                }
                if (empty($response)) {
                    $response = false;
                    $this->error = "NRF";
                }

                $this->data = $response;
                return $this->responseAction();
            }
//        } catch (\Exception $e) {
//            $this->error = "SFND";
//            $this->data = false;
//            return $this->responseAction();
//        }
    }

}

?>