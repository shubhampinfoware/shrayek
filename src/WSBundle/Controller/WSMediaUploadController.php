<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Customermaster;
use AdminBundle\Entity\Customersettings;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;

class WSMediaUploadController extends WSBaseController {

    /**
     * @Route("/ws/image_upload")
     * @Template()
     */
    public function image_uploadAction() {
        // try {
        $this->title = "Image Upload";
        $em = $this->getDoctrine()->getManager();
        $response = false;
        $Config_live_site = $this->container->getParameter('live_path');
        $file_path = $this->container->getParameter('file_path');
        if (isset($_FILES['image']) && !empty($_FILES['image'])) {
            for ($i = 0; $i < count($_FILES['image']['name']); $i++) {
                $media_id = NULL;

                $file = $_FILES['image']['name'][$i];
                // only profile image is allowed
                $extension = strtolower(pathinfo($_FILES['image']['name'][$i], PATHINFO_EXTENSION));
                //file extension check
                if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
                    $tmpname = $_FILES['image']['tmp_name'][$i];
                    $path = $file_path . '/uploads/WSimages';
                    $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';

                    $media_id = $this->mediauploadAction($file, $tmpname, $path, $upload_dir, 1);
                    $media_url = $this->getimage($media_id);
                    //media upload check
                    if ($media_id != "FALSE") {
                        $response[] = array("media_id" => $media_id, "media_url" => $media_url);
                        $this->error = "SFD";
                    } else {
                        $this->error = "PIM";
                    }
                } else {
                    $this->error = "PIW";
                }
            }
        } else {
            $this->error = "PIM";
            $this->data = false;
            return $this->responseAction();
        }

        if (empty($response)) {
            $this->error = "NRF";
            $this->data = false;
        }
        $this->data = $response;
        return $this->responseAction();
//        } catch (\Exception $e) {
//            $this->error = "SFND";
//            $this->data = false;
//            return $this->responseAction();
//        }
    }

}

?>
