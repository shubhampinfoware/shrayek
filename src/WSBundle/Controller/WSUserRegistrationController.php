<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Membershipdetails;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Gcmuser;
use AdminBundle\Entity\Apnsuser;
use AdminBundle\Entity\Usersetting;
use AdminBundle\Entity\Apppushnotificationmaster;
use AdminBundle\Entity\Addressmaster;

class WSUserRegistrationController extends WSBaseController {

    /**
     * @Route("/ws/userregistration/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function userRegistrationAction($param) {
        /* try
          { */
        $this->title = "User registration";
        $param = $this->requestAction($this->getRequest(), 0);
        // use to validate required param validation
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('contact_no', 'email', 'lang_id','date_of_birth', 'device_name','user_gender', 'app_id',  'device_id', 'device_token','password'),
            ),
        );
        $okflag = true;
        $firstname = $lastname = '' ;
        if ($this->error == 'token') {
            $this->error = "token";
            $okflag = false;
        }
        $msg_sent_error = "false";
        $location = NULL;
        $em = $this->getDoctrine()->getManager();
        $user_bio = '' ;
        $state_id = $country_id = $city_id = 0;
        if ($okflag == true) {
            if ($this->validateData($param)) {
                $contact_no = $param->contact_no;
                $email = $param->email;
                $lang_id = $param->lang_id;
                $date_of_birth = $param->date_of_birth/1000;
                $device_name = strtoupper($param->device_name);
                $app_id = $param->app_id;
                $domain_id = $domain_code = 1;
				
				$refer_by = isset($param->refer_by) ? $this->keyDecryptionAction($param->refer_by) : "";

                $state_id = $country_id = $city_id = 0;
                $store_code = $store_code_shop_master_id = NULL;
                $firstname = $password = $email_id = $device_id = $device_token = "";

                $em = $this->getDoctrine()->getManager();

               $user_gender = 'male';
                if (isset($param->firstname)) {
                    $firstname = $param->firstname;
                }
                if (isset($param->lastname)) {
                    $lastname = $param->lastname;
                }
                if (isset($param->nick_name)) {
                    $user_bio = $param->nick_name;
                }
                
                if (isset($param->password)) {
                    $password = $param->password;
                }
                if (isset($param->email)) {
                    $email = $param->email;
                }
                if (isset($param->device_id)) {
                    $device_id = $param->device_id;
                }
                if (isset($param->device_token)) {
                    $device_token = $param->device_token;
                }
                if (isset($param->user_gender)) {
                    $user_gender = $param->user_gender;
                }

                
                   
                

                $user_id = 0 ;
                $conn = $em->getConnection();
                $st1 = $conn->prepare("SELECT * FROM user_master WHERE  user_bio = '" . $user_bio . "'  AND ( user_role_id='3' AND domain_id='" . $domain_code . "' AND is_deleted=0)");
				
                $st1->execute();
                $userMasternikname = $st1->fetch();
                if(!empty($userMasternikname)){
                    $user_id = $new_user_id = $userMasternikname['user_master_id'];
                    $this->error = "UNAE";   
                    $this->data = false;
                    return $this->responseAction();
                }
                
                $conn = $em->getConnection();
                $st = $conn->prepare("SELECT * FROM user_master WHERE  email = '" . $email . "'  AND ( user_role_id='3' AND domain_id='" . $domain_code . "' AND is_deleted=0)");
                $st->execute();
                $userMaster = $st->fetch();
                $date = date("Y-m-d H:i:s");
                $timestamp = date('Y-m-d H:i:s', strtotime($date));
                $address_list = array();
                //-------------Already Exist User -------------------------//
                if (!empty($userMaster)) {
                    $user_id = $new_user_id = $userMaster['user_master_id'];
                    $this->error = "ARE";   
                    $this->data = false;
                    return $this->responseAction();
                }
                else{
                //-----------New User Entry ------------------------------//               
                    $date = date("Y-m-d H:i:s");
                    $timestamp = date('Y-m-d H:i:s', strtotime($date));
                    $address_list = array();
                    //if customer not found
                    $user_master_info_id = new Usermaster();
                    $user_master_info_id->setUser_role_id(3);
                    $user_master_info_id->setUser_bio($user_bio);
                    $user_master_info_id->setUsername($email);
                    $user_master_info_id->setEmail($email);
                     $Config_live_site = $this->container->getParameter('live_path');
                $file_path = $this->container->getParameter('file_path');
                if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                    //for ($i = 0; $i < count($_FILES['image']['name']); $i++) {
                        $media_id = NULL;

                        $file = $_FILES['image']['name'];
                        // only profile image is allowed
                        $extension = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
                        //file extension check
                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
                            $tmpname = $_FILES['image']['tmp_name'];
                            $path = $file_path . '/uploads/WSimages';
                            $upload_dir = $this->container->getParameter('upload_dir') . '/uploads/WSimages/';

                            $media_id = $this->mediauploadAction($file, $tmpname, $path, $upload_dir, 1);
                           // var_dump($media_id); exit;
                            $media_url = $this->getimage($media_id);
                            //media upload check
                            if ($media_id != "FALSE") {
                                 $user_master_info_id->setUser_image($media_id);
                                //$response[] = array("media_id" => $media_id, "media_url" => $media_url);
                               
                            }  else{
                                    $user_master_info_id->setUser_image(0);
                               }
                        } 
                   // }
                }
                    
                    if (isset($param->address_master_id)) {
                        $user_master_info_id->setAddress_master_id($param->address_master_id);
                    }
                    else{
                         $user_master_info_id->setAddress_master_id(0);
                    }
                    $user_master_info_id->setDate_of_birth(date('Y-m-d',$date_of_birth));
                    $user_master_info_id->setUser_gender($user_gender);
                    $user_master_info_id->setImage_url('');
                    $user_master_info_id->setParent_user_id(0);
                    $user_master_info_id->setUser_mobile($contact_no);
                    $user_master_info_id->setUser_firstname($firstname);
                    $user_master_info_id->setUser_lastname($lastname);
                    $user_master_info_id->setLast_modified(date('Y-m-d H:i:s'));
                    $user_master_info_id->setLast_login(date('Y-m-d H:i:s'));
                    $user_master_info_id->setStatus('active');
                    $user_master_info_id->setUser_type('user');
                    $user_master_info_id->setDomain_id($domain_code);
                    $user_master_info_id->setCreated_by(0);
                    $user_master_info_id->setCreated_datetime(date("Y-m-d H:i:s"));
                    $user_master_info_id->setLast_modified(date("Y-m-d H:i:s"));
                    $user_master_info_id->setLast_login(NULL);
                    $user_master_info_id->setPassword(md5($password));
                    $user_master_info_id->setShow_password($password);
                    $user_master_info_id->setEmail($email);
					if($refer_by != ""){
						$user_master_info_id->setRefer_by($refer_by);
					}
                    $em->persist($user_master_info_id);
                    $em->flush();
                    $user_id = $user_master_info_id->getUser_master_id();
                    /*                                 * ***** for IOS apns_reg_id issue solution ****** */
                    if ($device_name == 'IPHONE' && strlen($device_token) == 64) {
                        if (isset($device_id) && isset($device_token)) {
                            $apns_user_del = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository("AdminBundle:Apnsuser")
                                    ->findOneBy(array("user_id" => $user_id, "domain_id" => $domain_code, "app_id" => $app_id, "device_id" => $device_id,"is_deleted"=>0));
                            if (!empty($apns_user_del)) {
                                $apns_user_del->setApns_regid($device_token);
                                $apns_user_del->setCreated_date(date('Y-m-d H:i:s'));
                                $apns_user_del->setUser_type('user');
                                $apns_user_del->setIs_deleted(0);
                                $em->flush();
                            } 
                            else {
                                
                                // remove user from this device ==================/
                                $apns_guest_user_del = $this->getDoctrine()
                                        ->getManager()
                                        ->getRepository("AdminBundle:Apnsuser")
                                        ->findBy(array("domain_id" => $domain_code, "app_id" => $app_id, "device_id" => $device_id,"is_deleted"=>0));
                                if (!empty($apns_guest_user_del)) {
                                    foreach ($apns_guest_user_del as $ggkey => $ggval) {
                                        $ggval->setIs_deleted(1);
                                        $em->flush();
                                    }
                                }
                                //==new gcm entry -----
                                $apns_user = new Apnsuser();
                                $apns_user->setApns_regid($device_token);
                                $apns_user->setUser_id($user_id);
                                $apns_user->setUser_type('user');
                                $apns_user->setDevice_id($device_id);
                                $apns_user->setApp_id($app_id);
                                $apns_user->setDomain_id($domain_id);
                                $apns_user->setName($name . " / " . $contact_no);
                                $apns_user->setBadge(0);
                                $apns_user->setCreated_date(date("Y-m-d H:i:s"));
                                $apns_user->setIs_deleted(0);
                                $em->persist($apns_user);
                                $em->flush();
                            }
                        }
                    }
                    if ($device_name == 'ANDROID') {
                        $gcm_user_del = $this->getDoctrine()
                                ->getManager()
                                ->getRepository("AdminBundle:Gcmuser")
                                ->findOneBy(array("user_id" => $user_id, "domain_id" => $domain_code, "app_id" => $app_id, "device_id" => $device_id,"is_deleted"=>0));

                        if (!empty($gcm_user_del)) {
                            if ($device_token != '' && $device_token != NULL && !empty($device_token)) {
                                $gcm_user_del->setGcm_regid($device_token);
                            }
                            $gcm_user_del->setCreated_date(date('Y-m-d H:i:s'));
                            $gcm_user_del->setUser_type('user');
                            $gcm_user_del->setIs_deleted(0);
                            $em->persist($gcm_user_del);
                            $em->flush();
                        }
                        else {

                            // remove user from this device ==================/
                            $gcm_guest_user_del = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository("AdminBundle:Gcmuser")
                                    ->findBy(array("domain_id" => $domain_code, "app_id" => $app_id, "device_id" => $device_id,"is_deleted"=>0));
                            if (!empty($gcm_guest_user_del)) {
                                foreach ($gcm_guest_user_del as $ggkey => $ggval) {
                                    $ggval->setIs_deleted(1);
                                    $em->flush();
                                }
                            }
                            //==new gcm entry -----
                            $gcm_user = new Gcmuser();
                            $gcm_user->setGcm_regid($device_token);
                            $gcm_user->setUser_id($user_id);
                            $gcm_user->setUser_type('user');
                            $gcm_user->setDevice_id($device_id);
                            $gcm_user->setApp_id($app_id);
                            $gcm_user->setDomain_id($domain_id);
                            $gcm_user->setName($firstname . "  " . $lastname);
                            $gcm_user->setCreated_date(date("Y-m-d H:i:s"));
                            $gcm_user->setIs_deleted(0);
                            $em->persist($gcm_user);
                            $em->flush();
                        }
                    }
                    
                   $latest_address_info= $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted"=>0,"owner_id"=>$user_master_info_id->getUser_master_id(),"base_address_type"=>'primary')); 
                $address_list = NULL ;        
                if(!empty($latest_address_info)  && $latest_address_info != NULL){
                    $address_list = $this->getaddressAction($latest_address_info->getAddress_master_id(),$language_id);
                }
                if (!empty($address_list)) {
                    $userAddress = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Addressmaster")->findOneBy( array("address_master_id" => $user_master_info_id->getAddress_master_id(),"is_deleted" => 0) );
                } 

                    #addtion Loyalty Points
                    $points_added = $this->addCompetitionPointsToUser($user_master_info_id->getUser_master_id(),'registration',0,$user_master_info_id->getUser_master_id(),'user_master');
                    #addtion Loyalty Points done
					
					#refer a friend
                    if($refer_by != ""){
						// first refer by 
						// second new user
						$points_added = $this->addCompetitionPointsToUser($refer_by,'refer_friend',0,$user_master_info_id->getUser_master_id(),'user_master');
					}
					#end:refer a friend

					$response = array(
                         "usermaster_id" => $user_master_info_id->getUser_master_id(),
                    "nick_name" => $user_master_info_id->getUser_bio(),
                    "user_firstname" => $user_master_info_id->getUser_firstname(),
                    "user_lastname" => $user_master_info_id->getUser_lastname(),
                    "user_mobile" => $user_master_info_id->getUser_mobile(),
                    "user_image" => $this->getimage($user_master_info_id->getUser_image()),
                    "role_id" => $user_master_info_id->getUser_role_id(),
                    "user_emailid" => $user_master_info_id->getEmail(),
                    "date_of_birth" => strtotime($user_master_info_id->getDate_of_birth())*1000,
                   // "user_phoneno" => $this->keyDecryptionAction($userMaster->getUser_mobile()),
                    "username" => $user_master_info_id->getUsername(),
                    "language" => $user_master_info_id->getCurrent_lang_id(),
                    "domain_id" => $user_master_info_id->getDomain_id(),
                    "address" => $address_list
                       
                    );

                    $this->error = "SFD";
                }
                
               
                
               
            } else {
                $this->error = "PIM";
            }
        }

        if (empty($response)) {
            $response = false;
        }

        $this->data = $response;
        return $this->responseAction();
        /* }
          catch(\Exception $e)
          {
          $this->error = "SFND" ;
          $this->data = false ;
          return $this->responseAction() ;
          } */
    }

}

?>
