<?php
namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AdminBundle\Entity\Siteprivacy ;

class WSPrivacyController extends WSBaseController{
    /**
     * @Route("/ws/privacypolicy/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function privacypolicyAction($param){
		try{
			$response = false ;
			$faq_arr = [] ;
			$this->title = "Privacy Policy" ;
			//$param = $this->requestAction($this->getRequest()) ;
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('language_id'),
				),
			) ;
            		
			if($this->validateData($param)){
				//$faq_type_id = $param->faq_type_id ;
			    $language_id = $param->language_id ;
				$em = $this->getDoctrine()->getManager();
				$conn = $em->getConnection();
				$faq_info = $em->getRepository('AdminBundle:Siteprivacy')
					->findOneBy(array(
						'language_id'=>$language_id,
						'is_deleted'=>0
					)
				);

				if(!empty($faq_info)){
					
					$response = $faq_info->getdescription();
					
					$this->error = "SFD" ;
				}
				else{
					$this->error = "NRF";
				}
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response)){
				$response = false ;
				//$this->error = "NRF";
			}
			$this->data = $response ;
			return $this->responseAction() ;
		}
		catch(\Exception $e){
			$this->error = "SFND".$e ;
			$this->data = false ;
			return $this->responseAction() ;
		}
	}
	 /**
     * @Route("/ws/faqtypelist/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function faqtypelistAction($param){
		try{
			$response = false ;
			$faq_arr = [] ;
			$this->title = "FAQ list" ;
			//$param = $this->requestAction($this->getRequest()) ;
			$param = $this->requestAction($this->getRequest(),0);
			$this->validateRule = array(
				array(
					'rule'=>'NOTNULL',
					'field'=>array('lang_id'),
				),
			) ;
            		
			if($this->validateData($param)){
				
			    $language_id = $param->lang_id ;
				
				$em = $this->getDoctrine()->getManager();
				$conn = $em->getConnection();
				$faq_info = $em->getRepository('AdminBundle:Faqtype')
					->findBy(array(
						'language_id'=>$language_id,
						'status'=>'active',
						'is_deleted'=>0
					)
				);

				if(!empty($faq_info)){
					
					foreach($faq_info as $fkey=>$fval){
						$faq_arr[] = array(
							"faq_type_id"=>$fval->getMain_faq_type_id(),
							"faq_type_name"=>$fval->getFaq_type_name(),
							
						 );
					}
					$response = $faq_arr;
					
					$this->error = "SFD" ;
				}
				else{
					$this->error = "NRF";
				}
			}else{
				$this->error = "PIM" ;
			}
			if(empty($response)){
				$response = false ;
				$this->error = "NRF";
			}
			$this->data = $response ;
			return $this->responseAction() ;
		}
		catch(\Exception $e){
			$this->error = "SFND" ;
			$this->data = false ;
			return $this->responseAction() ;
		}
	}
}
?>