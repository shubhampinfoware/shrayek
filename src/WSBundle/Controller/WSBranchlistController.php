<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use AdminBundle\Entity\Productmaster;
use AdminBundle\Entity\Votemaster;
use AdminBundle\Entity\Attributecategoryrelation;

class WSBranchlistController extends WSBaseController {

    /**
     * @Route("/ws/branch_list/{param}",defaults =
      {"param"=""},requirements={"param"=".+"})
     *
     */
    public function branch_listAction($param) {
        /* try{ */
        $this->title = "Branch List";
        $param = $this->requestAction($this->getRequest(), 0);
        $this->validateRule = array(
            array(
                'rule' => 'NOTNULL',
                'field' => array('restaurant_id'),
            ),
        );
        if ($this->validateData($param)) {
            $language_id = 1;
            if (!empty($param->language_id))
                $language_id = $param->language_id;
			
            $restaurant_id = $param->restaurant_id;
            $response = array();
			
			$sql_brach_exist = "SELECT branch.*,address_master.address_name, address_master.owner_id ,address_master.lat ,address_master.lng FROM branch_master branch 
			LEFT JOIN address_master ON branch.branch_address_id = address_master.address_master_id and address_master.language_id = '$language_id'  
			WHERE branch.is_deleted = 0 AND branch.main_restaurant_id='" . $restaurant_id . "' and branch.language_id = '$language_id' order by main_branch_flag DESC";
		
			$con = $this->getDoctrine()->getManager()->getConnection();
			$stmt = $con->prepare($sql_brach_exist);
			$stmt->execute();
			$branch_data = $stmt->fetchAll();
			
			$tot_ev_points = 0;
			
			if(empty($branch_data)){
				$branch_data = null;
			}else{
				
				foreach($branch_data as $key=>$value){

				   $restaurant_total_evpointsQuery_branch = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '" . $restaurant_id. "' and  main_branch_id = '".$value['main_branch_master_id']."' and status='approved') as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level +evaluation_feedback.delivery_speed_level + evaluation_feedback.packaging_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.restaurant_id = " . $restaurant_id." and evaluation_feedback.main_branch_id = '".$value['main_branch_master_id']."'";
					$evpoints_branch = $this->firequery($restaurant_total_evpointsQuery_branch);
					
					$total_rate_branch = !empty($evpoints_branch) ? $evpoints_branch[0]['total_rate'] : 0;
					$evpoints_branch = !empty($evpoints_branch) ? $evpoints_branch[0]['Ev_points'] : 0;
					$tot_ev_points = $tot_ev_points + $evpoints_branch;                        
					$rating_point_branch = !empty($total_rate_branch) ? $evpoints_branch / ($total_rate_branch * 5) : 0 ;
					$rating_point_percentage_branch = 0 ;
					
					if($rating_point_branch > 0 ){
						$rating_point_percentage_branch = $rating_point_branch * 20 ;
					}
					
					
					$branch_data[$key]['points'] = $total_rate_branch;
					$branch_data[$key]['mobile_no'] = !empty($value['mobile_no']) ? $value['mobile_no'] : '--';
					$branch_data[$key]['rating'] = !empty($total_rate_branch) ? $evpoints_branch / ($total_rate_branch * 5) : 0;
					$branch_data[$key]['rating_point_percentage'] = $rating_point_percentage_branch;
				
					$branch_data[$key]["address"] = !empty($value['address_name']) ? $value['address_name'] : '--' ;
					$branch_data[$key]["lat"] = !empty($value['lat']) ? $value['lat'] : 0 ;
					$branch_data[$key]["lng"] = !empty($value['lng']) ? $value['lng'] : 0 ;
					
					#get Gallery of branch
					$gallery_branch = $this->getbranchcoverimage($value['main_branch_master_id']);
					$branch_data[$key]['shop_gallery'] = !empty($gallery_branch) ? $gallery_branch : null;
					#get Gallery of branch done
							
				}
			}
					
			if(!empty($branch_data)){
				$response = $branch_data;
				$this->error = "SFD";
			}else{
				$this->error = "NRF";
			}
			
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            }
            $this->data = $response;
        } else {
            $this->error = "PIM";
        }
        if (empty($response)) {
            $response = False;
        }
        return $this->responseAction();
        /* }
          catch (\Exception $e)
          {
          $this->error = "SFND";
          $this->data = false;
          return $this->responseAction();
          } */
    }

}

?>