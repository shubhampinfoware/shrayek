<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Organizationmaster;
use AdminBundle\Entity\Shopservicescategoryrelation;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Organizationservicearea;
use AdminBundle\Entity\Areamaster;
use AdminBundle\Entity\Shoptransactionhistory;

class WSRestaurantlistController extends WSBaseController {

    /**
     * @Route("/ws/restaurant_list/{param}",defaults = {"param"=""},requirements={"param"=".+"})
     * @Template()
     */
    public function restaurant_listAction($param) {

      //  try {
            $this->title = "Restaurant List";
            $param = $this->requestAction($this->getRequest(), 0);
            $this->validateRule = array(
                array(
                    'rule' => 'NOTNULL',
                    'field' => array(),
                ),
            );

            if ($this->validateData($param)) {

                $category_ids = !empty($param->category_ids) ? $param->category_ids : 0;
                $food_type_ids = !empty($param->food_type_ids) ? $param->food_type_ids : 0;

                $shop_id = !empty($param->shop_id) ? $param->shop_id : 0;
                $only_opening_soon = !empty($param->only_opening_soon) ? $param->only_opening_soon : '';
                $keyword = !empty($param->keyword) ? $param->keyword : '';
                $user_id = !empty($param->user_id) ? $param->user_id : '';
                $con = '';
                $con1 = '';

                $con2 = '';


                $lang_id = 1;
                if (isset($param->language_id)) {
                    $lang_id = $param->language_id;
                }
                $con .= ' and rm.language_id =' . $lang_id;
                $only_opening_soon = "";
				$res_status = " and rm.status='active'";
                if (isset($param->only_opening_soon)) {

                    if ($param->only_opening_soon == 'true')
                        $status = 'open_soon';
                    else
                        $status = 'active';
                    $res_status = " and rm.status='" . $status . "'";
                }
				$con .= $res_status;
                $food_type_ids = json_decode($food_type_ids);
                if (!empty($food_type_ids) && is_array($food_type_ids)) {
                    $food_type_ids = implode(',', $food_type_ids);
                    if (!empty($food_type_ids)) {

                        $con1 .= ' and rfr.main_foodtype_id in (' . $food_type_ids . ')';
                    }
                }

                if (!empty($shop_id)) {
                    $con .= ' and rm.main_restaurant_id =' . $shop_id;
                }

                if (!empty($keyword)) {
                   // $con .= " and (rm.restaurant_name LIKE '%$keyword%' or rm.description LIKE '%$keyword%' or cm.category_name LIKE '%$keyword%' or ftm.food_type_name LIKE '%$keyword%' )";
                    $con .= " and (rm.restaurant_name LIKE '%$keyword%' )";
                }




                $category_ids = json_decode($category_ids);
                if (!empty($category_ids) && is_array($category_ids)) {
                    $category_ids = implode(',', $category_ids);
                    if (!empty($category_ids)) {

                        $con2 .= ' and cm.main_category_id in (' . $category_ids . ')';
                    }
                }



                $em = $this->getDoctrine()->getManager();
                $conn = $em->getConnection();
                

                $st = $conn->prepare("SELECT rm.*,ftm.main_food_type_id,ftm.food_type_name,ftm.food_type_image_id,
							cm.main_category_id,cm.category_name,cm.category_image_id
							from restaurant_master as rm
							left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
							left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
							left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
							where rm.is_deleted=0 $con $con1 $con2 and rm.language_id=$lang_id and rm.is_deleted=0 and rm.status != 'inactive' and rfr.is_deleted = 0 group by rm.main_restaurant_id ORDER BY rm.restaurant_name ASC,cm.sort_order DESC");




                $st->execute();
                $shopList = $st->fetchAll();
                //echo '<pre>';print_r($shopList);exit;
                $tot_ev_points=0;
                if (!empty($shopList)) {
                    foreach ($shopList as $shopMaster) {
                        // get catgory
                        $query = "select cm.* from category_master as cm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_caetgory_id=cm.main_category_id  where cm.is_deleted=0 and cm.language_id='" . $lang_id . "' $con2 group by cm.main_category_id order by sort_order DESC";
                        $cat = $this->firequery($query);
                        $category = null;
                        if (!empty($cat)) {
                            foreach ($cat as $ca) {


                                // get cuisines
                                $query = "select * from food_type_master as ftm LEFT JOIN restaurant_foodtype_relation as rfr ON  rfr.main_foodtype_id=ftm.main_food_type_id where rfr.restaurant_id=" . $shopMaster['main_restaurant_id'] . " and rfr.is_deleted=0 and ftm.is_deleted=0 and ftm.language_id=$lang_id and rfr.main_caetgory_id=" . $ca['main_category_id'];

                                $cuisines = null;


                                $cuisine = $this->firequery($query);


                                if (!empty($cuisine)) {

                                    foreach ($cuisine as $val) {


                                        $cuisines[] = array(
                                            "food_type_id" => $val['main_food_type_id'],
                                            "food_type_name" => $val['food_type_name'],
                                            "food_type_image" => $this->getimage($val['food_type_image_id'])
                                        );
                                    }
                                    $category[] = array("category_id" => $ca['main_category_id'],
                                        "category_name" => $ca['category_name'],
                                        "category_logo" => $this->getimage($ca['category_image_id']), "food_type" => $cuisines);
                                }
                            }
                        }


//                        if (!empty($category)) {
                            $query1 = "SELECT * from address_master where main_address_id='" . $shopMaster['address_id'] . "' and language_id=" . $lang_id . " and is_deleted=0";
                            $addresss = $this->firequery($query1);

                            $gallery = $this->getshopcoverimage($shopMaster['main_restaurant_id']);
                            $evaluation_feedback=null;
                            $faq_info=null;
                        if(!empty($user_id)){
                             $evaluation_feedback = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Evaluationfeedback')
                                    ->findOneBy(array("restaurant_id"=>$shopMaster['main_restaurant_id'], "user_id"=>$user_id, "is_deleted"=>0));
                             $faq_info =  $this->getDoctrine()->getManager()->getRepository('AdminBundle:Bookmarkmaster')
					->findOneBy(array(
						'shop_id'=>$shopMaster['main_restaurant_id'],
                                                'user_id'=>$user_id,
                                                'type'=>'add',
						'is_deleted'=>0
					)
				);
                        } 
                        $restaurant_total_evpointsQuery = "SELECT (SELECT count(*) FROM evaluation_feedback WHERE restaurant_id = '".$shopMaster['main_restaurant_id']."'  and status='approved') as total_rate, sum( evaluation_feedback.service_level + evaluation_feedback.dining_level + evaluation_feedback.atmosphere_level + evaluation_feedback.price_level + evaluation_feedback.clean_level ) as Ev_points FROM `evaluation_feedback` JOIN restaurant_master ON evaluation_feedback.restaurant_id = restaurant_master.main_restaurant_id WHERE restaurant_master.is_deleted = 0 and evaluation_feedback.`is_deleted` = 0 and evaluation_feedback.status ='approved' and restaurant_master.language_id = 1 and evaluation_feedback.status ='approved' and evaluation_feedback.restaurant_id = " . $shopMaster['main_restaurant_id'];
                                $evpoints = $this->firequery($restaurant_total_evpointsQuery);
                                $total_rate = !empty($evpoints) ? $evpoints[0]['total_rate'] : 0;
                                $evpoints = !empty($evpoints) ? $evpoints[0]['Ev_points'] : 0;
                                $tot_ev_points = $tot_ev_points + $evpoints;
                                $where='';
                                if(!empty($shopMaster['main_restaurant_id'])){
                                    $where .= ' and restaurant_id='.$shopMaster['main_restaurant_id'];
                                }
                                    $query= "select * from restaurant_menu where is_deleted = 0 $where";
                                    $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
                                    $em->execute();
                                    $all_category = $em->fetchAll();
                                    $menu_image=null;
                                    if(count($all_category) > 0 )
                                    {
                                            foreach($all_category as $lval)
                                            {
                                                    $menu_image[]= array("media_id"=> $lval['media_id'],
                                                    "menu_image"=>$this->getimage($lval['media_id']),

                                                    );

                                            }

                                    }
									
							$response[] = array(
                                "shop_id" => $shopMaster['main_restaurant_id'],
                                "shop_name" => $shopMaster['restaurant_name'],
                                "shop_short_description" => $shopMaster['description'],
                                "shop_long_description" => $shopMaster['description'],
                                "shop_logo" => $this->getimage($shopMaster['logo_id']),
                                "opening_date" => strtotime($shopMaster['opening_date'])*1000,
                                "shop_menu" => $menu_image,
                                "shop_gallery" => !empty($gallery) ? $gallery : null,
								"total_rating_count" => $total_rate,
                                "rating" =>  !empty($total_rate)?$evpoints/($total_rate*5):0,
                                "cuisines" => $category,
                                "phone_number" => $shopMaster['phone_number'],
                                "additional_info" => $shopMaster['additional_info'],
                                "address" => !empty($addresss) ? $addresss[0]['address_name'] : 0,
                                "lat" => !empty($addresss) ? !empty($addresss[0]['lat'])?$addresss[0]['lat']:0 : 0,
                                "lng" => !empty($addresss) ? !empty($addresss[0]['lng'])?$addresss[0]['lng']:0 : 0,
                                "timing" => $shopMaster['timings'],
                                "is_evaluated" => !empty($evaluation_feedback)?true:false,
                                "is_bookmark" => !empty($faq_info)?true:false
                            );
//                        } else {
//                            $this->error = "NRF";
//                        }
                    }
                }
                if ($tot_ev_points != 0) {

                        if (!empty($response)) {
                            foreach ($response as $akey => $aval) {
                                //var_dump($aval['shop']['total_points_of_shop']); exit;
                                $res_points = $aval['total_rating_count'];
                                $points_percentage = ( $res_points * 25 ) / 100;
                                $rate = ( $points_percentage * 5 ) / 100;
                                $response[$akey]['points_percentage'] = $points_percentage;
                                $response[$akey]['rate_value_with_decimal'] = $rate;
                                $response[$akey]['rate'] = number_format((float) $rate, 1, '.', '');
                            }
                        }
                    }
            } else {
                $this->error = "PIM";
            }
            if (empty($response)) {
                $response = false;
                $this->error = "NRF";
            } else {
                $this->error = "SFD";
            }

            $this->data = $response;
            return $this->responseAction();
     /*   } catch (\Exception $e) {
            $this->error = "SFND " . $e;
            $this->data = false;
            return $this->responseAction();
        }*/
    }

}

?>
