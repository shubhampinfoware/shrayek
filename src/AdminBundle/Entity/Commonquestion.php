<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="common_question")
*/
class Commonquestion
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $common_question_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $question="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $answer="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_common_question_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $sort_order=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted=0;

	public function getCommon_question_id()
	{
		return $this->common_question_id;
	}

	public function getQuestion()
	{
		return $this->question;
	}
	public function setQuestion($question)
	{
		$this->question = $question;
	}

	public function getAnswer()
	{
		return $this->answer;
	}
	public function setAnswer($answer)
	{
		$this->answer = $answer;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_common_question_id()
	{
		return $this->main_common_question_id;
	}
	public function setMain_common_question_id($main_common_question_id)
	{
		$this->main_common_question_id = $main_common_question_id;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getSort_order()
	{
		return $this->sort_order;
	}
	public function setSort_order($sort_order)
	{
		$this->sort_order = $sort_order;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}