<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="product_master")
*/
class Productmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $product_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $product_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $product_description="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_product_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $product_image_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $product_status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getProduct_master_id()
	{
		return $this->product_master_id;
	}

	public function getProduct_name()
	{
		return $this->product_name;
	}
	public function setProduct_name($product_name)
	{
		$this->product_name = $product_name;
	}

	public function getProduct_description()
	{
		return $this->product_description;
	}
	public function setProduct_description($product_description)
	{
		$this->product_description = $product_description;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_product_id()
	{
		return $this->main_product_id;
	}
	public function setMain_product_id($main_product_id)
	{
		$this->main_product_id = $main_product_id;
	}

	public function getProduct_image_id()
	{
		return $this->product_image_id;
	}
	public function setProduct_image_id($product_image_id)
	{
		$this->product_image_id = $product_image_id;
	}

	public function getProduct_status()
	{
		return $this->product_status;
	}
	public function setProduct_status($product_status)
	{
		$this->product_status = $product_status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}