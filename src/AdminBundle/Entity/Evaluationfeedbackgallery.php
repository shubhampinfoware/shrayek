<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="evaluation_feedback_gallery")
*/
class Evaluationfeedbackgallery
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $evaluation_feedback_gallery_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $evaluation_feedback_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_type_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getEvaluation_feedback_gallery_id()
	{
		return $this->evaluation_feedback_gallery_id;
	}

	public function getEvaluation_feedback_id()
	{
		return $this->evaluation_feedback_id;
	}
	public function setEvaluation_feedback_id($evaluation_feedback_id)
	{
		$this->evaluation_feedback_id = $evaluation_feedback_id;
	}

	public function getMedia_id()
	{
		return $this->media_id;
	}
	public function setMedia_id($media_id)
	{
		$this->media_id = $media_id;
	}

	public function getMedia_type_id()
	{
		return $this->media_type_id;
	}
	public function setMedia_type_id($media_type_id)
	{
		$this->media_type_id = $media_type_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}