<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="maincar")
*/
class Maincarmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $maincar_id;
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_name="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_description="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_specification="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_driving="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_interior="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $color_name="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $trim_name="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $engine_name="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $feature_name="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_year="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_image="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $maincar_logo="0";
	/**
	* @ORM\Column(type="integer")
	*/
	protected $cartype_name="";
	/**
	* @ORM\Column(type="integer")
	*/
	protected $car_name="";
	/**
	* @ORM\Column(type="integer")
	*/
	protected $maincar_main_id=0;
	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";
	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;
	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;
	

	public function getMaincar_id()
	{
		return $this->maincar_id;
	}
	public function getMaincar_name()
	{
		return $this->maincar_name;
	}
	public function setMaincar_name($maincar_name)
	{
		$this->maincar_name = $maincar_name;
	}
	public function getMaincar_description()
	{
		return $this->maincar_description;
	}
	public function setMaincar_description($maincar_description)
	{
		$this->maincar_description = $maincar_description;
	}
	public function getMaincar_specification()
	{
		return $this->maincar_specification;
	}
	public function setMaincar_specification($maincar_specification)
	{
		$this->maincar_specification = $maincar_specification;
	}

	public function getMaincar_driving()
	{
		return $this->maincar_driving;
	}
	public function setMaincar_driving($maincar_driving)
	{
		$this->maincar_driving = $maincar_driving;
	}
	public function getMaincar_interior()
	{
		return $this->maincar_interior;
	}
	public function setMaincar_interior($maincar_interior)
	{
		$this->maincar_interior = $maincar_interior;
	}
	public function getColor_name()
	{
		return $this->color_name;
	}
	public function setColor_name($color_name)
	{
		$this->color_name = $color_name;
	}
	public function getTrim_name()
	{
		return $this->trim_name;
	}
	public function setTrim_name($trim_name)
	{
		$this->trim_name = $trim_name;
	}
	public function getEngine_name()
	{
		return $this->engine_name;
	}
	public function setEngine_name($engine_name)
	{
		$this->engine_name = $engine_name;
	}

	public function getFeature_name()
	{
		return $this->feature_name;
	}
	public function setFeature_name($feature_name)
	{
		$this->feature_name = $feature_name;
	}
	public function getMaincar_year()
	{
		return $this->maincar_year;
	}
	public function setMaincar_year($maincar_year)
	{
		$this->maincar_year = $maincar_year;
	}

	public function getPrice()
	{
		return $this->price;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}
	public function getMaincar_image()
	{
		return $this->maincar_image;
	}
	public function setMaincar_image($maincar_image)
	{
		$this->maincar_image = $maincar_image;
	}
	
	public function getMaincar_Logo()
	{
		return $this->maincar_logo;
	}
	public function setMaincar_Logo($maincar_logo)
	{
		$this->maincar_logo = $maincar_logo;
	}
	public function getCartype_name()
	{
		return $this->cartype_name;
	}
	public function setCartype_name($cartype_name)
	{
		$this->cartype_name = $cartype_name;
	}
	public function getCar_name()
	{
		return $this->car_name;
	}
	public function setCar_name($car_name)
	{
		$this->car_name = $car_name;
	}
	public function getMaincar_main_id()
	{
		return $this->maincar_main_id;
	}
	public function setMaincar_main_id($maincar_main_id)
	{
		$this->maincar_main_id = $maincar_main_id;
	}
	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}
	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}
	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
	
	
	

}
