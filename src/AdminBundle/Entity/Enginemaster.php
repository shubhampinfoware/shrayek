<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="engine_master")
*/
class Enginemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $engine_master_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $brand_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $car_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $engine_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $engine_bhp="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $engine_mpg="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $price="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_engine_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getEngine_master_id()
	{
		return $this->engine_master_id;
	}

	public function getBrand_id()
	{
		return $this->brand_id;
	}
	public function setBrand_id($brand_id)
	{
		$this->brand_id = $brand_id;
	}

	public function getCar_id()
	{
		return $this->car_id;
	}
	public function setCar_id($car_id)
	{
		$this->car_id = $car_id;
	}

	public function getEngine_name()
	{
		return $this->engine_name;
	}
	public function setEngine_name($engine_name)
	{
		$this->engine_name = $engine_name;
	}

	public function getEngine_bhp()
	{
		return $this->engine_bhp;
	}
	public function setEngine_bhp($engine_bhp)
	{
		$this->engine_bhp = $engine_bhp;
	}

	public function getEngine_mpg()
	{
		return $this->engine_mpg;
	}
	public function setEngine_mpg($engine_mpg)
	{
		$this->engine_mpg = $engine_mpg;
	}

public function getPrice()
	{
		return $this->price;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}
	public function getMain_engine_id()
	{
		return $this->main_engine_id;
	}
	public function setMain_engine_id($main_engine_id)
	{
		$this->main_engine_id = $main_engine_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}