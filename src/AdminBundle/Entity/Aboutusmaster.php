<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="aboutus_master")
*/
class Aboutusmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $aboutus_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $content="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $lang_id=0;
	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_id=0;


	/**
	* @ORM\Column(type="string")
	*/
	protected $create_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getAboutus_master_id()
	{
		return $this->aboutus_master_id;
	}

	public function getContent()
	{
		return $this->content;
	}
	public function setContent($content)
	{
		$this->content = $content;
	}

	public function getLang_id()
	{
		return $this->lang_id;
	}
	public function setLang_id($lang_id)
	{
		$this->lang_id = $lang_id;
	}
	public function getMedia_id()
	{
		return $this->media_id;
	}
	public function setMedia_id($media_id)
	{
		$this->media_id = $media_id;
	}

	public function getCreate_date()
	{
		return $this->create_date;
	}
	public function setCreate_date($create_date)
	{
		$this->create_date = $create_date;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}