<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="cupcakesize_master")
*/
class Cupcakesizemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $cupcakesize_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cupcakesize_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cupcakesize_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cupcakesize_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cupcakesize_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getCupcakesize_id()
	{
		return $this->cupcakesize_id;
	}
	public function setCupcakesize_id($cupcakesize_id)
	{
		$this->cupcakesize_id = $cupcakesize_id;
	}

	public function getCupcakesize_name()
	{
		return $this->cupcakesize_name;
	}
	public function setCupcakesize_name($cupcakesize_name)
	{
		$this->cupcakesize_name = $cupcakesize_name;
	}

	public function getCupcakesize_image()
	{
		return $this->cupcakesize_image;
	}
	public function setCupcakesize_image($cupcakesize_image)
	{
		$this->cupcakesize_image = $cupcakesize_image;
	}

	public function getCupcakesize_price()
	{
		return $this->cupcakesize_price;
	}
	public function setCupcakesize_price($cupcakesize_price)
	{
		$this->cupcakesize_price = $cupcakesize_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCupcakesize_main_id()
	{
		return $this->cupcakesize_main_id;
	}
	public function setCupcakesize_main_id($cupcakesize_main_id)
	{
		$this->cupcakesize_main_id = $cupcakesize_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}