<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="category_master")
*/
class Categorymaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $category_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $parent_category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_image_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $sort_order=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCategory_master_id()
	{
		return $this->category_master_id;
	}

	public function getCategory_name()
	{
		return $this->category_name;
	}
	public function setCategory_name($category_name)
	{
		$this->category_name = $category_name;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_category_id()
	{
		return $this->main_category_id;
	}
	public function setMain_category_id($main_category_id)
	{
		$this->main_category_id = $main_category_id;
	}

	public function getParent_category_id()
	{
		return $this->parent_category_id;
	}
	public function setParent_category_id($parent_category_id)
	{
		$this->parent_category_id = $parent_category_id;
	}

	public function getCategory_image_id()
	{
		return $this->category_image_id;
	}
	public function setCategory_image_id($category_image_id)
	{
		$this->category_image_id = $category_image_id;
	}

	public function getCategory_status()
	{
		return $this->category_status;
	}
	public function setCategory_status($category_status)
	{
		$this->category_status = $category_status;
	}

	public function getSort_order()
	{
		return $this->sort_order;
	}
	public function setSort_order($sort_order)
	{
		$this->sort_order = $sort_order;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}