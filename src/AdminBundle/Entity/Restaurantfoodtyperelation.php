<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="restaurant_foodtype_relation")
*/
class Restaurantfoodtyperelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $restaurant_foodtype_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_foodtype_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_caetgory_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getRestaurant_foodtype_relation_id()
	{
		return $this->restaurant_foodtype_relation_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getMain_foodtype_id()
	{
		return $this->main_foodtype_id;
	}
	public function setMain_foodtype_id($main_foodtype_id)
	{
		$this->main_foodtype_id = $main_foodtype_id;
	}

	public function getMain_caetgory_id()
	{
		return $this->main_caetgory_id;
	}
	public function setMain_caetgory_id($main_caetgory_id)
	{
		$this->main_caetgory_id = $main_caetgory_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}