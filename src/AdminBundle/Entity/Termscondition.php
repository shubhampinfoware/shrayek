<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="terms_condition")
*/
class Termscondition
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $terms_condition_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $terms_condition="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_id=0;

	public function getTerms_condition_id()
	{
		return $this->terms_condition_id;
	}

	public function getTerms_condition()
	{
		return $this->terms_condition;
	}
	public function setTerms_condition($terms_condition)
	{
		$this->terms_condition = $terms_condition;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_id()
	{
		return $this->main_id;
	}
	public function setMain_id($main_id)
	{
		$this->main_id = $main_id;
	}
}