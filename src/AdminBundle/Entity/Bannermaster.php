<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="banner_master")
*/
class Bannermaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $banner_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $text="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $image_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $banner_main_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getBanner_master_id()
	{
		return $this->banner_master_id;
	}

	public function getText()
	{
		return $this->text;
	}
	public function setText($text)
	{
		$this->text = $text;
	}

	public function getImage_id()
	{
		return $this->image_id;
	}
	public function setImage_id($image_id)
	{
		$this->image_id = $image_id;
	}

	public function getBanner_main_id()
	{
		return $this->banner_main_id;
	}
	public function setBanner_main_id($banner_main_id)
	{
		$this->banner_main_id = $banner_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}