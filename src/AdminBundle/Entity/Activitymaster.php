<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="activity_master")
*/
class Activitymaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $activity_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $activity_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $activity_title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_activity_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $defaults_points=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $created_by=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getActivity_master_id()
	{
		return $this->activity_master_id;
	}

	public function getActivity_name()
	{
		return $this->activity_name;
	}
	public function setActivity_name($activity_name)
	{
		$this->activity_name = $activity_name;
	}

	public function getActivity_title()
	{
		return $this->activity_title;
	}
	public function setActivity_title($activity_title)
	{
		$this->activity_title = $activity_title;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_activity_id()
	{
		return $this->main_activity_id;
	}
	public function setMain_activity_id($main_activity_id)
	{
		$this->main_activity_id = $main_activity_id;
	}

	public function getDefaults_points()
	{
		return $this->defaults_points;
	}
	public function setDefaults_points($defaults_points)
	{
		$this->defaults_points = $defaults_points;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}