<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="word_dictionary")
*/
class Worddictionary
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $word_dictionary_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $word_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_word_dictionary_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getWord_dictionary_id()
	{
		return $this->word_dictionary_id;
	}

	public function getWord_name()
	{
		return $this->word_name;
	}
	public function setWord_name($word_name)
	{
		$this->word_name = $word_name;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_word_dictionary_id()
	{
		return $this->main_word_dictionary_id;
	}
	public function setMain_word_dictionary_id($main_word_dictionary_id)
	{
		$this->main_word_dictionary_id = $main_word_dictionary_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}