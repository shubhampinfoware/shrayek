<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_activity_relation")
*/
class Competitionactivityrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_activity_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $competition_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $activity_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $points=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_activity_relation_id()
	{
		return $this->competition_activity_relation_id;
	}

	public function getCompetition_id()
	{
		return $this->competition_id;
	}
	public function setCompetition_id($competition_id)
	{
		$this->competition_id = $competition_id;
	}

	public function getActivity_id()
	{
		return $this->activity_id;
	}
	public function setActivity_id($activity_id)
	{
		$this->activity_id = $activity_id;
	}

	public function getPoints()
	{
		return $this->points;
	}
	public function setPoints($points)
	{
		$this->points = $points;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}