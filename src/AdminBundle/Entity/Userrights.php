<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="user_rights")
*/
class Userrights
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $right_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $display_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $right_code="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $domain_id="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getRight_master_id()
	{
		return $this->right_master_id;
	}

	public function getDisplay_name()
	{
		return $this->display_name;
	}
	public function setDisplay_name($display_name)
	{
		$this->display_name = $display_name;
	}

	public function getRight_code()
	{
		return $this->right_code;
	}
	public function setRight_code($right_code)
	{
		$this->right_code = $right_code;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}