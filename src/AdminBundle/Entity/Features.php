<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="features")
*/
class Features
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $feature_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $feature_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $feature_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $feature_main_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $brand_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $car_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getFeature_id()
	{
		return $this->feature_id;
	}

	public function getFeature_name()
	{
		return $this->feature_name;
	}
	public function setFeature_name($feature_name)
	{
		$this->feature_name = $feature_name;
	}

	public function getFeature_description()
	{
		return $this->feature_description;
	}
	public function setFeature_description($feature_description)
	{
		$this->feature_description = $feature_description;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getFeature_main_id()
	{
		return $this->feature_main_id;
	}
	public function setFeature_main_id($feature_main_id)
	{
		$this->feature_main_id = $feature_main_id;
	}

	public function getBrand_id()
	{
		return $this->brand_id;
	}
	public function setBrand_id($brand_id)
	{
		$this->brand_id = $brand_id;
	}

	public function getCar_id()
	{
		return $this->car_id;
	}
	public function setCar_id($car_id)
	{
		$this->car_id = $car_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}