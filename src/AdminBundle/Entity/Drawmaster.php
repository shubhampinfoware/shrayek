<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="draw_master")
*/
class Drawmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $draw_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $draw_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $draw_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $draw_type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $start_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $end_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $qualification_number=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $qualification_status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $created_by=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $winner_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $winner_declare_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $relation_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_branch_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $foodtype_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getDraw_master_id()
	{
		return $this->draw_master_id;
	}

	public function getDraw_name()
	{
		return $this->draw_name;
	}
	public function setDraw_name($draw_name)
	{
		$this->draw_name = $draw_name;
	}

	public function getDraw_description()
	{
		return $this->draw_description;
	}
	public function setDraw_description($draw_description)
	{
		$this->draw_description = $draw_description;
	}

	public function getDraw_type()
	{
		return $this->draw_type;
	}
	public function setDraw_type($draw_type)
	{
		$this->draw_type = $draw_type;
	}

	public function getStart_date()
	{
		return $this->start_date;
	}
	public function setStart_date($start_date)
	{
		$this->start_date = $start_date;
	}

	public function getEnd_date()
	{
		return $this->end_date;
	}
	public function setEnd_date($end_date)
	{
		$this->end_date = $end_date;
	}

	public function getQualification_number()
	{
		return $this->qualification_number;
	}
	public function setQualification_number($qualification_number)
	{
		$this->qualification_number = $qualification_number;
	}

	public function getQualification_status()
	{
		return $this->qualification_status;
	}
	public function setQualification_status($qualification_status)
	{
		$this->qualification_status = $qualification_status;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getWinner_id()
	{
		return $this->winner_id;
	}
	public function setWinner_id($winner_id)
	{
		$this->winner_id = $winner_id;
	}

	public function getWinner_declare_date()
	{
		return $this->winner_declare_date;
	}
	public function setWinner_declare_date($winner_declare_date)
	{
		$this->winner_declare_date = $winner_declare_date;
	}

	public function getRelation_id()
	{
		return $this->relation_id;
	}
	public function setRelation_id($relation_id)
	{
		$this->relation_id = $relation_id;
	}

	public function getMain_branch_id()
	{
		return $this->main_branch_id;
	}
	public function setMain_branch_id($main_branch_id)
	{
		$this->main_branch_id = $main_branch_id;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getFoodtype_id()
	{
		return $this->foodtype_id;
	}
	public function setFoodtype_id($foodtype_id)
	{
		$this->foodtype_id = $foodtype_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}