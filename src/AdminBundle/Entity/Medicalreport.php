<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="medical_report")
*/
class Medicalreport
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $medical_report_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $domain_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $date_of_upload="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $report_title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $report_description="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_medical_report_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $langugage_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getMedical_report_id()
	{
		return $this->medical_report_id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getDate_of_upload()
	{
		return $this->date_of_upload;
	}
	public function setDate_of_upload($date_of_upload)
	{
		$this->date_of_upload = $date_of_upload;
	}

	public function getReport_title()
	{
		return $this->report_title;
	}
	public function setReport_title($report_title)
	{
		$this->report_title = $report_title;
	}

	public function getReport_description()
	{
		return $this->report_description;
	}
	public function setReport_description($report_description)
	{
		$this->report_description = $report_description;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getMain_medical_report_id()
	{
		return $this->main_medical_report_id;
	}
	public function setMain_medical_report_id($main_medical_report_id)
	{
		$this->main_medical_report_id = $main_medical_report_id;
	}

	public function getLangugage_id()
	{
		return $this->langugage_id;
	}
	public function setLangugage_id($langugage_id)
	{
		$this->langugage_id = $langugage_id;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}