<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="community_chat")
*/
class Communitychat
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $community_chat_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $community_chat="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $parent_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCommunity_chat_id()
	{
		return $this->community_chat_id;
	}

	public function getCommunity_chat()
	{
		return $this->community_chat;
	}
	public function setCommunity_chat($community_chat)
	{
		$this->community_chat = $community_chat;
	}

	public function getParent_id()
	{
		return $this->parent_id;
	}
	public function setParent_id($parent_id)
	{
		$this->parent_id = $parent_id;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}