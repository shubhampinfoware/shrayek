<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="imagetopdecoration_master")
*/
class Imagetopdecorationmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $imagetopdecoration_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $imagetopdecoration_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $imagetopdecoration_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $imagetopdecoration_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $imagetopdecoration_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getImagetopdecoration_id()
	{
		return $this->imagetopdecoration_id;
	}

	public function getImagetopdecoration_name()
	{
		return $this->imagetopdecoration_name;
	}
	public function setImagetopdecoration_name($imagetopdecoration_name)
	{
		$this->imagetopdecoration_name = $imagetopdecoration_name;
	}

	public function getImagetopdecoration_image()
	{
		return $this->imagetopdecoration_image;
	}
	public function setImagetopdecoration_image($imagetopdecoration_image)
	{
		$this->imagetopdecoration_image = $imagetopdecoration_image;
	}

	public function getImagetopdecoration_price()
	{
		return $this->imagetopdecoration_price;
	}
	public function setImagetopdecoration_price($imagetopdecoration_price)
	{
		$this->imagetopdecoration_price = $imagetopdecoration_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getImagetopdecoration_main_id()
	{
		return $this->imagetopdecoration_main_id;
	}
	public function setImagetopdecoration_main_id($imagetopdecoration_main_id)
	{
		$this->imagetopdecoration_main_id = $imagetopdecoration_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}