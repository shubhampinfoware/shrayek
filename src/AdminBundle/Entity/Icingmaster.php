<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="icing_master")
*/
class Icingmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $icing_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $icing_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $icing_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $icing_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $icing_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getIcing_id()
	{
		return $this->icing_id;
	}
	public function setIcing_id($icing_id)
	{
		$this->icing_id = $icing_id;
	}

	public function getIcing_name()
	{
		return $this->icing_name;
	}
	public function setIcing_name($icing_name)
	{
		$this->icing_name = $icing_name;
	}

	public function getIcing_image()
	{
		return $this->icing_image;
	}
	public function setIcing_image($icing_image)
	{
		$this->icing_image = $icing_image;
	}

	public function getIcing_price()
	{
		return $this->icing_price;
	}
	public function setIcing_price($icing_price)
	{
		$this->icing_price = $icing_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIcing_main_id()
	{
		return $this->icing_main_id;
	}
	public function setIcing_main_id($icing_main_id)
	{
		$this->icing_main_id = $icing_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}