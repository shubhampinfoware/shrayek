<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="attribute_master")
*/
class Attributemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $attribute_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $feature_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $attribute_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $attribute_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted=0;

	public function getAttribute_id()
	{
		return $this->attribute_id;
	}

	public function getFeature_name()
	{
		return $this->feature_name;
	}
	public function setFeature_name($feature_name)
	{
		$this->feature_name = $feature_name;
	}

	public function getAttribute_name()
	{
		return $this->attribute_name;
	}
	public function setAttribute_name($attribute_name)
	{
		$this->attribute_name = $attribute_name;
	}

	public function getPrice()
	{
		return $this->price;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getAttribute_main_id()
	{
		return $this->attribute_main_id;
	}
	public function setAttribute_main_id($attribute_main_id)
	{
		$this->attribute_main_id = $attribute_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}