<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="slider_master")
*/
class Slidermaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $slider_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $slider_image="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $slider_main_id="";


	public function getSlider_id()
	{
		return $this->slider_id;
	}
	public function setSlider_id($slider_id)
	{
		$this->slider_id = $slider_id;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getSlider_image()
	{
		return $this->slider_image;
	}
	public function setSlider_image($slider_image)
	{
		$this->slider_image = $slider_image;
	}


	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}


	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
	public function getSlider_main_id()
	{
		return $this->slider_main_id;
	}
	public function setSlider_main_id($slider_main_id)
	{
		$this->slider_main_id = $slider_main_id;
	}
}