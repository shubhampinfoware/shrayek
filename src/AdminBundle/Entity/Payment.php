<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="payment")
*/
class Payment
{
	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_name="";

	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $payment_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $payment_method="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getLanguage_name()
	{
		return $this->language_name;
	}
	public function setLanguage_name($language_name)
	{
		$this->language_name = $language_name;
	}

	public function getPayment_id()
	{
		return $this->payment_id;
	}

	public function getPayment_method()
	{
		return $this->payment_method;
	}
	public function setPayment_method($payment_method)
	{
		$this->payment_method = $payment_method;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}
}