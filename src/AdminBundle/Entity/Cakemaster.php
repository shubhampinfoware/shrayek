<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="cake_master")
*/
class Cakemaster
{

/**
	* @ORM\Column(type="string")
	*/
	protected $parent_category_id="";

	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $cake_master_id;

	
	/**
	* @ORM\Column(type="string")
	*/
	protected $cake_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cake_description="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $cake_image="";


	/**
	* @ORM\Column(type="string")
	*/
	protected $delivery_info="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $care_instructions="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $pricetype="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $ingredients_id="";


	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $cake_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";



	public function getParent_category_id()
	{
		return $this->parent_category_id;
	}
	public function setParent_category_id($parent_category_id)
	{
		$this->parent_category_id = $parent_category_id;
	}

	public function getCake_master_id()
	{
		return $this->cake_master_id;
	}



	public function getCake_name()
	{
		return $this->cake_name;
	}
	public function setCake_name($cake_name)
	{
		$this->cake_name = $cake_name;
	}

	public function getCake_description()
	{
		return $this->cake_description;
	}
	public function setCake_description($cake_description)
	{
		$this->cake_description = $cake_description;
	}


	public function getCake_image()
	{
		return $this->cake_image;
	}
	public function setCake_image($cake_image)
	{
		$this->cake_image = $cake_image;
	}

	public function getDelivery_info()
	{
		return $this->delivery_info;
	}
	public function setDelivery_info($delivery_info)
	{
		$this->delivery_info = $delivery_info;
	}

	public function getCare_instructions()
	{
		return $this->care_instructions;
	}
	public function setCare_instructions($care_instructions)
	{
		$this->care_instructions = $care_instructions;
	}
	public function getPricetype()
	{
		return $this->pricetype;
	}
	public function setPricetype($pricetype)
	{
		$this->pricetype = $pricetype;
	}

	public function getPrice()
	{
		return $this->price;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getIngredients_id()
	{
		return $this->ingredients_id;
	}
	public function setIngredients_id($ingredients_id)
	{
		$this->ingredients_id = $ingredients_id;
	}

	
	
	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCake_main_id()
	{
		return $this->cake_main_id;
	}
	public function setCake_main_id($cake_main_id)
	{
		$this->cake_main_id = $cake_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}