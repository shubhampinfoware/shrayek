<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="governorate_master")
*/
class Governoratemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $governorate_id;
	/**
	* @ORM\Column(type="string")
	*/
	protected $governorate_name="";
	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;
	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_governorate_id=0;
	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getGovernorate_id()
	{
		return $this->governorate_id;
	}

	public function getGovernorate_name()
	{
		return $this->governorate_name;
	}
	public function setGovernorate_name($governorate_name)
	{
		$this->governorate_name = $governorate_name;
	}
	
	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}
	public function getMain_governorate_id()
	{
		return $this->main_governorate_id;
	}
	public function setMain_governorate_id($main_governorate_id)
	{
		$this->main_governorate_id = $main_governorate_id;
	}
	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}
