<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="dining_experience")
*/
class Diningexperience
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $dining_experience_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $nick_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $email_address="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $restaurant_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $branch_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $comments="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $create_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getDining_experience_id()
	{
		return $this->dining_experience_id;
	}

	public function getNick_name()
	{
		return $this->nick_name;
	}
	public function setNick_name($nick_name)
	{
		$this->nick_name = $nick_name;
	}

	public function getEmail_address()
	{
		return $this->email_address;
	}
	public function setEmail_address($email_address)
	{
		$this->email_address = $email_address;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getBranch_id()
	{
		return $this->branch_id;
	}
	public function setBranch_id($branch_id)
	{
		$this->branch_id = $branch_id;
	}

	public function getComments()
	{
		return $this->comments;
	}
	public function setComments($comments)
	{
		$this->comments = $comments;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCreate_date()
	{
		return $this->create_date;
	}
	public function setCreate_date($create_date)
	{
		$this->create_date = $create_date;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}