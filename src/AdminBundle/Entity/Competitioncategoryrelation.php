<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_category_relation")
*/
class Competitioncategoryrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_category_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $competition_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $extra_points=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_category_relation_id()
	{
		return $this->competition_category_relation_id;
	}

	public function getCompetition_id()
	{
		return $this->competition_id;
	}
	public function setCompetition_id($competition_id)
	{
		$this->competition_id = $competition_id;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getExtra_points()
	{
		return $this->extra_points;
	}
	public function setExtra_points($extra_points)
	{
		$this->extra_points = $extra_points;
	}

	public function getType()
	{
		return $this->type;
	}
	public function setType($type)
	{
		$this->type = $type;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}