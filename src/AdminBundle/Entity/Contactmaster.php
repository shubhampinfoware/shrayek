<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="contact_master")
*/
class Contactmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $contact_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $contact_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $contact_email="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $contact_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $contact_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getContact_id()
	{
		return $this->contact_id;
	}

	public function getContact_name()
	{
		return $this->contact_name;
	}
	public function setContact_name($contact_name)
	{
		$this->contact_name = $contact_name;
	}

	public function getContact_email()
	{
		return $this->contact_email;
	}
	public function setContact_email($contact_email)
	{
		$this->contact_email = $contact_email;
	}

	public function getContact_description()
	{
		return $this->contact_description;
	}
	public function setContact_description($contact_description)
	{
		$this->contact_description = $contact_description;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getContact_main_id()
	{
		return $this->contact_main_id;
	}
	public function setContact_main_id($contact_main_id)
	{
		$this->contact_main_id = $contact_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}