<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="spongeflavor_master")
*/
class Spongeflavormaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $spongeflavor_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $spongeflavor_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $spongeflavor_image;

	/**
	* @ORM\Column(type="string")
	*/
	protected $spongeflavor_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $spongeflavor_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getSpongeflavor_id()
	{
		return $this->spongeflavor_id;
	}

	public function getSpongeflavor_name()
	{
		return $this->spongeflavor_name;
	}
	public function setSpongeflavor_name($spongeflavor_name)
	{
		$this->spongeflavor_name = $spongeflavor_name;
	}

	public function getSpongeflavor_image()
	{
		return $this->spongeflavor_image;
	}
	public function setSpongeflavor_image($spongeflavor_image)
	{
		$this->spongeflavor_image = $spongeflavor_image;
	}

	public function getSpongeflavor_price()
	{
		return $this->spongeflavor_price;
	}
	public function setSpongeflavor_price($spongeflavor_price)
	{
		$this->spongeflavor_price = $spongeflavor_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}
	public function getSpongeflavor_main_id()
	{
		return $this->spongeflavor_main_id;
	}
	public function setSpongeflavor_main_id($spongeflavor_main_id)
	{
		$this->spongeflavor_main_id = $spongeflavor_main_id;
	}


	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}