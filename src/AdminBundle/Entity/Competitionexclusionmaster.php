<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_exclusion_master")
*/
class Competitionexclusionmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_exclusion_master_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $competition_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_exclusion_master_id()
	{
		return $this->competition_exclusion_master_id;
	}

	public function getCompetition_id()
	{
		return $this->competition_id;
	}
	public function setCompetition_id($competition_id)
	{
		$this->competition_id = $competition_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}