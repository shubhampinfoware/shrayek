<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="saved_categorywise_restaurant")
*/
class Savedcategorywiserestaurant
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $saved_categorywise_restaurant_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $branch_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $evaluation_cnt=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $total_evaluation=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $rating_point=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $rating_percentage="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $update_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSaved_categorywise_restaurant_id()
	{
		return $this->saved_categorywise_restaurant_id;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getBranch_id()
	{
		return $this->branch_id;
	}
	public function setBranch_id($branch_id)
	{
		$this->branch_id = $branch_id;
	}

	public function getEvaluation_cnt()
	{
		return $this->evaluation_cnt;
	}
	public function setEvaluation_cnt($evaluation_cnt)
	{
		$this->evaluation_cnt = $evaluation_cnt;
	}

	public function getTotal_evaluation()
	{
		return $this->total_evaluation;
	}
	public function setTotal_evaluation($total_evaluation)
	{
		$this->total_evaluation = $total_evaluation;
	}

	public function getRating_point()
	{
		return $this->rating_point;
	}
	public function setRating_point($rating_point)
	{
		$this->rating_point = $rating_point;
	}

	public function getRating_percentage()
	{
		return $this->rating_percentage;
	}
	public function setRating_percentage($rating_percentage)
	{
		$this->rating_percentage = $rating_percentage;
	}

	public function getUpdate_datetime()
	{
		return $this->update_datetime;
	}
	public function setUpdate_datetime($update_datetime)
	{
		$this->update_datetime = $update_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}