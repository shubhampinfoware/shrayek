<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="food_type_master")
*/
class Foodtypemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $food_type_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $food_type_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_food_type_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $food_type_image_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getFood_type_master_id()
	{
		return $this->food_type_master_id;
	}

	public function getFood_type_name()
	{
		return $this->food_type_name;
	}
	public function setFood_type_name($food_type_name)
	{
		$this->food_type_name = $food_type_name;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_food_type_id()
	{
		return $this->main_food_type_id;
	}
	public function setMain_food_type_id($main_food_type_id)
	{
		$this->main_food_type_id = $main_food_type_id;
	}

	public function getFood_type_image_id()
	{
		return $this->food_type_image_id;
	}
	public function setFood_type_image_id($food_type_image_id)
	{
		$this->food_type_image_id = $food_type_image_id;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}