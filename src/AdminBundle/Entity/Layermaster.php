<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="layer_master")
*/
class Layermaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $layer_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $shape_main_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $layer_number="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $layer_image="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $layer_image_display="";
	
	/**
	* @ORM\Column(type="string")
	*/
	protected $layer_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $layer_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getLayer_id()
	{
		return $this->layer_id;
	}

	public function getShape_main_id()
	{
		return $this->shape_main_id;
	}
	public function setShape_main_id($shape_main_id)
	{
		$this->shape_main_id = $shape_main_id;
	}

	public function getLayer_number()
	{
		return $this->layer_number;
	}
	public function setLayer_number($layer_number)
	{
		$this->layer_number = $layer_number;
	}

	public function getLayer_image()
	{
		return $this->layer_image;
	}
	public function setLayer_image($layer_image)
	{
		$this->layer_image = $layer_image;
	}
	public function getLayer_image_display()
	{
		return $this->layer_image_display;
	}
	public function setLayer_image_display($layer_image_display)
	{
		$this->layer_image_display = $layer_image_display;
	}

	

	public function getLayer_price()
	{
		return $this->layer_price;
	}
	public function setLayer_price($layer_price)
	{
		$this->layer_price = $layer_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getLayer_main_id()
	{
		return $this->layer_main_id;
	}
	public function setLayer_main_id($layer_main_id)
	{
		$this->layer_main_id = $layer_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}