<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_user_relation")
*/
class Competitionuserrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_user_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $competition_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $activity_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $related_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $related_table_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $points=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_user_relation_id()
	{
		return $this->competition_user_relation_id;
	}

	public function getCompetition_id()
	{
		return $this->competition_id;
	}
	public function setCompetition_id($competition_id)
	{
		$this->competition_id = $competition_id;
	}

	public function getActivity_id()
	{
		return $this->activity_id;
	}
	public function setActivity_id($activity_id)
	{
		$this->activity_id = $activity_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getRelated_id()
	{
		return $this->related_id;
	}
	public function setRelated_id($related_id)
	{
		$this->related_id = $related_id;
	}

	public function getRelated_table_name()
	{
		return $this->related_table_name;
	}
	public function setRelated_table_name($related_table_name)
	{
		$this->related_table_name = $related_table_name;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getPoints()
	{
		return $this->points;
	}
	public function setPoints($points)
	{
		$this->points = $points;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}