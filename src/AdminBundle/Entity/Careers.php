<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="careers")
*/
class Careers
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $career_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $career_title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $career_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $career_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getCareer_id()
	{
		return $this->career_id;
	}

	public function getCareer_title()
	{
		return $this->career_title;
	}
	public function setCareer_title($career_title)
	{
		$this->career_title = $career_title;
	}

	public function getCareer_description()
	{
		return $this->career_description;
	}
	public function setCareer_description($career_description)
	{
		$this->career_description = $career_description;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}
	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCareer_main_id()
	{
		return $this->career_main_id;
	}
	public function setCareer_main_id($career_main_id)
	{
		$this->career_main_id = $career_main_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}