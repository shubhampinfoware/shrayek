<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="level_master")
*/
class Levelmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $level_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $level_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $level_number=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_level_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getLevel_master_id()
	{
		return $this->level_master_id;
	}

	public function getLevel_name()
	{
		return $this->level_name;
	}
	public function setLevel_name($level_name)
	{
		$this->level_name = $level_name;
	}

	public function getLevel_number()
	{
		return $this->level_number;
	}
	public function setLevel_number($level_number)
	{
		$this->level_number = $level_number;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_level_id()
	{
		return $this->main_level_id;
	}
	public function setMain_level_id($main_level_id)
	{
		$this->main_level_id = $main_level_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}