<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="reviews")
*/
class Reviews
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $review_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_id="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $cake_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $rating="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $review_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getReview_id()
	{
		return $this->review_id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getCake_id()
	{
		return $this->cake_id;
	}
	public function setCake_id($cake_id)
	{
		$this->cake_id = $cake_id;
	}

	public function getRating()
	{
		return $this->rating;
	}
	public function setRating($rating)
	{
		$this->rating = $rating;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getReview_main_id()
	{
		return $this->review_main_id;
	}
	public function setReview_main_id($review_main_id)
	{
		$this->review_main_id = $review_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}