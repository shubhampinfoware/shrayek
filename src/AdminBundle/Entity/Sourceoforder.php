<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="source_of_order")
*/
class Sourceoforder
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $source_of_order_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $source_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getSource_of_order_id()
	{
		return $this->source_of_order_id;
	}

	public function getSource_name()
	{
		return $this->source_name;
	}
	public function setSource_name($source_name)
	{
		$this->source_name = $source_name;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}