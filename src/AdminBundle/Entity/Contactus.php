<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="contact_us")
*/
class Contactus
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $contact_us_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_contact_us_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $address="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $phone_number="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $email_address="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $fb_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $twitter_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $insta_link="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	public function getContact_us_id()
	{
		return $this->contact_us_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_contact_us_id()
	{
		return $this->main_contact_us_id;
	}
	public function setMain_contact_us_id($main_contact_us_id)
	{
		$this->main_contact_us_id = $main_contact_us_id;
	}

	public function getAddress()
	{
		return $this->address;
	}
	public function setAddress($address)
	{
		$this->address = $address;
	}

	public function getPhone_number()
	{
		return $this->phone_number;
	}
	public function setPhone_number($phone_number)
	{
		$this->phone_number = $phone_number;
	}

	public function getEmail_address()
	{
		return $this->email_address;
	}
	public function setEmail_address($email_address)
	{
		$this->email_address = $email_address;
	}

	public function getFb_link()
	{
		return $this->fb_link;
	}
	public function setFb_link($fb_link)
	{
		$this->fb_link = $fb_link;
	}

	public function getTwitter_link()
	{
		return $this->twitter_link;
	}
	public function setTwitter_link($twitter_link)
	{
		$this->twitter_link = $twitter_link;
	}

	public function getInsta_link()
	{
		return $this->insta_link;
	}
	public function setInsta_link($insta_link)
	{
		$this->insta_link = $insta_link;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}
}