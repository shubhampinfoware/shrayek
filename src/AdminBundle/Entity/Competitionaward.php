<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_award")
*/
class Competitionaward
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_award_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $competition_award_title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $competition_award_cover_image_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $competition_award_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $short_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $start_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $end_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_competition_award_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_award_id()
	{
		return $this->competition_award_id;
	}

	public function getCompetition_award_title()
	{
		return $this->competition_award_title;
	}
	public function setCompetition_award_title($competition_award_title)
	{
		$this->competition_award_title = $competition_award_title;
	}

	public function getCompetition_award_cover_image_id()
	{
		return $this->competition_award_cover_image_id;
	}
	public function setCompetition_award_cover_image_id($competition_award_cover_image_id)
	{
		$this->competition_award_cover_image_id = $competition_award_cover_image_id;
	}

	public function getCompetition_award_date()
	{
		return $this->competition_award_date;
	}
	public function setCompetition_award_date($competition_award_date)
	{
		$this->competition_award_date = $competition_award_date;
	}

	public function getShort_description()
	{
		return $this->short_description;
	}
	public function setShort_description($short_description)
	{
		$this->short_description = $short_description;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getStart_date()
	{
		return $this->start_date;
	}
	public function setStart_date($start_date)
	{
		$this->start_date = $start_date;
	}

	public function getEnd_date()
	{
		return $this->end_date;
	}
	public function setEnd_date($end_date)
	{
		$this->end_date = $end_date;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_competition_award_id()
	{
		return $this->main_competition_award_id;
	}
	public function setMain_competition_award_id($main_competition_award_id)
	{
		$this->main_competition_award_id = $main_competition_award_id;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}