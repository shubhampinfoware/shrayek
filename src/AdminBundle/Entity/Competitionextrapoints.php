<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_extra_points")
*/
class Competitionextrapoints
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_extra_points_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $rest_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $comp_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $extra_points=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_extra_points_id()
	{
		return $this->competition_extra_points_id;
	}

	public function getRest_id()
	{
		return $this->rest_id;
	}
	public function setRest_id($rest_id)
	{
		$this->rest_id = $rest_id;
	}

	public function getComp_id()
	{
		return $this->comp_id;
	}
	public function setComp_id($comp_id)
	{
		$this->comp_id = $comp_id;
	}

	public function getExtra_points()
	{
		return $this->extra_points;
	}
	public function setExtra_points($extra_points)
	{
		$this->extra_points = $extra_points;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}