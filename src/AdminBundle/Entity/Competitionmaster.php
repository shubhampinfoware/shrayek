<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_master")
*/
class Competitionmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $name_ar="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_special="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $start_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $end_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_competition_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $rule_book=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $extra_point_type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $extra_point_value=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $created_by=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getCompetition_master_id()
	{
		return $this->competition_master_id;
	}

	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName_ar()
	{
		return $this->name_ar;
	}
	public function setName_ar($name_ar)
	{
		$this->name_ar = $name_ar;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_special()
	{
		return $this->is_special;
	}
	public function setIs_special($is_special)
	{
		$this->is_special = $is_special;
	}

	public function getStart_date()
	{
		return $this->start_date;
	}
	public function setStart_date($start_date)
	{
		$this->start_date = $start_date;
	}

	public function getEnd_date()
	{
		return $this->end_date;
	}
	public function setEnd_date($end_date)
	{
		$this->end_date = $end_date;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_competition_id()
	{
		return $this->main_competition_id;
	}
	public function setMain_competition_id($main_competition_id)
	{
		$this->main_competition_id = $main_competition_id;
	}

	public function getRule_book()
	{
		return $this->rule_book;
	}
	public function setRule_book($rule_book)
	{
		$this->rule_book = $rule_book;
	}

	public function getExtra_point_type()
	{
		return $this->extra_point_type;
	}
	public function setExtra_point_type($extra_point_type)
	{
		$this->extra_point_type = $extra_point_type;
	}

	public function getExtra_point_value()
	{
		return $this->extra_point_value;
	}
	public function setExtra_point_value($extra_point_value)
	{
		$this->extra_point_value = $extra_point_value;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}