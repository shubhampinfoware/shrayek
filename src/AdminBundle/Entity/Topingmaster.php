<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="toping_master")
*/
class Topingmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $toping_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $toping_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $toping_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $toping_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $toping_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getToping_id()
	{
		return $this->toping_id;
	}
	public function setToping_id($toping_id)
	{
		$this->toping_id = $toping_id;
	}

	public function getToping_name()
	{
		return $this->toping_name;
	}
	public function setToping_name($toping_name)
	{
		$this->toping_name = $toping_name;
	}

	public function getToping_image()
	{
		return $this->toping_image;
	}
	public function setToping_image($toping_image)
	{
		$this->toping_image = $toping_image;
	}

	public function getToping_price()
	{
		return $this->toping_price;
	}
	public function setToping_price($toping_price)
	{
		$this->toping_price = $toping_price;
	}
	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getToping_main_id()
	{
		return $this->toping_main_id;
	}
	public function setToping_main_id($toping_main_id)
	{
		$this->toping_main_id = $toping_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}