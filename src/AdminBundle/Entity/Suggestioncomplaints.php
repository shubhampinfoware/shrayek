<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="suggestion_complaints")
*/
class Suggestioncomplaints
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $suggestion_complaints_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $nick_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $email_address="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $subject="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $comments="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_date="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSuggestion_complaints_id()
	{
		return $this->suggestion_complaints_id;
	}

	public function getNick_name()
	{
		return $this->nick_name;
	}
	public function setNick_name($nick_name)
	{
		$this->nick_name = $nick_name;
	}

	public function getEmail_address()
	{
		return $this->email_address;
	}
	public function setEmail_address($email_address)
	{
		$this->email_address = $email_address;
	}

	public function getSubject()
	{
		return $this->subject;
	}
	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	public function getComments()
	{
		return $this->comments;
	}
	public function setComments($comments)
	{
		$this->comments = $comments;
	}

	public function getCreated_date()
	{
		return $this->created_date;
	}
	public function setCreated_date($created_date)
	{
		$this->created_date = $created_date;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}