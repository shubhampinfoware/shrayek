<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="fillingtopdecor_master")
*/
class Fillingtopdecormaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $filling_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $filling_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $filling_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $filling_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $filling_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getFilling_id()
	{
		return $this->filling_id;
	}
	public function setFilling_id($filling_id)
	{
		$this->filling_id = $filling_id;
	}

	public function getFilling_name()
	{
		return $this->filling_name;
	}
	public function setFilling_name($filling_name)
	{
		$this->filling_name = $filling_name;
	}

	public function getFilling_image()
	{
		return $this->filling_image;
	}
	public function setFilling_image($filling_image)
	{
		$this->filling_image = $filling_image;
	}

	public function getFilling_price()
	{
		return $this->filling_price;
	}
	public function setFilling_price($filling_price)
	{
		$this->filling_price = $filling_price;
	}

	public function getFilling_main_id()
	{
		return $this->filling_main_id;
	}
	public function setFilling_main_id($filling_main_id)
	{
		$this->filling_main_id = $filling_main_id;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}