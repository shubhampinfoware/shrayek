<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="dining_experience_gallery")
*/
class Diningexperiencegallery
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $dining_experience_gallery_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $dining_experience_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $image_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getDining_experience_gallery_id()
	{
		return $this->dining_experience_gallery_id;
	}

	public function getDining_experience_id()
	{
		return $this->dining_experience_id;
	}
	public function setDining_experience_id($dining_experience_id)
	{
		$this->dining_experience_id = $dining_experience_id;
	}

	public function getImage_id()
	{
		return $this->image_id;
	}
	public function setImage_id($image_id)
	{
		$this->image_id = $image_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}