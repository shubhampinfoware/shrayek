<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="branch_master")
*/
class Branchmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $branch_master_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_restaurant_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $branch_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $branch_address_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_branch_flag=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $opening_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $timings="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $mobile_no="0";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_branch_master_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getBranch_master_id()
	{
		return $this->branch_master_id;
	}

	public function getMain_restaurant_id()
	{
		return $this->main_restaurant_id;
	}
	public function setMain_restaurant_id($main_restaurant_id)
	{
		$this->main_restaurant_id = $main_restaurant_id;
	}

	public function getBranch_name()
	{
		return $this->branch_name;
	}
	public function setBranch_name($branch_name)
	{
		$this->branch_name = $branch_name;
	}

	public function getBranch_address_id()
	{
		return $this->branch_address_id;
	}
	public function setBranch_address_id($branch_address_id)
	{
		$this->branch_address_id = $branch_address_id;
	}

	public function getMain_branch_flag()
	{
		return $this->main_branch_flag;
	}
	public function setMain_branch_flag($main_branch_flag)
	{
		$this->main_branch_flag = $main_branch_flag;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getOpening_date()
	{
		return $this->opening_date;
	}
	public function setOpening_date($opening_date)
	{
		$this->opening_date = $opening_date;
	}

	public function getTimings()
	{
		return $this->timings;
	}
	public function setTimings($timings)
	{
		$this->timings = $timings;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getMobile_no()
	{
		return $this->mobile_no;
	}
	public function setMobile_no($mobile_no = 0)
	{
		$this->mobile_no = $mobile_no;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_branch_master_id()
	{
		return $this->main_branch_master_id;
	}
	public function setMain_branch_master_id($main_branch_master_id)
	{
		$this->main_branch_master_id = $main_branch_master_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}