<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="news_master")
*/
class Newsmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $news_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $news_title="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $news_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";
	/**
	* @ORM\Column(type="integer")
	*/
	protected $news_main_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getNews_id()
	{
		return $this->news_id;
	}

	public function getNews_title()
	{
		return $this->news_title;
	}
	public function setNews_title($news_title)
	{
		$this->news_title = $news_title;
	}

	public function getNews_description()
	{
		return $this->news_description;
	}
	public function setNews_description($news_description)
	{
		$this->news_description = $news_description;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

public function getNews_main_id()
	{
		return $this->news_main_id;
	}
	public function setNews_main_id($news_main_id)
	{
		$this->news_main_id = $news_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}