<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="people_master")
*/
class Peoplemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $people_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $weight_type=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $weight="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $no_of_people="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getPeople_id()
	{
		return $this->people_id;
	}

	public function getWeight_type()
	{
		return $this->weight_type;
	}
	public function setWeight_type($weight_type)
	{
		$this->weight_type = $weight_type;
	}

	public function getWeight()
	{
		return $this->weight;
	}
	public function setWeight($weight)
	{
		$this->weight = $weight;
	}

	public function getNo_of_people()
	{
		return $this->no_of_people;
	}
	public function setNo_of_people($no_of_people)
	{
		$this->no_of_people = $no_of_people;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}