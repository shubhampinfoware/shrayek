<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="medical_report_image")
*/
class Medicalreportimage
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $medical_report_image_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $medical_report_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_report_type=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $report_image=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $report_video_thumnail=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getMedical_report_image_id()
	{
		return $this->medical_report_image_id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getMedical_report_id()
	{
		return $this->medical_report_id;
	}
	public function setMedical_report_id($medical_report_id)
	{
		$this->medical_report_id = $medical_report_id;
	}

	public function getMedia_report_type()
	{
		return $this->media_report_type;
	}
	public function setMedia_report_type($media_report_type)
	{
		$this->media_report_type = $media_report_type;
	}

	public function getReport_image()
	{
		return $this->report_image;
	}
	public function setReport_image($report_image)
	{
		$this->report_image = $report_image;
	}

	public function getReport_video_thumnail()
	{
		return $this->report_video_thumnail;
	}
	public function setReport_video_thumnail($report_video_thumnail)
	{
		$this->report_video_thumnail = $report_video_thumnail;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}