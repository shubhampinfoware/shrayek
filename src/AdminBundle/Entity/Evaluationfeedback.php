<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="evaluation_feedback")
*/
class Evaluationfeedback
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $evaluation_feedback_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $user_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $food_type_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_branch_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $service_level=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $dining_level=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $atmosphere_level=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $price_level=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $clean_level=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $delivery_speed_level=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $packaging_level=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $eval_type="dining";

	/**
	* @ORM\Column(type="string")
	*/
	protected $comments="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $invoice_image_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $rejected_reason="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_featured="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $show_featured="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getEvaluation_feedback_id()
	{
		return $this->evaluation_feedback_id;
	}

	public function getUser_id()
	{
		return $this->user_id;
	}
	public function setUser_id($user_id)
	{
		$this->user_id = $user_id;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getFood_type_id()
	{
		return $this->food_type_id;
	}
	public function setFood_type_id($food_type_id)
	{
		$this->food_type_id = $food_type_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getMain_branch_id()
	{
		return $this->main_branch_id;
	}
	public function setMain_branch_id($main_branch_id)
	{
		$this->main_branch_id = $main_branch_id;
	}

	public function getService_level()
	{
		return $this->service_level;
	}
	public function setService_level($service_level)
	{
		$this->service_level = $service_level;
	}

	public function getDining_level()
	{
		return $this->dining_level;
	}
	public function setDining_level($dining_level)
	{
		$this->dining_level = $dining_level;
	}

	public function getAtmosphere_level()
	{
		return $this->atmosphere_level;
	}
	public function setAtmosphere_level($atmosphere_level)
	{
		$this->atmosphere_level = $atmosphere_level;
	}

	public function getPrice_level()
	{
		return $this->price_level;
	}
	public function setPrice_level($price_level)
	{
		$this->price_level = $price_level;
	}

	public function getClean_level()
	{
		return $this->clean_level;
	}
	public function setClean_level($clean_level)
	{
		$this->clean_level = $clean_level;
	}

	public function getDelivery_speed_level()
	{
		return $this->delivery_speed_level;
	}
	public function setDelivery_speed_level($delivery_speed_level = 0)
	{
		$this->delivery_speed_level = $delivery_speed_level;
	}

	public function getPackaging_level()
	{
		return $this->packaging_level;
	}
	public function setPackaging_level($packaging_level = 0)
	{
		$this->packaging_level = $packaging_level;
	}

	public function getEval_type()
	{
		return $this->eval_type;
	}
	public function setEval_type($eval_type = 'dining')
	{
		$this->eval_type = $eval_type;
	}

	public function getComments()
	{
		return $this->comments;
	}
	public function setComments($comments)
	{
		$this->comments = $comments;
	}

	public function getInvoice_image_id()
	{
		return $this->invoice_image_id;
	}
	public function setInvoice_image_id($invoice_image_id)
	{
		$this->invoice_image_id = $invoice_image_id;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getRejected_reason()
	{
		return $this->rejected_reason;
	}
	public function setRejected_reason($rejected_reason)
	{
		$this->rejected_reason = $rejected_reason;
	}

	public function getIs_featured()
	{
		return $this->is_featured;
	}
	public function setIs_featured($is_featured)
	{
		$this->is_featured = $is_featured;
	}

	public function getShow_featured()
	{
		return $this->show_featured;
	}
	public function setShow_featured($show_featured)
	{
		$this->show_featured = $show_featured;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}