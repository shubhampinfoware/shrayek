<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="user_role_master")
*/
class Userrolemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $user_role_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_role_code="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_role_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $user_role_description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getUser_role_master_id()
	{
		return $this->user_role_master_id;
	}

	public function getUser_role_code()
	{
		return $this->user_role_code;
	}
	public function setUser_role_code($user_role_code)
	{
		$this->user_role_code = $user_role_code;
	}

	public function getUser_role_name()
	{
		return $this->user_role_name;
	}
	public function setUser_role_name($user_role_name)
	{
		$this->user_role_name = $user_role_name;
	}

	public function getUser_role_description()
	{
		return $this->user_role_description;
	}
	public function setUser_role_description($user_role_description)
	{
		$this->user_role_description = $user_role_description;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}