<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="restaurant_menu")
*/
class Restaurantmenu
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $restaurant_menu_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $sort_order=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getRestaurant_menu_id()
	{
		return $this->restaurant_menu_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMedia_id()
	{
		return $this->media_id;
	}
	public function setMedia_id($media_id)
	{
		$this->media_id = $media_id;
	}

	public function getSort_order()
	{
		return $this->sort_order;
	}
	public function setSort_order($sort_order)
	{
		$this->sort_order = $sort_order;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}