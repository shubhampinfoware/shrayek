<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="order_cart_estimate")
*/
class Ordercartestimate
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $order_cart_estimate_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $order_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $cart_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $qty=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $qty_type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $size=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $size_type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $unit_price="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $pricetype=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $currency_unit="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $item_type="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $cake_master_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $number_of_people=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $layer_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $spongeflavor_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $fillingtopdecoration_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $imagetopdecoration_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $sidedecoration_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $msg_on_cake="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $msg_color_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $cupcakesize_main_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $icing_main_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $toping_main_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $estimate_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $accept_by=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $accept_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getOrder_cart_estimate_id()
	{
		return $this->order_cart_estimate_id;
	}

	public function getOrder_id()
	{
		return $this->order_id;
	}
	public function setOrder_id($order_id)
	{
		$this->order_id = $order_id;
	}

	public function getCart_id()
	{
		return $this->cart_id;
	}
	public function setCart_id($cart_id)
	{
		$this->cart_id = $cart_id;
	}

	public function getQty()
	{
		return $this->qty;
	}
	public function setQty($qty)
	{
		$this->qty = $qty;
	}

	public function getQty_type()
	{
		return $this->qty_type;
	}
	public function setQty_type($qty_type)
	{
		$this->qty_type = $qty_type;
	}

	public function getSize()
	{
		return $this->size;
	}
	public function setSize($size)
	{
		$this->size = $size;
	}

	public function getSize_type()
	{
		return $this->size_type;
	}
	public function setSize_type($size_type)
	{
		$this->size_type = $size_type;
	}

	public function getUnit_price()
	{
		return $this->unit_price;
	}
	public function setUnit_price($unit_price)
	{
		$this->unit_price = $unit_price;
	}

	public function getPricetype()
	{
		return $this->pricetype;
	}
	public function setPricetype($pricetype)
	{
		$this->pricetype = $pricetype;
	}

	public function getCurrency_unit()
	{
		return $this->currency_unit;
	}
	public function setCurrency_unit($currency_unit)
	{
		$this->currency_unit = $currency_unit;
	}

	public function getItem_type()
	{
		return $this->item_type;
	}
	public function setItem_type($item_type)
	{
		$this->item_type = $item_type;
	}

	public function getCake_master_id()
	{
		return $this->cake_master_id;
	}
	public function setCake_master_id($cake_master_id)
	{
		$this->cake_master_id = $cake_master_id;
	}

	public function getNumber_of_people()
	{
		return $this->number_of_people;
	}
	public function setNumber_of_people($number_of_people)
	{
		$this->number_of_people = $number_of_people;
	}

	public function getLayer_id()
	{
		return $this->layer_id;
	}
	public function setLayer_id($layer_id)
	{
		$this->layer_id = $layer_id;
	}

	public function getSpongeflavor_id()
	{
		return $this->spongeflavor_id;
	}
	public function setSpongeflavor_id($spongeflavor_id)
	{
		$this->spongeflavor_id = $spongeflavor_id;
	}

	public function getFillingtopdecoration_id()
	{
		return $this->fillingtopdecoration_id;
	}
	public function setFillingtopdecoration_id($fillingtopdecoration_id)
	{
		$this->fillingtopdecoration_id = $fillingtopdecoration_id;
	}

	public function getImagetopdecoration_id()
	{
		return $this->imagetopdecoration_id;
	}
	public function setImagetopdecoration_id($imagetopdecoration_id)
	{
		$this->imagetopdecoration_id = $imagetopdecoration_id;
	}

	public function getSidedecoration_id()
	{
		return $this->sidedecoration_id;
	}
	public function setSidedecoration_id($sidedecoration_id)
	{
		$this->sidedecoration_id = $sidedecoration_id;
	}

	public function getMsg_on_cake()
	{
		return $this->msg_on_cake;
	}
	public function setMsg_on_cake($msg_on_cake)
	{
		$this->msg_on_cake = $msg_on_cake;
	}

	public function getMsg_color_id()
	{
		return $this->msg_color_id;
	}
	public function setMsg_color_id($msg_color_id)
	{
		$this->msg_color_id = $msg_color_id;
	}

	public function getCupcakesize_main_id()
	{
		return $this->cupcakesize_main_id;
	}
	public function setCupcakesize_main_id($cupcakesize_main_id)
	{
		$this->cupcakesize_main_id = $cupcakesize_main_id;
	}

	public function getIcing_main_id()
	{
		return $this->icing_main_id;
	}
	public function setIcing_main_id($icing_main_id)
	{
		$this->icing_main_id = $icing_main_id;
	}

	public function getToping_main_id()
	{
		return $this->toping_main_id;
	}
	public function setToping_main_id($toping_main_id)
	{
		$this->toping_main_id = $toping_main_id;
	}

	public function getEstimate_datetime()
	{
		return $this->estimate_datetime;
	}
	public function setEstimate_datetime($estimate_datetime)
	{
		$this->estimate_datetime = $estimate_datetime;
	}

	public function getAccept_by()
	{
		return $this->accept_by;
	}
	public function setAccept_by($accept_by)
	{
		$this->accept_by = $accept_by;
	}

	public function getAccept_datetime()
	{
		return $this->accept_datetime;
	}
	public function setAccept_datetime($accept_datetime)
	{
		$this->accept_datetime = $accept_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}