<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="shape_master")
*/
class Shapemaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $shape_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $shape_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $shape_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $shape_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $shape_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getShape_id()
	{
		return $this->shape_id;
	}
	public function setShape_id($shape_id)
	{
		$this->shape_id = $shape_id;
	}

	public function getShape_name()
	{
		return $this->shape_name;
	}
	public function setShape_name($shape_name)
	{
		$this->shape_name = $shape_name;
	}

	public function getShape_image()
	{
		return $this->shape_image;
	}
	public function setShape_image($shape_image)
	{
		$this->shape_image = $shape_image;
	}

	public function getShape_price()
	{
		return $this->shape_price;
	}
	public function setShape_price($shape_price)
	{
		$this->shape_price = $shape_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}
	public function getShape_main_id()
	{
		return $this->shape_main_id;
	}
	public function setShape_main_id($shape_main_id)
	{
		$this->shape_main_id = $shape_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}