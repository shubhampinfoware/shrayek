<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="food_type_category_relation")
*/
class Foodtypecategoryrelation
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $food_type_category_relation_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_food_type_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_category_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getFood_type_category_relation_id()
	{
		return $this->food_type_category_relation_id;
	}

	public function getMain_food_type_id()
	{
		return $this->main_food_type_id;
	}
	public function setMain_food_type_id($main_food_type_id)
	{
		$this->main_food_type_id = $main_food_type_id;
	}

	public function getMain_category_id()
	{
		return $this->main_category_id;
	}
	public function setMain_category_id($main_category_id)
	{
		$this->main_category_id = $main_category_id;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}