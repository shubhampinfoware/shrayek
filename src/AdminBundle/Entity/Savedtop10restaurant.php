<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="saved_top10_restaurant")
*/
class Savedtop10restaurant
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $saved_top10_restaurant_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $points=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $rating=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $rating_point_percentage="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $updated_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSaved_top10_restaurant_id()
	{
		return $this->saved_top10_restaurant_id;
	}

	public function getRestaurant_id()
	{
		return $this->restaurant_id;
	}
	public function setRestaurant_id($restaurant_id)
	{
		$this->restaurant_id = $restaurant_id;
	}

	public function getPoints()
	{
		return $this->points;
	}
	public function setPoints($points)
	{
		$this->points = $points;
	}

	public function getRating()
	{
		return $this->rating;
	}
	public function setRating($rating)
	{
		$this->rating = $rating;
	}

	public function getRating_point_percentage()
	{
		return $this->rating_point_percentage;
	}
	public function setRating_point_percentage($rating_point_percentage)
	{
		$this->rating_point_percentage = $rating_point_percentage;
	}

	public function getUpdated_datetime()
	{
		return $this->updated_datetime;
	}
	public function setUpdated_datetime($updated_datetime)
	{
		$this->updated_datetime = $updated_datetime;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}