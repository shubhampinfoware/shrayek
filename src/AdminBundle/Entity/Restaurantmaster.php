<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="restaurant_master")
*/
class Restaurantmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $restaurant_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $restaurant_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $restaurant_menu=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $restaraunt_branch="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $phone_number="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $description="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $opening_date="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $additional_info="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $instagram_link="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $logo_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $timings="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $address_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_restaurant_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $domain_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getRestaurant_master_id()
	{
		return $this->restaurant_master_id;
	}

	public function getRestaurant_name()
	{
		return $this->restaurant_name;
	}
	public function setRestaurant_name($restaurant_name)
	{
		$this->restaurant_name = $restaurant_name;
	}

	public function getRestaurant_menu()
	{
		return $this->restaurant_menu;
	}
	public function setRestaurant_menu($restaurant_menu)
	{
		$this->restaurant_menu = $restaurant_menu;
	}

	public function getRestaraunt_branch()
	{
		return $this->restaraunt_branch;
	}
	public function setRestaraunt_branch($restaraunt_branch)
	{
		$this->restaraunt_branch = $restaraunt_branch;
	}

	public function getPhone_number()
	{
		return $this->phone_number;
	}
	public function setPhone_number($phone_number)
	{
		$this->phone_number = $phone_number;
	}

	public function getDescription()
	{
		return $this->description;
	}
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getOpening_date()
	{
		return $this->opening_date;
	}
	public function setOpening_date($opening_date)
	{
		$this->opening_date = $opening_date;
	}

	public function getAdditional_info()
	{
		return $this->additional_info;
	}
	public function setAdditional_info($additional_info)
	{
		$this->additional_info = $additional_info;
	}

	public function getInstagram_link()
	{
		return $this->instagram_link;
	}
	public function setInstagram_link($instagram_link)
	{
		$this->instagram_link = $instagram_link;
	}

	public function getLogo_id()
	{
		return $this->logo_id;
	}
	public function setLogo_id($logo_id)
	{
		$this->logo_id = $logo_id;
	}

	public function getTimings()
	{
		return $this->timings;
	}
	public function setTimings($timings)
	{
		$this->timings = $timings;
	}

	public function getAddress_id()
	{
		return $this->address_id;
	}
	public function setAddress_id($address_id)
	{
		$this->address_id = $address_id;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getMain_restaurant_id()
	{
		return $this->main_restaurant_id;
	}
	public function setMain_restaurant_id($main_restaurant_id)
	{
		$this->main_restaurant_id = $main_restaurant_id;
	}

	public function getDomain_id()
	{
		return $this->domain_id;
	}
	public function setDomain_id($domain_id)
	{
		$this->domain_id = $domain_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}