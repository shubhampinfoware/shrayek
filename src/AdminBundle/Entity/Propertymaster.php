<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="property_master11")
*/
class Propertymaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $property_master_id;

	/**
	* @ORM\Column(type="string")
	*/
	protected $property_owner_name="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $governorate_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $city_id=0;

	/**
	* @ORM\OneToMany(targetEntity="Propertytype", inversedBy="property_type")
	* @ORM\JoinColumn(name="property_type", referencedColumnName="property_type_id")
	*/
	protected $property_type="";

	/**
	* @ORM\OneToMany(targetEntity="Addressmaster", inversedBy="address_id")
	* @ORM\JoinColumn(name="address_id", referencedColumnName="address_master_id")
	*/
	protected $address_id="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $price=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $details="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $advertise_type="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $created_datetime="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $created_by=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getProperty_master_id()
	{
		return $this->property_master_id;
	}

	public function getProperty_owner_name()
	{
		return $this->property_owner_name;
	}
	public function setProperty_owner_name($property_owner_name)
	{
		$this->property_owner_name = $property_owner_name;
	}

	public function getGovernorate_id()
	{
		return $this->governorate_id;
	}
	public function setGovernorate_id($governorate_id)
	{
		$this->governorate_id = $governorate_id;
	}

	public function getCity_id()
	{
		return $this->city_id;
	}
	public function setCity_id($city_id)
	{
		$this->city_id = $city_id;
	}

	public function getProperty_type()
	{
		return $this->property_type;
	}
	public function setProperty_type($property_type)
	{
		$this->property_type = $property_type;
	}

	public function getAddress_id()
	{
		return $this->address_id;
	}
	public function setAddress_id($address_id)
	{
		$this->address_id = $address_id;
	}

	public function getPrice()
	{
		return $this->price;
	}
	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getDetails()
	{
		return $this->details;
	}
	public function setDetails($details)
	{
		$this->details = $details;
	}

	public function getAdvertise_type()
	{
		return $this->advertise_type;
	}
	public function setAdvertise_type($advertise_type)
	{
		$this->advertise_type = $advertise_type;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getCreated_datetime()
	{
		return $this->created_datetime;
	}
	public function setCreated_datetime($created_datetime)
	{
		$this->created_datetime = $created_datetime;
	}

	public function getCreated_by()
	{
		return $this->created_by;
	}
	public function setCreated_by($created_by)
	{
		$this->created_by = $created_by;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}