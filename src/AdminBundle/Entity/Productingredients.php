<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="product_ingredients")
*/
class Productingredients
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $ingredients_id=0;
/**
	* @ORM\Column(type="integer")
	*/
	protected $ingredients_main_id=0;
	/**
	* @ORM\Column(type="integer")
	*/
	protected $cake_master_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $language_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getIngredients_id()
	{
		return $this->ingredients_id;
	}
	public function setIngredients_id($ingredients_id)
	{
		$this->ingredients_id = $ingredients_id;
	}

	public function getIngredients_main_id()
	{
		return $this->ingredients_main_id;
	}
	public function setIngredients_main_id($ingredients_main_id)
	{
		$this->ingredients_main_id = $ingredients_main_id;
	}

	public function getCake_master_id()
	{
		return $this->cake_master_id;
	}
	public function setCake_master_id($cake_master_id)
	{
		$this->cake_master_id = $cake_master_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}