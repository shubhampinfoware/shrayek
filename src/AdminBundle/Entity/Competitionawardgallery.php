<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="competition_award_gallery")
*/
class Competitionawardgallery
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $competition_award_gallery_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $main_competition_award_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $image_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getCompetition_award_gallery_id()
	{
		return $this->competition_award_gallery_id;
	}

	public function getMain_competition_award_id()
	{
		return $this->main_competition_award_id;
	}
	public function setMain_competition_award_id($main_competition_award_id)
	{
		$this->main_competition_award_id = $main_competition_award_id;
	}

	public function getImage_id()
	{
		return $this->image_id;
	}
	public function setImage_id($image_id)
	{
		$this->image_id = $image_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}