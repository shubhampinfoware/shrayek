<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="cake_image")
*/
class Cakeimage
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $cake_image_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $category_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $image_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getCake_image_id()
	{
		return $this->cake_image_id;
	}
	public function setCake_image_id($cake_image_id)
	{
		$this->cake_image_id = $cake_image_id;
	}

	public function getCategory_id()
	{
		return $this->category_id;
	}
	public function setCategory_id($category_id)
	{
		$this->category_id = $category_id;
	}

	public function getImage_id()
	{
		return $this->image_id;
	}
	public function setImage_id($image_id)
	{
		$this->image_id = $image_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}