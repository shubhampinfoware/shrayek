<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="settings_master")
*/
class Settingsmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $setting_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $media_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $contact_number="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $fb_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $instagram_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $linkedin_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $snapchat_link="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $youtube_link="";
	/**
	* @ORM\Column(type="string")
	*/
	protected $lang_id="";


	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getSetting_id()
	{
		return $this->setting_id;
	}

	public function getMedia_id()
	{
		return $this->media_id;
	}
	public function setMedia_id($media_id)
	{
		$this->media_id = $media_id;
	}

	public function getContact_number()
	{
		return $this->contact_number;
	}
	public function setContact_number($contact_number)
	{
		$this->contact_number = $contact_number;
	}

	public function getFb_link()
	{
		return $this->fb_link;
	}
	public function setFb_link($fb_link)
	{
		$this->fb_link = $fb_link;
	}

	public function getInstagram_link()
	{
		return $this->instagram_link;
	}
	public function setInstagram_link($instagram_link)
	{
		$this->instagram_link = $instagram_link;
	}

	public function getLinkedin_link()
	{
		return $this->linkedin_link;
	}
	public function setLinkedin_link($linkedin_link)
	{
		$this->linkedin_link = $linkedin_link;
	}

	public function getSnapchat_link()
	{
		return $this->snapchat_link;
	}
	public function setSnapchat_link($snapchat_link)
	{
		$this->snapchat_link = $snapchat_link;
	}

	public function getYoutube_link()
	{
		return $this->youtube_link;
	}
	public function setYoutube_link($youtube_link)
	{
		$this->youtube_link = $youtube_link;
	}
	public function getLang_id()
	{
		return $this->lang_id;
	}
	public function setLang_id($lang_id)
	{
		$this->lang_id = $lang_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}