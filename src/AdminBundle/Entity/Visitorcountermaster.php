<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="visitor_counter_master")
*/
class Visitorcountermaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $visitor_counter_master;

	/**
	* @ORM\Column(type="string")
	*/
	protected $ip_address="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getVisitor_counter_master()
	{
		return $this->visitor_counter_master;
	}

	public function getIp_address()
	{
		return $this->ip_address;
	}
	public function setIp_address($ip_address)
	{
		$this->ip_address = $ip_address;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}