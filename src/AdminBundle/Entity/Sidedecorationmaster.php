<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="sidedecoration_master")
*/
class Sidedecorationmaster
{
/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $sidedecoration_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $sidedecoration_name="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $sidedecoration_image="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $sidedecoration_price="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $sidedecoration_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getSidedecoration_id()
	{
		return $this->sidedecoration_id;
	}
	public function setSidedecoration_id($sidedecoration_id)
	{
		$this->sidedecoration_id = $sidedecoration_id;
	}

	public function getSidedecoration_name()
	{
		return $this->sidedecoration_name;
	}
	public function setSidedecoration_name($sidedecoration_name)
	{
		$this->sidedecoration_name = $sidedecoration_name;
	}

	public function getSidedecoration_image()
	{
		return $this->sidedecoration_image;
	}
	public function setSidedecoration_image($sidedecoration_image)
	{
		$this->sidedecoration_image = $sidedecoration_image;
	}

	public function getSidedecoration_price()
	{
		return $this->sidedecoration_price;
	}
	public function setSidedecoration_price($sidedecoration_price)
	{
		$this->sidedecoration_price = $sidedecoration_price;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getSidedecoration_main_id()
	{
		return $this->sidedecoration_main_id;
	}
	public function setSidedecoration_main_id($sidedecoration_main_id)
	{
		$this->sidedecoration_main_id = $sidedecoration_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}