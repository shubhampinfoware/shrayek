<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="evaluation_admin_comment")
*/
class Evaluationadmincomment
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $evaluation_admin_comment_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $evaluation_feedback_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $admin_comment="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $is_deleted=0;

	public function getEvaluation_admin_comment_id()
	{
		return $this->evaluation_admin_comment_id;
	}

	public function getEvaluation_feedback_id()
	{
		return $this->evaluation_feedback_id;
	}
	public function setEvaluation_feedback_id($evaluation_feedback_id)
	{
		$this->evaluation_feedback_id = $evaluation_feedback_id;
	}

	public function getAdmin_comment()
	{
		return $this->admin_comment;
	}
	public function setAdmin_comment($admin_comment)
	{
		$this->admin_comment = $admin_comment;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}