<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="ingredients_master")
*/
class Ingredientsmaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $ingredients_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $ingredients_name="";

	
	/**
	* @ORM\Column(type="string")
	*/
	protected $status="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $ingredients_main_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getIngredients_id()
	{
		return $this->ingredients_id;
	}
	public function setIngredients_id($ingredients_id)
	{
		$this->ingredients_id = $ingredients_id;
	}

	public function getIngredients_name()
	{
		return $this->ingredients_name;
	}
	public function setIngredients_name($ingredients_name)
	{
		$this->ingredients_name = $ingredients_name;
	}

	public function getStatus()
	{
		return $this->status;
	}
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getIngredients_main_id()
	{
		return $this->ingredients_main_id;
	}
	public function setIngredients_main_id($ingredients_main_id)
	{
		$this->ingredients_main_id = $ingredients_main_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}