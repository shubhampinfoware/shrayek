<?php 
namespace AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="color_master")
*/
class Colormaster
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $color_id;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $color_main_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $color_name="";
	

	/**
	* @ORM\Column(type="string")
	*/
	protected $color_code="";

	/**
	* @ORM\Column(type="integer")
	*/
	protected $brand_id=0;

	/**
	* @ORM\Column(type="integer")
	*/
	protected $car_id=0;

	/**
	* @ORM\Column(type="string")
	*/
	protected $language_id="";

	/**
	* @ORM\Column(type="string")
	*/
	protected $is_deleted="";

	public function getColor_id()
	{
		return $this->color_id;
	}

	public function getColor_main_id()
	{
		return $this->color_main_id;
	}
	public function setColor_main_id($color_main_id)
	{
		$this->color_main_id = $color_main_id;
	}

	public function getColor_name()
	{
		return $this->color_name;
	}
	public function setColor_name($color_name)
	{
		$this->color_name = $color_name;
	}
	

	public function getColor_code()
	{
		return $this->color_code;
	}
	public function setColor_code($color_code)
	{
		$this->color_code = $color_code;
	}

	public function getBrand_id()
	{
		return $this->brand_id;
	}
	public function setBrand_id($brand_id)
	{
		$this->brand_id = $brand_id;
	}

	public function getCar_id()
	{
		return $this->car_id;
	}
	public function setCar_id($car_id)
	{
		$this->car_id = $car_id;
	}

	public function getLanguage_id()
	{
		return $this->language_id;
	}
	public function setLanguage_id($language_id)
	{
		$this->language_id = $language_id;
	}

	public function getIs_deleted()
	{
		return $this->is_deleted;
	}
	public function setIs_deleted($is_deleted)
	{
		$this->is_deleted = $is_deleted;
	}
}