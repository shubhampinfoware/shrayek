<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Commonquestion;

/**
 * @Route("/admin")
 */
class CommonQuestionController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/commonQuestion",name="common_question")
     * @Template()
     */
    public function indexAction() {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        $question_details_final = array();
        $em = $this->getDoctrine()->getManager();
        $language_list = $em->getRepository("AdminBundle:Languagemaster")->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }

        $em = $this->getDoctrine()->getManager();
        $common_question_query = "SELECT * FROM common_question WHERE is_deleted = 0 GROUP by main_common_question_id";
        $common_question_list = $this->firequery($common_question_query);
    
        if(!empty($common_question_query)){
            foreach($common_question_list as $ckey=>$cval){
                $main_com_ques_id = $cval['main_common_question_id'];
                $question_data_lang_wise = [] ;
                foreach ($language_list as $language) {
                    $question = '';
                    $answer = $status = '';
                    $em = $this->getDoctrine()->getManager();
                    $lang_question = $em->getRepository("AdminBundle:Commonquestion")->findOneBy(array('is_deleted' => 0, 'language_id' => $language->getLanguage_master_id(), 'main_common_question_id' => $main_com_ques_id));
                    if (!empty($lang_question)) {
                        $question = $lang_question->getQuestion();
                        $answer = $lang_question->getAnswer();
                        $status = $lang_question->getStatus();                       
                    }
                    $question_data_lang_wise [] = array(
                        'language_id' => $language->getLanguage_master_id(),
                        'question' => $question,
                        'answer' => $answer,
                        'status' => $status,
                        "question_id"=>$main_com_ques_id
                        );
                }
               
                if (!empty($question_data_lang_wise)) {
                    $main_com_que_id = $main_com_ques_id;
                    $question_details_final [] = array(
                        "main_common_que_id"=>$main_com_ques_id,
                        "lang_wise_data"=>$question_data_lang_wise
                    );
                }
               
            }            
        }
  
        /* echo '<pre>';
          print_r($all_category);
          exit; */
        
        
        return (array('languages' => $language_list, 'que_lang_wise' => $question_details_final));
    }

    /**
     * @Route("/addCommonQuestion/{main_que_id}",defaults={"main_que_id"=0},name="add_common_question")
     * @Template()
     */
    public function addQuestionAction($main_que_id) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        $em = $this->getDoctrine()->getManager();
        $question_details = $em->getRepository(Commonquestion :: class)->findBy(array('is_deleted' => 0, 'main_common_question_id' => $main_que_id));
        if (empty($question_details)) {
            $question_details = array();
        }
        $em = $this->getDoctrine()->getManager();
        $language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }

        return (array('question_details' => $question_details, 'languages' => $language));
    }

    /**
     * @Route("/saveQuestion",name="save_common_que")
     * @Template()
     */
    public function saveQuestionAction(Request $req) {

		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
	
        if ($req->request->get('main_que_id') === '') {
            $flag = 0;
        } else {
            $flag = 1;
        }
        $em = $this->getDoctrine()->getManager();
        $question = $em->getRepository(Commonquestion :: class)->findOneBy(array('is_deleted' => 0, 'main_common_question_id' => $req->request->get('main_que_id'), 'language_id' => $req->request->get('language_id')));

        if (empty($question)) {
            $date = date('Y-m-d');
            $question_details = new Commonquestion();
            $question_details->setQuestion($req->request->get('question'));
            $question_details->setAnswer($req->request->get('answer'));
            $question_details->setLanguage_id($req->request->get('language_id'));
            $question_details->setMain_common_question_id(0);
            $question_details->setCreated_date($date);
            $question_details->setStatus($req->request->get('active'));
            $question_details->setSort_order($req->request->get('sort_order'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($question_details);
            $em->flush();
            if ($flag === 0) {
                $main_ques_id = $question_details->getCommon_question_id();
                $flag = 1;
            } else {
                if ($req->request->get('main_que_id') !== '') {
                    $main_ques_id = $req->request->get('main_que_id');
                }
            }
            $question_details->setMain_common_question_id($main_ques_id);
            $em->flush();

            $this->get('session')->getFlashBag()->set('success_msg', 'Question saved successfully');
            return $this->redirectToRoute('add_common_question',array("main_que_id"=>$question_details->getMain_common_question_id()));
        } else {
            //	var_dump($_REQUEST);exit;	
            $date = date('Y-m-d');
            $question->setQuestion($req->request->get('question'));
            $question->setAnswer($req->request->get('answer'));
            $question->setCreated_date($date);

            $em = $this->getDoctrine()->getManager();
            $question_details = $em->getRepository(Commonquestion :: class)->findBy(array('is_deleted' => 0, 'main_common_question_id' => $req->request->get('main_que_id')));
            if (empty($question_details)) {
                $question_details = array();
            }

            foreach ($question_details as $que) {
                $que->setStatus($req->request->get('active'));
                $em->flush();
            }

            $question->setSort_order($req->request->get('sort_order'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();
            $this->get('session')->getFlashBag()->set('success_msg', 'Question updated successfully');
            return $this->redirectToRoute('add_common_question',array("main_que_id"=>$question_details->getMain_common_question_id()));
        }
    }

    /**
     * @Route("/deleteCommonQue/{main_que_id}",defaults={"main_que_id"=0},name="delete_common_question")
     * @Template()
     */
    public function deleteQuestionAction($main_que_id) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        $em = $this->getDoctrine()->getManager();
        $question_details = $em->getRepository(Commonquestion :: class)->findBy(array('is_deleted' => 0, 'main_common_question_id' => $main_que_id));
        foreach ($question_details as $que) {
            $que->setIs_deleted(1);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->set('success_msg', 'Question deleted successfully');
        return $this->redirectToRoute('common_question');
    }

    /**
     * @Route("/changeStatusQue",name="change_status")
     * @Template()
     */
    public function changeStatusQuestionAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $question_details = $em->getRepository(Commonquestion :: class)->findBy(array('is_deleted' => 0, 'main_common_question_id' => $req->request->get('main_que_id')));
        if (empty($question_details)) {
            $question_details = array();
        }

        foreach ($question_details as $que) {
            if ($que->getStatus() === 'active') {
                $que->setStatus('inactive');
            } else {
                $que->setStatus('active');
            }
            $em->flush();
        }
        exit('done');
        $this->get('session')->getFlashBag()->set('success_msg', 'Question deleted successfully');
        return $this->redirectToRoute('common_question');
    }

    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }
            return new Response(base64_encode($res));
        }
        return new Response("");
    }

    /**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyDecryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');

            $res = '';
            $string = base64_decode($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
            return new Response($res);
        }
        return new Response("");
    }

}
