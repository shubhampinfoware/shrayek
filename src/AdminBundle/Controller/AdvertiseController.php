<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Advertisemaster;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Medialibrarymaster;

/**
 * @Route("/admin")
 */
class AdvertiseController extends BaseController {

	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/advertise/{type}/{adv_type}", defaults = {"type" = "", "adv_type" = ""})
     * @Template()
     */
    public function indexAction($type, $adv_type) {
		
		$right_codes = $this->userrightsAction();
		
		$language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted" => 0));

        //$sql = "select adv.*, media.media_title, media.media_location from advertise_master adv left join media_library_master media on adv.advertise_image_id = media.media_library_master_id where adv.is_deleted = 0 {$quer_str} group by adv.main_advertise_id order by sort_order desc";
        
		$type_str = '';
		if($adv_type != ''){
			$type_str = " and adv.advertise_type in('rightcolumn')";
		} else {
			if($type == 'web'){
				$type_str = " and adv.advertise_type in('mainheader','thirdcolumn')";
			} else if($type == 'mobile'){
				$type_str = " and adv.advertise_type in('beginning_page','front_page')";
			}
		}
		
		$sql = "select adv.*, media.media_title, media.media_location from advertise_master adv left join media_library_master media on adv.advertise_image_id = media.media_library_master_id where adv.is_deleted = 0 {$type_str} group by adv.main_advertise_id order by sort_order desc";
		
        $conn = $this->getDoctrine()->getManager()->getConnection();
        $prepare = $conn->prepare($sql);
        $prepare->execute();
        $advertise_list = $prepare->fetchAll();

        //$lan_adv = "select advertise_master_id, main_advertise_id, language_id, advertise_name from advertise_master where is_deleted = 0 {$quer_str}";
        
		$lan_adv = "select advertise_master_id, main_advertise_id, language_id, advertise_name from advertise_master where is_deleted = 0";
        $prepare = $conn->prepare($lan_adv);
        $prepare->execute();
        $lan_wise_advertise = $prepare->fetchAll();

		/* echo '<pre>';
		print_r($lan_wise_advertise);
		exit; */
		
        $all_advertise = array();
        foreach ($advertise_list as $_advertise_list) {
			
			$lan_adv = array();
            foreach ($lan_wise_advertise as $_lan_wise_advertise) {
				
                if ($_advertise_list['advertise_master_id'] == $_lan_wise_advertise['advertise_master_id'] && $_advertise_list['language_id'] == $_lan_wise_advertise['language_id']) {
                    $lan_adv[] = $_lan_wise_advertise;
                }
				
				if ($_advertise_list['advertise_master_id'] == $_lan_wise_advertise['main_advertise_id'] && $_advertise_list['language_id'] != $_lan_wise_advertise['language_id']) {
                    $lan_adv[] = $_lan_wise_advertise;
                }
            }
			
            $_advertise_list['lang_wise_advertise'] = $lan_adv;
            $all_advertise[] = $_advertise_list;
        }
		
		return array(
			'type' => $type,
			'sub_adv_type' => $adv_type,
            'languages' => $language_list,
            'advertise_list' => $all_advertise,
			'right_codes' => $right_codes
        );
    }

    /**
     * @Route("/addadvertise/{type}/{adv_id}/{adv_type}", defaults = {"type" = "","adv_id" = "", "adv_type" = ""})
     * @Template()
     */
    public function addadvertiseAction($type, $adv_id, $adv_type) {
        $language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted" => 0));
		$branch_list = array();
        $advetise = array();
        if (isset($adv_id) && $adv_id != '' && $adv_id != 0) {
            $repository = $this->getDoctrine()->getRepository(Advertisemaster::class);
            $existing_advertise_list = $repository->findBy(
				array(
					'main_advertise_id' => $adv_id,
					'is_deleted' => 0
				)
			);
			
			if (!empty($existing_advertise_list)) {
				foreach($existing_advertise_list as $existing_advertise){
					$repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
					$media_file = $repository->find($existing_advertise->getAdvertise_image_id());

					if (!empty($media_file)) {
						$media_url = $this->container->getParameter('live_path') . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();
					} else {
						$media_url = $this->container->getParameter('live_path') . 'bundles/Resource/default.png';
					}
					
#get branches of restaurant selected
						$branch_list = array();
						$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and main_restaurant_id = '".$existing_advertise->getRestaurant_id()."' group by main_branch_master_id";
						$branch_list_arr = $this->firequery($branch_query);
						if (!empty($branch_list_arr)) {
							foreach ($branch_list_arr as $cakey1 => $cval1) {
								$branch_list[] = array(
									"main_branch_master_id" => $cval1['main_branch_master_id'],
									"branch_name" => $cval1['branch_name'],
								);
							}
						}					
					
					$advetise[] = array(
						'advertise_master_id' => $existing_advertise->getAdvertise_master_id(),
						'advertise_name' => $existing_advertise->getAdvertise_name(),
						'language_id' => $existing_advertise->getLanguage_id(),
						'advertise_image_id' => $existing_advertise->getAdvertise_image_id(),
						'advertise_image_link' => $existing_advertise->getAdvertise_image_link(),
						'advertiser_name' => $existing_advertise->getName_of_advertiser(),
						'advertise_type' => $existing_advertise->getAdvertise_type(),
						'banner_advertise_type' => $existing_advertise->getBanner_advertise_type(),
						'website_mobile_type' => $existing_advertise->getWebsite_mobile_type(),
						'restaurant_id' => $existing_advertise->getRestaurant_id(),
						'main_branch_id' => $existing_advertise->getMain_branch_id(),
						'main_advertise_id' => $existing_advertise->getMain_advertise_id(),
						'start_date' => $existing_advertise->getStart_date(),
						'end_date' => $existing_advertise->getEnd_date(),
						'status' => $existing_advertise->getStatus(),
						'sort_order' => $existing_advertise->getSort_order(),
						'is_deleted' => $existing_advertise->getIs_deleted(),
						'media_url' => $media_url,
						'show_in_mobile' => $existing_advertise->getShow_in_mobile(),
						'show_in_website' => $existing_advertise->getShow_in_website(),
						
					);
				}
            } else {
                $advetise = array();
            }
        }
		
		## restaurant list
        $restaurant_list = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findBy(
                array(
                    'status' => 'active',
                    'is_deleted' => 0
                )
        );
//		echo"<pre>";print_r($advetise);exit;
		return array(
			"type" => $type,
			"sub_adv_type" => $adv_type,
            "languages" => $language_list,
            "restaurant_list" => $restaurant_list,
            "advetise" => $advetise,
			'branch_list'=>$branch_list
        );
    }

    /**
     * @Route("/addadvertisedb/{type}/{adv_type}", defaults={"adv_type"=""})
     * @Template()
     */
    public function addadvertisedbAction($type, $adv_type, Request $request) {
		
		/* echo '<pre>';
		print_r($request->request->all());
		exit; */
		
		if ($request->request->all()) {
			
			$em = $this->getDoctrine()->getManager();

            $image = $_FILES['image']['name'];
            if (isset($image) && $image != '') {
                $upload_dir = $this->container->getParameter('upload_dir1');
				$location = '/design/uploads/advertise';

                $mediatype = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(Mediatype::class)
                        ->findOneBy(array(
                    'media_type_name' => 'Image',
                    'is_deleted' => 0)
                );
                $allowedExts = explode(',', $mediatype->getMedia_type_allowed());
                $temp = explode('.', $_FILES['image']['name']);
                $extension = end($temp);

                $media_id = $this->mediaupload($_FILES['image'], $upload_dir, $location, $mediatype->getMedia_type_id());
            } else {
				if(array_key_exists('adv_image_hidden', $request->request->all())){
					$media_id = $request->get('adv_image_hidden');
				} else {
					$this->get('session')->getFlashBag()->set('error_msg', 'Please upload image for advertise');
					$referer = $request->headers->get('referer');
					return $this->redirect($referer);
				}
			}
			
			$language_id = $request->get('language_id');
			$restaurant_id = $request->get("restaurant_id");
			$branch_id = $request->get("branch_id");
			
            if ($request->get('language_id') && $request->get('advertise_id')) {
                /* for edit */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SUB37", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
				/* echo '<pre>';
				print_r($request->request->all());
				exit; */
				
                $advertise = $em->getRepository(Advertisemaster::class)->find($request->get('advertise_id'));

                $advertise->setAdvertise_name($request->get('advertise_name'));
                if (isset($media_id)) {
                    $advertise->setAdvertise_image_id($media_id);
                }

				if(array_key_exists('banner_sub_type', $request->request->all())){
					if ($request->get('banner_sub_type') == 'website') {
						$advertise->setBanner_advertise_type('website');
						$advertise->setAdvertise_image_link($request->get('image_link'));
					}

					if ($request->get('banner_sub_type') == 'restaurant') {
						//exit($request->get('restaurant_id'));
						$advertise->setBanner_advertise_type('restaurant');
						$advertise->setRestaurant_id($restaurant_id);
						$advertise->setMain|_branch_id($branch_id);
					}
				} else {
					$advertise->setBanner_advertise_type('website');
					$advertise->setAdvertise_image_link('');
				}

                $advertise->setName_of_advertiser($request->get('name_of_advertiser'));
				if($type == 'web'){
					$advertise->setWebsite_mobile_type('website');
				} else {
					$advertise->setWebsite_mobile_type('mobile');
				}
                $advertise->setAdvertise_type($request->get('advertise_type'));
                $advertise->setStart_date(date('Y-m-d H:i:s' ,strtotime($request->get('start_date'))));
                $advertise->setEnd_date(date('Y-m-d H:i:s' ,strtotime($request->get('end_date'))));
                $advertise->setStatus($request->get('status'));
                //$advertise->setMain_advertise_id($request->get('main_adverise_id'));
				if($request->get('sort_order') == ''){
					$advertise->setSort_order(0);
 				} else {
					$advertise->setSort_order($request->get('sort_order'));
				}
				
				if(!array_key_exists('show_in_other_view', $request->request->all())){
					$advertise->setShow_in_mobile('beginning_page');
					$advertise->setShow_in_website('mainheader');
				} else {
					if($request->get('other_view_type_hidden') == 'mobile'){
						$advertise->setShow_in_mobile($request->get('other_view_adv_type'));
						$advertise->setShow_in_website('mainheader');
					} else {
						$advertise->setShow_in_website($request->get('other_view_adv_type'));
						$advertise->setShow_in_mobile('beginning_page');
					}
				}
				
                $em->flush();
				$adv_id = $advertise->getMain_advertise_id();
                $this->get('session')->getFlashBag()->set('success_msg', "Advertise updated successfully");
            } else {
                /* for insert */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SAB36", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
                $language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted" => 0));
                $main_id = 0 ;
				
				$show_in_other = false;
				if(array_key_exists('show_in_other_view', $request->request->all())){
					$show_in_other = true;
				}
				
                if( $request->get('main_adverise_id') != 0 ){
					
					$main_id =  $request->get('main_adverise_id') ;                    
                    $advertise = new Advertisemaster();
                    $advertise->setAdvertise_name($request->get('advertise_name'));
                    if (isset($media_id)) {
                        $advertise->setAdvertise_image_id($media_id);
                    }
                    $advertise->setLanguage_id($request->get('language_master_id'));                        
                    //$advertise->setAdvertise_image_link($request->get('image_link'));
					$advertise->setName_of_advertiser($request->get('name_of_advertiser'));
                    $advertise->setAdvertise_type($request->get('advertise_type'));
					if($type == 'web'){
						$advertise->setWebsite_mobile_type('website');
					} else {
						$advertise->setWebsite_mobile_type('mobile');
					}
					$advertise->setStart_date(date('Y-m-d H:i:s' ,strtotime($request->get('start_date'))));
					$advertise->setEnd_date(date('Y-m-d H:i:s' ,strtotime($request->get('end_date'))));
                    $advertise->setStatus($request->get('status'));
                    if($request->get('sort_order') == ''){
						$advertise->setSort_order(0);
					}
                    $advertise->setMain_advertise_id($main_id);
                    $advertise->setIs_deleted(0);
					if(array_key_exists('banner_sub_type', $request->request->all())){
						if ($request->get('banner_sub_type') == 'website') {
							$advertise->setBanner_advertise_type('website');
							if(array_key_exists('image_link', $request->request->all())){
								$advertise->setAdvertise_image_link($request->get('image_link'));
							} else {
								$advertise->setAdvertise_image_link('-');
							}
						}
						if ($request->get('banner_sub_type') == 'restaurant') {
							$advertise->setBanner_advertise_type('restaurant');
							$advertise->setRestaurant_id($restaurant_id);
							$advertise->setMain_branch_id($branch_id);
						}
					} else {
						$advertise->setBanner_advertise_type('website');
						$advertise->setAdvertise_image_link('-');
					}
					
					if($show_in_other){
						if($request->get('other_view_type_hidden') == 'mobile'){
							$advertise->setShow_in_mobile($request->get('other_view_adv_type'));
							$advertise->setShow_in_website('mainheader');
						} else {
							$advertise->setShow_in_website($request->get('other_view_adv_type'));
							$advertise->setShow_in_mobile('beginning_page');
						}
					} else {
						$advertise->setShow_in_mobile('beginning_page');
						$advertise->setShow_in_website('mainheader');
					}
					
                    $em->persist($advertise);
                    $em->flush();
					$adv_id = $advertise->getMain_advertise_id();
                }
                else{
					
					$adv_main_id = 0;
					$othr_adv_main_id = 0;
//                    foreach($language_list as $langkey=>$langval){
                        $advertise = new Advertisemaster();
                        $advertise->setAdvertise_name($request->get('advertise_name'));
                        if (isset($media_id)) {
							$advertise->setAdvertise_image_id($media_id);
						} else {
							$advertise->setAdvertise_image_id(0);
						}
                        $advertise->setLanguage_id($request->get('language_master_id'));
//                        $advertise->setLanguage_id($langval->getLanguage_master_id());
                        //$advertise->setAdvertise_image_link($request->get('image_link'));
                        $advertise->setAdvertise_type($request->get('advertise_type'));
						if($type == 'web'){
							$advertise->setWebsite_mobile_type('website');
						} else {
							$advertise->setWebsite_mobile_type('mobile');
						}
						$advertise->setName_of_advertiser($request->get('name_of_advertiser'));
						$advertise->setStart_date(date('Y-m-d H:i:s' ,strtotime($request->get('start_date'))));
						$advertise->setEnd_date(date('Y-m-d H:i:s' ,strtotime($request->get('end_date'))));
                        $advertise->setStatus($request->get('status'));
                        if($request->get('sort_order') == ''){
							$advertise->setSort_order(0);
						}
                        $advertise->setSort_order($request->get('sort_order'));
						$advertise->setIs_deleted(0);
						
						if(array_key_exists('banner_sub_type', $request->request->all())){
							if ($request->get('banner_sub_type') == 'website') {
								$advertise->setBanner_advertise_type('website');
								if(array_key_exists('image_link', $request->request->all())){
									$advertise->setAdvertise_image_link($request->get('image_link'));
								} else {
									$advertise->setAdvertise_image_link('');
								}
							}

							if ($request->get('banner_sub_type') == 'restaurant') {
								$advertise->setBanner_advertise_type('restaurant');
								$advertise->setRestaurant_id($restaurant_id);
								$advertise->setMain_branch_id($branch_id);
							}
						} else {
							$advertise->setBanner_advertise_type('website');
							$advertise->setAdvertise_image_link('');
						}
						
						if($show_in_other){
							if($request->get('other_view_type_hidden') == 'mobile'){
								$advertise->setShow_in_mobile($request->get('other_view_adv_type'));
								$advertise->setShow_in_website('');
							} else {
								$advertise->setShow_in_website($request->get('other_view_adv_type'));
								$advertise->setShow_in_mobile('');
							}
						} else {
							$advertise->setShow_in_mobile('beginning_page');
							$advertise->setShow_in_website('mainheader');
						}
						
                        $em->persist($advertise);
                        $em->flush();

                 //       if ($langkey == 0) {                       
                           $main_id = $advertise->getAdvertise_master_id() ;
                 //       } 
                        $advertise->setMain_advertise_id($main_id);
                        $em->flush();
						
						$adv_id = $advertise->getMain_advertise_id();
                //    }
                }
                
                $this->get('session')->getFlashBag()->set('success_msg', "Advertise inserted successfully");
            }
        } else {
            $this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
        }

		if($adv_type != ''){
			$param = array('type' => $type, 'adv_id' => $adv_id,'adv_type' => $adv_type);
		} else {
			if(isset($adv_id)){
				$param = array('type' => $type, 'adv_id' => $adv_id);
			} else {
				EXIT('-');
				$param = array('type' => $type);
			}
		}
		
		$url = $this->generateUrl('admin_advertise_addadvertise', $param);
		return $this->redirect($url);
		
		/* $referer = $request->headers->get('referer');
		return $this->redirect($referer);		 */
    }

    /**
     * @Route("/deleteadvertise/{type}/{adv_id}", defaults = {"type" = "","adv_id" = ""})
     * @Template()
     */
    public function deleteadvertiseAction($type, $adv_id, Request $request) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDB38", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        if (isset($adv_id) && $adv_id != '') {
			
			$flash_str = '';
			if($type == 'secondbox'){
				$flash_str = 'Advertise';
			} else if($type == 'mainheader'){
				$flash_str = 'Banner';
			}
			
            $em = $this->getDoctrine()->getManager();
            $advertise = $em->getRepository(Advertisemaster::class)->findBy(
				array(
					'main_advertise_id' => $adv_id,
					'is_deleted' => 0
				)
			);

            if (!empty($advertise)) {
				
				foreach($advertise as $_advertise){
					$_advertise->setIs_deleted(1);
					$em->flush();
				}

				$this->get('session')->getFlashBag()->set('success_msg', "{$flash_str} deleted successfully");
            } else {
                $this->get('session')->getFlashBag()->get('error_msg', "{$flash_str} not found");
            }
        } else {
            $this->get('session')->getFlashBag()->get('error_msg', 'Something went wrong');
        }

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @Route("/ajaxupdatestatus")
     */
    public function ajaxupdatestatusAction(Request $request) {

		$status = $request->get('status');
        if ($request->get('cat_id')) {
            $em = $this->getDoctrine()->getManager();
            $advertise_all = $em->getRepository(Advertisemaster::class)->findBy(array('main_advertise_id'=>$request->get('cat_id')));
			foreach($advertise_all as $advertise){
				if ($status == 'true') {
					$advertise->setStatus('active');
				} else {
					$advertise->setStatus('inactive');
				}
			}
            $em->flush();
            echo 'true';
            exit;
        }
        echo 'false';
        exit;
    }

}
