<?php
namespace Main\AdminBundle\Controller;
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AdminBundle\Entity\Siteprivacy;
use AdminBundle\Entity\Languagemaster;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("/admin")
*/
class SitemembersController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
    * @Route("/sitemembers")
    * @Template()
    */
    public function indexAction(){
		// site members highlighted evaluations
		$em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
		
		$hilighted = "SELECT count(ef.user_id) as ecount,ef.*,um.user_master_id,um.user_firstname,um.user_lastname,media_library_master.media_location , media_library_master.media_name from evaluation_feedback as ef 
		left join user_master as um on um.user_master_id=ef.user_id 
		left join media_library_master ON um.user_image = media_library_master.media_library_master_id 
		where ef.is_deleted=0 and ef.is_featured='yes' group by ef.user_id order by count(ef.user_id) desc ";
       
        $stmt = $con->prepare($hilighted);
        $stmt->execute();
        $hilighted_evalution = $stmt->fetchAll();
        $hilighted_evalutions = [] ;
		
        if(!empty($hilighted_evalution)){
			$live_path = $this->container->getParameter('live_path');
			
            foreach($hilighted_evalution  as $key=>$val){
				if($val['media_location']!=='' && $val['media_name']!=''){
					$user_image=$live_path . $val['media_location'] . "/".$val['media_name'];
				} else {
					$user_image=$live_path.'/bundles/Resource/default.png';
				}
                
				$comment = '';
				if(strlen($val['comments']) <= 50){
					$comment = $val['comments'];
				} else {
					$comment = substr($val['comments'], 0, 50).'...';
				}
				
				$hilighted_evalutions[] = array(
                    "evaluation_feedback_id" => $val['evaluation_feedback_id'],
                    "comments" => $comment,
                    "user_id" => $val['user_id'],
                    "userimage" => $user_image,
                    "username" => $val['user_firstname'].' '.$val['user_lastname'],
					"status" => $val['status'],
					"created_at" => $val['created_datetime'],
					"max_counter" => $val['ecount']
                );
            }
        }
		
		return array(
			'hilighted_evalutions' => $hilighted_evalutions
		);
    }
	
	public function getMediaUrl($media_id){
		$media = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster')->find($media_id);
		
		if(!empty($media)){
			$media_url = $live_path.$media->getMedia_location().'/'.$media->getMedia_name();
		} else {
			$media_url = $live_path.'/bundles/Resource/default.png';
		}
		return $media_url;
	}
	
	/**
    * @Route("/sitemembers/deleteevaluation/{user_id}", defaults={"user_id" : ""})
    * @Template()
    */
    public function deleteEvaluationAction($user_id, Request $request){
		if(isset($user_id) && $user_id != ''){
			
			$em = $this->getDoctrine()->getManager();
			
			$evaluations = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedback')->findBy(array('user_id' => $user_id, 'is_deleted' => 0));
			
			if(!empty($evaluations)){
				
				foreach($evaluations as $_evaluation){
					
					$evaluation = $em->getRepository('AdminBundle:Evaluationfeedback')->find($_evaluation->getEvaluation_feedback_id());
					
					$evaluation->setIs_deleted(1);
					$em->flush();
				}
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Evaluation removed successfully');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
}
