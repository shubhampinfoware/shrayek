<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Communitychat;

/**
* @Route("/admin")
*/
class AsktheCommunityController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/asktheCommunityAdmin",name="admin_askthe_community_index")
     * @Template()
     */
    public function indexAction()
    {
		$right_codes = $this->userrightsAction();
		$sql_get_question = "select chat.*,chat.created_datetime as created_datetime,user.username from community_chat chat join user_master user on user.user_master_id=chat.user_id where chat.is_deleted=0 and user.is_deleted=0 and parent_id=0 order by chat.created_datetime DESC";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($sql_get_question);
		$stmt->execute();
		$question = $stmt->fetchAll();
//		print_r($question);
		return (array('question'=>$question, 'right_codes' => $right_codes));
	} 
	
    /**
     * @Route("/viewAnswerAdmin/{question_id}",defaults={"question_id"=""},name="admin_view_answer")
     * @Template()
     */
    public function viewAnswerAdminAction($question_id)
    {
		$right_codes = $this->userrightsAction();
	   $chat_data = array();	
	   $get_chat_que_sql = "select chat.* ,user.username,user.user_image,media.* from community_chat chat 
							join user_master user on chat.user_id = user.user_master_id 
							left join media_library_master media on media.media_library_master_id = user.user_image	where parent_id= 0 and user.is_deleted=0 and chat.is_deleted=0 and 
							community_chat_id='".$question_id."'";		
       $em = $this->getDoctrine()->getManager(); 
	   $con = $em->getConnection();
	   $stmt = $con->prepare($get_chat_que_sql);
	   $stmt->execute();
	   $chat_main_que = $stmt->fetchAll();	   
	   if($chat_main_que){
			foreach($chat_main_que as $chat_que){
			   $get_chat_ans_sql = "select chat.* ,user.username,user.user_image,media.* 
									from community_chat chat 
									join user_master user on chat.user_id = user.user_master_id 
									left join media_library_master media on media.media_library_master_id = user.user_image where parent_id='".$chat_que['community_chat_id']."' and user.is_deleted=0 and chat.is_deleted=0";		
			   $em = $this->getDoctrine()->getManager(); 
			   $con = $em->getConnection();
			   $stmt = $con->prepare($get_chat_ans_sql);
			   $stmt->execute();
			   $chat_ans = $stmt->fetchAll();
			   $chat_details [] = array(
									'questio_id'=>$chat_que['community_chat_id'],
									'que_asked_by'=>$chat_que['username'],
									'que_ask_date'=>$chat_que['created_datetime'],
									'media_title_asked_by'=>$chat_que['media_title'],
									'media_location_asked_by'=>$chat_que['media_location'],
									'main_que'=>$chat_que['community_chat'],
									'answer'=>$chat_ans,
									'parent_id'=>$chat_que['community_chat_id']
									);
			}
		}
		
		/* echo "<pre>";
		print_r($chat_details);
		exit; */
		
		return array(
			'question_id' => $question_id,
			'chat_details'=>$chat_details,
			'right_codes' => $right_codes
		);	
	} 	

	/**
     * @Route("/saveReplyAdmin",name="admin_save_reply")
     * @Template()
     */	
	public function saveReplyAction(Request $req)
	{

		$check_que = "select community_chat_id from community_chat where parent_id=0 and community_chat_id='".$req->request->get('question_id')."'";
		$em = $this->getDoctrine()->getManager(); 
		$con = $em->getConnection();
		$stmt = $con->prepare($check_que);
		$stmt->execute();
		   $chat_que = $stmt->fetchAll();
		   if(!empty($chat_que))
			{
				$Communitychat_new = new Communitychat();
				$Communitychat_new->setCommunity_chat($req->request->get('answer'));  
				$Communitychat_new->setParent_id($req->request->get('parent_id'));
				$Communitychat_new->setCreated_datetime(date('Y-m-d'));
				$Communitychat_new->setUser_id(1);
				$em = $this->getDoctrine()->getManager(); 
				$em->persist($Communitychat_new);
				$em->flush();
				$this->get('session')->getFlashBag()->set('success_msg', 'Your  Reply saved successfully');
				return $this->redirectToRoute('admin_view_answer',array('question_id'=>$req->request->get('question_id'))); 
			} 
	} 

	/**
     * @Route("/update-chat")
     * @Template()
     */	
	public function updatechatAction(Request $request)
	{
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SECC27", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            $data = array(
				'success' => '0',
				'message' => 'failed'
			);
			echo json_encode($data);exit;
        }
		/* end: check for access */
		
		$success = false;
		$chat_id = $request->get('chat_id');
		$chat = $request->get('chat');
		if(isset($chat_id)){
			$entity = $this->getDoctrine()->getManager();
			$community_chat = $entity->getRepository('AdminBundle:Communitychat')->findOneBy([
				'community_chat_id' => $chat_id,
				'is_deleted' => 0
			]);
			
			if(!empty($community_chat)){
				$community_chat->setCommunity_chat($chat);
				$entity->flush();
				$success = true;
			}
		}
		
		$data = array(
			'success' => $success,
			'message' => 'Updated Successfully'
		);
		
		echo json_encode($data);exit;
	}
	
	/**
     * @Route("/deleteQuestionAdmin/{question_id}",name="admin_delete_question")
     * @Template()
     */	
	public function deleteQuestionAction($question_id)
	{
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDCQ28", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$em = $this->getDoctrine()->getManager();
		$question = $em->getRepository(Communitychat :: class)->findOneBy(array('community_chat_id'=>$question_id));
		if($question){
			$question->setIs_deleted(1);
			$em->flush();
			return $this->redirectToRoute('admin_askthe_community_index');
		}	
	} 
	
	/**
     * @Route("/delete-chats")
     * @Template()
     */	
	public function deletechatsAction(Request $request)
	{
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDCC29", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            $data = array(
				'success' => '0',
				'message' => 'failed'
			);
			echo json_encode($data);exit;
        }
		/* end: check for access */
		
		$success = false;
		$chat_ids = $request->get('chat_ids');
		if(isset($chat_ids)){
			$chatIdArray = explode(',',$chat_ids);
			
			if(!empty($chatIdArray)){
				foreach($chatIdArray as $_chatId){
					$em = $this->getDoctrine()->getManager();
					$chat = $em->getRepository(Communitychat :: class)->findOneBy(array('community_chat_id'=>$_chatId));
					if($chat){
						$chat->setIs_deleted(1);
						$em->flush();
						$success = true;
					}
				}
			}
		}
		
		$data = array(
			'success' => $success,
			'message' => 'Removed Successfully'
		);
		
		echo json_encode($data);exit;
	}
	
	/**
     * @Route("/remove-parent-chat")
     * @Template()
     */	
	public function removeparentchatAction(Request $request)
	{
		$success = false;
		$chat_id = $request->get('chat_id');
		if(isset($chat_id)){
			$em = $this->getDoctrine()->getManager();
			$chat_list = $em->getRepository('AdminBundle:Communitychat')->findBy(
				array(
					'parent_id' => $chat_id,
					'is_deleted' => 0
				)
			);
			
			if(!empty($chat_list)){
				foreach($chat_list as $_chat){
					$_chat->setIs_deleted(1);
					$em->flush();
					$success = true;
				}
			}
			
			$entity = $this->getDoctrine()->getManager();
			$remove_parent = $entity->getRepository('AdminBundle:Communitychat')->findOneBy([
				'community_chat_id' => $chat_id,
				'is_deleted' => 0
			]);
			if(!empty($remove_parent)){
				$remove_parent->setIs_deleted(1);
				$entity->flush();
			}
		}
		
		$data = array(
			'success' => $success,
			'message' => 'Removed Successfully'
		);
		
		echo json_encode($data);exit;
	}
	
	/**
     * @Route("/delete-bulk-chat")
     * @Template()
     */	
	public function deletebulkchatAction(Request $request)
	{
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDCQ28", $right_codes);
		if ($rights_search != 1) {
			$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
			return new Response(json_encode(array('success' => '0', 'message' => 'failed')));
		}
		/* end: check for access */
		
		$success = false;
		$message = 'Not removed anything';
		$chat_id_list_str = $request->get('chat_ids');
		if(isset($chat_id_list_str)){
			
			$chat_id_list = explode(',', $chat_id_list_str);
			if(!empty($chat_id_list)){
				
				foreach($chat_id_list as $chat_id){
					$em = $this->getDoctrine()->getManager();
					$chat_list = $em->getRepository('AdminBundle:Communitychat')->findBy(
						array(
							'parent_id' => $chat_id,
							'is_deleted' => 0
						)
					);
					
					if(!empty($chat_list)){
						foreach($chat_list as $_chat){
							$_chat->setIs_deleted(1);
							$em->flush();
							$success = true;
							$message = 'Removed Successfully';
						}
					}
					
					$entity = $this->getDoctrine()->getManager();
					$remove_parent = $entity->getRepository('AdminBundle:Communitychat')->findOneBy([
						'community_chat_id' => $chat_id,
						'is_deleted' => 0
					]);
					if(!empty($remove_parent)){
						$remove_parent->setIs_deleted(1);
						$entity->flush();
					}
				}
			}
		}
		
		$data = array(
			'success' => $success,
			'message' => $message
		);
		
		echo json_encode($data);exit;
	}
	
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
}
