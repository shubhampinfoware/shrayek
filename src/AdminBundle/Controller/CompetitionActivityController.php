<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Competitionactivityrelation;
use AdminBundle\Entity\Competitionmaster;
use AdminBundle\Entity\Competitionexclusionmaster;
use AdminBundle\Entity\Competitionextrapoints;
use AdminBundle\Entity\Competitioncategoryrelation;

/**
 * @Route("/admin")
 */
class CompetitionActivityController extends BaseController {

    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/activityCompetitionList")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        /* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
        /* end: check for access */
        $today = date('Y-m-d H:i:s');

        $sql_update_to_inactive_sql = "UPDATE competition_master  SET status = 'inactive' where end_date <= '$today' and is_deleted = '0'";
        $stmt = $em->getConnection()->prepare($sql_update_to_inactive_sql);
        $stmt->execute();

        $language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));

        $competitions = $em->getRepository(Competitionmaster :: class)->findBy(array('is_deleted' => 0));

//		echo "<pre>";print_r($competitions);exit;
        return (array('languages' => $language, 'competitions' => $competitions));
    }

    /**
     * @Route("/addActivityCompetition/{competition_id}/{tab}",defaults={"competition_id":"0","tab"="lang"})
     * @Template()
     */
    public function addactivitycompetitionAction($competition_id, $tab, Request $req) {

        $extar_points_category = 0 ;
        $rule_book = '';
        $live_path = $this->container->getParameter('live_path');

        $em = $this->getDoctrine()->getManager();
        $language_master = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));

        $_sql = "SELECT * from category_master where is_deleted = 0 and category_status = 'active' group by main_category_id";
        $category_master = $this->firequery($_sql);

        $categoryList = null;
        if (!empty($category_master)) {
            foreach ($category_master as $_cat) {
                $sql = "SELECT * from category_master where is_deleted = 0 and category_status = 'active' and main_category_id = {$_cat['main_category_id']}";
                $category = $this->firequery($sql);

                $cat_name = '';
                if (!empty($category)) {
                    foreach ($category as $_category) {
                        $cat_name[] = $_category['category_name'];
                    }
                    $cat_name = implode(' / ', $cat_name);
                }
                $categoryList[] = array(
                    'cat_name' => $cat_name,
                    'main_category_id' => $_cat['main_category_id']
                );
                ;
            }
        }

        $sql = "SELECT cr.*, c.category_name from category_master c, competition_category_relation cr where c.is_deleted = 0 and cr.is_deleted = 0 and category_status = 'active' and c.main_category_id = cr.category_id and cr.competition_id = $competition_id and cr.type = 'exclude' group by cr.category_id";
        $categoryRelation = $this->firequery($sql);

        $categoryRelationList = null;
        if (!empty($categoryRelation)) {
            foreach ($categoryRelation as $_rel) {
                $categoryRelationList[] = $_rel['category_id'];
            }
        }

        $sql = "SELECT cr.*, c.category_name from category_master c, competition_category_relation cr where c.is_deleted = 0 and cr.is_deleted = 0 and category_status = 'active' and c.main_category_id = cr.category_id and cr.competition_id = $competition_id and cr.type = 'include' group by cr.category_id";
        $categoryRelationInclude = $this->firequery($sql);

        $categoryRelationListIncludes = null;
        $categoryRelationListIncludesList = null;
        if (!empty($categoryRelationInclude)) {
            foreach ($categoryRelationInclude as $_rel) {
                $categoryRelationListIncludesList [] = array(
                    "competition_category_relation_id" => $_rel['competition_category_relation_id'],
                    "name" => $_rel['category_name'],
                    "extra_points" => $_rel['extra_points']
                );
                $categoryRelationListIncludes[] = $_rel['category_id'];
            }
        }

//        echo"<pre>";print_r($categoryRelationListIncludesList);exit;


        $restaurants = array();

        $sql = "select main_restaurant_id from restaurant_master where status='active' and is_deleted=0 group by main_restaurant_id";
        $con = $em->getConnection();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $restaurants = $stmt->fetchAll();

        if (!empty($restaurants) && !empty($language_master)) {
            foreach ($restaurants as $key => $value) {
                $lang_wise = array();

                foreach ($language_master as $lang) {
                    $sql = "select * from restaurant_master where status='active' and is_deleted=0 and main_restaurant_id = " . $value['main_restaurant_id'] . " and language_id = " . $lang->getLanguage_master_id();
                    $con = $em->getConnection();
                    $stmt = $con->prepare($sql);
                    $stmt->execute();
                    $restaurants_lang = $stmt->fetchAll();
                    if (!empty($restaurants_lang)) {
                        $lang_wise [] = array('lang_id' => $lang->getLanguage_master_id(), 'name' => $restaurants_lang[0]['restaurant_name']);
                    } else {

                        $lang_wise [] = array('lang_id' => $lang->getLanguage_master_id(), 'name' => '-');
                    }
                }
                $restaurants[$key]['lang_wise'] = $lang_wise;
            }
        }

        $activity_master = null;
        $competition_master = null;
        $check_exclution_rest_arr = null;
        $check_extra_rest_arr = null;
        $check_extra_rest_list = null;
        $extraPoints = 0;

        if (!empty($competition_id)) {

            $competition_master = $em->getRepository("AdminBundle:Competitionmaster")->findOneBy(["main_competition_id" => $competition_id, 'is_deleted' => 0]);

            if ($competition_master) {

                $rule_book = $this->getImage($competition_master->getRule_book(), $live_path);

                $sql_activity = "select * from activity_master 
							LEFT JOIN competition_activity_relation ON activity_id = main_activity_id and competition_activity_relation.is_deleted = 0 and competition_activity_relation.competition_id = '$competition_id' 
				where activity_master.is_deleted = '0' and activity_master.status = 'active'  ";
                $activity_master = $this->firequery($sql_activity);

                $check_exclution_rest_sql = "select restaurant_id from competition_exclusion_master 
										where is_deleted = '0' and competition_id = '$competition_id'  ";
                $check_exclution_rest = $this->firequery($check_exclution_rest_sql);
                if (!empty($check_exclution_rest)) {
                    foreach ($check_exclution_rest as $_check_exclution_rest) {
                        $check_exclution_rest_arr[] = $_check_exclution_rest['restaurant_id'];
                    }
                }

                $check_extrapoints_rest_sql = "select rest_id,extra_points,competition_extra_points_id from competition_extra_points 
				where is_deleted = '0' and comp_id = '$competition_id'  ";
                $check_extrapoints = $this->firequery($check_extrapoints_rest_sql);
                if (!empty($check_extrapoints)) {
                    $extraPoints = $check_extrapoints[0]['extra_points'];
                    foreach ($check_extrapoints as $_check_extrapoints) {
                        
                        $lang_wise = '';

                        $check_extra_rest_arr[] = $_check_extrapoints['rest_id'];
                        
                        if($language_master){
                            foreach ($language_master as $lang) {
                                $sql = "select * from restaurant_master where status='active' and is_deleted=0 and main_restaurant_id = " . $_check_extrapoints['rest_id'] . " and language_id = " . $lang->getLanguage_master_id();
                                $con = $em->getConnection();
                                $stmt = $con->prepare($sql);
                                $stmt->execute();
                                $restaurants_lang = $stmt->fetchAll();
                                if (!empty($restaurants_lang)) {
                                    $lang_wise .=  $restaurants_lang[0]['restaurant_name']."/";
                                }
                            }
                        }

                        $check_extra_rest_list [] = array(
                            'name' => $lang_wise,
                            'extra_points' => $_check_extrapoints['extra_points'],
                            'competition_extra_points_id' => $_check_extrapoints['competition_extra_points_id']
                        );
                        

                    }
                }
            }
        }

        return array("main_comp_id" => $competition_id,
            "competition_master" => $competition_master,
            "activity_master" => $activity_master,
            "restaurants" => $restaurants,
            "check_exclution_rest_arr" => $check_exclution_rest_arr,
            "check_extra_rest_arr" => $check_extra_rest_arr,
            "extraPoints" => $extraPoints,
            "check_extra_rest_list" => $check_extra_rest_list,
            "rule_book" => $rule_book,
            "tab" => $tab,
            "categoryList" => $categoryList,
            "categoryRelationList" => $categoryRelationList,
            "categoryRelationListIncludes" => $categoryRelationListIncludes,
            "categoryRelationListIncludesList" => $categoryRelationListIncludesList,
        );
    }

    /**
     * @Route("/saveExtraPoints")
     * @Template()
     */
    public function saveextrapointsAction(Request $req) {
        $em = $this->getDoctrine()->getManager();
        $rest_id_excludes = $req->request->get('rest_id_excludes');
        $rest_id_includes = $req->request->get('rest_id_includes');
        $extar_points = $req->request->get('extar_points');

        $main_comp_id = $req->request->get('main_comp_id');
        $cat_id_list = $req->request->get('cat_id');
        $cat_id_include = $req->request->get('cat_id_include');
        $extra_point_type = $req->request->get('extra_point_type');

//       echo "<pre>";print_r($_REQUEST);exit;
            $competition = $em->getRepository("AdminBundle:Competitionmaster")->findOneBy([
                'main_competition_id' => $main_comp_id,
                'language_id' => 1
            ]);

            if (!empty($competition)) {
                $competition->setExtra_point_type($extra_point_type);
                $competition->setExtra_point_value($extar_points);
                $em->flush();
            }

            #deleteOldEntrys
            if($extra_point_type == 'include'){
/*
                if ($oldCompetitionextrapoints) {
                    foreach ($oldCompetitionextrapoints as $_oldCompetitionextrapoints) {
                        $_oldCompetitionextrapoints->setIs_deleted(1);
                        $em->flush();
                    }
                }
*/
                if(!empty($rest_id_includes)){

                    foreach ($rest_id_includes as $_rest_ids) {

                        $is_new_entry = false;
    
                        $oldCompetitionextrapoints = $em->getRepository("AdminBundle:Competitionextrapoints")->findOneBy(["comp_id" => $main_comp_id,"rest_id"=>$_rest_ids, "is_deleted" => 0]);
    
                        if($oldCompetitionextrapoints){
                            $newCompetitionextrapoints = $oldCompetitionextrapoints;
                        }else{
                            $newCompetitionextrapoints = new Competitionextrapoints();
                            $is_new_entry = true;
                        }
                        
                        $newCompetitionextrapoints->setRest_id($_rest_ids);
                        $newCompetitionextrapoints->setComp_id($main_comp_id);
                        $newCompetitionextrapoints->setExtra_points($extar_points);
                        $newCompetitionextrapoints->setCreated_datetime(date('Y-m-d H:i:s'));
                        $newCompetitionextrapoints->setis_deleted(0);
                        if($is_new_entry){
                            $em->persist($newCompetitionextrapoints);
                        }
    
                        $em->flush();
                    }

                }
                
/*
                $oldCatRelation = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findBy(["competition_id" => $main_comp_id, "is_deleted" => 0 , "type" => 'include' ]);

                if ($oldCatRelation) {
                    foreach ($oldCatRelation as $_oldCatRelation) {
                        $_oldCatRelation->setIs_deleted(1);
                        $em->flush();
                    }
                }
*/    
                if (!empty($cat_id_include)) {
                    foreach ($cat_id_include as $_catId) {

                        $new_cat = false;

                        $oldCatRelation = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findOneBy(["category_id" => $_catId,"competition_id" => $main_comp_id, "is_deleted" => 0 ]);

                        if ($oldCatRelation) {
                            $competition_category_relation = $oldCatRelation;
                        }else{
                            $new_cat = true;
                            $competition_category_relation = new Competitioncategoryrelation();
                        }

                        $competition_category_relation->setCompetition_id($main_comp_id);
                        $competition_category_relation->setCategory_id($_catId);
                        $competition_category_relation->setType('include');
                        $competition_category_relation->setIs_deleted(0);
                        $competition_category_relation->setExtra_points($extar_points);

                        if($new_cat){
                            $em->persist($competition_category_relation);
                        }

                        $em->flush();
                    }
                }

            }

            if($extra_point_type == 'exclude'){

                #deleteOldExclusion
                $CompetitionexclusionmasterOld = $em->getRepository("AdminBundle:Competitionexclusionmaster")->findBy(["competition_id" => $main_comp_id, 'is_deleted' => 0 ]);

                if ($CompetitionexclusionmasterOld) {
                    foreach ($CompetitionexclusionmasterOld as $_CompetitionexclusionmasterOld) {
                        $_CompetitionexclusionmasterOld->setIs_deleted(1);
                        $em->flush();
                    }
                }

                #newEntry
                if (!empty($rest_id_excludes)) {
                    foreach ($rest_id_excludes as $_relation_id) {
                        $newCompetitionexclusionmaster = new Competitionexclusionmaster();
                        $newCompetitionexclusionmaster->setCompetition_id($main_comp_id);
                        $newCompetitionexclusionmaster->setRestaurant_id($_relation_id);
                        $newCompetitionexclusionmaster->setIs_deleted(0);
                        $em->persist($newCompetitionexclusionmaster);
                        $em->flush();
                    }
                }
                #newEntry exclusion done

                $oldCatRelation = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findBy(["competition_id" => $main_comp_id, "is_deleted" => 0 , "type" => 'exclude' ]);

                if ($oldCatRelation) {
                    foreach ($oldCatRelation as $_oldCatRelation) {
                        $_oldCatRelation->setIs_deleted(1);
                        $em->flush();
                    }
                }
    
                if (!empty($cat_id_list)) {
                    foreach ($cat_id_list as $_catId) {

                        $new_cat = false;

                        $oldCatRelation = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findOneBy(["category_id" => $_catId,"competition_id" => $main_comp_id, "is_deleted" => 0 ]);

                        if ($oldCatRelation) {
                            $competition_category_relation = $oldCatRelation;
                        }else{
                            $new_cat = true;
                            $competition_category_relation = new Competitioncategoryrelation();
                        }

                        $competition_category_relation->setCompetition_id($main_comp_id);
                        $competition_category_relation->setCategory_id($_catId);
                        $competition_category_relation->setType('exclude');
                        $competition_category_relation->setIs_deleted(0);
                        $competition_category_relation->setExtra_points(0);

                        if($new_cat){
                            $em->persist($competition_category_relation);
                        }

                        $em->flush();
                    }
                }

            }



            

        $this->get('session')->getFlashBag()->set("success_msg", "Extra Points has been saved.");

        return $this->redirectToRoute("admin_competitionactivity_addactivitycompetition", array("competition_id" => $main_comp_id));
    }

    /**
     * @Route("/saveActivityCompetitionPoints",)
     * @Template()
     */
    public function saveactivitycompetitionpointsAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $main_comp_id = $req->request->get('main_comp_id');
        $activity_id = $req->request->get('activity_id');
        $activity_points = $req->request->get('activity_points');

        #deleteOld
        if (!empty($main_comp_id)) {

            $CompetitionactivityrelationOld = $em->getRepository("AdminBundle:Competitionactivityrelation")->findBy(["competition_id" => $main_comp_id, 'is_deleted' => 0]);

            if ($CompetitionactivityrelationOld) {
                foreach ($CompetitionactivityrelationOld as $_CompetitionactivityrelationOld) {
                    $_CompetitionactivityrelationOld->setIs_deleted(1);
                    $em->flush();
                }
            }
        }

        if (!empty($activity_id)) {
            foreach ($activity_id as $_activity_id) {
                #check entry
                $new_entry = false;

                $Competitionactivityrelation = $em->getRepository("AdminBundle:Competitionactivityrelation")->findOneBy(["competition_id" => $main_comp_id, "activity_id" => $_activity_id, 'is_deleted' => 0]);
                if (empty($Competitionactivityrelation)) {
                    $Competitionactivityrelation = new Competitionactivityrelation();
                    $new_entry = true;
                }

                $Competitionactivityrelation->setCompetition_id($main_comp_id);
                $Competitionactivityrelation->setActivity_id($_activity_id);
                $Competitionactivityrelation->setStatus('active');
                $Competitionactivityrelation->setIs_deleted(0);
                $Competitionactivityrelation->setPoints($activity_points[$_activity_id]);
                $Competitionactivityrelation->setCreated_datetime(date('Y-m-d H:i:s'));

                if ($new_entry) {
                    $em->persist($Competitionactivityrelation);
                }
                $em->flush();
            }
        }

        $this->get('session')->getFlashBag()->set('success_msg', 'Competition Point has been saved successfully.');

        return $this->redirectToRoute("admin_competitionactivity_addactivitycompetition", array("competition_id" => $main_comp_id));

//		return $this->redirect($req->headers->get('referer'));
    }

     /**
     * @Route("/saveExistedExtraPoints",)
     * @Template()
     */
    public function saveExistedExtraPointsAction(Request $req) {
        $extra_points = $req->request->get('extra_points');
        
        $em = $this->getDoctrine()->getManager();

        if(!empty($extra_points)){
            foreach($extra_points as $key => $value){

                $oldCompetitionextrapoints = $em->getRepository("AdminBundle:Competitionextrapoints")->findOneBy(["competition_extra_points_id" => $key,"is_deleted" => 0]);
                
                $oldCompetitionextrapoints->setExtra_points($value);
                $oldCompetitionextrapoints->setCreated_datetime(date('Y-m-d H:i:s'));
                $em->flush();
            }
        }

        $this->get('session')->getFlashBag()->set('success_msg','Included Extra Points has been updated.');

        return $this->redirect($req->headers->get('referer'));

    }

     /**
     * @Route("/saveExistedExtraPointsCategory",)
     * @Template()
     */
    public function saveExistedExtraPointsCategoryAction(Request $req) {
        $extra_points = $req->request->get('extra_points');
        
        $em = $this->getDoctrine()->getManager();

        if(!empty($extra_points)){
            foreach($extra_points as $key => $value){

                $oldCompetitionextrapoints = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findOneBy(["competition_category_relation_id" => $key,"is_deleted" => 0]);
                
                $oldCompetitionextrapoints->setExtra_points($value);
                $em->flush();
            }
        }

        $this->get('session')->getFlashBag()->set('success_msg','Included Extra Points has been updated.');

        return $this->redirect($req->headers->get('referer'));

    }

    /**
     * @Route("/deleteExistedExtraPointsCategory/{competition_category_relation_id}",defaults={"competition_category_relation_id"="0"})
     * @Template()
     */
    public function deleteExistedExtraPointsCategoryAction(Request $req , $competition_category_relation_id) {

        $em = $this->getDoctrine()->getManager();

//        $competition_extra_points_id = $req->request->get('competition_extra_points_id');
        $oldCompetitionextrapoints = $em->getRepository("AdminBundle:Competitioncategoryrelation")->findOneBy(["competition_category_relation_id" => $competition_category_relation_id,"is_deleted" => 0]);

        if($oldCompetitionextrapoints){
            $oldCompetitionextrapoints->setIs_deleted(1);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->set('success_msg','Included Extra Points has been deleted.');

        return $this->redirect($req->headers->get('referer'));

    }    


    /**
     * @Route("/deleteExistedExtraPoints/{competition_extra_points_id}",defaults={"competition_extra_points_id"="0"})
     * @Template()
     */
    public function deleteExistedExtraPointsAction(Request $req , $competition_extra_points_id) {

        $em = $this->getDoctrine()->getManager();

//        $competition_extra_points_id = $req->request->get('competition_extra_points_id');
        $oldCompetitionextrapoints = $em->getRepository("AdminBundle:Competitionextrapoints")->findOneBy(["competition_extra_points_id" => $competition_extra_points_id,"is_deleted" => 0]);

        if($oldCompetitionextrapoints){
            $oldCompetitionextrapoints->setIs_deleted(1);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->set('success_msg','Included Extra Points has been deleted.');

        return $this->redirect($req->headers->get('referer'));

    }



    /**
     * @Route("/saveActivityCompetition",)
     * @Template()
     */
    public function saveactivitycompetitionAction(Request $req) {

        $media_id = 0;

        if(isset($_FILES['rule_book'])){
            $media = $_FILES['rule_book']['name'];
            if (isset($media) && $media != '') {
    
                $upload_dir = $this->container->getParameter('upload_dir1');
                $location = '/Resource/category';
    
                $mediatype = $this->getDoctrine()
                        ->getManager()
                        ->getRepository("AdminBundle:Mediatype")
                        ->findOneBy(array(
                    'media_type_name' => 'Image',
                    'is_deleted' => 0)
                );
                $allowedExts = explode(',', $mediatype->getMedia_type_allowed());
                $temp = explode('.', $_FILES['rule_book']['name']);
                $extension = end($temp);
                if (in_array($extension, $allowedExts)) {
    
                    $media_id = $this->mediaupload($_FILES['rule_book'], $upload_dir, $location, $mediatype->getMedia_type_id());
                }
            }
        }


        $main_comp_id = $req->request->get('main_comp_id');
        $comp_name = $req->request->get('comp_name');
        $comp_name_ar = $req->request->get('comp_name_ar');
        $comp_description = $req->request->get('comp_description');
        $start_date = $req->request->get('start_date');
        $end_date = $req->request->get('end_date');
        $status = $req->request->get('status');
        $is_special = $req->request->get('is_special');
        $relation_id = $req->request->get('relation_id');

        $given_start_date = date('Y-m-d H:i:s', strtotime($start_date));
        $given_end_date = date('Y-m-d H:i:s', strtotime($end_date));

        $em = $this->getDoctrine()->getManager();

        #validationForDates
        $check_other = '';
        if (!empty($main_comp_id)) {
            $check_other = " and main_competition_id != '$main_comp_id' ";
        }
        $validate_sql = "select main_competition_id from competition_master
						where ((start_date BETWEEN '$given_start_date' AND '$given_end_date') 
						OR (end_date BETWEEN '$given_start_date' AND '$given_end_date')) and is_deleted = '0' and status = 'active' $check_other ";

        $date_not_available_sql = $em->getConnection()->prepare($validate_sql);
        $date_not_available_sql->execute();
        $date_not_available = $date_not_available_sql->fetchAll();

        if (!empty($date_not_available) && false) {
            $this->get('session')->getFlashBag()->set('error_msg', 'Competition exist in givent Time.');
            return $this->redirect($req->headers->get('referer'));
        }

        $competition_master = $em->getRepository("AdminBundle:Competitionmaster")->findOneBy(["main_competition_id" => $main_comp_id, 'is_deleted' => 0]);

        $new_entry = false;
        if (empty($competition_master)) {
            $new_entry = true;
            $competition_master = new Competitionmaster();
            $competition_master->setExtra_point_type('include');
            $competition_master->setExtra_point_value(0);
        }

        $competition_master->setName($comp_name);
        $competition_master->setName_ar($comp_name_ar);
        $competition_master->setDescription($comp_description);
        $competition_master->setStatus($status);
        $competition_master->setIs_special($is_special);
        $competition_master->setStart_date(date('Y-m-d H:i:s', strtotime($start_date)));
        $competition_master->setEnd_date(date('Y-m-d H:i:s', strtotime($end_date)));
        $competition_master->setLanguage_id(1);
        $competition_master->setMain_competition_id($main_comp_id);
        $competition_master->setCreated_datetime(date('Y-m-d H:i:s'));
        $competition_master->setCreated_by($this->get('session')->get('admin_user_id'));
        $competition_master->setIs_deleted(0);
        if ($new_entry) {
            $competition_master->setRule_book($media_id);
        } else {
            if (!empty($media_id)) {
                $competition_master->setRule_book($media_id);
            }
        }


        if ($new_entry) {
            $em->persist($competition_master);
        }

        $em->flush();

        if ($new_entry) {
            $main_comp_id = $competition_master->getCompetition_master_id();
            $competition_master->setMain_competition_id($main_comp_id);
            $em->flush();
        }

/*
        #deleteOldExclusion
        $CompetitionexclusionmasterOld = $em->getRepository("AdminBundle:Competitionexclusionmaster")->findBy(["competition_id" => $main_comp_id, 'is_deleted' => 0]);

        if ($CompetitionexclusionmasterOld) {
            foreach ($CompetitionexclusionmasterOld as $_CompetitionexclusionmasterOld) {
                $_CompetitionexclusionmasterOld->setIs_deleted(1);
                $em->flush();
            }
        }

        #newEntry
        if (!empty($relation_id)) {
            foreach ($relation_id as $_relation_id) {
                $newCompetitionexclusionmaster = new Competitionexclusionmaster();
                $newCompetitionexclusionmaster->setCompetition_id($main_comp_id);
                $newCompetitionexclusionmaster->setRestaurant_id($_relation_id);
                $newCompetitionexclusionmaster->setIs_deleted(0);
                $em->persist($newCompetitionexclusionmaster);
                $em->flush();
            }
        }
        #newEntry exclusion done
*/

        $this->get('session')->getFlashBag()->set('success_msg', 'Competition has been saved successfully.');

        if ($new_entry) {
            return $this->redirectToRoute('admin_competitionactivity_addactivitycompetition', array("domain" => $this->get('session')->get('domain'), "competition_id" => $main_comp_id));
        } else {
            return $this->redirect($req->headers->get('referer'));
        }
    }

    /**
     * @Route("/deleteActivityCompetition/{competition_id}",defaults={"competition_id":"0"})
     * @Template()
     */
    public function deleteactivitycompetitionAction($competition_id, Request $req) {
        $em = $this->getDoctrine()->getManager();
        $competition_master = $em->getRepository("AdminBundle:Competitionmaster")->findBy(["main_competition_id" => $competition_id, 'is_deleted' => 0]);
        if ($competition_master) {
            foreach ($competition_master as $_competition_master) {
                $_competition_master->setIs_deleted(1);
                $em->flush();
            }
        }
        $this->get('session')->getFlashBag()->set('success_msg', 'Competition has been deleted successfully.');
        return $this->redirect($req->headers->get('referer'));
    }

    /**
     * @Route("/getCompetitionUsers/{comp_id}",defaults={"comp_id"="0"})
     * @Template()
     */
    public function usersAction($comp_id) {
        return (array('comp_id' => $comp_id));
    }

    /**
     * @Route("/get-user-list-comp/{comp_id}",defaults={"comp_id"="0"})
     * @Template()
     */
    public function getUserListCompAction($comp_id , Request $req) {
        ini_set('xdebug.var_display_max_depth', 200);
        ini_set('xdebug.var_display_max_children', 256);
        ini_set('xdebug.var_display_max_data', 1024);


        $end_date = null;
        $start_date = null;
        if (array_key_exists('start_date', $_REQUEST)) {
            $start_date = $_REQUEST['start_date'];
        }

        if (array_key_exists('end_date', $_REQUEST)) {
            $end_date = $_REQUEST['end_date'];
        }

//        echo $start_date ." and " .$end_date;exit;

        $search_value = '';
        if (array_key_exists('search', $_REQUEST)) {
            $search_value = $_REQUEST['search']['value'];
        }

        $limit_sql = "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['length'];
        $em = $this->getDoctrine()->getManager();

        $joinPoints = " JOIN competition_user_relation 
						ON competition_user_relation.user_id = user_master.user_master_id and competition_user_relation.is_deleted = 0";

        $groupBy = " group by competition_user_relation.user_id ";

        $joinWhere = '';
        if (!empty($comp_id)) {
            $joinWhere = " AND competition_user_relation.competition_id = '$comp_id' ";
        }

        $last_month_date = date('Y-m-d H:i:s', strtotime("-30 days", strtotime(date('Y-m-d H:i:s'))));
        $monthlyDateWhere = '';

        if (empty($comp_id)) {
//            $monthlyDateWhere = " and competition_user_relation.created_datetime >= '$last_month_date' ";
        }

        if(!empty($start_date) && !empty($end_date)){
            $start_date = date('Y-m-d 00:00:00',strtotime($start_date));
            $end_date = date('Y-m-d 23:59:59',strtotime($end_date));

            $monthlyDateWhere = " and competition_user_relation.created_datetime BETWEEN '$start_date' AND '$end_date' ";            
        }


        if (isset($search_value) && $search_value != '') {
            $query = "select user_master_id ,sum(points) as points_total from user_master $joinPoints WHERE user_master.user_role_id = 3 AND user_master.is_deleted = 0 AND (user_master.email like '%{$search_value}%' or user_master.username like '%{$search_value}%' or user_master.user_firstname like '%{$search_value}%' or user_master.user_lastname like '%{$search_value}%') {$joinWhere} {$monthlyDateWhere} {$groupBy} ";
        } else {
            $query = "select user_master_id , sum(points) as points_total from user_master $joinPoints WHERE user_master.user_role_id = 3 AND user_master.is_deleted = 0 {$joinWhere} {$monthlyDateWhere} {$groupBy}";
        }

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt1 = $con->prepare($query);
        $stmt1->execute();
        $user_count = $stmt1->fetchAll();

        if (isset($search_value) && $search_value != '') {
            $sql_user_data = "select * , sum(points) as points_total from user_master $joinPoints WHERE user_master.user_role_id = 3 AND user_master.is_deleted = 0 AND (user_master.email like '%{$search_value}%' or user_master.username like '%{$search_value}%' or user_master.user_firstname like '%{$search_value}%' or user_master.user_lastname like '%{$search_value}%') {$joinWhere} {$monthlyDateWhere} {$groupBy} order by points_total DESC {$limit_sql}";
        } else {
            $sql_user_data = "SELECT * , sum(points) as points_total from user_master $joinPoints WHERE user_master.user_role_id = 3 AND user_master.is_deleted = 0 {$joinWhere} {$monthlyDateWhere} {$groupBy} order by points_total DESC {$limit_sql}";
        }

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_user_data);
        $stmt->execute();
        $userlist = $stmt->fetchAll();

        $user_list = array();
        if (!empty($userlist)) {
            foreach ($userlist as $_user) {

                $refered_by = $_user['refered_by'];
                $refered_by_name = '';
                if ($refered_by != null) {
                    $_sql = "SELECT * from user_master where user_master_id = {$refered_by} and is_deleted = 0";
                    $user1 = $this->firequery($_sql);

                    if (!empty($user1)) {
                        $refered_by_name = trim($user1[0]['user_firstname'] . ' ' . $user1[0]['user_lastname']);
                    }
                }

                $user = array();
                $user_id = $_user['user_master_id'];
                if ($_user['status'] == 'active') {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                $status = $_user['points_total'];

                $user[] = "<img class='user-img' src='{$this->getMediaUrl($_user['user_image'])}'>";
                $user[] = "<span id='remove_id_{$_user['user_master_id']}'>" . $_user['user_firstname'] . ' ' . $_user['user_lastname'] . "</span>";
                $user[] = $_user['user_bio'];
                $user[] = $_user['email'];
                /* $user[] = $_user['username']; */
                $user[] = $_user['user_mobile'];
                $user[] = $_user['user_gender'];
                //$user[] = $_user['date_of_birth']; 
                $user[] = $status;
                $user[] = $refered_by_name;

//                  $user[] = $_user['last_login'];
                $pointsDetailsLink = $this->generateUrl('admin_competitionactivity_getpointsdetails', array("user_id" => $user_id, "comp_id" => $comp_id));

                $pointsDetailsLinkAll = $this->generateUrl('admin_competitionactivity_getpointsdetails', array("user_id" => $user_id, "comp_id" => 0));


                if(!empty($comp_id)){
                    $user[] = "<a href='$pointsDetailsLinkAll' class='btn btn-info btn-xs' data-toggle='tooltip' title='View Details Overall' ><i class='fa fa-fw fa-book'></i></a>
                    <a href='$pointsDetailsLink' class='btn btn-info btn-xs' data-toggle='tooltip' title='View Details Of Competetion' ><i class='fa fa-fw fa-eye'></i></a>";
                }else{
                    $user[] = "<a href='$pointsDetailsLink' class='btn btn-info btn-xs' data-toggle='tooltip' title='View Details'' ><i class='fa fa-fw fa-eye'></i></a>";
                }


                $user_list[] = $user;
            }
        }

        $response = array(
            'draw' => $_REQUEST['draw'],
            'recordsTotal' => count($user_count),
            'recordsFiltered' => count($user_count),
            'data' => $user_list
        );

        echo json_encode($response);
        exit;
    }

    /**
     * @Route("/updatePointDetails/{comp_id}/{user_id}",defaults={"comp_id":"0","user_id":"0"})
     * @Template()
     */
    public function updatepointsdetailsAction(Request $req) {
        $points = $req->request->get('activity_points');

        $em = $this->getDoctrine()->getManager();

        if ($points) {
            foreach ($points as $relation_id => $point) {
                $point_entry = $em->getRepository("AdminBundle:Competitionuserrelation")->findOneBy(['competition_user_relation_id' => $relation_id]);
                if ($point_entry) {
                    $point_entry->setPoints($point);
                    $em->flush();
                }
            }
        }

        $this->get('session')->getFlashBag()->set('success_msg', 'Points are updated successfully');
        return $this->redirect($req->headers->get('referer'));
    }

    /**
     * @Route("/getPointDetails/{comp_id}/{user_id}",defaults={"comp_id":"0","user_id":"0"})
     * @Template()
     */
    public function getpointsdetailsAction($comp_id, $user_id, Request $req) {

        $user_name = '';
        $competition_name = '';

        $last_month_date = date('Y-m-d H:i:s', strtotime("-30 days", strtotime(date('Y-m-d H:i:s'))));
        $monthlyDateWhere = '';

        if (empty($comp_id)) {
            //$monthlyDateWhere = " and competition_user_relation.created_datetime >= '$last_month_date' ";
        }

        if (!empty($comp_id)) {
            $sql_activitys_users = "select *,competition_user_relation.created_datetime as gain_on,
            competition_master.name as comp_name,competition_master.start_date as comp_begins,competition_master.end_date as comp_ends 
             from competition_user_relation 
									JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
									JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
									where competition_user_relation.user_id = '$user_id' 
									AND competition_user_relation.competition_id = '$comp_id' and competition_user_relation.is_deleted = 0 order by competition_user_relation.created_datetime DESC ";
        } else {
            $sql_activitys_users = "select *,competition_user_relation.created_datetime as gain_on,
            competition_master.name as comp_name,competition_master.start_date as comp_begins,competition_master.end_date as comp_ends 
            from competition_user_relation 
				JOIN activity_master ON activity_master.main_activity_id = 	competition_user_relation.activity_id 
				JOIN competition_master ON competition_master.main_competition_id = competition_user_relation.competition_id 
				where competition_user_relation.user_id = '$user_id' {$monthlyDateWhere} and competition_user_relation.is_deleted = 0 
				order by competition_user_relation.created_datetime DESC ";
        }

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_activitys_users);
        $stmt->execute();
        $pointDetails = $stmt->fetchAll();
//		echo"<pre>";print_r($pointDetails);exit;

        $overAllCompArray = array();

        $today = strtotime("now");

        if(!empty($pointDetails)){
            foreach($pointDetails as $_pointDetails){
                if(!array_key_exists($_pointDetails['main_competition_id'],$overAllCompArray)){
                    $begins_date = strtotime(date('Y-m-d',strtotime($_pointDetails['comp_begins'])));
                    $ends_date = strtotime(date('Y-m-d',strtotime($_pointDetails['comp_ends'])));
                    
                    $status = 'running';

                    if($begins_date > $today) {
                        $status = 'upcoming';
                    }

                    if($ends_date < $today) {
                        $status = 'completed';
                    }
                    
                    $competition_id_ = $_pointDetails['main_competition_id'];

                    $overAllCompArray [$competition_id_] = array(
                        "comp_name" => $_pointDetails['comp_name'],
                        "comp_begins" => $_pointDetails['comp_begins'],
                        "comp_ends" => $_pointDetails['comp_ends'],
                        "status" => $status
                    );
                }
            }
        }

//        echo"<pre>";print_r($overAllCompArray);exit;

        $usermaster = $em->getRepository("AdminBundle:Usermaster")->findOneBy(['user_master_id' => $user_id]);
        $competition = $em->getRepository("AdminBundle:Competitionmaster")->findOneBy(['main_competition_id' => $comp_id]);

        if ($competition) {
            $competition_name = $competition->getName();
        }
        if ($usermaster) {
            $user_name = $usermaster->getUser_bio();
            if ($user_name == '') {
                $user_name = $usermaster->getUser_firstname() . " " . $usermaster->getUser_lastname();
            }
        }

//		echo"<pre>";print_r($pointDetails);exit;
        /*
          $detail_table_head = "<table class='table'>";
          $detail_table_head .= "<thead>";
          $detail_table_head .= "<tr>";
          $detail_table_head .= "<th>No</th>";
          $detail_table_head .= "<th>Activity</th>";
          $detail_table_head .= "<th>Date</th>";
          $detail_table_head .= "<th>Points</th>";
          $detail_table_head .= "</tr>";
          $detail_table_head .= "</thead>";
          $detail_table_head .= "<tbody id='pointDetailsId_".$user_id."'>";

          $detail_table_foot = "</tbody></table>";

          $table_rows = '';
          $table_rows .= "<form onsubmit='updatePoints(event,".$user_id.",".$comp_id.")'>";
          if(!empty($pointDetails)){
          $counter = 1;
          foreach($pointDetails as $_pointDetails){
          $table_rows .= "<tr>";
          $table_rows .= "<td>".$counter."</td>";
          $table_rows .= "<td>".$_pointDetails['activity_title']."</td>";
          $table_rows .= "<td>".date('d-m-Y',strtotime($_pointDetails['created_datetime']))."</td>";
          $table_rows .= "<td><input type='text' class='form-control' name='point_relation[".$comp_id."][".$_pointDetails['competition_user_relation_id']."]' value='".$_pointDetails['points']."' /></td>";
          $table_rows .= "</form>";
          $table_rows .= "</tr>";

          $counter += 1 ;
          }
          $table_rows .= "<tr>";
          $table_rows .= "<td colspan='4'>";
          $table_rows .= "<button class='btn btn-info pull-right'>Update Points</button>";
          $table_rows .= "</td>";
          $table_rows .= "</tr>";
          $table_rows .= "</form>";
          }

          $html_response = $detail_table_head.$table_rows.$detail_table_foot;
         */
        return array('pointDetails' => $pointDetails, 'comp_id' => $comp_id, "user_name" => $user_name, "competition_name" => $competition_name ,'overAllCompArray' => $overAllCompArray);
    }

    public function getMediaUrl($media_id) {
        $media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
        $media = $media_repository->find($media_id);

        $live_path = $this->container->getParameter('live_path');
        if (!empty($media)) {
            $media_url = $live_path . $media->getMedia_location() . '/' . $media->getMedia_name();
        } else {
            $media_url = $live_path . '/bundles/Resource/default.png';
        }

        return $media_url;
    }

    /**
     * @Route("/competetions/ajaxupdatestatus")
     */
    public function ajaxupdatestatusAction(Request $request) {

        if ($request->get('cat_id')) {
            $em = $this->getDoctrine()->getManager();
            $category = $em->getRepository(Competitionmaster::class)->findBy(array('main_competition_id' => $request->get('cat_id')));

            if (!empty($category)) {
                foreach ($category as $key => $value) {
                    $current_status = $value->getStatus();
                    if ($current_status == 'active') {
                        $value->setStatus('inactive');
                    } else {
                        $value->setStatus('active');
                    }
                    $em->flush();
                }
            }
            echo 'true';
            exit;
        }
        echo 'false';
        exit;
    }

}
