<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Competitionaward;
use AdminBundle\Entity\Competitionawardgallery;
use AdminBundle\Entity\Governoratemaster;

/**
 * @Route("/admin")
 */
class CompetitionController extends BaseController {
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
    
    /**
    * @Route("/competition-awards")
    * @Template()
    */
    public function indexAction() {
		$right_codes = $this->userrightsAction();
		
		$language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
		
		$sql = "select com.*, media.media_title, media.media_location from competition_award com left join media_library_master media on com.competition_award_cover_image_id = media.media_library_master_id where com.is_deleted = 0 group by com.main_competition_award_id";
		
		$conn = $this->getDoctrine()->getManager()->getConnection();
		$prepare = $conn->prepare($sql);
		$prepare->execute();
		$competition_list = $prepare->fetchAll();
		
		$lan_com = "select competition_award_id, main_competition_award_id,language_id, competition_award_title from competition_award where is_deleted = 0";
		$prepare = $conn->prepare($lan_com);
		$prepare->execute();
		$lan_wise_competition = $prepare->fetchAll();
		
		$all_competition = array();
		foreach($competition_list as $_competition_list){
			$lan_com= array();
			foreach($lan_wise_competition as $_lan_wise_competition){
				
				if($_competition_list['competition_award_id'] == $_lan_wise_competition['competition_award_id'] && $_competition_list['language_id'] == $_lan_wise_competition['language_id']){
					$lan_com[] = $_lan_wise_competition;
				}
				
				if($_competition_list['competition_award_id'] == $_lan_wise_competition['main_competition_award_id'] && $_competition_list['language_id'] != $_lan_wise_competition['language_id']){
					$lan_com[] = $_lan_wise_competition;
				}
			}
			
			$_competition_list['lang_wise_competition'] = $lan_com;
			$all_competition[] = $_competition_list;
		}
		
		/* echo '<pre>';
		print_r($all_competition);
		exit; */
		
		return array(
			"languages" => $language_list,
			"competition_list" => $all_competition,
			'right_codes' => $right_codes
		);
	}
	
	/**
    * @Route("/addcompetition/{com_id}", defaults = {"com_id" = ""})
    * @Template()
    */
    public function addcompetitionAction($com_id) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SAA30", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
		
		$competition = array();
		if(isset($com_id) && $com_id != ''){
			$repository = $this->getDoctrine()->getRepository(Competitionaward::class);
			$existing_competition_list = $repository->findBy(
					array(
						'main_competition_award_id' => $com_id,
						'is_deleted' => 0
					)
				);
			
			foreach($existing_competition_list as $existing_competition){
				if($existing_competition->getIs_deleted() == 0){
					$repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
					$media_file = $repository->find($existing_competition->getCompetition_award_cover_image_id());
					
					if(!empty($media_file)){
						$media_url = $this->container->getParameter('live_path').$media_file->getMedia_location().'/'.$media_file->getMedia_name();
					} else {
						$media_url = $this->container->getParameter('live_path').'bundles/Resource/default.png';
					}
					
					$repository = $this->getDoctrine()->getManager()->getRepository(Competitionawardgallery::class);
					$gallery_images = $repository->findBy(
							array(
								'main_competition_award_id' => $com_id,
								'is_deleted' => 0
							)
						);
					
					$gallery_files = array();
					if(!empty($gallery_images)){
						foreach($gallery_images as $_gallery_image){
							$repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
							$gallery_img = $repository->find($_gallery_image->getImage_id());
							
							if(!empty($gallery_img)){
								$gallery_files[] = array(
										'main_media_id' => $gallery_img->getMedia_library_master_id(),
										'main_gallery_id' => $_gallery_image->getCompetition_award_gallery_id(),
										'media_name' => $gallery_img->getMedia_name(),
										'media_location' => $gallery_img->getMedia_location()
									);
							}
						}
					}
					
					$competition[] = array(
						'competition_award_id' => $existing_competition->getCompetition_award_id(),
						'competition_award_title' => $existing_competition->getCompetition_award_title(),
						'main_competition_award_id' => $existing_competition->getMain_competition_award_id(),
						'competition_award_cover_image_id' => $existing_competition->getCompetition_award_cover_image_id(),
						'language_id' => $existing_competition->getLanguage_id(),
						'status' => $existing_competition->getStatus(),
						'competition_award_date' => $existing_competition->getCompetition_award_date(),
						'start_date' => date('d-m-Y',strtotime($existing_competition->getStart_date())),
						'end_date' => date('d-m-Y',strtotime($existing_competition->getEnd_date())),
						'short_description' => $existing_competition->getShort_description(),
						'description' => $existing_competition->getDescription(),
						'is_deleted' => $existing_competition->getIs_deleted(),
						'media_url' => $media_url,
						'media_library_master' => $gallery_files
					);
					
				} else {
					$competition = array();
				}
			}
		}
		
		/* echo '<pre>';
		print_r($competition);
		exit; */
		
		return array(
			'languages' => $language_list,
			'competition' => $competition
		);
	}
	
	/**
    * @Route("/addcompetitiondb")
    * @Template()
    */
    public function addcompetitiondbAction(Request $request) {
		
		if($request->request->all()){
			
			$em = $this->getDoctrine()->getManager();
			
			$cover_media = $_FILES['cover_image']['name'];
			if(isset($cover_media) && $cover_media != ''){
				
				$upload_dir = $this->container->getParameter('upload_dir1');
				$location = '/Resource/competition-awards';
				
				$mediatype = $this->getDoctrine()
						->getManager()
						->getRepository(Mediatype::class)
						->findOneBy(array(
							'media_type_name' => 'Image',
							'is_deleted'=>0)
						);
				$allowedExts = explode(',',$mediatype->getMedia_type_allowed());
				$temp = explode('.',$_FILES['cover_image']['name']);
				$extension = end($temp);
				if(in_array($extension, $allowedExts)){
					$cover_media_id = $this->mediaupload($_FILES['cover_image'], $upload_dir, $location, $mediatype->getMedia_type_id());
				}
			}
			
			$competition_id = '';
			if($request->get('language_master_id') && $request->get('competition_id')){
				
				/* for edit */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SUA31", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
				//$competition = $em->getRepository(Competitionaward::class)->find($request->get('competition_id'));
				$competition = $em->getRepository('AdminBundle:Competitionaward')->findOneBy([
					'main_competition_award_id' => $request->get('competition_id'),
					'language_id' => $request->get('language_master_id')
				]);
				
				$competition->setCompetition_award_title($request->get('competition_award_title'));
				if(isset($cover_media_id)){
					$competition->setCompetition_award_cover_image_id($cover_media_id);
				}
				$competition->setCompetition_award_date($request->get('competition_award_date'));
				$competition->setShort_description($request->get('short_description'));
				$competition->setDescription($request->get('description'));
				$competition->setStart_date(date('Y-m-d',strtotime($request->get('start_date'))));
				$competition->setEnd_date(date('Y-m-d',strtotime($request->get('end_date'))));
				$com_date = $request->get('competition_award_date');
				if(isset($com_date)){
					$com_date = date('Y-m-d h:i:s', strtotime($com_date));
					$competition->setCompetition_award_date($com_date);
				}
				//$competition->setCreated_date(date('Y-m-d h:i:s'));
				//$competition->setLanguage_id($request->get('language_master_id'));
				$competition->setStatus($request->get('status'));
				$competition->setIs_deleted(0);
				
				$em->flush();
				
				$competition_id = $competition->getMain_competition_award_id();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Competition & Award updated successfully');
			} else {
				/* for insert */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SAA30", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
				$competition = new Competitionaward();
				$competition->setCompetition_award_title($request->get('competition_award_title'));
				if(isset($cover_media_id)){
					$competition->setCompetition_award_cover_image_id($cover_media_id);
				}
				$competition->setCompetition_award_date($request->get('competition_award_date'));
				$competition->setShort_description($request->get('short_description'));
				$competition->setDescription($request->get('description'));
				$competition->setStart_date(date('Y-m-d',strtotime($request->get('start_date'))));
				$competition->setEnd_date(date('Y-m-d',strtotime($request->get('end_date'))));
				$com_date = $request->get('competition_award_date');
				if(isset($com_date)){
					$com_date = date('Y-m-d h:i:s', strtotime($com_date));
					$competition->setCompetition_award_date($com_date);
				}
				$competition->setCreated_date(date('Y-m-d h:i:s'));
				$competition->setLanguage_id($request->get('language_master_id'));
				$competition->setStatus($request->get('status'));
				$competition->setIs_deleted(0);
				
				$em->persist($competition);
				$em->flush();
				
				if($request->get('main_competition_id')){
					$competition->setMain_competition_award_id($request->get('main_competition_id'));
				} else {
					$competition->setMain_competition_award_id($competition->getCompetition_award_id());
				}
				$em->flush();
				
				$competition_id = $competition->getMain_competition_award_id();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Competition & Award inserted successfully');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		
		return $this->redirectToRoute('admin_competition_addcompetition', array('com_id' => $competition_id));
	}
	
	public function mediaupload($file, $upload_dir, $location, $media_type_id){
		
		$upload_dir = $upload_dir.$location;
		
		if(!is_dir($upload_dir)){
			mkdir($upload_dir);	
		}
		
		$clean_image = preg_replace('/\s+/', '', $file['name']);
        $media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;
		
		$is_uploaded = move_uploaded_file($file['tmp_name'], $upload_dir.'/'.$media_name);
		
		if($is_uploaded){
			$em = $this->getDoctrine()->getManager();
			
			$mediamaster = new Medialibrarymaster();
			$mediamaster->setMedia_type_id($media_type_id);
			$mediamaster->setMedia_title($media_name);
			$mediamaster->setMedia_location("/bundles/{$location}");
			$mediamaster->setMedia_name($media_name);
			$mediamaster->setCreated_on(date('Y-m-d h:i:s'));
			$mediamaster->setIs_deleted(0);
			
			$em->persist($mediamaster);
			$em->flush();
			
			return $mediamaster->getMedia_library_master_id();
		}
		return false;
	}
	
	/**
     * @Route("/removecompetitionimage")
     */
    public function removecompetitionimageAction(Request $request)
    {
    	$media_file = $this->getDoctrine()
				   ->getManager()
				   ->getRepository('AdminBundle:Medialibrarymaster')
				   ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'),'is_deleted'=>0));

		$gallery_file = $this->getDoctrine()
			   ->getManager()
			   ->getRepository('AdminBundle:Competitionawardgallery')
			   ->findOneBy(array('competition_award_gallery_id' => $request->get('main_gallery_id'),'is_deleted'=>0));

		if(!empty($media_file) && !empty($gallery_file))
		{
			$media_file->setIs_deleted(1);
			$em = $this->getDoctrine()->getManager();
			$em->flush();

			$gallery_file->setIs_deleted(1);
			$em = $this->getDoctrine()->getManager();
			$em->flush();
			
			$delete_img = $this->container->getParameter('root_dir').'/'.$media_file->getMedia_location().'/'.$media_file->getMedia_name();
			
			//unlink($delete_img);
			
			return new Response("true");
		}
		else
		{
			return new Response("false");
		}
    }
	
	/**
	* @Route("/deletecompetition/{com_id}", defaults = {"com_id" = ""})
	*/
	public function deletecompetitionAction($com_id, Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDA32", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if(isset($com_id) && $com_id != ''){
			
			$em = $this->getDoctrine()->getManager();
			$competition = $em->getRepository(Competitionaward::class)->find($com_id);
			
			if(!empty($competition)){
				$competition->setIs_deleted(1);
				$em->flush();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Competition & Award deleted successfully');
			} else {
				$this->get('session')->getFlashBag()->get('error_msg', 'Competition & Award not found');
			}
		} else {
			$this->get('session')->getFlashBag()->get('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
	* @Route("/savecompetitiongallery")
	*/
	public function savecompetitiongalleryAction(Request $request){
		if($request->request->all()){
			
			$em = $this->getDoctrine()->getManager();
			
			$media_library_master = new Medialibrarymaster();
    		$media_library_master->setMedia_type_id($request->get('media_type_id'));
    		$media_library_master->setMedia_title($request->get('img_name'));
    		$media_library_master->setMedia_location("/bundles/design/uploads/competition-awards");
    		$media_library_master->setMedia_name($request->get('img_name'));
    		$media_library_master->setCreated_on(date("Y-m-d H:i:s"));
    		$media_library_master->setIs_Deleted(0);
			$em->persist($media_library_master);
			$em->flush();
			
			$com_gallery = new Competitionawardgallery();
			$com_gallery->setMain_competition_award_id($request->get('competition_id'));
			$com_gallery->setImage_id($media_library_master->getMedia_library_master_id());
			$com_gallery->setIs_deleted(0);
			$em->persist($com_gallery);
			$em->flush();
			
			$media_file = $this->getDoctrine()
				   ->getManager()
				   ->getRepository('AdminBundle:Medialibrarymaster')
				   ->findOneBy(array('media_library_master_id' => $media_library_master->getMedia_library_master_id(),'is_deleted'=>0));
			
			if(!empty($media_file)){
				$content = array(
					"name" => $media_file->getMedia_name(),
					"path" => $this->container->getParameter('live_path').$media_file->getMedia_location()."/".$media_file->getMedia_name(),
					"thumbnail_path" => $this->container->getParameter('live_path').$media_file->getMedia_location()."/".$media_file->getMedia_name(),
					"media_library_master_id" => $media_library_master->getMedia_library_master_id()
				);
				return new Response(json_encode($content));
			}
		}
		return new Response(false);
	}
	
	/**
	* @Route("/ajaxupdatecomstatus")
	*/
	public function ajaxupdatecomstatusAction(Request $request){
		
		if($request->get('com_id')){
			$em = $this->getDoctrine()->getManager();
			$competition = $em->getRepository(Competitionaward::class)->find($request->get('com_id'));
			$main_comp_id = $competition->getmain_competition_award_id();
                        //$all_comp_list = $this->getdoctrine()->getManager()->getRepository("AdminBundle:Competitionaward")
			$current_status = $competition->getStatus();
			$all_comp_list = $this->getdoctrine()->getManager()->getRepository("AdminBundle:Competitionaward")->findBy(array("is_deleted"=>0,"main_competition_award_id"=>$main_comp_id));
			
			foreach($all_comp_list as $akey=>$aval){
				if($current_status == 'active'){
					$aval->setStatus('inactive');
				}
				else{
					$aval->setStatus('active');
				}
				$em->flush();
			}
			/*
			if($current_status == 'active'){
				$competition->setStatus('inactive');
			} else {
				$competition->setStatus('active');
                        }*/
			$em->flush();
			echo 'true';exit;
		}
		echo 'false';exit;
	}
}
