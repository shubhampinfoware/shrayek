<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Contactusfeedback;
use AdminBundle\Entity\Evaluationfeedback;
use AdminBundle\Entity\Evaluationadmincomment;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Generalnotification;
use AdminBundle\Entity\Evaluationfeedbackgallery;
use AdminBundle\Entity\Apppushnotificationmaster;

/**
* @Route("/admin")
*/
class EvaluationController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/evaluationList",name="admin_evaluation_list")
     * @Template()
     */
    public function indexAction()
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SVRE10", $right_codes);
		if ($rights_search != 1) {
			$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
			return $this->redirect($this->generateUrl('admin_dashboard_index'));
		}
		/* end: check for access */
		
		## restaurant list
        $restaurant_list = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findBy(
			array(
				'status' => 'active',
				'language_id' => 1,
				'is_deleted' => 0
			)
        );
		
		// loads from datatable function - getEvaluationListAction
		return array(
			"right_codes" => $right_codes,
			"restaurant_list" => $restaurant_list,
		);
	}



	/**
	 * @Route("/getevaluationlist")
	 * @Template()
	 */
	public function getEvaluationListAction()
	{
			$right_codes = $this->userrightsAction();
			ini_set('xdebug.var_display_max_depth', 200);
			ini_set('xdebug.var_display_max_children', 256);
			ini_set('xdebug.var_display_max_data', 1024);

			$search_value = '';
			if (array_key_exists('search', $_REQUEST)) {
				$search_value = $_REQUEST['search']['value'];
			}

			$session = $this->get('session');
			if (array_key_exists('eval_page_start', $session->all())) {
				$start = $session->get('eval_page_start');
				$session->remove('eval_page_start');
			} else {
				$start = $_REQUEST['start'];
			}

			if (array_key_exists('eval_page_length', $session->all())) {
				$length = $session->get('eval_page_length');
				$session->remove('eval_page_length');
			} else {
				$length = $_REQUEST['length'];
			}
			
			$limit_sql = "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['length'];

			if (isset($search_value) && $search_value != '') {
					$evaluation_count_sql = "SELECT user_firstname,email,user_mobile,restaurant_name,eval_type,eval.status,eval.created_datetime,evaluation_feedback_id,eval.status as eval_status,eval.created_datetime as created_datetime, rest.status as res_status from evaluation_feedback eval left join user_master user on user.user_master_id=eval.user_id left join restaurant_master rest on rest.main_restaurant_id =eval.restaurant_id where eval.is_deleted=0 and rest.language_id = '1' and (user_firstname like '%{$search_value}%' or email like '%{$search_value}%' or user_mobile like '%{$search_value}%' or restaurant_name like '%{$search_value}%') GROUP by eval.evaluation_feedback_id order by eval.evaluation_feedback_id desc";
			} else {
					$evaluation_count_sql = "SELECT user_firstname,email,user_mobile,restaurant_name,eval_type,eval.status,eval.created_datetime,evaluation_feedback_id,eval.status as eval_status,eval.created_datetime as created_datetime, rest.status as res_status from evaluation_feedback eval left join user_master user on user.user_master_id=eval.user_id left join restaurant_master rest on rest.main_restaurant_id =eval.restaurant_id where eval.is_deleted=0 and rest.language_id = '1' GROUP by eval.evaluation_feedback_id order by eval.evaluation_feedback_id desc";
			}

			$em = $this->getDoctrine()->getManager();
			
			$con = $em->getConnection();
			$stmt = $con->prepare($evaluation_count_sql);
			$stmt->execute();
			$evaluation_count = $stmt->fetchAll();

			if (isset($search_value) && $search_value != '') {
					$evaluation_data_sql = "SELECT user_firstname,email,user_mobile,restaurant_name,eval_type,eval.status,eval.created_datetime,evaluation_feedback_id,eval.status as eval_status,eval.created_datetime as created_datetime, is_featured, main_branch_id, rest.status as res_status from evaluation_feedback eval left join user_master user on user.user_master_id=eval.user_id left join restaurant_master rest on rest.main_restaurant_id =eval.restaurant_id where eval.is_deleted=0 and rest.language_id = '1' and (user_firstname like '%{$search_value}%' or email like '%{$search_value}%' or user_mobile like '%{$search_value}%' or restaurant_name like '%{$search_value}%') GROUP by eval.evaluation_feedback_id order by eval.evaluation_feedback_id desc {$limit_sql}";
			} else {
					$evaluation_data_sql = "SELECT user_firstname,email,user_mobile,restaurant_name,eval_type,eval.status,eval.created_datetime,evaluation_feedback_id,eval.status as eval_status,eval.created_datetime as created_datetime, is_featured, main_branch_id, rest.status as res_status from evaluation_feedback eval left join user_master user on user.user_master_id=eval.user_id left join restaurant_master rest on rest.main_restaurant_id =eval.restaurant_id where eval.is_deleted=0 and rest.language_id = '1' GROUP by eval.evaluation_feedback_id order by eval.evaluation_feedback_id desc {$limit_sql}";
			}
			
			$em = $this->getDoctrine()->getManager();
			$con = $em->getConnection();
			$stmt = $con->prepare($evaluation_data_sql);
			$stmt->execute();
			$eval_data = $stmt->fetchAll();
			
			$get_level_sql = "select * from level_master where status='active' and is_deleted=0";
			$em = $this->getDoctrine()->getManager();
			$con = $em->getConnection();
			$stmt = $con->prepare($get_level_sql);
			$stmt->execute();
			$level_data = $stmt->fetchAll();

			$start = $_REQUEST['start'];
			$evaluation_list = array();
			if(!empty($eval_data)){
				$i = 1;
				foreach($eval_data as $_eval){
					
						$evaluation = array();

						// is_featured
						if($_eval['is_featured'] == 'yes'){
								$is_featured = "<input id='is_featured_{$_eval['evaluation_feedback_id']}' data-on='yes' class='status status_1' data-off='no' onchange='change_feature({$_eval['evaluation_feedback_id']},this)' type='checkbox' data-toggle='toggle' data-size='mini' data-onstyle='success' checked/>";
						} else {
								$is_featured = "<input id='is_featured_{$_eval['evaluation_feedback_id']}' data-on='yes' class='status status_1' data-off='no' onchange='change_feature({$_eval['evaluation_feedback_id']},this)' type='checkbox' data-toggle='toggle' data-size='mini' data-onstyle='success'/>";
						}

						// opertation buttons
						
						$operation_buttons = '';
						$edit_url = $this->generateUrl('admin_evaluation_editcomment', array('eval_id' => $_eval['evaluation_feedback_id']));
						$edit_str = 'eval_update_pager();return confirm("Are you sure you want to Edit?")';
						if(in_array("SURE11", $right_codes)){
							$operation_buttons .= "<a class='btn btn-info btn-xs' onclick='". $edit_str ."'  href='{$edit_url}' data-toggle='tooltip' data-placement='top' data-original-title='edit'> <i class='fa fa-pencil'></i></a>";
						}

						$write_url = $this->generateUrl('admin_evaluation_addadmincomment', array('eval_id' => $_eval['evaluation_feedback_id']));
						$write_str = 'return confirm("Are you sure you want to write comment?")';
						if(in_array("SAACRE13", $right_codes)){
							$operation_buttons .= "<a class='btn btn-success btn-xs' onclick='". $write_str ."'  href='{$write_url}' data-toggle='tooltip' data-placement='top' data-original-title='Add admin comment' style='margin-left:2px;'> <i class='fa fa-commenting'></i> </a>";
						}
						
						// delete button
						$delete_url = $this->generateUrl('admin_evaluation_deleteevaluation').'/'.$_eval['evaluation_feedback_id'];
						$delete_str = 'deleterow("' . $delete_url . '")';
						$delete_button = "<a onclick='' class='btn btn-danger btn-xs deleterow' href='javascript:void(0);' data-toggle='tooltip' data-placement='top' data-original-title='delete'><i class='fa fa-trash-o' onclick='" . $delete_str . "'></i></a>";
						
						// check if user has uploaded gallery or not
						$feedback_gallery = $this->getDoctrine()->getRepository('AdminBundle:Evaluationfeedbackgallery')->findBy(
							array(
								'evaluation_feedback_id' => $_eval['evaluation_feedback_id'],
								'is_deleted' => 0
							)
						);
						
						if(!empty($feedback_gallery)){
							$attachment = 'Yes';
						} else {
							$attachment = 'No';
						}

						$branch_name = '';
						$_sql = "SELECT branch_name FROM branch_master where main_branch_master_id = {$_eval['main_branch_id']} and is_deleted = 0 and language_id = 1";
						$branch_data = $this->firequery($_sql);
						if(!empty($branch_data)){
							//$branch_name = $branch_data[0]['branch_name'];
							$tempArray = [];
							foreach($branch_data as $_branch){
								$tempArray[] = $_branch['branch_name'];
							}

							if(!empty($tempArray)){
								$branch_name = implode(' / ', $tempArray);
							}
						}
						
						$evaluation[] = $start + $i++;
						$evaluation[] = "<span id='evaluation_tr_{$_eval['evaluation_feedback_id']}'>".$_eval['user_firstname']."</span>";
						$evaluation[] = $_eval['email'];
						$evaluation[] = $_eval['user_mobile'];
						$evaluation[] = $_eval['restaurant_name'];
						$evaluation[] = $branch_name;//$_eval['branch_name'];
						$evaluation[] = ucfirst($_eval['eval_type']);
						$evaluation[] = $is_featured;
						$eval_status = '';
						$final_status = ucfirst($_eval['eval_status']);
						
						if(strtolower($_eval['eval_status']) == 'under_evaluation'){
							$eval_status = "<label style='color:#0000ff'>{$final_status}</label>";
						} else if(strtolower($_eval['eval_status']) == 'rejected'){
							$eval_status = "<label style='color:#ff0000'>{$final_status}</label>";
						} else if(strtolower($_eval['eval_status']) == 'pending'){
							$eval_status = "<label style='color:#f39c12'>{$final_status}</label>";
						} else if(strtolower($_eval['eval_status']) == 'approved'){
							$eval_status = "<label style='color:#008000'>{$final_status}</label>";
						}
						//$evaluation[] = ucfirst($_eval['status']);
						$resStatus = ucfirst($_eval['res_status']);
						if($_eval['res_status'] == 'pending'){
							$resStatus = "<label class='res-pending'>{$resStatus}</label>";
						}
						$evaluation[] = $resStatus;
						$evaluation[] = $eval_status;
						$evaluation[] = $attachment;
						$evaluation[] = date('d-m-Y', strtotime($_eval['created_datetime']));
						$evaluation[] = $operation_buttons;
						if(in_array("SDRE12", $right_codes)){
							
							$delete_button .= "<label class='container1'>
								<input type='checkbox'>
								<span class='checkmark' onclick='del_response({$_eval['evaluation_feedback_id']});'></span>
							</label>";
							
							$evaluation[] = $delete_button;
						}
						$evaluation[] = "<script>$('.status').bootstrapToggle()</script>";
						$evaluation[] = "<script>function deleterow(href_url){var cnfm = confirm('Are you sure, you want to delete?');if(cnfm){var cnfm1 = confirm('Confirmation: Are you sure, you want to delete?'); if(cnfm1){ location.href=href_url; } return false;} return false;}</script>";
						$evaluation[] = $_eval['res_status'];

						$evaluation_list[] = $evaluation;
				}
		}

		// set pagination in session for goto last pagination clicked
        $session = $this->get('session');
        
          /* echo '<pre>';
          print_r($session->all());
          exit; */

        if (array_key_exists('update_eval_pagination_number', $session->all())) {
            $session->set('eval_pagination_number', $session->get('update_eval_pagination_number'));
            $session->remove('update_eval_pagination_number');
        } else {
            $start = $_REQUEST['start'];
            if (isset($start)) {
                if ($start > 1) {
                    if ($session->has('eval_per_page')) {
                        $session->set('eval_pagination_number', $start);
                    }
                } else {
                    $session->set('eval_pagination_number', 0);
                }
            }
        }
		
		$response = array(
			'draw' => $_REQUEST['draw'],
			'recordsTotal' => count($evaluation_count),
			'recordsFiltered' => count($evaluation_count),
			'data' => $evaluation_list
		);

		echo json_encode($response);exit;
	}

	/**
     * @Route("/setevalshowperpage")
     */
    public function setEvalShowPerPageAction(Request $request) {
        $show_per_page = $request->get('show_per_page');
        if (isset($show_per_page)) {
            $session = $this->get('session');
            $session->set('eval_per_page', $show_per_page);
            echo true;
        } else {
            echo false;
        }
        exit;
    }
	
    /**
     * @Route("/eval/updatepager")
     */
    public function evalupdatepagerAction(Request $request) {
        $eval_pager_id = $request->get('eval_pager_id');
        //if (isset($eval_pager_id)) {
            $session = $this->get('session');
            $eval_per_page = 10;
			if (array_key_exists('eval_per_page', $session->all())) {
                $eval_per_page = $session->get('eval_per_page');
                $start = $eval_pager_id;
                $length = $session->get('eval_per_page');
            }
            if ($eval_per_page <= 0) {
                $eval_per_page = 10;
				$start = 10;
                $length = 10;
            }
            $session->set('eval_page_start', $start);
            $session->set('eval_page_length', $length);
            $session->set('eval_pagination_number', ($eval_pager_id * $eval_per_page) - $eval_per_page);
            $session->set('update_eval_pagination_number', ($eval_pager_id * $eval_per_page) - $eval_per_page);
            echo true;
        /* } else {
            echo false;
        } */
        exit;
    }

    /**
     * @Route("/FeaturedEvaluationList",name="admin_feature_evaluation_list")
     * @Template()
     */
    public function featuredEvalAction()
    {
		$em = $this->getDoctrine()->getManager();
		$evaluation_get_sql = "select branch.branch_name,user.user_firstname,cat.category_name,food_type.food_type_name,
								rest.restaurant_name,eval.is_featured,eval.eval_type, eval.comments,eval.show_featured,eval.evaluation_feedback_id,eval.service_level,eval.dining_level,eval.atmosphere_level,
								eval.price_level,eval.clean_level,eval.invoice_image_id,eval.status,eval.created_datetime,
								media_library_master.media_location , media_library_master.media_name
								from evaluation_feedback eval left join user_master user on user.user_master_id=eval.user_id
								left join category_master cat on eval.category_id=cat.main_category_id
								left join food_type_master food_type on eval.food_type_id= food_type.main_food_type_id
								left join restaurant_master rest on eval.restaurant_id=rest.main_restaurant_id
								left join branch_master branch on branch.main_branch_master_id=eval.main_branch_id 
								left join media_library_master ON user.user_image = media_library_master.media_library_master_id								
								where eval.is_deleted=0 and rest.language_id = '1' and eval.is_featured='yes' and branch.is_deleted = 0 group by eval.evaluation_feedback_id";
		$con = $em->getConnection();
		$stmt = $con->prepare($evaluation_get_sql);
		$stmt->execute();
		$evaluation_data = $stmt->fetchAll();

		$live_path = $this->container->getParameter('live_path');		
		if(!empty($evaluation_data)){
			foreach($evaluation_data as $key=>$val){
				if($val['media_location']!=='' && $val['media_name']!=''){
					$user_image=$live_path . $val['media_location'] . "/".$val['media_name'];
				} else {
					$user_image=$live_path.'/bundles/Resource/default.png';
				}			
				$evaluation_data[$key]['user_image'] = $user_image;				
				$evaluation_data[$key]['comments'] = $this->changeToEmoji($val['comments']);				
						
			}
		}

//		echo"<pre>";print_r($evaluation_data);exit;
		$get_level_sql = "select * from level_master where status='active' and is_deleted=0";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($get_level_sql);
		$stmt->execute();
		$level_data = $stmt->fetchAll();

		/* echo "<pre>";
		print_r($evaluation_data);
		exit; */

		return (array('evaluation_data'=>$evaluation_data,'level_data'=>$level_data));
	}

	/**
	* @Route("/evaluation/featured/ajaxupdatestatus")
	*/
	public function ajaxupdatestatusAction(Request $request){

		if($request->get('eval_id')){
			$em = $this->getDoctrine()->getManager();
			$evaluation = $em->getRepository(Evaluationfeedback::class)->find($request->get('eval_id'));

			$current_status = $evaluation->getShow_featured();
			if($current_status == 'active'){
				$evaluation->setShow_featured('inactive');
			} else {
				$evaluation->setShow_featured('active');
			}

			$em->flush();
			echo 'true';exit;
		}
		echo 'false';exit;
	}

    /**
     * @Route("/evaluation_change_featured",name="admin_evaluation_list_featured_change")
     * @Template()
     */
    public function changeFeatureAction(Request $req)
    {
		if($req->request->get('status') == 'true'){
			$em = $this->getDoctrine()->getManager();
			$evaluation = $em->getRepository(Evaluationfeedback :: class)->findOneBy(array('evaluation_feedback_id'=>$req->request->get('evaluation_id')));

			if($evaluation != null){
				$evaluation->setIs_featured('yes');
				$evaluation->setShow_featured('active');
				$em->flush();
			}
		}
		if($req->request->get('status') == 'false'){
			$em = $this->getDoctrine()->getManager();
			$evaluation = $em->getRepository(Evaluationfeedback :: class)->findOneBy(array('evaluation_feedback_id'=>$req->request->get('evaluation_id')));

			if($evaluation != null){
				$evaluation->setIs_featured('no');
				$evaluation->setShow_featured('inactive');
				$em->flush();
			}
		}
		return new Response('done');
	}

    /**
     * @Route("/view_evaluation/{evaluation_id}",name="viewevaluation_evaluation")
     * @Template()
     */
    public function viewevaluationAction($evaluation_id) {
		
		$right_codes = $this->userrightsAction();

        $sql_rest_data = "select * from restaurant_master where is_deleted=0 and language_id=1 and (status='active' or status='open_soon')";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

        $query = "SELECT evaluation_feedback.*,branch_master.branch_name,restaurant_master.restaurant_name,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_lastname FROM evaluation_feedback 
		LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.main_restaurant_id 		
		LEFT JOIN branch_master ON evaluation_feedback.main_branch_id=branch_master.main_branch_master_id  		
		LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.main_food_type_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.main_category_id=evaluation_feedback.category_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.evaluation_feedback_id='$evaluation_id' group by evaluation_feedback_id";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->execute();
        $evaluation = $stmt->fetchAll();
        $result = array();
		$evaluation_gallery = array();
		$evaluation_invoice = array();
        if (!empty($evaluation)) {

			foreach ($evaluation as $key => $value) {
						$query_eval_gallery = "SELECT media.* from evaluation_feedback_gallery eval_feed LEFT JOIN media_library_master media
									ON eval_feed.media_id = media.media_library_master_id where evaluation_feedback_id='".$value['evaluation_feedback_id']."' and eval_feed.is_deleted=0" ;
						$con = $em->getConnection();
						$stmt_1 = $con->prepare($query_eval_gallery);
						$stmt_1->execute();
						$evaluation_gallery = $stmt_1->fetchAll();
						$invoice_image = "SELECT * FROM `media_library_master` where media_library_master_id = '".$value['invoice_image_id']."' and is_deleted = 0" ;
						$con = $em->getConnection();
						$stmt_2 = $con->prepare($invoice_image);
						$stmt_2->execute();
						$evaluation_invoice = $stmt_2->fetchAll();

                $result[] = array(
                    'user_id' => $value['user_firstname'] . ' ' . $value['user_lastname'],
                    'category_name' => $value['category_name'],
                    'food_type_name' => $value['food_type_name'],
                    'restaurant_id' => $value['restaurant_name'],
                    'branch_name' => $value['branch_name'],
                    'restaurant_id1' => $value['restaurant_id'],
                    'comments' => $this->changeToEmoji($value['comments']),
                    'status' => $value['status'],
                    'evaluation_feedback_id' => $value['evaluation_feedback_id'],
                    'service_level' => $value['service_level'],
                    'dining_level' => $value['dining_level'],
                    'atmosphere_level' => $value['atmosphere_level'],
                    'price_level' => $value['price_level'],
                    'clean_level' => $value['clean_level'],
                    'invoice_image_id' => $value['invoice_image_id']
                );
				$gallery = $evaluation_gallery;
            }
        }

/*		echo"<pre>";
		print_r($gallery);exit; */
        return array('rest_final_data' => $result,'restaurantmaster'=>$Restaurantmaster,'gallery'=>$gallery,'invoice'=>$evaluation_invoice, 'right_codes' => $right_codes);
    }

	/**
     * @Route("/deleteevaluation/{eval_id}", defaults = {"eval_id" = ""})
     * @Template()
     */
    public function deleteevaluationAction($eval_id, Request $request) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDRE12", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        if (isset($eval_id) && $eval_id != '') {

            $em = $this->getDoctrine()->getManager();
            $evaluation = $em->getRepository(Evaluationfeedback::class)->findOneBy(
				array(
					'evaluation_feedback_id' => $eval_id,
					'is_deleted' => 0
				)
			);

            if (!empty($evaluation)) {

				#remove points
				$competition_user_relation = $em->getRepository("AdminBundle:Competitionuserrelation")->findBy([
					"related_table_name" => "evaluation_feedback",
					"related_id" => $evaluation->getEvaluation_feedback_id()
				]);

				if($competition_user_relation){
					foreach($competition_user_relation as $_competition_user_relation){
						$_competition_user_relation->setIs_deleted(1);
						$em->flush();
					}
				}

				$evaluation->setIs_deleted(1);
				$em->flush();

				## remove evaluation gallery
				$entity = $this->getDoctrine()->getManager();
				$evaluation_gallery = $entity->getRepository(Evaluationfeedbackgallery::class)->findBy(
					array(
						'evaluation_feedback_id' => $eval_id,
						'is_deleted' => 0
					)
				);

				if(!empty($evaluation_gallery)){
					foreach($evaluation_gallery as $_evaluation){
						$_evaluation->setIs_deleted(1);
						$entity->flush();
					}
				}


				$this->get('session')->getFlashBag()->set('success_msg', "Evaluation deleted successfully");
            } else {
                $this->get('session')->getFlashBag()->get('error_msg', "Evaluation not found");
            }
        } else {
            $this->get('session')->getFlashBag()->get('error_msg', 'Something went wrong');
        }

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

	/**
     * @Route("/delete-bulk-evaluation")
     * @Template()
     */
    public function deletebulkevaluationAction(Request $request) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDRE12", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$eval_ids = '';
		$postData = $request->request->all();
		if(!array_key_exists('eval_ids', $postData)){
			$data = array(
				'success' => false,
				'message' => 'Parameter not found'
			);
			
			echo json_encode($data);exit;
		}
		
		$eval_ids_str = $postData['eval_ids'];
		$eval_ids = explode(',', $eval_ids_str);
		
		if(!empty($eval_ids)){
			$em = $this->getDoctrine()->getManager();
			
			foreach($eval_ids as $eval_id){
				$evaluation = $em->getRepository('AdminBundle:Evaluationfeedback')->findOneBy(
					array(
						'evaluation_feedback_id' => $eval_id,
						'is_deleted' => 0
					)
				);

				$success = '';
				$message = '';
				if (!empty($evaluation)) {

				#remove points
				$competition_user_relation = $em->getRepository("AdminBundle:Competitionuserrelation")->findBy([
					"related_table_name" => "evaluation_feedback",
					"related_id" => $evaluation->getEvaluation_feedback_id()
				]);

				if($competition_user_relation){
					foreach($competition_user_relation as $_competition_user_relation){
						$_competition_user_relation->setIs_deleted(1);
						$em->flush();
					}
				}
									
					
					$em->remove($evaluation);
					$em->flush();

					## remove evaluation gallery
					$entity = $this->getDoctrine()->getManager();
					$evaluation_gallery = $entity->getRepository('AdminBundle:Evaluationfeedbackgallery')->findBy(
						array(
							'evaluation_feedback_id' => $eval_id,
							'is_deleted' => 0
						)
					);

					if(!empty($evaluation_gallery)){
						foreach($evaluation_gallery as $_evaluation){
							$entity->remove($_evaluation);
							$entity->flush();
						}
					}


					$success = true;
					$message = "Removed successfully";
				}
			} 
			
			$data = array(
				'success' => $success,
				'message' => $message
			);
		} else {
			$data = array(
				'success' => false,
				'message' => 'No value found'
			);
		}

        echo json_encode($data);exit;
    }
	
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
        /**
     * @Route("/changeevalRest",name="change_rest_evalt")
     * @Template()
     */
    public function changeevalRestAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $rest_details = $em->getRepository(Evaluationfeedback :: class)->findBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $req->request->get('evaluation_feedback_id')));
        if (empty($rest_details)) {
            $rest_details = array();
        }

        foreach ($rest_details as $rest) {

                $rest->setRestaurant_id($req->request->get('res_id'));

            $em->flush();
        }
        exit('done');
    }

    /**
     * @Route("/addAdminComment/{eval_id}")
     * @Template()
     */
    public function addadmincommentAction($eval_id) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SAACRE13", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$admin_comment_details = null;
        $admin_comment_details = $this->getDoctrine()->getManager()->getRepository(Evaluationadmincomment :: class)->findOneBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $eval_id));
        return array('admin_comment_details' => $admin_comment_details,'evaluation_id'=>$eval_id);
    }

    /**
     * @Route("/saveAdminComment")
     * @Template()
     */
    public function saveadmincommentAction(Request $req) {
		$admin_comment_details = null;
		$em = $this->getDoctrine()->getManager();
        $admin_comment_details = $em->getRepository(Evaluationadmincomment :: class)->findOneBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $req->request->get('eval_id')));
        if($admin_comment_details){
			/* check for access */
			$right_codes = $this->userrightsAction();
			$rights_search = in_array("SEUCRE14", $right_codes);
			if ($rights_search != 1) {
				$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
				return $this->redirect($this->generateUrl('admin_dashboard_index'));
			}
			/* end: check for access */
			
			$admin_comment_details->setAdmin_comment($req->request->get('admin_comment'));
			$em->flush();
			$this->get('session')->getFlashBag()->set('success_msg', 'Comment updated successfully');
			//return $this->redirectToRoute('admin_evaluation_list');
		}else{
			/* check for access */
			$right_codes = $this->userrightsAction();
			$rights_search = in_array("SAACRE13", $right_codes);
			if ($rights_search != 1) {
				$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
				return $this->redirect($this->generateUrl('admin_dashboard_index'));
			}
			/* end: check for access */
			
//			print_r($_REQUEST);exit;
			$admin_eval_comment = new Evaluationadmincomment();
			$admin_eval_comment->setAdmin_comment($req->request->get('admin_comment'));
			$admin_eval_comment->setEvaluation_feedback_id($req->request->get('eval_id'));
			$em = $this->getDoctrine()->getManager();
			$em->persist($admin_eval_comment);
			$em->flush();
			$this->get('session')->getFlashBag()->set('success_msg', 'Comment Inserted successfully');
			//return $this->redirectToRoute('admin_evaluation_list');
		}

		//return $this->redirect($this->generateUrl('viewevaluation_restaurant', array('main_restaurant_id' => $req->get('eval_id'))));
		return $this->redirect($this->generateUrl('admin_evaluation_list'));
    }

	/**
     * @Route("/editcommentdb")
     * @Template()
     */
    public function editcommentdbAction(Request $req) {
	//	print_r($_REQUEST);exit;
	
		if($req->request->all()){
			
			
			#eval_type changes
			/* $speed_level = $req->get('speed-level');
			$packaging_level = $req->get('packaging-level');
			$dining_level = $req->get('dining-level');
			$atmosphere_level = $req->get('atmosphere-level');
			
			if($req->request->get('evaluation_type') == 'dining'){
				$speed_level = 0;
				$packaging_level = 0;
			}
			
			if($req->request->get('evaluation_type') == 'delivery'){
				$dining_level = 0;
				$atmosphere_level = 0;
			} */
			
			$dining_level = $req->get('dining-level');
			$price_level = $req->get('price-level');
			$clean_level = $req->get('cleanliness-level');
			
			$speed_level = 0;
			$packaging_level = 0;
			$atmosphere_level = 0;
			$service_level = 0;
			
			if($req->request->get('evaluation_type') == 'dining'){
				$atmosphere_level = $req->get('atmosphere-level');
				$service_level = $req->get('service-level');
			}
			
			if($req->request->get('evaluation_type') == 'delivery'){
				$speed_level = $req->get('speed-level');
				$packaging_level = $req->get('packaging-level');
			}
			
			
			#eval_type changes ends
			
			$eval_id = $req->get('eval_id');
			$request_data = $req->request->all();

			$em = $this->getDoctrine()->getManager();
			$evaluation = $em->getRepository('AdminBundle:Evaluationfeedback')->findOneBy([
				'evaluation_feedback_id' => $eval_id,
				'is_deleted' => 0
			]);

			$invoice_media_id = '';

			if(!empty($_FILES)){
				$invoice_media = $_FILES['invoice']['name'];

				if (isset($invoice_media) && $invoice_media != '') {

					$upload_dir = $this->container->getParameter('upload_dir1');
					$location = '/Resource/Restaurant-Evaluate';

					//$allowedExts = explode(',', $mediatype->getMedia_type_allowed());
					$temp = explode('.', $_FILES['invoice']['name']);
					$extension = end($temp);

					$invoice_media_id = $this->mediaupload($_FILES['invoice'], $upload_dir, $location, 4);
				}
			}

			if(!empty($evaluation)){

				$oldResId = $evaluation->getRestaurant_id();

				## send mail for approve or reject status
				if($evaluation->getStatus() != $req->get('evaluation_status')){

					if($req->get('evaluation_status') == 'approved'){
						$em = $this->getDoctrine()->getManager();
						#make point countable for user in competition_user_relation
						$competition_user_relation = $em->getRepository("AdminBundle:Competitionuserrelation")->findBy([
							"related_table_name" => "evaluation_feedback",
							"is_deleted" => 1,
							"related_id" => $evaluation->getEvaluation_feedback_id()
						]);

						if($competition_user_relation){
							foreach($competition_user_relation as $_competition_user_relation){
								$_competition_user_relation->setIs_deleted(0);
								$em->flush();
							}
						}
					}else{

						$competition_user_relation = $em->getRepository("AdminBundle:Competitionuserrelation")->findBy([
							"related_table_name" => "evaluation_feedback",
							"related_id" => $evaluation->getEvaluation_feedback_id()
						]);

						if($competition_user_relation){
							foreach($competition_user_relation as $_competition_user_relation){
								$_competition_user_relation->setIs_deleted(1);
								$em->flush();
							}
						}

					}

					if(in_array($req->get('evaluation_status'), array('approved', 'rejected'))){

						## get email id of user
						$user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findOneBy([
							'user_master_id' => $evaluation->getUser_id(),
							'is_deleted' => 0
						]);

						if(!empty($user)){
							
							/*
							$email_message = "<b>Evaluation : </b>{$req->get('comment')}<br><b>date :</b> {}<br><br>Your Evaluation is Approved<br>Please Check";
							 */

							// Always set content-type when sending HTML email
							$email_address = $user->getEmail();

							$shreyak = $this->container->getParameter('live_path');

							$Config_live_site = $this->container->getParameter('live_path');

							$email_message1 = '';
							$note_message = '';
							
							if($req->get('evaluation_status') == 'approved'){
								$email_message1 = "<span style='color:#0000ff;'>Your Evaluation is Approved</span><br><br>";
							
								$note_message = "Your Evaluation is Approved.";
								
								$email_message1 .= "<span style='color:#0000ff;'>Your evaluation is now a valuable addition to Shrayek. It will be helpful for the people of Kuwait to know how the restaurant is in terms of service, delivery, dining, atmosphere and price.</span><br><br>";
							
							}else{
								$email_message1 = "<span style='color:#0000ff;'>We have reviewed your evaluation and have not been approved because of the following reason</span><br><br>";
								
								$note_message = "We have reviewed your evaluation and have not been approved because of the following reason , Reason Is ".$req->get('rejected_reason');
								
								$email_message1 .= "<span style='color:#0000ff;'>Reason : </span><br><br>";
								
								$email_message1 .= "<span style='color:#0000ff;'>".$req->get('rejected_reason')."</span><br><br>";
							}
							
							#send Notification to use as well
							$send_to = 'customer';
							
							$general_notification = new Generalnotification();
							$general_notification->setNotification_type('app_alert');
							$general_notification->setTitle("Evaluation Review");
							$general_notification->setMessage($note_message);
							$general_notification->setImage_id(0);
							$general_notification->setUser_master_id(0);
							$general_notification->setSend_to($send_to);
							if (!empty($this->get('session')->get('domain_id'))) {
								$general_notification->setDomain_id($this->get('session')->get('domain_id'));
							}
							$general_notification->setCreate_date(date("Y-m-d H:i:s"));
							$general_notification->setIs_deleted(0);
							
							$em->persist($general_notification);
							$em->flush();

							$notification_id_send = $general_notification->getGeneral_notification_id();
					
							$notificationDetails = $em->getRepository("AdminBundle:Generalnotification")->findOneBy(["general_notification_id"=>$notification_id_send]);
							
							if($notificationDetails){
								$Config_live_site = $this->container->getParameter('live_path');
						
								$title = $notificationDetails->getTitle();
								$message_notification= $notificationDetails->getMessage();
								$notification_type = $notificationDetails->getNotification_type();
								$media_id = $notificationDetails->getImage_id();
								
								$code = 0;
								
								if (!empty($notification_type) && $notification_type == 'healthtip') {
									$code = '7';
								} elseif (!empty($notification_type) && $notification_type == 'app_alert') {
									$code = '11';
								} else {
									$code = '10';
								}
								
								if(!empty($media_id)){
									$media_library = $this->getDoctrine()
											->getManager()
											->getRepository('AdminBundle:Medialibrarymaster')
											->findOneBy(array('media_library_master_id' => $media_id, 'is_deleted' => 0));
									if($media_library){
										$response = array(
											"notification_title" => $title,
											"notification_image" => $Config_live_site . $media_library->getMedia_location() . "/" . $media_library->getMedia_name()
										);		
										$message = json_encode(array("detail" => $message_notification, "code" => $code, "response" => $response));								
									}else{
										$message = json_encode(array("detail" => $message_notification, "code" => $code, "response" => $title));
									}
									
								}else{
									$message = json_encode(array("detail" => $message_notification, "code" => $code, "response" => $title));
								}
								
								$domain_id = 1;
								$app_id = 'CUST';
								$user_array [] = $user->getUser_master_id();
								$user_apns_id = array();
								
								$gcm_regids = $this->find_gcm_regid($user_array);
						
								$message = json_decode($message);
								
								$apppushnotificationmaster = new Apppushnotificationmaster();
								$apppushnotificationmaster->setStatus('unread');
								$apppushnotificationmaster->setDevice_name('android');
								$apppushnotificationmaster->setApp_id($app_id);
								$apppushnotificationmaster->setDomain_id($domain_id);
								$apppushnotificationmaster->setDevice_token(0);

								$apppushnotificationmaster->setUser_id($user->getUser_master_id());
								$apppushnotificationmaster->setLanguage_id(1);
								$apppushnotificationmaster->setDevice_id(0);

								$apppushnotificationmaster->setData($message_notification);
								$apppushnotificationmaster->setCode($code);
								$apppushnotificationmaster->setTable_name('general_notification');
								$apppushnotificationmaster->setTable_id($notification_id_send);
								$apppushnotificationmaster->setResponse($message->response);
								$apppushnotificationmaster->setDatetime(date("Y-m-d H:i:s"));
								$apppushnotificationmaster->setIs_deleted(0);
								$em = $this->getDoctrine()->getManager();
								$em->persist($apppushnotificationmaster);
								$em->flush();
								
							}
							
							#send Notification to use as well done
							
							
							$shreyak = $this->container->getParameter('live_path');

#email html template
$email_message = "<!doctype html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1.0'><title></title><style type='text/css'>body {margin: 0;}body, table, td, p, a, li, blockquote {font-family: arial,sans-serif;font-style: normal;font-weight: 400;}
button{width:90%;}@media screen and (max-width:600px) {body, table, td, p, a, li, blockquote {font-family: arial,sans-serif;}
table {width: 100%;}.footer {height: auto !important;max-width: 48% !important;width: 48% !important;}table.responsiveImage {height: auto !important;max-width: 30% !important;width: 30% !important;}table.responsiveContent {height: auto !important;max-width: 66% !important;width: 66% !important;}.top {height: auto !important;max-width: 48% !important;width: 48% !important;}.catalog {margin-left: 0%!important;}}@media screen and (max-width:480px) {body, table, td, p, a, li, blockquote {font-family: arial,sans-serif;}table {width: 100% !important;border-style: none !important;}.footer {height: auto !important;max-width: 96% !important;width: 96% !important;}.table.responsiveImage {height: auto !important;max-width: 96% !important;width: 96% !important;}.table.responsiveContent {height: auto !important;max-width: 96% !important;width: 96% !important;}.top {height: auto !important;max-width: 100% !important;width: 100% !important;}
.catalog {margin-left: 0%!important;}}</style></head><body><table width='100%' cellspacing='0' cellpadding='0'><tbody><tr><td><table width='70%'  align='center' cellpadding='0' cellspacing='0'><tbody><tr><td><table bgcolor='#FFB220' class='top' width='100%'  align='left' cellpadding='0' cellspacing='0' style='padding:10px 10px 10px 10px;border-radius:5px'><tbody><tr><td style='font-size: 12px; color:#FFF; padding-left:20px; font-family: arial,sans-serif;'><img src='".$Config_live_site."/bundles/design/images/shrayek.png' style='height:50px;width:50px' ></td></tr></tbody></table></td></tr><tr> <td><table width='100%'  align='left' cellpadding='0' cellspacing='0'><tr><td style='font-size: 14px; font-weight: bold; padding: 20px; color: #222; font-family: arial,sans-serif;'>Thank You For Your Evaluation , we have Review your evaluation.</td></tr><tr> <td align='center' style='font-size: 16px; font-weight:300; color: #929292; font-family: arial,sans-serif;'>

                    <h4>Your Evaluation on Shrayek (".$shreyak.") : ".$req->get('comment')."</h4>
                    
                    </td>
                  </tr>
                  <tr>
                    <td align='center' style='font-size: 16px; font-weight:300; color: #929292; font-family: arial,sans-serif;'>Evaluation Created at : ".$evaluation->getCreated_datetime()."
                    
                    </td>
</tr><tr><td>".$email_message1."</td></tr><tr><td style='font-size: 0; line-height: 0;' height='20'><table width='96%' align='left'  cellpadding='0' cellspacing='0'><tr><td style='font-size: 0; line-height: 0;' height='20'>&nbsp;</td></tr></table></td></tr><tr> <td align='left' style='font-size: 14px; font-style: normal; font-weight: bold; color: #222; line-height: 1.8; text-align:justify; padding:10px 20px 0px 20px; font-family:arial,sans-serif;'>Please Check <a href='".$shreyak."'>".$shreyak."</a></td></tr></table></td></tr><tr> <td style='font-size: 0; line-height: 0;' height='10'><table width='96%' align='left'  cellpadding='0' cellspacing='0'><tr><td style='font-size: 0; line-height: 0;' height='20'>&nbsp;</td></tr></table></td></tr><tr bgcolor='#FFB220'><td><table class='footer' width='48%'  align='left' cellpadding='0' cellspacing='0'><tr><td><p align='center'  style='font-size: 14px; font-weight:300; line-height: 10px; color: #FFF; font-family: arial,sans-serif;'>&copy; Copyright Shrayek</p></td></tr></table></td></tr></tbody></table></td></tr></tbody></table></body></html>
					";
					

#email html template done

							$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
							$headers .= 'From: <shreyak.com>' . "\r\n";

							@mail($email_address,"Thank you for your recent evaluation.",$email_message,$headers);
						}
					}
				}

				$evaluation->setPrice_level($price_level);
				$evaluation->setClean_level($clean_level);
				$evaluation->setDining_level($dining_level);
				
				$evaluation->setAtmosphere_level($atmosphere_level);
				$evaluation->setService_level($service_level);

				$evaluation->setDelivery_speed_level($speed_level);
				$evaluation->setPackaging_level($packaging_level);
				
				$evaluation->setComments(urlencode($req->get('comment')));
				$evaluation->setEval_type($req->get('evaluation_type'));
//				$evaluation->setCategory_id($req->get('category_id'));
//				$evaluation->setFood_type_id($req->get('food_type_id'));
				$evaluation->setRestaurant_id($req->get('restaurant_id'));
				$evaluation->setMain_branch_id($req->get('branch_id'));
				$evaluation->setStatus($req->get('evaluation_status'));
				
				if(array_key_exists('rejected_reason', $req->request->all())){
					$evaluation->setRejected_reason($req->get('rejected_reason'));
				}				
				if(isset($invoice_media_id) && $invoice_media_id != ''){
					$evaluation->setInvoice_image_id($invoice_media_id);
				}

				$em->flush();

				// delete older restaurant if new eval move to other restuarant
				if($req->get('restaurant_id') != $oldResId){
					$oldRes = $em->getRepository('AdminBundle:Restaurantmaster')->findBy([
						'main_restaurant_id' => $oldResId,
						'status' => 'pending',
						'is_deleted' => 0
					]);
	
					if(!empty($oldRes)){
						foreach($oldRes as $_res){
							$_res->setIs_deleted(1);
							$em->flush();
						}
					}
				}
			}
		}

		$referer = $req->headers->get('referer');
		return $this->redirect($referer);
	}

	/**
     * @Route("/editcomment/{eval_id}")
     * @Template()
     */
    public function editcommentAction($eval_id, Request $req) {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SURE11", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */

		$sql_rest_data = "select * from restaurant_master where is_deleted=0 and language_id=1 and (status='active' or status='open_soon' or status='pending')";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

		$sql_cat_data = "select * from category_master where is_deleted=0 and language_id=1 and category_status='active'";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt1 = $con->prepare($sql_cat_data);
        $stmt1->execute();
        $Categorymaster = $stmt1->fetchAll();

		$category_list = array();
		$category_query = "SELECT * FROM category_master where is_deleted = 0  and language_id = 1 group by main_category_id";
        $category_list_arr = $this->firequery($category_query);
        if (!empty($category_list_arr)) {
            foreach ($category_list_arr as $cakey => $cval) {
                $category_list[] = array(
                    "category_master_id" => $cval['category_master_id'],
                    "category_name" => $cval['category_name'],
                    "main_category_id" => $cval['main_category_id']
                );
            }
        }

		// food type list
        $foodtype_repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster');
        $foodtype_list = $foodtype_repository->findBy(array('is_deleted' => 0, 'language_id' => 1));

        $query = "SELECT evaluation_feedback.*,restaurant_master.restaurant_name,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_lastname FROM evaluation_feedback 
		LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.main_restaurant_id 		
		LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.main_food_type_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.main_category_id=evaluation_feedback.category_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.evaluation_feedback_id='$eval_id' group by evaluation_feedback_id";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->execute();
        $evaluation = $stmt->fetchAll();
        $result = array();
		$evaluation_gallery = array();
		$evaluation_invoice = array();
		
		$gallery = null;
		
        if (!empty($evaluation)) {

			foreach ($evaluation as $key => $value) {
						$query_eval_gallery = "SELECT media.*, evaluation_feedback_gallery_id from evaluation_feedback_gallery eval_feed LEFT JOIN media_library_master media
									ON eval_feed.media_id = media.media_library_master_id where evaluation_feedback_id='".$value['evaluation_feedback_id']."' and eval_feed.is_deleted=0" ;
						$con = $em->getConnection();
						$stmt_1 = $con->prepare($query_eval_gallery);
						$stmt_1->execute();
						$evaluation_gallery = $stmt_1->fetchAll();
						$invoice_image = "SELECT * FROM `media_library_master` where media_library_master_id = '".$value['invoice_image_id']."' and is_deleted = 0" ;
						$con = $em->getConnection();
						$stmt_2 = $con->prepare($invoice_image);
						$stmt_2->execute();
						$evaluation_invoice = $stmt_2->fetchAll();
				
#get branches of restaurant selected
						$branch_list = array();
						$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and main_restaurant_id = '".$value['restaurant_id']."' group by main_branch_master_id";
						$branch_list_arr = $this->firequery($branch_query);
						if (!empty($branch_list_arr)) {
							foreach ($branch_list_arr as $cakey1 => $cval1) {
								$branch_list[] = array(
									"main_branch_master_id" => $cval1['main_branch_master_id'],
									"branch_name" => $cval1['branch_name'],
								);
							}
						}
				
						

                $result[] = array(
                    'user_fullname' => $value['user_firstname'] . ' ' . $value['user_lastname'],
                    'category_name' => $value['category_name'],
                    'eval_type' => $value['eval_type'],
                    'food_type_name' => $value['food_type_name'],
                    'restaurant_name' => $value['restaurant_name'],
                    'restaurant_id1' => $value['restaurant_id'],
                    'main_branch_id' => $value['main_branch_id'],
                    'category_id' => $value['category_id'],
                    'food_type_id' => $value['food_type_id'],
                    'rejected_reason' => $value['rejected_reason'],
                  	'comments' => $this->changeToEmoji($value['comments']),
                    'status' => $value['status'],
                    'evaluation_feedback_id' => $value['evaluation_feedback_id'],
                    'service_level' => $value['service_level'],
                    'dining_level' => $value['dining_level'],
                    'atmosphere_level' => $value['atmosphere_level'],
                    'price_level' => $value['price_level'],
                    'clean_level' => $value['clean_level'],
                    'packaging_level' => $value['packaging_level'],
                    'delivery_speed_level' => $value['delivery_speed_level'],
                    'invoice_image_id' => $value['invoice_image_id'],
					'branch_list'=>$branch_list
                );
				$gallery = $evaluation_gallery;
            }
        }

		/* echo '<pre>';
		print_r($gallery);
		exit; */

		if(!empty($result)){
			$result = $result[0];
		}

		if(!empty($evaluation_invoice)){
			$evaluation_invoice = $evaluation_invoice[0];
		}

		// level list
        $level_repository = $this->getDoctrine()->getRepository('AdminBundle:Levelmaster');
        $level_list = $level_repository->findBy(array('is_deleted' => 0,"language_id"=>1));
		
		return array(
			'eval_id' => $eval_id,
			'level_list' => $level_list,
			'rest_final_data' => $result,
			'restaurantmaster' => $Restaurantmaster,
			'category_list' => $category_list,
			'foodtype_list' => $foodtype_list,
			'media_library_master' => $gallery,
			'invoice' => $evaluation_invoice
		);
	}

	public function changeToEmoji($src){
		$replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $src);

		$result = mb_convert_encoding($replaced, "UTF-8", "HTML-ENTITIES");
		
//		$result = mb_convert_encoding($result, 'utf-8', 'utf-16');

		return urldecode($src);
	}
	/**
     * @Route("/evaluation/getRestBranch")
     * @Template()
     */
    public function getRestBranchAction(Request $request) {
		
		$html = '';
		$main_restaurant_id = $request->request->get('main_restaurant_id');
		$branch_list = array();
		$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and status = 'active' and main_restaurant_id = '".$main_restaurant_id."' group by main_branch_master_id";
		$branch_list_arr = $this->firequery($branch_query);
		if (!empty($branch_list_arr)) {
			foreach ($branch_list_arr as $cakey1 => $cval1) {
				$html .= '<option value="'.$cval1['main_branch_master_id'].'">'.$cval1['branch_name'].'</option>';
			}
		}		

		return new Response($html);
		
	}	
	
	/**
     * @Route("/evaluation/addfeedbackgallery")
     * @Template()
     */
    public function addfeedbackgalleryAction(Request $request) {

		if ($request->request->all()) {

            $em = $this->getDoctrine()->getManager();

            $media_library_master = new Medialibrarymaster();
            $media_library_master->setMedia_type_id($request->get('media_type_id'));
            $media_library_master->setMedia_title($request->get('img_name'));
            $media_library_master->setMedia_location("/bundles/design/uploads/bulkupload");
            $media_library_master->setMedia_name($request->get('img_name'));
            $media_library_master->setCreated_on(date("Y-m-d H:i:s"));
            $media_library_master->setIs_Deleted(0);
            $em->persist($media_library_master);
            $em->flush();

            $eval_feedback = new Evaluationfeedbackgallery();
			$eval_feedback->setEvaluation_feedback_id($request->get('eval_id'));
			$eval_feedback->setMedia_id($media_library_master->getMedia_library_master_id());
			$eval_feedback->setMedia_type_id(1);
			$eval_feedback->setIs_deleted(0);
			$em = $this->getDoctrine()->getManager();
			$em->persist($eval_feedback);
			$em->flush();

            $media_file = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Medialibrarymaster')
                    ->findOneBy(array('media_library_master_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));

            if (!empty($media_file)) {
                $content = array(
                    "name" => $media_file->getMedia_name(),
                    "path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "thumbnail_path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "media_library_master_id" => $media_library_master->getMedia_library_master_id()
                );
                return new Response(json_encode($content));
            }

        }
        return new Response(false);
	}

	/**
     * @Route("/evaluation/feedback-images/remove")
     * @Template()
     */
    public function removeFeedbackImagesAction(Request $request) {
		$media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'), 'is_deleted' => 0));

        $gallery_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Evaluationfeedbackgallery')
                ->findOneBy(array('evaluation_feedback_gallery_id' => $request->get('feedback_gallery_id'), 'is_deleted' => 0));

		if (!empty($media_file) && !empty($gallery_file)) {
            $media_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $gallery_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $delete_img = $this->container->getParameter('root_dir') . '/' . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();

            //unlink($delete_img);

            return new Response("true");
        } else {
            return new Response("false");
        }
	}

	/**
     * @Route("/evaluation/getFilteredFoodtype/{category_id}", defaults={"category_id":""})
     * @Template()
     */
    public function getFilteredFoodtypeAction($category_id) {
		$language_id = 1;
        if (isset($category_id) && $category_id != 0) {
            $rel_repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypecategoryrelation');
            $cat_foodtype_relation = $rel_repository->findBy(
                    array(
                        'main_category_id' => $category_id,
                        'is_deleted' => 0
                    )
            );
//			print_r($cat_foodtype_relation);exit;
            $html = '';
            $food_type_list = array();
            if (!empty($cat_foodtype_relation)) {
                $repository = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster');

				$unique_foodtype_rel = array();
				if(!empty($cat_foodtype_relation)){
					foreach ($cat_foodtype_relation as $_relation) {
						$unique_foodtype_rel[] = $_relation->getMain_food_type_id();
					}
					$unique_foodtype_rel = array_unique($unique_foodtype_rel);
				}

				if(!empty($unique_foodtype_rel)){
					foreach ($unique_foodtype_rel as $foodtype_id) {
						$food_type = $repository->findOneBy(array('is_deleted'=>0,'main_food_type_id'=>$foodtype_id,'language_id' => $language_id));
						if (!empty($food_type)) {
							$food_type_list[] = $food_type;
						}
					}
				}

                //$html = "<select id='first-disabled' class='selectpicker form-control' data-hide-disabled='true' data-live-search='true'>";
                if (!empty($food_type_list)) {
                    foreach ($food_type_list as $_food_type) {
                        $html .= "<option value='" . $_food_type->getMain_food_type_id() . "'>{$_food_type->getFood_type_name()}</option>";
                    }
                }
                //$html .= "</select>";
                echo $html;
                exit;
            }
        } else {
            $html = '';
            $food_type_list = array();
            $food_type_list_all = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster')->findBy(array('is_deleted' => 0 ,'language_id' => $language_id));
            foreach ($food_type_list_all as $food_ty) {
                $food_type_list[] = $food_ty;
            }

			if (!empty($food_type_list)) {
                foreach ($food_type_list as $_food_t) {
                    $html .= "<option value='" . $_food_t->getMain_food_type_id() . "'>{$_food_t->getFood_type_name()}</option>";
                }
            }
            //$html .= "</select>";
            echo $html;
            exit;
        }
        echo 'false';
        exit;
    }

	/**
     * @Route("/evaluation/getFilteredRestaurants/{category_id}/{foodtype_id}", defaults={"category_id":"","foodtype_id":""})
     * @Template()
     */
    public function getFilteredRestaurantsAction($category_id, $foodtype_id) {

		$language_id = 1;
		if (isset($category_id) && $category_id != 0 && isset($foodtype_id) && $foodtype_id != 0) {
            $rel_repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantfoodtyperelation');
            $res_cat_foodtype_relation = $rel_repository->findBy(
                    array(
                        'main_caetgory_id' => $category_id,
                        'main_foodtype_id' => $foodtype_id,
                        'is_deleted' => 0
                    )
            );

			$html = '';
            $restaurant_list = array();
            if (!empty($res_cat_foodtype_relation)) {

                $repository = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster');
                foreach ($res_cat_foodtype_relation as $_relation) {
                    $restaurant = $repository->findOneBy(
                        array(
                            'restaurant_master_id'=>$_relation->getRestaurant_id(),
                            'language_id' => $language_id
                            ));
                    if (!empty($restaurant)) {
                        $restaurant_list[] = $restaurant;
                    }
                }

                //$html = "<select id='first-disabled' class='selectpicker form-control' data-hide-disabled='true' data-live-search='true'>";
                if (!empty($restaurant_list)) {
                    foreach ($restaurant_list as $_restaurant) {
                        $html .= "<option value='" . $_restaurant->getRestaurant_master_id() . "'>{$_restaurant->getRestaurant_name()}</option>";
                    }
                }
                //$html .= "</select>";
                echo $html;
                exit;
            }
        } else {
            $html = '';
            $restaurant_list = array();
            $rest_all = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findBy(array('is_deleted' => 0, 'status' => 'active', 'language_id' => $language_id));
            foreach ($rest_all as $restaurant) {
                $restaurant_list[] = $restaurant;
            }
//				print_r($rest_all);exit;
            if (!empty($restaurant_list)) {
                foreach ($restaurant_list as $_restaurant) {
                    $html .= "<option value='" . $_restaurant->getRestaurant_master_id() . "'>{$_restaurant->getRestaurant_name()}</option>";
                }
            }
            //$html .= "</select>";
            echo $html;
            exit;
        }
        echo 'false';
        exit;
    }
}
