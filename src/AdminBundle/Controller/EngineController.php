<?php
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AdminBundle\Entity\Rightmaster;
use AdminBundle\Entity\Rolerightmaster;
use AdminBundle\Entity\Userrolemaster;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Colormaster;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Enginemaster;




use Symfony\Component\HttpFoundation\JsonResponse;


/**
* @Route("/admin")
*/


class EngineController extends BaseController
{
	public function __construct()
    {
		parent::__construct();
        $obj = new BaseController();
		$obj->checkSessionAction();
    }



	/**
	 * @Route("/Manage-Engine")
     * @Template()
     */
    public function enginelistAction()
    {

    		ini_set('xdebug.var_display_max_depth', 200);
		ini_set('xdebug.var_display_max_children', 256);
		ini_set('xdebug.var_display_max_data', 1024);
    		$em = $this->getDoctrine()->getManager();
			$con = $em->getConnection();
			$st = $con->prepare("SELECT main_engine_id FROM engine_master WHERE is_deleted = 0 GROUP BY main_engine_id");
			$st->execute();
			$enginelist = $st->fetchAll();
			$data = array();
			if(!empty($enginelist)){
			foreach($enginelist as $k=>$v){
					$em = $this->getDoctrine()->getManager();
					$connection = $em->getConnection();
					$statement = $connection->prepare("SELECT * FROM engine_master WHERE is_deleted = 0 AND main_engine_id = ".$v['main_engine_id']);
					$statement->execute();
					$comp = $statement->fetchAll();


						for($i=0;$i<count($comp);$i++)
						{
							$get_parent_name = $this->getDoctrine()
								->getManager()
								->getRepository('AdminBundle:Enginemaster')
								->findOneBy(array(
										'engine_master_id'=>$comp[$i]['engine_master_id'],
										'language_id'=>$comp[$i]['language_id'],
										'is_deleted'=>0)
									);

						}


					$data[] = array("engine_id"=>$v['main_engine_id'],"data"=>$comp);
				}
			}
			$live_path=$this->getparams()->live;

			$langs = $this->getDoctrine()
					->getManager()
					->getRepository("AdminBundle:Languagemaster")
					->findBy(array("is_deleted"=>0));
					//var_dump($data);exit;
		return array("enginelist"=>$data,"languages"=>$langs,"live_path"=>$live_path);
    }



    /**
     * @Route("/Add-Update-Engine/{main_engine_id}",defaults={"main_engine_id"=""})
     * @Template()
     */
	 public function addengineAction($main_engine_id)
	 {
		$langs = $this->getDoctrine()
					->getManager()
					->getRepository("AdminBundle:Languagemaster")
					->findBy(array("is_deleted"=>0));

		$car_brand = $this->getDoctrine()
            ->getManager()
            ->getRepository("AdminBundle:Carmaster")
            ->findBy(array("is_deleted"=>0));

        $car = $this->getDoctrine()
            ->getManager()
            ->getRepository("AdminBundle:Maincarmaster")
            ->findBy(array("is_deleted"=>0));

		if(!empty($main_engine_id)){
			$em = $this->getDoctrine()->getManager();
    		$conn = $em->getConnection();
    		$st = $conn->prepare("SELECT * FROM engine_master WHERE main_engine_id = ".$main_engine_id." AND is_deleted = 0");
    		$st->execute();
    		$load_engine = $st->fetchAll();


			return array('engine'=>$load_engine,'engine_id'=>$main_engine_id,"languages"=>$langs,"car_brand"=>$car_brand,"car"=>$car);
		} else {

			return array("languages"=>$langs,"car_brand"=>$car_brand,"car"=>$car);

		}
	}

	/**
     * @Route("/addenginedb")
     * @Template()
     */
	 public function addenginedbAction(){
		$session = new Session();
		$user_id = $session->get("user_id");
		$language_id = $_REQUEST['language_master_id'];
		if(isset($_REQUEST['add'])){

			$engine_name = $_REQUEST['engine_name'];
			$engine_bhp = $_REQUEST['engine_bhp'];
			$engine_mpg = $_REQUEST['engine_mpg'];
			$price = $_REQUEST['price'];

			//$brand_id = $_REQUEST['brand_id'];
			//$car_id = $_REQUEST['car_id'];
			$engine_main_id = '';

			if(isset($_REQUEST['engine_main_id']) && !empty($_REQUEST['engine_main_id']))
			{

				$engine_main_id = $_REQUEST['engine_main_id'];
				$get_plan = $this->getDoctrine()
								->getManager()
								->getRepository('AdminBundle:Enginemaster')
								->findOneBy(array(
										'main_engine_id'=>$engine_main_id,
										'language_id'=>$language_id,
										'is_deleted'=>0)
									);


				if(count($get_plan) > 0){
					$em = $this->getDoctrine()->getManager();
					$update = $em->getRepository('AdminBundle:Enginemaster')
								->find($get_plan->getEngine_master_id());
					$update->setEngine_name($engine_name);
					$update->setEngine_bhp($engine_bhp);
					$update->setEngine_mpg($engine_mpg);
					$update->setPrice($price);

					//$update->setBrand_id($brand_id);
					//$update->setCar_id($car_id);

					$em->flush();

					  // for set same value for other languages start
                        $get_plans = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Enginemaster')
                        ->findBy(array(
                                'main_engine_id' => $engine_main_id,
                                'is_deleted' => 0)
                        );
                        if(!empty($get_plans))
                        {
                            foreach ($get_plans as $value) {
                                $em = $this->getDoctrine()->getManager();

                                $value->setEngine_bhp($engine_bhp);
								$value->setEngine_mpg($engine_mpg);
								$value->setPrice($price);

                                $em->flush();
                            }
                        }
                        // for set same value for other languages end
					//-------------------------------- Common Changes -----------------------------

					//-------------------------------- Common Changes -----------------------------
					$this->get('session')->getFlashBag()->set('success_msg','Engine Updated Successfully!');
					return $this->redirect($this->generateUrl('admin_engine_addengine',array("domain"=>$this->get('session')->get('domain'))).'/'.$engine_main_id);
				}
else {
					$Enginemaster = new Enginemaster();

					$Enginemaster->setEngine_name($engine_name);
					$Enginemaster->setEngine_bhp($engine_bhp);
					$Enginemaster->setEngine_mpg($engine_mpg);
					$Enginemaster->setPrice($price);
					//$Enginemaster->setBrand_id($brand_id);
					//$Enginemaster->setCar_id($car_id);

					$Enginemaster->setLanguage_id($language_id);
					$Enginemaster->setMain_engine_id($engine_main_id);
					$Enginemaster->setIs_deleted(0);



					$em = $this->getDoctrine()->getManager();
					$em->persist($Enginemaster);
					$em->flush();

					// for set same value for other languages start
                        $get_plans = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Enginemaster')
                        ->findBy(array(
                                'main_engine_id' => $engine_main_id,
                                'is_deleted' => 0)
                        );
                        if(!empty($get_plans))
                        {
                            foreach ($get_plans as $value) {
                                $em = $this->getDoctrine()->getManager();

                                $value->setEngine_bhp($engine_bhp);
								$value->setEngine_mpg($engine_mpg);
								$value->setPrice($price);


                                $em->flush();
                            }
                        }
                        // for set same value for other languages end
					if(isset($engine_main_id) && !empty($engine_main_id))
					{
						$this->get('session')->getFlashBag()
							->set('success_msg','Engine Inserted Successfully!');
						return $this->redirect($this
							->generateUrl('admin_engine_addengine',array("domain"=>$this->get('session')->get('domain'))).'/'.$engine_main_id);
					}
					else {
						$this->get('session')->getFlashBag()->set('error_msg','Engine Insertion Failed!');
						return $this->redirect($this->generateUrl('admin_engine_addengine',array("domain"=>$this->get('session')->get('domain'))));
					}
				}
			} else {
					$Enginemaster = new Enginemaster();

					$Enginemaster->setEngine_name($engine_name);
					$Enginemaster->setEngine_bhp($engine_bhp);
					$Enginemaster->setEngine_mpg($engine_mpg);
					$Enginemaster->setPrice($price);
					//$Enginemaster->setBrand_id($brand_id);
					//$Enginemaster->setCar_id($car_id);
					$Enginemaster->setLanguage_id($language_id);
					$Enginemaster->setMain_engine_id(0);
					$Enginemaster->setIs_deleted(0);


					$em = $this->getDoctrine()->getManager();
					$em->persist($Enginemaster);
					$em->flush();


				$engine_main_id = $Enginemaster->getEngine_master_id();

				if(isset($engine_main_id) && !empty($engine_main_id))
				{
					$em = $this->getDoctrine()->getManager();
					$update = $em->getRepository('AdminBundle:Enginemaster')->find($engine_main_id);
					$update->setMain_engine_id($engine_main_id);
					$em->flush();
					$this->get('session')->getFlashBag()->set('success_msg','Engine Inserted Successfully!');
					return $this->redirect($this->generateUrl('admin_engine_addengine',array("domain"=>$this->get('session')->get('domain'))).'/'.$engine_main_id);
				} else {
					$this->get('session')->getFlashBag()->set('error_msg','Engine Insertion Failed!');
					return $this->redirect($this->generateUrl('admin_engine_addengine',array("domain"=>$this->get('session')->get('domain'))));
				}

			}
		}
	 }



    /**
     * @Route("/DeleteEngine/{engine_main_id}",defaults={"engine_main_id"=""})
     * @Template()
     */
	 public function deleteengineAction($engine_main_id){

	if(!empty($engine_main_id)){
			$id = $engine_main_id;
			$list = $this->getDoctrine()
					->getManager()
					->getRepository("AdminBundle:Enginemaster")
					->findBy(array("is_deleted"=>0,"main_engine_id"=>$id));
			if(!empty($list)){
				foreach($list as $k=>$v){
					$em = $this->getDoctrine()->getManager();
					$update = $em->getRepository("AdminBundle:Enginemaster")->find($v->getEngine_master_id());
					$update->setIs_deleted(1);
					$em->flush();
				}
			}
			$this->get('session')->getFlashBag()->set('success_msg','Engine Deleted Successfuly!');
			return $this->redirect($this->generateUrl('admin_engine_enginelist',array("domain"=>$this->get('session')->get('domain'))));
		}
	}


}
