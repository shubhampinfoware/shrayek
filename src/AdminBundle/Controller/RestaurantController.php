<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Addressmaster;
use AdminBundle\Entity\Restaurantmaster;
use AdminBundle\Entity\Restaurantgallery;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Foodtypemaster;
use AdminBundle\Entity\Restaurantmenu;
use AdminBundle\Entity\Restaurantoffer;
use AdminBundle\Entity\Evaluationfeedback;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Entity\Restaurantcategoryrelation;
use AdminBundle\Entity\Restaurantfoodtyperelation;
use AdminBundle\Entity\Branchmaster;
use AdminBundle\Entity\Branchgallery;

/**
 * @Route("/admin")
 */
class RestaurantController extends BaseController {

    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

	/**
     * @Route("/update_res_food_catrelation")
     * @Template()
     */
    public function update_res_food_catrelationAction() {
		$sql = "select * from food_type_category_relation where is_deleted = 1";
		$deleted_record = $this->firequery($sql);
		
		if(!empty($deleted_record)){
			$entity = $this->getDoctrine()->getManager();
			foreach($deleted_record as $_record){
				echo 'Record : '.$_record['main_food_type_id'].'<br>';
				$delete_res_food_rel = $entity->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy(
					array(
						'main_foodtype_id' => $_record['main_food_type_id'],
						'main_caetgory_id' => $_record['main_category_id'],
					)
				);
				
				if(!empty($delete_res_food_rel)){
					foreach($delete_res_food_rel as $_deleteRel){
						$_deleteRel->setIs_deleted(1);
						$entity->flush();
					}
				}
			}
		}
		
		exit('restaurant foodtype category relation removed');
	}
	
    /**
     * @Route("/Restaurant",name="restaurant_page")
     * @Template()
     */
    public function indexAction() {

        $right_codes = $this->userrightsAction();

        $rest_final_data = array();
        $em = $this->getDoctrine()->getManager();
        $language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }
        $sql_rest_data = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and language_id = 1 group by rest.main_restaurant_id order by rest.main_restaurant_id desc limit 0,10";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

        $main_restaurant_id = $menu = '';
        foreach ($Restaurantmaster as $Restaurantmaster_data) {

            $address = $this->getDoctrine()->getRepository('AdminBundle:Addressmaster')->findBy([
                'owner_id' => $Restaurantmaster_data['main_restaurant_id'],
                'is_deleted' => 0
            ]);

            $address_name = '';
            if (!empty($address)) {
				foreach($address as $_address){
					$address_name = $_address->getAddress_name() . ' ' . $_address->getAddress_name2();
				}
            }
            $Restaurantmaster_data['address_name'] = $address_name;

            $restaurant_data_lang_wise = array();
            /* if ($main_restaurant_id !== $Restaurantmaster_data['main_restaurant_id']) { */
            foreach ($language_list as $language) {
                $rest_name = '';
                $main_rest_id = '';
                $timings = '';
                $status = '';
                $main_rest_id = $Restaurantmaster_data['main_restaurant_id'];
                $em = $this->getDoctrine()->getManager();
                $lang_type = $em->getRepository(Restaurantmaster :: class)->findOneBy(array('is_deleted' => 0, 'language_id' => $language->getLanguage_master_id(), 'main_restaurant_id' => $Restaurantmaster_data['main_restaurant_id']));
                if ($lang_type) {
                    $rest_name = $lang_type->getRestaurant_name();
					$rest_name = stripslashes($rest_name);

                    $timings = $lang_type->getTimings();
                    $status = $lang_type->getStatus();
                }
                if ($Restaurantmaster_data['restaurant_menu'] != 0) {
                    $live_path = $this->container->getParameter('live_path');
                    $menu = $this->getImage($Restaurantmaster_data['restaurant_menu'], $live_path);
                }
                $restaurant_data_lang_wise [] = array('language_id' => $language->getLanguage_master_id(),
                    'rest_name' => $rest_name,
                    'main_rest_id' => $main_rest_id,
                    'timings' => $timings,
                    'status' => $status,
                    'menu' => $menu,
                    'phone_number' => $Restaurantmaster_data['phone_number'],
                    'address' => $Restaurantmaster_data['address_name'],
                    'image_id' => $Restaurantmaster_data['media_library_master_id'],
                    'media_name' => $Restaurantmaster_data['media_name']
                );
            }
            /* } */
            if (!empty($restaurant_data_lang_wise)) {
                $main_restaurant_id = $Restaurantmaster_data['main_restaurant_id'];
                $rest_final_data [] = $restaurant_data_lang_wise;
            }
        }

        return (array('languages' => $language_list, 'rest_final_data' => $rest_final_data, 'right_codes' => $right_codes));
        //return array();
    }

    /**
     * @Route("/restaurant_list")
     */
    public function restaurant_listAction(Request $request) {

        $right_codes = $this->userrightsAction();
        $DEFAULT_LANGUAGE_ID = 1;

        ini_set('xdebug.var_display_max_depth', 200);
        ini_set('xdebug.var_display_max_children', 256);
        ini_set('xdebug.var_display_max_data', 1024);

        $session = new Session();

        if (array_key_exists('search', $_REQUEST)) {
            $search_value = $_REQUEST['search']['value'];
			$index = strpos($search_value, "'");
			if($index != ''){
				$search_value = addslashes($search_value);
			}
        }

        /* echo '<pre>';
          print_r($session->all());
          exit; */

        if (array_key_exists('page_start', $session->all())) {
            $start = $session->get('page_start');
            $session->remove('page_start');
        } else {
            $start = $_REQUEST['start'];
        }

        if (array_key_exists('page_length', $session->all())) {
            $length = $session->get('page_length');
            $session->remove('page_length');
        } else {
            $length = $_REQUEST['length'];
        }

        $limit_sql = "LIMIT " . $start . "," . $length;
        $rest_final_data = array();
        $em = $this->getDoctrine()->getManager();
        $language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }
        $sql_rest_data1 = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.status != 'open_soon' group by rest.main_restaurant_id";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt1 = $con->prepare($sql_rest_data1);
        $stmt1->execute();
        $Restaurantmaster_count = $stmt1->fetchAll();

        if (isset($search_value) && $search_value != '') {
            $sql_rest_data = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.status != 'open_soon' and restaurant_name like '%{$search_value}%' group by rest.main_restaurant_id order by rest.main_restaurant_id desc {$limit_sql}";
        } else {
            $sql_rest_data = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.status != 'open_soon' group by rest.main_restaurant_id order by rest.main_restaurant_id desc {$limit_sql}";
        }

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

        $main_restaurant_id = $menu = '';
        foreach ($Restaurantmaster as $Restaurantmaster_data) {

            #getBrachCount
            $main_restaurant_id_1 =  $Restaurantmaster_data['main_restaurant_id'];

            // restaraunt menus photos
            $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantmenu::class);
            $menu_images = $repository->findBy(
                    array(
                        'restaurant_id' => $main_restaurant_id_1,
                        'is_deleted' => 0
                    )
            );
            $menu_files = array();
            if (!empty($menu_images)) {
                foreach ($menu_images as $menu_image) {
                    $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                    $menu_imag = $repository->find($menu_image->getMedia_id());

                    if (!empty($menu_imag)) {
                        $menu_files[] = array(
                            'main_media_id' => $menu_imag->getMedia_library_master_id(),
                            'media_name' => $menu_imag->getMedia_name(),
                            'main_gallery_id' => $menu_image->getRestaurant_menu_id(),
                            'media_location' => $menu_imag->getMedia_location()
                        );
                    }
                }
            }

            $is_image_exist = 'No';
            if(!empty($menu_files)){
                $is_image_exist = 'Yes';
            }

            $branch_sql = "select * from branch_master where main_restaurant_id = '".$main_restaurant_id_1."' and is_deleted = 0 group by main_branch_master_id";
            $branches = $this->firequery($branch_sql);

            $branchNameList = array();
            $branch_count = 0;

            $brnch_timing = '';
            $brnch_phone = '';
            $brnch_loc_address = '';
            $brnch_lat_lng = '';

            if(!empty($branches)){

                // loc address
                $brn_address = $this->getDoctrine()->getRepository('AdminBundle:Addressmaster')->findOneBy([
                    'main_address_id' => $branches[0]['branch_address_id'],
                    'is_deleted' => 0,
                    'language_id' => $DEFAULT_LANGUAGE_ID
                ]);

                if(!empty($brn_address)){
                    $brnch_loc_address = $brn_address->getAddress_name();
                    $brnch_lat_lng = $brn_address->getLat().' / '.$brn_address->getLng();
                }

                $brnch_timing = $branches[0]['timings'];
                $brnch_phone = $branches[0]['mobile_no'];

                foreach($branches as $_branch){
                    $branch_sql = "select main_branch_master_id, branch_name, main_branch_flag from branch_master where main_branch_master_id = {$_branch['main_branch_master_id']} and is_deleted = 0 order by language_id";
                    $branchesList = $this->firequery($branch_sql);

                    $isMainBranch = '';
                    $tempArr = array();
                    if(!empty($branchesList)){
                        foreach($branchesList as $_branchesList){
                            if($_branchesList['main_branch_flag'] == 1 or $_branchesList['main_branch_flag'] == '1'){
                                $isMainBranch = '<b>(Main Branch)</b>';
                            }
                            $tempArr[] = $_branchesList['branch_name'];
                        }
                    }

                    

                    $branchNameList[] = trim(implode(' / ', $tempArr).' '.$isMainBranch);
                }
                $branch_count = count($branches);
            }

            #getBranchCountDone

            $food_type_query = "select foodtype.main_food_type_id, foodtype.language_id, foodtype.food_type_name from restaurant_master rest, restaurant_foodtype_relation rel, food_type_master foodtype where foodtype.main_food_type_id = rel.main_foodtype_id and rel.restaurant_id = rest.main_restaurant_id and foodtype.language_id = 1 and rest.is_deleted = 0 and rel.is_deleted = 0 and foodtype.is_deleted = 0 and rest.main_restaurant_id = {$Restaurantmaster_data['main_restaurant_id']} group by food_type_master_id";
            $food_type_data = $this->firequery($food_type_query);

            $cuisine_name_list = '';
            if (!empty($food_type_data)) {
                foreach ($food_type_data as $_foodtype) {

                    $cuisine_name = '';

                    $foodtype = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster')->findBy(
                            array(
                                'main_food_type_id' => $_foodtype['main_food_type_id'],
                                'is_deleted' => 0
                            )
                    );

                    $loop_index = 1;
                    if (!empty($foodtype)) {
                        foreach ($foodtype as $_type) {
                            if ($loop_index == 1) {
                                $cuisine_name .= $_type->getFood_type_name();
                            } else {
                                $cuisine_name .= ' / ' . $_type->getFood_type_name();
                            }
                            $loop_index++;
                        }
                    }

                    $cuisine_name_list[] = array(
                        'main_food_type_id' => $_foodtype['main_food_type_id'],
                        'cuisine_name' => $cuisine_name
                    );
                }
            }

            $address = $this->getDoctrine()->getRepository('AdminBundle:Addressmaster')->findOneBy([
                'owner_id' => $Restaurantmaster_data['main_restaurant_id'],
                'is_deleted' => 0
            ]);

            $lat = '';
            $lng = '';
            $lat_lng = '';
            $address_name = '';
            if (!empty($address)) {
                $lat = $address->getLat();
                $lng = $address->getLng();
                if (trim($lat) != '') {
                    if (trim($lng) != '') {
                        $lat_lng = $lat . ' / ';
                    } else {
                        $lat_lng = $lat;
                    }
                }
                if (trim($lng) != '') {
                    $lat_lng .= $lng;
                }
                $address_name = $address->getAddress_name() . ' ' . $address->getAddress_name2();
				//$address_name = '';
            }

            $Restaurantmaster_data['address_name'] = $address_name;
            $Restaurantmaster_data['lat_lng'] = $lat_lng;
            $Restaurantmaster_data['branchNameList'] = $branchNameList;
            $Restaurantmaster_data['branch_count'] = $branch_count;
            $Restaurantmaster_data['cuisine_name_list'] = $cuisine_name_list;
            $Restaurantmaster_data['is_image_exist'] = $is_image_exist;

            $restaurant_data_lang_wise = array();
            /*  if ($main_restaurant_id !== $Restaurantmaster_data['main_restaurant_id']) { */
            foreach ($language_list as $language) {
                $rest_name = '';
                $main_rest_id = '';
                $timings = '';
                $status = '';
                $main_rest_id = $Restaurantmaster_data['main_restaurant_id'];
                $em = $this->getDoctrine()->getManager();
                $lang_type = $em->getRepository(Restaurantmaster :: class)->findOneBy(array('is_deleted' => 0, 'language_id' => $language->getLanguage_master_id(), 'main_restaurant_id' => $Restaurantmaster_data['main_restaurant_id']));
                if ($lang_type) {
                    $rest_name = $lang_type->getRestaurant_name();
					$rest_name = stripslashes($rest_name);
                    $timings = $lang_type->getTimings();
                    $status = $lang_type->getStatus();
                }
                if ($Restaurantmaster_data['restaurant_menu'] != 0) {
                    $live_path = $this->container->getParameter('live_path');
                    $menu = $this->getImage($Restaurantmaster_data['restaurant_menu'], $live_path);
                }

                $res_timing = '';
                if($brnch_timing != ''){
                    $res_timing = $brnch_timing;
                } else {
                    $res_timing = $timings;
                }

                $res_phone = '';
                if($brnch_phone != ''){
                    $res_phone = $brnch_phone;
                } else {
                    $res_phone = $Restaurantmaster_data['phone_number'];
                }

                $res_address = '';
                if($brnch_loc_address != ''){
                    $res_address = $brnch_loc_address;
                } else {
                    $res_address = $Restaurantmaster_data['address_name'];
                }

                $res_lat_lng = '';
                if($brnch_lat_lng != ''){
                    $res_lat_lng = $brnch_lat_lng;
                } else {
                    $res_lat_lng = $lat_lng;
                }

                $restaurant_data_lang_wise = array('language_id' => $language->getLanguage_master_id(),
                    'rest_name' => $rest_name,
                    'res_master_id' => $Restaurantmaster_data['restaurant_master_id'],
                    'main_rest_id' => $main_rest_id,
                    'timings' => $res_timing,
                    'is_image_exist' => $is_image_exist,
                    'status' => $Restaurantmaster_data['status'],
                    'menu' => $menu,
                    'phone_number' => $res_phone,
                    'address' => $res_address,
                    'lat_lng' => $res_lat_lng,
                    'branchNameList'=>$branchNameList,
                    'branch_count'=>$branch_count,
                    'cuisine_name_list' => $Restaurantmaster_data['cuisine_name_list'],
                    'image_id' => $Restaurantmaster_data['media_library_master_id'],
                    'media_name' => $Restaurantmaster_data['media_name']
                );
            }
            /*   } */
            if (!empty($restaurant_data_lang_wise)) {
                $main_restaurant_id = $Restaurantmaster_data['main_restaurant_id'];
                $rest_final_data [] = $restaurant_data_lang_wise;
            }
        }

        /* echo '<pre>';
          print_r($rest_final_data);
          exit; 
		*/
        $live_path = $this->container->getParameter('live_path');
        $all_restaurant = array();
        foreach ($rest_final_data as $_resData) {
            $_restaurant = array();
            $_restaurant[] = "<span class='avatar avatar-online'><img src='{$this->getImage($_resData['image_id'], $live_path)}'></span>";

            foreach ($language_list as $_language) {
                $lang_wise_res_name = $this->getDoctrine()
                        ->getRepository('AdminBundle:Restaurantmaster')
                        ->findOneBy(array('main_restaurant_id' => $_resData['main_rest_id'], 'is_deleted' => 0, 'language_id' => $_language->getLanguage_master_id()));

                if (!empty($lang_wise_res_name)) {
                    $_restaurant[] = stripslashes($lang_wise_res_name->getRestaurant_name());
                } else {
                    $_restaurant[] = '';
                }
            }

            $_restaurant[] = $_resData['timings'];

            /* $col_5 = '';
              if ($_resData['menu'] != '') {
              $col_5 = "<a target='_blank' href='{$_resData['menu']}'>Menu</a>";
              }
              $_restaurant[] = $col_5; */


            $_restaurant[] = $_resData['phone_number'];
            $_restaurant[] = $_resData['address'];
            $_restaurant[] = $_resData['lat_lng'];
            
            $branch_html = '';
            if(!empty($_resData['branchNameList'])){
                $branch_html = '<ul>';
                foreach($_resData['branchNameList'] as $_name){
                    $branch_html .= "<li style='list-style:unset;width: max-content;'>{$_name}</li>";
                }
                $branch_html .= '</ul>';
            }

            $_restaurant[] = $branch_html;
            $_restaurant[] = $_resData['branch_count'];

            $cuisine_list = '';
            if (!empty($_resData['cuisine_name_list'])) {
                $cuisine_list = '<ul class="cuisine_list">';
                foreach ($_resData['cuisine_name_list'] as $_cuisine) {
                    $cuisine_list .= "<li><span>{$_cuisine['cuisine_name']}</span></li>";
                }
                $cuisine_list .= '</ul>';
            }
            $_restaurant[] = $cuisine_list;
            $_restaurant[] = $_resData['is_image_exist'];

            $col_6 = '';
            if ($_resData['status'] == 'active') {
                $checked = 'checked';
            } else {
                $checked = '';
            }

            if ($_resData['status'] == 'pending') {
                $col_6 = '<label class="label label-info">Pending</label>';
            }
            if ($_resData['status'] == 'open_soon') {
                $col_6 = '<label class="label label-info">Open Soon</label>';
            }

            if ($_resData['status'] == 'active' or $_resData['status'] == 'inactive') {
                $col_6 .= "<input data-on='active' class='status status_{$_resData['main_rest_id']}' data-off='inactive' type='checkbox' onchange= 'ch_status({$_resData['main_rest_id']}, this);' data-toggle='toggle' data-size='mini' data-onstyle='success' {$checked} />";
            }

            $_restaurant[] = $col_6;
            $col_7 = '';

            if (!empty($right_codes)) {
                if (in_array("SUR8", $right_codes)) {
                    $edit_url = $this->generateUrl('add_restaurant') . '/' . $_resData['main_rest_id'] . '/old';
                    $edit_str = 'return confirm("Are you sure you want to Edit?")';
                    $col_7 .= "<a class='editcategory btn btn-info btn-xs' onclick='" . $edit_str . "' href='{$edit_url}' data-toggle='tooltip' data-placement='top' data-original-title='edit'><i class='fa fa-pencil-square-o'></i></a>";
                }
				
				if (in_array("SVBRN45", $right_codes)) {
					$edit_url = $this->generateUrl('add_restaurant') . '/' . $_resData['main_rest_id'] . '/old?tab=branch';
                    $edit_str = 'return confirm("Are you sure you want to View  Branch List?")';
                    $col_7 .= "<a class='editcategory btn btn-default btn-xs' onclick='" . $edit_str . "' href='{$edit_url}' data-toggle='tooltip' data-placement='top' data-original-title='Branches'><i class='fa fa-share-alt'></i></a>";
				}
            }

            if (!empty($right_codes)) {
                if (in_array("SVRE10", $right_codes)) {
                    $view_str = 'return confirm("Are you sure you want to view?")';
                    $view_url = $this->generateUrl('viewevaluation_restaurant') . '/' . $_resData['main_rest_id'];
                    $col_7 .= "<a class='btn btn-success btn-xs' onclick='" . $view_str . "' href='{$view_url}' data-toggle='tooltip' data-placement='top' data-original-title='view'><i class='fa fa-eye'></i></a>";
                }
            }
            if (!empty($right_codes)) {
                if (in_array("SARM15", $right_codes)) {
                    $addmenu_str = 'return confirm("Are you sure you want to Add Menu?")';
                    $addmenu_url = $this->generateUrl('add_menu') . '/' . $_resData['main_rest_id'] . '/1';
                    $col_7 .= "<a class='btn btn-primary btn-xs' onclick='" . $addmenu_str . "' href='{$addmenu_url}' data-toggle='tooltip' data-placement='top' data-original-title='Add Menu'><i class='fa fa-list-alt'></i></a>";
                }
            }
            if (!empty($right_codes)) {
                if (in_array("SARO17", $right_codes)) {
                    $addoffer_str = 'return confirm("Are you sure you want to Add Offers?")';
                    $addoffer_url = $this->generateUrl('add_offer') . '/' . $_resData['main_rest_id'];
                    $col_7 .= "<a class='btn btn-warning btn-xs' onclick='" . $addoffer_str . "' href='{$addoffer_url}' data-toggle='tooltip' data-placement='top' data-original-title='Add offers'><i class='fa fa-tags'></i></a>";
                }
            }

            $_restaurant[] = $col_7;

            //$delete_str = 'return confirm("Are you sure you want to Delete?")';
            $live_path = $this->container->getParameter('live_path');
            $delete_url = $this->generateUrl('delete_restaurant') . '/' . $_resData['main_rest_id'] . '/old';
            $delete_str = 'deleterow("' . $delete_url . '")';

            if (in_array("SDR8", $right_codes)) {
                $_restaurant[] = "<a class='deleterow btn btn-danger btn-xs' href='javascript:void(0);' data-toggle='tooltip' data-placement='top' data-original-title='delete' onclick='" . $delete_str . "'><i class='fa fa-trash-o'></i></a>";
            }

            $_restaurant[] = "<script>function deleterow(href_url){var cnfm = confirm('Are you sure, you want to delete?');if(cnfm){var cnfm1 = confirm('Confirmation: Are you sure, you want to delete?'); if(cnfm1){ location.href=href_url; } return false; }}</script>";

            $_restaurant[] = "<script>$('.status').bootstrapToggle()</script>";

            $all_restaurant[] = $_restaurant;
        }

        // set pagination in session for goto last pagination clicked
        $session = $this->get('session');
        /*
          echo '<pre>';
          print_r($session->all());
          exit; */

        if (array_key_exists('update_pagination_number', $session->all())) {
            $session->set('pagination_number', $session->get('update_pagination_number'));
            $session->remove('update_pagination_number');
        } else {
            $start = $_REQUEST['start'];
            if (isset($start)) {
                if ($start > 1) {
                    if ($session->has('restaurant_per_page')) {
                        $session->set('pagination_number', $start);
                    }
                } else {
                    $session->set('pagination_number', 0);
                }
            }
        }

        $res = array(
            'draw' => $_REQUEST['draw'],
            'recordsTotal' => count($Restaurantmaster_count),
            'recordsFiltered' => count($Restaurantmaster_count),
            'data' => $all_restaurant
        );

        echo json_encode($res);
        exit;
    }

    /**
     * @Route("/restaurant/open-soon")
     * @Template()
     */
    public function opensoonlistAction() {

        $right_codes = $this->userrightsAction();

        $rest_final_data = array();
        $em = $this->getDoctrine()->getManager();
        $language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }
        $sql_rest_data = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.status='open_soon' and rest.is_deleted=0 and language_id = 1 group by rest.main_restaurant_id order by rest.main_restaurant_id desc";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

        $main_restaurant_id = $menu = '';
        foreach ($Restaurantmaster as $Restaurantmaster_data) {

            $food_type_query = "select foodtype.main_food_type_id, foodtype.language_id, foodtype.food_type_name from restaurant_master rest, restaurant_foodtype_relation rel, food_type_master foodtype where foodtype.main_food_type_id = rel.main_foodtype_id and rel.restaurant_id = rest.main_restaurant_id and foodtype.language_id = 1 and rest.is_deleted = 0 and rel.is_deleted = 0 and foodtype.is_deleted = 0 and rest.main_restaurant_id = {$Restaurantmaster_data['main_restaurant_id']} group by main_food_type_id";
            $food_type_data = $this->firequery($food_type_query);

            $cuisine_name_list = '';
            if (!empty($food_type_data)) {
                foreach ($food_type_data as $_foodtype) {

                    $cuisine_name = '';

                    $foodtype = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster')->findBy(
                            array(
                                'main_food_type_id' => $_foodtype['main_food_type_id'],
                                'is_deleted' => 0
                            )
                    );

                    $loop_index = 1;
                    if (!empty($foodtype)) {
                        foreach ($foodtype as $_type) {
                            if ($loop_index == 1) {
                                $cuisine_name .= $_type->getFood_type_name();
                            } else {
                                $cuisine_name .= ' / ' . $_type->getFood_type_name();
                            }
                            $loop_index++;
                        }
                    }

                    $cuisine_name_list[] = array(
                        'main_food_type_id' => $_foodtype['main_food_type_id'],
                        'cuisine_name' => $cuisine_name
                    );
                }
            }

            $address = $this->getDoctrine()->getRepository('AdminBundle:Addressmaster')->findOneBy([
                'owner_id' => $Restaurantmaster_data['main_restaurant_id'],
                'is_deleted' => 0
            ]);

            $lat = '';
            $lng = '';
            $lat_lng = '';
            $address_name = '';
            if (!empty($address)) {
                $lat = $address->getLat();
                $lng = $address->getLng();
                if (trim($lat) != '') {
                    if (trim($lng) != '') {
                        $lat_lng = $lat . ' / ';
                    } else {
                        $lat_lng = $lat;
                    }
                }
                if (trim($lng) != '') {
                    $lat_lng .= $lng;
                }
                $address_name = $address->getAddress_name() . ' ' . $address->getAddress_name2();
            }
            $Restaurantmaster_data['address_name'] = $address_name;

            $restaurant_data_lang_wise = array();
            /* if ($main_restaurant_id !== $Restaurantmaster_data['main_restaurant_id']) { */
            foreach ($language_list as $language) {
                $rest_name = '';
                $main_rest_id = '';
                $timings = '';
                $status = '';
                $main_rest_id = $Restaurantmaster_data['main_restaurant_id'];
                $em = $this->getDoctrine()->getManager();
                $lang_type = $em->getRepository(Restaurantmaster :: class)->findOneBy(
                        array(
                            'is_deleted' => 0,
                            'language_id' => $language->getLanguage_master_id(),
                            'main_restaurant_id' => $Restaurantmaster_data['main_restaurant_id']
                        )
                );
                if ($lang_type) {
                    $rest_name = $lang_type->getRestaurant_name();

                    $timings = $lang_type->getTimings();
                    $status = $lang_type->getStatus();
                }
                if ($Restaurantmaster_data['restaurant_menu'] != 0) {
                    $live_path = $this->container->getParameter('live_path');
                    $menu = $this->getImage($Restaurantmaster_data['restaurant_menu'], $live_path);
                }
                $restaurant_data_lang_wise [] = array('language_id' => $language->getLanguage_master_id(),
                    'rest_name' => $rest_name,
                    'main_rest_id' => $main_rest_id,
                    'timings' => $timings,
                    'status' => $status,
                    'menu' => $menu,
                    'cuisine_name_list' => $cuisine_name_list,
                    'phone_number' => $Restaurantmaster_data['phone_number'],
                    'address' => $Restaurantmaster_data['address_name'],
                    'lat_lng' => $lat_lng,
                    'image_id' => $Restaurantmaster_data['media_library_master_id'],
                    'media_name' => $Restaurantmaster_data['media_name']
                );
            }
            /* } */
            if (!empty($restaurant_data_lang_wise)) {
                $main_restaurant_id = $Restaurantmaster_data['main_restaurant_id'];
                $rest_final_data [] = $restaurant_data_lang_wise;
            }
        }

        return (array('languages' => $language_list, 'rest_final_data' => $rest_final_data, 'right_codes' => $right_codes));
        //return array();
    }

    /**
     * @Route("/setshowperpage")
     */
    public function setShowPerPageAction(Request $request) {
        $show_per_page = $request->get('show_per_page');
        if (isset($show_per_page)) {
            $session = $this->get('session');
            $session->set('restaurant_per_page', $show_per_page);
            echo true;
        } else {
            echo false;
        }
        exit;
    }

    /**
     * @Route("/updatepager")
     */
    public function updatepagerAction(Request $request) {
        $pager_id = $request->get('pager_id');
        if (isset($pager_id)) {
            $session = $this->get('session');
            $restaurant_per_page = 10;
            if (array_key_exists('restaurant_per_page', $session->all())) {
                $restaurant_per_page = $session->get('restaurant_per_page');
                $start = $pager_id;
                $length = $session->get('restaurant_per_page');
            }
            if ($restaurant_per_page <= 0) {
                $restaurant_per_page = 10;
                $start = 10;
                $length = 10;
            }
            $session->set('page_start', $start);
            $session->set('page_length', $length);
            $session->set('pagination_number', ($pager_id * $restaurant_per_page) - $restaurant_per_page);
            $session->set('update_pagination_number', ($pager_id * $restaurant_per_page) - $restaurant_per_page);
            echo true;
        } else {
            echo false;
        }
        exit;
    }

    /**
     * @Route("/Newrestaurant")
     * @Template()
     */
    public function newrestaurantAction() {

        $rest_final_data = array();
        $em = $this->getDoctrine()->getManager();
        $language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }
        //$sql_rest_data = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.status='pending' order by rest.restaurant_master_id desc";
		$sql_rest_data = "select rest.*,media.*, user_firstname, user_lastname, user_bio from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id left join evaluation_feedback eval on rest.main_restaurant_id = eval.restaurant_id left join user_master user on user.user_master_id = eval.user_id where rest.is_deleted=0 and rest.status='pending' order by rest.restaurant_master_id desc";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

//		echo"<pre>";print_r($Restaurantmaster);exit;
        $main_restaurant_id = $menu = '';

		foreach ($Restaurantmaster as $Restaurantmaster_data) {
            $restaurant_data_lang_wise = array();
            if ($main_restaurant_id !== $Restaurantmaster_data['main_restaurant_id']) {
                foreach ($language_list as $language) {
                    $rest_name = '';
                    $timings = '';
                    $status = '';
                    $main_rest_id = $Restaurantmaster_data['main_restaurant_id'];
                    $em = $this->getDoctrine()->getManager();
                    $lang_type = $em->getRepository(Restaurantmaster :: class)->findOneBy(array('is_deleted' => 0, 'language_id' => $language->getLanguage_master_id(), 'main_restaurant_id' => $Restaurantmaster_data['main_restaurant_id']));
                    if ($lang_type) {
                        $rest_name = $lang_type->getRestaurant_name();

                        $timings = $lang_type->getTimings();
                        $status = $lang_type->getStatus();
                    }
                    if ($Restaurantmaster_data['restaurant_menu'] != 0) {
                        $live_path = $this->container->getParameter('live_path');
                        $menu = $this->getImage($Restaurantmaster_data['restaurant_menu'], $live_path);
                    }

					$user_fullname = "";
					if($Restaurantmaster_data['user_firstname'] == '' && $Restaurantmaster_data['user_lastname'] == null){
						$user_fullname = $Restaurantmaster_data['user_bio'];
					} else {
						$user_fullname = $Restaurantmaster_data['user_firstname'].' '.$Restaurantmaster_data['user_lastname'];
					}
                    
                    #getBranchNameHere as Its new Restaurant it has only one branch for that moment and name for both lang also same
                    $branchMaster = $em->getRepository(Branchmaster :: class)->findOneBy(array('is_deleted' => 0,'main_restaurant_id' => $Restaurantmaster_data['main_restaurant_id']));

                    $branch_name = '';

                    if($branchMaster){
//                        foreach($branchMaster as $_branchMaster){
                            $branch_name .= $branchMaster->getBranch_name()." / ";
//                        }

                    }
                    #getBranchNameHere as Its new Restaurant it has only one branch for that moment done
                    $restaurant_data_lang_wise [] = array('language_id' => $language->getLanguage_master_id(),
                        'rest_name' => $rest_name,
                        'branch_name' =>trim($branch_name," / "),
                        'main_rest_id' => $Restaurantmaster_data['main_restaurant_id'],
                        'timings' => $timings,
						'user_fullname' => $user_fullname,
                        'status' => $status,
                        'menu' => $menu,
                        'image_id' => $Restaurantmaster_data['media_library_master_id'],
                        'media_name' => $Restaurantmaster_data['media_name']
                    );
                }
            }

            if (!empty($restaurant_data_lang_wise)) {
                $main_restaurant_id = $Restaurantmaster_data['main_restaurant_id'];
                $rest_final_data [] = $restaurant_data_lang_wise;
            }
        }
		
		## restaurant list
        $restaurant_list = $this->getDoctrine()->getRepository('AdminBundle:Restaurantmaster')->findBy(
			array(
				'status' => 'active',
				'language_id' => 1,
				'is_deleted' => 0
			)
        );

		
        return (array('languages' => $language_list, 'rest_final_data' => $rest_final_data,"restaurant_list" => $restaurant_list));
    }

	public function getFoodTypeNames($id){
		
		$em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
		
		$get_food_type = "select food_type_name from food_type_master where main_food_type_id = {$id}";
		$stmt1 = $con->prepare($get_food_type);
		$stmt1->execute();
		$food_types_details12 = $stmt1->fetchAll();		
		
		$temp_arr = array();
		if(!empty($food_types_details12)){
			foreach($food_types_details12 as $_type){
				$temp_arr[] = $_type['food_type_name'];
			}
		}
		
		$name = '';
		if(!empty($temp_arr)){
			$name = implode(' / ',$temp_arr);
		}
		
		return $name;
	}
	
    /**
     * @Route("/addRestaurant/{main_restaurant_id}/{type}",defaults={"main_restaurant_id"=0,"type"=""},name="add_restaurant")
     * @Template()
     */
    public function addRestaurantAction($main_restaurant_id, $type, Request $request) {

        $tab = !empty($request->query->get('tab')) ? $request->query->get('tab') : '' ; 

        /* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SAR7", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
        /* end: check for access */

        //$session = new Session();

        $RestaurantExist = array();
        $RestaurantExist_cat = array();
        $RestaurantExist_food_cat = array();
        $food_types_details = array();
        //language list
        $em = $this->getDoctrine()->getManager();
        $language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }

        //category list
        $em = $this->getDoctrine()->getManager();
        $category_list1 = $em->getRepository(Categorymaster :: class)->findBy(array('is_deleted' => 0, 'language_id' => 1));

        if (empty($category_list1)) {
            $category_list = array();
        }
        if (!empty($category_list1)) {
            foreach ($category_list1 as $key => $val) {
                $categories1 = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted' => 0, 'language_id' => '2', 'main_category_id' => $val->getMain_category_id()));
                $ar = !empty($categories1) ? $categories1->getCategory_name() : '';
                $category_list[] = array('main_category_id' => $val->getMain_category_id(), 'category_name' => !empty($ar) ? $val->getCategory_name() . ' / ' . $ar : $val->getCategory_name());
            }
        }
        //food types and category
        /* $sql_food_type = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.language_id= '1' and food_type.is_deleted=0 and cat.is_deleted=0 group by ftr.main_food_type_id"; */
		
		$sql_food_type = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.is_deleted=0 and cat.is_deleted=0 group by ftr.main_category_id";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_food_type);
        $stmt->execute();
        $food_types_details1 = $stmt->fetchAll();

        if (!empty($food_types_details1)) {
            foreach ($food_types_details1 as $key => $val) {
				//$sql_food_type1 = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.is_deleted=0 and cat.is_deleted=0 and food_type.main_food_type_id='" . $val['main_food_type_id'] . "' group by ftr.main_food_type_id";
				
				$sql_food_type1 = "SELECT food_type.food_type_name, food_type.main_food_type_id, rel.main_category_id FROM food_type_category_relation rel, food_type_master food_type where rel.is_deleted=0 and rel.main_food_type_id = food_type.main_food_type_id and food_type.is_deleted=0 and rel.main_category_id = {$val['main_category_id']} group by rel.main_food_type_id";

				$stmt1 = $con->prepare($sql_food_type1);
                $stmt1->execute();
                $food_types_details12 = $stmt1->fetchAll();
				
				$foodTypeName = '';
				if(!empty($food_types_details12)){
					$temp_arr = array();
					foreach($food_types_details12 as $_Type){
						$foodTypeName = $this->getFoodTypeNames($_Type['main_food_type_id']);
						$food_types_details[] = array(
							'main_category_id' => $val['main_category_id'],
							'main_food_type_id' => $_Type['main_food_type_id'],
							'food_type_name' => $foodTypeName
						);
						
					}
				}
				
                //$ar = !empty($food_types_details12) ? $food_types_details12[0]['food_type_name'] : '';
            }
        }

		/* echo '<pre>';
		print_r($food_types_details);
		exit;
		 */
        //$sql_rest_exist = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.main_restaurant_id='" . $main_restaurant_id . "'";
        $sql_rest_exist = "SELECT rest.*, media.*,  address_master.address_name, address_master.owner_id ,address_master.lat ,address_master.lng FROM restaurant_master rest LEFT JOIN media_library_master media ON rest.logo_id = media.media_library_master_id LEFT JOIN address_master ON rest.address_id = address_master.address_master_id WHERE rest.is_deleted = 0 AND rest.main_restaurant_id='" . $main_restaurant_id . "'";
		
		$em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_exist);
        $stmt->execute();
        $RestaurantExist_data1 = $stmt->fetchAll();
		
		$RestaurantExist_data = array();
		
        if (empty($RestaurantExist_data1)) {
            $RestaurantExist_data = array();
        } else {
			
			foreach($RestaurantExist_data1 as $_res1){
				
				$address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findBy(
					array(
						"owner_id" => $main_restaurant_id,
						'language_id' => $_res1['language_id']
					)
				);
				
				if(!empty($address_info)){
					foreach($address_info as $_addr){
						$_res1['address_name'] = $_addr->getAddress_name();
					}
				}
				
				$_res1['restaurant_name'] = stripslashes($_res1['restaurant_name']);
				
				$RestaurantExist_data[] = $_res1;
			}

            $rest_cat_sql = "select rest_cat_rel.main_category_id,rest_cat_rel.is_deleted as res_cat_delete from restaurant_category_relation rest_cat_rel where rest_cat_rel.is_deleted = 0 and rest_cat_rel.restaurant_id='" . $main_restaurant_id . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($rest_cat_sql);
            $stmt->execute();
            $RestaurantExist_cat = $stmt->fetchAll();

            $rest_food_cat_sql = "select * from restaurant_foodtype_relation rest_food_cat where rest_food_cat.is_deleted=0 and rest_food_cat.restaurant_id='" . $main_restaurant_id . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($rest_food_cat_sql);
            $stmt->execute();
            $RestaurantExist_food_cat = $stmt->fetchAll();
        }

        // restaraunt gallery photos
        $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantgallery::class);
        $gallery_images = $repository->findBy(
                array(
                    'restaurant_id' => $main_restaurant_id,
                    'is_deleted' => 0
                )
        );
        // restaraunt menus photos
        $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantmenu::class);
        $menu_images = $repository->findBy(
                array(
                    'restaurant_id' => $main_restaurant_id,
                    'is_deleted' => 0
                )
        );
        $menu_files = array();
        if (!empty($menu_images)) {
            foreach ($menu_images as $menu_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $menu_imag = $repository->find($menu_image->getMedia_id());

                if (!empty($menu_imag)) {
                    $menu_files[] = array(
                        'main_media_id' => $menu_imag->getMedia_library_master_id(),
                        'media_name' => $menu_imag->getMedia_name(),
                        'main_gallery_id' => $menu_image->getRestaurant_menu_id(),
                        'media_location' => $menu_imag->getMedia_location()
                    );
                }
            }
        }
        $gallery_files = array();
        if (!empty($gallery_images)) {
            foreach ($gallery_images as $_gallery_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $gallery_img = $repository->find($_gallery_image->getImage_id());

                if (!empty($gallery_img)) {
                    $gallery_files[] = array(
                        'main_media_id' => $gallery_img->getMedia_library_master_id(),
                        'main_gallery_id' => $_gallery_image->getRestaurant_gallery_id(),
                        'media_name' => $gallery_img->getMedia_name(),
                        'media_location' => $gallery_img->getMedia_location()
                    );
                }
            }
        }
        $res_menu = $RestaurantAddress = '';
        if (!empty($RestaurantExist_data)) {
            $res_menu = $this->getImage($RestaurantExist_data[0]['restaurant_menu'], $this->container->getParameter('live_path'));
            $address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("owner_id" => $main_restaurant_id, 'is_deleted' => 0));
			
			if (!empty($address_info)) {
				
                $RestaurantAddress = array(
                    "address_name" => $address_info->getAddress_name(),
                    "lat" => $address_info->getLat(),
                    "lng" => $address_info->getLng()
                );
            }
        }
		
#getRestaurat Braches
		$sql_get_main_branches = "select main_branch_master_id,main_restaurant_id,branch_address_id,main_branch_flag,status,mobile_no,timings from branch_master where branch_master.main_restaurant_id='" . $main_restaurant_id . "' and branch_master.is_deleted = 0 group by main_branch_master_id order by main_branch_flag DESC ";
			
		$stmt21 = $con->prepare($sql_get_main_branches);
		$stmt21->execute();
		$main_branches = $stmt21->fetchAll();
		

		if(!empty($main_branches)){
			foreach($main_branches as $key1=>$value1){
				$lang_wise = null;
				$main_branch_id = $value1['main_branch_master_id'];
                $adress_id =  $value1['branch_address_id'];

                $address_info_branch = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->find($adress_id);
            
                $BranchAddress = null;

                if (!empty($address_info_branch)) {
                    
                    $BranchAddress = array(
                        "address_name" => $address_info_branch->getAddress_name(),
                        "lat" => $address_info_branch->getLat(),
                        "lng" => $address_info_branch->getLng()
                    );
                }

                $_sql = "SELECT branch.language_id,branch.branch_name,branch.description FROM branch_master branch WHERE branch.is_deleted = 0 AND branch.main_restaurant_id='" . $main_restaurant_id . "' and main_branch_master_id = '$main_branch_id' order by language_id asc";
                $branchList = $this->firequery($_sql);

                if(!empty($branchList)){
                    foreach($branchList as $_branch){
                        $lang_wise[] = array(
                            'name' => stripslashes($_branch['branch_name']),
                            'lang_id' => $_branch['language_id']
                        );
                    }
                }

				/*if($language){
					foreach($language as $lang){
						
						$sql_get_braches ="SELECT branch.language_id,branch.branch_name,branch.description FROM branch_master branch WHERE branch.is_deleted = 0 AND branch.main_restaurant_id='" . $main_restaurant_id . "' and main_branch_master_id = '$main_branch_id' and language_id = '".$lang->getLanguage_master_id()."'";
						$stmt12 = $con->prepare($sql_get_braches);
						$stmt12->execute();
						$rest_branches = $stmt12->fetchAll();
				
						if(!empty($rest_branches)){
							$lang_wise[] = array('name'=>stripslashes($rest_branches[0]['branch_name']),'lang_id'=>$lang->getLanguage_master_id());
                        }else{
							$lang_wise []= array('name'=>'-','lang_id'=>$lang->getLanguage_master_id());
						}
					}
                }*/
				$main_branches[$key1]['lang_wise'] = $lang_wise;	
				$main_branches[$key1]['BranchAddress'] = $BranchAddress;	
			}
		}
    
		$RestaurantExist = array(
            'res_menu' => $res_menu, 
            'rest_data' => $RestaurantExist_data,
            'rest_cat' => $RestaurantExist_cat,
            'rest_branches' => $main_branches,
            'rest_address' => $RestaurantAddress,
            'rest_food' => $RestaurantExist_food_cat
		);
        
        if(!empty($main_restaurant_id)){
       //     $this->get('session')->getFlashBag()->set('main_restaurant_id', $main_restaurant_id);
        }
		$referer = $request->headers->get('referer');
		
        return (array('tab'=>$tab,'rest_details' => $RestaurantExist, 'type' => $type, 'languages' => $language, 'media_library_master' => $gallery_files, 'categories' => $category_list, 'food_types' => $food_types_details, 'menu_files' => $menu_files, 'right_codes' => $right_codes));
    }

    /**
     * @Route("/addmenu/{main_restaurant_id}/{lang_id}",defaults={"main_restaurant_id"=0, "lang_id" =1},name="add_menu")
     * @Template()
     */
    public function addmenuAction($main_restaurant_id, $lang_id) {

        /* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SARM15", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
        /* end: check for access */

        $RestaurantExist = array();
        $RestaurantExist_cat = array();
        $RestaurantExist_food_cat = array();
        $food_types_details = array();
        //language list
        $em = $this->getDoctrine()->getManager();
        $language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }

        //category list
        $em = $this->getDoctrine()->getManager();
        $category_list1 = $em->getRepository(Categorymaster :: class)->findBy(array('is_deleted' => 0, 'language_id' => 1));

        if (empty($category_list1)) {
            $category_list = array();
        }
        if (!empty($category_list1)) {
            foreach ($category_list1 as $key => $val) {
                $categories1 = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted' => 0, 'language_id' => '2', 'main_category_id' => $val->getMain_category_id()));
                $ar = !empty($categories1) ? $categories1->getCategory_name() : '';
                $category_list[] = array('main_category_id' => $val->getMain_category_id(), 'category_name' => !empty($ar) ? $val->getCategory_name() . ' / ' . $ar : $val->getCategory_name());
            }
        }
        //food types and category
        $sql_food_type = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.language_id= '1' and food_type.is_deleted=0 and cat.is_deleted=0 group by ftr.main_food_type_id";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_food_type);
        $stmt->execute();
        $food_types_details1 = $stmt->fetchAll();

        if (!empty($food_types_details1)) {
            foreach ($food_types_details1 as $key => $val) {
                $sql_food_type1 = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.language_id= '2' and food_type.is_deleted=0 and cat.is_deleted=0 and food_type.main_food_type_id='" . $val['main_food_type_id'] . "' group by ftr.main_food_type_id";

                $stmt1 = $con->prepare($sql_food_type1);
                $stmt1->execute();
                $food_types_details12 = $stmt1->fetchAll();
                $ar = !empty($food_types_details12) ? $food_types_details12[0]['food_type_name'] : '';
                $food_types_details[] = array('main_category_id' => $val['main_category_id'], 'main_food_type_id' => $val['main_food_type_id'], 'food_type_name' => !empty($ar) ? $val['food_type_name'] . ' / ' . $ar : $val['food_type_name']);
            }
        }

        $sql_rest_exist = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.main_restaurant_id='" . $main_restaurant_id . "'";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_exist);
        $stmt->execute();
        $RestaurantExist_data = $stmt->fetchAll();
        if (empty($RestaurantExist_data)) {
            $RestaurantExist_data = array();
        } else {

            $rest_cat_sql = "select rest_cat_rel.main_category_id,rest_cat_rel.is_deleted as res_cat_delete from restaurant_category_relation rest_cat_rel where rest_cat_rel.is_deleted = 0 and rest_cat_rel.restaurant_id='" . $main_restaurant_id . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($rest_cat_sql);
            $stmt->execute();
            $RestaurantExist_cat = $stmt->fetchAll();

            $rest_food_cat_sql = "select * from restaurant_foodtype_relation rest_food_cat where rest_food_cat.is_deleted=0 and rest_food_cat.restaurant_id='" . $main_restaurant_id . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($rest_food_cat_sql);
            $stmt->execute();
            $RestaurantExist_food_cat = $stmt->fetchAll();
        }

        // restaraunt gallery photos
        $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantgallery::class);
        $gallery_images = $repository->findBy(
                array(
                    'restaurant_id' => $main_restaurant_id,
                    'is_deleted' => 0
                )
        );
        // restaraunt menus photos
        if (!isset($lang_id)) {
            $lang_id = 1;
        }
        $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantmenu::class);
        $menu_images = $repository->findBy(
                array(
            'restaurant_id' => $main_restaurant_id,
            'language_id' => $lang_id,
            'is_deleted' => 0
                ), array('sort_order' => 'ASC')
        );

        $menu_files = array();
        $menu_files_ar = array();
        if (!empty($menu_images)) {
            foreach ($menu_images as $menu_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $menu_imag = $repository->find($menu_image->getMedia_id());

                if (!empty($menu_imag)) {
                    $menu_files[] = array(
                        'restaurant_menu_id' => $menu_image->getRestaurant_menu_id(),
                        'main_media_id' => $menu_imag->getMedia_library_master_id(),
                        'media_name' => $menu_imag->getMedia_name(),
                        'language_id' => $menu_image->getLanguage_id(),
                        'main_gallery_id' => $menu_image->getRestaurant_menu_id(),
                        'media_location' => $menu_imag->getMedia_location()
                    );
                }
            }
        }
        $gallery_files = array();
        if (!empty($gallery_images)) {
            foreach ($gallery_images as $_gallery_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $gallery_img = $repository->find($_gallery_image->getImage_id());

                if (!empty($gallery_img)) {
                    $gallery_files[] = array(
                        'main_media_id' => $gallery_img->getMedia_library_master_id(),
                        'main_gallery_id' => $_gallery_image->getRestaurant_gallery_id(),
                        'media_name' => $gallery_img->getMedia_name(),
                        'media_location' => $gallery_img->getMedia_location()
                    );
                }
            }
        }
        $res_menu = $RestaurantAddress = '';
        if (!empty($RestaurantExist_data)) {
            $res_menu = $this->getImage($RestaurantExist_data[0]['restaurant_menu'], $this->container->getParameter('live_path'));
            $address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("owner_id" => $main_restaurant_id));
            if (!empty($address_info)) {
                $RestaurantAddress = array(
                    "address_name" => $address_info->getAddress_name(),
                    "lat" => $address_info->getLat(),
                    "lng" => $address_info->getLng()
                );
            }
        }
        $RestaurantExist = array('res_menu' => $res_menu, 'rest_data' => $RestaurantExist_data,
            'rest_cat' => $RestaurantExist_cat,
            'rest_address' => $RestaurantAddress,
            'rest_food' => $RestaurantExist_food_cat);

        return (array('lang_id' => $lang_id, 'main_restaurant_id' => $main_restaurant_id, 'media_library_master' => $menu_files, 'categories' => $category_list, 'food_types' => $food_types_details, 'rest_details' => $RestaurantExist, 'languages' => $language, 'right_codes' => $right_codes));
    }

    /**
     * @Route("/addoffer/{main_restaurant_id}",defaults={"main_restaurant_id"=0},name="add_offer")
     * @Template()
     */
    public function addofferAction($main_restaurant_id) {

        /* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SARO17", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
        /* end: check for access */

        $RestaurantExist = array();
        $RestaurantExist_cat = array();
        $RestaurantExist_food_cat = array();
        $food_types_details = array();
        //language list
        $em = $this->getDoctrine()->getManager();
        $language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
        if (empty($language)) {
            $language = array();
        }

        //category list
        $em = $this->getDoctrine()->getManager();
        $category_list1 = $em->getRepository(Categorymaster :: class)->findBy(array('is_deleted' => 0, 'language_id' => 1));

        if (empty($category_list1)) {
            $category_list = array();
        }
        if (!empty($category_list1)) {
            foreach ($category_list1 as $key => $val) {
                $categories1 = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted' => 0, 'language_id' => '2', 'main_category_id' => $val->getMain_category_id()));
                $ar = !empty($categories1) ? $categories1->getCategory_name() : '';
                $category_list[] = array('main_category_id' => $val->getMain_category_id(), 'category_name' => !empty($ar) ? $val->getCategory_name() . ' / ' . $ar : $val->getCategory_name());
            }
        }
        //food types and category
        $sql_food_type = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.language_id= '1' and food_type.is_deleted=0 and cat.is_deleted=0 group by ftr.main_food_type_id";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_food_type);
        $stmt->execute();
        $food_types_details1 = $stmt->fetchAll();

        if (!empty($food_types_details1)) {
            foreach ($food_types_details1 as $key => $val) {
                $sql_food_type1 = "select food_type.food_type_name,food_type.main_food_type_id,ftr.main_category_id,cat.category_name,ftr.food_type_category_relation_id from food_type_master food_type join food_type_category_relation ftr on food_type.main_food_type_id = ftr.main_food_type_id join category_master cat on cat.main_category_id = ftr.main_category_id where ftr.is_deleted=0 and food_type.language_id= '2' and food_type.is_deleted=0 and cat.is_deleted=0 and food_type.main_food_type_id='" . $val['main_food_type_id'] . "' group by ftr.main_food_type_id";

                $stmt1 = $con->prepare($sql_food_type1);
                $stmt1->execute();
                $food_types_details12 = $stmt1->fetchAll();
                $ar = !empty($food_types_details12) ? $food_types_details12[0]['food_type_name'] : '';
                $food_types_details[] = array('main_category_id' => $val['main_category_id'], 'main_food_type_id' => $val['main_food_type_id'], 'food_type_name' => !empty($ar) ? $val['food_type_name'] . ' / ' . $ar : $val['food_type_name']);
            }
        }

        $sql_rest_exist = "select rest.*,media.* from restaurant_master rest left join media_library_master media on rest.logo_id = media.media_library_master_id where rest.is_deleted=0 and rest.main_restaurant_id='" . $main_restaurant_id . "'";

        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_exist);
        $stmt->execute();
        $RestaurantExist_data = $stmt->fetchAll();
        if (empty($RestaurantExist_data)) {
            $RestaurantExist_data = array();
        } else {

            $rest_cat_sql = "select rest_cat_rel.main_category_id,rest_cat_rel.is_deleted as res_cat_delete from restaurant_category_relation rest_cat_rel where rest_cat_rel.is_deleted = 0 and rest_cat_rel.restaurant_id='" . $main_restaurant_id . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($rest_cat_sql);
            $stmt->execute();
            $RestaurantExist_cat = $stmt->fetchAll();

            $rest_food_cat_sql = "select * from restaurant_foodtype_relation rest_food_cat where rest_food_cat.is_deleted=0 and rest_food_cat.restaurant_id='" . $main_restaurant_id . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($rest_food_cat_sql);
            $stmt->execute();
            $RestaurantExist_food_cat = $stmt->fetchAll();
        }

        // restaraunt gallery photos
        $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantgallery::class);
        $gallery_images = $repository->findBy(
                array(
                    'restaurant_id' => $main_restaurant_id,
                    'is_deleted' => 0
                )
        );
        // restaraunt menus photos
        $repository = $this->getDoctrine()->getManager()->getRepository(Restaurantoffer::class);
        $menu_images = $repository->findBy(
                array(
                    'restaurant_id' => $main_restaurant_id,
                    'is_deleted' => 0
                )
        );
        $menu_files = array();
        if (!empty($menu_images)) {
            foreach ($menu_images as $menu_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $menu_imag = $repository->find($menu_image->getMedia_id());

                if (!empty($menu_imag)) {
                    $menu_files[] = array(
                        'main_media_id' => $menu_imag->getMedia_library_master_id(),
                        'media_name' => $menu_imag->getMedia_name(),
                        'main_gallery_id' => $menu_image->getRestaurant_offer_id(),
                        'media_location' => $menu_imag->getMedia_location()
                    );
                }
            }
        }
        $gallery_files = array();
        if (!empty($gallery_images)) {
            foreach ($gallery_images as $_gallery_image) {
                $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                $gallery_img = $repository->find($_gallery_image->getImage_id());

                if (!empty($gallery_img)) {
                    $gallery_files[] = array(
                        'main_media_id' => $gallery_img->getMedia_library_master_id(),
                        'main_gallery_id' => $_gallery_image->getRestaurant_gallery_id(),
                        'media_name' => $gallery_img->getMedia_name(),
                        'media_location' => $gallery_img->getMedia_location()
                    );
                }
            }
        }
        $res_menu = $RestaurantAddress = '';
        if (!empty($RestaurantExist_data)) {
            $res_menu = $this->getImage($RestaurantExist_data[0]['restaurant_menu'], $this->container->getParameter('live_path'));
            $address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findOneBy(array("owner_id" => $main_restaurant_id));
            if (!empty($address_info)) {
                $RestaurantAddress = array(
                    "address_name" => $address_info->getAddress_name(),
                    "lat" => $address_info->getLat(),
                    "lng" => $address_info->getLng()
                );
            }
        }
        $RestaurantExist = array('res_menu' => $res_menu, 'rest_data' => $RestaurantExist_data,
            'rest_cat' => $RestaurantExist_cat,
            'rest_address' => $RestaurantAddress,
            'rest_food' => $RestaurantExist_food_cat);

        return (array('media_library_master' => $menu_files, 'categories' => $category_list, 'food_types' => $food_types_details, 'rest_details' => $RestaurantExist, 'languages' => $language, 'right_codes' => $right_codes));
    }

    /**
     * @Route("/saveRestaurantCategory",name="save_restaurant_cat")
     * @Template()
     */
    public function saveRestaurantCategoryAction(Request $req) {

        if (!empty($req->request->get('food_type_cat'))) {
            $sql_update_rest_cat = "update restaurant_category_relation set is_deleted = 1 where restaurant_id='" . $req->request->get('main_rest_id') . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($sql_update_rest_cat);
            $stmt->execute();
            $sql_update_rest_food_cat = "update restaurant_foodtype_relation set is_deleted = 1 where restaurant_id='" . $req->request->get('main_rest_id') . "'";
            $em = $this->getDoctrine()->getManager();
            $con = $em->getConnection();
            $stmt = $con->prepare($sql_update_rest_food_cat);
            $stmt->execute();
            if (!empty($req->request->get('category'))) {
                foreach ($req->request->get('category') as $food_category) {

                    $em = $this->getDoctrine()->getManager();
                    $Restaurantcategoryrelation_new = new Restaurantcategoryrelation();
                    $Restaurantcategoryrelation_new->setRestaurant_id($req->request->get('main_rest_id'));
                    $Restaurantcategoryrelation_new->setMain_category_id($food_category);
                    $em->persist($Restaurantcategoryrelation_new);
                    $em->flush();
                }
            }
            foreach ($req->request->get('food_type_cat') as $food_cat_id) {
                $food_cat = explode(",", $food_cat_id);
                $food_type = $food_cat[0];
                $food_cat = $food_cat[1];
                if (!empty($req->request->get('category'))) {
                    if (in_array($food_cat, $req->request->get('category'))) {
                        $em = $this->getDoctrine()->getManager();
                        $Restaurantfoodtyperelation_new = new Restaurantfoodtyperelation();
                        $Restaurantfoodtyperelation_new->setRestaurant_id($req->request->get('main_rest_id'));
                        $Restaurantfoodtyperelation_new->setMain_foodtype_id($food_type);
                        $Restaurantfoodtyperelation_new->setMain_caetgory_id($food_cat);
                        $em->persist($Restaurantfoodtyperelation_new);
                        $em->flush();
                    }
                }
            }
            $this->get('session')->getFlashBag()->set('success_msg', 'Restaurant Category and food type saved successfully');
            return $this->redirectToRoute('add_restaurant', array('main_restaurant_id' => $req->request->get('main_rest_id')));
        } else {
            $this->redirectToRoute('admin_dashboard_index');
        }
    }

    /**
     * @Route("/saveRestaurant",name="save_restaurant")
     * @Template()
     */
    public function saveRestaurant(Request $req) {

		$instagram_link = $req->request->get('instagram_link');
		
		//	save logo_image
        $media = $_FILES['image']['name'];
        $tmp_path = $_FILES['image']['tmp_name'];
        $logo_id = 0;
        if (isset($media) && $media != '') {

            $upload_dir = $this->container->getParameter('upload_dir1') . '/Resource/Restoraunt/';
            $path = "/bundles/Resource/Restoraunt";
            $mediatype = $this->getDoctrine()
                    ->getManager()
                    ->getRepository(Mediatype::class)
                    ->findOneBy(array(
                'media_type_name' => 'Image',
                'is_deleted' => 0)
            );
            $allowedExts = explode(',', $mediatype->getMedia_type_allowed());
            $temp = explode('.', $_FILES['image']['name']);
            $extension = end($temp);
            if (in_array(strtolower($extension), $allowedExts)) {
                $media_id = $this->mediauploadAction($_FILES['image']['name'], $tmp_path, $path, $upload_dir, $mediatype->getMedia_type_id());
            }
            if (isset($media_id)) {
                $logo_id = $media_id;
            } else {
                $logo_id = 0;
                $this->get('session')->getFlashBag()->set('error_msg', 'Image not uploaded properly');
            }
        }
        $restaurant_menu = 0;
        if (isset($_FILES['restaurant_menu'])) {
            $media1 = $_FILES['restaurant_menu']['name'];
            $tmp_path1 = $_FILES['restaurant_menu']['tmp_name'];

            if (isset($media1) && $media1 != '') {

                $upload_dir = $this->container->getParameter('upload_dir1') . '/Resource/Restoraunt/';
                $path = "/bundles/Resource/Restoraunt";

                $temp = explode('.', $_FILES['restaurant_menu']['name']);
                $extension = end($temp);

                //if(in_array($extension, $allowedExts)){
                $restaurant_menu = $this->mediauploadAction($_FILES['restaurant_menu']['name'], $tmp_path1, $path, $upload_dir, 4);
            }
        }
        // save logo image complete
        // save address
        /* 		if($req->request->get('address') !== ''){
          $em = $this->getDoctrine()->getManager();
          $Addressmaster_data = $em->getRepository(Addressmaster :: class)->findOneBy(array('is_deleted'=>0,
          'address_name'=>$req->request->get('address')));
          if(!empty($Addressmaster_data)){
          $address_id = $Addressmaster_data->getAddress_master_id();
          }else{
          $em = $this->getDoctrine()->getManager();
          $Addressmaster_new = new Addressmaster();
          $Addressmaster_new->setAddress_name($req->request->get('address'));
          $Addressmaster_new->setAddress_type('other');
          $em->persist($Addressmaster_new);
          $em->flush();
          $address_id = $Addressmaster_new->getAddress_master_id();
          }
          }
          else{
          $this->get('session')->getFlashBag()->set('error_msg', 'Provide address Please');
          }
          var_dump($address_id);
          exit('done'); */
        // save adress complete
        if ($req->request->all()) {
            //	var_dump($_REQUEST);exit;
            if ($req->request->get('main_restaurant_id') === '') {
                $flag = 0;
            } else {
                $flag = 1;
            }

            $em = $this->getDoctrine()->getManager();
            $Restaurant = $em->getRepository(Restaurantmaster :: class)->findOneBy(array('is_deleted' => 0, 'main_restaurant_id' => $req->request->get('main_restaurant_id'), 'language_id' => $req->get('language_id')));
            
            // If the new restaurant request comes with the evaluation. If the admin move the evaluation to other restaurant, means the existing one. the client wants that the restaurant should get deleted in the admin. [it type new]
            /* if($req->request->get('type') == 'new'){
                if(!empty($Restaurant)){
                    $_name = $req->request->get('name');
                    if(strtolower(trim($_name)) == strtolower(trim($Restaurant->getRestaurant_name()))){
                        //remove new restaurant
                        $Restaurant->setIs_deleted(1);
                        $em->flush();
                        $this->get('session')->getFlashBag()->set('error_msg', 'New Restaurant Request removed');
                        return $this->redirect($this->generateUrl('admin_restaurant_newrestaurant'));
                    }
                }
            } */
			
			if($logo_id == 0){
				$getRestaurant = $em->getRepository(Restaurantmaster :: class)->findBy(array('is_deleted' => 0, 'main_restaurant_id' => $req->request->get('main_restaurant_id')));
				
				if(!empty($getRestaurant)){
					foreach($getRestaurant as $_res){
						if($_res->getLogo_id() != ''){
							$logo_id = $_res->getLogo_id();
						}
					}
				}
			}
			
            if (empty($Restaurant)) {
//				print_r($_REQUEST);exit;

                $checkRestaurantExists = $this->getDoctrine()->getRepository(Restaurantmaster :: class)->findOneBy(
                    array(
                        'is_deleted' => 0,
                        'main_restaurant_id' => $req->request->get('main_restaurant_id')
                    )
                );

                if ($req->request->get('status') == "open_soon") {
                    /* check for access */
                    $right_codes = $this->userrightsAction();
                    $rights_search = in_array("SAOSR19", $right_codes);
                    if ($rights_search != 1) {
                        $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                        return $this->redirect($this->generateUrl('admin_dashboard_index'));
                    }
                    /* end: check for access */
                } 
				else {
                    /* check for access */
                    $right_codes = $this->userrightsAction();
                    $rights_search = in_array("SAR7", $right_codes);
                    if ($rights_search != 1) {
                        $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                        return $this->redirect($this->generateUrl('admin_dashboard_index'));
                    }
                    /* end: check for access */
                }

                $Restaurant_data = new Restaurantmaster();
				
				$rest_name = $req->request->get('name');
				$index = strpos($rest_name, "'");
				if($index != ''){
					$rest_name = addslashes($rest_name);
				}
				
                $Restaurant_data->setRestaurant_name($rest_name);
                $Restaurant_data->setInstagram_link($instagram_link);
                $Restaurant_data->setRestaurant_menu($restaurant_menu);
                if (!empty($req->request->get('phone_number')) && $req->request->get('phone_number')) {
                    $Restaurant_data->setPhone_number($req->request->get('phone_number'));
                }else{
                    $Restaurant_data->setPhone_number(0);					
				}

                $Restaurant_data->setDescription($req->request->get('description') ? $req->request->get('description') : '');
				
				if(!empty($req->request->get('additional_info'))){
					$Restaurant_data->setAdditional_info($req->request->get('additional_info'));				
				}else{
					$Restaurant_data->setAdditional_info('');
				}

                if ($req->request->get('opening_date')) {
                    $Restaurant_data->setOpening_date(date('Y-m-d', strtotime($req->request->get('opening_date'))));
                }else{
					$Restaurant_data->setOpening_date(date('Y-m-d'));
				}
                $Restaurant_data->setLogo_id($logo_id);
                if ($req->request->get('timing')) {
                    $Restaurant_data->setTimings($req->request->get('timing'));
                }else{
                    $Restaurant_data->setTimings('');
				}
                $main_address_id = 0;
                $Restaurant_data->setAddress_id($main_address_id);
                $Restaurant_data->setStatus($req->request->get('status'));
                $Restaurant_data->setLanguage_id($req->request->get('language_id'));
                $Restaurant_data->setMain_restaurant_id(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($Restaurant_data);
                $em->flush();

                if ($flag === 0) {
                    $main_restaurant_id = $Restaurant_data->getRestaurant_master_id();
                    //$main_address_id = $Restaurant_data->getAddress_id();
                    $flag = 1;
                } else {
                    if ($req->request->get('main_restaurant_id') !== '') {
                        $main_restaurant_id = $req->request->get('main_restaurant_id');
                    }
                }

				if($restaurant_menu != 0 ){
					$Restaurantmenu = new Restaurantmenu();
					$Restaurantmenu->setRestaurant_id($main_restaurant_id);
					$Restaurantmenu->setMedia_id($restaurant_menu);
					$Restaurantmenu->setLanguage_id($req->request->get('language_id'));
					$Restaurantmenu->setIs_deleted(0);
					$em1 = $this->getDoctrine()->getManager();
					$em1->persist($Restaurantmenu);
					$em1->flush();
				}
                $em2 = $this->getDoctrine()->getManager();
				//------------
                $address_master_info = $em2->getRepository("AdminBundle:Addressmaster")->findBy(array("owner_id" => $main_restaurant_id, "language_id" => $req->request->get('language_id')));
				
				// update lat lng for all languages
				$em1 = $this->getDoctrine()->getManager();
				$update_latlng = $em1->getRepository("AdminBundle:Addressmaster")->findBy(array("owner_id" => $main_restaurant_id, "is_deleted" => 0));
				
				if(!empty($update_latlng)){
					foreach($update_latlng as $_latlng){
						if(!empty($req->request->get('lat'))){
							$_latlng->setLat($req->request->get('lat'));	
						}else{
							$_latlng->setLat(0);
						}
						if(!empty($req->request->get('lng'))){
							$_latlng->setLng($req->request->get('lng'));
						}else{
							$_latlng->setLng(0);
						}

						$em1->flush();
					}
				}
				
				
				if (!empty($address_master_info)) {
					$address_id = 0;
					foreach($address_master_info as $_address){
						
						if(!empty($req->request->get('address'))){
							$_address->setAddress_name($req->request->get('address'));							
						}else{
							$_address->setAddress_name('');
						}

						
						if(!empty($req->request->get('lat'))){
							$_address->setLat($req->request->get('lat'));	
						}else{
							$_address->setLat(0);
						}
						if(!empty($req->request->get('lng'))){
							$_address->setLng($req->request->get('lng'));
						}else{
							$_address->setLng(0);
						}
						
						$address_id = $_address->getAddress_master_id();
						$em2->flush();
					}
					
					if($Restaurant_data->getAddress_id() == 0 or $Restaurant_data->getAddress_id() == ''){
						$Restaurant_data->setAddress_id($address_id);
					}
					
                } 
				else {
					
                    //---------Save Address -----------------
                    $address_master = new Addressmaster();
					if(!empty($req->request->get('address'))){
						$address_master->setAddress_name($req->request->get('address'));						
					}else{
						$address_master->setAddress_name('');
					}

                    $address_master->setAddress_name2(NULL);
                    $address_master->setOwner_id($main_restaurant_id);
                    $address_master->setBase_address_type('main');
                    $address_master->setAddress_type('home');
                    $address_master->setContact_no(0);
                    $address_master->setCity_id(0);
                    $address_master->setArea_id(0);
                    $address_master->setStreet('');
                    $address_master->setFlate_house_number('');
                    $address_master->setSociety_building_name('');
                    $address_master->setLandmark('');
                    $address_master->setPincode(0);
                    $address_master->setLanguage_id($req->request->get('language_id'));
                    $address_master->setMain_address_id(0);
                    $address_master->setIs_defaulte_ship_address('true');
                    $address_master->setGmap_link('');

					if(!empty($req->request->get('lat'))){
						$address_master->setLat($req->request->get('lat'));	
					}else{
						$address_master->setLat(0);
					}
					
					if(!empty($req->request->get('lng'))){
						$address_master->setLng($req->request->get('lng'));
					}else{
						$address_master->setLng(0);
					}
					
                    $address_master->setDomain_id(1);
                    $em3 = $this->getDoctrine()->getManager();
                    $em3->persist($address_master);
                    $em3->flush();
                    $main_address_id = $address_master->getAddress_master_id();
                    $address_master->setMain_address_id($address_master->getAddress_master_id());
                    $em3->flush();
                    $Restaurant_data->setAddress_id($main_address_id);
                }

                $Restaurant_data->setMain_restaurant_id($main_restaurant_id);
                $em->flush();
                $main_rest_id_var = $Restaurant_data->getMain_restaurant_id();
                $this->get('session')->getFlashBag()->set('success_msg', 'Restaurant saved successfully');

				//return $this->redirectToRoute('restaurant_page');
				

                if(empty($checkRestaurantExists)){
                    #add default branch (Deleivery at time of rest add)	
                    $rest_name_branch = $rest_name."_Delivery";

                    $language = $this->getDoctrine()->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));
                    if(!empty($language)){
                        $mainBranchId = 0;
                        foreach($language as $_lang){

                            // check if branch exists - Make Branch single for all lang. - Feedback - 10-01-2019
                            $chkBranch = $this->getDoctrine()->getRepository("AdminBundle:Branchmaster")->findOneBy([
                                'main_restaurant_id' => $main_rest_id_var,
                                'language_id' => $_lang->getLanguage_master_id(),
                                'is_deleted' => 0
                            ]);

                            //$req->request->get('language_id')

                            if(empty($chkBranch)){
                                $new_branch_master = new Branchmaster();
                                $new_branch_master->setMain_restaurant_id($main_rest_id_var);
                                $new_branch_master->setBranch_name($rest_name_branch);
                                $new_branch_master->setBranch_address_id($main_address_id);
                                $new_branch_master->setMain_branch_flag(1);
                                $new_branch_master->setStatus($req->request->get('status'));
                                if(!empty($req->request->get('opening_date'))){
                                    $new_branch_master->setOpening_date(date('Y-m-d',strtotime($req->request->get('opening_date'))));							
                                }else{
                                    $new_branch_master->setOpening_date(date('Y-m-d'));	
                                }

                                if(!empty($req->request->get('timing'))){
                                    $new_branch_master->setTimings($req->request->get('timing'));							
                                }else{
                                    $new_branch_master->setTimings('');
                                }
                                if(!empty($req->request->get('additional_info'))){
                                $new_branch_master->setDescription($req->request->get('additional_info'));													
                                }else{
                                    $new_branch_master->setDescription('');
                                }

                                //$new_branch_master->setLanguage_id($req->request->get('language_id'));
                                $new_branch_master->setLanguage_id($_lang->getLanguage_master_id());
                                $new_branch_master->setMain_branch_master_id(0);
                                $new_branch_master->setIs_deleted(0);
                                $em->persist($new_branch_master);
                                $em->flush();
                                
                                if($mainBranchId == 0){
                                    $mainBranchId = $new_branch_master->getBranch_master_id();
                                }
                                
                                //$new_branch_master->setMain_branch_master_id($main_branch_id);
                                $new_branch_master->setMain_branch_master_id($mainBranchId);
                                $em->flush();
                            }
                        }
                    }

                    #add default branch (Deleivery at time of rest add) ends...	
                }

                return $this->redirectToRoute('add_restaurant', array('main_restaurant_id' => $main_rest_id_var));
            } 
            else {
				
				if ($req->request->get('status') == "open_soon") {
                    /* check for access */
                    $right_codes = $this->userrightsAction();
                    $rights_search = in_array("SAOSR19", $right_codes);
                    if ($rights_search != 1) {
                        $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                        return $this->redirect($this->generateUrl('admin_dashboard_index'));
                    }
                    /* end: check for access */
                } 
                else {
                    /* check for access */
                    $right_codes = $this->userrightsAction();
                    $rights_search = in_array("SAR7", $right_codes);
                    if ($rights_search != 1) {
                        $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                        return $this->redirect($this->generateUrl('admin_dashboard_index'));
                    }
                    /* end: check for access */
                }

				$rest_name = $req->request->get('name');
				$index = strpos($rest_name, "'");
				if($index != ''){
					$rest_name = addslashes($rest_name);
				}

                $Restaurant->setRestaurant_name($rest_name);
                $Restaurant->setInstagram_link($instagram_link);

                $Restaurant->setDescription($req->request->get('description') ? $req->request->get('description') : '');
                $Restaurant->setAdditional_info($req->request->get('additional_info'));

                // for both languages
                $Restaurant_exist = $em->getRepository(Restaurantmaster :: class)->findBy(array('is_deleted' => 0, 'main_restaurant_id' => $req->request->get('main_restaurant_id')));
                if (isset($restaurant_menu) && $restaurant_menu != '') {
                    $Restaurantmenu = new Restaurantmenu();
                    $Restaurantmenu->setRestaurant_id($req->request->get('main_restaurant_id'));
                    $Restaurantmenu->setMedia_id($restaurant_menu);
                    $Restaurantmenu->setIs_deleted(0);
                    $em->persist($Restaurantmenu);
                    $em->flush();
                }
                 // get Address Of Restaurant
				$entity = $this->getDoctrine()->getManager();
                $address_master_info = $entity->getRepository("AdminBundle:Addressmaster")->findBy(array("owner_id" => $req->request->get('main_restaurant_id'), "language_id" => $req->request->get('language_id'), "is_deleted" => 0));
				
				// update lat lng for all languages
				$em1 = $this->getDoctrine()->getManager();
				$update_latlng = $em1->getRepository("AdminBundle:Addressmaster")->findBy(array("owner_id" => $req->request->get('main_restaurant_id'), "is_deleted" => 0));
				
				if(!empty($update_latlng)){
					foreach($update_latlng as $_latlng){
						$_latlng->setLat($req->request->get('lat'));
						$_latlng->setLng($req->request->get('lng'));
						$em1->flush();
					}
				}
				
				if (!empty($address_master_info)) {
					$address_id = 0;
					foreach($address_master_info as $_address){
						$_address->setAddress_name($req->request->get('address'));
						$_address->setLat($req->request->get('lat'));
						$_address->setLng($req->request->get('lng'));
						$address_id = $_address->getAddress_master_id();
						$entity->flush();
					}
					
					if($Restaurant->getAddress_id() == 0 or $Restaurant->getAddress_id() == ''){
						$Restaurant->setAddress_id($address_id);
					}
                } 
                else {
                        //---------Save Address -----------------
                        $address_master = new Addressmaster();
                        $address_master->setAddress_name($req->request->get('address'));
                        $address_master->setAddress_name2(NULL);
                        $address_master->setOwner_id($req->request->get('main_restaurant_id'));
                        $address_master->setBase_address_type('main');
                        $address_master->setAddress_type('home');
                        $address_master->setContact_no(0);
                        $address_master->setCity_id(0);
                        $address_master->setArea_id(0);
                        $address_master->setStreet('');
                        $address_master->setFlate_house_number('');
                        $address_master->setSociety_building_name('');
                        $address_master->setLandmark('');
                        $address_master->setPincode(0);
                        $address_master->setLanguage_id($req->request->get('language_id'));
                        $address_master->setMain_address_id(0);
                        $address_master->setIs_defaulte_ship_address('true');
                        $address_master->setGmap_link('');
                        $address_master->setLat($req->request->get('lat'));
                        $address_master->setLng($req->request->get('lng'));
                        $address_master->setDomain_id(1);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($address_master);
                        $em->flush();
                        $main_address_id = $address_master->getAddress_master_id();
                        $address_master->setMain_address_id($address_master->getAddress_master_id());
                        $em->flush();
                        $Restaurant->setAddress_id($main_address_id);
                }
				
                foreach ($Restaurant_exist as $rest) {

                    if (isset($restaurant_menu) && $restaurant_menu != '') {
                        $rest->setRestaurant_menu($restaurant_menu);
                    }
                    if (isset($media) && $media != '') {
                        $rest->setLogo_id($logo_id);
                    }
                  
                    $rest->setOpening_date(date('Y-m-d', strtotime($req->request->get('opening_date'))));
                    $rest->setTimings($req->request->get('timing'));
                    $rest->setPhone_number($req->request->get('phone_number'));

                    $rest->setStatus($req->request->get('status'));
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                }
                //for both language completed..
				
#update status of main branch as well		
				$getAllBranch = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Branchmaster")->findBy(array('main_restaurant_id'=>$req->request->get('main_restaurant_id')));
				$em = $this->getDoctrine()->getManager();
				 
				if($getAllBranch){
					foreach($getAllBranch as $branch_only){
						$branch_only->setStatus($req->request->get('status'));
						$em->flush();
					}
				}
#update status of main branch as well ends..				
				
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $type = $_REQUEST['type'];
                $this->get('session')->getFlashBag()->set('success_msg', 'Restaurant updated successfully');
                if ($type == 'old') {
                    //return $this->redirectToRoute('restaurant_page');
					$referer = $req->headers->get('referer');
                    return $this->redirect($referer);
                } else {
//                    return $this->redirect($this->generateUrl('admin_restaurant_newrestaurant'));
					//return $this->redirectToRoute('restaurant_page');
                    $referer = $req->headers->get('referer');
                    return $this->redirect($referer);
                }
            }
        } else {
            return $this->redirectToRoute('admin_dashboard_index');
        }
    }

    /**
     * @Route("/savemenu",name="save_menu")
     */
    public function savemenuAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $media_library_master = new Medialibrarymaster();
        $media_library_master->setMedia_type_id($request->get('media_type_id'));
        $media_library_master->setMedia_title($request->get('img_name'));
        $media_library_master->setMedia_location("/bundles/design/uploads/Restoraunt-gallery");
        $media_library_master->setMedia_name($request->get('img_name'));
        $media_library_master->setCreated_on(date("Y-m-d H:i:s"));
        $media_library_master->setIs_Deleted(0);
        $em->persist($media_library_master);
        $em->flush();
        if ($request->get('type') == 1) {

            /* check for access */
            $right_codes = $this->userrightsAction();
            $rights_search = in_array("SARM15", $right_codes);
            if ($rights_search != 1) {
                $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                return new Response(json_encode(array('success' => '0', 'name' => 'failed')));
            }
            /* end: check for access */


            $Restaurantoffer = new Restaurantoffer();
            $Restaurantoffer->setRestaurant_id($request->request->get('rest_id'));
            $Restaurantoffer->setMedia_id($media_library_master->getMedia_library_master_id());
            $Restaurantoffer->setIs_deleted(0);
            $em->persist($Restaurantoffer);
            $em->flush();

            $gallery_master = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Restaurantoffer')
                    ->findOneBy(array('restaurant_id' => $request->request->get('rest_id'), 'media_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));
            $gallery_master_id = $gallery_master->getRestaurant_offer_id();
        } else {

            /* check for access */
            $right_codes = $this->userrightsAction();
            $rights_search = in_array("SARM15", $right_codes);
            if ($rights_search != 1) {
                $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                return new Response(json_encode(array('success' => '0', 'name' => 'failed')));
            }
            /* end: check for access */

            $Restaurantmenu = new Restaurantmenu();
            $Restaurantmenu->setRestaurant_id($request->request->get('rest_id'));
            $Restaurantmenu->setMedia_id($media_library_master->getMedia_library_master_id());

            $language_id = 1;
            if (array_key_exists('language_id', $request->request->all())) {
                $language_id = $request->get('language_id');
            }

            $Restaurantmenu->setLanguage_id($language_id);
            $Restaurantmenu->setIs_deleted(0);
            $em->persist($Restaurantmenu);
            $em->flush();
            $gallery_master = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Restaurantmenu')
                    ->findOneBy(array('restaurant_id' => $request->request->get('rest_id'), 'media_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));
            $gallery_master_id = $gallery_master->getRestaurant_menu_id();
        }
        $media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));


        if (!empty($media_file)) {
            $content = array(
                "name" => $media_file->getMedia_name(),
                "path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                "thumbnail_path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                "media_library_master_id" => $media_library_master->getMedia_library_master_id(),
                "gallery_master_id" => $gallery_master_id
            );
        }
        return new Response(json_encode($content));
    }

    /**
     * @Route("/saverestaurangallery",name="save_rest_gallery")
     */
    public function saverestaurangalleryAction(Request $request) {
        if ($request->request->all()) {

            $em = $this->getDoctrine()->getManager();

            $media_library_master = new Medialibrarymaster();
            $media_library_master->setMedia_type_id($request->get('media_type_id'));
            $media_library_master->setMedia_title($request->get('img_name'));
            $media_library_master->setMedia_location("/bundles/design/uploads/bulkupload");
            $media_library_master->setMedia_name($request->get('img_name'));
            $media_library_master->setCreated_on(date("Y-m-d H:i:s"));
            $media_library_master->setIs_Deleted(0);
            $em->persist($media_library_master);
            $em->flush();

            $rest_gallery = new Restaurantgallery();
            $rest_gallery->setRestaurant_id($request->get('rest_id'));
            $rest_gallery->setImage_id($media_library_master->getMedia_library_master_id());
            $rest_gallery->setIs_deleted(0);
            $em->persist($rest_gallery);
            $em->flush();

            $media_file = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Medialibrarymaster')
                    ->findOneBy(array('media_library_master_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));

            if (!empty($media_file)) {
                $content = array(
                    "name" => $media_file->getMedia_name(),
                    "path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "thumbnail_path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "media_library_master_id" => $media_library_master->getMedia_library_master_id()
                );
                return new Response(json_encode($content));
            }
        }
        return new Response(false);
    }

    /**
     * @Route("/saverbranchgallery",name="save_branch_gallery")
     */
    public function savebranchgalleryAction(Request $request) {
        if ($request->request->all()) {

            $em = $this->getDoctrine()->getManager();

            $media_library_master = new Medialibrarymaster();
            $media_library_master->setMedia_type_id($request->get('media_type_id'));
            $media_library_master->setMedia_title($request->get('img_name'));
            $media_library_master->setMedia_location("/bundles/design/uploads/bulkupload");
            $media_library_master->setMedia_name($request->get('img_name'));
            $media_library_master->setCreated_on(date("Y-m-d H:i:s"));
            $media_library_master->setIs_Deleted(0);
            $em->persist($media_library_master);
            $em->flush();

            $rest_gallery = new Branchgallery();
            $rest_gallery->setBranch_id($request->get('rest_id'));
            $rest_gallery->setImage_id($media_library_master->getMedia_library_master_id());
            $rest_gallery->setIs_deleted(0);
            $em->persist($rest_gallery);
            $em->flush();

            $media_file = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Medialibrarymaster')
                    ->findOneBy(array('media_library_master_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));

            if (!empty($media_file)) {
                $content = array(
                    "name" => $media_file->getMedia_name(),
                    "gallery_master_id" => $rest_gallery->getBranch_gallery_id(),
                    "path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "thumbnail_path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "media_library_master_id" => $media_library_master->getMedia_library_master_id()
                );
                return new Response(json_encode($content));
            }
        }
        return new Response(false);
    }

    /**
     * @Route("/removemenu",name="remove_menu_img")
     */
    public function removemenuAction(Request $request) {

        /* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SDRM16", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return new Response("false");
        }
        /* end: check for access */

        $media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'), 'is_deleted' => 0));


        $gallery_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Restaurantmenu')
                ->findOneBy(array('restaurant_menu_id' => $request->get('main_gallery_id'), 'is_deleted' => 0));


        if (!empty($media_file) && !empty($gallery_file)) {
            $media_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $gallery_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $delete_img = $this->container->getParameter('root_dir') . '/' . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();

            //unlink($delete_img);

            return new Response("true");
        } else {
            return new Response("false");
        }
    }

    /**
     * @Route("/removeoffer",name="remove_offer_img")
     */
    public function removeofferAction(Request $request) {

        /* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SDRO18", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return new Response("false");
        }
        /* end: check for access */

        $media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'), 'is_deleted' => 0));


        $gallery_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Restaurantoffer')
                ->findOneBy(array('restaurant_offer_id' => $request->get('main_gallery_id'), 'is_deleted' => 0));


        if (!empty($media_file) && !empty($gallery_file)) {
            $media_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $gallery_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $delete_img = $this->container->getParameter('root_dir') . '/' . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();

            //unlink($delete_img);

            return new Response("true");
        } else {
            return new Response("false");
        }
    }

    /**
     * @Route("/removerestaurangallery",name="remove_gallery_img")
     */
    public function removerestaurangalleryAction(Request $request) {
        $media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'), 'is_deleted' => 0));

        $gallery_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Restaurantgallery')
                ->findOneBy(array('restaurant_gallery_id' => $request->get('main_gallery_id'), 'is_deleted' => 0));

        if (!empty($media_file) && !empty($gallery_file)) {
            $media_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $gallery_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $delete_img = $this->container->getParameter('root_dir') . '/' . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();

            //unlink($delete_img);

            return new Response("true");
        } else {
            return new Response("false");
        }
    }

    /**
     * @Route("/removebranchgallery",name="remove_gallery_img_branch")
     */
    public function removebranchgalleryAction(Request $request) {
        $media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'), 'is_deleted' => 0));

        $gallery_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Branchgallery')
                ->findOneBy(array('branch_gallery_id' => $request->get('main_gallery_id'), 'is_deleted' => 0));

        if (!empty($media_file) && !empty($gallery_file)) {
            $media_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $gallery_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $delete_img = $this->container->getParameter('root_dir') . '/' . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();

            //unlink($delete_img);

            return new Response("true");
        } else {
            return new Response("false");
        }
    }    

    //ajax change status from toggle button
    /**
     * @Route("/changeStatusRest",name="change_status_rest")
     * @Template()
     */
    public function changeStatusAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $rest_details = $em->getRepository(Restaurantmaster :: class)->findBy(array('is_deleted' => 0, 'main_restaurant_id' => $req->request->get('main_restaurant_id')));
        if (empty($rest_details)) {
            $rest_details = array();
        }

        foreach ($rest_details as $rest) {
            if ($rest->getStatus() === 'active') {
                $rest->setStatus('inactive');
            } else {
                $rest->setStatus('active');
            }
            $em->flush();
        }
        exit('done');
    }

    //ajax change status from toggle button
    /**
     * @Route("/changeStatusResteval",name="change_status_evalt")
     * @Template()
     */
    public function changeStatusRestevalAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $rest_details = $em->getRepository(Evaluationfeedback :: class)->findBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $req->request->get('evaluation_feedback_id')));
        if (empty($rest_details)) {
            $rest_details = array();
        }

        foreach ($rest_details as $rest) {

            $rest->setStatus($req->request->get('status'));

            $em->flush();
        }
        exit('done');
    }

    //ajax change status from toggle button
    /**
     * @Route("/changeStatusRestevalreason",name="change_status_evaltreason")
     * @Template()
     */
    public function changeStatusRestevalreasonAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $rest_details = $em->getRepository(Evaluationfeedback :: class)->findBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $req->request->get('evaluation_feedback_id')));
        if (empty($rest_details)) {
            $rest_details = array();
        }

        foreach ($rest_details as $rest) {
            $rest->setStatus($req->request->get('status'));
            $rest->setRejected_reason($req->request->get('reason'));

            $em->flush();
        }
        exit('done');
    }

    /**
     * @Route("/deleteRest/{main_restaurant_id}/{type}",defaults={"main_restaurant_id"=0,"type":""},name="delete_restaurant")
     * @Template()
     */
    public function deleteRestAction($main_restaurant_id, $type) {

        $em = $this->getDoctrine()->getManager();
        $rest_details = $em->getRepository(Restaurantmaster :: class)->findBy(array('is_deleted' => 0, 'main_restaurant_id' => $main_restaurant_id));
        $right_codes = $this->userrightsAction();
        foreach ($rest_details as $rest) {

            if ($rest->getStatus() == "open_soon") {
                /* check for access */
                $rights_search = in_array("SDOSR21", $right_codes);
                if ($rights_search != 1) {
                    $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                    return $this->redirect($this->generateUrl('admin_dashboard_index'));
                }
                /* end: check for access */
            } else {
                /* check for access */
                $rights_search = in_array("SDR8", $right_codes);
                if ($rights_search != 1) {
                    $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
                    return $this->redirect($this->generateUrl('admin_dashboard_index'));
                }
                /* end: check for access */
            }

            //$rest->setIs_deleted(1);
			$em->remove($rest);
            $em->flush();
			
			// remove restaurant category relation
			$restaurant_category_relation = $em->getRepository('AdminBundle:Restaurantcategoryrelation')->findBy(
				array(
					'restaurant_id' => $main_restaurant_id
				)
			);
			
			if(!empty($restaurant_category_relation)){
				foreach($restaurant_category_relation as $_resCatRel){
					$em->remove($_resCatRel);
					$em->flush();
				}
			}
			
			// remove restaurant_foodtype_relation
			$restaurant_foodtype_relation = $em->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy(
				array(
					'restaurant_id' => $main_restaurant_id
				)
			);
			
			if(!empty($restaurant_foodtype_relation)){
				foreach($restaurant_foodtype_relation as $_resFoodCatRel){
					$em->remove($_resFoodCatRel);
					$em->flush();
				}
			}
			
			// remove evaluation_feedback
			$evaluation_feedback = $em->getRepository('AdminBundle:Evaluationfeedback')->findBy(
				array(
					'restaurant_id' => $main_restaurant_id
				)
			);
			
			if(!empty($evaluation_feedback)){
				foreach($evaluation_feedback as $_feedback){
					$em->remove($_feedback);
					$em->flush();
				}
			}
        }

        $this->get('session')->getFlashBag()->set('success_msg', 'Restaurant deleted successfully');
        if ($type == 'old') {
            return $this->redirectToRoute('restaurant_page');
        } else {
            return $this->redirect($this->generateUrl('admin_restaurant_newrestaurant'));
        }
    }
	
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');
            $res = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);
            }
            return new Response(base64_encode($res));
        }
        return new Response("");
    }

    /**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyDecryptionAction($string) {
        if ($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false) {
            $key = $this->container->getParameter('key');

            $res = '';
            $string = base64_decode($string);
            for ($i = 0; $i < strlen($string); $i++) {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);
            }
            return new Response($res);
        }
        return new Response("");
    }

    /**
     * @Route("/viewevaluation/{main_restaurant_id}/{main_branch_id}",defaults={"main_restaurant_id"=0,"main_branch_id"=0},name="viewevaluation_restaurant")
     * @Template()
     */
    public function viewevaluationAction($main_restaurant_id,$main_branch_id, Request $request) {
        $right_codes = $this->userrightsAction();
        $sql_rest_data = "select * from restaurant_master where is_deleted=0 and language_id=1 and (status='active' or status='open_soon' or status = 'pending')";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();

#branch related changes	

	$branch_where = '';
	
	if($main_branch_id != 0){
		$branch_where = " AND evaluation_feedback.main_branch_id = '$main_branch_id' ";
	}
	
#branch related changes done		
		
        $query = "SELECT evaluation_feedback.*,branch_master.branch_name,restaurant_master.restaurant_name,category_master.category_name,food_type_master.food_type_name,user_master.user_firstname,user_master.user_lastname FROM evaluation_feedback LEFT JOIN restaurant_master ON evaluation_feedback.restaurant_id=restaurant_master.main_restaurant_id
		LEFT JOIN branch_master ON evaluation_feedback.main_branch_id = branch_master.main_branch_master_id 
		LEFT JOIN user_master ON evaluation_feedback.user_id=user_master.user_master_id LEFT JOIN food_type_master ON food_type_master.main_food_type_id=evaluation_feedback.food_type_id LEFT JOIN category_master ON category_master.main_category_id=evaluation_feedback.category_id WHERE evaluation_feedback.is_deleted=0 AND evaluation_feedback.restaurant_id='$main_restaurant_id' $branch_where group by evaluation_feedback_id";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($query);
        $stmt->execute();
        $evaluation = $stmt->fetchAll();
        $result = array();

//		print($query);exit;
//		echo "<pre>";print_r($evaluation);exit;	
		
        if (!empty($evaluation)) {
            foreach ($evaluation as $key => $value) {
                $query_eval_gallery = "SELECT media.* from evaluation_feedback_gallery eval_feed LEFT JOIN media_library_master media
									ON eval_feed.media_id = media.media_library_master_id where evaluation_feedback_id='" . $value['evaluation_feedback_id'] . "' and eval_feed.is_deleted=0";
                $con = $em->getConnection();
                $stmt_1 = $con->prepare($query_eval_gallery);
                $stmt_1->execute();
                $evaluation_gallery = $stmt_1->fetchAll();

                $result[] = array(
                    'user_id' => $value['user_firstname'] . ' ' . $value['user_lastname'],
                    'category_name' => $value['category_name'],
                    'food_type_name' => $value['food_type_name'],
                    'restaurant_id' => $value['restaurant_name'],
                    'branch_name' => $value['branch_name'],
                    'restaurant_id1' => $value['restaurant_id'],
                    'comments' => $this->changeToEmoji($value['comments']),
                    'status' => $value['status'],
                    'is_featured' => $value['is_featured'],
                    'evaluation_feedback_id' => $value['evaluation_feedback_id'],
                    'service_level' => $value['service_level'],
                    'dining_level' => $value['dining_level'],
                    'atmosphere_level' => $value['atmosphere_level'],
                    'price_level' => $value['price_level'],
                    'clean_level' => $value['clean_level'],
                    'delivery_speed_level' => $value['delivery_speed_level'],
                    'packaging_level' => $value['packaging_level'],
                    'eval_type' => $value['eval_type'],
                    'invoice_image_id' => $value['invoice_image_id'],
                    'evaluation_gallery' => $evaluation_gallery
                );
            }
        }

//		echo"<pre>";print_r($Restaurantmaster);exit;
        $referer = $request->headers->get('referer');
        return array('rest_final_data' => $result, 'restaurantmaster' => $Restaurantmaster, 'referer' => $referer, 'right_codes' => $right_codes);
    }

    public function changeToEmoji($src){
		$replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $src);

		$result = mb_convert_encoding($replaced, "UTF-16", "HTML-ENTITIES");
		
		$result = mb_convert_encoding($result, 'utf-8', 'utf-16');

		return urldecode($src);
    }
    
    /**
     * @Route("/changeevalRest",name="change_rest_evalt")
     * @Template()
     */
    public function changeevalRestAction(Request $req) {

        $em = $this->getDoctrine()->getManager();
        $rest_details = $em->getRepository(Evaluationfeedback :: class)->findBy(array('is_deleted' => 0, 'evaluation_feedback_id' => $req->request->get('evaluation_feedback_id')));
        if (empty($rest_details)) {
            $rest_details = array();
        } else {
			// remove new restaurant
			$_restaurant = $em->getRepository('AdminBundle:Restaurantmaster')->findBy([
				'main_restaurant_id' => $rest_details[0]->getRestaurant_id(),
				'status' => 'pending'
			]);
			
			if(!empty($_restaurant)){
				foreach($_restaurant as $_res){
					$_res->setIs_deleted(1);
					$em->flush();
				}
			}
		}

        foreach ($rest_details as $rest) {

            $rest->setRestaurant_id($req->request->get('res_id'));

            $em->flush();
        }
        exit('done');
    }

    /**
     * @Route("/restaurant/setmenusortorder")
     */
    public function setmenusortorderAction() {
        $em = $this->getDoctrine()->getManager();
        $count = 0;
        if ($_POST['arr']) {
            $array = $_POST['arr'];
            $array1 = explode(",", $array);

            foreach ($array1 as $value) {
                $rest_menu = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Restaurantmenu')->findOneBy(array("is_deleted" => 0, "restaurant_menu_id" => $value));

                $rest_menu->setSort_order($count);
                $em->persist($rest_menu);
                $em->flush();
                $count = $count + 1;
            }
            return new JsonResponse("ok");
        }
    }
	
    /**
     * @Route("/restaurant/addBranch/{rest_id}/{main_branch_id}/{rest_type}",defaults={"main_branch_id"="0","rest_type":"","rest_id"="0"})
	 * @Template()
     */
    public function addbranchAction(Request $req, $main_branch_id,$rest_id,$rest_type) {
        
		/* check for access */
		$right_codes = $this->userrightsAction();
		
		if($main_branch_id != 0 && $main_branch_id != '0'){
			// edit privileges
			$rights_search = in_array("SEBRN43", $right_codes);
			if ($rights_search != 1) {
				$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
				return $this->redirect($this->generateUrl('admin_dashboard_index'));
			}
			/* end: check for access */
		} else {
			// add privileges
			$rights_search = in_array("SABRN42", $right_codes);
			if ($rights_search != 1) {
				$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
				return $this->redirect($this->generateUrl('admin_dashboard_index'));
			}
			/* end: check for access */
		}
		
        $tab = !empty($req->query->get('tab')) ? $req->query->get('tab') : '' ; 
		$em = $this->getDoctrine()->getManager();
		$languages = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted' => 0));		
        $gallery_files = array();
        
        $sql_brach_exist = "SELECT branch.*,address_master.address_name, address_master.owner_id ,address_master.lat ,address_master.lng FROM branch_master branch LEFT JOIN address_master ON branch.branch_address_id = address_master.address_master_id WHERE branch.is_deleted = 0 AND branch.main_branch_master_id='" . $main_branch_id . "'";
		
		
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_brach_exist);
        $stmt->execute();
        $branch_data = $stmt->fetchAll();

        if (!empty($branch_data)) {
			foreach($branch_data as $key=>$value){
				
				$address_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Addressmaster')->findBy(
					array(
						"main_address_id" => $value['branch_address_id'],
						'language_id' => $value['language_id']
					)
				);
				
				if(!empty($address_info)){
					foreach($address_info as $_addr){
						$branch_data[$key]['address_name'] = $_addr->getAddress_name();
					}
				}
				
				$branch_data[$key]['branch_name'] = stripslashes($value['branch_name']);		
            }
                    // restaraunt gallery photos
            $repository = $this->getDoctrine()->getManager()->getRepository(Branchgallery::class);
            $gallery_images = $repository->findBy(
                    array(
                        'branch_id' => $main_branch_id,
                        'is_deleted' => 0
                    )
            );

            
            if (!empty($gallery_images)) {
                foreach ($gallery_images as $_gallery_image) {
                    $repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
                    $gallery_img = $repository->find($_gallery_image->getImage_id());
    
                    if (!empty($gallery_img)) {
                        $gallery_files[] = array(
                            'main_media_id' => $gallery_img->getMedia_library_master_id(),
                            'main_gallery_id' => $_gallery_image->getBranch_gallery_id(),
                            'media_name' => $gallery_img->getMedia_name(),
                            'media_location' => $gallery_img->getMedia_location()
                        );
                    }
                }
            }

        }
        $referer = $req->headers->get('referer');
        
        /* echo '<pre>';
        print_r($branch_data);
        exit; */

		return array('tab'=>$tab,'branch_data'=>$branch_data,'languages'=>$languages,'rest_type'=>$rest_type,'rest_id'=>$rest_id,'media_library_master'=>$gallery_files);
	}
	
	/**
     * @Route("/restaurant/changeMainBranch")
     */
    public function changemainbranchAction(Request $req) {
		$main_rest_id = $req->request->get('main_rest_id');
		$main_branch_id = $req->request->get('main_branch_id');
		$status = $req->request->get('status');
		
		$em = $this->getDoctrine()->getManager();
		
		$all_branch = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Branchmaster")->findBy(array('is_deleted'=>0,'main_restaurant_id'=>$main_rest_id));
		
		if($all_branch){
			foreach($all_branch as $branch_only){
				if($branch_only->getMain_branch_master_id() == $main_branch_id){
					if($status == 'true'){
						$branch_only->setMain_branch_flag(1);						
					}else{
						$branch_only->setMain_branch_flag(0);							
					}

				}else{
					$branch_only->setMain_branch_flag(0);					
				}
				$em->flush();
			}
		}
		
		return new Response('done');
	}

	/**
     * @Route("/restaurant/deleteBranch")
     */
    public function deletebranchAction(Request $req) {
		
		/* check for access */
        $right_codes = $this->userrightsAction();
        $rights_search = in_array("SDBRN44", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
        /* end: check for access */
		
		$branch_master_id = $req->request->get('main_branch_id');
		$em = $this->getDoctrine()->getManager();
		 $branch_master = $em->getRepository('AdminBundle:Branchmaster')->findBy(array("is_deleted" => 0, "main_branch_master_id" => $branch_master_id));
		 
		 if($branch_master){
			foreach($branch_master as $branch_){
				$branch_->setIs_deleted(1);
				$em->flush();				
			} 
		 }
		 return new Response('done');
	}	
	
    /**
     * @Route("/restaurant/saveBranches")
     */
    public function savebranchAction(Request $req) {
		
//		echo"<pre>";print_r($_REQUEST);exit;
		$em = $this->getDoctrine()->getManager();
		
		$main_branch_id = $req->request->get('main_branch_id');
		$branch_name = $req->request->get('branch_name');
		$mobile_no = !empty($req->request->get('mobile_no')) ? $req->request->get('mobile_no') : 0 ;
		$branch_status = $req->request->get('status');
		$main_branch = $req->request->get('main_branch');
		$opening_date = $req->request->get('opening_date');
		$timing = $req->request->get('timing');
		$address_branch = $req->request->get('address_branch');
		$lat = $req->request->get('lat');
		$lng = $req->request->get('lng');
		$additional_info = $req->request->get('additional_info');
		$language_id_branch = $req->request->get('language_id_branch');
		$main_restaurant_id = $req->request->get('main_restaurant_id');
		
			 
			 $branch_master = $em->getRepository('AdminBundle:Branchmaster')->findOneBy(array("is_deleted" => 0, "main_branch_master_id" => $main_branch_id,'language_id'=>$language_id_branch));
			 
			 if($branch_master){
				$branch_master->setBranch_name($branch_name);
				if(isset($main_branch)){
					$branch_master->setMain_branch_flag($main_branch);
					if($main_branch == 1){
						#set all to 0 and make this one main branch
										
						$all_branch = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Branchmaster")->findBy(array('is_deleted'=>0,'main_restaurant_id'=>$main_restaurant_id));
						
						if($all_branch){
							foreach($all_branch as $branch_only){
								if($branch_only->getMain_branch_master_id() == $main_branch_id){
									$branch_only->setMain_branch_flag(1);
								}else{
									$branch_only->setMain_branch_flag(0);
								}
								$em->flush();
							}
						}						
						
					}
				}else{
					$branch_master->setMain_branch_flag(0);					
				}

				$branch_master->setStatus($branch_status);
				$branch_master->setOpening_date(date('Y-m-d',strtotime($opening_date)));
				$branch_master->setTimings($timing);
				$branch_master->setDescription($additional_info);
				$em->flush();
				
				#updated address_master
				$address_id = $branch_master->getBranch_address_id();
				
				$address_master_exist = $em->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted" => 0, "address_master_id" => $address_id,'language_id'=>$language_id_branch));
				
				if($address_master_exist){
					$address_master_exist->setAddress_name($address_branch);
					$address_master_exist->setLat($lat);
                    $address_master_exist->setLng($lng);
					$em->flush();
				}else{
						$address_master = new Addressmaster();
                        $address_master->setAddress_name($address_branch);
                        $address_master->setAddress_name2(NULL);
                        $address_master->setOwner_id(0);
                        $address_master->setBase_address_type('main');
                        $address_master->setAddress_type('home');
                        $address_master->setContact_no(0);
                        $address_master->setCity_id(0);
                        $address_master->setArea_id(0);
                        $address_master->setStreet('');
                        $address_master->setFlate_house_number('');
                        $address_master->setSociety_building_name('');
                        $address_master->setLandmark('');
                        $address_master->setPincode(0);
                        $address_master->setLanguage_id($language_id_branch);
                        $address_master->setMain_address_id(0);
                        $address_master->setIs_defaulte_ship_address('true');
                        $address_master->setGmap_link('');
                        $address_master->setLat($lat);
                        $address_master->setLng($lng);
                        $address_master->setDomain_id(1);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($address_master);
                        $em->flush();
                       
                        $address_master->setMain_address_id($address_master->getAddress_master_id());
						$main_address_id = $address_master->getAddress_master_id();
                        $em->flush();
						
						$branch_master->setBranch_address_id($main_address_id);
						$em->flush();
						
				
				}
				
				#common_changes
						$branch_master_all = $em->getRepository('AdminBundle:Branchmaster')->findBy(array("is_deleted" => 0, "main_branch_master_id" => $main_branch_id));
								
						if($branch_master_all){
							foreach($branch_master_all as $lang_branch){
								$lang_branch->setMain_branch_flag($main_branch);
								$lang_branch->setStatus($branch_status);
								$lang_branch->setMobile_no($mobile_no);
								$lang_branch->setOpening_date(date('Y-m-d',strtotime($opening_date)));
								$lang_branch->setTimings($timing);								
								$em->flush();
								
								#common lat lng changes
								$address_master_all = $em->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted" => 0, "address_master_id" => $lang_branch->getBranch_address_id()));
								if($address_master_all){
								    $address_master_all->setLat($lat);
									$address_master_all->setLng($lng);	
									$em->flush();
								}	
								
							}
						}
						
						$this->get('session')->getFlashBag()->set('success_msg',"Branch Updated successfully");					
				
				
			 }else{
				#make entry for both languages
						
						$new_branch_master = new Branchmaster();
						$new_branch_master->setMain_restaurant_id($main_restaurant_id);
						$new_branch_master->setBranch_name($branch_name);
						$new_branch_master->setMobile_no($mobile_no);
						$new_branch_master->setBranch_address_id(0);
						if(isset($main_branch)){
							$new_branch_master->setMain_branch_flag($main_branch);
						}else{
							$new_branch_master->setMain_branch_flag(0);					
						}
						$new_branch_master->setStatus($branch_status);
						$new_branch_master->setOpening_date(date('Y-m-d',strtotime($opening_date)));
						$new_branch_master->setTimings($timing);
						$new_branch_master->setDescription($additional_info);						
						$new_branch_master->setLanguage_id($language_id_branch);
						$new_branch_master->setMain_branch_master_id($main_branch_id);
						$new_branch_master->setIs_deleted(0);
						$em->persist($new_branch_master);
						$em->flush();
						
						if($main_branch_id == 0){
							$main_branch_id = $new_branch_master->getBranch_master_id();
						}
						
						if($main_branch == 1){
							#set all to 0 and make this one main branch
											
							$all_branch = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Branchmaster")->findBy(array('is_deleted'=>0,'main_restaurant_id'=>$main_restaurant_id));
							
							if($all_branch){
								foreach($all_branch as $branch_only){
									if($branch_only->getMain_branch_master_id() == $main_branch_id){
										$branch_only->setMain_branch_flag(1);
									}else{
										$branch_only->setMain_branch_flag(0);
									}
									$em->flush();
								}
							}						
							
						}
					
						$new_branch_master->setMain_branch_master_id($main_branch_id);
						$em->flush();
						
						$address_master = new Addressmaster();
                        $address_master->setAddress_name($address_branch);
                        $address_master->setAddress_name2(NULL);
                        $address_master->setOwner_id(0);
                        $address_master->setBase_address_type('main');
                        $address_master->setAddress_type('home');
                        $address_master->setContact_no(0);
                        $address_master->setCity_id(0);
                        $address_master->setArea_id(0);
                        $address_master->setStreet('');
                        $address_master->setFlate_house_number('');
                        $address_master->setSociety_building_name('');
                        $address_master->setLandmark('');
                        $address_master->setPincode(0);
                        $address_master->setLanguage_id($language_id_branch);
                        $address_master->setMain_address_id(0);
                        $address_master->setIs_defaulte_ship_address('true');
                        $address_master->setGmap_link('');
                        $address_master->setLat($lat);
                        $address_master->setLng($lng);
                        $address_master->setDomain_id(1);
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($address_master);
                        $em->flush();
                       
                        $address_master->setMain_address_id($address_master->getAddress_master_id());
						$main_address_id = $address_master->getAddress_master_id();
                        $em->flush();
						
						$new_branch_master->setBranch_address_id($main_address_id);
						$em->flush();	

						#common_changes
						$branch_master_all = $em->getRepository('AdminBundle:Branchmaster')->findBy(array("is_deleted" => 0, "main_branch_master_id" => $main_branch_id));
								
						if($branch_master_all){
							foreach($branch_master_all as $lang_branch){
								$lang_branch->setMain_branch_flag($main_branch);
								$lang_branch->setStatus($branch_status);
								$lang_branch->setMobile_no($mobile_no);
								$lang_branch->setOpening_date(date('Y-m-d',strtotime($opening_date)));
								$lang_branch->setTimings($timing);								
								$em->flush();
								
								#common lat lng changes
								$address_master_all = $em->getRepository('AdminBundle:Addressmaster')->findOneBy(array("is_deleted" => 0, "address_master_id" => $lang_branch->getBranch_address_id()));
								if($address_master_all){
								    $address_master_all->setLat($lat);
									$address_master_all->setLng($lng);	
									$em->flush();
								}	
								
							}
						}
						
						$this->get('session')->getFlashBag()->set('success_msg',"Branch Inserted successfully");	
						return $this->redirectToRoute('admin_restaurant_addbranch',array('domain'=>$this->get('session')->get('domain'),'main_branch_id'=>$main_branch_id,'rest_id'=>$main_restaurant_id));
			 }
		 			
		$referer = $req->headers->get('referer');
		return $this->redirect($referer);
	}	
	

}
