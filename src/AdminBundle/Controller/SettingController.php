<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Generalsetting;

/**
* @Route("/admin")
*/
class SettingController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/applink")
     * @Template()
     */
    public function applinkAction()
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$repository = $this->getDoctrine()->getRepository(Generalsetting::class);
		$general_setting = $repository->findBy(
			array(
				'general_setting_key' => 'app_link',
				'is_deleted' => 0
			)
		);
		
		if(empty($general_setting)){
			$general_setting = array();
		}
		
		$app_link = array();
		$_appLink = $general_setting[0]->getGeneral_setting_value();
		if(isset($_appLink) && $_appLink != ''){
			$_app_link = json_decode($_appLink);
			if(isset($_app_link->android_link) && $_app_link->android_link != ''){
				$app_link['android_link'] = $_app_link->android_link;
			}
			if(isset($_app_link->ios_link) && $_app_link->ios_link != ''){
				$app_link['ios_link'] = $_app_link->ios_link;
			}
		}			
		
		return array(
			'app_link' => $app_link
		);
	}
	
	/**
     * @Route("/websetting")
     * @Template()
     */
    public function websettingAction(Request $request)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$general_setting = $this->getDoctrine()->getRepository('AdminBundle:Generalsetting')->findOneBy([
			'general_setting_key' => 'visitor_member_setting',
			'is_deleted' => 0
		]);
		
		$all_setting = array();
		if(!empty($general_setting)){
			$value = $general_setting->getGeneral_setting_value();
			if($value != ''){
				$web_setting = json_decode($value);
				
				if(!empty($web_setting)){
					$all_setting['show_visitor'] = $web_setting->show_visitor;
					$all_setting['show_member'] = $web_setting->show_member;
				}
			}
		}
		
		return array(
			'web_setting' => $all_setting
		);
	}
	
	/**
     * @Route("/websetting/changestatus")
     * @Template()
     */
    public function changestatusAction(Request $request)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$type = $request->get('str');
			$em = $this->getDoctrine()->getManager();
			$general_setting = $em->getRepository('AdminBundle:Generalsetting')->findOneBy([
				'general_setting_key' => 'visitor_member_setting',
				'is_deleted' => 0
			]);
			
			if(!empty($general_setting)){
			$value = $general_setting->getGeneral_setting_value();
			if($value != ''){
				$web_setting = json_decode($value);
				
				if(!empty($web_setting)){
					$wetsetting = array();
					if($type == 'show_member'){
						if($web_setting->show_member == true){
							$wetsetting['show_member'] = false;
							$wetsetting['show_visitor'] = $web_setting->show_visitor;
						} else {
							$wetsetting['show_member'] = true;
							$wetsetting['show_visitor'] = $web_setting->show_visitor;
						}
					} else if($type == 'show_visitor') {
						if($web_setting->show_visitor == true){
							$wetsetting['show_visitor'] = false;
							$wetsetting['show_member'] = $web_setting->show_member;
						} else {
							$wetsetting['show_visitor'] = true;
							$wetsetting['show_member'] = $web_setting->show_member;
						}
					}
					$general_setting->setGeneral_setting_value(json_encode($wetsetting));
					$em->flush();
					
					echo 'true';exit;
				}
			}
		}
		echo 'false';exit;
	}
	
	/**
     * @Route("/applink/save")
     * @Template()
     */
    public function saveapplinkAction(Request $request)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if($request->request->all()){
			$repository = $this->getDoctrine()->getRepository(Generalsetting::class);
			$general_setting = $repository->findBy(
				array(
					'general_setting_key' => 'app_link',
					'is_deleted' => 0
				)
			);
			
			$android_link = $request->get('android-link');
			$ios_link = $request->get('ios-link');
			
			$app_link = array(
					'android_link' => $android_link,
					'ios_link' => $ios_link
				);
			
			$em = $this->getDoctrine()->getManager();
			
			if(empty($general_setting)){
				// insert
				
				$general_setting = new Generalsetting();
				$general_setting->setGeneral_setting_key('app_link');
				$general_setting->setGeneral_setting_value(json_encode($app_link));
				$general_setting->setDomain_id($this->get('session')->get('domain_id'));
				$general_setting->setIs_deleted(0);
				
				$em->persist($general_setting);
				$em->flush();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'App-Link inserted successfully');
			} else {
				$general_setting = $em->getRepository(Generalsetting::class)->findBy(
					array(
						'general_setting_key' => 'app_link'
					)
				);
				
				if(!empty($general_setting)){
					$_appLink = $general_setting[0]->getGeneral_setting_value();
					$new_app_link = array();
					$app_link = json_decode($_appLink);
					if(isset($android_link) && $android_link != ''){
						$app_link->android_link = $android_link;
					}
					if(isset($ios_link) && $ios_link != ''){
						$app_link->ios_link = $ios_link;
					}
					
					$general_setting[0]->setGeneral_setting_value(json_encode($app_link));
					$em->flush();
					
					$this->get('session')->getFlashBag()->set('success_msg', 'App-Link updated successfully');
				}
			}			
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
     * @Route("/change-profile")
     * @Template()
     */
    public function changeprofileAction(Request $request)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$session = $this->get('session');
		$admin_user_id = $session->get('admin_user_id');
		
		$admin_user = array();
		if(isset($admin_user_id)){
			$admin_user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findOneBy([
				'user_master_id' => $admin_user_id,
				'is_deleted' => 0
			]);
			
			if(!empty($admin_user)){
				return array(
					'admin_user' => $admin_user
				);
			}
		}
		
		return $this->redirectToRoute('admin_default_index');
	}
	
	/**
     * @Route("/change-profile/save")
     * @Template()
     */
    public function saveprofileAction(Request $request)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$session = $this->get('session');
		$admin_user_id = $session->get('admin_user_id');
		$email = $session->get('email');
		
		$old_password = $request->get('old_password');
		$new_password = $request->get('new_password');
		$confirm_password = $request->get('confirm_password');
		
		$error_flag = false;
		if(!isset($email) or $email == ''){
			$this->get('session')->getFlashBag()->set('error_msg', 'Current email id not found');
			$error_flag = true;
		}
		
		if($new_password != $confirm_password){
			$this->get('session')->getFlashBag()->set('error_msg', 'New Password and confirm password should be equal');
			$error_flag = true;
		}
		
		if($error_flag){
			$referer = $request->headers->get('referer');
			return $this->redirect($referer);
		}
		
		$admin_user = array();
		if(isset($old_password) && isset($new_password) && isset($confirm_password)){
			
			$em = $this->getDoctrine()->getManager();
			$admin_user = $em->getRepository('AdminBundle:Usermaster')->findOneBy([
				'email' => $email,
				'password' => md5($old_password),
				'is_deleted' => 0
			]);
			
			if(!empty($admin_user)){
				$admin_user->setUsername($request->get('username'));
				$admin_user->setEmail($request->get('username'));
				$admin_user->setPassword(md5($new_password));
				$em->flush();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Username and Password updated successfully');
			} else {
				$this->get('session')->getFlashBag()->set('error_msg', 'User not found');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'User not found');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
}
