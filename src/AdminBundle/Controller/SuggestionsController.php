<?php
namespace Main\AdminBundle\Controller;
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AdminBundle\Entity\Suggestioncomplaints;

use Symfony\Component\HttpFoundation\JsonResponse;
/**
* @Route("/admin")
*/
class SuggestionsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/suggestions")
     * @Template()
     */
    public function indexAction(){
		
//		$repository = $this->getDoctrine()->getRepository(Suggestioncomplaints::class);
//		$suggestions = $repository->findBy(array('is_deleted' => 0));
		
		$con = $this->getDoctrine()->getManager()->getConnection();
		$stmt = $con->prepare("SELECT suggestion_complaints.*,user_master.user_mobile FROM `suggestion_complaints` join user_master on suggestion_complaints.email_address = user_master.email where suggestion_complaints.is_deleted = '0' and user_master.is_deleted=0 ORDER BY `suggestion_complaints_id` DESC");
		$stmt->execute();
		$suggestions = $stmt->fetchAll();
		if(empty($suggestions)){
			$suggestions = array();
		}
		
		return array(
			'suggestions' => $suggestions
		);
    }
	
	/**
     * @Route("/suggestions/remove/{suggestion_id}")
     * @Template()
     */
    public function removeSuggestionsAction($suggestion_id, Request $request)
    {
		if(isset($suggestion_id)){
			$em = $this->getDoctrine()->getManager();
			$suggestion = $em->getRepository('AdminBundle:Suggestioncomplaints')->findOneBy(['suggestion_complaints_id' => $suggestion_id, 'is_deleted' => 0]);
			
			if(!empty($suggestion)){
				$suggestion->setIs_deleted(1);
				$em->flush();
				$this->get('session')->getFlashBag()->set('success_msg', 'Removed successfully');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
     * @Route("/bulk-suggestions/remove")
     * @Template()
     */
    public function removebulkSuggestionsAction(Request $request)
    {
		$feedback_ids = '';
		$postData = $_REQUEST;
		if(!array_key_exists('feedback_ids', $postData)){
			$data = array(
				'success' => false,
				'message' => 'Parameter not found'
			);
			
			echo json_encode($data);exit;
		}
		
		$feedback_id_str = $postData['feedback_ids'];
		$feedback_ids = explode(',', $feedback_id_str);
		
		if(!empty($feedback_ids)){
			foreach($feedback_ids as $_feedbackId){
				$em = $this->getDoctrine()->getManager();
				$suggestion = $em->getRepository('AdminBundle:Suggestioncomplaints')->findOneBy(['suggestion_complaints_id' => $_feedbackId, 'is_deleted' => 0]);		
				if(!empty($suggestion)){
					$suggestion->setIs_deleted(1);
					$em->flush();
				}
			}
			
			$data = array(
				'success' => true,
				'message' => "Removed successfully"
			);
		} else {
			$data = array(
				'success' => false,
				'message' => 'No value found'
			);
		}
		
		echo json_encode($data);exit;
	}

    /**
    * @Route("/getsuggestion")
     * @Template()
    */
    public function getsuggestionAction(Request $request){

            if($request->get('suggestion_id')){
                    $em = $this->getDoctrine()->getManager();
                    $suggestion = $em->getRepository(Suggestioncomplaints::class)->find($request->get('suggestion_id'));

                    $email_address = $suggestion->getEmail_address();
                    $nick_name = $suggestion->getNick_name();                   
                   
                    return new Response(json_encode(array("email"=>$email_address,"nick_name"=>$nick_name)));
                  
            }
            
    }
    
     /**
    * @Route("/sendemail")
    */
    public function sendemailAction(Request $request){
           $suggestion_id = $_REQUEST['suggestion_id'];
           $email_message = $_REQUEST['email_message'];
           $email_address = $_REQUEST['email_address_hidden'];
           
           // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: help@shrayek.com' . "\r\n";

            @mail($email_address,"Suggestion & Feedback Reply ",$email_message,$headers);
        
           $referer = $request->headers->get('referer');
		return $this->redirect($referer);
    }
  }
