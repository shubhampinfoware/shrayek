<?php
namespace Main\AdminBundle\Controller;
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AdminBundle\Entity\Contactus;
use AdminBundle\Entity\Contactusmaster;
use AdminBundle\Entity\Contactusfeedback;
use AdminBundle\Entity\Languagemaster;

use Symfony\Component\HttpFoundation\JsonResponse;
/**
* @Route("/admin")
*/
class ContactusController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
    * @Route("/contactus")
    * @Template()
    */
    public function indexAction(){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMC40", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$repository = $this->getDoctrine()->getRepository(Languagemaster::class);
		$languages = $repository->findBy(
				array(
					'is_deleted' => 0
				)
			);
		
		if(empty($languages)){
			$languages = array();
		}
		
		$repository = $this->getDoctrine()->getRepository(Contactus::class);
		$contact_info = $repository->findAll();
		
		if(empty($contact_info)){
			$contact_info = array();
		}
		
		return array(
			'languages' => $languages,
			'contact_info' => $contact_info
		);
    }
	
	/**
    * @Route("/contactus/save")
    */
	public function saveAction(Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMC40", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if($request->request->all()){
			
			$em = $this->getDoctrine()->getManager();
			
			$contactus = new Contactus();
			$contactus->setLanguage_id($request->get('language_id'));
			$contactus->setAddress($request->get('address'));
			$contactus->setPhone_number($request->get('phone_number'));
			$contactus->setEmail_address($request->get('email'));
			$contactus->setFb_link($request->get('fb_link'));
			$contactus->setTwitter_link($request->get('twitter_link'));
			$contactus->setInsta_link($request->get('insta_link'));
			$domain_id = $this->get('session')->get('domain_id');
			if(isset($domain_id)){
				$contactus->setDomain_id($domain_id);
			}
			
			$em->persist($contactus);
			$em->flush();
			
			$this->get('session')->getFlashBag()->set('success_msg', 'Data inserted successfully');
			
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
    * @Route("/contactus/update")
    */
	public function updateAction(Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMC40", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if($request->request->all()) {
			
			$contact_us_id = $request->get('contact_us_id');
			if(isset($contact_us_id)){
				$em = $this->getDoctrine()->getManager();
				$contactus = $em->getRepository(Contactus::class)->find($contact_us_id);
				
				if(!empty($contactus)) {
					
					$contactus->setAddress($request->get('address'));
					$contactus->setPhone_number($request->get('phone_number'));
					$contactus->setEmail_address($request->get('email'));
					$contactus->setFb_link($request->get('fb_link'));
					$contactus->setTwitter_link($request->get('twitter_link'));
					$contactus->setInsta_link($request->get('insta_link'));
					$domain_id = $this->get('session')->get('domain_id');
					if(isset($domain_id)){
						$contactus->setDomain_id($domain_id);
					}

					$em->flush();
					
					$this->get('session')->getFlashBag()->set('success_msg', 'Data updated successfully');
				}
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
    * @Route("/member-messages")
    * @Template()
    */
    public function contactusAction(){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMC40", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$repository = $this->getDoctrine()->getRepository(Contactusfeedback::class);
		$contact_info = $repository->findBy(array('is_deleted' => 0),array('created_date'=>'DESC'));
		
		if(empty($contact_info)){
			$contact_info = array();
		}
		
		return array(
			'contact_info' => $contact_info
		);
	}
	
	/**
    * @Route("/removecontactus/{contact_id}", defaults={"contact_id":""})
    * @Template()
    */
    public function removecontactusAction($contact_id,Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMC40", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if(isset($contact_id) && $contact_id != ''){
			
			$em = $this->getDoctrine()->getManager();
			$repository = $em->getRepository(Contactusfeedback::class);
			$contact_info = $repository->find($contact_id);
			if(!empty($contact_info)){
				$contact_info->setIs_deleted(1);
				$em->flush();
				$this->get('session')->getFlashBag()->set('success_msg', 'Removed successfully');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
    * @Route("/remove-bulk-contactus")
    * @Template()
    */
    public function removebulkcontactusAction(Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMC40", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$feedback_ids = '';
		$postData = $request->request->all();
		if(!array_key_exists('feedback_ids', $postData)){
			$data = array(
				'success' => false,
				'message' => 'Parameter not found'
			);
			
			echo json_encode($data);exit;
		}
		
		$feedback_id_str = $postData['feedback_ids'];
		$feedback_ids = explode(',', $feedback_id_str);
		
		if(!empty($feedback_ids)){
			foreach($feedback_ids as $_feedbackId){
				$em = $this->getDoctrine()->getManager();
				$repository = $em->getRepository(Contactusfeedback::class);
				$contact_info = $repository->find($_feedbackId);
				if(!empty($contact_info)){
					$contact_info->setIs_deleted(1);
					$em->flush();	
				}
			}
			
			$data = array(
				'success' => true,
				'message' => "Removed successfully"
			);
		} else {
			$data = array(
				'success' => false,
				'message' => 'No value found'
			);
		}
		
		echo json_encode($data);exit;
	}
	
	/**
    * @Route("/getfeedback")
     * @Template()
    */
    public function getfeedbackAction(Request $request){
		
		if($request->get('contact_id')){
			$em = $this->getDoctrine()->getManager();
			$feedback = $em->getRepository(Contactusfeedback::class)->find($request->get('contact_id'));
			
			if(!empty($feedback)){
				$email_address = $feedback->getEmail_address();
				$nick_name = $feedback->getName();                   
			}
			return new Response(json_encode(array("email"=>$email_address,"nick_name"=>$nick_name)));
		}
	}
	
	/**
    * @Route("/contactus/sendemail")
    */
    public function sendemailAction(Request $request){
		$feedback_id = $_REQUEST['contactus_id'];
		$email_message = $_REQUEST['email_message'];
		$email_address = $_REQUEST['email_address_hidden'];

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: help@shrayek.com' . "\r\n";

		@mail($email_address,"Member Message - Reply ",$email_message,$headers);
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
    }
}
