<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request; 
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Termsconditionsmaster;

/**
* @Route("/admin")
*/
class TermAndConditionController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/TermAndCondition",name="term_and_condition")
     * @Template()
     */
    public function indexAction()
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$em = $this->getDoctrine()->getManager();
		$language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($language)){
			$feedback_details = array();
		}
		
		$em = $this->getDoctrine()->getManager();
		$termsCondition = $em->getRepository(Termsconditionsmaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($termsCondition)){
			$termsCondition = array();
		}
	//	var_dump($termsCondition);exit;
		return (array('languages'=>$language,'termsCondition'=>$termsCondition));	
	}

		
	/**
     * @Route("/saveTermsCondition",name="save_terms")
     * @Template()
     */
    public function saveTermsAction(Request $req)
    {	
		$em = $this->getDoctrine()->getManager();
		$language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($language)){
			$feedback_details = array();
		}
		
		$em = $this->getDoctrine()->getManager();
		$termsCondition = $em->getRepository(Termsconditionsmaster :: class)->findOneBy(array('terms_conditions_master_id'=>$req->request->get('terms_id'),'is_deleted'=>0));
		if(empty($termsCondition)){
			$date = date('Y-m-d');
			$terms_details = new Termsconditionsmaster();
			$terms_details->setDescription($req->request->get('termsDetails'));
			$terms_details->setLanguage_id($req->request->get('language_id'));
			$terms_details->setCreate_date($date);
			$terms_details->setLast_updated($date);
			$em = $this->getDoctrine()->getManager();
			$em->persist($terms_details);
			$em->flush();
			$this->get('session')->getFlashBag()->set('success_msg', 'Terms saved successfully');
			return $this->redirectToRoute('term_and_condition');
		//	exit('save_terms');			
		}else{
			//var_dump($termsCondition);exit;	
			$date = date('Y-m-d');
			$termsCondition->setDescription($req->request->get('termsDetails'));			
			$termsCondition->setLanguage_id($req->request->get('language_id'));			
			$termsCondition->setLast_updated($date);
			$em->flush();
			$this->get('session')->getFlashBag()->set('success_msg', 'Terms updated successfully');
			return $this->redirectToRoute('term_and_condition');
		}
		
	}
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
}
