<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Areamaster;
use AdminBundle\Entity\Governoratemaster;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @Route("/admin")
 */
class UserController extends BaseController {

    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/userlist")
     * @Template()
     */
    public function userlistAction() {

        $all_category = '';
        /* if($domain_id == "" || $domain_id == 0 ){ */

        $query = "SELECT * FROM user_master WHERE is_deleted = 0 AND user_type!='admin' AND user_type!='shop' GROUP BY user_master_id";

        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $all_category = $em->fetchAll();

        return array("users" => $all_category);
        //var_dump($all_category_details);exit;
    }

    /**
     * @Route("/Add-Update-User/{user_master_id}",defaults={"user_master_id":""})
     * @Template()
     */
    public function adduserAction($user_master_id) {



        $governoratemaster = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Governoratemaster')
                ->findBy(array('is_deleted' => 0, 'language_id' => 1));

        $areamaster = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Areamaster')
                ->findBy(array('is_deleted' => 0, 'language_id' => 1));



        $live_path = $this->getParameter('live_path');
        $usermaster = null;
        if (!empty($user_master_id)) {
            $usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findBy(array('is_deleted' => 0, 'user_master_id' => $user_master_id));
        }


        return array("usermaster" => $usermaster, "user_master_id" => $user_master_id, "areamaster" => $areamaster, "governoratemaster" => $governoratemaster, "live_path" => $live_path);
    }

    /**
     * @Route("/adduserdb")
     * @Template()
     */
    public function adduserdbAction() {
        $session = new Session();

        if (isset($_REQUEST['save'])) {



            $firstname = $_REQUEST['firstname'];
            $lastname = $_REQUEST['lastname'];
            $email = $_REQUEST['email'];
            $password = md5($_REQUEST['password']);
            $mobile = $_REQUEST['mobile'];
            $governorate = $_REQUEST['governorate'];
            $area = $_REQUEST['area'];
            $user_type = $_REQUEST['user_type'];
            $registration_type = $_REQUEST['registration_type'];
            $status = $_REQUEST['status'];
            $user_master_id = '';

            if (isset($_REQUEST['user_master_id']) && !empty($_REQUEST['user_master_id'])) {

                $user_master_id = $_REQUEST['user_master_id'];
                $get_plan = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Usermaster')
                        ->findOneBy(array(
                    'user_master_id' => $user_master_id,
                    'is_deleted' => 0)
                );


                if (count($get_plan) > 0) {
                    $em = $this->getDoctrine()->getManager();
                    $update = $em->getRepository('AdminBundle:Usermaster')
                            ->find($get_plan->getUser_master_id());
                    $update->setFirstname($firstname);
                    $update->setLastname($lastname);
                    $update->setEmail($email);
                    $update->setPassword($password);
                    $update->setMobile($mobile);
                    $update->setGovernorate($governorate);
                    $update->setArea($area);
                    $update->setUser_type($user_type);
                    $update->setRegistration_type($registration_type);
                    $update->setStatus($status);




                    $em->flush();


                    //-------------------------------- Common Changes -----------------------------
                    //-------------------------------- Common Changes -----------------------------
                    $this->get('session')->getFlashBag()->set('success_msg', 'Customer Updated Successfully!');
                    return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))) . '/' . $user_master_id);
                } else {

                    $usermaster = new Usermaster();
                    $usermaster->setFirstname($firstname);
                    $usermaster->setLastname($lastname);
                    $usermaster->setEmail($email);
                    $usermaster->setPassword($password);
                    $usermaster->setMobile($mobile);
                    $usermaster->setGovernorate($governorate);
                    $usermaster->setArea($area);
                    $usermaster->setUser_type($user_type);
                    $usermaster->setRegistration_type($registration_type);
                    $usermaster->setStatus($status);



                    //$usermaster->setUser_master_id($user_master_id);
                    $usermaster->setIs_deleted(0);


                    $em = $this->getDoctrine()->getManager();
                    $em->persist($usermaster);
                    $em->flush();

                    if (isset($user_master_id) && !empty($user_master_id)) {
                        $this->get('session')->getFlashBag()
                                ->set('success_msg', 'Customer Inserted Successfully!');
                        return $this->redirect($this
                                                ->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))) . '/' . $user_master_id);
                    } else {
                        $this->get('session')->getFlashBag()->set('error_msg', 'Customer Insertion Failed!');
                        return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))));
                    }
                }
            } else {
                $usermaster = new Usermaster();

                $usermaster = new Usermaster();
                $usermaster->setFirstname($firstname);
                $usermaster->setLastname($lastname);
                $usermaster->setEmail($email);
                $usermaster->setPassword($password);
                $usermaster->setMobile($mobile);
                $usermaster->setGovernorate($governorate);
                $usermaster->setArea($area);
                $usermaster->setUser_type($user_type);
                $usermaster->setRegistration_type($registration_type);
                $usermaster->setStatus($status);

                $usermaster->setIs_deleted(0);


                $em = $this->getDoctrine()->getManager();
                $em->persist($usermaster);
                $em->flush();


                $user_master_id = $usermaster->getUser_master_id();

                if (isset($user_master_id) && !empty($user_master_id)) {
                    $em = $this->getDoctrine()->getManager();
                    $update = $em->getRepository('AdminBundle:Usermaster')->find($user_master_id);

                    $em->flush();
                    $this->get('session')->getFlashBag()->set('success_msg', 'Customer Inserted Successfully!');
                    return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))) . '/' . $user_master_id);
                } else {
                    $this->get('session')->getFlashBag()->set('error_msg', 'Customer Insertion Failed!');
                    return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))));
                }
            }
        }
    }

    /**

     * @Route("/statususer")

     * @Template()

     */
    public function statususerAction() {
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {

            $request = $this->getRequest();
            $session = $request->getSession();
            $em = $this->getDoctrine()->getManager();
            $status = $_REQUEST['status'];
            $id = $_REQUEST['id'];
            if ($_REQUEST['status'] == "true") {
                $status = "active";
            } else {
                $status = "inactive";
            }

            $list = $em->getRepository('AdminBundle:Usermaster')
                    ->findBy(
                    array(
                        'user_master_id' => $id,
                        'is_deleted' => 0
                    )
                    );



            if (!empty($list)) {

                foreach ($list as $key => $val) {

                    $user = $em->getRepository('AdminBundle:Usermaster')
                            ->findOneBy(
                            array(
                                'user_master_id' => $val->getUser_master_id(),
                                'is_deleted' => 0
                            )
                            );

                    $user->setStatus($status);

                    $em->persist($user);

                    $em->flush();
                }
            }
        }

        return new Response();
    }

    /**
     * @Route("/checkemailexist")
     */
    public function checkemailexistAction() {
        $session = new Session();

        if (!empty($_POST['email_id'])) {
            $Usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository("AdminBundle:Usermaster")
                    ->findBy(array(
                "email" => $_POST['email_id'],
                'user_type' => 'user',
                //"domain_id" =>  $session->get("domain_id"),
                "is_deleted" => 0,
                    )
            );
            if (!empty($Usermaster)) {
                return new Response(1);
            } else {
                return new Response(0);
            }
        }
    }

    /**
     * @Route("/checkphoneexist")
     */
    public function checkphoneexistAction() {
        $session = new Session();

        if (!empty($_POST['phone_number'])) {
            $Usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository("AdminBundle:Usermaster")
                    ->findBy(array(
                "mobile_number" => $_POST['phone_number'],
                'user_type' => 'user',
                //"domain_id" =>  $session->get("domain_id"),
                "is_deleted" => 0,
                    )
            );
            if (!empty($Usermaster)) {
                return new Response(1);
            } else {
                return new Response(0);
            }
        }
    }

}
