<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Areamaster;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Governoratemaster;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Userroleright;
use AdminBundle\Entity\Restaurantmaster;

/**
 * @Route("/admin")
 */
class UsertypeController extends BaseController {

    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/usertypes")
     * @Template()
     */
    public function indexAction() {
        // load user list from dataTables function - getUserListAction
		$session = $this->get('session');
		/* if($session->get('role_id') == 1){
			$user_types_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userrolemaster")->findBy(array("is_deleted" => 0));
		} else {
			$user_types_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userrolemaster")->findBy(array("is_deleted" => 0, 'user_role_master_id' => $session->get('role_id')));
		} */
		$user_types_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userrolemaster")->findBy(array("is_deleted" => 0));
        
		return array("user_types" => $user_types_info);
    }

    /**
     * @Route("/addusertype/{user_type_id}",defaults={"user_type_id"=0})
     * @Template()
     */
    public function addusertypeAction($user_type_id) {

        $this->userrightsAction();

        $right_arr = [];
        // load user list from dataTables function - getUserListAction
        $user_types_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userrolemaster")->findOneBy(array("is_deleted" => 0, "user_role_master_id" => $user_type_id));
        $user_right_list = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userrights")->findBy(array("is_deleted" => 0));
        if (!empty($user_right_list)) {
            foreach ($user_right_list as $urkey => $urval) {
                //check that Right is Selected per User tYpe 
               // if($user_type_id != 3 ){
                $user_role_right_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userroleright")->findOneBy(array("is_deleted" => 0, "right_id" => $urval->getRight_master_id(), "role_id" => $user_type_id));
                $checked = "false";
                if (!empty($user_role_right_info)) {
                    $checked = "true";
                }
                $right_arr[] = array(
                    "right_master_id" => $urval->getRight_master_id(),
                    "display_name" => $urval->getDisplay_name(),
                    "checked" => $checked
                );
               // }
            }
        } 

		$sql = "select module.display_name as module_name, rights.* from user_rights rights, module_master module where rights.module_id = module.module_master_id and rights.is_deleted = 0";
		$right_arr = $this->firequery($sql);
		
		$final_array = array();
		if(!empty($right_arr)){
			foreach($right_arr as $_right){
				
				$user_role_right_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Userroleright")->findOneBy(array("is_deleted" => 0, "right_id" => $_right['right_master_id'], "role_id" => $user_type_id));
                $checked = "false";
                if (!empty($user_role_right_info)) {
                    $checked = "true";
                }
				
				$_right['checked'] = $checked;
				
				$final_array[$_right['module_name']][] = $_right;
			}
		}
		
        return array("user_type_id" => $user_type_id, "user_types" => $user_types_info, "right_list" => $final_array);
    }

    /**
     * @Route("/updateusertype")
     * @Template()
     */
    public function updateusertypeAction(Request $request) {
        $postdata = $request->request->all();
        $referer = $request->headers->get('referer');

        if (!array_key_exists('user_type_id', $postdata)) {
            $this->get('session')->getFlashBag()->set('error_msg', 'User type not found');
            return $this->redirect($referer);
        }
        if (!array_key_exists('user_rights', $postdata)) {
            $this->get('session')->getFlashBag()->set('error_msg', 'Please select rights');
            return $this->redirect($referer);
        }

        $user_rights = $request->get('user_rights');
        if (empty($user_rights)) {
            $this->get('session')->getFlashBag()->set('error_msg', 'Please select rights');
            return $this->redirect($referer);
        }

        $user_type_id = $request->get('user_type_id');
        $em = $this->getDoctrine()->getManager();
        $user_role_rights = $em->getRepository("AdminBundle:Userroleright")->findBy(
                array(
                    "is_deleted" => 0,
                    "role_id" => $user_type_id
                )
        );

        // remove older rights
        if (!empty($user_role_rights)) {
            foreach ($user_role_rights as $_userRole) {
                $_userRole->setIs_deleted(1);
                $em->flush();
            }
        }

        if (!empty($user_rights)) {
            foreach ($user_rights as $_right) {
                $user_role_right = new Userroleright();
                $user_role_right->setRole_id($user_type_id);
                $user_role_right->setRight_id($_right);
                $user_role_right->setDomain_id(1);
                $user_role_right->setIs_deleted(0);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user_role_right);
                $em->flush();
            }
            $this->get('session')->getFlashBag()->set('success_msg', 'Role rights updated successfully');
        }

        //$_url = $this->generateUrl('admin_usertype_index');
        return $this->redirect($referer);
    }

    /**
     * @Route("/adminuser/list")
     * @Template()
     */
    public function adminuserlistAction(Request $request) {
		
		$session = $this->get('session');
		
		$roles = " and user_role_id in ('2','4','5','6','7')";
		if($session->get('role_id') == 1){
			$roles = " and user_role_id in ('1','2','4','5','6','7')";
		}
		
        $conn = $this->getDoctrine()->getManager()->getConnection();
		$prepare = $conn->prepare("select user_master.*,user_role_master.user_role_name from user_master JOIN user_role_master ON user_master.user_role_id = user_role_master.user_role_master_id where user_master.is_deleted = 0 {$roles} order by created_datetime");
        $prepare->execute();
        $admin_user_list = $prepare->fetchAll();

        $live_path = $this->container->getParameter('live_path');
        $admin_list = array();
        if (!empty($admin_user_list)) {
            foreach ($admin_user_list as $_admin) {
				$media_url = $this->getImage($_admin['user_image'], $live_path);
                if ($media_url == '') {
                    $_admin['admin_image_url'] = $live_path . '/' . 'bundles/design/images/default-user.png';
                } else {
                    $_admin['admin_image_url'] = $media_url;
                }
                $admin_list[] = $_admin;
            }
        }

        return array(
            'admin_list' => $admin_list
        );
    }

    /**
     * @Route("/adminuser/add/{user_id}", defaults={"user_id"=""})
     * @Template()
     */
    public function addadminuserAction($user_id, Request $request) {
		
		$user_data = array();
		if($user_id != ''){
			$user_data = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findOneBy([
				'user_master_id' => $user_id,
				'is_deleted' => 0
			]);
		}
		
		$session = $this->get('session');
		$filter = array(
				'status' => 'active',
				'is_deleted' => 0
			);
		
        // get userroles
        $user_roles = $this->getDoctrine()->getRepository('AdminBundle:Userrolemaster')->findBy($filter);

        return array(
            'user_roles' => $user_roles,
			'user_data' => $user_data
        );
    }

    /**
     * @Route("/adminuser/saveadminuser")
     * @Template()
     */
    public function saveadminuserAction(Request $request) {
        $postdata = $request->request->all();
        $referer = $request->headers->get('referer');
	
		$em = $this->getDoctrine()->getManager();
		
		$hasError = true;
		if(array_key_exists('hidden_user_id', $postdata)){
			// edit
			$hidden_user_id = $postdata['hidden_user_id'];
			$user_master = $em->getRepository('AdminBundle:Usermaster')->findOneBy([
				'user_master_id' => $hidden_user_id,
				'is_deleted' => 0
			]);
			
			if(!empty($user_master)){
				
				// check email/username
				$check_username_email_sql = "select email, username from user_master where is_deleted = 0 and user_master_id != {$hidden_user_id}";
				$check_username_email = $this->firequery($check_username_email_sql);
				
				if(!empty($check_username_email)){
					
					$emailExists = false;
					$usernameExists = false;
					
					foreach($check_username_email as $_checkUsernameEmail){
						/*if($postdata['email'] == $_checkUsernameEmail['email']){
							$emailExists = true;
						}*/
						if($postdata['username'] == $_checkUsernameEmail['username']){
							$usernameExists = true;
						}
					}
					
					if($usernameExists){
						$this->get('session')->getFlashBag()->set('error_msg', 'Username already exists');
						return $this->redirect($referer);
					}
					if($emailExists){
						$this->get('session')->getFlashBag()->set('error_msg', 'Email address already exists');
						return $this->redirect($referer);
					}
				}
				
				$hasError = false;
			}
		} else {
			// insert
			
			// check email address
			$check_user_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Usermaster')->findOneBy(array("is_deleted"=>0,"username"=>$postdata['username']));
			
			if(!empty($check_user_name)){
				// can't insert
				$this->get('session')->getFlashBag()->set('error_msg', 'Email address/Username already exists');
				return $this->redirect($referer);
			}
			
			$user_master = new Usermaster();
			$hasError = false;
		}
		
		$user_id = '';
		if(!$hasError){
			$user_master->setUser_role_id($postdata['userrole']);
			$user_master->setUser_bio($postdata['nickname']);
			$user_master->setEmail($postdata['username']);
			$user_master->setUsername($postdata['username']);
			$user_master->setPassword(md5($postdata['password']));
			$user_master->setShow_password($postdata['password']);
			$user_master->setUser_firstname($postdata['firstname']);
			$user_master->setUser_lastname($postdata['lastname']);
			$user_master->setUser_mobile('');
			$user_master->setUser_image(0);
			$user_master->setImage_url(NULL);
			$user_master->setAddress_master_id(0);
			$user_master->setParent_user_id(0);
			$user_master->setUser_gender('male');
			$user_master->setDate_of_birth(date("1991-08-08"));
			$user_master->setCreated_by($this->get('session')->get('user_id'));
			$user_master->setStatus($postdata['status']);
			$user_master->setUser_type('user');
			$user_master->setCurrent_lang_id(1);
			$user_master->setDomain_id(1);
			$user_master->setCreated_datetime(date("Y-m-d H:i:s"));
			$user_master->setLast_modified(date("Y-m-d H:i:s"));
			$user_master->setLast_login(NULL);
			$user_master->setIs_deleted(0);
			
			if(array_key_exists('hidden_user_id', $postdata)){
				$this->get('session')->getFlashBag()->set('success_msg', 'Succssfully Updated');
			} else {
				$em->persist($user_master);
				$this->get('session')->getFlashBag()->set('success_msg', 'Succssfully Inserted');
			}
			
			$em->flush();
			
			$user_id = $user_master->getUser_master_id();
		}
		
		if($user_id != ''){
			$referer = $this->generateUrl('admin_usertype_addadminuser', array('user_id' => $user_id));
		}
		
		return $this->redirect($referer);
    }
	
	/**
     * @Route("/remove/admin_user/{id}",defaults={"id"=""})
     * @Template()
     */
    public function removeAdminUserAction($id, Request $request) {
		
		if($id != ""){
			
			$sql = "select user_master.*,user_role_master.user_role_name from user_master JOIN user_role_master ON user_master.user_role_id = user_role_master.user_role_master_id where user_role_id in ('2','4','5','6','7')";
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository('AdminBundle:Usermaster')->findOneBy([
				'user_master_id' => $id,
				'is_deleted' => 0
			]);
			
			$admin_role = ['2', '4', '5', '6', '7'];
			if(!empty($user)){
				if(in_array($user->getUser_role_id(), $admin_role)){
					$user->setIs_deleted(1);
					$em->flush();
					$this->get('session')->getFlashBag()->set('success_msg', 'Admin User Removed Successfully');
				} else {
					$this->get('session')->getFlashBag()->set('error_msg', 'Not an Admin User');
				}
			} else {
				$this->get('session')->getFlashBag()->set('error_msg', 'User not found');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'User not found');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
	* @Route("/cat/update_user_status")
	*/
	public function updateUserStatusAction(Request $request){

		if($request->get('id')){
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository('AdminBundle:Usermaster')->findOneBy(array('user_master_id' => $request->get('id'), 'is_deleted' => 0));

			if(!empty($user)){				
				$current_status = $user->getStatus();
				if($current_status == 'active'){
					$user->setStatus('inactive');
				} else {
					$user->setStatus('active');
				}
				$em->flush();
			}
			echo 'true';exit;
		}
		echo 'false';exit;
	}
}