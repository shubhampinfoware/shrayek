<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Reviewmaster;

/**
* @Route("/admin")
*/
class ReviewController extends BaseController
{
    /**
     * @Route("/review",name="admin_review")
     * @Template()
     */
    public function indexAction()
    {
		$get_review_sql = "select review.*,rest.restaurant_name,rest.main_restaurant_id,user.username,user.user_master_id 
						   from review_master review join restaurant_master rest on review.restaurant_id=rest.main_restaurant_id
						   join user_master user on review.created_by=user.user_master_id where user.is_deleted=0 and review.is_deleted =0 and rest.is_deleted=0";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($get_review_sql);
		$stmt->execute();
		$review_data = $stmt->fetchAll();	
//		echo "<pre>";print_r($review_data);exit;
		return (array('review_data'=>$review_data));	
	} 

	/**
     * @Route("/ViewReview/{review_id}",name="admin_review_viewReview",defaults={"review_id"="0"})
     * @Template()
     */
    public function viewReviewAction($review_id)
    {
		$review = array();	
		$review_gallery_sql = array();
		$get_review_sql = "select review.*,rest.restaurant_name,rest.main_restaurant_id,user.username,user.user_master_id 
						   from review_master review join restaurant_master rest on review.restaurant_id=rest.main_restaurant_id
						   join user_master user on review.created_by=user.user_master_id where user.is_deleted=0 and review.is_deleted =0 and rest.is_deleted=0
						   and review.review_master_id='".$review_id."'";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($get_review_sql);
		$stmt->execute();
		$review = $stmt->fetchAll();
		
		if($review){
			$review_gallery_sql = "select media.* from media_library_master media join review_gallery review_gallery on review_gallery.media_id = media.media_library_master_id where review_gallery.is_deleted=0 and review_gallery.review_id='".$review_id."'";	
			$em = $this->getDoctrine()->getManager();
			$con = $em->getConnection();
			$stmt = $con->prepare($review_gallery_sql);
			$stmt->execute();
			$review_gallery_sql = $stmt->fetchAll();			
		}

		$review_data = array('review'=>$review,
								   'review_gallery'=>$review_gallery_sql);
//		var_dump($review_data);exit;
		return(array('review_data'=>$review_data));
	}

	
    /**
     * @Route("/deleteReview/{review_id}",name="admin_delete_review",defaults={"review_id"="0"})
     * @Template()
     */
    public function deleteReviewAction($review_id)
    {
		$em = $this->getDoctrine()->getManager();
		$review = $em->getRepository(Reviewmaster :: class)->findOneBy(array('review_master_id'=>$review_id));
		
		if($review){
			$review->setIs_deleted(1);
			$em->flush();
		}
			$this->get('session')->getFlashBag()->set('success_msg', 'Review Deleted successfully');		
			return $this->redirectToRoute('admin_review');
		
	} 

	
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
}
