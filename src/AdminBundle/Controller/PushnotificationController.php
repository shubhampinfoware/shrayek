<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Generalnotification;

/**
 * @Route("/admin")
 */
class PushnotificationController extends BaseController {

	private $userPage = 200;
	
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/pushnotification")
     * @Template()
     */
    public function indexAction() {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMPN41", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        $domain_id = 1 ;
        if ($this->get('session')->get('role_id') == '1') {

            $note_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Generalnotification')
                    ->findBy(array('is_deleted' => 0, 'notification_type' => 'general'), array('create_date' => 'desc'));

            $health_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Generalnotification')
                    ->findBy(array('is_deleted' => 0, 'notification_type' => 'healthtip'), array('create_date' => 'desc'));
        } else {

            $note_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Generalnotification')
                    ->findBy(array('is_deleted' => 0, 'notification_type' => 'general', 'domain_id' => $this->get('session')->get('domain_id')), array('create_date' => 'desc'));

            $health_list = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Generalnotification')
                    ->findBy(array('is_deleted' => 0, 'notification_type' => 'healthtip', 'domain_id' => $this->get('session')->get('domain_id')), array('create_date' => 'desc'));
        }

		return array("note_list" => $note_list, "health_list" => $health_list);
    }

	/**
     * @Route("/notification/delete/{id}",defaults={"id"=""})
     * @Template()
     */
    public function deleteNotificationAction($id, Request $request) {
		if($id != ''){
			$entity = $this->getDoctrine()->getManager();
			$note_list = $entity->getRepository('AdminBundle:Generalnotification')->findOneBy([
				'general_notification_id' => $id,
				'is_deleted' => 0
			]);
			
			if(!empty($note_list)){
				$note_list->setIs_deleted(1);
				$entity->flush();
				$this->get('session')->getFlashBag()->set('success_msg', 'Notification Removed Successfully');
			} else {
				$this->get('session')->getFlashBag()->set('error_msg', 'Notification not found');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Notification not found');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
     * @Route("/notification-bulk/delete")
     * @Template()
     */
    public function deleteNotificationBulkAction(Request $request) {
		$postData = $_REQUEST;
		if(!array_key_exists('note_ids', $postData)){
			$data = array(
				'success' => false,
				'message' => 'Parameter not found'
			);
			
			echo json_encode($data);exit;
		}
		
		$note_id_str = $postData['note_ids'];
		$note_ids = explode(',', $note_id_str);
		
		if(!empty($note_ids)){
			$entity = $this->getDoctrine()->getManager();
			foreach($note_ids as $_noteId){
				$note_list = $entity->getRepository('AdminBundle:Generalnotification')->findOneBy([
					'general_notification_id' => $_noteId,
					'is_deleted' => 0
				]);
				
				if(!empty($note_list)){
					$note_list->setIs_deleted(1);
					$entity->flush();
				}
			}
			$data = array(
				'success' => true,
				'message' => "Removed successfully"
			);
		} else {
			$data = array(
				'success' => false,
				'message' => 'No value found'
			);
		}
		
		echo json_encode($data);exit;
	}
	
	
    /**

     * @Route("/addpushnotification/{notification_id}",defaults={"notification_id"="0"})

     * @Template()

     */
    public function addpushnotificationAction($notification_id) {

		$userLimits = $this->userPage;
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMPN41", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		$user_list = null;
		$userListCount = 0;
		
		if($notification_id){
			
			if(!empty($this->get('session')->get('user_page'))){
				$user_page = $this->get('session')->get('user_page') ;
				
				$user_page = $user_page + 1 ;
				$this->get('session')->set('user_page',$user_page);
			}else{
				$this->get('session')->set('user_page',1);
				$user_page = $this->get('session')->get('user_page') ;
			}
			
		//	echo $user_page;exit;
			if(!empty($user_page)){
				$offeset1 = ( $user_page - 1 ) * $userLimits;				
			}else{
				$offeset1 = 0;
			}

			/* $user_list =$this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findBy(array("is_deleted"=>0,"user_role_id"=>3,"status"=>"active"),null,$userLimits,$offeset1); */
			$user_list =$this->getDoctrine()->getManager()->getRepository("AdminBundle:Usermaster")->findBy(array("is_deleted"=>0,"user_role_id"=>3,"status"=>"active"));
			
			$userListCount = count($user_list);
			
			if(empty($user_list)){
				$this->get('session')->getFlashBag()->set('success_msg', "Notification sent successfully");

				return $this->redirect($this->generateUrl('admin_pushnotification_index', array("domain" => $this->get('session')->get('domain'))));
				
			}
		}else{
			$this->get('session')->remove('user_page');
		}
		
		

        return array( "user_list" => $user_list,"notification_id"=>$notification_id,"userListCount"=>$userListCount );
    }

    /**

     * @Route("/sendnotification/{notification_id}",defaults={"notification_id"="0"})

     */
    public function sendnotificationAction($notification_id) {
        
		$em = $this->getDoctrine()->getManager();

		$send_to = 'customer';
		 
		$media_id = "FALSE"; 
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMPN41", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
        if (isset($_POST['send_notification']) && $_POST['send_notification'] == "send_notification") {
            
			
			if($notification_id == 0){
                
				#only save notification  in Generalnotification
			
					if ($_POST['note_title'] != "" && $_POST['note_message'] ) {
					
					if (!empty($_FILES['image'])) {
						$Config_live_site = $this->container->getParameter('live_path');
						$file_path = $this->container->getParameter('file_path');
						$file = $_FILES['image']['name'];     // only profile image is allowed
						$extension = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
						if ($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg') {
							$tmpname = $_FILES['image']['tmp_name'];
							$path = $file_path . '/uploads/notification';
							$upload_dir = $this->container->getParameter('upload_dir') . '/uploads/notification/';
							$media_id = $this->mediauploadAction($file, $tmpname, $path, $upload_dir, 1);
							
						}
					}
				
					$general_notification = new Generalnotification();
					$general_notification->setNotification_type($_POST['notification_type']);
					$general_notification->setTitle($_POST['note_title']);
					$general_notification->setMessage($_POST['note_message']);
					if (!empty($_FILES['image'])) {
						if ($media_id != "FALSE") {
							$general_notification->setImage_id($media_id);
						}
					}else{
						$general_notification->setImage_id(0);
					}
					$general_notification->setUser_master_id(0);
					$general_notification->setSend_to($send_to);
					if (!empty($this->get('session')->get('domain_id'))) {
						$general_notification->setDomain_id($this->get('session')->get('domain_id'));
					}
					$general_notification->setCreate_date(date("Y-m-d H:i:s"));
					$general_notification->setIs_deleted(0);
					
					$em->persist($general_notification);
					$em->flush();

					$notification_id_send = $general_notification->getGeneral_notification_id();
				
					$this->get('session')->getFlashBag()->set('success_msg', "Notification has been saved , Please select users to send.");
										
					return $this->redirect($this->generateUrl('admin_pushnotification_addpushnotification', array("domain" => $this->get('session')->get('domain'),"notification_id"=>$notification_id_send)));
						
				} else {

					return $this->redirect($this->generateUrl('admin_pushnotification_addpushnotification', array("domain" => $this->get('session')->get('domain'))));
				
					$this->get('session')->getFlashBag()->set('error_msg', "Notification title and message is required");
				}
			}	
				
				#only save notification  in Generalnotification done
				
				#send to users 
				if($notification_id != 0){
					
					$userListCount = $_POST['userListCount'];
								
					$notificationDetails = $em->getRepository("AdminBundle:Generalnotification")->findOneBy(["general_notification_id"=>$notification_id]);
					
					if($notificationDetails){
						$Config_live_site = $this->container->getParameter('live_path');
						
						$title = $notificationDetails->getTitle();
						$message_notification= $notificationDetails->getMessage();
						$notification_type = $notificationDetails->getNotification_type();
						$media_id = $notificationDetails->getImage_id();
						
						if (!empty($notification_type) && $notification_type == 'healthtip') {
							$code = '7';
						} elseif (!empty($notification_type) && $notification_type == 'app_alert') {
							$code = '11';
						} else {
							$code = '10';
						}
						
						if(!empty($media_id)){
							$media_library = $this->getDoctrine()
                                    ->getManager()
                                    ->getRepository('AdminBundle:Medialibrarymaster')
                                    ->findOneBy(array('media_library_master_id' => $media_id, 'is_deleted' => 0));
							if($media_library){
								$response = array(
									"notification_title" => $title,
									"notification_image" => $Config_live_site . $media_library->getMedia_location() . "/" . $media_library->getMedia_name()
								);		
								$message = json_encode(array("detail" => $message_notification, "code" => $code, "response" => $response, 'notification_image' => $Config_live_site . $media_library->getMedia_location() . "/" . $media_library->getMedia_name()));								
							}else{
								$message = json_encode(array("detail" => $message_notification, "code" => $code, "response" => $title, 'notification_image' => null));
							}
							
						}else{
							$message = json_encode(array("detail" => $message_notification, "code" => $code, "response" => $title, 'notification_image' => null));
						}
						
						$domain_id = 1;
						$app_id = 'CUST';
						$user_array = array();
						$user_apns_id = array();
						
						if (isset($_POST['user']) && !empty($_POST['user'])) {
							foreach ($_POST['user'] as $check) {
								$user_array[] = $check;
							}
						}
						
						$user_gcm_id = array();
						
						if (isset($_POST['guest_user'])) {

							$user_master_guest_gcm = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Gcmuser')
												->findBy(array("user_type" => "guest", "is_deleted" => 0));				
						
							if($user_master_guest_gcm){
								foreach ($user_master_guest_gcm as $key => $val) {
									$user_gcm_id[] = $val->getGcm_regid();
								}						
							}
							if(!empty($user_gcm_id)){
								$this->send_notification($user_gcm_id, $title, $message, 2, $app_id, $domain_id, "general_notification", $notification_id);											
							}

							$user_master_guest_apns = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Apnsuser')
												->findBy(array("user_type" => "guest", "is_deleted" => 0));				
						
							if($user_master_guest_apns){
								foreach ($user_master_guest_apns as $key => $val) {
									$user_apns_id[] = $val->getApns_regid();
								}						
							}
							if(!empty($user_apns_id)){
								$this->send_notification($user_apns_id, $title, $message, 1, $app_id, $domain_id, "general_notification", $notification_id);											
							}
						}
						
						if ($send_to == "customer") {
							if (isset($_POST['sendall'])) {  // not in use now
								if (!empty($this->get('session')->get('domain_id'))) {
									if ($this->get('session')->get('role_id') == '1') {

										$user_master = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Usermaster')
												->findBy(array("user_role_id" => 7, "status" => "active", "user_type" => "user", "is_deleted" => 0));

										//"user_type"=>"user"



										foreach ($user_master as $key => $val) {
											$user_array[] = $val->getUser_master_id();
										}
									} else {

										$user_master = $this->getDoctrine()
												->getManager()
												->getRepository('AdminBundle:Usermaster')
												->findBy(array("user_role_id" => 7, "user_status" => "active", "user_type" => "user", "is_deleted" => 0, "domain_id" => $this->get('session')->get('domain_id')));

										//"user_type"=>"user"



										foreach ($user_master as $key => $val) {

											$user_array[] = $val->getUser_master_id();
										}
									}
								}
							}
						}
						
						$gcm_regids = $this->find_gcm_regid($user_array);
						
						if (!empty($gcm_regids)) {
							if (count($gcm_regids) > 0) {
								$this->send_notification($gcm_regids, $title, $message, 2, $app_id, $domain_id, "general_notification", $notification_id);
							}
						}
						
						$apns_regids = $this->find_apns_regid($user_array);
						
						if (!empty($apns_regids)) {

							if (count($apns_regids[0]) > 0) {

								$this->send_notification($apns_regids, $title, $message, 1, $app_id, $domain_id, "general_notification", $notification_id);
							}
						}
						
						$this->get('session')->getFlashBag()->set('success_msg', "Notification sent successfully");

						if($userListCount < $this->userPage){
							return $this->redirect($this->generateUrl('admin_pushnotification_index', array("domain" => $this->get('session')->get('domain'))));
						}else{
							return $this->redirect($this->generateUrl('admin_pushnotification_addpushnotification', array("domain" => $this->get('session')->get('domain'),"notification_id"=>$notification_id)));
						}

						
					}
				}
				#send to users done 
            
        } else {

            $this->get('session')->getFlashBag()->set('error_msg', 'Oops! Something goes wrong! Try again later');
        }

        return $this->redirect($this->generateUrl('admin_pushnotification_addpushnotification', array("domain" => $this->get('session')->get('domain'))));
    }

    /**

     * @Route("/getuserlist")

     */
    public function getuserlistAction() {
        $html = "";
        if (isset($_POST['flag']) && $_POST['flag'] == 'getuser' && $_POST['user_type'] != "") {

            $user_type = $_POST['user_type'];
            $city_id = !empty($_POST['city_id']) ? $_POST['city_id'] : 0;
            $user_list = array();
            if ($user_type == 'CUST') {

                $user_list = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Usermaster')
                        ->findBy(array("user_role_id" => 7, "user_status" => "active", "is_deleted" => 0, "domain_id" => $this->get('session')->get('domain_id'), "user_type" => "user"));
            }

            if ($user_type == 'DEL') {

                $user_list = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Usermaster')
                        ->findBy(array("user_role_id" => 6, "user_status" => "active", "is_deleted" => 0, "domain_id" => $this->get('session')->get('domain_id'), "user_type" => "user"));
            }



            if (!empty($user_list)) {



                $html .= '<label class="col-sm-2 control-label">&nbsp;</label>

								<div class="col-md-10">

									<div class="box box-success box-solid">

										<div class="box-header with-border">

											<h3 class="box-title">Users list</h3>

											<input type="checkbox" id="checkAll" class="checkbox pull-right"/>

										</div>

										<div class="box-body" id="userlistbox">';

                foreach ($user_list as $key => $val) {

                    if (isset($city_id) && !empty($city_id)) {

                        $address_id = 0;

                        $order_master_id = "";

                        $em = $this->getDoctrine()->getManager();

                        $connection = $em->getConnection();

                        $statement = $connection->prepare("select delivery_address_id from order_master where order_createdby='" . $val->getUser_master_id() . "' order by `order_master_id` desc LIMIT 0,1");

                        $statement->execute();

                        $order_master_id = $statement->fetchAll();

                        if (isset($order_master_id) && !empty($order_master_id)) {

                            $address_id = $order_master_id[0]['delivery_address_id'];
                        } else {

                            if ($val->getAddress_master_id() == "0") {

                                //echo "test";

                                continue;
                            } else {

                                $address_id = $val->getAddress_master_id();
                            }
                        }

                        $address_master_id = "";

                        $connection = $em->getConnection();

                        $statement = $connection->prepare("select `city_id` from address_master where `address_master_id`='" . $address_id . "'");

                        $statement->execute();

                        $address_master_id = $statement->fetchAll();

                        if (isset($address_master_id) && !empty($address_master_id)) {

                            $get_city_id = $address_master_id[0]['city_id'];

                            //if($address_master_id[0]['city_id'])
                        }

                        if ($get_city_id == $city_id) {

                            $mono = $this->keyDecryptionAction($val->getUser_mobile());
                            if ($mono != '' && $mono != 0 && $mono != NULL) {
                                $html .= '<div class="col-md-3"><input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getUser_master_id() . '"> ' . $mono . '&emsp;</div>';
                            }
                        }
                    } else {

                        $mono = $this->keyDecryptionAction($val->getUser_mobile());

                        $html .= '<input type="checkbox" name="user[]" class="checkBoxClass" id="mychk" value="' . $val->getUser_master_id() . '"> ' . $mono . '&emsp;';
                    }
                }

                $html .= '</div>

									</div>

								</div>';

                $html .= "<script>$('#checkAll').change(function () {

							$('input:checkbox').prop('checked', $(this).prop('checked'));

						});</script>";
            }
        }

        echo $html;
        return new Response;
    }

}
