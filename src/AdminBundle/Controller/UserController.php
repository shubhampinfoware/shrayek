<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Areamaster;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Governoratemaster;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Restaurantmaster;

/**
 * @Route("/admin")
 */
class UserController extends BaseController {

    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

	/**
     * @Route("/users")
     * @Template()
     */
    public function usersAction() {
        // load user list from dataTables function - getUserListAction
        return array();
    }
	
	/**
     * @Route("/searchuser")
     * @Template()
     */
    public function searchuserAction(Request $request){
        $postData = $request->request->all();
		
		$html = '';
		$success = false;
		if(array_key_exists('keyword', $postData)){
			$keyword = $postData['keyword'];
			//if($keyword != ''){
				$sql = "select * from user_master where user_firstname like '%{$keyword}%' or user_lastname like '%{$keyword}%'";
				$user_list = $this->firequery($sql);
				
				if(!empty($user_list)){
					
					foreach($user_list as $_user){
						$user_fullname = '';
						if($_user['user_firstname'] == '' and $_user['user_lastname'] == ''){
							$user_fullname = $_user['username'];
						} else {
							$user_fullname = $_user['user_firstname'].' '.$_user['user_lastname'];
						}
						
						$html .= "<div class='col-xs-3'>";
							$html .= "<input type='checkbox' name='user[]' class='checkBoxClass' 	value='{{user_list.user_master_id}}' >".$user_fullname;
						$html .= "</div>";
					}
				}
				
				$success = true;
			//}
		}
		
		$data = array(
			'success' => $success,
			'html' => $html
		);
		
		echo json_encode($data);exit;
    }

      /**
       * @Route("/get-user-list")
       * @Template()
       */
      public function getUserListAction() {
          ini_set('xdebug.var_display_max_depth', 200);
          ini_set('xdebug.var_display_max_children', 256);
          ini_set('xdebug.var_display_max_data', 1024);

          $order_by_query = "";

//          print_r($_REQUEST['order'][0]['column']);exit;
          if(isset($_REQUEST['order'][0]['column']) && isset($_REQUEST['order'][0]['column'])){
            $col_no = $_REQUEST['order'][0]['column'];
            $col_dir = $_REQUEST['order'][0]['dir'];
            
            #by user_id sorting
            if($col_no == 0){
                $order_by_query = " order by user_master_id $col_dir ";
            }
            #by date of birth sorting.
            if($col_no == 7){
//                $order_by_query = " order by date_of_birth $col_dir ";
                $order_by_query = " order by (case when date_format(date_of_birth, '%m-%d') >= date_format(now(), '%m-%d') then 0 else 1 end), date_format(date_of_birth, '%m-%d') $col_dir ";
            }

//            exit($order_by_query);

          }

          $search_value = '';
          if (array_key_exists('search', $_REQUEST)) {
              $search_value = $_REQUEST['search']['value'];
          }

          $limit_sql = "LIMIT " . $_REQUEST['start'] . "," . $_REQUEST['length'];
          $em = $this->getDoctrine()->getManager();

          if (isset($search_value) && $search_value != '') {
              $query = "select * from user_master WHERE user_role_id = 3 AND is_deleted = 0 AND (email like '%{$search_value}%' or username like '%{$search_value}%' or user_firstname like '%{$search_value}%' or user_lastname like '%{$search_value}%' or user_mobile like '%{$search_value}%' or user_bio like '%{$search_value}%' ) order by (case when date_format(date_of_birth, '%m-%d') >= date_format(now(), '%m-%d') then 0 else 1 end), date_format(date_of_birth, '%m-%d')";
          } else {
              $query = "select * from user_master WHERE user_role_id = 3 AND is_deleted = 0 order by (case when date_format(date_of_birth, '%m-%d') >= date_format(now(), '%m-%d') then 0 else 1 end), date_format(date_of_birth, '%m-%d')";
          }

          $em = $this->getDoctrine()->getManager();
          $con = $em->getConnection();
          $stmt1 = $con->prepare($query);
          $stmt1->execute();
          $user_count = $stmt1->fetchAll();

          if (isset($search_value) && $search_value != '') {
              if($order_by_query != ''){
                $sql_user_data = "select * from user_master WHERE user_role_id = 3 AND is_deleted = 0 AND (email like '%{$search_value}%' or username like '%{$search_value}%' or user_firstname like '%{$search_value}%' or user_lastname like '%{$search_value}%' or user_mobile like '%{$search_value}%' or user_bio like '%{$search_value}%' ) {$order_by_query} {$limit_sql}";
              }else{
                $sql_user_data = "select * from user_master WHERE user_role_id = 3 AND is_deleted = 0 AND (email like '%{$search_value}%' or username like '%{$search_value}%' or user_firstname like '%{$search_value}%' or user_lastname like '%{$search_value}%' or user_mobile like '%{$search_value}%' or user_bio like '%{$search_value}%' ) order by (case when date_format(date_of_birth, '%m-%d') >= date_format(now(), '%m-%d') then 0 else 1 end), date_format(date_of_birth, '%m-%d') {$limit_sql}";
              }
          } else {
            if($order_by_query != ''){
                $sql_user_data = "select * from user_master WHERE user_role_id = 3 AND is_deleted = 0 {$order_by_query} {$limit_sql}";
            }else{
                $sql_user_data = "select * from user_master WHERE user_role_id = 3 AND is_deleted = 0 order by (case when date_format(date_of_birth, '%m-%d') >= date_format(now(), '%m-%d') then 0 else 1 end), date_format(date_of_birth, '%m-%d') {$limit_sql}";
            }
          }

          $em = $this->getDoctrine()->getManager();
          $con = $em->getConnection();
          $stmt = $con->prepare($sql_user_data);
          $stmt->execute();
          $userlist = $stmt->fetchAll();

          $user_list = array();
          if(!empty($userlist)){
              foreach($userlist as $_user){
				  
				  $refered_by = $_user['refered_by'];
				  if($refered_by != ''){
					$_usersql = "SELECT * from user_master where user_master_id = {$refered_by}";
					$referUser = $this->firequery($_usersql);
					
				  }
				  
                  $user = array();

                  if($_user['status'] == 'active'){
                    $checked = 'checked';
                  } else {
                    $checked = '';
                  }
                  $status = "<input data-on='active' class='status' data-off='inactive' type='checkbox' onchange= 'change_status({$_user['user_master_id']}, this);' data-toggle='toggle' data-size='mini' data-onstyle='success' {$checked} />";

                  //$user[] = $_user['user_master_id'];
                  $user[] = "<img class='user-img' src='{$this->getMediaUrl($_user['user_image'])}'>";
                  $user[] = "<span id='remove_id_{$_user['user_master_id']}'>".$_user['user_firstname'].' '.$_user['user_lastname']."</span>";
                  $user[] = $_user['user_bio'];
                  $user[] = $_user['email'];
                  /*$user[] = $_user['username'];*/
                  $user[] = $_user['user_mobile'];
                  $user[] = $_user['user_gender'];
                  $user[] = $_user['date_of_birth'];
                  $user[] = $status;
//                  $user[] = $_user['last_login'];
                  $bookmart_url = $this->generateUrl('admin_user_bookmark_rest');
                  $user[] = "<a href='{$bookmart_url}/{$_user['user_master_id']}' class='btn btn-info btn-xs' data-toggle='tooltip' title='View Bookmark Restaraunts'><i class='fa fa-fw fa-eye'></i></a>";
                  
				  $live_path = $this->container->getParameter('live_path');
				  $delete_url = $this->generateUrl('admin_user_removeuser').'/'.$_user['user_master_id'];
				  $delete_str = 'deleterow("' . $delete_url . '")';
				  
				  $delete_button = "<a href='{$delete_url}' class='deletecategory btn btn-danger btn-xs' data-toggle='tooltip' title='Remove User' onclick='return " . $delete_str . "'><i class='fa fa-trash'></i></a>";
				  
				  $delete_button .= "<label class='container1'>
						<input type='checkbox'>
						<span class='checkmark' onclick='del_response({$_user['user_master_id']});'></span>
					</label>";
				  
				  $user[] = $delete_button;
                  $user[] = "<script>function deleterow(href_url){var cnfm = confirm('Are you sure, you want to delete?');if(cnfm){var cnfm1 = confirm('Confirmation: Are you sure, you want to delete?'); if(cnfm1){ location.href=href_url; } return false; }}</script>";
				  $user[] = "<script>$('.status').bootstrapToggle()</script>";

                  $user_list[] = $user;
              }
          }

          $response = array(
              'draw' => $_REQUEST['draw'],
              'recordsTotal' => count($user_count),
              'recordsFiltered' => count($user_count),
              'data' => $user_list
          );

          echo json_encode($response);exit;
      }

	/**
	* @Route("/removeuser/{id}", defaults={"id"=""})
	*/
	public function removeuserAction($id, Request $request){
		if($id != ''){
			$entity = $this->getDoctrine()->getManager();
			$user = $entity->getRepository('AdminBundle:Usermaster')->findOneBy([
				'user_master_id' => $id,
				'is_deleted' => 0
			]);
			
			if(!empty($user)){
				$user->setIs_deleted(1);
				$entity->flush();
				$this->get('session')->getFlashBag()->set('success_msg', 'User Removed Successfully');
			} else {
				$this->get('session')->getFlashBag()->set('error_msg', 'User not found');
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'User not found');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
	* @Route("/deletebulkusers")
	*/
	public function deletebulkusersAction(Request $request){
		$user_ids = '';
		$postData = $request->request->all();
		if(!array_key_exists('user_ids', $postData)){
			$data = array(
				'success' => false,
				'message' => 'Parameter not found'
			);
			
			echo json_encode($data);exit;
		}
		
		$user_ids_str = $postData['user_ids'];
		$user_ids = explode(',', $user_ids_str);
		
		if(!empty($user_ids)){
			$em = $this->getDoctrine()->getManager();
			
			foreach($user_ids as $user_id){
				$entity = $this->getDoctrine()->getManager();
				$user = $entity->getRepository('AdminBundle:Usermaster')->findOneBy([
					'user_master_id' => $user_id
				]);
				
				if(!empty($user)){
					$em->remove($user);
					$em->flush();
				}
			}
			
			$data = array(
				'success' => true,
				'message' => 'Removed successfully'
			);
		}
		
		echo json_encode($data);exit;
	}
	
	/**
	* @Route("/ajaxupdateuserstatus")
	*/
	public function ajaxupdateuserstatusAction(Request $request){

		if($request->get('user_id')){
			$em = $this->getDoctrine()->getManager();
			$user = $em->getRepository(Usermaster::class)->find($request->get('user_id'));

			$current_status = $user->getStatus();
			if($current_status == 'active'){
				$user->setStatus('inactive');
			} else {
				$user->setStatus('active');
			}

			$em->flush();
			echo 'true';exit;
		}
		echo 'false';exit;
	}

	public function getMediaUrl($media_id){
		$media_repository = $this->getDoctrine()->getRepository('AdminBundle:Medialibrarymaster');
		$media = $media_repository->find($media_id);

		$live_path = $this->container->getParameter('live_path');
		if(!empty($media)){
			$media_url = $live_path.$media->getMedia_location().'/'.$media->getMedia_name();
		} else {
			$media_url = $live_path.'/bundles/Resource/default.png';
		}

		return $media_url;
	}

    /**
     * @Route("/userlist")
     * @Template()
     */
    public function userlistAction() {

        $all_category = '';
        /* if($domain_id == "" || $domain_id == 0 ){ */

        $query = "SELECT * FROM user_master WHERE is_deleted = 0 AND user_type!='admin' AND user_type!='shop' GROUP BY user_master_id";

        $em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
        $em->execute();
        $all_category = $em->fetchAll();

        return array("users" => $all_category);
        //var_dump($all_category_details);exit;
    }

    /**
     * @Route("/Add-Update-User/{user_master_id}",defaults={"user_master_id":""})
     * @Template()
     */
    public function adduserAction($user_master_id) {



        $governoratemaster = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Governoratemaster')
                ->findBy(array('is_deleted' => 0, 'language_id' => 1));

        $areamaster = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Areamaster')
                ->findBy(array('is_deleted' => 0, 'language_id' => 1));



        $live_path = $this->getParameter('live_path');
        $usermaster = null;
        if (!empty($user_master_id)) {
            $usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Usermaster')
                    ->findBy(array('is_deleted' => 0, 'user_master_id' => $user_master_id));
        }


        return array("usermaster" => $usermaster, "user_master_id" => $user_master_id, "areamaster" => $areamaster, "governoratemaster" => $governoratemaster, "live_path" => $live_path);
    }

    /**
     * @Route("/adduserdb")
     * @Template()
     */
    public function adduserdbAction() {
        $session = new Session();

        if (isset($_REQUEST['save'])) {



            $firstname = $_REQUEST['firstname'];
            $lastname = $_REQUEST['lastname'];
            $email = $_REQUEST['email'];
            $password = md5($_REQUEST['password']);
            $mobile = $_REQUEST['mobile'];
            $governorate = $_REQUEST['governorate'];
            $area = $_REQUEST['area'];
            $user_type = $_REQUEST['user_type'];
            $registration_type = $_REQUEST['registration_type'];
            $status = $_REQUEST['status'];
            $user_master_id = '';

            if (isset($_REQUEST['user_master_id']) && !empty($_REQUEST['user_master_id'])) {
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SUU34", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */

                $user_master_id = $_REQUEST['user_master_id'];
                $get_plan = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('AdminBundle:Usermaster')
                        ->findOneBy(array(
                    'user_master_id' => $user_master_id,
                    'is_deleted' => 0)
                );


                if (count($get_plan) > 0) {
                    $em = $this->getDoctrine()->getManager();
                    $update = $em->getRepository('AdminBundle:Usermaster')
                            ->find($get_plan->getUser_master_id());
                    $update->setFirstname($firstname);
                    $update->setLastname($lastname);
                    $update->setEmail($email);
                    $update->setPassword($password);
                    $update->setMobile($mobile);
                    $update->setGovernorate($governorate);
                    $update->setArea($area);
                    $update->setUser_type($user_type);
                    $update->setRegistration_type($registration_type);
                    $update->setStatus($status);




                    $em->flush();


                    //-------------------------------- Common Changes -----------------------------
                    //-------------------------------- Common Changes -----------------------------
                    $this->get('session')->getFlashBag()->set('success_msg', 'Customer Updated Successfully!');
                    return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))) . '/' . $user_master_id);
                } else {

                    $usermaster = new Usermaster();
                    $usermaster->setFirstname($firstname);
                    $usermaster->setLastname($lastname);
                    $usermaster->setEmail($email);
                    $usermaster->setPassword($password);
                    $usermaster->setMobile($mobile);
                    $usermaster->setGovernorate($governorate);
                    $usermaster->setArea($area);
                    $usermaster->setUser_type($user_type);
                    $usermaster->setRegistration_type($registration_type);
                    $usermaster->setStatus($status);



                    //$usermaster->setUser_master_id($user_master_id);
                    $usermaster->setIs_deleted(0);


                    $em = $this->getDoctrine()->getManager();
                    $em->persist($usermaster);
                    $em->flush();

                    if (isset($user_master_id) && !empty($user_master_id)) {
                        $this->get('session')->getFlashBag()
                                ->set('success_msg', 'Customer Inserted Successfully!');
                        return $this->redirect($this
                                                ->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))) . '/' . $user_master_id);
                    } else {
                        $this->get('session')->getFlashBag()->set('error_msg', 'Customer Insertion Failed!');
                        return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))));
                    }
                }
            } else {
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SAU33", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
                $usermaster = new Usermaster();

                $usermaster = new Usermaster();
                $usermaster->setFirstname($firstname);
                $usermaster->setLastname($lastname);
                $usermaster->setEmail($email);
                $usermaster->setPassword($password);
                $usermaster->setMobile($mobile);
                $usermaster->setGovernorate($governorate);
                $usermaster->setArea($area);
                $usermaster->setUser_type($user_type);
                $usermaster->setRegistration_type($registration_type);
                $usermaster->setStatus($status);

                $usermaster->setIs_deleted(0);


                $em = $this->getDoctrine()->getManager();
                $em->persist($usermaster);
                $em->flush();


                $user_master_id = $usermaster->getUser_master_id();

                if (isset($user_master_id) && !empty($user_master_id)) {
                    $em = $this->getDoctrine()->getManager();
                    $update = $em->getRepository('AdminBundle:Usermaster')->find($user_master_id);

                    $em->flush();
                    $this->get('session')->getFlashBag()->set('success_msg', 'Customer Inserted Successfully!');
                    return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))) . '/' . $user_master_id);
                } else {
                    $this->get('session')->getFlashBag()->set('error_msg', 'Customer Insertion Failed!');
                    return $this->redirect($this->generateUrl('admin_user_adduser', array("domain" => $this->get('session')->get('domain'))));
                }
            }
        }
    }

    /**

     * @Route("/statususer")

     * @Template()

     */
    public function statususerAction() {
        if (isset($_REQUEST['id']) && !empty($_REQUEST['id']) && isset($_REQUEST['status']) && !empty($_REQUEST['status'])) {

            $request = $this->getRequest();
            $session = $request->getSession();
            $em = $this->getDoctrine()->getManager();
            $status = $_REQUEST['status'];
            $id = $_REQUEST['id'];
            if ($_REQUEST['status'] == "true") {
                $status = "active";
            } else {
                $status = "inactive";
            }

            $list = $em->getRepository('AdminBundle:Usermaster')
                    ->findBy(
                    array(
                        'user_master_id' => $id,
                        'is_deleted' => 0
                    )
                    );



            if (!empty($list)) {

                foreach ($list as $key => $val) {

                    $user = $em->getRepository('AdminBundle:Usermaster')
                            ->findOneBy(
                            array(
                                'user_master_id' => $val->getUser_master_id(),
                                'is_deleted' => 0
                            )
                            );

                    $user->setStatus($status);

                    $em->persist($user);

                    $em->flush();
                }
            }
        }

        return new Response();
    }

    /**
     * @Route("/checkemailexist")
     */
    public function checkemailexistAction() {
        $session = new Session();

        if (!empty($_POST['email_id'])) {
            $Usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository("AdminBundle:Usermaster")
                    ->findBy(array(
                "email" => $_POST['email_id'],
                'user_type' => 'user',
                //"domain_id" =>  $session->get("domain_id"),
                "is_deleted" => 0,
                    )
            );
            if (!empty($Usermaster)) {
                return new Response(1);
            } else {
                return new Response(0);
            }
        }
    }

    /**
     * @Route("/checkphoneexist")
     */
    public function checkphoneexistAction() {
        $session = new Session();

        if (!empty($_POST['phone_number'])) {
            $Usermaster = $this->getDoctrine()
                    ->getManager()
                    ->getRepository("AdminBundle:Usermaster")
                    ->findBy(array(
                "mobile_number" => $_POST['phone_number'],
                'user_type' => 'user',
                //"domain_id" =>  $session->get("domain_id"),
                "is_deleted" => 0,
                    )
            );
            if (!empty($Usermaster)) {
                return new Response(1);
            } else {
                return new Response(0);
            }
        }
    }

	/**
     * @Route("/bookMarkRestaraunts/{user_id}",defaults={"user_id"=""},name="admin_user_bookmark_rest")
	 * @Template()
     */
    public function bookmarkRestarauntsViewAction($user_id) {

		$rest_final_data = array();
		$em = $this->getDoctrine()->getManager();
		$language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($language)){
			$language = array();
		}

		$get_bookmark_rest_sql = "select rest.restaurant_name,rest.main_restaurant_id from bookmark_master book_mark join restaurant_master rest on book_mark.shop_id=rest.main_restaurant_id
								  where book_mark.is_deleted=0 and rest.is_deleted=0 and book_mark.user_id='".$user_id."' and book_mark.type='add'";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($get_bookmark_rest_sql);
		$stmt->execute();
		$bookmarkRestaraunts = $stmt->fetchAll();

		$main_restaurant_id = '';
		foreach($bookmarkRestaraunts as $Restaurantmaster_data){
			$restaurant_data_lang_wise = array();
			if($main_restaurant_id !== $Restaurantmaster_data['main_restaurant_id']){
				foreach($language_list as $language){
					$rest_name = '';
					$main_rest_id = '';
					$timings = '';
					$status = '';
					$em = $this->getDoctrine()->getManager();
					$lang_type = $em->getRepository(Restaurantmaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>$language->getLanguage_master_id(),'main_restaurant_id'=>$Restaurantmaster_data['main_restaurant_id']));
					if($lang_type){
						$rest_name = $lang_type->getRestaurant_name();
						$main_rest_id = $lang_type->getMain_restaurant_id();
					}

					$restaurant_data_lang_wise [] = array('language_id'=>$language->getLanguage_master_id(),
														'rest_name'=>$rest_name,
														'main_rest_id'=>$main_rest_id,
														);
				}
			}
			if(!empty($restaurant_data_lang_wise)){
				$main_restaurant_id = $Restaurantmaster_data['main_restaurant_id'];
				$rest_final_data [] = $restaurant_data_lang_wise;
			}
		}

		return array('bookmarkRestaraunts'=>$rest_final_data,'language_list'=>$language_list);
    }
}
