<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request; 
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Foodtypemaster;
use AdminBundle\Entity\Foodtypecategoryrelation;
use AdminBundle\Entity\Categorymaster;

/**
* @Route("/admin")
*/
class FoodtypeController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/foodType",name="food_type")
     * @Template()
     */
    public function indexAction()
    {
		$right_codes = $this->userrightsAction();
		$food_type_final_data = array();	
		$em = $this->getDoctrine()->getManager();
		$language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($language)){
			$language = array();
		}
		
		$em = $this->getDoctrine()->getManager();
		$Foodtypemaster = $em->getRepository(Foodtypemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($Foodtypemaster)){
			$Foodtypemaster = array();
		}
		$main_food_id = '';
		foreach($Foodtypemaster as $Foodtypemaster_data){
			$food_type_data_lang_wise = array();
			if($main_food_id !== $Foodtypemaster_data->getMain_food_type_id()){
			//	var_dump($Foodtypemaster_data->getMain_food_type_id());
				foreach($language_list as $language){
					$food_type_name = '';
                                        $main_food_type='';
					$em = $this->getDoctrine()->getManager();
					$lang_type = $em->getRepository(Foodtypemaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>$language->getLanguage_master_id(),'main_food_type_id'=>$Foodtypemaster_data->getMain_food_type_id()));
					
                                        if($lang_type){
						$food_type_name = $lang_type->getFood_type_name();
						$main_food_type_id = $Foodtypemaster_data->getMain_food_type_id();
                                        }
						$main_food_type = $Foodtypemaster_data->getMain_food_type_id();
					
					$food_type_data_lang_wise [] = array('language_id'=>$language->getLanguage_master_id(),
														'food_type'=>$food_type_name,
														'main_food_type_id'=>$main_food_type_id,
														'main_food_type'=>$main_food_type);	
				}
			}
                       
			if(!empty($food_type_data_lang_wise)){
				$main_food_id = $Foodtypemaster_data->getMain_food_type_id();
			//	var_dump($main_food_id);
				$food_type_final_data [] = $food_type_data_lang_wise;
			}
		}
	
		/* echo '<pre>';
		print_r($food_type_final_data);
		exit; */ 
		return (array('right_codes'=>$right_codes,'languages'=>$language_list,'food_lang_wise'=>$food_type_final_data));	
	//	return(array('languages'=>$language_list));
	}
	
	/**
     * @Route("/foodtype/category-list/{main_food_type_id}",defaults={"main_food_type_id"=0})
     * @Template()
     */
    public function categoryListAction($main_food_type_id)
    {
		$foodtype_name = '';
		$all_category = array();
		$em = $this->getDoctrine()->getManager();
		$language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(isset($main_food_type_id)){
			
			$query = "SELECT type.food_type_master_id, type.main_food_type_id, cat.category_name, cat.main_category_id, cat.category_status, cat.category_master_id, CONCAT(media.media_location, '/', media.media_name) as media_url from food_type_category_relation rel, food_type_master type, category_master cat, media_library_master media where rel.main_food_type_id = type.main_food_type_id and media.media_library_master_id = cat.category_image_id and rel.main_category_id  = cat.main_category_id and cat.language_id = 1 and type.main_food_type_id = {$main_food_type_id} group by rel.main_category_id";
			
            $category_list = $this->firequery($query);
			
			if(!empty($category_list)){
				foreach($category_list as $_category){
					
					$lang_wise_cat = array();
					foreach($language_list as $_language){
						$cat = $this->getDoctrine()->getRepository('AdminBundle:Categorymaster')->findOneBy([
							'main_category_id' => $_category['main_category_id'],
							'language_id' => $_language->getLanguage_master_id(),
							'is_deleted' => 0
						]);
						
						if(!empty($cat)){
							$lang_wise_cat[] = array(
								'language_id' => $_language->getLanguage_master_id(),
								'category_name' => $cat->getCategory_name()
							);
						}
					}
					
					$_category['category_status'] = ucfirst($_category['category_status']);
					$_category['lang_wise_cat'] = $lang_wise_cat;
					$all_category[] = $_category;
				}
			}
		}
		
		/* echo '<pre>';
		print_r($all_category);
		exit; */
		
		return array(
			'foodtype_name' => $foodtype_name,
			'languages' => $language_list,
			'category_list' => $all_category
		);
	}
	
	/**
     * @Route("/addFoodType/{main_food_type_id}",defaults={"main_food_type_id"=0},name="add_food_type")
     * @Template()
     */
    public function addFoodTypeAction($main_food_type_id)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SAFT4", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$em = $this->getDoctrine()->getManager();
		$language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($language)){
			$language = array();
		}
		$food_type_sql = "select food.* from food_type_master food where food.main_food_type_id='".$main_food_type_id."'"; 
	
		$em = $this->getDoctrine()->getManager();
		$conn = $em->getConnection();
		$stmt = $conn->prepare($food_type_sql);
		$stmt->execute();
		$food_type_details = $stmt->fetchAll();
		
		$food_category_sql = "select cat.* from food_type_category_relation cat where cat.main_food_type_id='".$main_food_type_id."' and is_deleted=0";
		$em = $this->getDoctrine()->getManager();
		$conn = $em->getConnection();
		$stmt = $conn->prepare($food_category_sql);
		$stmt->execute();
		$food_category_details = $stmt->fetchAll();
		
		$food_type_category = $food_category_details;

		$em = $this->getDoctrine()->getManager();
		$categories = $em->getRepository(Categorymaster :: class)->findBy(array('is_deleted'=>0,'language_id'=>'1'));
		if(empty($categories)){
			$categories = array();
		}
               
                if(!empty($categories)){
                    foreach($categories as $key=>$val){
                        $categories1 = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>'2','main_category_id'=>$val->getMain_category_id()));
                       $ar = !empty($categories1)?$categories1->getCategory_name():'';
                         $cate_list[]=array('main_category_id'=>$val->getMain_category_id(),'category_name'=>!empty($ar)?$val->getCategory_name().' / '.$ar:$val->getCategory_name());
                    }
                }
                
		return (array('food_type_details'=>$food_type_details,'languages'=>$language,'categories'=>$cate_list,'food_category'=>$food_type_category));	
	}
		
	/**
     * @Route("/saveFoodType",name="save_food_type")
     * @Template()
     */
	public function saveFoodType(Request $req)
    {
	//	var_dump($_REQUEST);exit;
	
		if($req->request->get('main_food_type_id') === ''){
			$flag = 0;
		}else{
			$flag=1;
		}	
		$em = $this->getDoctrine()->getManager();
		$foodType = $em->getRepository(Foodtypemaster :: class)->findOneBy(array('is_deleted'=>0,'main_food_type_id'=>$req->request->get('main_food_type_id'),'language_id'=>$req->request->get('language_id')));	
	
		if(empty($foodType)){	
		
			/* check for access */
			$right_codes = $this->userrightsAction();
			$rights_search = in_array("SAFT4", $right_codes);
			if ($rights_search != 1) {
				$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
				return $this->redirect($this->generateUrl('admin_dashboard_index'));
			}
			/* end: check for access */
		
		//	$date = date('Y-m-d');
			$foodTypeNew = new Foodtypemaster();
			$foodTypeNew->setFood_type_name($req->request->get('food_type'));
			$foodTypeNew->setLanguage_id($req->request->get('language_id'));
			$foodTypeNew->setMain_food_type_id(0);
			$foodTypeNew->setFood_type_image_id(0);
			$em = $this->getDoctrine()->getManager();
			$em->persist($foodTypeNew);
			$em->flush();
			if($flag === 0){
				$food_type_id_main = $foodTypeNew->getFood_type_master_id();
				$flag = 1;
			}else{
				if($req->request->get('main_food_type_id') !== ''){	
					$food_type_id_main = $req->request->get('main_food_type_id');
				}	
			}
			$foodTypeNew->setMain_food_type_id($food_type_id_main);
			$em->flush();
			
			// categpry table
			$main_food_type_id = $foodTypeNew->getMain_food_type_id(); 
			
			$em = $this->getDoctrine()->getManager();
			$foodTypeCategory = $em->getRepository(Foodtypecategoryrelation :: class)->findOneBy(array('is_deleted'=>0,'main_food_type_id'=>$req->request->get('main_food_type_id')));
			if(empty($foodTypeCategory)){
                            if(!empty($req->request->get('category'))){
				foreach($req->request->get('category') as $category){
					$em = $this->getDoctrine()->getManager();
					$food_category = new Foodtypecategoryrelation();
					$food_category->setMain_food_type_id($main_food_type_id);	
					$food_category->setMain_category_id($category);
					$em->persist($food_category);
					$em->flush();
				}
                            }	
			}
                        $foodTypeNew1 = new Foodtypemaster();
			$foodTypeNew1->setFood_type_name($req->request->get('food_type_ar'));
			$foodTypeNew1->setLanguage_id(2);
			$foodTypeNew1->setMain_food_type_id($food_type_id_main);
			$foodTypeNew1->setFood_type_image_id(0);
			$em = $this->getDoctrine()->getManager();
			$em->persist($foodTypeNew1);
			$em->flush();
			
			// categpry table
			$main_food_type_id = $foodTypeNew1->getMain_food_type_id(); 
			
			$em = $this->getDoctrine()->getManager();
			$foodTypeCategory = $em->getRepository(Foodtypecategoryrelation :: class)->findOneBy(array('is_deleted'=>0,'main_food_type_id'=>$req->request->get('main_food_type_id')));
			if(empty($foodTypeCategory)){
                            if(!empty($req->request->get('category'))){
				foreach($req->request->get('category') as $category){
					$em = $this->getDoctrine()->getManager();
					$food_category = new Foodtypecategoryrelation();
					$food_category->setMain_food_type_id($main_food_type_id);	
					$food_category->setMain_category_id($category);
					$em->persist($food_category);
					$em->flush();
				}
                            }	
			}
			$this->get('session')->getFlashBag()->set('success_msg', 'Food Type saved successfully');		
			return $this->redirectToRoute('food_type');
			
		}else{
			
			/* check for access */
			$right_codes = $this->userrightsAction();
			$rights_search = in_array("SUFT5", $right_codes);
			if ($rights_search != 1) {
				$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
				return $this->redirect($this->generateUrl('admin_dashboard_index'));
			}
			/* end: check for access */
			
		//	var_dump($_REQUEST);exit;	
			$date = date('Y-m-d');
			$foodType->setFood_type_name($req->request->get('food_type'));
			$em = $this->getDoctrine()->getManager();
			$em->flush();
			$foodType1 = $em->getRepository(Foodtypemaster :: class)->findOneBy(array('is_deleted'=>0,'main_food_type_id'=>$req->request->get('main_food_type_id'),'language_id'=>2));	
                        if(!empty($foodType1)){
                            $date = date('Y-m-d');
                            $foodType1->setFood_type_name($req->request->get('food_type_ar'));
                            $em = $this->getDoctrine()->getManager();
                            $em->flush();
                        }else{
                            $foodTypeNew1 = new Foodtypemaster();
                            $foodTypeNew1->setFood_type_name($req->request->get('food_type_ar'));
                            $foodTypeNew1->setLanguage_id(2);
                            $foodTypeNew1->setMain_food_type_id($req->request->get('main_food_type_id'));
                            $foodTypeNew1->setFood_type_image_id(0);
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($foodTypeNew1);
                            $em->flush();

                            // categpry table
                            $main_food_type_id = $foodTypeNew1->getMain_food_type_id(); 

                            $em = $this->getDoctrine()->getManager();
                            $foodTypeCategory = $em->getRepository(Foodtypecategoryrelation :: class)->findOneBy(array('is_deleted'=>0,'main_food_type_id'=>$req->request->get('main_food_type_id')));
                            if(empty($foodTypeCategory)){
                                if(!empty($req->request->get('category'))){
                                    foreach($req->request->get('category') as $category){
                                            $em = $this->getDoctrine()->getManager();
                                            $food_category = new Foodtypecategoryrelation();
                                            $food_category->setMain_food_type_id($main_food_type_id);	
                                            $food_category->setMain_category_id($category);
                                            $em->persist($food_category);
                                            $em->flush();
                                    }
                                }	
                            } 
                        }
			$main_food_type_id = $foodType->getMain_food_type_id(); 
			
			$em = $this->getDoctrine()->getManager();
			$foodTypeCategory = $em->getRepository(Foodtypecategoryrelation :: class)->findBy(array('is_deleted'=>0,'main_food_type_id'=>$req->request->get('main_food_type_id')));
			if($foodTypeCategory){
				foreach($foodTypeCategory as $cat_food_type){
					
					$entity = $this->getDoctrine()->getManager();
					$restaurant_foodtype_relation = $entity->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy(
						array(
							'is_deleted' => 0,
							'main_foodtype_id' => $cat_food_type->getMain_food_type_id(),
							'main_caetgory_id' => $cat_food_type->getMain_category_id()
						)
					);
					
					if(!empty($restaurant_foodtype_relation)){
						foreach($restaurant_foodtype_relation as $_relation){
							$_relation->setIs_deleted(1);
							$entity->flush();
						}
					}
					
					$cat_food_type->setIs_deleted(1);
					$em->flush();	
				}
			} 
			if(!empty($req->request->get('category'))){
				foreach($req->request->get('category') as $category){
						$em = $this->getDoctrine()->getManager();
						$food_category = new Foodtypecategoryrelation();
						$food_category->setMain_food_type_id($main_food_type_id);	
						$food_category->setMain_category_id($category);
						$em->persist($food_category);
						$em->flush();
				}
			}
			$this->get('session')->getFlashBag()->set('success_msg', 'Food Type updated successfully');
			return $this->redirectToRoute('food_type');
		}
		
	} 

	/**
     * @Route("/deleteFoodType/{main_food_type_id}",defaults={"main_food_type_id"=0},name="delete_food_type")
     * @Template()
     */
    public function deleteFoodTypeAction($main_food_type_id)
    {
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDFT6", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		
		$em = $this->getDoctrine()->getManager();
		$food_type_details = $em->getRepository(Foodtypemaster :: class)->findBy(array('is_deleted'=>0,'main_food_type_id'=>$main_food_type_id));
		foreach($food_type_details as $food_type){
			$food_type->setIs_deleted(1);
			$em->flush();		
		}
		
		if(!empty($food_type_details)){
			// remove foodtype relation
			$food_type_relation = $em->getRepository('AdminBundle:Foodtypecategoryrelation')->findBy(
				array(
					'is_deleted'=>0,
					'main_food_type_id'=>$main_food_type_id
				)
			);
			
			if(!empty($food_type_relation)){
				foreach($food_type_relation as $_rel){
					$em->remove($_rel);
					$em->flush();
				}
			}
			
			// remove res-foodtype relation
			$res_foodtype_relation = $em->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy(
				array(
					'is_deleted'=>0,
					'main_foodtype_id'=>$main_food_type_id
				)
			);
			
			if(!empty($res_foodtype_relation)){
				foreach($res_foodtype_relation as $_rel){
					$em->remove($_rel);
					$em->flush();
				}
			}
		}

		$this->get('session')->getFlashBag()->set('success_msg', 'Food Type deleted successfully');
		//return $this->redirectToRoute('food_type');
		return $this->redirect($this->generateUrl('food_type'));
	}	
	

    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
}
