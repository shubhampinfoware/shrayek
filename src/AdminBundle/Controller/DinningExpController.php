<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Diningexperience;
use AdminBundle\Entity\Contactusfeedback;
use AdminBundle\Entity\Diningexperiencegallery;
use AdminBundle\Entity\Medialibrarymaster;

/**
* @Route("/admin")
*/
class DinningExpController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/DinningExperience",name="dinning_experience")
     * @Template()
     */
    public function indexAction()
    {
		$right_codes = $this->userrightsAction();
		$dinnig_expr = array();	
		//$din_exr_sql = "select din_expr.*,rest.restaurant_name from dining_experience din_expr join restaurant_master rest on rest.restaurant_master_id = din_expr.restaurant_id where din_expr.is_deleted=0 and rest.is_deleted=0";	
		$din_exr_sql = "SELECT * FROM `dining_experience` where is_deleted = 0 order by dining_experience_id DESC";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($din_exr_sql);
		$stmt->execute();
		$dinnig_expr = $stmt->fetchAll();
        
		/* echo '<pre>'; 
		print_r($dinnig_expr);
		exit; */
		
		return(array('dinning_expr_data'=>$dinnig_expr, 'right_codes' => $right_codes));
	} 
	
	/**
     * @Route("/DinningExperienceView/{din_expr_id}",name="dinning_experience_view",defaults={"din_expr_id"="0"})
     * @Template()
     */
    public function dinningExprViewAction($din_expr_id)
    {
		$dinnig_expr = array();	
		$dinning_view_data = array();
		//$din_exr_sql = "select din_expr.*,rest.restaurant_name from dining_experience din_expr join restaurant_master rest on rest.restaurant_master_id = din_expr.restaurant_id where din_expr.is_deleted=0 and rest.is_deleted=0 and 	din_expr.dining_experience_id='".$din_expr_id."'";	
		$din_exr_sql = "select * from dining_experience where dining_experience_id = {$din_expr_id} and is_deleted = 0";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($din_exr_sql);
		$stmt->execute();
		$dinnig_expr = $stmt->fetchAll();
		
		$din_exr_gallery = array();
		if($dinnig_expr){
			$din_exr_gallery_sql = "select media.* from media_library_master media join dining_experience_gallery d_e_g on d_e_g.image_id = media.media_library_master_id where d_e_g.is_deleted=0 and d_e_g.dining_experience_id='".$din_expr_id."'";	
			$em = $this->getDoctrine()->getManager();
			$con = $em->getConnection();
			$stmt = $con->prepare($din_exr_gallery_sql);
			$stmt->execute();
			$din_exr_gallery = $stmt->fetchAll();			
		}

		$dinning_view_data = array(
			'din_expr_data'=>$dinnig_expr,
			'din_gallery'=>$din_exr_gallery
		);
		return(array('dinning_view_data'=>$dinning_view_data));
	} 
	
	/**
     * @Route("/dinning-exprience/edit/{din_expr_id}", defaults={"din_expr_id"="0"})
     * @Template()
     */
    public function editAction($din_expr_id)
    {	
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SEDE24", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
	
		$dinnig_expr = array();	
		//$din_exr_sql = "select din_expr.*,rest.restaurant_name from dining_experience din_expr join restaurant_master rest on rest.restaurant_master_id = din_expr.restaurant_id where din_expr.is_deleted=0 and dining_experience_id = {$din_expr_id} and rest.is_deleted=0";	
		$din_exr_sql = "SELECT * FROM `dining_experience` where dining_experience_id = {$din_expr_id} and is_deleted = 0";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($din_exr_sql);
		$stmt->execute();
		$dinnig_expr = $stmt->fetchAll();
        
		if(!empty($dinnig_expr)){
			$dinnig_expr = $dinnig_expr[0];
		}
		
		## get restaurant list
		$sql_rest_data = "select * from restaurant_master where is_deleted=0 and language_id=1 and (status='active' or status='open_soon')";
        $em = $this->getDoctrine()->getManager();
        $con = $em->getConnection();
        $stmt = $con->prepare($sql_rest_data);
        $stmt->execute();
        $Restaurantmaster = $stmt->fetchAll();
		
		## get media list
		$media_query = "select exp.dining_experience_gallery_id, media.* from dining_experience_gallery exp, media_library_master media where exp.image_id = media.media_library_master_id and dining_experience_id = {$din_expr_id} and exp.is_deleted = 0";
		$media_library_master = $this->firequery($media_query);
		
		return array(
			'din_expr_id' => $din_expr_id,
			'dinning_expr_data' => $dinnig_expr,
			'media_library_master' => $media_library_master,
			'restaurantmaster' => $Restaurantmaster
		);
	}
	
	/**
     * @Route("/dinning-exp/gallery/remove")
     * @Template()
     */
    public function removeExpGalleryAction(Request $request) {
		$media_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Medialibrarymaster')
                ->findOneBy(array('media_library_master_id' => $request->get('main_media_id'), 'is_deleted' => 0));

        $gallery_file = $this->getDoctrine()
                ->getManager()
                ->getRepository('AdminBundle:Diningexperiencegallery')
                ->findOneBy(array('dining_experience_gallery_id' => $request->get('gallery_id'), 'is_deleted' => 0));
				
		if (!empty($media_file) && !empty($gallery_file)) {
            $media_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $gallery_file->setIs_deleted(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $delete_img = $this->container->getParameter('root_dir') . '/' . $media_file->getMedia_location() . '/' . $media_file->getMedia_name();

            //unlink($delete_img);

            return new Response("true");
        } else {
            return new Response("false");
        }
	}
	
	/**
     * @Route("/dinning-experience/gallery/add")
     * @Template()
     */
    public function addExpGalleryAction(Request $request) {
	
		if ($request->request->all()) {

            $em = $this->getDoctrine()->getManager();

            $media_library_master = new Medialibrarymaster();
            $media_library_master->setMedia_type_id($request->get('media_type_id'));
            $media_library_master->setMedia_title($request->get('img_name'));
            $media_library_master->setMedia_location("/bundles/design/uploads/bulkupload");
            $media_library_master->setMedia_name($request->get('img_name'));
            $media_library_master->setCreated_on(date("Y-m-d H:i:s"));
            $media_library_master->setIs_Deleted(0);
            $em->persist($media_library_master);
            $em->flush();

            $exp_gallery = new Diningexperiencegallery();
			$exp_gallery->setDining_experience_id($request->get('din_expr_id'));
			$exp_gallery->setImage_id($media_library_master->getMedia_library_master_id());
			$exp_gallery->setIs_deleted(0);
			$em = $this->getDoctrine()->getManager();
			$em->persist($exp_gallery);
			$em->flush();
			
            $media_file = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AdminBundle:Medialibrarymaster')
                    ->findOneBy(array('media_library_master_id' => $media_library_master->getMedia_library_master_id(), 'is_deleted' => 0));

            if (!empty($media_file)) {
                $content = array(
                    "name" => $media_file->getMedia_name(),
                    "path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "thumbnail_path" => $this->container->getParameter('live_path') . $media_file->getMedia_location() . "/" . $media_file->getMedia_name(),
                    "media_library_master_id" => $media_library_master->getMedia_library_master_id(),
					"gallery_master_id" => $exp_gallery->getDining_experience_gallery_id()
                );
                return new Response(json_encode($content));
            }

        }
        return new Response(false);
	}
	
	/**
     * @Route("/dinning-exprience/editcommentdb")
     * @Template()
     */
    public function editcommentdbAction(Request $req)
    {
		if($req->request->all()){
			$din_expr_id = $req->get('din_expr_id');
			$request_data = $req->request->all();
			
			$em = $this->getDoctrine()->getManager();
			$dinning_exp = $em->getRepository('AdminBundle:Diningexperience')->findOneBy([
				'dining_experience_id' => $din_expr_id,
				'is_deleted' => 0
			]);
			
			if(!empty($dinning_exp)){
				$dinning_exp->setNick_name($req->get('nickname'));
				$dinning_exp->setRestaurant_id($req->get('restaurant'));
				$dinning_exp->setComments($req->get('comment'));
				$dinning_exp->setStatus($req->get('status'));
				$em->flush();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Dinning experience updated successfully');
			} else {
				$this->get('session')->getFlashBag()->set('error_msg', 'Dinning experience not found');
			}
		}
		
		$referer = $req->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
     * @Route("/DeleteDinningExperience/{din_expr_id}",name="delete_dinning_experience",defaults={"din_expr_id"="0"})
     * @Template()
     */
    public function deleteDinExprAction($din_expr_id)
    {
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDDE25", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$din_exr_del_sql = "update dining_experience set is_deleted=1 where dining_experience_id='".$din_expr_id."'";
		$em = $this->getDoctrine()->getManager();
		$con = $em->getConnection();
		$stmt = $con->prepare($din_exr_del_sql);
		$deleted = $stmt->execute();
		if($deleted){
			return $this->redirectToRoute('dinning_experience');
		}
	}
	
	/**
	* @Route("/dinning_exp/ajaxupdatestatus")
	*/
	public function ajaxupdatestatusAction(Request $request){
		
		if($request->get('exp_id')){
			$em = $this->getDoctrine()->getManager();
			$dinning_exp = $em->getRepository(Diningexperience::class)->find($request->get('exp_id'));
			
			$current_status = $dinning_exp->getStatus();
			if($current_status == 'active'){
				$dinning_exp->setStatus('inactive');
			} else {
				$dinning_exp->setStatus('active');
			}
			
			$em->flush();
			echo 'true';exit;
		}
		echo 'false';exit;
	}
	
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}

		/**
	* @Route("/deletebulkdinings")
	*/
	public function deletebulkdinsAction(Request $request){
		$user_ids = '';
		$postData = $request->request->all();
		if(!array_key_exists('user_ids', $postData)){
			$data = array(
				'success' => false,
				'message' => 'Parameter not found'
			);
			
			echo json_encode($data);exit;
		}
		
		$user_ids_str = $postData['user_ids'];
		$user_ids = explode(',', $user_ids_str);
		
		if(!empty($user_ids)){
			$em = $this->getDoctrine()->getManager();
			
			foreach($user_ids as $user_id){
				$entity = $this->getDoctrine()->getManager();
				$user = $entity->getRepository('AdminBundle:Diningexperience')->findOneBy([
					'dining_experience_id' => $user_id
				]);
				
				if(!empty($user)){
					$user->setIs_deleted(1);
					$em->flush();
				}
			}
			
			$data = array(
				'success' => true,
				'message' => 'Removed successfully'
			);
		}
		
		echo json_encode($data);exit;
	}

}
