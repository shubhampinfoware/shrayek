<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Contactusfeedback;

/**
* @Route("/admin")
*/
class FeedbackController extends BaseController
{
	public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
     * @Route("/member-messages",name="feedback")
     * @Template()
     */
    public function indexAction()
    {
		$em = $this->getDoctrine()->getManager();
		$feedback_details = $em->getRepository(Contactusfeedback :: class)->findBy(array('is_deleted'=>0));
		
		$all_feedbacks = array();
		if(!empty($feedback_details)){
			foreach($feedback_details as $_feedback){
				$short_message = $_feedback->getMessage();
				if(strlen($_feedback->getMessage()) > 200){
					$short_message = substr($_feedback->getMessage(), 0, 200).'...';
				}
				$_feedback->short_message = $short_message;
				
				$all_feedbacks[] = $_feedback;
			}
		}
		
		/* echo '<pre>';
		print_r($all_feedbacks);
		exit; */
		
		return (array('feedback_details'=>$all_feedbacks));	
	} 
	
	/**
     * @Route("/feedback/remove/{feedback_id}")
     * @Template()
     */
    public function removeFeedbackAction($feedback_id, Request $request)
    {
		if(isset($feedback_id)){
			$em = $this->getDoctrine()->getManager();
			$feedback = $em->getRepository('AdminBundle:Contactusfeedback')->findOneBy(['contact_us_feedback_id' => $feedback_id, 'is_deleted' => 0]);
			
			if(!empty($feedback)){
				$feedback->setIs_deleted(1);
				$em->flush();
			}
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
    * @Route("/getfeedback")
     * @Template()
    */
    public function getfeedbackAction(Request $request){
		if($request->get('feedback_id')){
			$em = $this->getDoctrine()->getManager();
			$feedback = $em->getRepository(Contactusfeedback::class)->find($request->get('feedback_id'));

			$email_address = $feedback->getEmail_address();
			$nick_name = $feedback->getName();                   
		   
			return new Response(json_encode(array("email"=>$email_address,"nick_name"=>$nick_name)));
		}
	}
	
	/**
    * @Route("/feedback/sendemail")
    */
    public function sendemailAction(Request $request){
		$feedback_id = $_REQUEST['feedback_id'];
		$email_message = $_REQUEST['email_message'];
		$email_address = $_REQUEST['email_address_hidden'];

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <shreyak.com>' . "\r\n";
		$headers .= 'Cc: aaska@infoware.ws' . "\r\n";

		@mail($email_address,"Member Message - Reply ",$email_message,$headers);

		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
    }
	
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
}
