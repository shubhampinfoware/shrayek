<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Aboutus;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @Route("/admin")
 */
class WhoweareController extends BaseController {
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
    
    /**
     * @Route("/whoweare")
     * @Template()
     */
    public function whoweareAction() {
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
	
        $language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
        $about_us_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Aboutus')->findAll();
        
       // var_dump($about_us_list);exit;
        return array("languages" => $language_list ,"aboutuslist"=>$about_us_list);
        //var_dump($all_category_details);exit;
    }
    
     /**
     * @Route("/savewhoweare",name="save_about_us")
     * @Template()
     */
    public function savewhoweareAction() {

		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
	//var_dump($_REQUEST);exit;
       $session = new Session();
       $em = $this->getDoctrine()->getManager();
       $domain_id = $this->get('session')->get('domain_id');
       $about_us_info = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Aboutus')->findOneBy(array("is_deleted"=>0,"language_id"=>$_REQUEST['language_id'],"domain_id"=>$domain_id));
       if(!empty($about_us_info)){
           $about_us_info->setDescription($_REQUEST['who_we_are']) ;
           $em->persist($about_us_info);
           $em->flush();
       }
       else{
           $about_us_info = new Aboutus();
           $about_us_info->setDescription($_REQUEST['who_we_are']);
           $about_us_info->setLanguage_id($_REQUEST['language_id']);
           $about_us_info->setDomain_id($domain_id);
           $about_us_info->setMain_about_us_id(0);
           $about_us_info->setIs_deleted(0);
          
           $em->persist($about_us_info);
           $em->flush();
           $main_abt_id = $about_us_info->getAbout_us_id();
           
           $about_us_info->setMain_about_us_id($main_abt_id);
           $em->persist($about_us_info);
           $em->flush();
           
       }
        $this->get('session')->getFlashBag()->set('success_msg', 'Inserted successfully');
	return $this->redirect($this->generateUrl('admin_whoweare_whoweare',array("domain"=>$this->get('session')->get('domain'))));
    }


}
