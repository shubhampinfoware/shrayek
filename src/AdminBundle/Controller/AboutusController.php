<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Restaurantmaster;
use AdminBundle\Entity\Branchmaster;

/**
* @Route("/admin")
*/
class AboutusController extends BaseController
{
    /**
     * @Route("/aboutUs",name="about_us")
     * @Template()
     */
    public function indexAction()
    {
		$em = $this->getDoctrine()->getManager();
		$language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		if(empty($language)){
			$feedback_details = array();
		}
	
		$em = $this->getDoctrine()->getManager();
		$about_us = $em->getRepository(Aboutus :: class)->findBy(array('is_deleted'=>0));
		if(empty($about_us)){
			$about_us = array();
		}
	
		return (array('language'=>$language,'about_us'=>$about_us));	
	} 
    /**
     * @Route("/keyEncryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
    public function keyEncryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');
			$res = '';
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c += ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr($c & 0xFF);

            }
			return new Response(base64_encode($res));
		}
		return new Response("");
	}
	/**
     * @Route("/keyDecryption/{string}",defaults = {"string"=""},requirements={"string"=".+"})
     */
	public function keyDecryptionAction($string)
	{
		if($string != "" && $string != NULL && !empty($string) && ctype_space($string) == false)
		{
			$key = $this->container->getParameter('key');

			$res = '';
			$string = base64_decode($string);
			for( $i = 0; $i < strlen($string); $i++)
            {
                $c = ord(substr($string, $i));

                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $res .= chr(abs($c) & 0xFF);

            }
			return new Response($res);
		}
		return new Response("");
	}
	
    /**
     * @Route("/copyRestarauntToBranch",name="copy_rest_to_branch")
     * @Template()
     */
    public function copyRestarauntToBranchAction()
    {
		$restaurant_details = null;
		$em = $this->getDoctrine()->getManager();
		$language = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		$sql = "select main_restaurant_id from restaurant_master where is_deleted = 0 group by main_restaurant_id";
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();
		$restaurants = $stmt->fetchAll();

	
		if(!empty($restaurants)){
			foreach($restaurants as $restaraunt){
				$main_restaurant_id = $restaraunt['main_restaurant_id'];		
				$sql1 = "select * from restaurant_master where is_deleted = 0 and main_restaurant_id = '$main_restaurant_id'";
				$stmt1 = $em->getConnection()->prepare($sql1);
				$stmt1->execute();
				$restaurant_details [] = $stmt1->fetchAll();						
						
			}	
		}
	
//		echo "<pre>";print_r($restaurant_details);exit;
		
		if(!empty($restaurant_details)){
			foreach($restaurant_details as $data_rest){
				#create new branch 
				$counter = 0 ;
				if(!empty($data_rest)){
					foreach($data_rest as $one_entry){
				
						if($counter == 0){
							$main_branch_id = 0;
							$main_branch_id1 = 0;
						}
						
//						exit(utf8_decode(utf8_encode($one_entry['restaraunt_branch'])));
						if($one_entry['restaraunt_branch'] != ''){
						
							$new_branch_no_1 = new Branchmaster();
							$new_branch_no_1->setMain_restaurant_id($one_entry['main_restaurant_id']);
							$new_branch_no_1->setBranch_name($one_entry['restaraunt_branch']);
							$new_branch_no_1->setBranch_address_id($one_entry['address_id']);
							$new_branch_no_1->setMain_branch_flag(0);
							$new_branch_no_1->setStatus($one_entry['status']);
							$new_branch_no_1->setOpening_date(date('Y-m-d',strtotime($one_entry['opening_date'])));
							$new_branch_no_1->setTimings($one_entry['timings']);
							$new_branch_no_1->setDescription($one_entry['description']);
							$new_branch_no_1->setLanguage_id($one_entry['language_id']);
							$new_branch_no_1->setMain_branch_master_id($main_branch_id);
							$new_branch_no_1->setIs_deleted(0);
							$em->persist($new_branch_no_1);
							$em->flush();
							
							if($counter == 0){
								$main_branch_id = $new_branch_no_1->getBranch_master_id();
							}
							$new_branch_no_1->setMain_branch_master_id($main_branch_id);
							$em->flush();
						
						}
						
						$delivery_branch_name = $one_entry['restaurant_name']."_Delivery";
						
						$new_branch_delivery = new Branchmaster();
						$new_branch_delivery->setMain_restaurant_id($one_entry['main_restaurant_id']);
						$new_branch_delivery->setBranch_name($delivery_branch_name);
						$new_branch_delivery->setBranch_address_id($one_entry['address_id']);
						$new_branch_delivery->setMain_branch_flag(1);
						$new_branch_delivery->setStatus($one_entry['status']);
						$new_branch_delivery->setOpening_date(date('Y-m-d',strtotime($one_entry['opening_date'])));
						$new_branch_delivery->setTimings($one_entry['timings']);
						$new_branch_delivery->setDescription($one_entry['description']);
						$new_branch_delivery->setLanguage_id($one_entry['language_id']);
						$new_branch_delivery->setMain_branch_master_id($main_branch_id1);
						$new_branch_delivery->setIs_deleted(0);
						$em->persist($new_branch_delivery);
						$em->flush();
						
						if($counter == 0){
							$main_branch_id1 = $new_branch_delivery->getBranch_master_id();
						}
						$new_branch_delivery->setMain_branch_master_id($main_branch_id1);
						$em->flush();

						if($counter == 0){
							$main_branch_id_for_evaluation = $new_branch_delivery->getMain_branch_master_id();
							
							#add branch in evaluation_feedback
							#find evaluation of looped restaraunts
							
							$evaluated_rest = $em->getRepository("AdminBundle:Evaluationfeedback")->findBy(array('restaurant_id'=>$one_entry['main_restaurant_id']));
							
							if($evaluated_rest){
								foreach($evaluated_rest as $evaluation_entry){
									$evaluation_entry->setMain_branch_id($main_branch_id1);
									$em->flush();
								}
							}
						}						
						$counter = $counter + 1; 	
						
					}
				}
			}
		}
		
		return new Response('Done');
	}	
}
