<?php
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

class DefaultController extends BaseController
{
    /**
     * @Route("/admin")
     * @Template()
     */
    public function indexAction()
    {
		return array();
    }


	/**
     * @Route("/admin/logincheck")
     * @Template()
     */
    public function logincheckAction()
    {
		$this->get('session')->invalidate();
		if(isset($_POST['_csrf_token']) && $_POST['_csrf_token'] != "")
		{
                  
			if($_POST['username'] != "" && $_POST['password'] != "" && isset($_REQUEST['domain']) && $_REQUEST['domain'] != "")
			{
				$username = $this->bwiz_security($_POST['username']);
				$password = $this->bwiz_security($_POST['password']);
					$user = $this->getDoctrine()
						   ->getManager()
						   ->getRepository('AdminBundle:Usermaster')
					   ->findOneBy(array('email'=>$username,'password'=>md5($password),'status'=>'active','is_deleted'=>0));

				$domain_check ='';
				// check domain ---
                                
				if(!empty($user)){
						$domain_check = $this->getDoctrine()
						   ->getManager()
						   ->getRepository('AdminBundle:Domainmaster')
						   ->findOneBy(array('domain_name'=>$this->bwiz_security($_POST['domain']),'is_deleted'=>'0'));

				}

				if(!empty($user) && (in_array($user->getUser_role_id(), array('1', '2', '4', '5' , '6', '7'))) && count($user) == 1 && $user != "" && $user != NULL)
				{
					//$this->get('session')->set('role_id',$user->getUser_role_id());
					$this->get('session')->set('user_id',$user->getUser_master_id());
					$this->get('session')->set('admin_user_id',$user->getUser_master_id());
					$this->get('session')->set('user_type',$user->getUser_type());
					$this->get('session')->set('domain_id',$user->getDomain_id());
					$this->get('session')->set('email',$user->getEmail());
					$this->get('session')->set('role_id',$user->getUser_role_id());
					//$this->get('session')->set("domain",$_REQUEST['domain']);
					//$this->get('session')->set("domain_logo_id",$domain_check->getDomain_logo_id());
					//$this->get('session')->set("domain_cover_image_id",$domain_check->getDomain_logo_id());
					$this->get('session')->getFlashBag()->set('success_msg', 'Login successfully');
					return $this->redirect($this->generateUrl('admin_dashboard_index',array("domain"=>$this->get('session')->get('domain'))));
				}
				else
				{
					$this->get('session')->getFlashBag()->set('error_msg', 'Email or password or Domain is wrong');
				}


			  


			}
			else
			{
				$this->get('session')->getFlashBag()->set('error_msg', 'Email and password is required..');
			}
			//return array();
		}
		else
		{
			$this->get('session')->getFlashBag()->set('error_msg', 'Oops! Something goes wrong! Try again later');
		}
		return $this->redirect($this->generateUrl('admin_default_index'));

}







	/**
     * @Route("/admin/logout")
     * @Template()
     */
    public function logoutAction()
    {
		$this->get('session')->remove('role_id');
		$this->get('session')->remove('user_id');
		$this->get('session')->remove('domain_id');
		$this->get('session')->remove('email');
		$this->get('session')->remove('domain');

		return $this->redirect($this->generateUrl('admin_default_index'));
	}
}
