<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Productmaster;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Votemaster;
use AdminBundle\Entity\Votecomments;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Governoratemaster;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @Route("/admin")
 */
class ProductController extends BaseController {
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
    
    /**
     * @Route("/product")
     * @Template()
     */
    public function indexAction() {
		
		$right_codes = $this->userrightsAction();
		
        $language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
		
		$sql = "select cat.*, media.media_title, media.media_location from product_master cat left join media_library_master media on cat.product_image_id = media.media_library_master_id where cat.is_deleted = 0 group by cat.main_product_id";
		
		$conn = $this->getDoctrine()->getManager()->getConnection();
		$prepare = $conn->prepare($sql);
		$prepare->execute();
		$category_list = $prepare->fetchAll();
		
		
		$i = 0;
		$all_category = array();
		foreach($category_list as $_category_list){
			$lan_cat= array();
                        $lang_wise_category=array();
			foreach($language_list as $_lan_wise_category){
				$lang_category_name = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Productmaster')->findOneBy(array('language_id'=>$_lan_wise_category->getLanguage_master_id(),"main_product_id"=>$_category_list['main_product_id'],"is_deleted"=>0));
					$category_name='';
					if(!empty($lang_category_name)){
						$category_name = $lang_category_name->getProduct_name();
					}
					$lang_wise_category[] = array(
						"language_id"=>$_lan_wise_category->getLanguage_master_id(),
						"product_name"=>$category_name
					);
				
			}
			
			$_category_list['lang_wise_category'] = $lang_wise_category;
			$all_category[] = $_category_list;
		}
		
//		 echo '<pre>';
//		print_r($all_category);
//		exit; 
		
        return array(
			"languages" => $language_list,
			'category_list' => $all_category,
			'right_codes' => $right_codes
		);
        //var_dump($all_category_details);exit;
    }
	
	
    /**
     * @Route("/product/productComments/{product_id}",defaults={"product_id"="0"})
     * @Template()
     */
    public function productcommentAction($product_id) {
		$right_codes = $this->userrightsAction();
		$sql_get_product_comment = "select product_master.*,vote_comments.* from product_master 
									join vote_comments ON product_master.main_product_id = vote_comments.product_id	
									where vote_comments.is_deleted = 0 and product_master.is_deleted = 0
									and vote_comments.product_id = '$product_id' and product_master.product_master_id='$product_id'";	
		$con = $this->getDoctrine()->getManager()->getConnection();
		$stmt = $con->prepare($sql_get_product_comment);	
		$stmt->execute();
		$comments = $stmt->fetchAll();
		return array('comments'=>$comments, 'right_codes' => $right_codes);
//		echo"<pre>";print_r($comments);exit;
	}

    /**
     * @Route("/product/editProductComments")
     * @Template()
     */
    public function editproductcommentAction(Request $req) {
		$em = $this->getDoctrine()->getManager();
		$comments = $em->getRepository('AdminBundle:Votecomments')->find($req->request->get('com_id'));
		if($comments){
			$comments->setComments($req->request->get('comment'));
			$em->flush();
		}
		return new Response('done');
//		echo"<pre>";print_r($comments);exit;
	}
	
    /**
     * @Route("/product/deleteProductComments/{comment_id}",defaults={"comment_id"="0"})
     * @Template()
     */
    public function deleteproductcommentAction($comment_id) {
		$em = $this->getDoctrine()->getManager();
		$comments = $em->getRepository('AdminBundle:Votecomments')->find($comment_id);
		if($comments){
			$comments->setIs_deleted(1);
			$em->flush();
		}
		return $this->redirectTORoute('admin_product_productcomment');
	}
	

	
    
     /**
     * @Route("/addproduct/{catmain_id}", defaults = {"catmain_id" = ""})
     * @Template()
     */
    public function addproductAction($catmain_id) {
		
		$language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
		$em = $this->getDoctrine()->getManager();
		$domain_id = $this->get('session')->get('domain_id');
        
		$category = array();
		if(isset($catmain_id) && $catmain_id != ''){
                    $existing_category = $this->getDoctrine()
					   ->getManager()
					   ->getRepository('AdminBundle:Productmaster')
					   ->findBy(array('is_deleted'=>0,'main_product_id'=>$catmain_id));

                            foreach($existing_category as $existing_category){
				$repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
				$media_file = $repository->find($existing_category->getProduct_image_id());
				
				if(!empty($media_file)){
					$media_url = $this->container->getParameter('live_path').$media_file->getMedia_location().'/'.$media_file->getMedia_name();
				} else {
					$media_url = $this->container->getParameter('live_path').'bundles/Resource/default.png';
				}
				
				$category[] = array(
					'product_master_id' => $existing_category->getProduct_master_id(),
					'product_name' => $existing_category->getProduct_name(),
                                    'product_description' => $existing_category->getProduct_description(),
					'language_id' => $existing_category->getLanguage_id(),
					'main_product_id' => $existing_category->getMain_product_id(),
					'product_image_id' => $existing_category->getProduct_image_id(),
					'product_status' => $existing_category->getProduct_status(),
					'is_deleted' => $existing_category->getIs_deleted(),
					'media_url' => $media_url
				);
                            }
			
		}
		return array(
			"languages" => $language_list ,
			"category" => $category
		);
    }

    /**
     * @Route("/addproductdb")
     * @Template()
     */
    public function addproductdbAction(Request $request) {
		
		if($request->request->all()){
			$em = $this->getDoctrine()->getManager();
			
			$media = $_FILES['image']['name'];
			if(isset($media) && $media != ''){
				
				$upload_dir = $this->container->getParameter('upload_dir1');
				$location = '/Resource/product';
				
				$mediatype = $this->getDoctrine()
						->getManager()
						->getRepository(Mediatype::class)
						->findOneBy(array(
							'media_type_name' => 'Image',
							'is_deleted'=>0)
						);
				$allowedExts = explode(',',$mediatype->getMedia_type_allowed());
				$temp = explode('.',$_FILES['image']['name']);
				$extension = end($temp);
				if(in_array($extension, $allowedExts)){
					$media_id = $this->mediaupload($_FILES['image'], $upload_dir, $location, $mediatype->getMedia_type_id());
				}
			}
			
			if($request->get('language_master_id') && $request->get('main_product_id')){
				/* for edit */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SEPV22", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
				$category1 = $this->getDoctrine()
					   ->getManager()
					   ->getRepository('AdminBundle:Productmaster')
					   ->findOneBy(array('is_deleted'=>0,'main_product_id'=>$request->get('main_product_id'),"language_id"=>$request->get('language_master_id')));

				
				if(!empty($category1)){
                                   
                                    $category1->setProduct_name($request->get('product_name'));
                                    $category1->setProduct_description($request->get('product_description'));
                                    $category1->setLanguage_id($request->get('language_master_id'));
                                    if(isset($media_id)){
                                            $category1->setProduct_image_id($media_id);
                                    }
                                    $category1->setProduct_status($request->get('status'));
                                    $em->flush();
                                    $this->get('session')->getFlashBag()->set('success_msg', 'Data updated successfully');
                                }else{
                                    $category2 = $this->getDoctrine()
					   ->getManager()
					   ->getRepository('AdminBundle:Productmaster')
					   ->findOneBy(array('is_deleted'=>0,'product_master_id'=>$request->get('main_product_id')));
                                    
                                    $category = new Productmaster();
                                    $category->setProduct_name($request->get('product_name'));
                                    $category->setProduct_description($request->get('product_description'));
                                    $category->setLanguage_id($request->get('language_master_id'));
                                    $category->setMain_product_id($request->get('main_product_id'));
                                    if(isset($media_id)){
                                            $category->setProduct_image_id($media_id);
                                    }else{
                                       $category->setProduct_image_id($category2->getProduct_image_id()); 
                                    }
                                    $category->setProduct_status($request->get('status'));
                                    $em->persist($category);
                                    $em->flush();
                                    $this->get('session')->getFlashBag()->set('success_msg', 'Data inserted successfully');
                                }
				
				
				
			} else {
				/* for insert */
				
				$category = new Productmaster();
				$category->setProduct_name($request->get('product_name'));
                                $category->setProduct_description($request->get('product_description'));
				$category->setLanguage_id($request->get('language_master_id'));
				if(isset($media_id)){
					$category->setProduct_image_id($media_id);
				}
				
				$category->setProduct_status($request->get('status'));
				
				$em->persist($category);
				$em->flush();
				$category->setMain_product_id($category->getProduct_master_id());
				$em->flush();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Data inserted successfully');
			}
			
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		return $this->redirectToRoute('admin_product_index');
    }
	
	public function mediaupload($file, $upload_dir, $location, $media_type_id){
		
		$upload_dir = $upload_dir.$location;
		if(!is_dir($upload_dir)){
			mkdir($upload_dir);	
		}
		
		$clean_image = preg_replace('/\s+/', '', $file['name']);
        $media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;
		
		$is_uploaded = move_uploaded_file($file['tmp_name'], $upload_dir.'/'.$media_name);
		
		if($is_uploaded){
			$em = $this->getDoctrine()->getManager();
			
			$mediamaster = new Medialibrarymaster();
			$mediamaster->setMedia_type_id($media_type_id);
			$mediamaster->setMedia_title($media_name);
			$mediamaster->setMedia_location("/bundles/{$location}");
			$mediamaster->setMedia_name($media_name);
			$mediamaster->setCreated_on(date('Y-m-d h:i:s'));
			$mediamaster->setIs_deleted(0);
			
			$em->persist($mediamaster);
			$em->flush();
			
			return $mediamaster->getMedia_library_master_id();
		}
		return false;
	}
	
	/**
	* @Route("/deleteProduct/{cat_id}", defaults = {"cat_id" = ""})
	*/
	public function deleteProductAction($cat_id, Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDPV23", $right_codes);
		if ($rights_search != 1) {
			$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
			return $this->redirect($this->generateUrl('admin_dashboard_index'));
		}
		/* end: check for access */
		
		if(isset($cat_id) && $cat_id != ''){
			
			$em = $this->getDoctrine()->getManager();
			$category = $em->getRepository(Productmaster::class)->findBy(array('main_product_id'=>$cat_id,'is_deleted'=>0));
			
			if(!empty($category)){
				foreach($category as $ct_){
				
					$ct_->setIs_deleted(1);
					$em->flush();
				
				}
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Product deleted successfully');
			} else {
				$this->get('session')->getFlashBag()->get('error_msg', 'Product not found');
			}
		} else {
			$this->get('session')->getFlashBag()->get('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
	* @Route("/product/vote/delete/{vote_id}/{comment_id}", defaults = {"vote_id" = "", "comment_id" = ""})
	*/
	public function deleteVoteAction($vote_id, $comment_id, Request $request){
		
		if(isset($comment_id)){
			$entity = $this->getDoctrine()->getManager();
			$vote_master = $entity->getRepository(Votecomments::class)->findOneBy([
				'vote_comment_id' => $comment_id,
				'is_deleted' => 0
			]);
			
			if(!empty($vote_master)){
				$vote_master->setIs_deleted(1);
				$entity->flush();
			}
		}
		
		if(isset($vote_id) && $vote_id != ''){
			
			$em = $this->getDoctrine()->getManager();
			$vote_master = $em->getRepository(Votemaster::class)->findOneBy([
				'vote_master_id' => $vote_id,
				'is_deleted' => 0
			]);
			
			if(!empty($vote_master)){
				$vote_master->setIs_deleted(1);
				$em->flush();
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Vote deleted successfully');
			} else {
				$this->get('session')->getFlashBag()->get('error_msg', 'Vote not found');
			}
		} else {
			$this->get('session')->getFlashBag()->get('error_msg', 'Something went wrong');
		}
		
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
	* @Route("/productajaxupdatestatus")
	*/
	public function ajaxupdatestatusAction(Request $request){
		
		if($request->get('cat_id')){
			$em = $this->getDoctrine()->getManager();
			$category = $em->getRepository(Productmaster::class)->findBy(array("main_product_id"=>$request->get('cat_id')));
			foreach($category as $product){
				$current_status = $product->getProduct_status();
				if($current_status == 'active'){
					$product->setProduct_status('inactive');
				} else {
					$product->setProduct_status('active');
				}
				$em->flush();				
			}
			echo 'true';exit;
		}
		echo 'false';exit;
	}

	/**
	* @Route("/product/vote/view/{product_id}", defaults={"product_id" = ""})
	* @Template()
	*/
	public function viewvoteAction($product_id){
		$vote_data = array();
		$product_name = '';
		$product_media_url = '';
		if(isset($product_id)){
			$sql_query = "select product.product_name, vote.*, CONCAT(user.user_firstname, ' ', user.user_lastname) as user_fullname, user.email, CONCAT(media.media_location,'/',media.media_name) as media_url from product_master product, vote_master vote, user_master user, media_library_master media where vote.product_id = product.product_master_id and vote.user_id = user.user_master_id and product.product_image_id = media.media_library_master_id and product.product_master_id = {$product_id} and product.is_deleted = 0 and vote.is_deleted = 0 group by vote.vote_master_id";
			$vote_master = $this->firequery($sql_query);
			
			if(!empty($vote_master)){
				$live_path = $this->container->getParameter('live_path');
				foreach($vote_master as $_vote){
					
					$product_name = $_vote['product_name'];
					$product_media_url = $live_path.$_vote['media_url'];
					
					$vote_comment = $this->getDoctrine()->getRepository('AdminBundle:Votecomments')->findOneBy([
						'user_id' => $_vote['user_id'],
						'product_id' => $_vote['product_id'],
						'is_deleted' => 0
					]);
					
					$_vote['vote_type'] = ucfirst($_vote['vote_type']);
					
					$_vote['vote_comment_id'] = '';
					$_vote['comment'] = '';
					if(!empty($vote_comment)){
						$_vote['vote_comment_id'] = $vote_comment->getVote_comment_id();
						$_vote['comment'] = $vote_comment->getComments();
					}
					
					$vote_data[] = $_vote;
				}
			}
		}
		
		/* echo '<pre>';
		print_r($vote_data);
		exit; */
		
		return array(
			'product_name' => $product_name,
			'product_media_url' => $product_media_url,
			'vote_data' => $vote_data
		);
	}
}
