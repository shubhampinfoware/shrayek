<?php
namespace Main\AdminBundle\Controller;
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AdminBundle\Entity\Siteprivacy;
use AdminBundle\Entity\Languagemaster;

use Symfony\Component\HttpFoundation\JsonResponse;
/**
* @Route("/admin")
*/
class SiteprivacyController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
    * @Route("/siteprivacy")
    * @Template()
    */
    public function indexAction(){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		$repository = $this->getDoctrine()->getRepository(Languagemaster::class);
		$languages = $repository->findBy(
				array(
					'is_deleted' => 0
				)
			);
		
		if(empty($languages)){
			$languages = array();
		}
		
		$repository = $this->getDoctrine()->getRepository(Siteprivacy::class);
		$siteprivacy_info = $repository->findAll();
		
		if(empty($siteprivacy_info)){
			$siteprivacy_info = array();
		}
		
		return array(
			'languages' => $languages,
			'siteprivacy_info' => $siteprivacy_info
		);
    }
	
	/**
    * @Route("/siteprivacy/save")
    * @Template()
    */
    public function saveAction(Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if($request->request->all()) {
			
			$em = $this->getDoctrine()->getManager();
			
			$siteprivacy = new Siteprivacy();
			$siteprivacy->setDescription($request->get('description'));
			$siteprivacy->setLanguage_id($request->get('language_id'));
			$siteprivacy->setLast_updated(date('Y-m-d h:i:s'));
			$domain_id = $this->get('session')->get('domain_id');
			if(isset($domain_id)){
				$siteprivacy->setDomain_id($this->get('session')->get('domain_id'));
			}
			$siteprivacy->setIs_deleted(0);
			
			$em->persist($siteprivacy);
			$em->flush();
			
			$this->get('session')->getFlashBag()->set('success_msg', 'Data inserted successfully');
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
	
	/**
    * @Route("/siteprivacy/update")
    */
	public function updateAction(Request $request){
		
		/* check for access */
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SMSS39", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		/* end: check for access */
		
		if($request->request->all()) {
			$site_privacy_id = $request->get('site_privacy_id');
			
			if(isset($site_privacy_id)){
				$em = $this->getDoctrine()->getManager();
				$siteprivacy = $em->getRepository(Siteprivacy::class)->find($site_privacy_id);
				
				if(!empty($siteprivacy)) {
					$siteprivacy->setDescription($request->get('description'));
					$siteprivacy->setLanguage_id($request->get('language_id'));
					$siteprivacy->setLast_updated(date('Y-m-d h:i:s'));
					$domain_id = $this->get('session')->get('domain_id');
					if(isset($domain_id)){
						$siteprivacy->setDomain_id($this->get('session')->get('domain_id'));
					}
					$siteprivacy->setIs_deleted(0);
					$em->flush();
					
					$this->get('session')->getFlashBag()->set('success_msg', 'Data updated successfully');
				}
			}
		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}
}
