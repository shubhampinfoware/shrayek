<?php
namespace Main\AdminBundle\Controller;
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use AdminBundle\Entity\Contactus;
use AdminBundle\Entity\Drawmaster;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Contactusmaster;
use AdminBundle\Entity\Contactusfeedback;
use AdminBundle\Entity\Draweligibleusers;
use AdminBundle\Entity\Languagemaster;

use Symfony\Component\HttpFoundation\JsonResponse;
/**
* @Route("/admin")
*/
class DrawController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }
	
    /**
    * @Route("/manageDraw/{type}",defaults={"type"=""})
    * @Template()
    */
    public function indexAction($type){
		$draws = $this->getDoctrine()->getRepository(Drawmaster :: class)->findBy(
			array(
				'is_deleted'=>0,
				'draw_type'=>$type
			)
		);

		$draw_list = array();
		if(!empty($draws)){
			foreach($draws as $_draw){
				
				$user_fullname = '';
				$email = '';
				if($_draw->getWinner_id() != '0' && $_draw->getWinner_id() != 0){
					// get winner
					$user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findOneBy([
						'user_master_id' => $_draw->getWinner_id(),
						'status' => 'active',
						'is_deleted' => 0
					]);
					
					if(!empty($user)){
						if($user->getUser_bio() != ''){
							$user_fullname = $user->getUser_bio();
						} else {
							$user_fullname = $user->getUser_firstname().' '.$user->getUser_lastname();
						}
						$email = $user->getEmail();
					}
				}
				
				$_draw->user_fullname = $user_fullname;
				$_draw->user_email = $email;
				
				$draw_id = $_draw->getDraw_master_id();
				$em = $this->getDoctrine()->getManager();
				$draw_users = $em->getRepository('AdminBundle:Draweligibleusers')->findBy(array(
					'draw_id' => $draw_id
				));
				
				$_draw->eligible_user_count = count($draw_users);
				$draw_list[] = $_draw;
			}
		}
		
		return array('draw_type'=>$type,'draw'=>$draw_list);
	}
	
    /**
    * @Route("/addDraw/{type}/{draw_id}",defaults={"draw_id"="","type"=""})
    * @Template()
    */
    public function adddrawAction($draw_id,$type){		
	
		$em = $this->getDoctrine()->getManager();
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			
		
		$branch_list = null;
		
		if($draw_check){	
			$relation_id = $draw_check->getRelation_id();		

#get branches of restaurant SELECTed
			$branch_list = array();
			$branch_query = "SELECT * FROM branch_master where is_deleted = 0 and main_restaurant_id = '".$relation_id."' group by main_branch_master_id";
			$branch_list_arr = $this->firequery($branch_query);
			if (!empty($branch_list_arr)) {
				foreach ($branch_list_arr as $cakey1 => $cval1) {
					$branch_list[] = array(
						"main_branch_master_id" => $cval1['main_branch_master_id'],
						"branch_name" => $cval1['branch_name'],
					);
				}
			}
						
		}

		
		$language_master = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));			
		
		$products = array();
		$categorys = array();
		$foodtypes = array();
		$restaurants = array();
		
		if($type == 'evaluation' || $type == 'eval_with_gallery'){
			
			$cat_sql = "select main_category_id from category_master where category_status='active' and is_deleted=0 group by main_category_id";
			$categorys = $this->firequery($cat_sql);
		
			if(!empty($categorys)){
				foreach($categorys as $key=>$value){
					$lang_wise = array();
					
					foreach($language_master as $lang){
						$sql = "select * from category_master where category_status='active' and is_deleted=0 and main_category_id = ".$value['main_category_id']." and language_id = ".$lang->getLanguage_master_id();
						$category_lang = $this->firequery($sql);
						if(!empty($category_lang)){
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>$category_lang[0]['category_name']);							
						}else{
							
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>'-');							
						}	

					}	
					$categorys[$key]['lang_wise'] = $lang_wise;
				}
			}
			
			$foodtype_sql = "select foodtype.main_food_type_id from food_type_master foodtype where foodtype.is_deleted=0 group by foodtype.main_food_type_id";
			$foodtypes = $this->firequery($foodtype_sql);
		
			if(!empty($foodtypes)){
				foreach($foodtypes as $key=>$value){
					
					$lang_wise = array();
					foreach($language_master as $lang){
						$sql = "select * from food_type_master where is_deleted=0 and main_food_type_id = ".$value['main_food_type_id']." and language_id = ".$lang->getLanguage_master_id();
						$foodtype_lang = $this->firequery($sql);
						if(!empty($foodtype_lang)){
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>$foodtype_lang[0]['food_type_name']);
						}else{
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>'-');
						}
					}
					
					$foodtypes[$key]['lang_wise'] = $lang_wise;
				}
			}
			
			$sql = "select main_restaurant_id from restaurant_master where status='active' and is_deleted=0 group by main_restaurant_id";
			$con = $em->getConnection();
			$stmt = $con->prepare($sql);
			$stmt->execute();
			$restaurants = $stmt->fetchAll();
			
			if(!empty($restaurants) && !empty($language_master)){
				foreach($restaurants as $key=>$value){
					$lang_wise = array();
					
					foreach($language_master as $lang){
						$sql = "select * from restaurant_master where status='active' and is_deleted=0 and main_restaurant_id = ".$value['main_restaurant_id']." and language_id = ".$lang->getLanguage_master_id();
						$con = $em->getConnection();
						$stmt = $con->prepare($sql);
						$stmt->execute();
						$restaurants_lang = $stmt->fetchAll();
						if(!empty($restaurants_lang)){
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>$restaurants_lang[0]['restaurant_name']);							
						}else{
							
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>'-');							
						}	

					}	
					$restaurants[$key]['lang_wise'] = $lang_wise;
				}	
			}
		}

//		echo"<pre>";print_r($restaurants);exit;
		if($type == 'voting'){
			
			$sql = "select main_product_id from product_master where is_deleted=0 group by main_product_id";
			$con = $em->getConnection();
			$stmt = $con->prepare($sql);
			$stmt->execute();
			$products = $stmt->fetchAll();
			
			if(!empty($products) && !empty($language_master)){
				foreach($products as $key=>$value){
					$lang_wise = array();
					
					foreach($language_master as $lang){
						$sql = "select * from product_master where is_deleted=0 and main_product_id = ".$value['main_product_id']." and language_id = ".$lang->getLanguage_master_id();
						$con = $em->getConnection();
						$stmt = $con->prepare($sql);
						$stmt->execute();
						$product_lang = $stmt->fetchAll();
						if(!empty($product_lang)){
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>$product_lang[0]['product_name']);							
						}else{
							
							$lang_wise [] = array('lang_id'=>$lang->getLanguage_master_id(),'name'=>'-');							
						}	

					}	
					$products[$key]['lang_wise'] = $lang_wise;
				}	
			}
			

		}
		
//		echo"<pre>";print_r($products);exit;	
		
		return array(
			"draw_type" => $type,
			'draw' => $draw_check,
			'categorys' => $categorys,
			'foodtypes' => $foodtypes,
			'restaurants' => $restaurants,
			'products' => $products,
			'branch_list'=>$branch_list
		);
	}	
	
	/**
    * @Route("/getFoodTypeList")
    * @Template()
    */
    public function getFoodTypeListAction(Request $request){
		$postData = $request->request->all();
		
		$html = '';
		$success = false;
		if(array_key_exists('cat_id', $postData)){
			$cat_id = $postData['cat_id'];
			
			$em = $this->getDoctrine()->getManager();
			$language_master = $em->getRepository('AdminBundle:Languagemaster')->findBy(array('is_deleted'=>0));
			$foodtype_sql = "select foodtype.main_food_type_id from food_type_master foodtype, food_type_category_relation rel where foodtype.main_food_type_id = rel.main_food_type_id and rel.main_category_id = {$cat_id} and foodtype.is_deleted=0 and rel.is_deleted = 0 group by foodtype.main_food_type_id";
			$foodtypes = $this->firequery($foodtype_sql);
		
			if(!empty($foodtypes)){
				$html = "<option value=''>Select Foodtype</option>";
				foreach($foodtypes as $key=>$value){
					
					$lang_wise_array = array();
					foreach($language_master as $lang){
						$sql = "select * from food_type_master where is_deleted=0 and main_food_type_id = ".$value['main_food_type_id']." and language_id = ".$lang->getLanguage_master_id();
						$foodtype_lang = $this->firequery($sql);
						if(!empty($foodtype_lang)){
							$lang_wise_array[] = $foodtype_lang[0]['food_type_name'];				
						}else{
							$lang_wise_array[] = '-';
						}
					}
					
					$lang_wise_name = implode(' / ', $lang_wise_array);
					
					$html .= "<option value='{$value['main_food_type_id']}'>{$lang_wise_name}</option>";
				}
				$success = true;
			}
		}
		
		$data = array(
			'success' => $success,
			'html' => $html
		);
		
		echo json_encode($data);exit;
	}
	
	/**
    * @Route("/getRestaurantList")
    * @Template()
    */
    public function getRestaurantListAction(Request $request){
		$postData = $request->request->all();
		
		$html = '';
		$success = false;
		if(array_key_exists('cat_id', $postData) && array_key_exists('foodtype_id', $postData)){
			$cat_id = $postData['cat_id'];
			$foodtype_id = $postData['foodtype_id'];
			
			$em = $this->getDoctrine()->getManager();
			$language_master = $em->getRepository('AdminBundle:Languagemaster')->findBy(array('is_deleted'=>0));
			
			//$res_sql = "select res.main_restaurant_id from restaurant_master res, restaurant_foodtype_relation rel, category_master cat, food_type_master ftype where cat.main_category_id = rel.main_caetgory_id and ftype.main_food_type_id = rel.main_foodtype_id and cat.main_category_id = {$cat_id} and ftype.main_food_type_id = {$foodtype_id} and cat.is_deleted = 0 and ftype.is_deleted = 0 and res.is_deleted=0 and rel.is_deleted=0 group by res.main_restaurant_id ORDER BY res.restaurant_name ASC";
			$cat_type = "and cm.main_category_id = {$cat_id}";
			$food_type = "and ftm.main_food_type_id = {$foodtype_id}";
			$res_sql = "SELECT rm.*
						from restaurant_master as rm
						left join restaurant_foodtype_relation as rfr on rfr.restaurant_id=rm.main_restaurant_id
						left join category_master as cm on cm.main_category_id=rfr.main_caetgory_id
						left join food_type_master as ftm on ftm.main_food_type_id=rfr.main_foodtype_id
						where rm.is_deleted=0 {$cat_type} {$food_type} and rm.is_deleted=0 and rm.status != 'inactive' and rfr.is_deleted = 0 group by rm.main_restaurant_id ORDER BY rm.restaurant_name ASC,cm.sort_order DESC";
			$restaurants = $this->firequery($res_sql);
		
			if(!empty($restaurants)){
				
				$final_array = array();
				foreach($restaurants as $key=>$value){
					
					$lang_wise_array = array();
					foreach($language_master as $lang){
						$sql = "select * from restaurant_master where status='active' and is_deleted=0 and main_restaurant_id = ".$value['main_restaurant_id']." and language_id = ".$lang->getLanguage_master_id()." order by language_id";
						$res_lang = $this->firequery($sql);
						if(!empty($res_lang)){
							$lang_wise_array[] = $res_lang[0]['restaurant_name'];				
						}else{
							$lang_wise_array[] = '-';
						}
					}
					
					$lang_wise_name = implode(' / ', $lang_wise_array);
					$final_array[] = array(
						'main_restaurant_id' => $value['main_restaurant_id'],
						'lang_wise_name' => stripslashes($lang_wise_name)
					);
				}
				
				if(!empty($final_array)){
					
					$final_array = $this->msort($final_array, array('lang_wise_name'));
					
					$html = "<option value=''>Select Restaurants</option>";
					foreach($final_array as $key=>$value){
						
						$html .= "<option value='{$value['main_restaurant_id']}'>{$value['lang_wise_name']}</option>";
					}
					$success = true;
				}
			}
		}
		
		$data = array(
			'success' => $success,
			'html' => $html
		);
		
		echo json_encode($data);exit;
	}
	
	function msort($array, $key, $sort_flags = SORT_REGULAR) {
		if (is_array($array) && count($array) > 0) {
			if (!empty($key)) {
				$mapping = array();
				foreach ($array as $k => $v) {
					$sort_key = '';
					if (!is_array($key)) {
						$sort_key = $v[$key];
					} else {
						foreach ($key as $key_key) {
							$sort_key .= $v[$key_key];
						}
						$sort_flags = SORT_STRING;
					}
					$mapping[$k] = $sort_key;
				}
				asort($mapping, $sort_flags);
				$sorted = array();
				foreach ($mapping as $k => $v) {
					$sorted[] = $array[$k];
				}
				return $sorted;
			}
		}
		return $array;
	}
	
    /**
    * @Route("/sendMail")
    * @Template()
    */
    public function sendmailAction(Request $req){		
	
		$draw_id = $req->request->get('draw_id');
		$message = $req->request->get('message');
		
		$em = $this->getDoctrine()->getManager();
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			
		
		if($draw_check){
			
			$winner = $draw_check->getWinner_id();
			
			$draw_name = $draw_check->getDraw_name();
			
			$type_label = '';
			
			$type = $draw_check->getDraw_type();
			if($type == 'voting'){
				$type_label = "Voting";
			}
			if($type == 'evaluation'){
				$type_label = "Evaluation";
			}
			if($type == 'eval_with_gallery'){
				$type_label = "Evaluation With Gallery";
			}
			if($type == 'ask_community'){
				$type_label = "Ask the Community";
			}
			if($type == 'new_user'){
				$type_label = "New User";
			}
			if($type == 'all_user'){
				$type_label = "Regular";
			}			
			if($winner != 0){
				
				$user_master = $em->getRepository(Usermaster :: class)->findOneBy(array('user_master_id'=>$winner,'is_deleted'=>0));							
				if($user_master){
							
					$email = $user_master->getEmail();
			
					$shreyak = $this->container->getParameter('live_path');

					$email_message = "<b>In recent ".$type_label." draw on Shrayek</b><br><br>";
					$email_message .= "<b>Draw Name</b> : ".$draw_name."<br><br>";
					$email_message .= $message;
					//$email_message .= "<span style='color:#0000ff;'>Congratulations,  You are selected as winner</span><br><br>";

					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
					$headers .= 'Bcc: shubhamp@infoware.ws' . "\r\n";
					$headers .= 'From: <shreyak.com>' . "\r\n";

					// echo "$email_message";exit;
					//@mail($email,"Shrayek Draw REsults ",$email_message,$headers);
				}
			}
			return new Response('done');
		}else{
			return new Response('notdone');		
		}
	}	
	
	/**
    * @Route("/elegibleUser/{type}/{draw_id}",defaults={"draw_id"="","type"=""})
    * @Template()
    */
    public function elegibleuserAction($draw_id,$type){
	
		$em = $this->getDoctrine()->getManager();
		$users = array();
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			

		$users = array();
		if($draw_check){
			$sql = "select user_bio, user_firstname, user_lastname, email, username, user_mobile, user_gender, date_of_birth, rel.user_id, rel.draw_id, rel.status from user_master, draw_eligible_users rel where user_master.user_master_id = rel.user_id and rel.draw_id = {$draw_id} and user_master.is_deleted = 0";
			$users = $this->firequery($sql);
		}
		
		return array(
			"draw_type" => $type,
			"users" => $users
		);
	}
	
	/**
    * @Route("/drawupdatestatus")
    * @Template()
    */
	public function drawupdatestatusAction(Request $request){
		$postData = $request->request->all();
		
		if(array_key_exists('draw_id', $postData) && array_key_exists('user_id', $postData)){
			$draw_id = $postData['draw_id'];
			$user_id = $postData['user_id'];
			
			$em = $this->getDoctrine()->getManager();
			$draw_user = $em->getRepository('AdminBundle:Draweligibleusers')->findOneBy(
				array(
					'draw_id' => $draw_id,
					'user_id' => $user_id
				)
			);
			
			if(!empty($draw_user)){
				$status = $draw_user->getStatus();
				if($status == 'active'){
					$draw_user->setStatus('inactive');
				} else {
					$draw_user->setStatus('active');
				}
				
				$em->flush();
				echo 'true';exit;
			}
		}
		echo 'false';exit;
	}
	
    /**
    * @Route("/elegibleUserOLD/{type}/{draw_id}",defaults={"draw_id"="","type"=""})
    * @Template()
    */
    /*public function elegibleuserAction($draw_id,$type){
	
		$em = $this->getDoctrine()->getManager();
		$users = array();
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			

		if($draw_check){
			$start_date = $draw_check->getStart_date();
			$end_date = $draw_check->getEnd_date();
			
			$relation_id = $draw_check->getRelation_id();
			
			$relation_condtion = "";
			
			if($relation_id != 0 && ($type == 'evaluation' || $type == 'eval_with_gallery')){
				$relation_condtion = " AND evaluation_feedback.restaurant_id = $relation_id";				
			}
			
			if($relation_id != 0 && $type == 'voting'){
				$relation_condtion = " AND product_id = $relation_id";				
			}			
			
			
			if($type == 'voting'){
				if($start_date == null){
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from vote_master where is_deleted = 0 and created_datetime < '$end_date' $relation_condtion) and is_deleted=0";					
				}else{
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from vote_master where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' $relation_condtion) and is_deleted=0";					
				}

				$con = $em->getConnection();
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$users = $stmt->fetchAll();
			}
			
			if($type == 'new_user' || $type == 'all_user'){
				
				if($start_date == null){
					$sql = "select * from user_master where user_type='user' and is_deleted=0 and created_datetime < '$end_date'";
				}else{
					$sql = "select * from user_master where user_type='user' and is_deleted=0 and created_datetime < '$end_date' and created_datetime > '$start_date'";					
				}
			
				$con = $em->getConnection();
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$users = $stmt->fetchAll();
			}			
		
			
			if($type == 'evaluation'){
				if($start_date == null){
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from evaluation_feedback where is_deleted = 0 and created_datetime < '$end_date' and status='approved' $relation_condtion) and is_deleted=0";
				}else{
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from evaluation_feedback where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and status='approved' $relation_condtion) and is_deleted=0";					
				}	

				$con = $em->getConnection();
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$users = $stmt->fetchAll();
			}
			
			if($type == 'ask_community'){
				if($start_date == null){
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from community_chat where is_deleted = 0 and created_datetime < '$end_date') and is_deleted=0";
				}else{
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from community_chat where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date') and is_deleted=0";					
				}	
				$con = $em->getConnection();
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$users = $stmt->fetchAll();
			}
			

			if($type == 'eval_with_gallery'){
				
				if($start_date == null){
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from evaluation_feedback
							JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
							where evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and e_f_g.is_deleted = 0 and status='approved' $relation_condtion) and user_master.is_deleted=0";
				}else{
					$sql = "select * from user_master where user_master_id IN (select DISTINCT user_id from evaluation_feedback JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
					where evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and e_f_g.is_deleted = 0 and status='approved' $relation_condtion) and user_master.is_deleted=0";					
				}	
				
				$con = $em->getConnection();
				$stmt = $con->prepare($sql);
				$stmt->execute();
				$users = $stmt->fetchAll();
			}			
			
		}	
//		echo"<pre>";print_r($users);exit;
		return array("draw_type"=>$type,'users'=>$users);
	}*/

    /**
    * @Route("/selectWinner/{type}/{draw_id}",defaults={"draw_id"="","type"=""})
    * @Template()
    */
    public function selectwinnerAction($draw_id,$type){		
	
		$em = $this->getDoctrine()->getManager();
		$users = array();

			
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			
		$live_path = $this->container->getParameter('live_path');
		if($draw_check){
			if($draw_check->getQualification_status() == 'active'){
				$start_date = $draw_check->getStart_date();
				$end_date = $draw_check->getEnd_date();
				$type = $draw_check->getDraw_type();
				$qualification_number = $draw_check->getQualification_number();

				if($type == 'evaluation'){
					// EVALUATION

					if($start_date == null){
						$sql = "SELECT u.*, count(*) as total from evaluation_feedback e, user_master u where e.user_id = u.user_master_id and e.is_deleted = 0 and user_id in (SELECT user_id from evaluation_feedback where created_datetime < '{$end_date}' and status='approved' group by user_id having count(user_id) >= {$qualification_number}) and e.created_datetime < '{$end_date}' and e.status='approved' and u.is_deleted = 0 group by user_master_id order by rand()";
					} else {
						$sql = "SELECT u.*, count(*) as total from evaluation_feedback e, user_master u where e.user_id = u.user_master_id and e.is_deleted = 0 and user_id in (SELECT user_id from evaluation_feedback where created_datetime < '{$end_date}' and created_datetime > '{$start_date}' and status='approved' group by user_id having count(user_id) >= {$qualification_number}) and e.created_datetime < '{$end_date}' and e.created_datetime > '{$start_date}' and e.status='approved' and u.is_deleted = 0 group by user_master_id order by rand()";
					}
				} else if($type == 'eval_with_gallery') {
					// EVALUATION WITH GALLERY

					if($start_date == null){
						$innerQuery = "SELECT user_id from evaluation_feedback
						JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
						WHERE evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and e_f_g.is_deleted = 0 
						AND status='approved' 
						GROUP BY user_id 
						HAVING count(user_id) >= {$qualification_number}";
						
						$sql = "SELECT u.*, count(*) as total from user_master u, evaluation_feedback e, evaluation_feedback_gallery g where e.user_id = u.user_master_id and e.evaluation_feedback_id = g.evaluation_feedback_id and user_master_id IN ({$innerQuery}) and e.created_datetime < '$end_date' and e.status='approved' and u.is_deleted=0 and e.is_deleted = 0 and g.is_deleted = 0 and user_role_id = 3 group by user_master_id order by rand()";
					} else {
						$innerQuery = "SELECT user_id from evaluation_feedback
						JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
						WHERE evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and e_f_g.is_deleted = 0 
						AND status='approved' 
						GROUP BY user_id 
						HAVING count(user_id) >= {$qualification_number}";
					
						$sql = "SELECT u.*, count(*) as total from user_master u, evaluation_feedback e, evaluation_feedback_gallery g where e.user_id = u.user_master_id and e.evaluation_feedback_id = g.evaluation_feedback_id and user_master_id IN ({$innerQuery}) and e.created_datetime < '$end_date' and e.created_datetime > '$start_date' and e.status='approved' and u.is_deleted=0 and e.is_deleted = 0 and g.is_deleted = 0 and user_role_id = 3 group by user_master_id order by rand()";
					}
				} else if($type == 'ask_community') {
					// ASK THE COMMUNITY

					if($start_date == null){
						$sql = "SELECT u.*, count(*) as total from user_master u, community_chat c where u.user_master_id = c.user_id and user_master_id IN (SELECT user_id from community_chat where community_chat.is_deleted = 0 and parent_id != 0 and created_datetime < '$end_date' group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and u.is_deleted=0 and parent_id != 0 and c.created_datetime < '$end_date' group by user_master_id order by rand()";
					}else{
						$sql = "SELECT u.*, count(*) as total from user_master u, community_chat c where u.user_master_id = c.user_id and user_master_id IN (SELECT user_id from community_chat where community_chat.is_deleted = 0 and parent_id != 0 and created_datetime < '$end_date' and created_datetime > '$start_date' group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and u.is_deleted=0 and parent_id != 0 and c.created_datetime < '$end_date' and c.created_datetime > '$start_date'  group by user_master_id order by rand()";
					}
				} else if($type == 'loyalty_points') {
					if($start_date == null){
						$sql = "SELECT u.*, count(*) as total from user_master u, competition_user_relation c where u.user_master_id = c.user_id and u.is_deleted=0 and c.is_deleted=0 and c.created_datetime < '$end_date' and user_master_id in (SELECT user_id from competition_user_relation where is_deleted = 0 and created_datetime < '$end_date' group by user_id having count(user_id) >= {$qualification_number}) group by user_id";
					}else{
						$sql = "SELECT u.*, count(*) as total from user_master u, competition_user_relation c where u.user_master_id = c.user_id and u.is_deleted=0 and c.is_deleted=0 and c.created_datetime < '$end_date' and user_master_id in (SELECT user_id from competition_user_relation where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' group by user_id having count(user_id) >= {$qualification_number}) group by user_id";
					}
				}

				$userList = $this->firequery($sql);

				$users = [];
				if(!empty($userList)){
					$qualification_number = $draw_check->getQualification_number();

					foreach($userList as $_user){
						$no_of_user_eligibility = floor($_user['total'] / $qualification_number);
						for($i=1; $i <= $no_of_user_eligibility; $i++){
							$temp_user = array();
							$temp_user['user_master_id'] = $_user['user_master_id'];
							$temp_user['user_bio'] = $_user['user_bio'];
							$temp_user['user_firstname'] = $_user['user_firstname'];
							$temp_user['user_lastname'] = $_user['user_lastname'];
							$temp_user['user_image'] = $_user['user_image'];

							$users[] = $temp_user;
						}
					}
					shuffle($users);

					$winner = array();
					$data = array();
					
					$winner_html = '';
					
					if(!empty($users)){
						
						$users_list = array();
						foreach($users as $_user){
							$temp_user = array();
							$temp_user['user_master_id'] = $_user['user_master_id'];
							$temp_user['user_bio'] = $_user['user_bio'];
							$temp_user['user_firstname'] = $_user['user_firstname'];
							$temp_user['user_lastname'] = $_user['user_lastname'];
							$temp_user['image_url'] = $this->getImage($_user['user_image'],$live_path);
							$users_list[] = $temp_user;
						}
						
						$key = array_rand($users,1);
						$winner = $users[$key];

						$image_url = $this->getImage($winner['user_image'],$live_path);
						
						$name_winner = ($winner['user_bio'] != '') ? $winner['user_bio'] : $winner['user_firstname'];
						
						$winner_html .= "<div class='row'><div class='col-md-12'>";
						$winner_html .= "<div class='col-md-2'><img src='$image_url' style='height:50px;width:50px;float:left'></div><div class='col-md-8'><b>Winner Name</b> : ".$name_winner."</div>";
						$winner_html .= "</div></div>";			
						$data = array('users'=>$users_list);	
					}
				}

			} else {
				$sql = "SELECT user_master.*, rel.user_id, rel.draw_id, rel.status from user_master, draw_eligible_users rel where user_master.user_master_id = rel.user_id and rel.draw_id = {$draw_id} and rel.status='active' and user_master.is_deleted = 0 and user_master.status='active'";
				$users = $this->firequery($sql);

				$winner = array();
				$data = array();
				
				$winner_html = '';
				
				if(!empty($users)){
					
					$users_list = array();
					foreach($users as $_user){
						$temp_user = array();
						$temp_user['user_master_id'] = $_user['user_master_id'];
						$temp_user['user_bio'] = $_user['user_bio'];
						$temp_user['user_firstname'] = $_user['user_firstname'];
						$temp_user['user_lastname'] = $_user['user_lastname'];
						$temp_user['image_url'] = $this->getImage($_user['user_image'],$live_path);
						$users_list[] = $temp_user;
					}
					
					$key = array_rand($users,1);
					$winner = $users[$key];

					$image_url = $this->getImage($winner['user_image'],$live_path);
					
					$name_winner = ($winner['user_bio'] != '') ? $winner['user_bio'] : $winner['user_firstname'];
					
					$winner_html .= "<div class='row'><div class='col-md-12'>";
					$winner_html .= "<div class='col-md-2'><img src='$image_url' style='height:50px;width:50px;float:left'></div><div class='col-md-8'><b>Winner Name</b> : ".$name_winner."</div>";
					$winner_html .= "</div></div>";			
					$data = array('users'=>$users_list);	
				}
			}
		}
		
		return new Response(json_encode($data));
	}
	
	/**
    * @Route("/getWinnerData/{draw_id}",defaults={"draw_id"="0"})
    * @Template()
    */
    public function getWinnerDataAction($draw_id){
		$name = '';
		$email = '';
		$status = false;
		if($draw_id != ''){
			
			$em = $this->getDoctrine()->getManager();
			$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));

			if(!empty($draw_check)){
				$user_id = $draw_check->getWinner_id();
			}
			
			if(isset($user_id)){
				$user = $this->getDoctrine()->getRepository('AdminBundle:Usermaster')->findOneBy([
					'user_master_id' => $user_id,
					'is_deleted' => 0,
					'status' => 'active'
				]);
				if(!empty($user)){
					$name = ($user->getUser_bio() != '') ? $user->getUser_bio() : $user->getUser_firstname();
					$email = $user->getEmail();
					$status = true;
				}
			}
		}
		
		$data = array(
			'name' => $name,
			'email' => $email,
			'status' => $status
		);
		
		echo json_encode($data);exit;
	}
	
    /**
    * @Route("/getWinner/{user_id}/{draw_id}",defaults={"user_id"="0","draw_id"="0"})
    * @Template()
    */
    public function getwinnerAction($user_id, $draw_id){
	
		if($user_id != 0 && $draw_id != 0){
			
			$draw = $this->getDoctrine()->getRepository('AdminBundle:Drawmaster')->findOneBy([
				'draw_master_id' => $draw_id,
				'status' => 'active',
				'is_deleted' => 0
			]);
			
			$draw_date = '';
			if(!empty($draw)){
				$draw_date = $draw->getWinner_declare_date();
			}
			
			$em = $this->getDoctrine()->getManager();
			$users = array();
			
			$sql = "select * from user_master where is_deleted=0 and user_master_id=$user_id";
			$con = $em->getConnection();
			$stmt = $con->prepare($sql);
			$stmt->execute();
			$users = $stmt->fetchAll();
			
			$winner = array();
			
			$winner_html = '';
			
			if(!empty($users)){
				$winner = $users[0];

				$live_path = $this->container->getParameter('live_path');
				
				$image_url = $this->getImage($winner['user_image'],$live_path);
				
				$name_winner = ($winner['user_bio'] != '') ? $winner['user_bio'] : $winner['user_firstname'];
				$email = $winner['email'];
				
				$winner_html .= "<div class='row'><div class='col-md-12'>";
				$winner_html .= "<div class='col-md-2'><img src='$image_url' style='height:50px;width:50px;float:left'></div><div class='col-md-8'><span><b>Winner Name</b> : ".$name_winner."</span><br><span><b>Winner Email</b> : ".$email."</span><br><span><b>Draw date</b> : ".$draw_date."</span></div>";
				$winner_html .= "</div></div>";			
			
			
			}
			$data = array('html'=>$winner_html,'user_id'=>$winner['user_master_id']);
		} else {
			$data = array('html' => '', 'user_id' => '');
		}
		
		return new Response(json_encode($data));
	}
	
	
	
	
    /**
    * @Route("/deleteDraw/{type}/{draw_id}",defaults={"draw_id"="","type"=""})
    * @Template()
    */
    public function deletedrawAction($draw_id,$type){		
	
		$em = $this->getDoctrine()->getManager();
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			
		if($draw_check){
			$draw_check->setIs_deleted(1);
			$em->flush();		
		}
		$this->get('session')->getFlashBag()->set('success_msg', 'Draw Deleted successfully');				
				
		return $this->redirectToRoute('admin_draw_index',array('domain'=>$this->get('session')->get('domain'),'type'=>$type));		
	}		
	

    /**
    * @Route("/setWinner")
    * @Template()
    */
    public function setWinnerAction(Request $req){		
	
		$em = $this->getDoctrine()->getManager();
		$draw_id = $req->request->get('draw_id');
		$winner_id = $req->request->get('winner_id');
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			
		if($draw_check){
			$draw_check->setWinner_id($winner_id);
			$draw_check->setWinner_declare_date(date('Y-m-d H:i:s'));
			$em->flush();	
			
			#loyaltyPoints changes
			if(!empty($draw_check->getDraw_master_id())){
									
				$activity_type = 'winner_of_draw';
				$points_added = $this->addCompetitionPointsToUser($winner_id,$activity_type,0,$draw_check->getDraw_master_id(),'draw_master');
			}
			#loyaltyPoints changes

		}
		return new Response('winner set');
	}		
	

    /**
    * @Route("/deleteWinner")
    * @Template()
    */
    public function deletewinnerAction(Request $req){		
	
		$em = $this->getDoctrine()->getManager();
		$draw_id = $req->request->get('draw_id');
		
		$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(array('draw_master_id'=>$draw_id,'is_deleted'=>0));			
		if($draw_check){
			$draw_check->setWinner_id(0);
			$draw_check->setWinner_declare_date(null);
			$em->flush();		
		}
		return new Response('winner deleted');
	}		
	
	
	
    /**
    * @Route("/saveDraw")
    * @Template()
    */
    public function savedrawAction(Request $req){	

		// echo "<pre>";print_r($_REQUEST);exit;
		$em = $this->getDoctrine()->getManager();
		$draw_type = $req->request->get('draw_type');
		if($req->request->all()){
			$draw_name = $req->request->get('draw_title');
			$draw_desc = $req->request->get('draw_desc');
			
			$all_time = ($req->request->get('all_time') != null ) ? true : false;		
			
			$relation_id = ($req->request->get('relation_id') != null ) ? $req->request->get('relation_id') : 0;
			$branch_id = ($req->request->get('branch_id') != null ) ? $req->request->get('branch_id') : 0;
			$category_id = ($req->request->get('category_id') != null ) ? $req->request->get('category_id') : 0;
			$foodtype_id = ($req->request->get('foodtype_id') != null ) ? $req->request->get('foodtype_id') : 0;
			
			if($all_time){
				$start_date = null;
			}else{
				$start_date = date('Y-m-d H:i:s',strtotime($req->request->get('start_date')));	
			}

			$end_date = $req->request->get('end_date');
			$qualification_number = $req->request->has('qualification_number') ? $req->request->get('qualification_number') : 1;
			$qualification_status = $req->request->has('qualification_status') ? $req->request->get('qualification_status') : 'inactive';
			if($qualification_status == 'inactive'){
				$qualification_number = 1;
			}
			$status = $req->request->get('status');
			$draw_type = $req->request->get('draw_type');
			
			$draw_check = $em->getRepository(Drawmaster :: class)->findOneBy(
				array(
					'draw_master_id' => $req->request->get('draw_id'),
					'is_deleted'=>0
				)
			);
			
			if($draw_check){
				$draw_check->setDraw_name($draw_name);
				$draw_check->setDraw_description($draw_desc);
				$draw_check->setDraw_type($draw_type);
				$draw_check->setStart_date($start_date);
				$draw_check->setEnd_date(date('Y-m-d H:i:s',strtotime($end_date)));
				$draw_check->setQualification_number($qualification_number);
				$draw_check->setQualification_status($qualification_status);
				$draw_check->setStatus($status);
				$draw_check->setCreated_date(date('Y-m-d H:i:s'));
				$draw_check->setCreated_by($this->get('session')->get('admin_user_id'));
				$draw_check->setWinner_id(0);
				$draw_check->setRelation_id($relation_id);
				$draw_check->setCategory_id($category_id);
				$draw_check->setFoodtype_id($foodtype_id);
				$draw_check->setMain_branch_id($branch_id);
				$draw_check->setWinner_declare_date(null);
				$draw_check->setIs_deleted(0);
			
				$em->flush();
				
				$draw = $draw_check;
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Draw Updated successfully');
			}else{

				$new_draw = new Drawmaster();
				$new_draw->setDraw_name($draw_name);
				$new_draw->setDraw_description($draw_desc);
				$new_draw->setDraw_type($draw_type);
				$new_draw->setStart_date($start_date);
				$new_draw->setEnd_date(date('Y-m-d H:i:s',strtotime($end_date)));
				$new_draw->setQualification_number($qualification_number);
				$new_draw->setQualification_status($qualification_status);
				$new_draw->setStatus($status);
				$new_draw->setCreated_date(date('Y-m-d H:i:s'));
				$new_draw->setCreated_by($this->get('session')->get('admin_user_id'));
				$new_draw->setWinner_id(0);
				$new_draw->setRelation_id($relation_id);
				$new_draw->setCategory_id($category_id);
				$new_draw->setFoodtype_id($foodtype_id);
				$new_draw->setMain_branch_id($branch_id);
				$new_draw->setWinner_declare_date(null);
				$new_draw->setIs_deleted(0);
				
				$em->persist($new_draw);
				$em->flush();
				
				$draw = $new_draw;
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Draw Inserted successfully');
			}
			
			if(!empty($draw)){
				
				// remove older draw-users
				$draw_id = $draw->getDraw_master_id();
				$em = $this->getDoctrine()->getManager();
				$olderRelation = $em->getRepository('AdminBundle:Draweligibleusers')->findBy(array(
					'draw_id' => $draw_id
				));
				
				if(!empty($olderRelation)){
					foreach($olderRelation as $_olderVal){
						$em->remove($_olderVal);
						$em->flush();
					}
				}
				
				// eligible user
				$users = $this->getEligibleUsers($draw_type, $draw, $qualification_number);

				if(!empty($users)){
					$em = $this->getDoctrine()->getManager();
					foreach($users as $user){

						$draw_eligible_users = new Draweligibleusers();
						$draw_eligible_users->setDraw_id($draw_id);
						$draw_eligible_users->setUser_id($user['user_master_id']);
						$draw_eligible_users->setStatus('active');
						$draw_eligible_users->setIs_deleted(0);
						
						$em->persist($draw_eligible_users);
						$em->flush();
					}
				}
			}
			
			return $this->redirectToRoute('admin_draw_index',array('domain'=>$this->get('session')->get('domain'),'type'=>$draw_type));
			
		}else{
			return $this->redirectToRoute('admin_draw_index',array('domain'=>$this->get('session')->get('domain'),'type'=>$draw_type));
		}
	}
	
	public function getEligibleUsers($type, $draw, $qualification_number = 1){
		$draw_id = $draw->getDraw_master_id();
		$start_date = $draw->getStart_date();
		$end_date = $draw->getEnd_date();
		
		$relation_id = $draw->getRelation_id();
		$main_branch_id = $draw->getMain_branch_id();
		
		$relation_condtion = "";
		
		if($relation_id != 0 && ($type == 'evaluation' || $type == 'eval_with_gallery')){
			$relation_condtion = " AND evaluation_feedback.restaurant_id = $relation_id";			

			if($main_branch_id != 0){
				$relation_condtion .= " AND evaluation_feedback.main_branch_id = '$main_branch_id'";
			}	
		}
		
		if($relation_id != 0 && $type == 'voting'){
			$relation_condtion = " AND product_id = $relation_id";				
		}			
		
		
		if($type == 'voting'){
			if($start_date == null){
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from vote_master where is_deleted = 0 and created_datetime < '$end_date' $relation_condtion group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and is_deleted=0";
				} else {
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from vote_master where is_deleted = 0 and created_datetime < '$end_date' $relation_condtion) and user_role_id = 3 and is_deleted=0";
				}
			}else{
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from vote_master where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' $relation_condtion group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and is_deleted=0";
				} else {
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from vote_master where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' $relation_condtion) and user_role_id = 3 and is_deleted=0";
				}
			}
		}
		
		if($type == 'new_user' || $type == 'all_user'){
			
			if($start_date == null){
				$sql = "SELECT user_master_id from user_master where user_type='user' and user_role_id = 3 and is_deleted=0 and created_datetime < '$end_date'";
			}else{
				$sql = "SELECT user_master_id from user_master where user_type='user' and user_role_id = 3 and is_deleted=0 and created_datetime < '$end_date' and created_datetime > '$start_date'";
			}
		}			
	
		if($type == 'evaluation'){
			if($start_date == null){
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from evaluation_feedback where is_deleted = 0 and created_datetime < '$end_date' and status='approved' $relation_condtion group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and is_deleted=0";

					// $sql = "SELECT user_master_id, count(*) as total from evaluation_feedback e, user_master u where e.user_id = u.user_master_id and e.is_deleted = 0 and user_id in (SELECT user_id from evaluation_feedback where created_datetime < '{$end_date}' and status='approved' group by user_id having count(user_id) >= {$qualification_number}) and e.created_datetime < '{$end_date}' and e.status='approved' and u.is_deleted = 0 group by user_master_id order by user_master_id";
				} else {
					$sql = "select user_master_id from user_master where user_master_id IN (select DISTINCT user_id from evaluation_feedback where is_deleted = 0 and created_datetime < '$end_date' and status='approved' $relation_condtion) and user_role_id = 3 and is_deleted=0";
				}
			}else{
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from evaluation_feedback where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and status='approved' $relation_condtion group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and is_deleted=0";

					// $sql = "SELECT user_master_id, count(*) as total from evaluation_feedback e, user_master u where e.user_id = u.user_master_id and e.is_deleted = 0 and user_id in (SELECT user_id from evaluation_feedback where created_datetime < '{$end_date}' and created_datetime > '{$start_date}' and status='approved' group by user_id having count(user_id) >= {$qualification_number}) and e.created_datetime < '{$end_date}' and e.created_datetime > '{$start_date}' and e.status='approved' and u.is_deleted = 0 group by user_master_id order by user_master_id";
				} else {
					$sql = "select user_master_id from user_master where user_master_id IN (select DISTINCT user_id from evaluation_feedback where is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and status='approved' $relation_condtion) and user_role_id = 3 and is_deleted=0";
				}
			}
		}

		if($type == 'ask_community'){
			if($start_date == null){
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from community_chat where community_chat.is_deleted = 0 and parent_id != 0 and created_datetime < '$end_date' group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and user_master.is_deleted=0";
				} else {
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from community_chat where community_chat.is_deleted = 0 and parent_id != 0 and created_datetime < '$end_date') and user_role_id = 3 and user_master.is_deleted=0";
				}
			}else{
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from community_chat where community_chat.is_deleted = 0 and parent_id != 0 and created_datetime < '$end_date' and created_datetime > '$start_date' group by user_id having count(user_id) >= {$qualification_number}) and user_role_id = 3 and user_master.is_deleted=0";
				} else {
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from community_chat where community_chat.is_deleted = 0 and parent_id != 0 and created_datetime < '$end_date' and created_datetime > '$start_date') and user_role_id = 3 and user_master.is_deleted=0";
				}
			}
		}
		
		if($type == 'eval_with_gallery'){	
			if($start_date == null){
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from evaluation_feedback
					JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
					where evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and e_f_g.is_deleted = 0 and status='approved' $relation_condtion group by user_id having count(user_id) >= {$qualification_number}) and user_master.is_deleted=0 and user_role_id = 3";
				} else {
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from evaluation_feedback
					JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
					where evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and e_f_g.is_deleted = 0 and status='approved' $relation_condtion) and user_master.is_deleted=0 and user_role_id = 3";
				}
			}else{
				if($qualification_number > 1){
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT user_id from evaluation_feedback JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
					where evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and e_f_g.is_deleted = 0 and status='approved' $relation_condtion group by user_id having count(user_id) >= {$qualification_number}) and user_master.is_deleted=0 and user_role_id = 3";
				} else {
					$sql = "SELECT user_master_id from user_master where user_master_id IN (SELECT DISTINCT user_id from evaluation_feedback JOIN evaluation_feedback_gallery e_f_g ON evaluation_feedback.evaluation_feedback_id = e_f_g.evaluation_feedback_id 
					where evaluation_feedback.is_deleted = 0 and created_datetime < '$end_date' and created_datetime > '$start_date' and e_f_g.is_deleted = 0 and status='approved' $relation_condtion) and user_master.is_deleted=0 and user_role_id = 3";
				}
			}
		}
		
		if($type == 'loyalty_points'){
			
			if($start_date == null){
				$sql = "SELECT user_id as user_master_id from competition_user_relation where is_deleted=0 and created_datetime < '$end_date' group by user_id";
			}else{
				$sql = "SELECT user_id as user_master_id from competition_user_relation where is_deleted=0 and created_datetime <= '$end_date' and created_datetime >= '$start_date' group by user_id";	
			}
		}
		
		$users = array();
		if(isset($sql)){
			$users = $this->firequery($sql);
		}

		return $users;
	}
}
