<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AdminBundle\Entity\Usermaster;
use AdminBundle\Entity\Aboutus;
use AdminBundle\Entity\Categorymaster;
use AdminBundle\Entity\Mediatype;
use AdminBundle\Entity\Medialibrarymaster;
use AdminBundle\Entity\Languagemaster;
use AdminBundle\Entity\Governoratemaster;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * @Route("/admin")
 */
class CategoryController extends BaseController {
    public function __construct() {
        parent::__construct();
        $obj = new BaseController();
        $obj->checkSessionAction();
    }

    /**
     * @Route("/category")
     * @Template()
     */
    public function indexAction() {
		$right_codes = $this->userrightsAction();
        $language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));

		$sql = "select cat.*, media.media_title, media.media_location from category_master cat left join media_library_master media on cat.category_image_id = media.media_library_master_id where cat.is_deleted = 0 group by cat.main_category_id order by sort_order desc";

		$conn = $this->getDoctrine()->getManager()->getConnection();
		$prepare = $conn->prepare($sql);
		$prepare->execute();
		$category_list = $prepare->fetchAll();

		$lan_cat = "select category_master_id, main_category_id,language_id, category_name from category_master where is_deleted = 0";
		$prepare = $conn->prepare($lan_cat);
		$prepare->execute();
		$lan_wise_category = $prepare->fetchAll();

		$i = 0;
		$all_category = array();
		foreach($category_list as $_category_list){
			$lan_cat= array();
			foreach($lan_wise_category as $_lan_wise_category){

				if($_category_list['category_master_id'] == $_lan_wise_category['category_master_id'] && $_category_list['language_id'] == $_lan_wise_category['language_id']){
					$lan_cat[] = $_lan_wise_category;
				}

				if($_category_list['category_master_id'] == $_lan_wise_category['main_category_id'] && $_category_list['language_id'] != $_lan_wise_category['language_id']){
					$lan_cat[] = $_lan_wise_category;
				}
			}

			$_category_list['lang_wise_category'] = $lan_cat;

			## sub category list
			$subcatrgory_query = "SELECT * from category_master where parent_category_id = {$_category_list['main_category_id']} and is_deleted = 0 group by main_category_id";
			$subcatrgory_list = $this->firequery($subcatrgory_query);

			$sub_cat = false;
			if(!empty($subcatrgory_list)){
				$sub_cat = true;
			}

			$_category_list['have_sub_category'] = $sub_cat;

			$all_category[] = $_category_list;
		}

		return array(
			"languages" => $language_list,
			'category_list' => $all_category,
			'right_codes' => $right_codes
		);
    }

     /**
     * @Route("/addcategory/{catmain_id}", defaults = {"catmain_id" = ""})
     * @Template()
     */
    public function addcategoryAction($catmain_id) {
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SAC1", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }

		$language_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Languagemaster')->findBy(array("is_deleted"=>0));
		$em = $this->getDoctrine()->getManager();
		$domain_id = $this->get('session')->get('domain_id');
                $food_type_arr_final = [] ;
		$parent_category_list = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Categorymaster')->findBy(array("is_deleted"=>0,'language_id'=>'1'));
		if(!empty($parent_category_list)){
                    foreach($parent_category_list as $key=>$val){
                        $categories1 = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>'2','main_category_id'=>$val->getMain_category_id()));
                       $ar = !empty($categories1)?$categories1->getCategory_name():'';
                         $cate_list[]=array('main_category_id'=>$val->getMain_category_id(),'category_master_id'=>$val->getParent_category_id(),'category_name'=>!empty($ar)?$val->getCategory_name().'/'.$ar:$val->getCategory_name());
                    }
                }
		$category = array();
		if(isset($catmain_id) && $catmain_id != ''){
                    $repository = $this->getDoctrine()->getRepository(Categorymaster::class);
                    $existing_category_list = $repository->findBy(
                                    array(
                                            'main_category_id' => $catmain_id,
                                            'is_deleted' => 0
                                    )
                            );

                    foreach($existing_category_list as $existing_category){
				if($existing_category->getIs_deleted() == 0){
					$repository = $this->getDoctrine()->getManager()->getRepository(Medialibrarymaster::class);
					$media_file = $repository->find($existing_category->getCategory_image_id());

					if(!empty($media_file)){
						$media_url = $this->container->getParameter('live_path').$media_file->getMedia_location().'/'.$media_file->getMedia_name();
					} else {
						$media_url = $this->container->getParameter('live_path').'bundles/Resource/default.png';
					}

					$category[] = array(
						'category_master_id' => $existing_category->getCategory_master_id(),
						'category_name' => $existing_category->getCategory_name(),
						'language_id' => $existing_category->getLanguage_id(),
						'main_category_id' => $existing_category->getMain_category_id(),
						'parent_category_id' => $existing_category->getParent_category_id(),
						'category_image_id' => $existing_category->getCategory_image_id(),
						'category_status' => $existing_category->getCategory_status(),
						'sort_order' => $existing_category->getSort_order(),
						'is_deleted' => $existing_category->getIs_deleted(),
						'media_url' => $media_url
					);

				} else {
					$category = array();
				}
			}


                    //get Food type list =================
                    $food_type_query = "SELECT * FROM `food_type_master` where main_food_type_id IN (SELECT main_food_type_id FROM `food_type_category_relation` WHERE `main_category_id` = ".$catmain_id." AND `is_deleted` = 0) and is_deleted = 0 GROUP by main_food_type_id";
                    $food_type_list = $this->firequery($food_type_query);
                    if(!empty($food_type_list)){
                        foreach($food_type_list as $fkey=>$fval){
                            $main_food_type_id = $fval['main_food_type_id'];
                            $language_list = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Languagemaster")->findBy(array("is_deleted"=>0));
                            if(!empty($language_list)){
                                $food_type_lang_wise = [] ;
                                foreach($language_list as $langkey=>$langval){
                                    $food_type_info = $this->getDoctrine()->getManager()->getRepository("AdminBundle:Foodtypemaster")->findOneBy(array("is_deleted"=>0,"main_food_type_id"=>$main_food_type_id,"language_id"=>$langval->getLanguage_master_id()));
                                    if(!empty($food_type_info)){
                                        $food_type_lang_wise[] = array(
                                            "food_type_id"=>$food_type_info->getFood_type_master_id(),
                                            "food_type_name"=>$food_type_info->getFood_type_name(),
                                            "language_id"=>$langval->getLanguage_master_id()
                                        );
                                    }
                                }
                                $food_type_arr_final[] = array(
                                        "main_food_type_id"=>$main_food_type_id,
                                        "lang_wise_food_type"=>$food_type_lang_wise
                                    );
                            }
                        }
                    }
		}

//		echo "<pre>";print_r($food_type_arr_final);
 //               exit;
		return array(
			"languages" => $language_list ,
			"parentcategory" => $cate_list,
			"category" => $category,
                        "food_type_list"=>$food_type_arr_final

		);
    }

    /**
     * @Route("/addcategorydb")
     * @Template()
     */
    public function addcategorydbAction(Request $request) {
		
//		print_r($_REQUEST);exit;
		if($request->request->all()){
			$em = $this->getDoctrine()->getManager();

			$media = $_FILES['image']['name'];
			if(isset($media) && $media != ''){

				$upload_dir = $this->container->getParameter('upload_dir1');
				$location = '/Resource/category';

				$mediatype = $this->getDoctrine()
						->getManager()
						->getRepository(Mediatype::class)
						->findOneBy(array(
							'media_type_name' => 'Image',
							'is_deleted'=>0)
						);
				$allowedExts = explode(',',$mediatype->getMedia_type_allowed());
				$temp = explode('.',$_FILES['image']['name']);
				$extension = end($temp);
				if(in_array($extension, $allowedExts)){
					
					// for croped image
					$isValid = true;
					$coords = array();
					if($request->get('coords')){
						$coord_string = $request->get('coords');
						$coords = explode(',', $coord_string);
						
						if(count($coords) != 4){
							$isValid = false;
						}
					}
					if(($isValid) && (!empty($coords))){
						// $coords - [x, y, width, height]
						$media_id = $this->mediaUploadWithCrop($_FILES['image'], $upload_dir, $location, $mediatype->getMedia_type_id(), $coords);
					} else {
						$media_id = $this->mediaupload($_FILES['image'], $upload_dir, $location, $mediatype->getMedia_type_id());
					}
				}
			}

			if($request->get('language_id') && $request->get('category_id')){
				/* for edit */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SUC2", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */

				//$category = $em->getRepository(Categorymaster::class)->find($request->get('category_id'));
				$category = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>$request->get('language_master_id'),'main_category_id'=>$request->get('category_id')));

				if(isset($_REQUEST['category_name_ar']) && !empty($_REQUEST['category_name_ar'])){
					$category_ar = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>2,'main_category_id'=>$request->get('category_id')));

					if(!empty($category_ar)){
						$category_ar->setCategory_name($request->get('category_name_ar'));
						$category_ar->setLanguage_id(2);
						$category_ar->setParent_category_id(0);
						if(isset($media_id)){
								$category_ar->setCategory_image_id($media_id);
						}
						$category_ar->setSort_order($request->get('sort_order'));
						$category_ar->setCategory_status($request->get('status'));
						$em->flush();

					}
				}
				if(!empty($category)){
					$category->setCategory_name($request->get('category_name'));
					$category->setLanguage_id($request->get('language_master_id'));
					$category->setParent_category_id(0);
					if(isset($media_id)){
							$category->setCategory_image_id($media_id);
					}
					$category->setSort_order($request->get('sort_order'));
					$category->setCategory_status($request->get('status'));
					$em->flush();

					$this->get('session')->getFlashBag()->set('success_msg', 'Data updated successfully');
				}else{
					$category1 = $em->getRepository(Categorymaster :: class)->findOneBy(array('is_deleted'=>0,'language_id'=>2,'main_category_id'=>$request->get('category_id')));
					$category1->setCategory_name($request->get('category_name_ar'));
					$category1->setLanguage_id(2);
					$category1->setParent_category_id(0);
					if(isset($media_id)){
							$category1->setCategory_image_id($media_id);
					}
					$category1->setSort_order($request->get('sort_order'));
					$category1->setCategory_status($request->get('status'));
					$em->flush();

					$this->get('session')->getFlashBag()->set('success_msg', 'Data updated successfully');
				}
			} else {
				/* for insert */
				
				/* check for access */
				$right_codes = $this->userrightsAction();
				$rights_search = in_array("SAC1", $right_codes);
				if ($rights_search != 1) {
					$this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
					return $this->redirect($this->generateUrl('admin_dashboard_index'));
				}
				/* end: check for access */
				
				$category = new Categorymaster();
				$category->setCategory_name($request->get('category_name'));
				$category->setLanguage_id($request->get('language_master_id'));
				$category->setParent_category_id(0);
				if(isset($media_id)){
					$category->setCategory_image_id($media_id);
				}
				if($request->get('sort_order') == ''){
					$category->setSort_order(0);
				} else {
					$category->setSort_order($request->get('sort_order'));
				}

				$category->setCategory_status($request->get('status'));

				$em->persist($category);
				$em->flush();

				if($request->get('main_category_id')){
					$category->setMain_category_id($request->get('main_category_id'));
				} else {
					$category->setMain_category_id($category->getCategory_master_id());
				}

				$em->flush();
                                $category1 = new Categorymaster();
				$category1->setCategory_name($request->get('category_name_ar'));
				$category1->setLanguage_id(2);
				if($request->get('parent_category_id')){
					$category1->setParent_category_id($request->get('parent_category_id'));
				} else {
					$category1->setParent_category_id(0);
				}
				if(isset($media_id)){
					$category1->setCategory_image_id($media_id);
				}
				if($request->get('sort_order') == ''){
					$category1->setSort_order(0);
				} else {
					$category1->setSort_order($request->get('sort_order'));
				}
				$category1->setMain_category_id($category->getCategory_master_id());
				$category1->setCategory_status($request->get('status'));

				$em->persist($category1);
				$em->flush();

				$this->get('session')->getFlashBag()->set('success_msg', 'Data inserted successfully');
			}

		} else {
			$this->get('session')->getFlashBag()->set('error_msg', 'Something went wrong');
		}
		return $this->redirectToRoute('admin_category_index');
    }

	public function mediaupload($file, $upload_dir, $location, $media_type_id){

		$upload_dir = $upload_dir.$location;
		if(!is_dir($upload_dir)){
			mkdir($upload_dir);
		}

		$clean_image = preg_replace('/\s+/', '', $file['name']);
        $media_name = date('Y_m_d_H_i_s') . '_' . $clean_image;

		$is_uploaded = move_uploaded_file($file['tmp_name'], $upload_dir.'/'.$media_name);

		if($is_uploaded){
			$em = $this->getDoctrine()->getManager();

			$mediamaster = new Medialibrarymaster();
			$mediamaster->setMedia_type_id($media_type_id);
			$mediamaster->setMedia_title($media_name);
			$mediamaster->setMedia_location("/bundles/{$location}");
			$mediamaster->setMedia_name($media_name);
			$mediamaster->setCreated_on(date('Y-m-d h:i:s'));
			$mediamaster->setIs_deleted(0);

			$em->persist($mediamaster);
			$em->flush();

			return $mediamaster->getMedia_library_master_id();
		}
		return false;
	}

	/**
	* @Route("/deleteCategory/{cat_id}", defaults = {"cat_id" = ""})
	*/
	public function deleteCategoryAction($cat_id, Request $request){
		$right_codes = $this->userrightsAction();
		$rights_search = in_array("SDC3", $right_codes);
        if ($rights_search != 1) {
            $this->get('session')->getFlashBag()->set('error_msg', 'You do not have the privileges to perform this action');
            return $this->redirect($this->generateUrl('admin_dashboard_index'));
        }
		
		if(isset($cat_id) && $cat_id != ''){

			$em = $this->getDoctrine()->getManager();
			$category = $em->getRepository(Categorymaster::class)->find($cat_id);

			if(!empty($category)){
				$category->setIs_deleted(1);
				$em->flush();

				// remove category_foodtype relation
				$cat_relation = $em->getRepository('AdminBundle:Foodtypecategoryrelation')->findBy(
					array(
						'is_deleted'=>0,
						'main_category_id'=>$cat_id
					)
				);
				
				if(!empty($cat_relation)){
					foreach($cat_relation as $_rel){
						$em->remove($_rel);
						$em->flush();
					}
				}
				
				// remove restaurant_category_relation
				$res_category_relation = $em->getRepository('AdminBundle:Restaurantcategoryrelation')->findBy(
					array(
						'is_deleted'=>0,
						'main_category_id'=>$cat_id
					)
				);
				
				if(!empty($res_category_relation)){
					foreach($res_category_relation as $_rel){
						$em->remove($_rel);
						$em->flush();
					}
				}
				
				// remove restaurant_foodtype_relation
				$res_category_foodtype_relation = $em->getRepository('AdminBundle:Restaurantfoodtyperelation')->findBy(
					array(
						'is_deleted'=>0,
						'main_caetgory_id'=>$cat_id
					)
				);
				
				if(!empty($res_category_foodtype_relation)){
					foreach($res_category_foodtype_relation as $_rel){
						$em->remove($_rel);
						$em->flush();
					}
				}
				
				$this->get('session')->getFlashBag()->set('success_msg', 'Category deleted successfully');
			} else {
				$this->get('session')->getFlashBag()->get('error_msg', 'Category not found');
			}
		} else {
			$this->get('session')->getFlashBag()->get('error_msg', 'Something went wrong');
		}

		$referer = $request->headers->get('referer');
		return $this->redirect($referer);
	}

	/**
	* @Route("/cat/ajaxupdatestatus")
	*/
	public function ajaxupdatestatusAction(Request $request){

		if($request->get('cat_id')){
			$em = $this->getDoctrine()->getManager();
			$category = $em->getRepository(Categorymaster::class)->findBy(array('main_category_id' => $request->get('cat_id')));

			  if(!empty($category)){
				  foreach ($category as $key => $value) {
					$current_status = $value->getCategory_status();
						if($current_status == 'active'){
							$value->setCategory_status('inactive');
						} else {
							$value->setCategory_status('active');
						}
					$em->flush();
				  }
			  }
			echo 'true';exit;
		}
		echo 'false';exit;
	}

	/**
	* @Route("/sub-category/list/{category_id}", defaults = {"category_id" = ""})
	* @Template()
	*/
	public function subcategory_listAction($category_id, Request $request){

		$category_name = '';
		$em = $this->getDoctrine()->getManager();
		$language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));
		$all_subcategory = array();
		if(isset($category_id)){

			## get category name
			$category = $this->getDoctrine()->getRepository('AdminBundle:Categorymaster')->findBy(
				array(
					'main_category_id' => $category_id,
					'category_status' => 'active',
					'is_deleted' => 0
				)
			);

			if(!empty($category)){
				$cat_name = array();
				foreach($category as $_category){
					$cat_name[] = $_category->getCategory_name();
				}
				$category_name = implode(' / ', $cat_name);
			}

			$query = "SELECT cat.* from category_master cat where parent_category_id = {$category_id} and cat.is_deleted = 0 group by main_category_id";
            $subcatrgory_list = $this->firequery($query);

			if(!empty($subcatrgory_list)){
				foreach($subcatrgory_list as $_category){

					$live_path = $this->container->getParameter('live_path');
					$_category['image_url'] = $this->getImage($_category['category_image_id'], $live_path);

					$lang_wise_cat = array();
					foreach($language_list as $_language){
						$cat = $this->getDoctrine()->getRepository('AdminBundle:Categorymaster')->findOneBy([
							'main_category_id' => $_category['main_category_id'],
							'language_id' => $_language->getLanguage_master_id(),
							'is_deleted' => 0
						]);

						if(!empty($cat)){
							$lang_wise_cat[] = array(
								'language_id' => $_language->getLanguage_master_id(),
								'category_name' => $cat->getCategory_name()
							);
						}
					}

					$_category['category_status'] = ucfirst($_category['category_status']);
					$_category['lang_wise_cat'] = $lang_wise_cat;
					$all_subcategory[] = $_category;
				}
			}
		}
		/*
		echo '<pre>';
		print_r($all_subcategory);
		exit;
		 */
		return array(
			'category_name' => $category_name,
			'languages' => $language_list,
			'all_subcategory' => $all_subcategory
		);
	}

	/**
     * @Route("/category/foodtype-list/{category_id}",defaults={"category_id"=0})
     * @Template()
     */
    public function foodtypeListAction($category_id){

		$category_name = '';
		$all_foodtype = array();

		$em = $this->getDoctrine()->getManager();
		$language_list = $em->getRepository(Languagemaster :: class)->findBy(array('is_deleted'=>0));

		if(isset($category_id) && $category_id != ''){

			$category_name_arr = $this->getDoctrine()->getRepository('AdminBundle:Categorymaster')->findBy([
				'is_deleted' => 0,
				'main_category_id' => $category_id
			]);

			if(!empty($category_name_arr)){
				$cat_arr = array();
				foreach($category_name_arr as $_category){
					$cat_arr[] = $_category->getCategory_name();
				}

				if(!empty($cat_arr)){
					$category_name = implode(' / ',$cat_arr);
				}
			}

			$query = "SELECT type.food_type_master_id, type.main_food_type_id, type.food_type_name, type.food_type_image_id, cat.main_category_id, cat.category_master_id from food_type_category_relation rel, food_type_master type, category_master cat where rel.main_food_type_id = type.main_food_type_id and rel.main_category_id  = cat.main_category_id and cat.language_id = 1 and cat.main_category_id = {$category_id} and type.is_deleted = 0 and cat.is_deleted = 0 and rel.is_deleted = 0 group by rel.main_food_type_id";

			$foodtype_list = $this->firequery($query);

			if(!empty($foodtype_list)){
				$live_path = $this->container->getParameter('live_path');
				foreach($foodtype_list as $_foodtype){
					$lang_wise_foodtype = array();
					foreach($language_list as $_language){
						$foodtype = $this->getDoctrine()->getRepository('AdminBundle:Foodtypemaster')->findOneBy([
							'main_food_type_id' => $_foodtype['main_food_type_id'],
							'language_id' => $_language->getLanguage_master_id(),
							'is_deleted' => 0
						]);

						if(!empty($foodtype)){
							$lang_wise_foodtype[] = array(
								'language_id' => $_language->getLanguage_master_id(),
								'foodtype_name' => $foodtype->getFood_type_name()
							);
						}
					}

					$_foodtype['media_url'] = $this->getImage($_foodtype['food_type_image_id'], $live_path);
					$_foodtype['lang_wise_foodtype'] = $lang_wise_foodtype;
					$all_foodtype[] = $_foodtype;
				}
			}
		}

		return array(
			'category_name' => $category_name,
			'languages' => $language_list,
			'foodtype_list' => $all_foodtype
		);
	}
}
