<?php
namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AdminBundle\Entity\Usermaster;



use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;



/**
* @Route("/{domain}")
*/


class UsersController extends BaseController
{
	public function __construct()
    {
		parent::__construct();
        $obj = new BaseController();
		$obj->checkSessionAction();
    }

    /**
     * @Route("/users/{domain_id}",defaults={"domain_id":""})
     * @Template()
     */
    public function indexAction($domain_id)
    {
    	$live_path=$this->container->getParameter('live_path');
		$language = $this->getDoctrine()
					   ->getManager()
					   ->getRepository('AdminBundle:Languagemaster')
					   ->findBy(array('is_deleted'=>0));
		$all_category = '';
		/*if($domain_id == "" || $domain_id == 0 ){*/
		if(!empty($this->get('session')->get('domain_id')))
    	{
			$query= "select * from user_master where is_deleted = 0 and user_role=2";
			$em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
			$em->execute();
			$all_category = $em->fetchAll();
		}else{
			$query= "select * from user_master where is_deleted = 0 and user_role=2 and domain_id='".$this->get('session')->get('domain_id')."'";

			$em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
			$em->execute();
			$all_category = $em->fetchAll();
		}
		$test_img='';
		$image=array();
		$all_category1=array();
		if(!empty($all_category))
		{
			foreach ($all_category as  $all_category) {
				
					$query= "select * from media_library_master where is_deleted = 0 and media_library_master_id='".$all_category['user_image']."'";

					$em = $this->getDoctrine()->getManager()->getConnection()->prepare($query);
					$em->execute();
					$image = $em->fetchAll();
					if(!empty($image))
					{
						$test_img=$live_path.$image[0]['media_location'].'/'.$image[0]['media_name'] ;
					
					}else
					{
						$test_img='' ;
						
					}
				
					 
				$all_category1[]=array(
					'image_url'=>$test_img,
					"user_master_id"=>$all_category['user_master_id'],
					"user_email" =>$all_category['user_email'],
					"first_name" =>$all_category['first_name'],
					"last_name"=>$all_category['last_name'],
					"mhr_number"=>$all_category['mhr_number'],
					"user_school_name"=>$all_category['user_school_name'],
					"user_color_of_first_car"=>$all_category['user_color_of_first_car'],
					"user_city"=>$all_category['user_city'],
					"date_of_birth"=>$all_category['date_of_birth'],
					"name"=>$all_category['name'],
					"hospital_file_number"=>$all_category['hospital_file_number'],
					"name_of_hospital"=>$all_category['name_of_hospital'],
					"following_doctor"=>$all_category['following_doctor'],
					"next_appointment_date"=>$all_category['next_appointment_date'],
					"next_of_ken"=>$all_category['next_of_ken'],
					"summary"=>$all_category['summary'],
					"created_date"=>$all_category['created_date'],
					"status"=>$all_category['status'],
					) ;
			}

		}
		
		 return array("users"=>$all_category1);
		//var_dump($all_category_details);exit;

    }

	/**
     * @Route("/deleteuser/{category_id}",defaults={"category_id":""})
     * @Template()
     */
    public function deleteuserAction($category_id)
    {
		if($category_id != '0'){
			$selected_category = $this->getDoctrine()
					   ->getManager()
					   ->getRepository('AdminBundle:Usermaster')
					   ->findOneBy(array('is_deleted'=>0,'user_master_id'=>$category_id));
			
			if(!empty($selected_category)){
			
					$selected_category->setIs_deleted(1);
					$em = $this->getDoctrine()->getManager();
					$em->persist($selected_category);
					$em->flush();

				
			}

		}
		$this->get("session")->getFlashBag()->set("success_msg","Users Deleted successfully");
	    return $this->redirect($this->generateUrl("admin_users_index",array("domain"=>$this->get('session')->get('domain'))));
	}
	/**
     * @Route("/statususer/{category_id}/{status}",defaults={"category_id":"","status":""})
     * @Template()
     */
    public function statususerAction($category_id,$status)
    {
	
		//if($category_id != '0'){
			$selected_category = $this->getDoctrine()
					   ->getManager()
					   ->getRepository('AdminBundle:Usermaster')
					   ->findOneBy(array('is_deleted'=>0,'user_master_id'=>$category_id));
			if(!empty($selected_category)){
					if($status == 1){
						$selected_category->setStatus(0);
					}else{
						$selected_category->setStatus(1);
					}
$em = $this->getDoctrine()->getManager();
					$em->persist($selected_category);
					$em->flush();
				
			}
			
		//}
		$this->get("session")->getFlashBag()->set("success_msg","User Status Changed successfully");
	    return $this->redirect($this->generateUrl("admin_users_index",array("domain"=>$this->get('session')->get('domain'))));
	}
	

}
